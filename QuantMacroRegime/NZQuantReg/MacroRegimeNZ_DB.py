#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Imports
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import datetime as dt
from pandas.tseries.offsets import *
import numpy as np
import scipy.stats as scs

import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.dates import YearLocator, MonthLocator

# In[2]:


# Getting BBG data and cleaning
StartDate = '1995-12-30'
EndDate = (dt.datetime.today() + BDay(30)).strftime("%m/%d/%Y")

Dates = pd.DataFrame(pd.date_range(StartDate, EndDate, freq="D"), columns=['Date'])
Dates = Dates.set_index('Date', drop=True)
Dates.index.name = None

Month = pd.DataFrame(pd.date_range(StartDate, EndDate, freq="M"), columns=['Date'])
Month = Month.set_index('Date', drop=True)
Month.index.name = None

Quarter = pd.DataFrame(pd.date_range(StartDate, EndDate, freq="Q"), columns=['Date'])
Quarter = Quarter.set_index('Date', drop=True)
Quarter.index.name = None

# In[3]:


# Take EPI data from excel

# Read EPI
EPIDat = pd.read_csv(r'./DataUpdateFiles\MacroRegEPI.csv')
EPIDat.index = pd.to_datetime(EPIDat['DATE'], format = "%Y-%m-%d")
#EPIDat = EPIDat.drop(columns = ['DATE'])
EPIDat = EPIDat.drop(columns = ['DATE'])
EPIDat = EPIDat.fillna(method='ffill')
EPIDat = Dates.merge(EPIDat,right_index=True,left_index=True,how='left')
EPIDat = EPIDat.fillna(method='ffill')
EPIDat = Month.merge(EPIDat,right_index=True,left_index=True,how='left')
#EPIDat.tail()

EPIIdx = ['EpiM_Headline_NZ','EpiM_Business_NZ','EpiM_Consumer_NZ','EpiM_Employment_NZ','EpiM_Growth_NZ',
'EpiM_Inflation_NZ']

EPIData = EPIDat[EPIIdx]
#EPIData.tail()

# In[5]:


# Take Bloomberg data from excel

# Read Bloomberg Data
# Read PX last data
PXLastDat = pd.read_csv(r'./DataUpdateFiles\PXLAST.csv')
PXLastDat = pd.pivot_table(PXLastDat, values='Value', index=['Data_Date'],
                    columns=['Security'])
PXLastDat = PXLastDat.fillna(method='ffill')
PXLastDat.index = pd.to_datetime(PXLastDat.index, format = "%Y%m%d")
PXLastDat = Dates.merge(PXLastDat,right_index=True,left_index=True,how='left')
PXLastDat = PXLastDat.fillna(method='ffill')
PXLastDat = Month.merge(PXLastDat,right_index=True,left_index=True,how='left')
#PXLastDat.tail()

PXLastIdx = ['NZNTGCNQ Index','NZBCIV Index','NZMSNHPT Index','NZNTCINS Index',
'NZMTBAL Index','NZMTEXP Index','NZMTIMP Index','EHBBNZY Index','NZNTGOVQ Index','NZLCPY Index',
'NZICSFY Index','NZCPTRSY Index','NZCPCPIY Index','OEEONZUG Index','NZLFUNER Index','NZBCCU Index',
'QVNZTOTL Index','CPNFNZHG Index','CPNFNZNG Index','CPNFNZOG Index','FARBCRED Index',
'EBBSTOTA Index','EURUSD Curncy','BJACTOTL Index','JPYUSD Curncy','CNBMTTAS Index','CNYUSD Curncy',
'NZMSNBM Index','USCABAL Index','ECOCEAS Index','ECOCJPN Index','ECOCCNN Index','NZOCRS Index',
'NDGGBE10 Index','NZCPIYOY Index','USNREUAR Index','USNRUS Index','GNZGB10 Index','196.028 Index',
'CTOTNZD Index']

PXLastData = PXLastDat[PXLastIdx]
#PXLastData.tail()

# Read EPS data
OASDat = pd.read_csv(r'./DataUpdateFiles\OASSPREADMID.csv')
OASDat = pd.pivot_table(OASDat, values='Value', index=['Data_Date'],
                    columns=['Security'])
OASDat = OASDat.fillna(method='ffill')
OASDat.index = pd.to_datetime(OASDat.index, format = "%Y%m%d")
OASDat = Dates.merge(OASDat,right_index=True,left_index=True,how='left')
OASDat = OASDat.fillna(method='ffill')
OASDat = Month.merge(OASDat,right_index=True,left_index=True,how='left')
#OASDat.tail()

OASIdx = ['BNZCR0 Index']

OASData = OASDat[OASIdx]
#OASData.tail()

# Read EPS data
BestEPSDat = pd.read_csv(r'./DataUpdateFiles\BESTEPS.csv')
BestEPSDat = pd.pivot_table(BestEPSDat, values='Value', index=['Data_Date'],
                    columns=['Security'])
BestEPSDat = BestEPSDat.fillna(method='ffill')
BestEPSDat.index = pd.to_datetime(BestEPSDat.index, format = "%Y%m%d")
BestEPSDat = Dates.merge(BestEPSDat,right_index=True,left_index=True,how='left')
BestEPSDat = BestEPSDat.fillna(method='ffill')
BestEPSDat = Month.merge(BestEPSDat,right_index=True,left_index=True,how='left')
#BestEPSDat.tail()

BestEPSIdx = ['NZSE50FG Index']

BestEPSData = BestEPSDat[BestEPSIdx]
#BestEPSData.tail()

# Read PE data
BestPEDat = pd.read_csv(r'./DataUpdateFiles\BESTPERATIO.csv')
BestPEDat = pd.pivot_table(BestPEDat, values='Value', index=['Data_Date'],
                    columns=['Security'])
BestPEDat = BestPEDat.fillna(method='ffill')
BestPEDat.index = pd.to_datetime(BestPEDat.index, format = "%Y%m%d")
BestPEDat = Dates.merge(BestPEDat,right_index=True,left_index=True,how='left')
BestPEDat = BestPEDat.fillna(method='ffill')
BestPEDat = Month.merge(BestPEDat,right_index=True,left_index=True,how='left')
#BestPEDat.tail()

BestPEIdx = ['NZSE50FG Index']

BestPEData = BestPEDat[BestPEIdx]
#BestPEData.tail()

# Read NIMS data
NIMsDat = pd.read_csv(r'./DataUpdateFiles\ANNNETINTMARGIN.csv')
NIMsDat = pd.pivot_table(NIMsDat, values='Value', index=['Data_Date'],
                    columns=['Security'])
NIMsDat = NIMsDat.fillna(method='ffill')
NIMsDat.index = pd.to_datetime(NIMsDat.index, format = "%Y%m%d")
NIMsDat = Dates.merge(NIMsDat,right_index=True,left_index=True,how='left')
NIMsDat = NIMsDat.fillna(method='ffill')
NIMsDat = Month.merge(NIMsDat,right_index=True,left_index=True,how='left')
#NIMsDat.tail()

NIMsIdx = ['CBA AU Equity','WBC NZ Equity','BZL NZ Equity','ANZ NZ Equity']

NIMsData = NIMsDat[NIMsIdx]
#NIMsData.tail()

# In[6]:


# Put the codes in

NamesIdx = ['EPI ex inflation M', 'EPI ex inflation L', 'EPI Synchronised Sectors or Regions M',
            'EPI Synchronised Sectors or Regions L',
            'Earnings Growth M', 'Earnings Growth L', 'Investment/Capex M', 'Investment/Capex L', 'Credit Growth M',
            'Credit Growth L', 'Inventory Cycle M',
            'Inventory Cycle L', 'Global Trade M', 'Global Trade L', 'Fiscal Impulse M', 'Fiscal Impulse L',
            'EPI Inflation M', 'EPI Inflation L', 'Wage Inflation M', 'Wage Inflation L',
            'Core (Persistence/Breadth) M',
            'Core (Persistence/Breadth) L', 'OECD Output Gap M', 'OECD Output Gap L', 'Employment EPI M',
            'Employment EPI L', 'Unemployment level vs NAIRU M', 'Unemployment level vs NAIRU L',
            'Capacity Utilisation M', 'Capacity Utilisation L', 'Property (1y momentum) M', 'Property (level) L',
            'Equities M', 'Equities L', 'Household Leverage M', 'Household Leverage L', 'Corporate Leverage M',
            'Corporate Leverage L', 'Government Leverage M', 'Government Leverage L',
            'Public Sector Liquidity ==> Reserves, Central Bank BS M',
            'Public Sector Liquidity ==> Reserves, Central Bank BS L',
            'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M',
            'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L',
            'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M',
            'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L',
            'Real Policy Rate vs real neutral rate M', 'Real Policy Rate vs real neutral rate L',
            'Real Long End Yields M',
            'Real Long End Yield L', 'Real Ccy vs ToT M', 'Real Ccy vs ToT L', 'Financial Sector Health M',
            'Financial Sector Health L', 'Credit Spreads M', 'Credit Spreads L']

CodesIdx = ['NZSE50FG Index BEPS', 'NZNTGCNQ Index', 'NZBCIV Index', 'NZMSNHPT Index', 'NZNTCINS Index',
            'NZMTBAL Index', 'NZMTEXP Index', 'NZMTIMP Index', 'EHBBNZY Index', 'NZNTGOVQ Index', 'NZLCPY Index',
            'NZICSFY Index', 'NZCPTRSY Index', 'NZCPCPIY Index', 'OEEONZUG Index', 'NZLFUNER Index', 'NZBCCU Index',
            'QVNZTOTL Index', 'NZSE50FG Index PE', 'CPNFNZHG Index', 'CPNFNZNG Index', 'CPNFNZOG Index',
            'FARBCRED Index',
            'EBBSTOTA Index', 'EURUSD Curncy', 'BJACTOTL Index', 'JPYUSD Curncy', 'CNBMTTAS Index', 'CNYUSD Curncy',
            'NZMSNBM Index', 'USCABAL Index', 'ECOCEAS Index', 'ECOCJPN Index', 'ECOCCNN Index', 'NZOCRS Index',
            'NDGGBE10 Index', 'NZCPIYOY Index', 'USNREUAR Index', 'USNRUS Index', 'GNZGB10 Index', '196.028 Index',
            'CTOTNZD Index', 'CBA AU Equity', 'WBC NZ Equity', 'BZL NZ Equity', 'ANZ NZ Equity', 'BNZCR0 Index']

# In[7]:


CodesNamesIdx = ['S&P/NZX 50 Index Gross', 'New Zealand GDP Gross Capital', 'ANZ Survey of Business Opinion',
                 'RBA Financial Aggregate Credit', 'New Zealand Housing & personal', 'NZ GDP Change in Inventories N',
                 'New Zealand Merchandise Trade', 'New Zealand Merchandise Export', 'New Zealand Merchandise Import',
                 'New Zealand Budget Balance (%', 'New Zealand GDP Govt Administr', 'New Zealand Labor Cost All Sal',
                 'RBNZ New Zealand CPI Sectoral', 'New Zealand CPI All Groups 20', 'NZ CPI All groups less Food gr',
                 'OECD Eco Outlook New Zealand O', 'New Zealand Labor Force Unempl', 'ANZ Survey of Business Opinion',
                 'NZ Quotable Value Residential', 'S&P/NZX 50 Index Gross', 'BIS New Zealand Credit to Hous',
                 'BIS New Zealand Credit to Non', 'BIS New Zealand Credit to Gene', 'US Factors Supplying Reserve F',
                 'ECB Balance Sheet All Assets', 'EUR-USD X-RATE', 'Bank of Japan assets:Total',
                 'JPY-USD X-RATE', 'China Central Bank Balance She', 'CNY-USD X-RATE',
                 'NZ Broad money', 'US Nominal Account Balance In', 'Eurozone Current Account Balan',
                 'Japan Current Account Balance', 'China Current Account Balance', 'Reserve Bank of New Zealand Of',
                 'New Zealand Breakeven 10 Year', 'New Zealand CPI All Groups YoY', 'Laubach Williams Natural Rate',
                 'Laubach Williams Natural Rate', 'New Zealand Govt Bond 10 Year', 'IMF New Zealand Real Effective',
                 'Citi Terms of Trade Index - Ne', 'ASB BANK LTD', 'WESTPAC BANKING CORP', 'BANK OF NEW ZEALAND',
                 'AUST AND NZ BANKING GROUP', 'BBG NZB Credit 0+Y']

# In[8]:


#Copies and Adjustments

#Main Code df
RegCode = pd.DataFrame(0,index=Month.index, columns=NamesIdx)

PXLast = PXLastData.copy()
BestEPS = BestEPSData.copy()
BestPE = BestPEData.copy()
BankNIMs = NIMsData.copy()
OASSpread = OASData.copy()

# Add correct api calls
#PXLast = PXLast.drop(columns=['SPX Index'])
PXLast['NZSE50FG Index BEPS'] = BestEPS['NZSE50FG Index'].copy() # EPS Growth
PXLast['NZSE50FG Index PE'] = BestPE['NZSE50FG Index'].copy() # PE Ratio
PXLast['BNZCR0 Index'] = OASSpread['BNZCR0 Index'].copy() # Credit Spread
PXLast['CBA AU Equity'] = BankNIMs['CBA AU Equity'].copy()
PXLast['WBC NZ Equity'] = BankNIMs['WBC NZ Equity'].copy()
PXLast['BZL NZ Equity'] = BankNIMs['BZL NZ Equity'].copy()
PXLast['ANZ NZ Equity'] = BankNIMs['ANZ NZ Equity'].copy()

#Add in EPI
PXLast['EpiM_Headline_NZ'] = EPIData['EpiM_Headline_NZ'].copy()
PXLast['EpiM_Business_NZ'] = EPIData['EpiM_Business_NZ'].copy()
PXLast['EpiM_Consumer_NZ'] = EPIData['EpiM_Consumer_NZ'].copy()
PXLast['EpiM_Employment_NZ'] = EPIData['EpiM_Employment_NZ'].copy()
PXLast['EpiM_Growth_NZ'] = EPIData['EpiM_Growth_NZ'].copy()
PXLast['EpiM_Inflation_NZ'] = EPIData['EpiM_Inflation_NZ'].copy()

# In[9]:


PXLast.tail()

# In[10]:


# Start adjusting data series

# YOY%
PXLast['NZMSNHPT Index'] = (PXLast['NZMSNHPT Index'] / PXLast['NZMSNHPT Index'].shift(12) - 1)
PXLast['NZMTEXP Index'] = (PXLast['NZMTEXP Index'] / PXLast['NZMTEXP Index'].shift(12) - 1)
PXLast['NZMTIMP Index'] = (PXLast['NZMTIMP Index'] / PXLast['NZMTIMP Index'].shift(12) - 1)
PXLast['QVNZTOTL Index'] = (PXLast['QVNZTOTL Index'] / PXLast['QVNZTOTL Index'].shift(12) - 1)

# Unemployment vs NAIRU estimate of 4.5% for Australia
PXLast['NZLFUNER Index'] = (PXLast['NZLFUNER Index'] - 4.5)

# Inverse for unemployment, fiscal and debt, credit spreads
PXLast['NZLFUNER Index'] = -1 * PXLast['NZLFUNER Index']
PXLast['EHBBNZY Index'] = -1 * PXLast['EHBBNZY Index']
PXLast['CPNFNZHG Index'] = -1 * PXLast['CPNFNZHG Index']
PXLast['CPNFNZNG Index'] = -1 * PXLast['CPNFNZNG Index']
PXLast['CPNFNZOG Index'] = -1 * PXLast['CPNFNZOG Index']
PXLast['BNZCR0 Index'] = -1 * PXLast['BNZCR0 Index']

# 12M SUm for inventories, trade balance
PXLast['NZNTCINS Index'] = -1 * PXLast['NZNTCINS Index'].rolling(12).sum()
PXLast['NZMTBAL Index'] = PXLast['NZMTBAL Index'].rolling(12).sum()

# Central Bank Balance sheet, convert all CB B/S to USD and Sum
PXLast['FARBCRED Index'] = ((PXLast['FARBCRED Index'] / 1000) + (PXLast['EBBSTOTA Index'] * PXLast['EURUSD Curncy'])
                            + (PXLast['BJACTOTL Index'] * PXLast['JPYUSD Curncy'])
                            + (PXLast['CNBMTTAS Index'] * PXLast['CNYUSD Curncy']))

# Current Account Balance G4 Sums
PXLast['USCABAL Index'] = PXLast['USCABAL Index'] + PXLast['ECOCEAS Index'] + PXLast['ECOCJPN Index'] + PXLast[
    'ECOCCNN Index']

# Real Rate Calcs policy and long end, and vs neutral rate for policy
PXLast['GNZGB10 Index'] = ((PXLast['NZCPIYOY Index'])
                           - PXLast['GNZGB10 Index'])  # Real long end
PXLast['NZOCRS Index'] = -1 * ((PXLast['NZOCRS Index'] - (PXLast['NZCPIYOY Index']))
                               - (0.3 * PXLast['USNRUS Index'] + 0.6 * PXLast['USNREUAR Index']
                                  + 0.1 * PXLast['GNZGB10 Index'].rolling(12).mean()))  # Real policy rate

# Real currency v Terms of trade
PXLast['196.028 Index'] = PXLast['CTOTNZD Index'] / PXLast['196.028 Index']

# In[11]:


# Z-scoring at different time frames

# 0.5Y
PXLast05Yzs = (PXLast - PXLast.rolling(6).mean()) / PXLast.rolling(6).std()
PXLast05Yzs = PXLast05Yzs.replace([np.inf, -np.inf], np.nan).ffill()

# 1y
PXLast1Yzs = (PXLast - PXLast.rolling(12).mean()) / PXLast.rolling(12).std()
PXLast1Yzs = PXLast1Yzs.replace([np.inf, -np.inf], np.nan).ffill()

# 3Y
PXLast3Yzs = (PXLast - PXLast.rolling(3 * 12).mean()) / PXLast.rolling(3 * 12).std()

# 5y
PXLast5Yzs = (PXLast - PXLast.rolling(5 * 12).mean()) / PXLast.rolling(5 * 12).std()

# 7Y
PXLast7Yzs = (PXLast - PXLast.rolling(7 * 12).mean()) / PXLast.rolling(7 * 12).std()

# 10Y
PXLast10Yzs = (PXLast - PXLast.rolling(10 * 12).mean()) / PXLast.rolling(10 * 12).std()

# In[12]:


# Add in Adjusted Series
# Growth
RegCode['EPI ex inflation M'] = PXLast1Yzs['EpiM_Headline_NZ']
RegCode['EPI ex inflation L'] = PXLast5Yzs['EpiM_Headline_NZ']

'''
RegCode['EPI Synchronised Sectors or Regions M'] = (PXLast1Yzs['EpiM_Business_NZ'] + PXLast1Yzs['EpiM_Consumer_NZ'] +
                                                    PXLast1Yzs['EpiM_Employment_NZ'] + PXLast1Yzs['EpiM_Growth_NZ'] +
                                                    PXLast1Yzs['EpiM_Inflation_NZ']) / 5
RegCode['EPI Synchronised Sectors or Regions L'] = (PXLast5Yzs['EpiM_Business_NZ'] + PXLast5Yzs['EpiM_Consumer_NZ'] +
                                                    PXLast5Yzs['EpiM_Employment_NZ'] + PXLast5Yzs['EpiM_Growth_NZ'] +
                                                    PXLast5Yzs['EpiM_Inflation_NZ']) / 5
'''


RegCode['EPI Synchronised Sectors or Regions M'] = pd.DataFrame([PXLast1Yzs['EpiM_Business_NZ'],
             PXLast1Yzs['EpiM_Consumer_NZ'],
             PXLast1Yzs['EpiM_Employment_NZ'],
             PXLast1Yzs['EpiM_Growth_NZ'],
             PXLast1Yzs['EpiM_Inflation_NZ']]).transpose().mean(axis=1)


RegCode['EPI Synchronised Sectors or Regions L'] = pd.DataFrame([PXLast5Yzs['EpiM_Business_NZ'],
             PXLast5Yzs['EpiM_Consumer_NZ'],
             PXLast5Yzs['EpiM_Employment_NZ'],
             PXLast5Yzs['EpiM_Growth_NZ'],
             PXLast5Yzs['EpiM_Inflation_NZ']]).transpose().mean(axis=1)


RegCode['Earnings Growth M'] = PXLast1Yzs['NZSE50FG Index BEPS']
RegCode['Earnings Growth L'] = PXLast7Yzs['NZSE50FG Index BEPS']
RegCode['Investment/Capex M'] = (PXLast5Yzs['NZNTGCNQ Index'] + PXLast5Yzs['NZBCIV Index']) / 2
RegCode['Investment/Capex L'] = (PXLast10Yzs['NZNTGCNQ Index'] + PXLast3Yzs['NZBCIV Index']) / 2
RegCode['Credit Growth M'] = PXLast1Yzs['NZMSNHPT Index']
RegCode['Credit Growth L'] = PXLast7Yzs['NZMSNHPT Index']
RegCode['Inventory Cycle M'] = PXLast1Yzs['NZNTCINS Index']
RegCode['Inventory Cycle L'] = PXLast7Yzs['NZNTCINS Index']
RegCode['Global Trade M'] = (PXLast1Yzs['NZMTBAL Index'] + PXLast1Yzs['NZMTEXP Index'] + PXLast1Yzs[
    'NZMTIMP Index']) / 3
RegCode['Global Trade L'] = (PXLast7Yzs['NZMTBAL Index'] + PXLast7Yzs['NZMTEXP Index'] + PXLast7Yzs[
    'NZMTIMP Index']) / 3
RegCode['Fiscal Impulse M'] = (PXLast5Yzs['EHBBNZY Index'] + PXLast5Yzs['NZNTGOVQ Index']) / 2
RegCode['Fiscal Impulse L'] = (PXLast10Yzs['EHBBNZY Index'] + PXLast10Yzs['NZNTGOVQ Index']) / 2

# In[13]:


# Output Gap inflation
RegCode['EPI Inflation M'] = PXLast1Yzs['EpiM_Inflation_NZ'] #Change from 05 to 1Y
RegCode['EPI Inflation L'] = PXLast5Yzs['EpiM_Inflation_NZ']
RegCode['Wage Inflation M'] = PXLast1Yzs['NZLCPY Index']
RegCode['Wage Inflation L'] = PXLast7Yzs['NZLCPY Index']
RegCode['Core (Persistence/Breadth) M'] = (PXLast1Yzs['NZICSFY Index'] + PXLast1Yzs['NZCPCPIY Index']) / 2 #Change from 05 to 1Y
RegCode['Core (Persistence/Breadth) L'] = (PXLast7Yzs['NZICSFY Index'] + PXLast7Yzs['NZCPCPIY Index']) / 2
RegCode['OECD Output Gap M'] = PXLast5Yzs['OEEONZUG Index']
RegCode['OECD Output Gap L'] = PXLast10Yzs['OEEONZUG Index']
RegCode['Employment EPI M'] = PXLast5Yzs['EpiM_Employment_NZ']
RegCode['Employment EPI L'] = PXLast10Yzs['EpiM_Employment_NZ']
RegCode['Unemployment level vs NAIRU M'] = PXLast5Yzs['NZLFUNER Index']
RegCode['Unemployment level vs NAIRU L'] = PXLast10Yzs['NZLFUNER Index']
RegCode['Capacity Utilisation M'] = PXLast5Yzs['NZBCCU Index']
RegCode['Capacity Utilisation L'] = PXLast10Yzs['NZBCCU Index']

# In[14]:


#### Financial Conditions ###
# Capacity to borrow
RegCode['Property (1y momentum) M'] = PXLast1Yzs['QVNZTOTL Index']
RegCode['Property (level) L'] = PXLast7Yzs['QVNZTOTL Index']
RegCode['Equities M'] = PXLast05Yzs['NZSE50FG Index PE']
RegCode['Equities L'] = PXLast7Yzs['NZSE50FG Index PE']
RegCode['Household Leverage M'] = PXLast5Yzs['CPNFNZHG Index']
RegCode['Household Leverage L'] = PXLast10Yzs['CPNFNZHG Index']
RegCode['Corporate Leverage M'] = PXLast5Yzs['CPNFNZNG Index']
RegCode['Corporate Leverage L'] = PXLast10Yzs['CPNFNZNG Index']
RegCode['Government Leverage M'] = PXLast5Yzs['CPNFNZOG Index']
RegCode['Government Leverage L'] = PXLast10Yzs['CPNFNZOG Index']

# Liquidity
RegCode['Public Sector Liquidity ==> Reserves, Central Bank BS M'] = PXLast05Yzs['FARBCRED Index']
RegCode['Public Sector Liquidity ==> Reserves, Central Bank BS L'] = PXLast3Yzs['FARBCRED Index']
RegCode[
    'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M'] = \
PXLast1Yzs['NZMSNBM Index']
RegCode[
    'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L'] = \
PXLast7Yzs['NZMSNBM Index']
RegCode['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M'] = PXLast5Yzs['USCABAL Index']
RegCode['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L'] = PXLast10Yzs['USCABAL Index']

# Funding Costs
RegCode['Real Policy Rate vs real neutral rate M'] = PXLast05Yzs['NZOCRS Index']
RegCode['Real Policy Rate vs real neutral rate L'] = PXLast5Yzs['NZOCRS Index']
RegCode['Real Long End Yields M'] = PXLast05Yzs['GNZGB10 Index']
RegCode['Real Long End Yield L'] = PXLast5Yzs['GNZGB10 Index']
RegCode['Real Ccy vs ToT M'] = PXLast1Yzs['196.028 Index']
RegCode['Real Ccy vs ToT L'] = PXLast7Yzs['196.028 Index']
RegCode['Financial Sector Health M'] = (PXLast1Yzs['CBA AU Equity'] + PXLast1Yzs['WBC NZ Equity']
                                        + PXLast1Yzs['BZL NZ Equity'] + PXLast1Yzs['ANZ NZ Equity']) / 4
RegCode['Financial Sector Health L'] = (PXLast7Yzs['CBA AU Equity'] + PXLast7Yzs['WBC NZ Equity']
                                        + PXLast7Yzs['BZL NZ Equity'] + PXLast7Yzs['ANZ NZ Equity']) / 4
RegCode['Credit Spreads M'] = PXLast05Yzs['BNZCR0 Index']
RegCode['Credit Spreads L'] = PXLast3Yzs['BNZCR0 Index']

# In[15]:


# Frame conditioning logic

ConditionFrame = pd.DataFrame(RegCode.copy())
for i in (ConditionFrame.columns):
    for j in (ConditionFrame.index):
        if (ConditionFrame.loc[j, i] <= -1):
            ConditionFrame.loc[j, i] = 0
        elif (ConditionFrame.loc[j, i] >= 1):
            ConditionFrame.loc[j, i] = 2
        else:
            ConditionFrame.loc[j, i] = 1

ConditionFrame.tail()

# In[16]:


# Create Growth Momentum Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
GrowthMPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

GrowthMCols = ['EPI ex inflation M', 'EPI Synchronised Sectors or Regions M', 'Earnings Growth M',
               'Investment/Capex M', 'Credit Growth M', 'Inventory Cycle M', 'Global Trade M', 'Fiscal Impulse M']

for i in GrowthMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            GrowthMPerc.loc[j, 'Reflation'] += 1
            
            '''=> new logic midcycle momentum is no longer countered'''            
            #GrowthMPerc.loc[j, 'Mid Cycle'] += 1
            
        elif ConditionFrame.loc[j, i] == 1:
            GrowthMPerc.loc[j, 'Late Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            GrowthMPerc.loc[j, 'Downturn'] += 1
            GrowthMPerc.loc[j, 'Recession'] += 1
            
            

# Growth Momentum Frame Percentages
GrowthMPerc = GrowthMPerc / len(GrowthMCols)

'''=> new logic midcycle momentum is no longer countered'''
GrowthMPerc['Mid Cycle'] = np.nan

GrowthMPerc.tail()

# In[28]:


# Create Growth Level Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
GrowthLPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

GrowthLCols = ['EPI ex inflation L', 'EPI Synchronised Sectors or Regions L', 'Earnings Growth L',
               'Investment/Capex L', 'Credit Growth L', 'Inventory Cycle L', 'Global Trade L', 'Fiscal Impulse L']

for i in GrowthLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            
            '''=> new logic growth level is no longer countered for downturn'''
            GrowthLPerc.loc[j, 'Downturn'] += 1
            
            GrowthLPerc.loc[j, 'Late Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 1:
            GrowthLPerc.loc[j, 'Mid Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            GrowthLPerc.loc[j, 'Recession'] += 1
            GrowthLPerc.loc[j, 'Reflation'] += 1

# Growth Momentum Frame Percentages
GrowthLPerc = GrowthLPerc / len(GrowthLCols)

'''=> new logic growth level is no longer countered for downturn'''
#GrowthLPerc['Downturn'] = np.nan

GrowthLPerc.tail()

# In[29]:


# Create Inflation Momentum Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
InflationMPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

InflationMCols = ['EPI Inflation M', 'Wage Inflation M', 'Core (Persistence/Breadth) M', 'OECD Output Gap M',
                  'Employment EPI M', 'Unemployment level vs NAIRU M', 'Capacity Utilisation M']

for i in InflationMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            InflationMPerc.loc[j, 'Mid Cycle'] += 1
            InflationMPerc.loc[j, 'Late Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 1:
            InflationMPerc.loc[j, 'Downturn'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            InflationMPerc.loc[j, 'Recession'] += 1
            '''=> new logic inflation momentum is countered for reflation'''
            InflationMPerc.loc[j, 'Reflation'] += 1

# Growth Momentum Frame Percentages
InflationMPerc = InflationMPerc / len(InflationMCols)

InflationMPerc.tail()

# In[30]:


# Create Inflation Level Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
InflationLPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

InflationLCols = ['EPI Inflation L', 'Wage Inflation L', 'Core (Persistence/Breadth) L', 'OECD Output Gap L',
                  'Employment EPI L', 'Unemployment level vs NAIRU L', 'Capacity Utilisation L']

for i in InflationLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            InflationLPerc.loc[j, 'Late Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 1:
            InflationLPerc.loc[j, 'Mid Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            InflationLPerc.loc[j, 'Recession'] += 1
            InflationLPerc.loc[j, 'Reflation'] += 1

# Growth Momentum Frame Percentages
InflationLPerc = InflationLPerc / len(InflationLCols)

'''=> existing logic inflation level is not countered for downturn - just make it clear by setting it to nan'''
InflationLPerc['Downturn'] = np.nan

InflationLPerc.tail()

# In[31]:


# Create Financial Conditions Momentum Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
FinCondMPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

FinCondMCols = ['Property (1y momentum) M', 'Equities M', 'Household Leverage M', 'Corporate Leverage M',
                'Government Leverage M', 'Public Sector Liquidity ==> Reserves, Central Bank BS M',
                'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M',
                'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M',
                'Real Policy Rate vs real neutral rate M', 'Real Long End Yields M', 'Real Ccy vs ToT M',
                'Financial Sector Health M', 'Credit Spreads M']

for i in FinCondMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            FinCondMPerc.loc[j, 'Reflation'] += 1
        elif ConditionFrame.loc[j, i] == 1:
            FinCondMPerc.loc[j, 'Mid Cycle'] += 1
            FinCondMPerc.loc[j, 'Downturn'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            FinCondMPerc.loc[j, 'Late Cycle'] += 1

# Growth Momentum Frame Percentages
FinCondMPerc = FinCondMPerc / len(FinCondMCols)


'''=> existing logic FC momentum is not countered for recession - just make it clear by setting it to nan'''
FinCondMPerc['Recession'] = np.nan

FinCondMPerc.tail()

# In[32]:


# Create Financial Conditions Level Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
FinCondLPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

FinCondLCols = ['Property (level) L', 'Equities L', 'Household Leverage L', 'Corporate Leverage L',
                'Government Leverage L', 'Public Sector Liquidity ==> Reserves, Central Bank BS L',
                'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L',
                'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L',
                'Real Policy Rate vs real neutral rate L', 'Real Long End Yields M', 'Real Ccy vs ToT L',
                'Financial Sector Health L', 'Credit Spreads L']

for i in FinCondLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            FinCondLPerc.loc[j, 'Reflation'] += 1
            FinCondLPerc.loc[j, 'Mid Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 1:
            FinCondLPerc.loc[j, 'Late Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            FinCondLPerc.loc[j, 'Recession'] += 1

# Growth Momentum Frame Percentages
FinCondLPerc = FinCondLPerc / len(FinCondLCols)


'''=> existing logic FC level is not countered for downturn - just make it clear by setting it to nan'''
FinCondLPerc['Downturn'] = np.nan



FinCondLPerc.head()

# In[33]:


# Generate overall probabilities
# Loop makes 0s into 0.0001s to avoid totally nullifying regime outcomes


'''=> New logic to calc joint prob

    mid cycle GrowthMPerc is not relevent 
    downturn GrowthLPerc is not relevent
    therefore for mid cycle and downtun there is no joint prod prob for growth. hence set them to 1 to fake joint product

    downturn InflationLPerc is not relevent
    therefore for downtun there is no joint prod prob for inflation. hence set them to 1 to fake joint product

    recession FinCondMPerc is not relevent
    downturn FinCondLPerc is not relevent
    therefore for downtun and recession there is no joint prod prob for financial condition. hence set them to 1 to fake joint product

'''

GrowthProb = GrowthMPerc.fillna(1) * GrowthLPerc.fillna(1)
InflationProb = InflationMPerc.fillna(1) * InflationLPerc.fillna(1)
FinCondProb = FinCondMPerc.fillna(1) * FinCondLPerc.fillna(1)
TotalProb = GrowthProb * InflationProb * FinCondProb


''' The following codes are replaced by the 4 lines code above
 
GrowthProb = GrowthMPerc * GrowthLPerc

for i in GrowthProb.columns:
    for j in GrowthProb.index:
        if GrowthProb.loc[j, i] < 0.01:
            GrowthProb.loc[j, i] = 0.0001
InflationProb = InflationMPerc * InflationLPerc
for i in InflationProb.columns:
    for j in InflationProb.index:
        if InflationProb.loc[j, i] < 0.01:
            InflationProb.loc[j, i] = 0.0001
FinCondProb = FinCondMPerc * FinCondLPerc
for i in FinCondProb.columns:
    for j in FinCondProb.index:
        if FinCondProb.loc[j, i] < 0.01:
            FinCondProb.loc[j, i] = 0.0001

# Adjust for blank regimes
InflationProb['Downturn'] = InflationMPerc['Downturn'] * InflationMPerc['Downturn']
InflationProb['Reflation'] = InflationLPerc['Reflation'] * InflationLPerc['Reflation']
FinCondProb['Downturn'] = FinCondMPerc['Downturn'] * FinCondMPerc['Downturn']
FinCondProb['Recession'] = FinCondLPerc['Recession'] * FinCondLPerc['Recession']



GrowthProb.tail()

# In[34]:


InflationProb.tail()

# In[35]:


FinCondProb.tail()

# In[36]:


# Calculate the total probabilities

# TotalProb = GrowthProb*((InflationProb+FinCondProb)/2) #higher dominance to Growth
TotalProb = GrowthProb * InflationProb * FinCondProb
'''



# Calc in 100%
GrowthProb[RegCols] = GrowthProb[RegCols].div(GrowthProb[RegCols].sum(axis=1), axis=0).multiply(100)
InflationProb[RegCols] = InflationProb[RegCols].div(InflationProb[RegCols].sum(axis=1), axis=0).multiply(100)
FinCondProb[RegCols] = FinCondProb[RegCols].div(FinCondProb[RegCols].sum(axis=1), axis=0).multiply(100)
TotalProb[RegCols] = TotalProb[RegCols].div(TotalProb[RegCols].sum(axis=1), axis=0).multiply(100)




'''=> New logic a second version of joint prob
    This time sum all the lvl/momentum from growth/inflation/fc and avg them by 6.
    for those component that is not relevent we treat them as 0 which will effectively
    downscore the regime prob for those having non relevent component. This will compensate the 
    original total product prob because they overweight the significance of regime with non relevent compoennet

    the only exception is downturn which will be overridden by oroginal TotalProb downturn 
'''

TotalProbV2 = (GrowthMPerc.fillna(0) + GrowthLPerc.fillna(0) + 
              InflationMPerc.fillna(0) + InflationLPerc.fillna(0) + 
              FinCondMPerc.fillna(0) * FinCondLPerc.fillna(0)) / 6 

TotalProbV2['Downturn'] = TotalProb['Downturn']/100 
#normalising
TotalProbV2 = TotalProbV2.divide(TotalProbV2.sum(axis=1), axis=0) * 100


TotalProbAvg = (TotalProb.fillna(0) + TotalProbV2.fillna(0)) / (1 - TotalProbV2.isna()) + (1 - TotalProb.isna())


GrowthProb.to_excel(
    r'./output/NZDRegGrowthProbability.xlsx')
InflationProb.to_excel(
    r'./output/NZDRegInflationProbability.xlsx')
FinCondProb.to_excel(
    r'./output/NZDRegFinancialCondProbability.xlsx')

TotalProb.ffill().to_excel(
    r'./output/NZDRegTotalProbabilityV1.xlsx')

TotalProbV2.ffill().to_excel(
    r'./output/NZDRegTotalProbabilityV2.xlsx')

TotalProbAvg.ffill().to_excel(
    r'./output/NZDRegTotalProbabilityAvg.xlsx')

RegCode.transpose().to_excel(
    r'./output/NZDRegimeFullScoring.xlsx')



