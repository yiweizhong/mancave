#!/usr/bin/env python
# coding: utf-8

# In[3]:


### Imports

import subprocess

# In[4]:


### Run Macro Regimes

subprocess.call(['python.exe',
                 r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\AustraliaQuantReg\MacroRegimeAus_DB.py'])
subprocess.call(['python.exe',
                 r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\EUQuantReg\MacroRegimeEU_DB.py'])
subprocess.call(['python.exe',
                 r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\GlbQuantReg\MacroRegimeGlb_DB.py'])
subprocess.call(['python.exe',
                 r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\JPQuantReg\MacroRegimeJP_DB.py'])
subprocess.call(['python.exe',
                 r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\NZQuantReg\MacroRegimeNZ_DB.py'])
subprocess.call(['python.exe',
                 r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\USQuantReg\MacroRegimeUS_DB.py'])
subprocess.call(['python.exe',
                 r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\UKQuantReg\MacroRegimeUK_DB.py'])
subprocess.call(['python.exe',
                 r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\CNQuantReg\MacroRegimeCN_DB.py'])

# In[ ]:




