#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Imports
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import datetime as dt
from pandas.tseries.offsets import *
import numpy as np
import scipy.stats as scs

import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.dates import YearLocator, MonthLocator



# In[2]:


# Getting BBG data and cleaning
StartDate = '1995-12-30'
EndDate = (dt.datetime.today()+BDay(30)).strftime("%m/%d/%Y")

Dates = pd.DataFrame(pd.date_range(StartDate,EndDate,freq="D"),columns=['Date'])
Dates = Dates.set_index('Date',drop=True)
Dates.index.name = None

Month = pd.DataFrame(pd.date_range(StartDate,EndDate,freq="M"),columns=['Date'])
Month = Month.set_index('Date',drop=True)
Month.index.name = None

Quarter = pd.DataFrame(pd.date_range(StartDate,EndDate,freq="Q"),columns=['Date'])
Quarter = Quarter.set_index('Date',drop=True)
Quarter.index.name = None


# In[5]:


# Take EPI data from excel

# Read EPI
CNYEPIEx = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegEPI.xlsx').parse('MacroRegEPI')
CNYEPIEx.index = CNYEPIEx['Date']
CNYEPIEx = CNYEPIEx.drop(columns = ['Date'])
CNYEPIEx = CNYEPIEx.fillna(method='ffill')
CNYEPIEx = Dates.merge(CNYEPIEx,right_index=True,left_index=True,how='left')
CNYEPIEx = CNYEPIEx.fillna(method='ffill')
CNYEPIEx = Month.merge(CNYEPIEx,right_index=True,left_index=True,how='left')


# In[6]:


CNYEPIEx.tail()


# In[7]:


# Take Bloomberg data from excel

# Read Bloomberg Data
Ecodata = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegBBGUpdate.xlsx').parse('Month')
Ecodata = Ecodata.iloc[2:]
Ecodata.index = Ecodata['NAME']
Ecodata = Ecodata.drop(columns = ['NAME'])
Ecodata = Ecodata.fillna(method='ffill')
Ecodata = Dates.merge(Ecodata,right_index=True,left_index=True,how='left')
Ecodata = Ecodata.fillna(method='ffill')
Ecodata = Month.merge(Ecodata,right_index=True,left_index=True,how='left')


# Read in half-yearly bak NIMs
BankData1 = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegBBGUpdate.xlsx').parse('Quarter')
BankData1 = BankData1.iloc[1:]
BankData1.index = BankData1['Unnamed: 0']
BankData1 = BankData1.drop(columns = ['Unnamed: 0'])
BankData1 = BankData1.fillna(method='ffill')
BankData1 = Dates.merge(BankData1,right_index=True,left_index=True,how='left')
BankData1 = BankData1.fillna(method='ffill')
BankData1 = Month.merge(BankData1,right_index=True,left_index=True,how='left')
Ecodata = Ecodata.merge(BankData1,right_index=True,left_index=True,how='left')

#Read in quarterly Bank NIMs
BankData2 = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegBBGUpdate.xlsx').parse('BiAnn')
BankData2 = BankData2.iloc[1:]
BankData2.index = BankData2['Unnamed: 0']
BankData2 = BankData2.drop(columns = ['Unnamed: 0'])
BankData2 = BankData2.fillna(method='ffill')
BankData2 = Dates.merge(BankData2,right_index=True,left_index=True,how='left')
BankData2 = BankData2.fillna(method='ffill')
BankData2 = Month.merge(BankData2,right_index=True,left_index=True,how='left')
Ecodata = Ecodata.merge(BankData2,right_index=True,left_index=True,how='left')


# In[19]:


# Put the codes in 

NamesIdx = ['EPI ex inflation M','EPI ex inflation L','EPI Synchronised Sectors or Regions M','EPI Synchronised Sectors or Regions L',
'Earnings Growth M','Earnings Growth L','Investment/Capex M','Investment/Capex L','Credit Growth M',
'Credit Growth L','Inventory Cycle M',
'Inventory Cycle L','Global Trade M','Global Trade L','Fiscal Impulse M','Fiscal Impulse L',
'EPI Inflation M','EPI Inflation L','Wage Inflation M','Wage Inflation L','Core (Persistence/Breadth) M',
'Core (Persistence/Breadth) L','OECD Output Gap M','OECD Output Gap L','Employment EPI M',
'Employment EPI L','Unemployment level vs NAIRU M','Unemployment level vs NAIRU L',
'Capacity Utilisation M','Capacity Utilisation L','Property (1y momentum) M','Property (level) L',
'Equities M','Equities L','Household Leverage M','Household Leverage L','Corporate Leverage M',
'Corporate Leverage L','Government Leverage M','Government Leverage L',
'Public Sector Liquidity ==> Reserves, Central Bank BS M','Public Sector Liquidity ==> Reserves, Central Bank BS L',
'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M',
'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L',
'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M',
'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L',
'Real Policy Rate vs real neutral rate M','Real Policy Rate vs real neutral rate L','Real Long End Yields M',
'Real Long End Yield L','Real Ccy vs ToT M','Real Ccy vs ToT L','Financial Sector Health M',
'Financial Sector Health L','Credit Spreads M','Credit Spreads L']

CodesIdx = ['SHSZ300 Index BEPS','CNFAYOY Index','CNGDNBEY Index','CNGDN0LY Index','CNMS1YOY Index','CPMIIFGS Index',
'CZININVY Index','CNFRBAL$ Index','CNFREXPY Index','CNFRIMPY Index','CHGRTTL Index','CHGETTL Index','CNNGPQ$ Index',
'OEEOCNBR Index','CPMIEMPL Index','CNCPNFY Index','CNCPCRY Index','CNUERATE Index','CNBVCU Index',
'CHHEAVG Index','SHSZ300 Index PE','CPNFCNHG Index','CPNFCNNG Index','CPNFCNOG Index','FARBCRED Index',
'EBBSTOTA Index','EURUSD Curncy','BJACTOTL Index','JPYUSD Curncy','CNBMTTAS Index','CNYUSD Curncy',
'CNMSM0 Index','CNMSM1 Index','CNMSM2 Index','USCABAL Index','ECOCEAS Index','ECOCJPN Index','ECOCCNN Index',
'GCNY1YR Index','CNCPIYOY Index','USNREUAR Index','USNRUS Index','GCNY10YR Index','924.028 Index',
'CTOTCNY Index','3988 HK Equity','1398 HK Equity','1288 HK Equity','939 HK Equity',]

#Recopy CodesIdx and CodesNames Idx as added things for Japan along the way


# In[15]:


#### Global Output Gap Code

CountryNames = ['AUSTRALIA','NEW ZEALAND','JAPAN','US','CANADA','IRELAND','UK','GERMANY','FRANCE','SWITZERLAND','SPAIN',
'NETHERLANDS','NORWAY','SWEDEN','DENMARK','ITALY','BELGIUM','GREECE','TURKEY','POLAND','CZECHIA','HUNGARY','CHILE',
'MEXICO',]

OutputGapNames = ['OEEOAUUG Index','OEEONZUG Index','OEEOJPUG Index','OEEOUSUG Index','OEEOCAUG Index','OEEOIEUG Index',
'OEEOGBUG Index','OEEODEUG Index','OEEOFRUG Index','OEEOCHUG Index','OEEOESUG Index','OEEONLUG Index','OEEONOUG Index',
'OEEOSEUG Index','OEEODKUG Index','OEEOITUG Index','OEEOBEUG Index','OEEOGRUG Index','OEEOTRUG Index','OEEOPLUG Index',
'OEEOCZUG Index','OEEOHUUG Index','OEEOCLUG Index','OEEOMXUG Index',]

IMFDOTSExports = ['193D0001 Index','196D0001 Index','158D0001 Index','111D0001 Index','156D0001 Index',
'178D0001 Index','112D0001 Index','134D0001 Index','132D0001 Index','146D0001 Index','184D0001 Index','138D0001 Index',
'142D0001 Index','144D0001 Index','128D0001 Index','136D0001 Index','124D0001 Index','174D0001 Index','186D0001 Index',
'964D0001 Index','935D0001 Index','944D0001 Index','228D0001 Index','273D0001 Index',]

IMFDOTImports =['193D1001 Index','196D1001 Index','158D1001 Index','111D1001 Index','156D1001 Index',
'178D1001 Index','112D1001 Index','134D1001 Index','132D1001 Index','146D1001 Index','184D1001 Index','138D1001 Index',
'142D1001 Index','144D1001 Index','128D1001 Index','136D1001 Index','124D1001 Index','174D1001 Index','186D1001 Index',
'964D1001 Index','935D1001 Index','944D1001 Index','228D1001 Index','273D1001 Index',]


# In[16]:


# Output Gap Generation

# Copies of the data 

OutputGap = pd.DataFrame(0,index=Month.index, columns=['Blank'])
for i in OutputGapNames:
    OutputGap = OutputGap.merge(pd.DataFrame(Ecodata[[i]]),right_index=True,left_index=True,how='left')
OutputGap = OutputGap.drop(columns=['Blank'])
OutputGap.columns = CountryNames

DotsExports = pd.DataFrame(0,index=Month.index, columns=['Blank'])

for i in IMFDOTSExports:
    DotsExports = DotsExports.merge(Ecodata[[i]],right_index=True,left_index=True,how='left')
DotsExports = DotsExports.drop(columns=['Blank'])
DotsExports.columns = CountryNames

DotsImports = pd.DataFrame(0,index=Month.index, columns=['Blank'])

for i in IMFDOTImports:
    DotsImports = DotsImports.merge(Ecodata[[i]],right_index=True,left_index=True,how='left')
DotsImports = DotsImports.drop(columns=['Blank'])
DotsImports.columns = CountryNames

#Create Trade Weights

DotsTrade = DotsExports + DotsImports
DotsTradeSUM = DotsTrade.sum(axis=1)

DotsTradeSum = pd.DataFrame(Month.index,columns=['index'])
DotsTradeSum = DotsTradeSum.set_index(['index'],drop=True)
DotsTradeSum.index.name = None

for i in range(len(OutputGap.columns)):
    TotalSumCopy = pd.DataFrame(DotsTradeSUM.copy())
    DotsTradeSum = DotsTradeSum.merge(TotalSumCopy,right_index=True,left_index=True,how='left')
    
DotsTradeSum.columns = CountryNames    
DotsWeight = (DotsTrade/DotsTradeSum)

WeightedOuputG = OutputGap*DotsWeight
GlobalOutputGap = pd.DataFrame(WeightedOuputG.sum(axis=1))

# Convert to Quarterly
#GlobalOutputGap = Quarter.merge(GlobalOutputGap,right_index=True,left_index=True,how='left')


# In[20]:


#Copies and Adjustments

#Main Code df
RegCode = pd.DataFrame(0,index=Month.index, columns=NamesIdx)
PXLast = pd.DataFrame(0,index=Month.index, columns=['Blank'])
for i in CodesIdx:
    PXLast = PXLast.merge(pd.DataFrame(Ecodata[[i]]),right_index=True,left_index=True,how='left')
PXLast = PXLast.drop(columns=['Blank'])

#Add in EPI
PXLast['EpiM_Headline_CH'] = CNYEPIEx['EpiM_Headline_CH'].copy()
PXLast['EpiM_Business_CH'] = CNYEPIEx['EpiM_Business_CH'].copy()
PXLast['EpiM_Consumer_CH'] = CNYEPIEx['EpiM_Consumer_CH'].copy()
PXLast['EpiM_Employment_CH'] = CNYEPIEx['EpiM_Employment_CH'].copy()
PXLast['EpiM_Growth_CH'] = CNYEPIEx['EpiM_Growth_CH'].copy()
PXLast['EpiM_Inflation_CH'] = CNYEPIEx['EpiM_Inflation_CH'].copy()



# In[22]:


# Start adjusting data series

#YOY%
PXLast['OEEOCNBR Index'] = (PXLast['OEEOCNBR Index']/PXLast['OEEOCNBR Index'].shift(12) -1)



# Unemployment vs NAIRU estimate of 4Y average
PXLast['CNUERATE Index'] = (PXLast['CNUERATE Index'] - PXLast['CNUERATE Index'].rolling(4*12).mean())


# Inverse for unemployment, fiscal and debt, credit spreads
PXLast['CNUERATE Index'] = -1*PXLast['CNUERATE Index']
PXLast['CPMIEMPL Index'] = -1*PXLast['CPMIEMPL Index']
PXLast['CPMIIFGS Index'] = -1*PXLast['CPMIIFGS Index']
PXLast['CPNFCNHG Index'] = -1*PXLast['CPNFCNHG Index']
PXLast['CPNFCNNG Index'] = -1*PXLast['CPNFCNNG Index']
PXLast['CPNFCNOG Index'] = -1*PXLast['CPNFCNOG Index']

# Quarterly Sum for inventories, 12M sum for trade balance
PXLast['CNFRBAL$ Index'] = PXLast['CNFRBAL$ Index'].rolling(12).sum()

# Central Bank Balance sheet, convert all CB B/S to USD and Sum
PXLast['FARBCRED Index'] = ((PXLast['FARBCRED Index']/1000)+(PXLast['EBBSTOTA Index']*PXLast['EURUSD Curncy'])
                            +(PXLast['BJACTOTL Index']*PXLast['JPYUSD Curncy'])
                            +(PXLast['CNBMTTAS Index']*PXLast['CNYUSD Curncy']))                            
                            
# Current Account Balance G4 Sums
PXLast['USCABAL Index'] = PXLast['USCABAL Index']+PXLast['ECOCEAS Index']+PXLast['ECOCJPN Index']+PXLast['ECOCCNN Index']

# Real Rate Calcs policy and long end, and vs neutral rate for policy
PXLast['GCNY10YR Index'] = (PXLast['CNCPIYOY Index']
                           - PXLast['GCNY10YR Index'])#Real long end
PXLast['GCNY1YR Index'] = -1*((PXLast['GCNY1YR Index'] - PXLast['CNCPIYOY Index'])
                          -(0.5*PXLast['USNRUS Index']
                          +0.5*PXLast['GCNY10YR Index'].rolling(12).mean())) #Real policy rate

# China Fiscal Adjustment
PXLast['CHGRTTL Index'] = ((PXLast['CHGRTTL Index'].rolling(12).sum() - PXLast['CHGETTL Index'].rolling(12).sum())
                           /(PXLast['CHGRTTL Index']*4))

# Real currency v Terms of trade
PXLast['924.028 Index'] = PXLast['CTOTCNY Index']/PXLast['924.028 Index']

# Add Output Gap
PXLast['Output Gap'] = GlobalOutputGap


# In[23]:


# Z-scoring at different time frames

# 0.5Y
PXLast05Yzs = (PXLast - PXLast.rolling(6).mean())/PXLast.rolling(6).std()

#1y
PXLast1Yzs = (PXLast - PXLast.rolling(12).mean())/PXLast.rolling(12).std()

#3Y
PXLast3Yzs = (PXLast - PXLast.rolling(3*12).mean())/PXLast.rolling(3*12).std()

#5y
PXLast5Yzs = (PXLast - PXLast.rolling(5*12).mean())/PXLast.rolling(5*12).std()

#7Y
PXLast7Yzs = (PXLast - PXLast.rolling(7*12).mean())/PXLast.rolling(7*12).std()

#10Y
PXLast10Yzs = (PXLast - PXLast.rolling(10*12).mean())/PXLast.rolling(10*12).std()


# In[24]:


# Add in Adjusted Series
#Growth
RegCode['EPI ex inflation M'] = PXLast05Yzs['EpiM_Headline_CH']
RegCode['EPI ex inflation L'] = PXLast5Yzs['EpiM_Headline_CH']
RegCode['EPI Synchronised Sectors or Regions M'] = (PXLast05Yzs['EpiM_Business_CH']+PXLast05Yzs['EpiM_Consumer_CH']+
                                                    PXLast05Yzs['EpiM_Employment_CH']+PXLast05Yzs['EpiM_Growth_CH']+
                                                    PXLast05Yzs['EpiM_Inflation_CH'])/5
RegCode['EPI Synchronised Sectors or Regions L'] = (PXLast5Yzs['EpiM_Business_CH']+PXLast5Yzs['EpiM_Consumer_CH']+
                                                    PXLast5Yzs['EpiM_Employment_CH']+PXLast5Yzs['EpiM_Growth_CH']+
                                                    PXLast5Yzs['EpiM_Inflation_CH'])/5
RegCode['Earnings Growth M'] = PXLast05Yzs['SHSZ300 Index BEPS']
RegCode['Earnings Growth L'] = PXLast7Yzs['SHSZ300 Index BEPS']
RegCode['Investment/Capex M'] = (PXLast5Yzs['CNFAYOY Index']+PXLast5Yzs['CNGDNBEY Index']+
                                                    PXLast5Yzs['CNGDN0LY Index'])/3
RegCode['Investment/Capex L'] = (PXLast10Yzs['CNFAYOY Index']+PXLast10Yzs['CNGDNBEY Index']+
                                                    PXLast10Yzs['CNGDN0LY Index'])/3
RegCode['Credit Growth M'] = PXLast1Yzs['CNMS1YOY Index']
RegCode['Credit Growth L'] = PXLast7Yzs['CNMS1YOY Index']
RegCode['Inventory Cycle M'] = (PXLast1Yzs['CPMIIFGS Index']+PXLast1Yzs['CZININVY Index'])/2
RegCode['Inventory Cycle L'] = (PXLast7Yzs['CPMIIFGS Index']+PXLast7Yzs['CZININVY Index'])/2
RegCode['Global Trade M'] = (PXLast1Yzs['CNFRBAL$ Index']+PXLast1Yzs['CNFREXPY Index']+PXLast1Yzs['CNFRIMPY Index'])/3
RegCode['Global Trade L'] = (PXLast7Yzs['CNFRBAL$ Index']+PXLast7Yzs['CNFREXPY Index']+PXLast7Yzs['CNFRIMPY Index'])/3
RegCode['Fiscal Impulse M'] = PXLast5Yzs['CHGRTTL Index']
RegCode['Fiscal Impulse L'] = PXLast10Yzs['CHGRTTL Index']




# In[26]:


#Output Gap inflation
RegCode['EPI Inflation M'] = PXLast05Yzs['EpiM_Inflation_CH']
RegCode['EPI Inflation L'] = PXLast5Yzs['EpiM_Inflation_CH']
RegCode['Wage Inflation M'] = (PXLast05Yzs['OEEOCNBR Index']+PXLast05Yzs['CPMIEMPL Index'])/2
RegCode['Wage Inflation L'] = (PXLast7Yzs['OEEOCNBR Index']+PXLast7Yzs['CPMIEMPL Index'])/2
RegCode['Core (Persistence/Breadth) M'] = (PXLast05Yzs['CNCPNFY Index']+PXLast05Yzs['CNCPCRY Index'])/2
RegCode['Core (Persistence/Breadth) L'] = (PXLast7Yzs['CNCPNFY Index']+PXLast7Yzs['CNCPCRY Index'])/2
RegCode['OECD Output Gap M'] = PXLast5Yzs['Output Gap']
RegCode['OECD Output Gap L'] = PXLast10Yzs['Output Gap']
RegCode['Employment EPI M'] = PXLast5Yzs['EpiM_Employment_CH']
RegCode['Employment EPI L'] = PXLast10Yzs['EpiM_Employment_CH']
RegCode['Unemployment level vs NAIRU M'] = (PXLast5Yzs['CNUERATE Index']+PXLast3Yzs['CPMIEMPL Index'])/2
RegCode['Unemployment level vs NAIRU L'] = (PXLast10Yzs['CNUERATE Index']+PXLast5Yzs['CPMIEMPL Index'])/2
RegCode['Capacity Utilisation M'] = PXLast5Yzs['CNBVCU Index']
RegCode['Capacity Utilisation L'] = PXLast10Yzs['CNBVCU Index']



# In[25]:


#### Financial Conditions ###
# Capacity to borrow
RegCode['Property (1y momentum) M'] = PXLast1Yzs['CHHEAVG Index']
RegCode['Property (level) L'] = PXLast7Yzs['CHHEAVG Index']
RegCode['Equities M'] = PXLast05Yzs['SHSZ300 Index PE']
RegCode['Equities L'] = PXLast7Yzs['SHSZ300 Index PE']
RegCode['Household Leverage M'] = PXLast5Yzs['CPNFCNHG Index']
RegCode['Household Leverage L'] = PXLast10Yzs['CPNFCNHG Index']
RegCode['Corporate Leverage M'] = PXLast5Yzs['CPNFCNNG Index']
RegCode['Corporate Leverage L'] = PXLast10Yzs['CPNFCNNG Index']
RegCode['Government Leverage M'] = PXLast5Yzs['CPNFCNOG Index']
RegCode['Government Leverage L'] = PXLast10Yzs['CPNFCNOG Index']

# Liquidity
RegCode['Public Sector Liquidity ==> Reserves, Central Bank BS M'] = PXLast05Yzs['FARBCRED Index']
RegCode['Public Sector Liquidity ==> Reserves, Central Bank BS L'] = PXLast3Yzs['FARBCRED Index']
RegCode['Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M'] = (PXLast1Yzs['CNMSM0 Index']+PXLast1Yzs['CNMSM1 Index']+PXLast1Yzs['CNMSM2 Index'])/3
RegCode['Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L'] = (PXLast7Yzs['CNMSM0 Index']+PXLast7Yzs['CNMSM1 Index']+PXLast7Yzs['CNMSM2 Index'])/3
RegCode['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M'] = PXLast5Yzs['USCABAL Index']
RegCode['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L'] = PXLast10Yzs['USCABAL Index']

# Funding Costs
RegCode['Real Policy Rate vs real neutral rate M'] = PXLast05Yzs['GCNY1YR Index']
RegCode['Real Policy Rate vs real neutral rate L'] = PXLast5Yzs['GCNY1YR Index']
RegCode['Real Long End Yields M'] = PXLast05Yzs['GCNY10YR Index']
RegCode['Real Long End Yield L'] = PXLast5Yzs['GCNY10YR Index']
RegCode['Real Ccy vs ToT M'] = PXLast1Yzs['924.028 Index']
RegCode['Real Ccy vs ToT L'] = PXLast7Yzs['924.028 Index']
RegCode['Financial Sector Health M'] = (PXLast1Yzs['3988 HK Equity']+PXLast1Yzs['1398 HK Equity']+PXLast1Yzs['1288 HK Equity']+PXLast1Yzs['939 HK Equity'])/4
RegCode['Financial Sector Health L'] = (PXLast7Yzs['3988 HK Equity']+PXLast7Yzs['1398 HK Equity']+PXLast7Yzs['1288 HK Equity']+PXLast7Yzs['939 HK Equity'])/4
RegCode['Credit Spreads M'] = PXLast05Yzs['GCNY10YR Index']
RegCode['Credit Spreads L'] = PXLast3Yzs['GCNY10YR Index']


# In[27]:


# Frame conditioning logic

ConditionFrame = pd.DataFrame(RegCode.copy())
for i in (ConditionFrame.columns):
    for j in (ConditionFrame.index):
        if (ConditionFrame.loc[j,i] < -1):
            ConditionFrame.loc[j,i] = 0
        elif (ConditionFrame.loc[j,i] > 1):
            ConditionFrame.loc[j,i] = 2
        else:
            ConditionFrame.loc[j,i] = 1
            

ConditionFrame.tail()


# In[28]:


# Create Growth Momentum Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
GrowthMPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

GrowthMCols = ['EPI ex inflation M','EPI Synchronised Sectors or Regions M','Earnings Growth M',
              'Investment/Capex M','Credit Growth M','Inventory Cycle M','Global Trade M','Fiscal Impulse M']

for i in GrowthMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            GrowthMPerc.loc[j,'Reflation'] += 1
            GrowthMPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            GrowthMPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            GrowthMPerc.loc[j,'Downturn'] += 1
            GrowthMPerc.loc[j,'Recession'] += 1

# Growth Momentum Frame Percentages            
GrowthMPerc = GrowthMPerc/len(GrowthMCols)

GrowthMPerc.tail()


# In[29]:


# Create Growth Level Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
GrowthLPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

GrowthLCols = ['EPI ex inflation L','EPI Synchronised Sectors or Regions L','Earnings Growth L',
              'Investment/Capex L','Credit Growth L','Inventory Cycle L','Global Trade L','Fiscal Impulse L']

for i in GrowthLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            GrowthLPerc.loc[j,'Downturn'] += 1
            GrowthLPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            GrowthLPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            GrowthLPerc.loc[j,'Recession'] += 1
            GrowthLPerc.loc[j,'Reflation'] += 1

# Growth Momentum Frame Percentages            
GrowthLPerc = GrowthLPerc/len(GrowthLCols)


GrowthLPerc.tail()


# In[30]:


# Create Inflation Momentum Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
InflationMPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

InflationMCols = ['EPI Inflation M','Wage Inflation M','Core (Persistence/Breadth) M','OECD Output Gap M',
              'Employment EPI M','Unemployment level vs NAIRU M','Capacity Utilisation M']

for i in InflationMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            InflationMPerc.loc[j,'Mid Cycle'] += 1
            InflationMPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            InflationMPerc.loc[j,'Downturn'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            InflationMPerc.loc[j,'Recession'] += 1
            
            

# Growth Momentum Frame Percentages            
InflationMPerc = InflationMPerc/len(InflationMCols)


InflationMPerc.tail()


# In[31]:


# Create Inflation Level Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
InflationLPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

InflationLCols = ['EPI Inflation L','Wage Inflation L','Core (Persistence/Breadth) L','OECD Output Gap L',
              'Employment EPI L','Unemployment level vs NAIRU L','Capacity Utilisation L']

for i in InflationLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            InflationLPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            InflationLPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            InflationLPerc.loc[j,'Recession'] += 1
            InflationLPerc.loc[j,'Reflation'] += 1
            

# Growth Momentum Frame Percentages            
InflationLPerc = InflationLPerc/len(InflationLCols)


InflationLPerc.tail()


# In[32]:


# Create Financial Conditions Momentum Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
FinCondMPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

FinCondMCols = ['Property (1y momentum) M','Equities M','Household Leverage M','Corporate Leverage M',
               'Government Leverage M','Public Sector Liquidity ==> Reserves, Central Bank BS M',
               'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M',
               'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M',
               'Real Policy Rate vs real neutral rate M','Real Long End Yields M','Real Ccy vs ToT M',
               'Financial Sector Health M','Credit Spreads M']

for i in FinCondMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            FinCondMPerc.loc[j,'Reflation'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            FinCondMPerc.loc[j,'Mid Cycle'] += 1
            FinCondMPerc.loc[j,'Downturn'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            FinCondMPerc.loc[j,'Late Cycle'] += 1
            

# Growth Momentum Frame Percentages            
FinCondMPerc = FinCondMPerc/len(FinCondMCols)


FinCondMPerc.tail()


# In[33]:


# Create Financial Conditions Level Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
FinCondLPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

FinCondLCols = ['Property (level) L','Equities L','Household Leverage L','Corporate Leverage L',
               'Government Leverage L','Public Sector Liquidity ==> Reserves, Central Bank BS L',
               'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L',
               'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L',
               'Real Policy Rate vs real neutral rate L','Real Long End Yields M','Real Ccy vs ToT L',
               'Financial Sector Health L','Credit Spreads L']

for i in FinCondLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            FinCondLPerc.loc[j,'Reflation'] += 1
            FinCondLPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            FinCondLPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            FinCondLPerc.loc[j,'Recession'] += 1
            

# Growth Momentum Frame Percentages            
FinCondLPerc = FinCondLPerc/len(FinCondLCols)


FinCondLPerc.head()


# In[34]:


# Generate overall probabilities
# Loop makes 0s into 0.0001s to avoid totally nullifying regime outcomes

GrowthProb = GrowthMPerc*GrowthLPerc
for i in GrowthProb.columns:
    for j in GrowthProb.index:
        if GrowthProb.loc[j,i] < 0.01:
            GrowthProb.loc[j,i] = 0.0001
InflationProb = InflationMPerc*InflationLPerc
for i in InflationProb.columns:
    for j in InflationProb.index:
        if InflationProb.loc[j,i] < 0.01:
            InflationProb.loc[j,i] = 0.0001
FinCondProb = FinCondMPerc*FinCondLPerc
for i in FinCondProb.columns:
    for j in FinCondProb.index:
        if FinCondProb.loc[j,i] < 0.01:
            FinCondProb.loc[j,i] = 0.0001

# Adjust for blank regimes
InflationProb['Downturn'] = InflationMPerc['Downturn']*InflationMPerc['Downturn']
InflationProb['Reflation'] = InflationLPerc['Reflation']*InflationLPerc['Reflation']
FinCondProb['Downturn'] = FinCondMPerc['Downturn']*FinCondMPerc['Downturn']
FinCondProb['Recession'] = FinCondLPerc['Recession']*FinCondLPerc['Recession']

GrowthProb.tail()


# In[35]:


InflationProb.tail()


# In[36]:


FinCondProb.tail()


# In[37]:


# Calculate the total probabilities

#TotalProb = GrowthProb*((InflationProb+FinCondProb)/2) #higher dominance to Growth
TotalProb = GrowthProb*InflationProb*FinCondProb

#Calc in 100%
GrowthProb[RegCols] = GrowthProb[RegCols].div(GrowthProb[RegCols].sum(axis=1), axis=0).multiply(100)
InflationProb[RegCols] = InflationProb[RegCols].div(InflationProb[RegCols].sum(axis=1), axis=0).multiply(100)
FinCondProb[RegCols] = FinCondProb[RegCols].div(FinCondProb[RegCols].sum(axis=1), axis=0).multiply(100)
TotalProb[RegCols] = TotalProb[RegCols].div(TotalProb[RegCols].sum(axis=1), axis=0).multiply(100)

GrowthProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\CNQuantReg\CNRegGrowthProbability.xlsx')
InflationProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\CNQuantReg\CNRegInflationProbability.xlsx')
FinCondProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\CNQuantReg\CNRegFinancialCondProbability.xlsx')
TotalProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\CNQuantReg\CNRegTotalProbability.xlsx')
RegCode.transpose().to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\CNQuantReg\CNRegimeFullScoring.xlsx')


# In[ ]:




