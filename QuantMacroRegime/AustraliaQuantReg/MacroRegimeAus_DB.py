#!/usr/bin/env python
# coding: utf-8

# In[1]:

'''running from the root folder QuantMacroRegime not the country folder'''


# Imports
import numpy as np
import pandas as pd
import datetime as dt
from pandas.tseries.offsets import *
import numpy as np
import scipy.stats as scs

import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.dates import YearLocator, MonthLocator

# In[2]:


# Getting BBG data and cleaning
StartDate = '1995-12-30'
EndDate = (dt.datetime.today() + BDay(30)).strftime("%m/%d/%Y")

Dates = pd.DataFrame(pd.date_range(StartDate, EndDate, freq="D"), columns=['Date'])
Dates = Dates.set_index('Date', drop=True)
Dates.index.name = None

Month = pd.DataFrame(pd.date_range(StartDate, EndDate, freq="M"), columns=['Date'])
Month = Month.set_index('Date', drop=True)
Month.index.name = None

Quarter = pd.DataFrame(pd.date_range(StartDate, EndDate, freq="Q"), columns=['Date'])
Quarter = Quarter.set_index('Date', drop=True)
Quarter.index.name = None

# In[3]:


# Take EPI data from excel

# Read EPI
EPIDat = pd.read_csv(r'./DataUpdateFiles\MacroRegEPI.csv')
EPIDat.index = EPIDat['DATE']
EPIDat = EPIDat.drop(columns = ['DATE'])
EPIDat = EPIDat.fillna(method='ffill')
EPIDat = Dates.merge(EPIDat,right_index=True,left_index=True,how='left')
EPIDat = EPIDat.fillna(method='ffill')
EPIDat = Month.merge(EPIDat,right_index=True,left_index=True,how='left')
EPIDat.tail()

EPIIdx = ['EpiM_Headline_AU','EpiM_Business_AU','EpiM_Consumer_AU','EpiM_Employment_AU','EpiM_Growth_AU',
'EpiM_Inflation_AU']

EPIData = EPIDat[EPIIdx]
#EPIData.tail()

# Read PX last data
PXLastDat = pd.read_csv(r'./DataUpdateFiles/PXLAST.csv')
PXLastDat = pd.pivot_table(PXLastDat, values='Value', index=['Data_Date'],
                    columns=['Security'])
PXLastDat = PXLastDat.fillna(method='ffill')
PXLastDat.index = pd.to_datetime(PXLastDat.index, format = "%Y%m%d")
PXLastDat = Dates.merge(PXLastDat,right_index=True,left_index=True,how='left')
PXLastDat = PXLastDat.fillna(method='ffill')
PXLastDat = Month.merge(PXLastDat,right_index=True,left_index=True,how='left')
#PXLastDat.tail()

PXLastIdx = ['AUCEYOY Index','OZCACRED Index','NABSCAPS Index','AUNAINV Index','AUITGSB Index','AUITEXP Index',
'AUITIMP Index','EHBBAUY Index','AUNASFCE Index','AUNATEXP Index','AUNATGG Index','AUWSTTSA Index','RBCPWM%Y Index',
'OEEOAUUG Index','AULFUNEM Index','NABSCUTI Index','RPAUMED Index','CPNFAUHG Index',
'CPNFAUA3 Index','AUNATGI Index','CPNFAUNG Index','CPNFAUOG Index',
'FARBCRED Index',
'EBBSTOTA Index',
'EURUSD Curncy',
'BJACTOTL Index',
'JPYUSD Curncy',
'CNBMTTAS Index',
'CNYUSD Curncy',
'OZCABASY Index','OZCABRDY Index','AUM3Y Index',
'AUM1Y Index','USCABAL Index','ECOCEAS Index','ECOCJPN Index','ECOCCNN Index','RBACOR Index','ADGGBE05 Index',
'AUCPIYOY Index',
'USNREUAR Index',
'USNRUS Index',
'GACGB10 Index','193.028 Index','CTOTAUD Index','BAUCOAS Index']

PXLastData = PXLastDat[PXLastIdx]
#PXLastData.tail()

# Read EPS data
BestEPSDat = pd.read_csv(r'./DataUpdateFiles/BESTEPS.csv')
BestEPSDat = pd.pivot_table(BestEPSDat, values='Value', index=['Data_Date'],
                    columns=['Security'])
BestEPSDat = BestEPSDat.fillna(method='ffill')
BestEPSDat.index = pd.to_datetime(BestEPSDat.index, format = "%Y%m%d")
BestEPSDat = Dates.merge(BestEPSDat,right_index=True,left_index=True,how='left')
BestEPSDat = BestEPSDat.fillna(method='ffill')
BestEPSDat = Month.merge(BestEPSDat,right_index=True,left_index=True,how='left')
#BestEPSDat.tail()

BestEPSIdx = ['AS51 Index']

BestEPSData = BestEPSDat[BestEPSIdx]
#BestEPSData.tail()

# Read PE data
BestPEDat = pd.read_csv(r'./DataUpdateFiles\BESTPERATIO.csv')
BestPEDat = pd.pivot_table(BestPEDat, values='Value', index=['Data_Date'],
                    columns=['Security'])
BestPEDat = BestPEDat.fillna(method='ffill')
BestPEDat.index = pd.to_datetime(BestPEDat.index, format = "%Y%m%d")
BestPEDat = Dates.merge(BestPEDat,right_index=True,left_index=True,how='left')
BestPEDat = BestPEDat.fillna(method='ffill')
BestPEDat = Month.merge(BestPEDat,right_index=True,left_index=True,how='left')
#BestPEDat.tail()

BestPEIdx = ['AS51 Index']

BestPEData = BestPEDat[BestPEIdx]
#BestPEData.tail()

# Read NIMS data
NIMsDat = pd.read_csv(r'./DataUpdateFiles\ANNNETINTMARGIN.csv')
NIMsDat = pd.pivot_table(NIMsDat, values='Value', index=['Data_Date'],
                    columns=['Security'])
NIMsDat = NIMsDat.fillna(method='ffill')
NIMsDat.index = pd.to_datetime(NIMsDat.index, format = "%Y%m%d")
NIMsDat = Dates.merge(NIMsDat,right_index=True,left_index=True,how='left')
NIMsDat = NIMsDat.fillna(method='ffill')
NIMsDat = Month.merge(NIMsDat,right_index=True,left_index=True,how='left')
#NIMsDat.tail()

NIMsIdx = ['CBA AU Equity','WBC AU Equity','NAB AU Equity',
'ANZ AU Equity']

NIMsData = NIMsDat[NIMsIdx]
#NIMsData.tail()


# Put the codes in

NamesIdx = ['EPI ex inflation M', 'EPI ex inflation L', 'EPI Synchronised Sectors or Regions M',
            'EPI Synchronised Sectors or Regions L',
            'Earnings Growth M', 'Earnings Growth L', 'Investment/Capex M', 'Investment/Capex L', 'Credit Growth M',
            'Credit Growth L', 'Inventory Cycle M',
            'Inventory Cycle L', 'Global Trade M', 'Global Trade L', 'Fiscal Impulse M', 'Fiscal Impulse L',
            'EPI Inflation M', 'EPI Inflation L', 'Wage Inflation M', 'Wage Inflation L',
            'Core (Persistence/Breadth) M',
            'Core (Persistence/Breadth) L', 'OECD Output Gap M', 'OECD Output Gap L', 'Employment EPI M',
            'Employment EPI L', 'Unemployment level vs NAIRU M', 'Unemployment level vs NAIRU L',
            'Capacity Utilisation M', 'Capacity Utilisation L', 'Property (1y momentum) M', 'Property (level) L',
            'Equities M', 'Equities L', 'Household Leverage M', 'Household Leverage L', 'Corporate Leverage M',
            'Corporate Leverage L', 'Government Leverage M', 'Government Leverage L',
            'Public Sector Liquidity ==> Reserves, Central Bank BS M',
            'Public Sector Liquidity ==> Reserves, Central Bank BS L',
            'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M',
            'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L',
            'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M',
            'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L',
            'Real Policy Rate vs real neutral rate M', 'Real Policy Rate vs real neutral rate L',
            'Real Long End Yields M',
            'Real Long End Yield L', 'Real Ccy vs ToT M', 'Real Ccy vs ToT L', 'Financial Sector Health M',
            'Financial Sector Health L', 'Credit Spreads M', 'Credit Spreads L']

CodesIdx = ['AS51 Index BEPS', 'AUCEYOY Index', 'OZCACRED Index', 'NABSCAPS Index', 'AUNAINV Index', 'AUITGSB Index',
            'AUITEXP Index',
            'AUITIMP Index', 'EHBBAUY Index', 'AUNASFCE Index', 'AUNATEXP Index', 'AUNATGG Index', 'AUWSTTSA Index',
            'RBCPWM%Y Index',
            'OEEOAUUG Index', 'AULFUNEM Index', 'NABSCUTI Index', 'RPAUMED Index', 'AS51 Index PE', 'CPNFAUHG Index',
            'CPNFAUA3 Index', 'AUNATGI Index', 'CPNFAUNG Index', 'CPNFAUOG Index',
            'FARBCRED Index',
            'EBBSTOTA Index',
            'EURUSD Curncy',
            'BJACTOTL Index',
            'JPYUSD Curncy',
            'CNBMTTAS Index',
            'CNYUSD Curncy',
            'OZCABASY Index', 'OZCABRDY Index', 'AUM3Y Index',
            'AUM1Y Index', 'USCABAL Index', 'ECOCEAS Index', 'ECOCJPN Index', 'ECOCCNN Index', 'RBACOR Index',
            'ADGGBE05 Index',
            'AUCPIYOY Index',
            'USNREUAR Index',
            'USNRUS Index',
            'GACGB10 Index', '193.028 Index', 'CTOTAUD Index', 'CBA AU Equity', 'WBC AU Equity', 'NAB AU Equity',
            'ANZ AU Equity', 'BAUCOAS Index']

# In[7]:


CodesNamesIdx = ['S&P/ASX 200 INDEX', 'Australia New Capital Expendit', 'RBA Financial Aggregate Credit',
                 'National Australia Bank Busine',
                 'GDP Inventories', 'Australia Trade Balance Goods',
                 'Australia Exports Goods & Serv', 'Australia Imports Goods & Serv', 'Australia Budget Balance (% GD',
                 'Australia GDP State & Local Go', 'Australia GDP Final Consumptio', 'Australia GDP Public GFCF Gene',
                 'Australia Wages & Salaries Tot', 'Australia CPI Measures Weighte',
                 'OECD Eco Outlook Australia Out', 'Australia Labor Force Unemploy', 'National Australia Bank Busine',
                 'Australia CoreLogic - Median C', 'BIS Australia Credit to Househ',
                 'Australia Total Credit to Hous', 'Australia Household Income Use', 'BIS Australia Credit to Non Fi',
                 'BIS Australia Credit to Genera',
                 'US Factors Supplying Reserve F',
                 'ECB Balance Sheet All Assets',
                 'EUR-USD X-RATE',
                 'Bank of Japan assets:Total',
                 'JPY-USD X-RATE',
                 'China Central Bank Balance She',
                 'CNY-USD X-RATE',
                 'RBA Financial Aggregate Money', 'RBA Financial Aggregate Broad',
                 'Australia Money Supply M3 YoY', 'Australia Money Supply M1 YoY', 'US Nominal Account Balance In',
                 'Eurozone Current Account Balan', 'Japan Current Account Balance', 'China Current Account Balance',
                 'RBA Interbank Overnight Cash R', 'Australia Breakeven 5 Year', 'Australia CPI All Groups Goods',
                 'Laubach Williams Natural Rate',
                 'Laubach Williams Natural Rate',
                 'Australia Govt Bonds Generic Y', 'IMF Australia Real Effective E', 'Citi Terms of Trade Index - Au',
                 'COMMONWEALTH BANK OF AUSTRAL', 'WESTPAC BANKING CORP', 'NATIONAL AUSTRALIA BANK LTD',
                 'AUST AND NZ BANKING GROUP', 'Bloomberg Barclays Australian']

# In[20]:


#Copies and Adjustments

#Main Code df
RegCode = pd.DataFrame(0,index=Month.index, columns=NamesIdx)

PXLast = PXLastData.copy()
BestEPS = BestEPSData.copy()
BestPE = BestPEData.copy()
BankNIMs = NIMsData.copy()

# Add correct api calls
#PXLast = PXLast.drop(columns=['SPX Index'])
PXLast['AS51 Index BEPS'] = BestEPS['AS51 Index'].copy() # EPS Growth
PXLast['AS51 Index PE'] = BestPE['AS51 Index'].copy() # PE Ratio
PXLast['CBA AU Equity'] = NIMsData['CBA AU Equity'].copy()
PXLast['WBC AU Equity'] = NIMsData['WBC AU Equity'].copy()
PXLast['NAB AU Equity'] = NIMsData['NAB AU Equity'].copy()
PXLast['ANZ AU Equity'] = NIMsData['ANZ AU Equity'].copy()



#Add in EPI
PXLast['EpiM_Headline_AU'] = EPIDat['EpiM_Headline_AU'].copy()
PXLast['EpiM_Business_AU'] = EPIDat['EpiM_Business_AU'].copy()
PXLast['EpiM_Consumer_AU'] = EPIDat['EpiM_Consumer_AU'].copy()
PXLast['EpiM_Employment_AU'] = EPIDat['EpiM_Employment_AU'].copy()
PXLast['EpiM_Growth_AU'] = EPIDat['EpiM_Growth_AU'].copy()
PXLast['EpiM_Inflation_AU'] = EPIDat['EpiM_Inflation_AU'].copy()

# In[19]:


PXLast.tail()

# In[21]:


# Start adjusting data series

# YOY%
PXLast['OZCACRED Index'] = (PXLast['OZCACRED Index'] / PXLast['OZCACRED Index'].shift(12) - 1)
PXLast['AUITEXP Index'] = (PXLast['AUITEXP Index'] / PXLast['AUITEXP Index'].shift(12) - 1)
PXLast['AUITIMP Index'] = (PXLast['AUITIMP Index'] / PXLast['AUITIMP Index'].shift(12) - 1)
PXLast['AUNASFCE Index'] = (PXLast['AUNASFCE Index'] / PXLast['AUNASFCE Index'].shift(12) - 1)
PXLast['AUNATEXP Index'] = (PXLast['AUNATEXP Index'] / PXLast['AUNATEXP Index'].shift(12) - 1)
PXLast['AUNATGG Index'] = (PXLast['AUNATGG Index'] / PXLast['AUNATGG Index'].shift(12) - 1)
PXLast['AUWSTTSA Index'] = (PXLast['AUWSTTSA Index'] / PXLast['AUWSTTSA Index'].shift(12) - 1)

# Unemployment vs NAIRU estimate of 4.5% for Australia
PXLast['AULFUNEM Index'] = (PXLast['AULFUNEM Index'] - 4.5)

# Inverse for unemployment, fiscal and debt, credit spreads
PXLast['AULFUNEM Index'] = -1 * PXLast['AULFUNEM Index']
PXLast['EHBBAUY Index'] = -1 * PXLast['EHBBAUY Index']
PXLast['CPNFAUHG Index'] = -1 * PXLast['CPNFAUHG Index']
PXLast['CPNFAUNG Index'] = -1 * PXLast['CPNFAUNG Index']
PXLast['CPNFAUOG Index'] = -1 * PXLast['CPNFAUOG Index']
PXLast['BAUCOAS Index'] = -1 * PXLast['BAUCOAS Index']

# 12M SUm for inventories, trade balance
PXLast['AUNAINV Index'] = -1 * PXLast['AUNAINV Index'].rolling(12).sum()
PXLast['AUITGSB Index'] = PXLast['AUITGSB Index'].rolling(12).sum()

# Household debt as % of income
PXLast['CPNFAUA3 Index'] = (PXLast['CPNFAUA3 Index'] / (PXLast['AUNATGI Index'] / 1000))  # HH income adj for blns

# Central Bank Balance sheet, convert all CB B/S to USD and Sum
PXLast['FARBCRED Index'] = ((PXLast['FARBCRED Index'] / 1000) + (PXLast['EBBSTOTA Index'] * PXLast['EURUSD Curncy'])
                            + (PXLast['BJACTOTL Index'] * PXLast['JPYUSD Curncy'])
                            + (PXLast['CNBMTTAS Index'] * PXLast['CNYUSD Curncy']))

# Current Account Balance G4 Sums
PXLast['USCABAL Index'] = PXLast['USCABAL Index'] + PXLast['ECOCEAS Index'] + PXLast['ECOCJPN Index'] + PXLast[
    'ECOCCNN Index']

# Real Rate Calcs policy and long end, and vs neutral rate for policy
PXLast['GACGB10 Index'] = ((PXLast['ADGGBE05 Index'] + PXLast['AUCPIYOY Index']) / 2
                           - PXLast['GACGB10 Index'])  # Real long end
PXLast['RBACOR Index'] = -1 * ((PXLast['RBACOR Index'] - (PXLast['ADGGBE05 Index'] + PXLast['AUCPIYOY Index']) / 2)
                               - (0.3 * PXLast['USNRUS Index'] + 0.6 * PXLast['USNREUAR Index']
                                  + 0.1 * PXLast['GACGB10 Index'].rolling(12).mean()))  # Real policy rate

# Real currency v Terms of trade
PXLast['193.028 Index'] = PXLast['CTOTAUD Index'] / PXLast['193.028 Index']

# In[22]:


# Z-scoring at different time frames

# 0.5Y
PXLast05Yzs = (PXLast - PXLast.rolling(6).mean()) / PXLast.rolling(6).std()

# 1y
PXLast1Yzs = (PXLast - PXLast.rolling(12).mean()) / PXLast.rolling(12).std()

# 3Y
PXLast3Yzs = (PXLast - PXLast.rolling(3 * 12).mean()) / PXLast.rolling(3 * 12).std()

# 5y
PXLast5Yzs = (PXLast - PXLast.rolling(5 * 12).mean()) / PXLast.rolling(5 * 12).std()

# 7Y
PXLast7Yzs = (PXLast - PXLast.rolling(7 * 12).mean()) / PXLast.rolling(7 * 12).std()

# 10Y
PXLast10Yzs = (PXLast - PXLast.rolling(10 * 12).mean()) / PXLast.rolling(10 * 12).std()

# In[23]:


# Add in Adjusted Series
# Growth
RegCode['EPI ex inflation M'] = PXLast1Yzs['EpiM_Headline_AU']
RegCode['EPI ex inflation L'] = PXLast5Yzs['EpiM_Headline_AU']

'''
RegCode['EPI Synchronised Sectors or Regions M'] = (PXLast1Yzs['EpiM_Business_AU'] + PXLast1Yzs['EpiM_Consumer_AU'] +
                                                    PXLast1Yzs['EpiM_Employment_AU'] + PXLast1Yzs['EpiM_Growth_AU'] +
                                                    PXLast1Yzs['EpiM_Inflation_AU']) / 5
RegCode['EPI Synchronised Sectors or Regions L'] = (PXLast5Yzs['EpiM_Business_AU'] + PXLast5Yzs['EpiM_Consumer_AU'] +
                                                    PXLast5Yzs['EpiM_Employment_AU'] + PXLast5Yzs['EpiM_Growth_AU'] +
                                                    PXLast5Yzs['EpiM_Inflation_AU']) / 5
'''


RegCode['EPI Synchronised Sectors or Regions M'] = pd.DataFrame([PXLast1Yzs['EpiM_Business_AU'],
             PXLast1Yzs['EpiM_Consumer_AU'],
             PXLast1Yzs['EpiM_Employment_AU'],
             PXLast1Yzs['EpiM_Growth_AU'],
             PXLast1Yzs['EpiM_Inflation_AU']]).transpose().mean(axis=1)


RegCode['EPI Synchronised Sectors or Regions L'] = pd.DataFrame([PXLast5Yzs['EpiM_Business_AU'],
             PXLast5Yzs['EpiM_Consumer_AU'],
             PXLast5Yzs['EpiM_Employment_AU'],
             PXLast5Yzs['EpiM_Growth_AU'],
             PXLast5Yzs['EpiM_Inflation_AU']]).transpose().mean(axis=1)



RegCode['Earnings Growth M'] = PXLast1Yzs['AS51 Index BEPS']
RegCode['Earnings Growth L'] = PXLast7Yzs['AS51 Index BEPS']
RegCode['Investment/Capex M'] = (PXLast5Yzs['AUCEYOY Index'] + PXLast5Yzs['NABSCAPS Index']) / 2
RegCode['Investment/Capex L'] = (PXLast10Yzs['AUCEYOY Index'] + PXLast10Yzs['NABSCAPS Index']) / 2
RegCode['Credit Growth M'] = PXLast1Yzs['OZCACRED Index']
RegCode['Credit Growth L'] = PXLast7Yzs['OZCACRED Index']
RegCode['Inventory Cycle M'] = PXLast1Yzs['AUNAINV Index']
RegCode['Inventory Cycle L'] = PXLast7Yzs['AUNAINV Index']
RegCode['Global Trade M'] = (PXLast1Yzs['AUITGSB Index'] + PXLast1Yzs['AUITEXP Index'] + PXLast1Yzs[
    'AUITIMP Index']) / 3
RegCode['Global Trade L'] = (PXLast7Yzs['AUITGSB Index'] + PXLast7Yzs['AUITEXP Index'] + PXLast7Yzs[
    'AUITIMP Index']) / 3
RegCode['Fiscal Impulse M'] = (PXLast5Yzs['EHBBAUY Index'] + PXLast5Yzs['AUNASFCE Index'] +
                               PXLast5Yzs['AUNATEXP Index'] + PXLast5Yzs['AUNATGG Index']) / 4
RegCode['Fiscal Impulse L'] = (PXLast10Yzs['EHBBAUY Index'] + PXLast10Yzs['AUNASFCE Index'] +
                               PXLast10Yzs['AUNATEXP Index'] + PXLast10Yzs['AUNATGG Index']) / 4

# In[24]:


# Output Gap inflation
RegCode['EPI Inflation M'] = PXLast1Yzs['EpiM_Inflation_AU']
RegCode['EPI Inflation L'] = PXLast5Yzs['EpiM_Inflation_AU']
RegCode['Wage Inflation M'] = PXLast1Yzs['AUWSTTSA Index']
RegCode['Wage Inflation L'] = PXLast7Yzs['AUWSTTSA Index']
RegCode['Core (Persistence/Breadth) M'] = PXLast1Yzs['RBCPWM%Y Index']
RegCode['Core (Persistence/Breadth) L'] = PXLast7Yzs['RBCPWM%Y Index']
RegCode['OECD Output Gap M'] = PXLast5Yzs['OEEOAUUG Index']
RegCode['OECD Output Gap L'] = PXLast10Yzs['OEEOAUUG Index']
RegCode['Employment EPI M'] = PXLast5Yzs['EpiM_Employment_AU']
RegCode['Employment EPI L'] = PXLast10Yzs['EpiM_Employment_AU']
RegCode['Unemployment level vs NAIRU M'] = PXLast5Yzs['AULFUNEM Index']
RegCode['Unemployment level vs NAIRU L'] = PXLast10Yzs['AULFUNEM Index']
RegCode['Capacity Utilisation M'] = PXLast5Yzs['NABSCUTI Index']
RegCode['Capacity Utilisation L'] = PXLast10Yzs['NABSCUTI Index']

# In[25]:


#### Financial Conditions ###
# Capacity to borrow
RegCode['Property (1y momentum) M'] = PXLast1Yzs['RPAUMED Index']
RegCode['Property (level) L'] = PXLast7Yzs['RPAUMED Index']
RegCode['Equities M'] = PXLast05Yzs['AS51 Index PE']
RegCode['Equities L'] = PXLast7Yzs['AS51 Index PE']
RegCode['Household Leverage M'] = (PXLast5Yzs['CPNFAUHG Index'] + PXLast5Yzs['CPNFAUA3 Index']) / 2
RegCode['Household Leverage L'] = (PXLast10Yzs['CPNFAUHG Index'] + PXLast10Yzs['CPNFAUA3 Index']) / 2
RegCode['Corporate Leverage M'] = PXLast5Yzs['CPNFAUNG Index']
RegCode['Corporate Leverage L'] = PXLast10Yzs['CPNFAUNG Index']
RegCode['Government Leverage M'] = PXLast5Yzs['CPNFAUOG Index']
RegCode['Government Leverage L'] = PXLast10Yzs['CPNFAUOG Index']

# Liquidity
RegCode['Public Sector Liquidity ==> Reserves, Central Bank BS M'] = PXLast05Yzs['FARBCRED Index']
RegCode['Public Sector Liquidity ==> Reserves, Central Bank BS L'] = PXLast3Yzs['FARBCRED Index']
RegCode[
    'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M'] = (
                                                                                                                                  PXLast1Yzs[
                                                                                                                                      'OZCABASY Index'] +
                                                                                                                                  PXLast1Yzs[
                                                                                                                                      'OZCABRDY Index'] +
                                                                                                                                  PXLast1Yzs[
                                                                                                                                      'AUM3Y Index'] +
                                                                                                                                  PXLast1Yzs[
                                                                                                                                      'AUM1Y Index']) / 4
RegCode[
    'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L'] = (
                                                                                                                                  PXLast7Yzs[
                                                                                                                                      'OZCABASY Index'] +
                                                                                                                                  PXLast7Yzs[
                                                                                                                                      'OZCABRDY Index'] +
                                                                                                                                  PXLast7Yzs[
                                                                                                                                      'AUM3Y Index'] +
                                                                                                                                  PXLast7Yzs[
                                                                                                                                      'AUM1Y Index']) / 4
RegCode['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M'] = PXLast5Yzs['USCABAL Index']
RegCode['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L'] = PXLast10Yzs['USCABAL Index']

# Funding Costs
RegCode['Real Policy Rate vs real neutral rate M'] = PXLast05Yzs['RBACOR Index']
RegCode['Real Policy Rate vs real neutral rate L'] = PXLast5Yzs['RBACOR Index']
RegCode['Real Long End Yields M'] = PXLast05Yzs['GACGB10 Index']
RegCode['Real Long End Yield L'] = PXLast5Yzs['GACGB10 Index']
RegCode['Real Ccy vs ToT M'] = PXLast1Yzs['193.028 Index']
RegCode['Real Ccy vs ToT L'] = PXLast7Yzs['193.028 Index']
RegCode['Financial Sector Health M'] = (PXLast1Yzs['CBA AU Equity'] + PXLast1Yzs['WBC AU Equity'] + PXLast1Yzs[
    'NAB AU Equity'] + PXLast1Yzs['ANZ AU Equity']) / 4
RegCode['Financial Sector Health L'] = (PXLast7Yzs['CBA AU Equity'] + PXLast7Yzs['WBC AU Equity'] + PXLast7Yzs[
    'NAB AU Equity'] + PXLast7Yzs['ANZ AU Equity']) / 4
RegCode['Credit Spreads M'] = PXLast05Yzs['BAUCOAS Index']
RegCode['Credit Spreads L'] = PXLast3Yzs['BAUCOAS Index']

# In[26]:


# Frame conditioning logic

ConditionFrame = pd.DataFrame(RegCode.copy())
for i in (ConditionFrame.columns):
    for j in (ConditionFrame.index):
        if (ConditionFrame.loc[j, i] <= -1):
            ConditionFrame.loc[j, i] = 0
        elif (ConditionFrame.loc[j, i] >= 1):
            ConditionFrame.loc[j, i] = 2
        else:
            ConditionFrame.loc[j, i] = 1

ConditionFrame.tail()

# In[27]:


# Create Growth Momentum Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
GrowthMPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

GrowthMCols = ['EPI ex inflation M', 'EPI Synchronised Sectors or Regions M', 'Earnings Growth M',
               'Investment/Capex M', 'Credit Growth M', 'Inventory Cycle M', 'Global Trade M', 'Fiscal Impulse M']

for i in GrowthMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            GrowthMPerc.loc[j, 'Reflation'] += 1
            
            '''=> new logic midcycle momentum is no longer countered'''            
            #GrowthMPerc.loc[j, 'Mid Cycle'] += 1
            
        elif ConditionFrame.loc[j, i] == 1:
            GrowthMPerc.loc[j, 'Late Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            GrowthMPerc.loc[j, 'Downturn'] += 1
            GrowthMPerc.loc[j, 'Recession'] += 1
            
            

# Growth Momentum Frame Percentages
GrowthMPerc = GrowthMPerc / len(GrowthMCols)

'''=> new logic midcycle momentum is no longer countered'''
GrowthMPerc['Mid Cycle'] = np.nan

GrowthMPerc.tail()

# In[28]:


# Create Growth Level Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
GrowthLPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

GrowthLCols = ['EPI ex inflation L', 'EPI Synchronised Sectors or Regions L', 'Earnings Growth L',
               'Investment/Capex L', 'Credit Growth L', 'Inventory Cycle L', 'Global Trade L', 'Fiscal Impulse L']

for i in GrowthLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            
            '''=> new logic growth level is no longer countered for downturn'''
            GrowthLPerc.loc[j, 'Downturn'] += 1
            
            GrowthLPerc.loc[j, 'Late Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 1:
            GrowthLPerc.loc[j, 'Mid Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            GrowthLPerc.loc[j, 'Recession'] += 1
            GrowthLPerc.loc[j, 'Reflation'] += 1

# Growth Momentum Frame Percentages
GrowthLPerc = GrowthLPerc / len(GrowthLCols)

'''=> new logic growth level is no longer countered for downturn'''
#GrowthLPerc['Downturn'] = np.nan

GrowthLPerc.tail()

# In[29]:


# Create Inflation Momentum Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
InflationMPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

InflationMCols = ['EPI Inflation M', 'Wage Inflation M', 'Core (Persistence/Breadth) M', 'OECD Output Gap M',
                  'Employment EPI M', 'Unemployment level vs NAIRU M', 'Capacity Utilisation M']

for i in InflationMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            InflationMPerc.loc[j, 'Mid Cycle'] += 1
            InflationMPerc.loc[j, 'Late Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 1:
            InflationMPerc.loc[j, 'Downturn'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            InflationMPerc.loc[j, 'Recession'] += 1
            '''=> new logic inflation momentum is countered for reflation'''
            InflationMPerc.loc[j, 'Reflation'] += 1

# Growth Momentum Frame Percentages
InflationMPerc = InflationMPerc / len(InflationMCols)

InflationMPerc.tail()

# In[30]:


# Create Inflation Level Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
InflationLPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

InflationLCols = ['EPI Inflation L', 'Wage Inflation L', 'Core (Persistence/Breadth) L', 'OECD Output Gap L',
                  'Employment EPI L', 'Unemployment level vs NAIRU L', 'Capacity Utilisation L']

for i in InflationLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            InflationLPerc.loc[j, 'Late Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 1:
            InflationLPerc.loc[j, 'Mid Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            InflationLPerc.loc[j, 'Recession'] += 1
            InflationLPerc.loc[j, 'Reflation'] += 1

# Growth Momentum Frame Percentages
InflationLPerc = InflationLPerc / len(InflationLCols)

'''=> existing logic inflation level is not countered for downturn - just make it clear by setting it to nan'''
InflationLPerc['Downturn'] = np.nan

InflationLPerc.tail()

# In[31]:


# Create Financial Conditions Momentum Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
FinCondMPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

FinCondMCols = ['Property (1y momentum) M', 'Equities M', 'Household Leverage M', 'Corporate Leverage M',
                'Government Leverage M', 'Public Sector Liquidity ==> Reserves, Central Bank BS M',
                'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M',
                'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M',
                'Real Policy Rate vs real neutral rate M', 'Real Long End Yields M', 'Real Ccy vs ToT M',
                'Financial Sector Health M', 'Credit Spreads M']

for i in FinCondMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            FinCondMPerc.loc[j, 'Reflation'] += 1
        elif ConditionFrame.loc[j, i] == 1:
            FinCondMPerc.loc[j, 'Mid Cycle'] += 1
            FinCondMPerc.loc[j, 'Downturn'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            FinCondMPerc.loc[j, 'Late Cycle'] += 1

# Growth Momentum Frame Percentages
FinCondMPerc = FinCondMPerc / len(FinCondMCols)


'''=> existing logic FC momentum is not countered for recession - just make it clear by setting it to nan'''
FinCondMPerc['Recession'] = np.nan

FinCondMPerc.tail()

# In[32]:


# Create Financial Conditions Level Regime Percentages
RegCols = ['Reflation', 'Mid Cycle', 'Late Cycle', 'Downturn', 'Recession']
FinCondLPerc = pd.DataFrame(0, index=RegCode.index, columns=RegCols)

FinCondLCols = ['Property (level) L', 'Equities L', 'Household Leverage L', 'Corporate Leverage L',
                'Government Leverage L', 'Public Sector Liquidity ==> Reserves, Central Bank BS L',
                'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L',
                'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L',
                'Real Policy Rate vs real neutral rate L', 'Real Long End Yields M', 'Real Ccy vs ToT L',
                'Financial Sector Health L', 'Credit Spreads L']

for i in FinCondLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j, i] == 2:
            FinCondLPerc.loc[j, 'Reflation'] += 1
            FinCondLPerc.loc[j, 'Mid Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 1:
            FinCondLPerc.loc[j, 'Late Cycle'] += 1
        elif ConditionFrame.loc[j, i] == 0:
            FinCondLPerc.loc[j, 'Recession'] += 1

# Growth Momentum Frame Percentages
FinCondLPerc = FinCondLPerc / len(FinCondLCols)


'''=> existing logic FC level is not countered for downturn - just make it clear by setting it to nan'''
FinCondLPerc['Downturn'] = np.nan



FinCondLPerc.head()

# In[33]:


# Generate overall probabilities
# Loop makes 0s into 0.0001s to avoid totally nullifying regime outcomes


'''=> New logic to calc joint prob

    mid cycle GrowthMPerc is not relevent 
    downturn GrowthLPerc is not relevent
    therefore for mid cycle and downtun there is no joint prod prob for growth. hence set them to 1 to fake joint product

    downturn InflationLPerc is not relevent
    therefore for downtun there is no joint prod prob for inflation. hence set them to 1 to fake joint product

    recession FinCondMPerc is not relevent
    downturn FinCondLPerc is not relevent
    therefore for downtun and recession there is no joint prod prob for financial condition. hence set them to 1 to fake joint product

'''

GrowthProb = GrowthMPerc.fillna(1) * GrowthLPerc.fillna(1)
InflationProb = InflationMPerc.fillna(1) * InflationLPerc.fillna(1)
FinCondProb = FinCondMPerc.fillna(1) * FinCondLPerc.fillna(1)
TotalProb = GrowthProb * InflationProb * FinCondProb


''' The following codes are replaced by the 4 lines code above
 
GrowthProb = GrowthMPerc * GrowthLPerc

for i in GrowthProb.columns:
    for j in GrowthProb.index:
        if GrowthProb.loc[j, i] < 0.01:
            GrowthProb.loc[j, i] = 0.0001
InflationProb = InflationMPerc * InflationLPerc
for i in InflationProb.columns:
    for j in InflationProb.index:
        if InflationProb.loc[j, i] < 0.01:
            InflationProb.loc[j, i] = 0.0001
FinCondProb = FinCondMPerc * FinCondLPerc
for i in FinCondProb.columns:
    for j in FinCondProb.index:
        if FinCondProb.loc[j, i] < 0.01:
            FinCondProb.loc[j, i] = 0.0001

# Adjust for blank regimes
InflationProb['Downturn'] = InflationMPerc['Downturn'] * InflationMPerc['Downturn']
InflationProb['Reflation'] = InflationLPerc['Reflation'] * InflationLPerc['Reflation']
FinCondProb['Downturn'] = FinCondMPerc['Downturn'] * FinCondMPerc['Downturn']
FinCondProb['Recession'] = FinCondLPerc['Recession'] * FinCondLPerc['Recession']



GrowthProb.tail()

# In[34]:


InflationProb.tail()

# In[35]:


FinCondProb.tail()

# In[36]:


# Calculate the total probabilities

# TotalProb = GrowthProb*((InflationProb+FinCondProb)/2) #higher dominance to Growth
TotalProb = GrowthProb * InflationProb * FinCondProb
'''



# Calc in 100%
GrowthProb[RegCols] = GrowthProb[RegCols].div(GrowthProb[RegCols].sum(axis=1), axis=0).multiply(100)
InflationProb[RegCols] = InflationProb[RegCols].div(InflationProb[RegCols].sum(axis=1), axis=0).multiply(100)
FinCondProb[RegCols] = FinCondProb[RegCols].div(FinCondProb[RegCols].sum(axis=1), axis=0).multiply(100)
TotalProb[RegCols] = TotalProb[RegCols].div(TotalProb[RegCols].sum(axis=1), axis=0).multiply(100)




'''=> New logic a second version of joint prob
    This time sum all the lvl/momentum from growth/inflation/fc and avg them by 6.
    for those component that is not relevent we treat them as 0 which will effectively
    downscore the regime prob for those having non relevent component. This will compensate the 
    original total product prob because they overweight the significance of regime with non relevent compoennet

    the only exception is downturn which will be overridden by oroginal TotalProb downturn 
'''

TotalProbV2 = (GrowthMPerc.fillna(0) + GrowthLPerc.fillna(0) + 
              InflationMPerc.fillna(0) + InflationLPerc.fillna(0) + 
              FinCondMPerc.fillna(0) * FinCondLPerc.fillna(0)) / 6 

TotalProbV2['Downturn'] = TotalProb['Downturn']/100 
#normalising
TotalProbV2 = TotalProbV2.divide(TotalProbV2.sum(axis=1), axis=0) * 100


TotalProbAvg = (TotalProb.fillna(0) + TotalProbV2.fillna(0)) / (1 - TotalProbV2.isna()) + (1 - TotalProb.isna())


GrowthProb.to_excel(
    r'./output/AusRegGrowthProbability.xlsx')
InflationProb.to_excel(
    r'./output/AusRegInflationProbability.xlsx')
FinCondProb.to_excel(
    r'./output/AusRegFinancialCondProbability.xlsx')

TotalProb.ffill().to_excel(
    r'./output/AusRegTotalProbabilityV1.xlsx')

TotalProbV2.ffill().to_excel(
    r'./output/AusRegTotalProbabilityV2.xlsx')

TotalProbAvg.ffill().to_excel(
    r'./output/AusRegTotalProbabilityAvg.xlsx')

RegCode.transpose().to_excel(
    r'./output/AusRegimeFullScoring.xlsx')



''' the following logic is replaced by code above

GrowthProb.to_excel(
    r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\AustraliaQuantReg\AusRegGrowthProbability.xlsx')
InflationProb.to_excel(
    r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\AustraliaQuantReg\AusRegInflationProbability.xlsx')
FinCondProb.to_excel(
    r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\AustraliaQuantReg\AusRegFinancialCondProbability.xlsx')
TotalProb.to_excel(
    r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\AustraliaQuantReg\AusRegTotalProbability.xlsx')
RegCode.transpose().to_excel(
    r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\AustraliaQuantReg\AusRegimeFullScoring.xlsx')

'''




