#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Imports
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import datetime as dt
from pandas.tseries.offsets import *
import numpy as np
import scipy.stats as scs

import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.dates import YearLocator, MonthLocator


# In[2]:


# Getting BBG data and cleaning
StartDate = '1995-12-30'
EndDate = (dt.datetime.today()+BDay(30)).strftime("%m/%d/%Y")

Dates = pd.DataFrame(pd.date_range(StartDate,EndDate,freq="D"),columns=['Date'])
Dates = Dates.set_index('Date',drop=True)
Dates.index.name = None

Month = pd.DataFrame(pd.date_range(StartDate,EndDate,freq="M"),columns=['Date'])
Month = Month.set_index('Date',drop=True)
Month.index.name = None

Quarter = pd.DataFrame(pd.date_range(StartDate,EndDate,freq="Q"),columns=['Date'])
Quarter = Quarter.set_index('Date',drop=True)
Quarter.index.name = None


# In[3]:


# Take EPI data from excel

# Read EPI
UKEPIEx = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegEPI.xlsx').parse('MacroRegEPI')
UKEPIEx.index = UKEPIEx['Date']
UKEPIEx = UKEPIEx.drop(columns = ['Date'])
UKEPIEx = UKEPIEx.fillna(method='ffill')
UKEPIEx = Dates.merge(UKEPIEx,right_index=True,left_index=True,how='left')
UKEPIEx = UKEPIEx.fillna(method='ffill')
UKEPIEx = Month.merge(UKEPIEx,right_index=True,left_index=True,how='left')


# In[4]:


UKEPIEx.tail()


# In[5]:


# Read Bloomberg Data
Ecodata = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegBBGUpdate.xlsx').parse('Month')
Ecodata = Ecodata.iloc[2:]
Ecodata.index = Ecodata['NAME']
Ecodata = Ecodata.drop(columns = ['NAME'])
Ecodata = Ecodata.fillna(method='ffill')
Ecodata = Dates.merge(Ecodata,right_index=True,left_index=True,how='left')
Ecodata = Ecodata.fillna(method='ffill')
Ecodata = Month.merge(Ecodata,right_index=True,left_index=True,how='left')


# Read in half-yearly bak NIMs
BankData1 = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegBBGUpdate.xlsx').parse('Quarter')
BankData1 = BankData1.iloc[1:]
BankData1.index = BankData1['Unnamed: 0']
BankData1 = BankData1.drop(columns = ['Unnamed: 0'])
BankData1 = BankData1.fillna(method='ffill')
BankData1 = Dates.merge(BankData1,right_index=True,left_index=True,how='left')
BankData1 = BankData1.fillna(method='ffill')
BankData1 = Month.merge(BankData1,right_index=True,left_index=True,how='left')
Ecodata = Ecodata.merge(BankData1,right_index=True,left_index=True,how='left')

#Read in quarterly Bank NIMs
BankData2 = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegBBGUpdate.xlsx').parse('BiAnn')
BankData2 = BankData2.iloc[1:]
BankData2.index = BankData2['Unnamed: 0']
BankData2 = BankData2.drop(columns = ['Unnamed: 0'])
BankData2 = BankData2.fillna(method='ffill')
BankData2 = Dates.merge(BankData2,right_index=True,left_index=True,how='left')
BankData2 = BankData2.fillna(method='ffill')
BankData2 = Month.merge(BankData2,right_index=True,left_index=True,how='left')
Ecodata = Ecodata.merge(BankData2,right_index=True,left_index=True,how='left')


# In[6]:


# Put the codes in UP TO HERE

NamesIdx = ['EPI ex inflation M','EPI ex inflation L','EPI Synchronised Sectors or Regions M','EPI Synchronised Sectors or Regions L',
'Earnings Growth M','Earnings Growth L','Investment/Capex M','Investment/Capex L','Credit Growth M',
'Credit Growth L','Inventory Cycle M',
'Inventory Cycle L','Global Trade M','Global Trade L','Fiscal Impulse M','Fiscal Impulse L',
'EPI Inflation M','EPI Inflation L','Wage Inflation M','Wage Inflation L','Core (Persistence/Breadth) M',
'Core (Persistence/Breadth) L','OECD Output Gap M','OECD Output Gap L','Employment EPI M',
'Employment EPI L','Unemployment level vs NAIRU M','Unemployment level vs NAIRU L',
'Capacity Utilisation M','Capacity Utilisation L','Property (1y momentum) M','Property (level) L',
'Equities M','Equities L','Household Leverage M','Household Leverage L','Corporate Leverage M',
'Corporate Leverage L','Government Leverage M','Government Leverage L',
'Public Sector Liquidity ==> Reserves, Central Bank BS M','Public Sector Liquidity ==> Reserves, Central Bank BS L',
'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M',
'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L',
'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M',
'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L',
'Real Policy Rate vs real neutral rate M','Real Policy Rate vs real neutral rate L','Real Long End Yields M',
'Real Long End Yield L','Real Ccy vs ToT M','Real Ccy vs ToT L','Financial Sector Health M',
'Financial Sector Health L','Credit Spreads M','Credit Spreads L']

CodesIdx = ['UKX Index BEPS','UKIPIYOY Index','UKGVNPQY Index','UKIPDURY Index','UKIPINVY Index','C12 TCAA Index',
'B14 B3UL Index','UKGECAFU Index','UKTBTTBA Index','UKTBTTEX Index','UKTBTTIM Index','EHBBGB Index','UKGENMRP Index',
'UKAWXTOY Index','UKLBYBUS Index','LNTNUKYY Index','UKHCA9IQ Index','CPEXUKYY Index','UKEFUREP Index','OEEOGBUG Index',
'IOG%GBR Index','UKUEILOR Index','EUUCUK Index','UKNBAAYY Index','UKVSUK Index','UKX Index PE','UKLSCNC Index',
'UKLSCNCB Index','CPNFGBHG Index','CPNFGBA3 Index','UKGBQWND Index','CPNFGBNG Index','CPNFGBOG Index','FARBCRED Index',
'EBBSTOTA Index','EURUSD Curncy','BJACTOTL Index','JPYUSD Curncy','CNBMTTAS Index','CNYUSD Curncy','A23 VWYT Index',
'A23 VWYW Index','1121136 Index','USCABAL Index','ECOCEAS Index','ECOCJPN Index','ECOCCNN Index','UKBRBASE Index',
'G0022 5Y5Y BLC2 Curncy','UKRPYOY Index','USNRUK Index','USNRUS Index','USNREUAR Index','GUKG10 Index','UKGGBE10 Index',
'UKHCA9IQ Index','112.028 Index','CTOTGBP Index','HSBA LN Equity','LLOY LN Equity','BARC LN Equity','STAN LN Equity',
'LC84OAS Index',]


# In[7]:


CodesNamesIdx = ['FTSE 100 INDEX','UK Industrial Production YoY S','UK GDP Chained Gross Fixed Cap',
'UK Industrial Production Durab','UK Industrial Production Inves','BOE Facilities Granted Total t',
'BOE MFI Excl CBBS Amt OS of Fr','CBI MTE Full Volume of Stocks','UK Trade Balance Value of Trad',
'UK Trade Balance Value of Trad','UK Trade Balance Value of Trad','UK Budget Balance (% GDP)',
'UK GDP Current Market Prices G','UK AWE Regular Pay Whole Econo','UK Labor Force Survey Total We',
'Eurostat Labor Costs Nominal V','UK CPI Ex Energy Food Alcohol','Eurostat UK Core HICP YoY NSA',
'UK OBR Forecast Output Gap  %','OECD Eco Outlook UK Output Gap','IMF United Kingdom Output Gap',
'UK Unemployment ILO Unemployme','European Commission Capacity U','UK Nationwide House Price All',
'United Kingdom Land Registry S','FTSE 100 INDEX','Corporate Lending Availability','Corporate Lending Loan Covenan',
'BIS United Kingdom Credit to H','United Kingdom Total Credit to','UK Household NPI Serving House',
'BIS United Kingdom Credit to N','BIS United Kingdom Credit to G','US Factors Supplying Reserve F',
'ECB Balance Sheet All Assets','EUR-USD X-RATE','Bank of Japan assets:Total','JPY-USD X-RATE',
'China Central Bank Balance She','CNY-USD X-RATE','BOE Total M1 Amounts Outstandi','BOE Total M2 Amounts Outstandi',
'IMF UK Central Bank Monetary B','US Nominal Account Balance In','Eurozone Current Account Balan',
'Japan Current Account Balance','China Current Account Balance','UK Bank of England Official Ba',
'GBP I22 FORWARD RATE 5Y5Y','UK RPI YoY NSA','Laubach Williams Natural Rate','Laubach Williams Natural Rate',
'Laubach Williams Natural Rate','UK Govt Bonds 10 Year Note Gen','UK Breakeven 10 Year',
'UK CPI Ex Energy Food Alcohol','IMF UK Real Effective Exchange','Citi Terms of Trade Index - Un',
'HSBC HOLDINGS PLC','LLOYDS BANKING GROUP PLC','BARCLAYS PLC','STANDARD CHARTERED PLC',
'Bloomberg Barclays Sterling Ag']


# In[9]:


#Copies and Adjustments

#Main Code df
RegCode = pd.DataFrame(0,index=Month.index, columns=NamesIdx)
PXLast = pd.DataFrame(0,index=Month.index, columns=NamesIdx)

for i in Ecodata.columns:
    PXLast = PXLast.merge(Ecodata[i],right_index=True,left_index=True,how='left')


#Add in EPI
PXLast['EpiM_Headline_UK'] = UKEPIEx['EpiM_Headline_UK'].copy()
PXLast['EpiM_Business_UK'] = UKEPIEx['EpiM_Business_UK'].copy()
PXLast['EpiM_Consumer_UK'] = UKEPIEx['EpiM_Consumer_UK'].copy()
PXLast['EpiM_Employment_UK'] = UKEPIEx['EpiM_Employment_UK'].copy()
PXLast['EpiM_Growth_UK'] = UKEPIEx['EpiM_Growth_UK'].copy()
PXLast['EpiM_Inflation_UK'] = UKEPIEx['EpiM_Inflation_UK'].copy()


# In[10]:


# Start adjusting data series

#YOY%
PXLast['C12 TCAA Index'] = (PXLast['C12 TCAA Index']/PXLast['C12 TCAA Index'].shift(12) -1)
PXLast['B14 B3UL Index'] = (PXLast['B14 B3UL Index']/PXLast['B14 B3UL Index'].shift(12) -1) #both credit growth, may need avg
PXLast['UKTBTTEX Index'] = (PXLast['UKTBTTEX Index']/PXLast['UKTBTTEX Index'].shift(12) -1)
PXLast['UKTBTTIM Index'] = (PXLast['UKTBTTIM Index']/PXLast['UKTBTTIM Index'].shift(12) -1)
PXLast['UKGENMRP Index'] = (PXLast['UKGENMRP Index']/PXLast['UKGENMRP Index'].shift(12) -1)

# Unemployment vs NAIRU estimate of 4.5% for UK (OBR website)
PXLast['UKUEILOR Index'] = (PXLast['UKUEILOR Index'] - 4.5)

# Inverse for unemployment, fiscal and debt, credit spreads
PXLast['UKUEILOR Index'] = -1*PXLast['UKUEILOR Index']
PXLast['EHBBGB Index'] = -1*PXLast['EHBBGB Index']
PXLast['CPNFGBHG Index'] = -1*PXLast['CPNFGBHG Index']
PXLast['CPNFGBNG Index'] = -1*PXLast['CPNFGBNG Index']
PXLast['CPNFGBOG Index'] = -1*PXLast['CPNFGBOG Index']
PXLast['LC84OAS Index'] = -1*PXLast['LC84OAS Index']

# 12M SUm for inventories (quarterly), trade balance
PXLast['UKGECAFU Index'] = -1*PXLast['UKGECAFU Index'].rolling(4).sum()
PXLast['UKTBTTBA Index'] = PXLast['UKTBTTBA Index'].rolling(12).sum()

# Household debt as % of income - not used outside AU
#PXLast['CPNFGBA3 Index'] = (PXLast['CPNFGBA3 Index']/(PXLast['UKGBQWND Index']/1000)) #HH income adj for blns

# Central Bank Balance sheet, convert all CB B/S to USD and Sum
PXLast['FARBCRED Index'] = ((PXLast['FARBCRED Index']/1000)+(PXLast['EBBSTOTA Index']*PXLast['EURUSD Curncy'])
                            +(PXLast['BJACTOTL Index']*PXLast['JPYUSD Curncy'])
                            +(PXLast['CNBMTTAS Index']*PXLast['CNYUSD Curncy']))                            
                            
# Current Account Balance G4 Sums
PXLast['USCABAL Index'] = PXLast['USCABAL Index']+PXLast['ECOCEAS Index']+PXLast['ECOCJPN Index']+PXLast['ECOCCNN Index']

# Real Rate Calcs policy and long end, and vs neutral rate for policy
PXLast['GUKG10 Index'] = -(PXLast['UKGGBE10 Index'] - PXLast['GUKG10 Index']) #Real long end
PXLast['UKBRBASE Index'] = -1*((PXLast['UKBRBASE Index'] - (PXLast['G0022 5Y5Y BLC2 Curncy']+PXLast['UKRPYOY Index'])/2)
                        -(0.33*PXLast['USNRUS Index']+0.33*PXLast['USNREUAR Index']+0.33*PXLast['USNRUK Index'])) #Real policy rate

# Real currency v Terms of trade
PXLast['112.028 Index'] = PXLast['CTOTGBP Index']/PXLast['112.028 Index']


# In[11]:


# Z-scoring at different time frames

# 0.5Y
PXLast05Yzs = (PXLast - PXLast.rolling(6).mean())/PXLast.rolling(6).std()

#1y
PXLast1Yzs = (PXLast - PXLast.rolling(12).mean())/PXLast.rolling(12).std()

#3Y
PXLast3Yzs = (PXLast - PXLast.rolling(3*12).mean())/PXLast.rolling(3*12).std()

#5y
PXLast5Yzs = (PXLast - PXLast.rolling(5*12).mean())/PXLast.rolling(5*12).std()

#7Y
PXLast7Yzs = (PXLast - PXLast.rolling(7*12).mean())/PXLast.rolling(7*12).std()

#10Y
PXLast10Yzs = (PXLast - PXLast.rolling(10*12).mean())/PXLast.rolling(10*12).std()


# In[13]:


# Add in Adjusted Series
#Growth
RegCode['EPI ex inflation M'] = PXLast05Yzs['EpiM_Headline_UK']
RegCode['EPI ex inflation L'] = PXLast5Yzs['EpiM_Headline_UK']
RegCode['EPI Synchronised Sectors or Regions M'] = (PXLast05Yzs['EpiM_Business_UK']+PXLast05Yzs['EpiM_Consumer_UK']+
                                                    PXLast05Yzs['EpiM_Employment_UK']+PXLast05Yzs['EpiM_Growth_UK']+
                                                    PXLast05Yzs['EpiM_Inflation_UK'])/5
RegCode['EPI Synchronised Sectors or Regions L'] = (PXLast5Yzs['EpiM_Business_UK']+PXLast5Yzs['EpiM_Consumer_UK']+
                                                    PXLast5Yzs['EpiM_Employment_UK']+PXLast5Yzs['EpiM_Growth_UK']+
                                                    PXLast5Yzs['EpiM_Inflation_UK'])/5
RegCode['Earnings Growth M'] = PXLast05Yzs['UKX Index BEPS']
RegCode['Earnings Growth L'] = PXLast7Yzs['UKX Index BEPS']
RegCode['Investment/Capex M'] = (PXLast5Yzs['UKIPIYOY Index']+PXLast5Yzs['UKGVNPQY Index']+PXLast5Yzs['UKIPDURY Index']+PXLast5Yzs['UKIPINVY Index'])/4
RegCode['Investment/Capex L'] = (PXLast10Yzs['UKIPIYOY Index']+PXLast10Yzs['UKGVNPQY Index']+PXLast10Yzs['UKIPDURY Index']+PXLast10Yzs['UKIPINVY Index'])/4
RegCode['Credit Growth M'] = (PXLast1Yzs['C12 TCAA Index']+PXLast1Yzs['B14 B3UL Index'])/2
RegCode['Credit Growth L'] = (PXLast7Yzs['C12 TCAA Index']+PXLast7Yzs['B14 B3UL Index'])/2
RegCode['Inventory Cycle M'] = PXLast1Yzs['UKGECAFU Index']
RegCode['Inventory Cycle L'] = PXLast7Yzs['UKGECAFU Index']
RegCode['Global Trade M'] = (PXLast1Yzs['UKTBTTBA Index']+PXLast1Yzs['UKTBTTEX Index']+PXLast1Yzs['UKTBTTIM Index'])/3
RegCode['Global Trade L'] = (PXLast7Yzs['UKTBTTBA Index']+PXLast7Yzs['UKTBTTEX Index']+PXLast7Yzs['UKTBTTIM Index'])/3
RegCode['Fiscal Impulse M'] = (PXLast5Yzs['EHBBGB Index']+PXLast5Yzs['UKGENMRP Index'])/2
RegCode['Fiscal Impulse L'] = (PXLast10Yzs['EHBBGB Index']+PXLast10Yzs['UKGENMRP Index'])/2



# In[15]:


#Output Gap inflation
RegCode['EPI Inflation M'] = PXLast05Yzs['EpiM_Inflation_UK']
RegCode['EPI Inflation L'] = PXLast5Yzs['EpiM_Inflation_UK']
RegCode['Wage Inflation M'] = PXLast05Yzs['UKAWXTOY Index']
RegCode['Wage Inflation L'] = PXLast7Yzs['UKAWXTOY Index']
RegCode['Core (Persistence/Breadth) M'] = PXLast05Yzs['UKHCA9IQ Index']
RegCode['Core (Persistence/Breadth) L'] = PXLast7Yzs['UKHCA9IQ Index']
RegCode['OECD Output Gap M'] = PXLast5Yzs['OEEOGBUG Index']
RegCode['OECD Output Gap L'] = PXLast10Yzs['OEEOGBUG Index']
RegCode['Employment EPI M'] = PXLast5Yzs['EpiM_Employment_UK']
RegCode['Employment EPI L'] = PXLast10Yzs['EpiM_Employment_UK']
RegCode['Unemployment level vs NAIRU M'] = PXLast5Yzs['UKUEILOR Index']
RegCode['Unemployment level vs NAIRU L'] = PXLast10Yzs['UKUEILOR Index']
RegCode['Capacity Utilisation M'] = PXLast5Yzs['EUUCUK Index']
RegCode['Capacity Utilisation L'] = PXLast10Yzs['EUUCUK Index']


# In[16]:


#### Financial Conditions ###
# Capacity to borrow
RegCode['Property (1y momentum) M'] = PXLast1Yzs['UKNBAAYY Index']
RegCode['Property (level) L'] = PXLast7Yzs['UKNBAAYY Index']
RegCode['Equities M'] = PXLast05Yzs['UKX Index PE']
RegCode['Equities L'] = PXLast7Yzs['UKX Index PE']
RegCode['Household Leverage M'] = PXLast5Yzs['CPNFGBHG Index']
RegCode['Household Leverage L'] = PXLast10Yzs['CPNFGBHG Index']
RegCode['Corporate Leverage M'] = PXLast5Yzs['CPNFGBNG Index']
RegCode['Corporate Leverage L'] = PXLast10Yzs['CPNFGBNG Index']
RegCode['Government Leverage M'] = PXLast5Yzs['CPNFGBOG Index']
RegCode['Government Leverage L'] = PXLast10Yzs['CPNFGBOG Index']

# Liquidity
RegCode['Public Sector Liquidity ==> Reserves, Central Bank BS M'] = PXLast05Yzs['FARBCRED Index']
RegCode['Public Sector Liquidity ==> Reserves, Central Bank BS L'] = PXLast3Yzs['FARBCRED Index']
RegCode['Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M'] = (PXLast1Yzs['A23 VWYT Index']+PXLast1Yzs['A23 VWYW Index']+PXLast1Yzs['1121136 Index'])/3
RegCode['Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L'] = (PXLast7Yzs['A23 VWYT Index']+PXLast7Yzs['A23 VWYW Index']+PXLast7Yzs['1121136 Index'])/3
RegCode['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M'] = PXLast5Yzs['USCABAL Index']
RegCode['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L'] = PXLast10Yzs['USCABAL Index']

# Funding Costs
RegCode['Real Policy Rate vs real neutral rate M'] = PXLast05Yzs['UKBRBASE Index']
RegCode['Real Policy Rate vs real neutral rate L'] = PXLast5Yzs['UKBRBASE Index']
RegCode['Real Long End Yields M'] = PXLast05Yzs['GUKG10 Index']
RegCode['Real Long End Yield L'] = PXLast5Yzs['GUKG10 Index']
RegCode['Real Ccy vs ToT M'] = PXLast1Yzs['112.028 Index']
RegCode['Real Ccy vs ToT L'] = PXLast7Yzs['112.028 Index']
RegCode['Financial Sector Health M'] = (PXLast1Yzs['HSBA LN Equity']+PXLast1Yzs['LLOY LN Equity']+PXLast1Yzs['BARC LN Equity']+PXLast1Yzs['STAN LN Equity'])/4
RegCode['Financial Sector Health L'] = (PXLast7Yzs['HSBA LN Equity']+PXLast7Yzs['LLOY LN Equity']+PXLast7Yzs['BARC LN Equity']+PXLast7Yzs['STAN LN Equity'])/4
RegCode['Credit Spreads M'] = PXLast05Yzs['LC84OAS Index']
RegCode['Credit Spreads L'] = PXLast3Yzs['LC84OAS Index']


# In[17]:


# Frame conditioning logic

ConditionFrame = pd.DataFrame(RegCode.copy())
for i in (ConditionFrame.columns):
    for j in (ConditionFrame.index):
        if (ConditionFrame.loc[j,i] < -1):
            ConditionFrame.loc[j,i] = 0
        elif (ConditionFrame.loc[j,i] > 1):
            ConditionFrame.loc[j,i] = 2
        else:
            ConditionFrame.loc[j,i] = 1
            

ConditionFrame.tail()


# In[18]:


# Create Growth Momentum Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
GrowthMPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

GrowthMCols = ['EPI ex inflation M','EPI Synchronised Sectors or Regions M','Earnings Growth M',
              'Investment/Capex M','Credit Growth M','Inventory Cycle M','Global Trade M','Fiscal Impulse M']

for i in GrowthMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            GrowthMPerc.loc[j,'Reflation'] += 1
            GrowthMPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            GrowthMPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            GrowthMPerc.loc[j,'Downturn'] += 1
            GrowthMPerc.loc[j,'Recession'] += 1

# Growth Momentum Frame Percentages            
GrowthMPerc = GrowthMPerc/len(GrowthMCols)

GrowthMPerc.tail()


# In[19]:


# Create Growth Level Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
GrowthLPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

GrowthLCols = ['EPI ex inflation L','EPI Synchronised Sectors or Regions L','Earnings Growth L',
              'Investment/Capex L','Credit Growth L','Inventory Cycle L','Global Trade L','Fiscal Impulse L']

for i in GrowthLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            GrowthLPerc.loc[j,'Downturn'] += 1
            GrowthLPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            GrowthLPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            GrowthLPerc.loc[j,'Recession'] += 1
            GrowthLPerc.loc[j,'Reflation'] += 1

# Growth Momentum Frame Percentages            
GrowthLPerc = GrowthLPerc/len(GrowthLCols)


GrowthLPerc.tail()


# In[20]:


# Create Inflation Momentum Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
InflationMPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

InflationMCols = ['EPI Inflation M','Wage Inflation M','Core (Persistence/Breadth) M','OECD Output Gap M',
              'Employment EPI M','Unemployment level vs NAIRU M','Capacity Utilisation M']

for i in InflationMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            InflationMPerc.loc[j,'Mid Cycle'] += 1
            InflationMPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            InflationMPerc.loc[j,'Downturn'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            InflationMPerc.loc[j,'Recession'] += 1
            
            

# Growth Momentum Frame Percentages            
InflationMPerc = InflationMPerc/len(InflationMCols)


InflationMPerc.tail()


# In[21]:


# Create Inflation Level Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
InflationLPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

InflationLCols = ['EPI Inflation L','Wage Inflation L','Core (Persistence/Breadth) L','OECD Output Gap L',
              'Employment EPI L','Unemployment level vs NAIRU L','Capacity Utilisation L']

for i in InflationLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            InflationLPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            InflationLPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            InflationLPerc.loc[j,'Recession'] += 1
            InflationLPerc.loc[j,'Reflation'] += 1
            

# Growth Momentum Frame Percentages            
InflationLPerc = InflationLPerc/len(InflationLCols)


InflationLPerc.tail()


# In[22]:


# Create Financial Conditions Momentum Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
FinCondMPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

FinCondMCols = ['Property (1y momentum) M','Equities M','Household Leverage M','Corporate Leverage M',
               'Government Leverage M','Public Sector Liquidity ==> Reserves, Central Bank BS M',
               'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M',
               'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M',
               'Real Policy Rate vs real neutral rate M','Real Long End Yields M','Real Ccy vs ToT M',
               'Financial Sector Health M','Credit Spreads M']

for i in FinCondMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            FinCondMPerc.loc[j,'Reflation'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            FinCondMPerc.loc[j,'Mid Cycle'] += 1
            FinCondMPerc.loc[j,'Downturn'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            FinCondMPerc.loc[j,'Late Cycle'] += 1
            

# Growth Momentum Frame Percentages            
FinCondMPerc = FinCondMPerc/len(FinCondMCols)


FinCondMPerc.tail()


# In[23]:


# Create Financial Conditions Level Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
FinCondLPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

FinCondLCols = ['Property (level) L','Equities L','Household Leverage L','Corporate Leverage L',
               'Government Leverage L','Public Sector Liquidity ==> Reserves, Central Bank BS L',
               'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L',
               'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L',
               'Real Policy Rate vs real neutral rate L','Real Long End Yields M','Real Ccy vs ToT L',
               'Financial Sector Health L','Credit Spreads L']

for i in FinCondLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            FinCondLPerc.loc[j,'Reflation'] += 1
            FinCondLPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            FinCondLPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            FinCondLPerc.loc[j,'Recession'] += 1
            

# Growth Momentum Frame Percentages            
FinCondLPerc = FinCondLPerc/len(FinCondLCols)


FinCondLPerc.head()


# In[24]:


# Generate overall probabilities
# Loop makes 0s into 0.0001s to avoid totally nullifying regime outcomes

GrowthProb = GrowthMPerc*GrowthLPerc
for i in GrowthProb.columns:
    for j in GrowthProb.index:
        if GrowthProb.loc[j,i] < 0.01:
            GrowthProb.loc[j,i] = 0.0001
InflationProb = InflationMPerc*InflationLPerc
for i in InflationProb.columns:
    for j in InflationProb.index:
        if InflationProb.loc[j,i] < 0.01:
            InflationProb.loc[j,i] = 0.0001
FinCondProb = FinCondMPerc*FinCondLPerc
for i in FinCondProb.columns:
    for j in FinCondProb.index:
        if FinCondProb.loc[j,i] < 0.01:
            FinCondProb.loc[j,i] = 0.0001

# Adjust for blank regimes
InflationProb['Downturn'] = InflationMPerc['Downturn']*InflationMPerc['Downturn']
InflationProb['Reflation'] = InflationLPerc['Reflation']*InflationLPerc['Reflation']
FinCondProb['Downturn'] = FinCondMPerc['Downturn']*FinCondMPerc['Downturn']
FinCondProb['Recession'] = FinCondLPerc['Recession']*FinCondLPerc['Recession']

GrowthProb.tail()


# In[25]:


InflationProb.tail()


# In[26]:


FinCondProb.tail()


# In[28]:


# Calculate the total probabilities

#TotalProb = GrowthProb*((InflationProb+FinCondProb)/2) #higher dominance to Growth
TotalProb = GrowthProb*InflationProb*FinCondProb

#Calc in 100%
GrowthProb[RegCols] = GrowthProb[RegCols].div(GrowthProb[RegCols].sum(axis=1), axis=0).multiply(100)
InflationProb[RegCols] = InflationProb[RegCols].div(InflationProb[RegCols].sum(axis=1), axis=0).multiply(100)
FinCondProb[RegCols] = FinCondProb[RegCols].div(FinCondProb[RegCols].sum(axis=1), axis=0).multiply(100)
TotalProb[RegCols] = TotalProb[RegCols].div(TotalProb[RegCols].sum(axis=1), axis=0).multiply(100)

GrowthProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\UKQuantReg\UKRegGrowthProbability.xlsx')
InflationProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\UKQuantReg\UKRegInflationProbability.xlsx')
FinCondProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\UKQuantReg\UKRegFinancialCondProbability.xlsx')
TotalProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\UKQuantReg\UKRegTotalProbability.xlsx')
RegCode.transpose().to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\UKQuantReg\UKRegimeFullScoring.xlsx')


# In[25]:





# In[ ]:




