#!/usr/bin/env python
# coding: utf-8

# In[17]:


#Imports
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import datetime as dt
from pandas.tseries.offsets import *
import numpy as np
import scipy.stats as scs

import matplotlib as mpl
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.dates import YearLocator, MonthLocator



# In[18]:


# Getting BBG data and cleaning
StartDate = '1995-12-30'
EndDate = (dt.datetime.today()+BDay(30)).strftime("%m/%d/%Y")

Dates = pd.DataFrame(pd.date_range(StartDate,EndDate,freq="D"),columns=['Date'])
Dates = Dates.set_index('Date',drop=True)
Dates.index.name = None

Month = pd.DataFrame(pd.date_range(StartDate,EndDate,freq="M"),columns=['Date'])
Month = Month.set_index('Date',drop=True)
Month.index.name = None

Quarter = pd.DataFrame(pd.date_range(StartDate,EndDate,freq="Q"),columns=['Date'])
Quarter = Quarter.set_index('Date',drop=True)
Quarter.index.name = None


# In[19]:


# Take EPI data from excel

# Read EPI
EurEPIEx = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegEPI.xlsx').parse('MacroRegEPI')
EurEPIEx.index = EurEPIEx['Date']
EurEPIEx = EurEPIEx.drop(columns = ['Date'])
EurEPIEx = EurEPIEx.fillna(method='ffill')
EurEPIEx = Dates.merge(EurEPIEx,right_index=True,left_index=True,how='left')
EurEPIEx = EurEPIEx.fillna(method='ffill')
EurEPIEx = Month.merge(EurEPIEx,right_index=True,left_index=True,how='left')


# In[20]:


EurEPIEx.tail()


# In[25]:


# Take Bloomberg data from excel

# Read Bloomberg Data
Ecodata = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegBBGUpdate.xlsx').parse('Month')
Ecodata = Ecodata.iloc[2:]
Ecodata.index = Ecodata['NAME']
Ecodata = Ecodata.drop(columns = ['NAME'])
Ecodata = Ecodata.fillna(method='ffill')
Ecodata = Dates.merge(Ecodata,right_index=True,left_index=True,how='left')
Ecodata = Ecodata.fillna(method='ffill')
Ecodata = Month.merge(Ecodata,right_index=True,left_index=True,how='left')


# Read in half-yearly bak NIMs
BankData1 = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegBBGUpdate.xlsx').parse('Quarter')
BankData1 = BankData1.iloc[1:]
BankData1.index = BankData1['Unnamed: 0']
BankData1 = BankData1.drop(columns = ['Unnamed: 0'])
BankData1 = BankData1.fillna(method='ffill')
BankData1 = Dates.merge(BankData1,right_index=True,left_index=True,how='left')
BankData1 = BankData1.fillna(method='ffill')
BankData1 = Month.merge(BankData1,right_index=True,left_index=True,how='left')
Ecodata = Ecodata.merge(BankData1,right_index=True,left_index=True,how='left')

#Read in quarterly Bank NIMs
BankData2 = pd.ExcelFile(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\MacroRegBBGUpdate.xlsx').parse('BiAnn')
BankData2 = BankData2.iloc[1:]
BankData2.index = BankData2['Unnamed: 0']
BankData2 = BankData2.drop(columns = ['Unnamed: 0'])
BankData2 = BankData2.fillna(method='ffill')
BankData2 = Dates.merge(BankData2,right_index=True,left_index=True,how='left')
BankData2 = BankData2.fillna(method='ffill')
BankData2 = Month.merge(BankData2,right_index=True,left_index=True,how='left')
Ecodata = Ecodata.merge(BankData2,right_index=True,left_index=True,how='left')


# In[29]:


# Put the codes in 

NamesIdx = ['EPI ex inflation M','EPI ex inflation L','EPI Synchronised Sectors or Regions M','EPI Synchronised Sectors or Regions L',
'Earnings Growth M','Earnings Growth L','Investment/Capex M','Investment/Capex L','Credit Growth M',
'Credit Growth L','Inventory Cycle M',
'Inventory Cycle L','Global Trade M','Global Trade L','Fiscal Impulse M','Fiscal Impulse L',
'EPI Inflation M','EPI Inflation L','Wage Inflation M','Wage Inflation L','Core (Persistence/Breadth) M',
'Core (Persistence/Breadth) L','OECD Output Gap M','OECD Output Gap L','Employment EPI M',
'Employment EPI L','Unemployment level vs NAIRU M','Unemployment level vs NAIRU L',
'Capacity Utilisation M','Capacity Utilisation L','Property (1y momentum) M','Property (level) L',
'Equities M','Equities L','Household Leverage M','Household Leverage L','Corporate Leverage M',
'Corporate Leverage L','Government Leverage M','Government Leverage L',
'Public Sector Liquidity ==> Reserves, Central Bank BS M','Public Sector Liquidity ==> Reserves, Central Bank BS L',
'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M',
'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L',
'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M',
'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L',
'Real Policy Rate vs real neutral rate M','Real Policy Rate vs real neutral rate L','Real Long End Yields M',
'Real Long End Yield L','Real Ccy vs ToT M','Real Ccy vs ToT L','Financial Sector Health M',
'Financial Sector Health L','Credit Spreads M','Credit Spreads L']

CodesIdx = ['SXXP Index BEPS','GEIOYY Index','GRGDGCIY Index','FPIPYOY Index',
'FRGEFCAY Index','EUIPITYY Index','ITPIRCYS Index',
'EUIPESYY Index','SPNAGFCZ Index','GLNBL1CB Index','FRFIPRVS Index','GEIFOMIA Index',
'INSEINVE Index',
'GRBTEXYY Index','GRBTIMYY Index','FRTEEXPY Index','FRTEIMPY Index',
'ITTRESA Index','ITTRIMSA Index','SPTBEUEX Index','SPTBEUIM Index','EUBDGERM Index','GRGDGCY Index',
'EUBDFRAN Index','FRGEGCOY Index',
'EUBDITAL Index','ITPIRUYS Index','EUBDSPAI Index','SPNAFCGZ Index',
'GRHIWAYY Index','LNTWFRY Index','ITNHYOY Index','LNTWESY Index','GRCP2HYY Index','CPEXFRYY Index',
'ITCQXEUY Index','SPIPCCYY Index','OEEODEUG Index','OEEOFRUG Index','OEEOITUG Index','OEEOESUG Index',
'GRUEPR Index','BCMPNRDE Index','FRUEREU Index','BCMPNRFR Index','UMRTIT Index','BCMPNRIT Index',
'UMRTES Index','BCMPNRES Index','EUUCDE Index','EUUCFR Index','EUUCIT Index','EUUCES Index',
'HOPIDEYY Index','FRHPIYOY Index','ITHPGYY Index','SPNPTHY Index','SXXP Index PE','CPNFXMHG Index',
'CPNFXMNG Index','CPNFXMOG Index','FARBCRED Index','EBBSTOTA Index','EURUSD Curncy','BJACTOTL Index',
'JPYUSD Curncy','CNBMTTAS Index','CNYUSD Curncy','ECMSM1Y Index','ECMSM2Y Index','ECMAM3YY Index',
'USCABAL Index','ECOCEAS Index','ECOCJPN Index','ECOCCNN Index','EUORDEPO Index','FWISEU55 Index',
'CPTFEMUY Index','USNREUAR Index','USNRUS Index','GECU30YR Index','163.028 Index','CTOTEUR Index',
'CBK GR Equity','DBK GR Equity','UCG IM Equity','ISP IM Equity',
'SAN SM Equity','CABK SM Equity','LP05OAS Index']



# In[30]:


CodesNamesIdx = ['STXE 600 (EUR) Pr','Germany Manufacturing Orders Y','Germany GDP Chain Linked Gross',
'France Industrial Production S','France GDP Chain Linked Prices','Eurostat Industrial Production',
'Italy Real GDP Consumption YoY','Eurostat Industrial Production','Spain Real Chained GDP Prices',
'#N/A Invalid Security','France MFI Loans to Private Se','Markit Germany Manufacturing O',
'Markit France Manufacturing Or','Markit Italy Manufacturing Ord','Markit Spain Manufacturing Ord',
'Germany Foreign Trade - Export','Germany Foreign Trade - Import','France Trade Balance Exports Y',
'France Trade Balance Imports Y','Italy Exports SA','Italy Imports SA','Spain Trade Balance Exports EU',
'Spain Trade Balance Imports EU','Eurostat Germany Budget Defici','Germany GDP Chain Linked Gover',
'Eurostat France Budget Deficit','France GDP Chain Linked Prices','Eurostat Italy Budget Deficit',
'Italy Real GDP Public Consumpt','Eurostat Spain Budget Deficit','Spain Real Chained GDP Prices',
'Germany Household Income - Gro','France Hourly Wage Index YoY','Italy Hourly Wages YoY NSA',
'Eurostat Labor Costs Nominal W','Germany HICP All Items YoY','Eurostat France Core HICP YoY',
'Italy HICP Overall Index excl','Spain CPI Core YoY','OECD Eco Outlook Germany Outpu',
'OECD Eco Outlook France Output','OECD Eco Outlook Italy Output','OECD Eco Outlook Spain Output',
'Germany Unemployment Rate SWDA','Bloomberg Economics Germany NA','France Unemployment Rate ILO M',
'Bloomberg Economics France NAI','Eurostat Unemployment Italy SA','Bloomberg Economics Italy NAIR',
'Eurostat Unemployment Spain SA','Bloomberg Economics Spain NAIR','European Commission Capacity U',
'European Commission Capacity U','European Commission Capacity U','European Commission Capacity U',
'Eurostat House Price Index Ger','France House Price Index YoY','Italy ISI Property Price Gener',
'Spain House Prices for Total h','BIS Euro Area Credit to Househ',
'BIS Euro Area Credit to Non Fi','BIS Euro Area Credit to Genera','US Factors Supplying Reserve F',
'ECB Balance Sheet All Assets','EUR-USD X-RATE','Bank of Japan assets:Total','JPY-USD X-RATE',
'China Central Bank Balance She','CNY-USD X-RATE','ECB Money Aggregates M1 YoY','ECB Money Aggregates M2 YoY',
'ECB M3 Annual Growth Rate SA','US Nominal Account Balance In','Eurozone Current Account Balan','Japan Current Account Balance',
'China Current Account Balance','ECB Deposit Facility Announcem','EUR Inflation Swap Forward 5Y5',
'Eurostat Eurozone HICP Ex Toba','USNREUAR Index','USNRUS Index','Euro Generic Govt Bond 30 Year',
'IMF Euro Area Real Effective E','Citi Terms of Trade Index - Eu','COMMERZBANK AG',
'DEUTSCHE BANK AG-REGISTERED','CREDIT AGRICOLE SA','SOCIETE GENERALE SA','UNICREDIT SPA',
'INTESA SANPAOLO','BANCO SANTANDER SA','CAIXABANK SA','Bloomberg Barclays Pan Europea']

GDPNames = ['Germany','France','Italy','Spain']
GDPIdx = ['EUACDE Index','EUACFR Index','EUACIT Index','EUACES Index']


# In[36]:


#Copies and Adjustments

#Main Code df
RegCode = pd.DataFrame(0,index=Month.index, columns=NamesIdx)
PXLast = pd.DataFrame(0,index=Month.index, columns=['Blank'])
for i in CodesIdx:
    PXLast = PXLast.merge(pd.DataFrame(Ecodata[[i]]),right_index=True,left_index=True,how='left')
PXLast = PXLast.drop(columns=['Blank'])

#Add in EPI
PXLast['EpiM_Headline_EU'] = EurEPIEx['EpiM_Headline_EU'].copy()
PXLast['EpiM_Business_EU'] = EurEPIEx['EpiM_Business_EU'].copy()
PXLast['EpiM_Consumer_EU'] = EurEPIEx['EpiM_Consumer_EU'].copy()
PXLast['EpiM_Employment_EU'] = EurEPIEx['EpiM_Employment_EU'].copy()
PXLast['EpiM_Growth_EU'] = EurEPIEx['EpiM_Growth_EU'].copy()
PXLast['EpiM_Inflation_EU'] = EurEPIEx['EpiM_Inflation_EU'].copy()

# Create GDP Weights
GDPNomAnn = pd.DataFrame(0,index=Month.index, columns=['Blank'])
for i in GDPIdx:
    GDPNomAnn = GDPNomAnn.merge(pd.DataFrame(Ecodata[[i]]),right_index=True,left_index=True,how='left')
GDPNomAnn = GDPNomAnn.drop(columns=['Blank'])
GDPNomAnn.columns = GDPNames
GDPNomAnn[GDPNames] = GDPNomAnn[GDPNames].div(GDPNomAnn[GDPNames].sum(axis=1), axis=0)


# In[37]:


GDPNomAnn.tail()


# In[38]:


# Start adjusting data series

#YOY%
PXLast['GLNBL1CB Index'] = (PXLast['GLNBL1CB Index']/PXLast['GLNBL1CB Index'].shift(12) -1)
PXLast['FRFIPRVS Index'] = (PXLast['FRFIPRVS Index']/PXLast['FRFIPRVS Index'].shift(12) -1)
PXLast['ITTRESA Index'] = (PXLast['ITTRESA Index']/PXLast['ITTRESA Index'].shift(12) -1)
PXLast['ITTRIMSA Index'] = (PXLast['ITTRIMSA Index']/PXLast['ITTRIMSA Index'].shift(12) -1)
PXLast['SPTBEUEX Index'] = (PXLast['SPTBEUEX Index']/PXLast['SPTBEUEX Index'].shift(12) -1)
PXLast['SPTBEUIM Index'] = (PXLast['SPTBEUIM Index']/PXLast['SPTBEUIM Index'].shift(12) -1)

# Unemployment vs BBG Economic's NAIRU estimate
PXLast['GRUEPR Index'] = (PXLast['GRUEPR Index'] - PXLast['BCMPNRDE Index'])
PXLast['FRUEREU Index'] = (PXLast['FRUEREU Index'] - PXLast['BCMPNRFR Index'])
PXLast['UMRTIT Index'] = (PXLast['UMRTIT Index'] - PXLast['BCMPNRIT Index'])
PXLast['UMRTES Index'] = (PXLast['UMRTES Index'] - PXLast['BCMPNRES Index'])

# Inverse for fiscal, unemployment and debt
PXLast['EUBDGERM Index'] = -1*PXLast['EUBDGERM Index']
PXLast['EUBDFRAN Index'] = -1*PXLast['EUBDFRAN Index']
PXLast['EUBDITAL Index'] = -1*PXLast['EUBDITAL Index']
PXLast['EUBDSPAI Index'] = -1*PXLast['EUBDSPAI Index']
PXLast['GRUEPR Index'] = -1*PXLast['GRUEPR Index']
PXLast['FRUEREU Index'] = -1*PXLast['FRUEREU Index']
PXLast['UMRTIT Index'] = -1*PXLast['UMRTIT Index']
PXLast['UMRTES Index'] = -1*PXLast['UMRTES Index']
PXLast['CPNFXMHG Index'] = -1*PXLast['CPNFXMHG Index']
PXLast['CPNFXMNG Index'] = -1*PXLast['CPNFXMNG Index']
PXLast['CPNFXMOG Index'] = -1*PXLast['CPNFXMOG Index']
PXLast['LP05OAS Index'] = -1*PXLast['LP05OAS Index']



# Central Bank Balance sheet, convert all CB B/S to USD and Sum
PXLast['FARBCRED Index'] = ((PXLast['FARBCRED Index']/1000)+(PXLast['EBBSTOTA Index']*PXLast['EURUSD Curncy'])
                            +(PXLast['BJACTOTL Index']*PXLast['JPYUSD Curncy'])
                            +(PXLast['CNBMTTAS Index']*PXLast['CNYUSD Curncy']))                            
                            
# Current Account Balance G4 Sums
PXLast['USCABAL Index'] = PXLast['USCABAL Index']+PXLast['ECOCEAS Index']+PXLast['ECOCJPN Index']+PXLast['ECOCCNN Index']

# Real Rate Calcs policy and long end, and vs neutral rate for policy
PXLast['EUORDEPO Index'] = ((PXLast['USNREUAR Index']*0.7 + PXLast['USNRUS Index']*0.3) 
                            -(PXLast['EUORDEPO Index'] - (PXLast['FWISEU55 Index']+PXLast['CPTFEMUY Index'])/2))
PXLast['GECU30YR Index'] = (PXLast['FWISEU55 Index']-PXLast['GECU30YR Index'])
                          

# Real currency v Terms of trade
PXLast['163.028 Index'] = PXLast['CTOTEUR Index']/PXLast['163.028 Index']



# In[39]:


# Z-scoring at different time frames

# 0.5Y
PXLast05Yzs = (PXLast - PXLast.rolling(6).mean())/PXLast.rolling(6).std()

#1y
PXLast1Yzs = (PXLast - PXLast.rolling(12).mean())/PXLast.rolling(12).std()

#3Y
PXLast3Yzs = (PXLast - PXLast.rolling(3*12).mean())/PXLast.rolling(3*12).std()

#5y
PXLast5Yzs = pd.DataFrame((PXLast - PXLast.rolling(5*12).mean())/PXLast.rolling(5*12).std())

#7Y
PXLast7Yzs = (PXLast - PXLast.rolling(7*12).mean())/PXLast.rolling(7*12).std()

#10Y
PXLast10Yzs = (PXLast - PXLast.rolling(10*12).mean())/PXLast.rolling(10*12).std()


# In[41]:


# Add in Adjusted Series
#Growth
RegCode['EPI ex inflation M'] = PXLast05Yzs['EpiM_Headline_EU']
RegCode['EPI ex inflation L'] = PXLast5Yzs['EpiM_Headline_EU']
RegCode['EPI Synchronised Sectors or Regions M'] = (PXLast05Yzs['EpiM_Business_EU']+PXLast05Yzs['EpiM_Consumer_EU']+
                                                    PXLast05Yzs['EpiM_Employment_EU']+PXLast05Yzs['EpiM_Growth_EU']+
                                                    PXLast05Yzs['EpiM_Inflation_EU'])/5
RegCode['EPI Synchronised Sectors or Regions L'] = (PXLast5Yzs['EpiM_Business_EU']+PXLast5Yzs['EpiM_Consumer_EU']+
                                                    PXLast5Yzs['EpiM_Employment_EU']+PXLast5Yzs['EpiM_Growth_EU']+
                                                    PXLast5Yzs['EpiM_Inflation_EU'])/5
RegCode['Earnings Growth M'] = PXLast05Yzs['SXXP Index BEPS']
RegCode['Earnings Growth L'] = PXLast7Yzs['SXXP Index BEPS']
RegCode['Investment/Capex M'] = ((GDPNomAnn['Germany']*((PXLast5Yzs['GEIOYY Index']+PXLast5Yzs['GRGDGCIY Index'])/2))
                                 +(GDPNomAnn['France']*((PXLast5Yzs['FPIPYOY Index']+PXLast5Yzs['FRGEFCAY Index'])/2))
                                 +(GDPNomAnn['Italy']*((PXLast5Yzs['EUIPITYY Index']+PXLast5Yzs['ITPIRCYS Index'])/2))
                                 +(GDPNomAnn['Spain']*((PXLast5Yzs['EUIPESYY Index']+PXLast5Yzs['SPNAGFCZ Index'])/2)))
RegCode['Investment/Capex L'] = ((GDPNomAnn['Germany']*((PXLast10Yzs['GEIOYY Index']+PXLast10Yzs['GRGDGCIY Index'])/2))
                                 +(GDPNomAnn['France']*((PXLast10Yzs['FPIPYOY Index']+PXLast10Yzs['FRGEFCAY Index'])/2))
                                 +(GDPNomAnn['Italy']*((PXLast10Yzs['EUIPITYY Index']+PXLast10Yzs['ITPIRCYS Index'])/2))
                                 +(GDPNomAnn['Spain']*((PXLast10Yzs['EUIPESYY Index']+PXLast10Yzs['SPNAGFCZ Index'])/2)))
RegCode['Credit Growth M'] = (PXLast1Yzs['GLNBL1CB Index']+PXLast1Yzs['FRFIPRVS Index'])/2
RegCode['Credit Growth L'] = (PXLast7Yzs['GLNBL1CB Index']+PXLast7Yzs['FRFIPRVS Index'])/2
RegCode['Inventory Cycle M'] = (PXLast05Yzs['GEIFOMIA Index']+PXLast05Yzs['INSEINVE Index'])/2
RegCode['Inventory Cycle L'] = (PXLast3Yzs['GEIFOMIA Index']+PXLast3Yzs['INSEINVE Index'])/2
RegCode['Global Trade M'] = ((GDPNomAnn['Germany']*((PXLast1Yzs['GEIOYY Index']+PXLast1Yzs['GRGDGCIY Index'])/2))
                             +(GDPNomAnn['France']*((PXLast1Yzs['FPIPYOY Index']+PXLast1Yzs['FRGEFCAY Index'])/2))
                             +(GDPNomAnn['Italy']*((PXLast1Yzs['EUIPITYY Index']+PXLast1Yzs['ITPIRCYS Index'])/2))
                             +(GDPNomAnn['Spain']*((PXLast1Yzs['EUIPESYY Index']+PXLast1Yzs['SPNAGFCZ Index'])/2)))
RegCode['Global Trade L'] = ((GDPNomAnn['Germany']*((PXLast7Yzs['GEIOYY Index']+PXLast7Yzs['GRGDGCIY Index'])/2))
                             +(GDPNomAnn['France']*((PXLast7Yzs['FPIPYOY Index']+PXLast7Yzs['FRGEFCAY Index'])/2))
                             +(GDPNomAnn['Italy']*((PXLast7Yzs['EUIPITYY Index']+PXLast7Yzs['ITPIRCYS Index'])/2))
                             +(GDPNomAnn['Spain']*((PXLast7Yzs['EUIPESYY Index']+PXLast7Yzs['SPNAGFCZ Index'])/2)))
RegCode['Fiscal Impulse M'] = ((GDPNomAnn['Germany']*((PXLast5Yzs['EUBDGERM Index']+PXLast5Yzs['GRGDGCY Index'])/2))
                               +(GDPNomAnn['France']*((PXLast5Yzs['EUBDFRAN Index']+PXLast5Yzs['FRGEGCOY Index'])/2))
                               +(GDPNomAnn['Italy']*((PXLast5Yzs['EUBDITAL Index']+PXLast5Yzs['ITPIRUYS Index'])/2))
                               +(GDPNomAnn['Spain']*((PXLast5Yzs['EUBDSPAI Index']+PXLast5Yzs['SPNAFCGZ Index'])/2)))
RegCode['Fiscal Impulse L'] = ((GDPNomAnn['Germany']*((PXLast10Yzs['EUBDGERM Index']+PXLast10Yzs['GRGDGCY Index'])/2))
                               +(GDPNomAnn['France']*((PXLast10Yzs['EUBDFRAN Index']+PXLast10Yzs['FRGEGCOY Index'])/2))
                               +(GDPNomAnn['Italy']*((PXLast10Yzs['EUBDITAL Index']+PXLast10Yzs['ITPIRUYS Index'])/2))
                               +(GDPNomAnn['Spain']*((PXLast10Yzs['EUBDSPAI Index']+PXLast10Yzs['SPNAFCGZ Index'])/2)))




# In[42]:


#Output Gap inflation
RegCode['EPI Inflation M'] = PXLast05Yzs['EpiM_Inflation_EU']
RegCode['EPI Inflation L'] = PXLast5Yzs['EpiM_Inflation_EU']
RegCode['Wage Inflation M'] = (GDPNomAnn['Germany']*PXLast05Yzs['GRHIWAYY Index']
                                +GDPNomAnn['France']*PXLast05Yzs['LNTWFRY Index']
                                +GDPNomAnn['Italy']*PXLast05Yzs['ITNHYOY Index']
                                +GDPNomAnn['Spain']*PXLast05Yzs['LNTWESY Index'])
RegCode['Wage Inflation L'] = (GDPNomAnn['Germany']*PXLast7Yzs['GRHIWAYY Index']
                                +GDPNomAnn['France']*PXLast7Yzs['LNTWFRY Index']
                                +GDPNomAnn['Italy']*PXLast7Yzs['ITNHYOY Index']
                                +GDPNomAnn['Spain']*PXLast7Yzs['LNTWESY Index'])
RegCode['Core (Persistence/Breadth) M'] = (GDPNomAnn['Germany']*PXLast05Yzs['GRCP2HYY Index']
                                           +GDPNomAnn['France']*PXLast05Yzs['CPEXFRYY Index']
                                           +GDPNomAnn['Italy']*PXLast05Yzs['ITCQXEUY Index']
                                           +GDPNomAnn['Spain']*PXLast05Yzs['SPIPCCYY Index'])
RegCode['Core (Persistence/Breadth) L'] = (GDPNomAnn['Germany']*PXLast7Yzs['GRCP2HYY Index']
                                           +GDPNomAnn['France']*PXLast7Yzs['CPEXFRYY Index']
                                           +GDPNomAnn['Italy']*PXLast7Yzs['ITCQXEUY Index']
                                           +GDPNomAnn['Spain']*PXLast7Yzs['SPIPCCYY Index'])
RegCode['OECD Output Gap M'] = (GDPNomAnn['Germany']*PXLast5Yzs['OEEODEUG Index']
                                           +GDPNomAnn['France']*PXLast5Yzs['OEEOFRUG Index']
                                           +GDPNomAnn['Italy']*PXLast5Yzs['OEEOITUG Index']
                                           +GDPNomAnn['Spain']*PXLast5Yzs['OEEOESUG Index'])
RegCode['OECD Output Gap L'] = (GDPNomAnn['Germany']*PXLast10Yzs['OEEODEUG Index']
                                           +GDPNomAnn['France']*PXLast10Yzs['OEEOFRUG Index']
                                           +GDPNomAnn['Italy']*PXLast10Yzs['OEEOITUG Index']
                                           +GDPNomAnn['Spain']*PXLast10Yzs['OEEOESUG Index'])
RegCode['Employment EPI M'] = PXLast5Yzs['EpiM_Employment_EU']
RegCode['Employment EPI L'] = PXLast10Yzs['EpiM_Employment_EU']
RegCode['Unemployment level vs NAIRU M'] = (GDPNomAnn['Germany']*PXLast5Yzs['GRUEPR Index']
                                            +GDPNomAnn['France']*PXLast5Yzs['FRUEREU Index']
                                            +GDPNomAnn['Italy']*PXLast5Yzs['UMRTIT Index']
                                            +GDPNomAnn['Spain']*PXLast5Yzs['UMRTES Index'])
RegCode['Unemployment level vs NAIRU L'] = (GDPNomAnn['Germany']*PXLast10Yzs['GRUEPR Index']
                                            +GDPNomAnn['France']*PXLast10Yzs['FRUEREU Index']
                                            +GDPNomAnn['Italy']*PXLast10Yzs['UMRTIT Index']
                                            +GDPNomAnn['Spain']*PXLast10Yzs['UMRTES Index'])
RegCode['Capacity Utilisation M'] = (GDPNomAnn['Germany']*PXLast5Yzs['GRUEPR Index']
                                            +GDPNomAnn['France']*PXLast5Yzs['EUUCDE Index']
                                            +GDPNomAnn['Italy']*PXLast5Yzs['EUUCIT Index']
                                            +GDPNomAnn['Spain']*PXLast5Yzs['EUUCES Index'])
RegCode['Capacity Utilisation L'] = (GDPNomAnn['Germany']*PXLast10Yzs['GRUEPR Index']
                                            +GDPNomAnn['France']*PXLast10Yzs['EUUCDE Index']
                                            +GDPNomAnn['Italy']*PXLast10Yzs['EUUCIT Index']
                                            +GDPNomAnn['Spain']*PXLast10Yzs['EUUCES Index'])



# In[43]:


#### Financial Conditions ###
# Capacity to borrow
RegCode['Property (1y momentum) M'] = (GDPNomAnn['Germany']*PXLast1Yzs['HOPIDEYY Index']
                                            +GDPNomAnn['France']*PXLast1Yzs['FRHPIYOY Index']
                                            +GDPNomAnn['Italy']*PXLast1Yzs['ITHPGYY Index']
                                            +GDPNomAnn['Spain']*PXLast1Yzs['SPNPTHY Index'])
RegCode['Property (level) L'] = (GDPNomAnn['Germany']*PXLast7Yzs['HOPIDEYY Index']
                                            +GDPNomAnn['France']*PXLast7Yzs['FRHPIYOY Index']
                                            +GDPNomAnn['Italy']*PXLast7Yzs['ITHPGYY Index']
                                            +GDPNomAnn['Spain']*PXLast7Yzs['SPNPTHY Index'])
RegCode['Equities M'] = PXLast05Yzs['SXXP Index PE']
RegCode['Equities L'] = PXLast7Yzs['SXXP Index PE']
RegCode['Household Leverage M'] = PXLast5Yzs['CPNFXMHG Index']
RegCode['Household Leverage L'] = PXLast10Yzs['CPNFXMHG Index']
RegCode['Corporate Leverage M'] = PXLast5Yzs['CPNFXMNG Index']
RegCode['Corporate Leverage L'] = PXLast10Yzs['CPNFXMNG Index']
RegCode['Government Leverage M'] = PXLast5Yzs['CPNFXMOG Index']
RegCode['Government Leverage L'] = PXLast10Yzs['CPNFXMOG Index']

# Liquidity
RegCode['Public Sector Liquidity ==> Reserves, Central Bank BS M'] = PXLast05Yzs['FARBCRED Index']
RegCode['Public Sector Liquidity ==> Reserves, Central Bank BS L'] = PXLast3Yzs['FARBCRED Index']
RegCode['Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M'] = (PXLast1Yzs['ECMSM1Y Index']+PXLast1Yzs['ECMSM2Y Index']+PXLast1Yzs['ECMAM3YY Index'])/3
RegCode['Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L'] = (PXLast7Yzs['ECMSM1Y Index']+PXLast7Yzs['ECMSM2Y Index']+PXLast7Yzs['ECMAM3YY Index'])/3
RegCode['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M'] = PXLast5Yzs['USCABAL Index']
RegCode['Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L'] = PXLast10Yzs['USCABAL Index']

# Funding Costs
RegCode['Real Policy Rate vs real neutral rate M'] = PXLast05Yzs['EUORDEPO Index']
RegCode['Real Policy Rate vs real neutral rate L'] = PXLast5Yzs['EUORDEPO Index']
RegCode['Real Long End Yields M'] = PXLast05Yzs['GECU30YR Index']
RegCode['Real Long End Yield L'] = PXLast5Yzs['GECU30YR Index']
RegCode['Real Ccy vs ToT M'] = PXLast1Yzs['163.028 Index']
RegCode['Real Ccy vs ToT L'] = PXLast7Yzs['163.028 Index']
RegCode['Financial Sector Health M'] = (PXLast1Yzs['CBK GR Equity']+PXLast1Yzs['DBK GR Equity']
                                       +PXLast1Yzs['UCG IM Equity']+PXLast1Yzs['ISP IM Equity']
                                       +PXLast1Yzs['SAN SM Equity']+PXLast1Yzs['CABK SM Equity'])/6
RegCode['Financial Sector Health L'] = (PXLast7Yzs['CBK GR Equity']+PXLast7Yzs['DBK GR Equity']
                                       +PXLast7Yzs['UCG IM Equity']+PXLast7Yzs['ISP IM Equity']
                                       +PXLast7Yzs['SAN SM Equity']+PXLast7Yzs['CABK SM Equity'])/6
RegCode['Credit Spreads M'] = PXLast05Yzs['LP05OAS Index']
RegCode['Credit Spreads L'] = PXLast3Yzs['LP05OAS Index']


# In[44]:


# Frame conditioning logic

ConditionFrame = pd.DataFrame(RegCode.copy())
for i in (ConditionFrame.columns):
    for j in (ConditionFrame.index):
        if (ConditionFrame.loc[j,i] < -1):
            ConditionFrame.loc[j,i] = 0
        elif (ConditionFrame.loc[j,i] > 1):
            ConditionFrame.loc[j,i] = 2
        else:
            ConditionFrame.loc[j,i] = 1
            

ConditionFrame.tail()


# In[45]:


# Create Growth Momentum Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
GrowthMPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

GrowthMCols = ['EPI ex inflation M','EPI Synchronised Sectors or Regions M','Earnings Growth M',
              'Investment/Capex M','Credit Growth M','Inventory Cycle M','Global Trade M','Fiscal Impulse M']

for i in GrowthMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            GrowthMPerc.loc[j,'Reflation'] += 1
            GrowthMPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            GrowthMPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            GrowthMPerc.loc[j,'Downturn'] += 1
            GrowthMPerc.loc[j,'Recession'] += 1

# Growth Momentum Frame Percentages            
GrowthMPerc = GrowthMPerc/len(GrowthMCols)

GrowthMPerc.tail()


# In[46]:


# Create Growth Level Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
GrowthLPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

GrowthLCols = ['EPI ex inflation L','EPI Synchronised Sectors or Regions L','Earnings Growth L',
              'Investment/Capex L','Credit Growth L','Inventory Cycle L','Global Trade L','Fiscal Impulse L']

for i in GrowthLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            GrowthLPerc.loc[j,'Downturn'] += 1
            GrowthLPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            GrowthLPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            GrowthLPerc.loc[j,'Recession'] += 1
            GrowthLPerc.loc[j,'Reflation'] += 1

# Growth Momentum Frame Percentages            
GrowthLPerc = GrowthLPerc/len(GrowthLCols)


GrowthLPerc.tail()


# In[47]:


# Create Inflation Momentum Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
InflationMPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

InflationMCols = ['EPI Inflation M','Wage Inflation M','Core (Persistence/Breadth) M','OECD Output Gap M',
              'Employment EPI M','Unemployment level vs NAIRU M','Capacity Utilisation M']

for i in InflationMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            InflationMPerc.loc[j,'Mid Cycle'] += 1
            InflationMPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            InflationMPerc.loc[j,'Downturn'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            InflationMPerc.loc[j,'Recession'] += 1
            
            

# Growth Momentum Frame Percentages            
InflationMPerc = InflationMPerc/len(InflationMCols)


InflationMPerc.tail()


# In[48]:


# Create Inflation Level Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
InflationLPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

InflationLCols = ['EPI Inflation L','Wage Inflation L','Core (Persistence/Breadth) L','OECD Output Gap L',
              'Employment EPI L','Unemployment level vs NAIRU L','Capacity Utilisation L']

for i in InflationLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            InflationLPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            InflationLPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            InflationLPerc.loc[j,'Recession'] += 1
            InflationLPerc.loc[j,'Reflation'] += 1
            

# Growth Momentum Frame Percentages            
InflationLPerc = InflationLPerc/len(InflationLCols)


InflationLPerc.tail()


# In[49]:


# Create Financial Conditions Momentum Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
FinCondMPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

FinCondMCols = ['Property (1y momentum) M','Equities M','Household Leverage M','Corporate Leverage M',
               'Government Leverage M','Public Sector Liquidity ==> Reserves, Central Bank BS M',
               'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) M',
               'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc M',
               'Real Policy Rate vs real neutral rate M','Real Long End Yields M','Real Ccy vs ToT M',
               'Financial Sector Health M','Credit Spreads M']

for i in FinCondMCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            FinCondMPerc.loc[j,'Reflation'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            FinCondMPerc.loc[j,'Mid Cycle'] += 1
            FinCondMPerc.loc[j,'Downturn'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            FinCondMPerc.loc[j,'Late Cycle'] += 1
            

# Growth Momentum Frame Percentages            
FinCondMPerc = FinCondMPerc/len(FinCondMCols)


FinCondMPerc.tail()


# In[50]:


# Create Financial Conditions Level Regime Percentages
RegCols = ['Reflation','Mid Cycle','Late Cycle','Downturn','Recession']
FinCondLPerc = pd.DataFrame(0,index=RegCode.index,columns=RegCols)

FinCondLCols = ['Property (level) L','Equities L','Household Leverage L','Corporate Leverage L',
               'Government Leverage L','Public Sector Liquidity ==> Reserves, Central Bank BS L',
               'Private Sector Liquidity ==> Banking System Liquidity (Broader monetary aggregates, Shadow bank liquidity) L',
               'Cross Border Liquidity ==> CA Surpluses, Petrodollar flows, EM flows etc L',
               'Real Policy Rate vs real neutral rate L','Real Long End Yields M','Real Ccy vs ToT L',
               'Financial Sector Health L','Credit Spreads L']

for i in FinCondLCols:
    for j in ConditionFrame.index:
        if ConditionFrame.loc[j,i] == 2:
            FinCondLPerc.loc[j,'Reflation'] += 1
            FinCondLPerc.loc[j,'Mid Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 1:
            FinCondLPerc.loc[j,'Late Cycle'] += 1
        elif ConditionFrame.loc[j,i] == 0:
            FinCondLPerc.loc[j,'Recession'] += 1
            

# Growth Momentum Frame Percentages            
FinCondLPerc = FinCondLPerc/len(FinCondLCols)


FinCondLPerc.head()


# In[54]:


# Generate overall probabilities
# Loop makes 0s into 0.0001s to avoid totally nullifying regime outcomes

GrowthProb = GrowthMPerc*GrowthLPerc
for i in GrowthProb.columns:
    for j in GrowthProb.index:
        if GrowthProb.loc[j,i] < 0.01:
            GrowthProb.loc[j,i] = 0.0001
InflationProb = InflationMPerc*InflationLPerc
for i in InflationProb.columns:
    for j in InflationProb.index:
        if InflationProb.loc[j,i] < 0.01:
            InflationProb.loc[j,i] = 0.0001
FinCondProb = FinCondMPerc*FinCondLPerc
for i in FinCondProb.columns:
    for j in FinCondProb.index:
        if FinCondProb.loc[j,i] < 0.01:
            FinCondProb.loc[j,i] = 0.0001

# Adjust for blank regimes
InflationProb['Downturn'] = InflationMPerc['Downturn']*InflationMPerc['Downturn']
InflationProb['Reflation'] = InflationLPerc['Reflation']*InflationLPerc['Reflation']
FinCondProb['Downturn'] = FinCondMPerc['Downturn']*FinCondMPerc['Downturn']
FinCondProb['Recession'] = FinCondLPerc['Recession']*FinCondLPerc['Recession']

#GrowthProb.tail()


# In[53]:


# Calculate the total probabilities

#TotalProb = GrowthProb*((InflationProb+FinCondProb)/2) #higher dominance to Growth
TotalProb = GrowthProb*InflationProb*FinCondProb

#Calc in 100%
GrowthProb[RegCols] = GrowthProb[RegCols].div(GrowthProb[RegCols].sum(axis=1), axis=0).multiply(100)
InflationProb[RegCols] = InflationProb[RegCols].div(InflationProb[RegCols].sum(axis=1), axis=0).multiply(100)
FinCondProb[RegCols] = FinCondProb[RegCols].div(FinCondProb[RegCols].sum(axis=1), axis=0).multiply(100)
TotalProb[RegCols] = TotalProb[RegCols].div(TotalProb[RegCols].sum(axis=1), axis=0).multiply(100)

GrowthProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\EUQuantReg\EURegGrowthProbability.xlsx')
InflationProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\EUQuantReg\EURegInflationProbability.xlsx')
FinCondProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\EUQuantReg\EURegFinancialCondProbability.xlsx')
TotalProb.to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\EUQuantReg\EURegTotalProbability.xlsx')
RegCode.transpose().to_excel(r'\\capricorn\AusFI\FXNEW\FX Model v clean THIS ONE\Regime Scorecard\QuantMacroRegime\EUQuantReg\EURegimeFullScoring.xlsx')


# In[ ]:



# In[ ]:




