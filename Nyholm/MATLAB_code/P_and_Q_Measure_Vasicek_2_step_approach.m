%% Two-step estimation procedure for the discrete-time Vasicek model
% Used in the section: the P and Q measures of the lecture notes

%% Load yield factors and construct yield curves
%
path_=[pwd,'\MATLAB_classes'];
addpath(path_);
load('Data_GSW.mat'); 
GSW_         = GSW;                          % instance of the GSW class
GSW_.tau     = [3 12:12:120]';               % vector of maturities
GSW_.beta    = GSW_factors(:,2:5);           % yield curve factors
GSW_.lambda  = GSW_factors(:,6:7);           % lambdas
GSW_         = GSW_.getYields;               % getting yields
figure
    plot(GSW_factors(:,1),GSW_.yields(:,[1 11]));
    datetick('x','mmm-yy'), title('US yields'), legend('3m','10y')

RDNS             = TSM;                     % instance of the TSM class
RDNS.yields      = GSW_.yields;             % adds yields to the model
RDNS.tau         = GSW_.tau;                % adds maturities
RDNS.biasCorrect = 0;
RDNS.DataFreq    = 12;
RDNS.nF          = 3;
RDNS             = RDNS.getSRB3;            % est. a 3 factor SRB model
figure
    plot(GSW_factors(:,1), RDNS.beta'),
    title('Extracted yield curve factors')
    datetick('x','mmm-yy'), 
    legend('Short rate','Slope','Curvature')
figure
    plot(GSW_factors(:,1),[RDNS.beta(1,:)' RDNS.yields(:,1)]),
    title('Model and Observed short rate')
    datetick('x','mmm-yy'), legend('Model','Observed')
figure
    plot(GSW_factors(:,1),[RDNS.TP(:,11) ACM(:,2)]),...
    title('10Y Term Premium')
    datetick('x','mmm-yy'), legend('SRB','ACM')    
    
[nObs,nTau] = size(RDNS.yields);     
%% Time-varying volatility and the Vasicek model
% Below we implement a two-step approach to estimating the Vasicek model 
% with time-varying volatility, as outlined in the lecture notes.
%
Y   = RDNS.yields./1200;   % US Yields in decimal form
tau = RDNS.tau;           % for maturities 3, 12:12:120 months
%
% ... Step 1
Sr            = Y(:,1);                        % 3-month rate = short rate
Mdl_AR_garch  = arima('ARLags',1,'Variance',garch(1,1), ...
                      'Distribution','Gaussian'); % AR(1)-GARCH(1,1) model
Est_AR_garch  = estimate(Mdl_AR_garch,Sr);        % estimate the model
[eps,s2]      = infer(Est_AR_garch,Sr);           % extract cond. variances 
cP            = Est_AR_garch.Constant;  
aP            = Est_AR_garch.AR{:};
s             = sqrt(s2);

%
% ... Step 2
p0  = [0;0];
lb_ = [-100;-100];
ub_ = [ 100; 100];
%
% minimise the squared residuals defined in the function 
%    Est_Vasicek - see below 
[pHat,fval,flagg,output,lamb_,G_,H_] = fmincon(@Est_Vasicek,p0,...
                                                [],[],[],[],lb_,ub_,...
                                                [],[],Y,s,cP,aP,tau,Sr)

[err2,Y_hat,a_tau,b_tau] = Est_Vasicek(pHat,Y,s,cP,aP,tau,Sr);
Y_hat = 12.*Y_hat;
RMSE  = 10000.*(mean((12.*Y-Y_hat).^2)).^(1/2);

figure
    plot(GSW_factors(:,1),sqrt(s2))
    datetick('x','mmm-yy')
    ylabel('\sigma_t') 
%   print -depsc P_Q_distribution

function [err2,Y_hat,a_tau,b_tau] = Est_Vasicek(p,Y,s,cP,aP,tau,Sr)
% This function calculates the difference between model and observed
% yields that can be used to estimate the parameters $\lambda_0$ and 
% $\lambda_1$
%
nObs = size(s,1);  
nTau = max(tau);

a_nF = @(a_,n_,c_,s_) -c_/(1-a_)*( n_ - (1-a_.^n_)/(1-a_) )...
                      +(s_^2)/(2*((1-a_)^2)).*(n_ + ...
                      (1-a_.^(2*n_))./(1-a_^2) - 2*(1-a_.^n_)./(1-a_) );
b_nF = @(a_,n_) -(1-a_.^n_)./((1-a_));

L0 = p(1);
L1 = p(2);
cQ = cP - L0.*s;
aQ = aP - L1.*s;
a_tau = NaN(size(tau,1),nObs);
b_tau = NaN(size(tau,1),nObs);
Y_hat = NaN(nObs,size(tau,1));
for (j=1:nObs)
   a_tau(:,j) = -a_nF( aQ(j,1), tau, cQ(j,1), s(j,1) )./tau;
   b_tau(:,j) = -b_nF( aQ(j,1), tau )./tau;
   Y_hat(j,:) = (a_tau(:,j) + b_tau(:,j)*Sr(j,1))';
end
err2  = sum(sum((Y-Y_hat).^2));
end








