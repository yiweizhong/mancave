%% Load yield factors and construct yield curves
%
load('Data_GSW.mat'); 
GSW_         = GSW;                  % creates an instance of the GSW class
GSW_.tau     = [3 12:12:120]';       % vector of maturities
GSW_.beta    = GSW_factors(:,2:5);   % yield curve factors
GSW_.lambda  = GSW_factors(:,6:7);   % lambdas
GSW_         = GSW_.getYields;       % getting yields
figure
    plot(GSW_factors(:,1),GSW_.yields(:,[1 11]));
    datetick('x','mmm-yy'), title('US yields'), legend('3m','10y')

RDNS             = TSM;              % creates an instance of the TSM class
RDNS.yields      = GSW_.yields;      % adds yields to the model
RDNS.tau         = GSW_.tau;         % adds maturities
RDNS.biasCorrect = 0;
RDNS.DataFreq    = 12;
RDNS.nF          = 3;
RDNS             = RDNS.getAFSRB;     % estimates a 3 factor SRB model
figure
    plot(GSW_factors(:,1), RDNS.beta'),
    title('Extracted yield curve factors')
    datetick('x','mmm-yy'), 
    legend('Short rate','Slope','Curvature')
figure
    plot(GSW_factors(:,1),[RDNS.beta(1,:)' RDNS.yields(:,1)]),
    title('Model and Observed short rate'),
    datetick('x','mmm-yy'), legend('Model','Observed')
figure
    plot(GSW_factors(:,1),[RDNS.TP(:,11) ACM(:,2)]), 
         title('10Y Term Premium'),
         datetick('x','mmm-yy'), legend('SRB','ACM')    
    
[nObs,nTau] = size(RDNS.yields);     

j = 686;
figure
    plot(RDNS.tau,[RDNS.Er(j,:)' RDNS.yields(j,:)'], 'LineWidth',2), 
    title(datestr(GSW_factors(j,1)))
    xticks(RDNS.tau)
    xticklabels(RDNS.tau), ylim([0 7]), 
    xlabel('Maturity in months'), ylabel('Yields (pct)'), legend('P-curve','Q-curve','Location','NW')

j = 386;
figure
    plot(RDNS.tau,[RDNS.Er(j,:)' RDNS.yields(j,:)'], 'LineWidth',2), 
    title(datestr(GSW_factors(j,1)))
    xticks(RDNS.tau)
    xticklabels(RDNS.tau), ylim([0 7])     
    xlabel('Maturity in months'), ylabel('Yields (pct)'), legend('P-curve','Q-curve','Location','NW')
    


