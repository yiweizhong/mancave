function [R,S,T,U,Mean0,Cov0,StateType] = pMap( p, flagg, tau )
%
% Parameter mapping function for MATLAB's SSM mudule
%
    nTau = size(tau,1);
    
    Phi  = [p(1) p(4) p(7)  ;
            p(2) p(5) p(8)  ;
            p(3) p(6) p(9) ];
   
    k    = diag([p(10);p(11);p(12)]);   

    Sx     = zeros(3,3);
    Sx(1,1)=p(13); Sx(2,1)=p(14); Sx(2,2)=p(15);
    Sx(3,1)=p(16); Sx(3,2)=p(17); Sx(3,3)=p(18);
    
    if (strcmp(flagg,'Emp'))
        b = [ p(19:29,1) p(30:40,1) p(41:51,1) ];
    elseif (strcmp(flagg,'NS'))
        L = p(19,1); p(20:51,1)=0;
        b = [ ones(nTau,1) ...
              (1-exp(-L.*tau))./(L.*tau) ...
              (1-exp(-L.*tau))./(L.*tau) - exp(-L.*tau)];
    else
        disp('The variable flagg must take on either of the following values')
        disp('NS (Nelson-Siegel)')
        disp('or, Emp (empirical model) ')
    end
   
    a  = diag(p(52:62,1));
    Sy = diag(p(63:73,1)); 

% ... Assigning the parameters following MATLAB's notation
%
    R = [ Phi k zeros(3,nTau-3); zeros(nTau,3) eye(nTau) ];
    S = [ Sx; zeros(nTau,3) ];
    
    T = [ b a; zeros(nTau,3) eye(nTau) ];  
    U = [ Sy; zeros(nTau,nTau) ];
    
% ... other assignments
    Mean0     = [];
    Cov0      = [];
    StateType = [ 0 0 0 ones(1,nTau) ]; 
end

