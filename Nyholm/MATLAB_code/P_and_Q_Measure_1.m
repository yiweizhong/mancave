%
% Used in the section: the P and Q measures
%

%% The first part deals with the estimation of the short rate equation 
% under the P-measure
%
%% Load yield factors and construct yield curves
%
path_=[pwd,'\MATLAB_classes'];
addpath(path_);
load('Data_GSW.mat'); 
GSW_         = GSW;                          % creates an instance of the GSW class
GSW_.tau     = [3 12:12:120]';               % vector of maturities
GSW_.beta    = GSW_factors(:,2:5);           % yield curve factors
GSW_.lambda  = GSW_factors(:,6:7);           % lambdas
GSW_         = GSW_.getYields;                         % getting yields
figure
    plot(GSW_factors(:,1),GSW_.yields(:,[1 11]));
    datetick('x','mmm-yy'), title('US yields'), legend('3m','10y')

%% Estimate AR(1) model for the 3 months rate
%

y_dat = GSW_.yields./100;

mdl_ar1               = varm(1,1);
[EstMdl,EstSE,logL,E] = estimate(mdl_ar1,y_dat(:,1) );

disp('       c      alpha     s')
disp([EstMdl.Constant EstMdl.AR{1,1} sqrt(EstMdl.Covariance)])

m = EstMdl.Constant/(1-EstMdl.AR{1,1})
s = sqrt(EstMdl.Covariance/(1-EstMdl.AR{1,1}^2))

a = 1-EstMdl.AR{1,1}
b = EstMdl.Constant/a 

%% P-measure prices
Delta_t = 1/12;
tau_    = GSW_.tau./12;
y_obs   = y_dat(530,:)';

r_n   = @(r_0, c_, a_, n_) r_0*(1-a_.^n_)/(1-a_) + c_*(a_.^n_-a_.*n_+n_-1)./(a_-1)^2 ;

r_t   = y_dat(end,1);
c     = EstMdl.Constant;
alpha = EstMdl.AR{1,1};

r120  = r_n(r_t,c,alpha,tau_*12)

r = zeros(120,1);
r(1,1) = r_t;
for (j=2:120)
    r(j,1) = c + alpha*r(j-1,1); 
end

p_tilde = exp(-r120.*Delta_t)

p_   = exp(-tau_.*y_obs);
out_ = [p_';p_tilde';p_tilde'-p_'].*100; 
disp('prices and risk premium')
disp(out_)

% how much do we need to change the constant and the AR coefficient to match the Q-dist prices?

c_ = c     + 1e-7;
a_ = alpha + 0.0007;
P_model  = exp(-r_n( y_obs(1,1), c_ ,a_ ,tau_*12 ).*(1/12)).*100;
P_target = p_.*100;

[P_target P_model P_target-P_model]

[c/(1-alpha) c_/(1-a_)]

% plot pdf's of P and Q measure distributions

r = (0.03:0.0001:0.07)';
pdfP = pdf('Normal',r,c/(1-alpha),sqrt(EstMdl.Covariance)); 
pdfQ = pdf('Normal',r,c_/(1-a_),sqrt(EstMdl.Covariance));

figure
    plot(r*100,pdfP, 'k--')
    hold on
    plot(r*100,pdfQ, 'r-'), legend({'P','Q'},'FontSize',10)
    xlabel('Short rate'), ylabel('pdf') 
    print -depsc P_Q_distribution

% ... matching short rate distribution to match premia
P_target = p_.*100;
c_       = c;
n_       = tau_*12;
p0       = [0.99];
a_fit = fmincon(@min_prem,p0,[],[],[],[],[0.90],[1.00],[],[],n_, P_target, r_t, c_);

mNew = c_/(1-a_fit)

function [err2,pp_] = min_prem( p, n_, P_target, r_t, c_ )
%
    a_   = p(1,1);
    r_n  = @(r_t, c_, a_, n_) r_t.*(1-a_.^n_)/(1-a_) + c_.*(a_.^n_-a_.*n_+n_-1)./(a_-1)^2 ;
    pp_  = exp(-r_n( r_t, c_ ,a_ ,n_ ).*(1/12)).*100;
    err2 = sum((P_target-pp_).^2); 
end