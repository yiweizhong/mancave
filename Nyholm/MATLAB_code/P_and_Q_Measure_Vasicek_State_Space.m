%% State-space estimation of the Vasicek model
% Used in the section: the P and Q measures of the lecture notes

%% Load yield factors and construct yield curves
%
path_=[pwd,'\MATLAB_classes'];
addpath(path_);
load('Data_GSW.mat'); 
GSW_         = GSW;                  % creates an instance of the GSW class
GSW_.tau     = [3 12:12:120]';       % vector of maturities
GSW_.beta    = GSW_factors(:,2:5);   % yield curve factors
GSW_.lambda  = GSW_factors(:,6:7);   % lambdas
GSW_         = GSW_.getYields;       % getting yields
figure
    plot(GSW_factors(:,1),GSW_.yields(:,[1 11]));
    datetick('x','mmm-yy'), title('US yields'), legend('3m','10y')

RDNS             = TSM;              % creates an instance of the TSM class
RDNS.yields      = GSW_.yields;      % adds yields to the model
RDNS.tau         = GSW_.tau;         % adds maturities
RDNS.biasCorrect = 0;
RDNS.DataFreq    = 12;
RDNS.nF          = 3;
RDNS             = RDNS.getSRB3;     % estimates a 3 factor SRB model
figure
    plot(GSW_factors(:,1), RDNS.beta'),
    title('Extracted yield curve factors')
    datetick('x','mmm-yy'), 
    legend('Short rate','Slope','Curvature')
figure
    plot(GSW_factors(:,1),[RDNS.beta(1,:)' RDNS.yields(:,1)]),
    title('Model and Observed short rate'),
    datetick('x','mmm-yy'), legend('Model','Observed')
figure
    plot(GSW_factors(:,1),[RDNS.TP(:,11) ACM(:,2)]), 
         title('10Y Term Premium'),
         datetick('x','mmm-yy'), legend('SRB','ACM')    
    
[nObs,nTau] = size(RDNS.yields);     

%% Estimating the parameters of the discrete-time one-factor model
% Data are scaled to monthly decimals (percentage annual yields are
% converted to monthly decimal rates, because the formulas for the 
% yield curve loadings are calculated for monthly step-sizes and thus 
% for monthly rates.
%
scl_        = 1200;
Y           = [ RDNS.yields./scl_ ...
                RDNS.yields(:,1)./scl_ ...
                ones(nObs,1) ];    

cP  = 0.01;
aP  = 0.95;
s   = 1.15;
L0  = 0;
L1  = 0;
sY  = 1.25.*ones(nTau,1);

p0  = [ cP; aP; s; L0; L1; sY ];
lb_ = [ 0.00; 0.00; 0; -inf; -inf; zeros(nTau,1) ];
ub_ = [ 1.00; 1.00; 1;  inf; inf; 1000.*ones(nTau,1) ];

% constraints that ensure that all yield volatilities, ie. the 
% entries of the variance-covariance matrix in the observation 
% equation are equal for all maturities included in the analysis.
%
nP  = size(p0,1);
Aeq = zeros(nTau-1,nP);
Aeq(1,[6 7])   = [1 -1];Aeq(2,[7 8])  =[1 -1];Aeq(3,[8 9])  = [1 -1];
Aeq(4,[9 10])  = [1 -1];Aeq(5,[10 11])=[1 -1];Aeq(6,[11 12])= [1 -1];
Aeq(7,[12 13]) = [1 -1];Aeq(8,[13 14])=[1 -1];Aeq(9,[14 15])= [1 -1];
Aeq(10,[15 16])= [1 -1];
beq = zeros(size(Aeq,1),1);

Mdl_sr  = ssm(@(p) pMap(p, RDNS.tau));
options = optimoptions(@fmincon,'Algorithm','interior-point',...
                                'MaxIterations',1e6, ...
                                'MaxFunctionEvaluations',1e6, ...
                                'TolFun', 1e-6, 'TolX', 1e-6); 

disp('... Estimating the model using the SSM module')
[ EstMdl_sr, pHat, pCov, logl, outFlags ] = ... 
         estimate( Mdl_sr,Y,p0,'Display','iter','Aeq',Aeq,'beq',beq,...
                  'lb',lb_,'ub',ub_,'univariate',true,'options',options )

x_filter  = filter( EstMdl_sr, Y );  % extract filtered state variables
sr_filter = x_filter(:,1);           % filtered short rate

cP_  = pHat(1,1);
aP_  = pHat(2,1);
s_   = pHat(3,1);
L0_  = pHat(4,1);
L1_  = pHat(5,1);
sY_  = pHat(6:end,1);
mP  = (cP_/(1-aP_))*scl_;

a_tau_ = EstMdl_sr.C(1:nTau,2);  
b_tau_ = EstMdl_sr.C(1:nTau,1);
Y_fit  = (a_tau_ + b_tau_*sr_filter')';    

RMSE  = 100.*(mean((scl_.*Y(:,1:11)-scl_.*Y_fit).^2)).^(1/2)

figure
    plot(GSW_factors(:,1),[sr_filter Y(:,12)]), 
    title('Yield curve factor')
    datetick('x','mmm-yy'), legend('obs','fit')
figure
    plot(GSW_factors(:,1),[Y_fit(:,1) Y(:,1)]), 
    title('3M rate')
    datetick('x','mmm-yy'), legend('fit','obs')   
figure
    plot(GSW_factors(:,1),[Y_fit(:,2) Y(:,2)]), 
    title('1Y rate')
    datetick('x','mmm-yy'), legend('fit','obs')
figure
    plot(GSW_factors(:,1),[Y_fit(:,3) Y(:,3)]), 
    title('2Y rate')
    datetick('x','mmm-yy'), legend('fit','obs')
figure
    plot(GSW_factors(:,1),[Y_fit(:,4) Y(:,4)]), 
    title('3Y rate')
    datetick('x','mmm-yy'), legend('fit','obs')
figure
    plot(GSW_factors(:,1),[Y_fit(:,5) Y(:,5)]), 
    title('4Y rate')
    datetick('x','mmm-yy'), legend('fit','obs')
figure
    plot(GSW_factors(:,1),[Y_fit(:,6) Y(:,6)]), 
    title('5Y rate')
    datetick('x','mmm-yy'), legend('fit','obs')
figure
    plot(GSW_factors(:,1),[Y_fit(:,11) Y(:,11)]), 
    title('10Y rate')
    datetick('x','mmm-yy'), legend('fit','obs')

%%
function [R,S,T,U,Mean0,Cov0,StateType] = pMap( p, tau )
%
% Setting up the matrices necessary to estimate the state-space model
%
nTau_1 = length(tau);
nTau   = max(tau);

cP  = p(1,1);
aP  = p(2,1);
s   = p(3,1);
L0  = p(4,1);
L1  = p(5,1);
sY  = p(6:end,1);

cQ  = cP - s*L0;
aQ  = aP - s*L1;

[ a_tau, b_tau ] = find_a_b(s,cQ,aQ,tau);

R = [ aP cP; 0 1 ];
S = [ s; 0 ];
T = [ b_tau a_tau; 1 0; 0 1 ];  
U = [ diag(sY); zeros(2,nTau_1) ];

% ... other assignments
    Mean0     = [];
    Cov0      = [];
    StateType = [ 0; 1 ];    
end

function [a_n, b_n] = find_a_b(s,cQ,aQ,tau)
% determines the loadings and the constant vector using the 
% recursive equations and closed form expressions. 
%
flagg = 1;    % 1-> closed form results, 0->iterative solution

a_nF = @(a_,n_,c_,s_) -c_/(1-a_)*( n_ - (1-a_.^n_)/(1-a_) )...
                      +(s_^2)/(2*((1-a_)^2)).*(n_ + ...
                      (1-a_.^(2*n_))./(1-a_^2) - 2*(1-a_.^n_)./(1-a_) );
b_nF = @(a_,n_) -(1-a_.^n_)./((1-a_));

if (flagg==0)
    nTau = max(tau(:));
    ttau = (1:1:nTau)';
    a_t  = zeros(nTau,1);
    b_t  = zeros(nTau,1);
    for (j=2:nTau+1)
        b_t(j,1) = b_t(j-1,1)*aQ - 1;
        a_t(j,1) = a_t(j-1,1) + b_t(j-1,1)*cQ - 0.5*s^2*(b_t(j-1,1))^2;
    end
    a_n = -a_t(tau+1,1)./tau;
    b_n = -b_t(tau+1,1)./tau;
else
    a_n = -a_nF(aQ,tau,cQ,s)./tau;
    b_n = -b_nF(aQ,tau)./tau;
end
end

