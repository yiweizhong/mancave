load('Data_GSW.mat'); 
GSW_         = GSW;                  % creates an instance of the GSW class
GSW_.tau     = [3 12:12:120]';       % vector of maturities
GSW_.beta    = GSW_factors(:,2:5);   % yield curve factors
GSW_.lambda  = GSW_factors(:,6:7);   % lambdas
GSW_         = GSW_.getYields;       % getting yields
figure
    plot(GSW_factors(:,1),GSW_.yields(:,[1 11]));
    datetick('x','mmm-yy'), title('US yields'), legend('3m','10y')

start_ = 1;    
    
tau   = GSW_.tau;                                 % maturity     
Y     = GSW_.yields;                              % yields
dY    = Y(start_+1:end,:)-Y(start_:end-1,:);                  % first differences
pY    = log(Y(start_+1:end,:)./Y(start_:end-1,:));            % percentage returns

[vY,eY,lY]    = pca(cov(Y));                   % PCA on correlations
[vdY,edY,ldY] = pca(cov(dY));
[vpY,epY,lpY] = pca(cov(pY));

% ... comparing loadings ...
figure
    subplot(3,1,1), plot(tau,vY(:,1:3)), title('Loadings for Yields')
    subplot(3,1,2), plot(tau,vdY(:,1:3)), title('Loadings for Yield Changes')
    subplot(3,1,3), plot(tau,vpY(:,1:3)), title('Loadings for Yield Perentage Changes')
    