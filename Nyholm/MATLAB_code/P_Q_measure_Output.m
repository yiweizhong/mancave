%% Output for the P and Q measure section of the lecture notes 
% for the section on the Vasicek model
%

% ... run the state-space estimation
P_and_Q_Measure_Vasicek_State_Space
RMSE_SSM  = RMSE;
Y_obs_2Y  = scl_.*Y(:,3);
Y_obs_10Y = scl_.*Y(:,11);
Y_SSM_2Y  = scl_.*Y_fit(:,3);
Y_SSM_10Y = scl_.*Y_fit(:,11);

% ... run the empirical model
Empirical_1F_Model
RMSE_EMP  = RMSE*100;
Y_EMP_2Y  = Y_fit(:,3);
Y_EMP_10Y = Y_fit(:,11);

% ... run 2-step estimation approach
P_and_Q_Measure_Vasicek_2_step_approach
RMSE_2step  = RMSE;
vol         = s;
Y_2step_2Y  = 100*Y_hat(:,3);
Y_2step_10Y = 100*Y_hat(:,11);

figure
    plot(GSW_factors(:,1),100*s)
    datetick('x','mmm-yy')
    ylabel('\sigma_t (pct)') 
    print -depsc PQ_tv_vol

figure
    subplot(2,1,1), plot(GSW_factors(:,1),[Y_obs_2Y Y_SSM_2Y ...
                                           Y_EMP_2Y Y_2step_2Y])
    datetick('x','mmm-yy')
    ylabel('(pct)'), legend('Obs','State-Space','Empirical',...
                            'Two-step','location','NE'),
    title('2 year yields')

    subplot(2,1,2), plot(GSW_factors(:,1),[Y_obs_10Y Y_SSM_10Y ...
                                           Y_EMP_10Y Y_2step_10Y])
    datetick('x','mmm-yy')
    ylabel('(pct)')
    title('10 year yields')
    print -depsc PQ_modelFit

RMSE_all_models = [ round(RMSE_SSM.*100)./100; round(RMSE_EMP.*100)./100; ...
                    round(RMSE_2step.*100)./100 ]


