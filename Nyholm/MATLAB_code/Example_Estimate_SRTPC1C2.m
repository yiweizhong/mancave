%% Loading and plotting data
% 
load('Data_GSW.mat'); 
load('TP_10Y');                            % contains Kim_Wright, Morgan Stanley 10Y term permia 

GSW_         = GSW;                  % creates an instance of the GSW class
GSW_.tau     = [3 12:12:120]';       % vector of maturities
GSW_.beta    = GSW_factors(:,2:5);   % yield curve factors
GSW_.lambda  = GSW_factors(:,6:7);   % lambdas
GSW_         = GSW_.getYields;       % getting yields


dates = GSW_factors(:,1);
Y     = GSW_.yields;
tau   = GSW_.tau;
nTau  = size(tau,1);

figure('units','normalized','outerposition',[0 0 1 1])
    surf(tau./12,dates,Y)
    date_ticks = datenum(1960:4:2020,1,1);
    set(gca, 'ytick', date_ticks);
    datetick('y','mmm-yy','keepticks')
    xticks(0:1:11), xticklabels({tau}),
    xlabel('Maturity (months)'), zlabel('Yield (pct)'), 
    view([-109 38]),
    ytickangle(-25),
    set(gca, 'FontSize', 18)
    %print -depsc Y3D

SR = TSM;
SR.yields   = Y;
SR.tau      = tau;
SR.nF       = 4;
SR.DataFreq = 12;

SR = SR.getSRTPC1C2;

figure
    plot(SR.tau,SR.B', 'LineWidth',2), title('Loadings'), 
    xticks(SR.tau)
    legend('Short rate', '10-year TP', 'C1', 'C2', 'location','SW')
    
figure
    plot(dates,SR.beta(1:2,:)','LineWidth',2)
    hold on
    plot(dates,SR.beta(3:4,:)','LineWidth',1)    
    date_ticks = datenum(1960:4:2020,1,1);
    set(gca, 'xtick', date_ticks);
    datetick('x','mmm-yy','keepticks')
    title('Factor realisations')
    legend('Short rate', '10-year TP', 'C1', 'C2', 'location','SW')
 
figure
    plot(dates,[SR.yields(:,11 ) ACM(:,2) SR.beta(2,:)' TP_10y(:,2)],'LineWidth',1)
    date_ticks = datenum(1960:4:2020,1,1);
    set(gca, 'xtick', date_ticks);
    datetick('x','mmm-yy','keepticks')
    title('10-year Term Premia')
    ylabel('percentage')
    legend('10-year yield','Adrian, Crump and Moench (2013)', 'SRB ', 'Kim and Wright', 'location','NE')
    
    
    
    
    
    
    
    