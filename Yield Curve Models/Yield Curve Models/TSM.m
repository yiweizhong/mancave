classdef TSM
    %
    % Usage:   .getDNS       -> Dynamic Nelson-Siegel model and related metrics
    %          .getDSS       -> Dynamic Svensson-Soderlind model and related metrics
    %          .getSRB3      -> Short-Rate based 3-factor model and related metrics (following Nyholm 2018)
    %          .getSRB4      -> Short-Rate based 4-factor model and related metrics (following Nyholm 2018)
    %          .getJSZ       -> Joslin, Singleton, Zhu (2011)
    %          .getAFSRB     -> Arbitrage-free SRB model with 2,3, or 4 factors
    %          .getSRTPC1C2  -> Model with Short rate, 10-year termpremium, and 2 additional empirical factors
    %          .getSSR3      -> Shadow-short rate model with 3 factors (following Coche, Nyholm, Sahakyan 2018)
    %
    % TSM : Dynamic term structure models of the Nelson-Siegel family. 
    %       Arbitrage constraints are not imposed in this modelling
    %       framework. See Nelson-Siegel(1987), Diebold-Li(2011), Nyholm(2018) 
    %       for references.
    %
    %       Small sample bias correction using Pope's closed form solution
    %       can be invoked via the flagg "biasCorrect=1".
    %
    %       The basic model setup is the following:
    %
    %           Y_t(tau) = B(lambda) * beta_t + e_t
    %           beta_t   = mP + PhiP * (beta_{t-1}-mP) + v_t
    %
    %       The model is estimated using two-step OLS.
    %
    %       B(lambda) specifies the applied model. The following
    %       alternatives are valid:
    %   
    %
    %  Input:
    %  ------
    %       yields   :  panel of yields      ( nObs-by-nTau )
    %       nF       :  number of factors    ( used only for the arbitrage-free models )
    %       tau      :  vector of maturities ( nTau-by-1 )
    %       mP_pre   :  pre-specified mean for the betas 
    %       DataFreq :  Data frequency of the yield curve data. This is 
    %                   relevant for the calculation of Term Premia.
    %                   The frequency is provided as the 
    %                   "number of observations per year" to allow for
    %                   flexibility, so, e.g.:
    %                                         DataFreq      Interpretation
    %                                         ----------------------------
    %                                             360    =   daily
    %                                              52    =   weekly
    %                                              12    =   Monthly
    %
    %       Z        :  panel of exogenous variables that can affect the 
    %                   yield curve factors via the VAR model. They are
    %                   treated as unspanned factors as they do not impact
    %                   the pricing of bonds
    %  Output:
    %  -------
    %       beta   : panel of extracted factors 
    %       lambda : optimal lambda values
    %       Yfit   : fitted yields
    %       RMSE   : Root mean squared error of yield residuals (in Basis points)
    %       e_t    : residuals from the yield equation
    %       v_t    : residuals from the beta equation
    %       PhiP   : matrix of autoregressive parameters
    %       mP     : estimated mean of the betas
    %       rho1   : vector that defines the short rate as a function of
    %                   the yield curve factors
    %       TP     : term premia calculated at the maturities tau
    %       Er     : expectations component (risk free term structure)
    %
    % Ken Nyholm
    % June 2018  
    % Version 1.0
    % Version 1.1 (October 2018)
    % updated with the ppossible inclusion of exogenous variables in the
    %         VAR for the yield curve factors
    %
    properties ( Access=public )
        yields, A, B, An, Bn, beta, mP, mP_pre, PhiP, PhiP_pre, PhiP_bc, e_t, v_t, nF double 
        lambda, Yfit, RMSE, Omega_yields, Omega_beta double
        DataFreq, nObs, nTau, rho0, rho1, TP, Er, Er_doubleCheck double
        PhiQ, mQ, mQ_pre, kQ, gamma double
        AX, BX, W double
        Yfit_Shadow, rL, beta_Shadow double
        tau double = [3 12:12:120]';
        model char
        biasCorrect double = 0;
        eXo double = [];
        n_eXo double = 0;
        nVarExo double = 0;
    end
    properties ( Access=private )
        I, L, scl, nMax, gamma0, kQ0, p0 double
        Aeq_, beq_, nP, s, AA, BB double
        rho0x, rho1x, rho00, rho11 double
        Y_, B_, err double
        X_shadow, pHat_sr, pHat double 
    end
    
    methods ( Access=private )     
        function [TSM] = chkInput(TSM)
            TSM.nObs = size(TSM.yields,1);
            TSM.nTau = size(TSM.yields,2);
            if ( isempty(TSM.yields) || isempty(TSM.tau) || isempty(TSM.DataFreq) )
                disp('Error: please check the input data, something is not right...')
                return
            end
            if ( ~isempty(TSM.eXo) )
                [TSM.n_eXo, TSM.nVarExo] = size(TSM.eXo);
                if ( TSM.n_eXo~=TSM.nObs )
                    disp('Error: please check input data -> different number of observations of yield and exogenous variables...')
                    return
                end
            end
            if ( ~isempty(TSM.PhiP_pre) )
                [nn,cc] = size(TSM.PhiP_pre);
                if (TSM.nF+TSM.n_eXo~=nn || TSM.nF+TSM.nVareXo~=cc)
                    disp('please ensure that the dimensions of PhiP_pre matches with the numbers of factors and exogenous variables...')
                end
            end
            TSM.Omega_beta = eye(TSM.nF);          % just assigning a temporary value
            TSM.mQ         = zeros(TSM.nF,1);      % just assigning a temporary value        
        end
        
        function [TSM] = vare(TSM)
            % VAR(1) model is estimated.
            %        MATLAB's built-in functionality 'varm' is not used 
            %        becase the TSM class allows the user to prespecify 
            %        constrains on the mean of the process (i.e. not the
            %        constants). It appears that this is not possible
            %        using 'varm', and we therefore use fmincon and 
            %        write the model in its mean-corrected version
            %
            if (~isempty(TSM.eXo)) 
                TSM.mP   = [ mean(TSM.beta,2); mean(TSM.eXo)' ] ;
                TSM.beta = [ TSM.beta; TSM.eXo' ];
            else
                TSM.mP   = [ mean(TSM.beta,2)];
            end
            Xt       = TSM.beta - repmat(TSM.mP,1,TSM.nObs);
            TSM.PhiP = (Xt(:,1:end-1)'\Xt(:,2:end)')'; 
            TSM.v_t  = TSM.beta(:,2:end) - ( repmat(TSM.mP,1,TSM.nObs-1) + ...
                                                 TSM.PhiP*(TSM.beta(:,1:end-1)-repmat(TSM.mP,1,TSM.nObs-1)));
            TSM.Omega_beta = 1/TSM.nObs.*(TSM.v_t*TSM.v_t');            
            if ( ~isempty(TSM.mP_pre) || ~isempty(TSM.PhiP_pre) || sum(diag(TSM.PhiP)<0)>0 || sum(diag(TSM.PhiP)>1.5)>0 || strcmp(TSM.model,'SRTPC1C2'))
                nR = length(TSM.mP_pre);                             % number of restrictions
                if ( nR==0 )
                    TSM.mP_pre = TSM.mP;
                    nR         = length(TSM.mP_pre);
                end
                nP     = nR+nR^2;                                    % number of parameters
                lst    = [nP:-nR-1:1]'; lst = lst(1:nR);
                err2   = @(p_,Dat) sum(sum((Dat(:,2:end) - ...
                                    ( p_(1:nR,1) + [ reshape(p_(nR+1:end),nR,nR) ]* ...
                                                ( Dat(:,1:end-1)-repmat(p_(1:nR,1),1,TSM.nObs-1)) )).^2));
                fmin       = @(p_) err2(p_,TSM.beta);
                lb_        = -100*ones(nP,1);
                ub_        =  100*ones(nP,1);
                %lb_(lst,1) = -100;
                %ub_(lst,1) = 100;
                Aeq      = [eye(nR) zeros(nR,nR^2) ];
                beq      = TSM.mP_pre;
                if ( ~isempty(TSM.PhiP_pre)  )       % Added 6 Dec 2018 to allow for restrictions on the Autoregressive matrix
                    nR1     = sum(sum(TSM.PhiP_pre==0));  % number of zero restrictions
                    [ri,~] = find(TSM.PhiP_pre(:)==0);
                    tmp     = zeros(nR1,nP);
                    ci      = (1:1:nR1)';
                    rii     = ri+nR;
                    for (z=1:nR1)
                        tmp(ci(z,1),rii(z,1))=1;
                    end
                    Aeq     = [ Aeq; tmp ];
                    beq     = [ beq; zeros(nR1,1)];
                end
                if( strcmp(TSM.model,'SRTPC1C2') )   % Added 6 Nov 2018 to impose no corr between C4 and Sr, TP
                    Aeq       = [ Aeq; zeros(3,20) ];
                    Aeq(5,17) = 1; Aeq(6,19)=1;  
                    Aeq(7,15) = 1;  Aeq(7,20)= -1; 
                    beq  = [beq;0;0;0];
                end
                I        = zeros(nR,nR);
                p0       = [ TSM.mP_pre; I(:) ];
                opt_     = optimset('MaxFunEval',1e5,'MaxIter',1e5, 'TolFun',1e-8, 'TolX',1e-8, 'Display','iter'  );
                pHat     = fmincon(fmin,p0,[],[],Aeq,beq,lb_,ub_,[],opt_);
                TSM.mP   = pHat(1:nR,1);
                TSM.PhiP = reshape(pHat(nR+1:end),nR,nR);
                TSM.v_t  = TSM.beta(:,2:end) - ( repmat(TSM.mP,1,TSM.nObs-1) + ...
                                                 TSM.PhiP*(TSM.beta(:,1:end-1)-repmat(TSM.mP,1,TSM.nObs-1)));
                TSM.Omega_beta = 1/(TSM.nObs-1).*(TSM.v_t*TSM.v_t');                             
            end
        end
   
        function [TSM] = pope(TSM)
            %
            % Bias correction for small sample VAR following Pope(1990)
            %
            k     = size(TSM.PhiP,1);
            % long run variance Var(X_t)
            Gamma0 = reshape( (eye(k^2) - kron(TSM.PhiP, TSM.PhiP))^(-1) * TSM.Omega_beta(:), k, k);
            tmpsum = zeros(k,k);
            eig_ = eig(TSM.PhiP);
            for i=1:k
                tmpsum = tmpsum + eig_(i)*( eye(k) - eig_(i)*TSM.PhiP' )^(-1);
            end
            b           = TSM.Omega_beta * ( ( eye(k)-TSM.PhiP' )^(-1) + TSM.PhiP'*inv(eye(k)-(TSM.PhiP'*TSM.PhiP')) + tmpsum ) * (Gamma0)^(-1);
            TSM.PhiP_bc = TSM.PhiP+b/TSM.nObs;       % following Wang, Phillips, Yu
            k_indx = ( 0.99:-0.01:0 )';
            q = 1;
            while ( max(abs(eig(TSM.PhiP_bc)))>0.9999 && q<=length(k_indx) )  % ensuring stationarity following Killian
                TSM.PhiP_bc = TSM.PhiP + (b./TSM.nObs).*k_indx(q,1);
                q = q+1;
                if ( q==length(k_indx) )
                    disp('NOTE: VAR(1) matrix may not be stationary')
                end
            end    
        end
       
        function [TSM] = TermPremia(TSM)
            %
            % Calculating term premia and expectations component (risk free term structure)
            %
            ExpR_doubleCheck = @(V_,D_,n_,X0_,cP_,rho1_,I_) rho1_'*( ((1/n_)*(V_*D_.^n_*V_^(-1)-I_)*(V_*D_*V_^(-1)-I_)^(-1))*X0_) - ...
                                                rho1_'*((( V_*D_*V_^(-1)-I_)^(-1) - (1/n_)*( V_*D_.^n_*V_^(-1)-I_ )*(V_*D_*V_^(-1)-I_)^(-2))*cP_);  
            
            ExpR_calc = @(V_,D_,n_,X0_,mP_,rho1_,I_)  (1/n_)*(( X0_'*rho1_ ) +  ...
                                                  ( ( n_-1 ).*mP_ + ... 
                                                  ((( V_*D_^n_*V_^(-1) ) - ( V_*D_*V_^(-1) )) *( V_*D_*V_^(-1)-I_ )^(-1) )*(X0_-mP_))'*rho1_);
                                            
            TSM.Er_doubleCheck = NaN( TSM.nObs, TSM.nTau );
            TSM.Er             = NaN( TSM.nObs, TSM.nTau );
            I      = eye(size(TSM.beta,1));
            [V,D]  = eig(TSM.PhiP);
            if ( ~isempty(TSM.PhiP_bc) )      
                [V,D] = eig(TSM.PhiP_bc);
            end
            cP = ( I-V*D*V^(-1) )*TSM.mP;
            for ( j=1:TSM.nObs )
                for ( k=1:TSM.nTau )
                    n_                      = TSM.tau(k,1)*(TSM.DataFreq)/12;
                    TSM.Er_doubleCheck(j,k) = ExpR_doubleCheck(V(1:TSM.nF,1:TSM.nF),D(1:TSM.nF,1:TSM.nF),n_,TSM.beta(1:TSM.nF,j),cP(1:TSM.nF),TSM.rho1(1:TSM.nF,1),I(1:TSM.nF,1:TSM.nF));
                    TSM.Er(j,k)             = ExpR_calc(V(1:TSM.nF,1:TSM.nF),D(1:TSM.nF,1:TSM.nF),n_,TSM.beta(1:TSM.nF,j),TSM.mP(1:TSM.nF),TSM.rho1(1:TSM.nF,1),I(1:TSM.nF,1:TSM.nF));
                end
            end
            TSM.TP = TSM.Yfit-TSM.Er;
        end
        
        function [TSM] = An_Bn(TSM)
            %
            % Calculation of the recursive equations for the factor 
            %             loadings in the JSZ yield equation. 
            %
            % Note: n is used as short hand for tau.
            %       (1) no division by n, and all maturities from 1:max(n) are provided. 
            %       (2) mQ is the mean of the process i.e. mQ=(I-PhiQ)^-1 * cQ
            %
            TSM.An      = NaN( max(TSM.nMax), 1 );
            TSM.Bn      = NaN( TSM.nF, max(TSM.nMax) );
            rho00       = TSM.rho0x/TSM.DataFreq;
            rho11       = TSM.rho1x./TSM.DataFreq;
            TSM.An(1,1) = rho00;
            TSM.Bn(:,1) = rho11;
            TSM.I       = eye(TSM.nF);
            for ( j=2:max(TSM.nMax) )
                TSM.An(j,1) = TSM.An(j-1,1) + TSM.Bn(:,j-1)'*((TSM.I-TSM.PhiQ)*TSM.mQ) + ... 
                                 0.5*TSM.Bn(:,j-1)'*(TSM.Omega_beta)*TSM.Bn(:,j-1); 
                if ( real(max(eig(TSM.PhiQ)))<1 )
                    TSM.Bn(:,j) = -(( TSM.I - TSM.PhiQ^(j) )*( TSM.I - TSM.PhiQ )^(-1))'*rho11;
                else
                    TSM.s = zeros(TSM.nF,TSM.nF);
                    for ( k=0:j-1 )
                        TSM.s = TSM.s+TSM.PhiQ^(k);
                    end
                    TSM.Bn(:,j) = -TSM.s'*rho11;
                end
            end
            TSM.AA = -TSM.An./TSM.nMax;
            TSM.BB = -TSM.Bn'./TSM.nMax;
            TSM.A  = TSM.AA(TSM.tau,1);
            TSM.B  = TSM.BB(TSM.tau,:);
        end
        
        function [SSE_JSZ,TSM] = JSZ_minSSE(p0,TSM)
            % finds PhiQ, and kQ in the JSZ model
            %
            TSM.PhiQ = diag(p0(1:TSM.nF));
            TSM.mQ   = [p0(end,1);zeros(TSM.nF-1,1)];
            [ TSM ]  = An_Bn(TSM);
            TSM.AX   = TSM.A;               % a_tau based on Jordan form
            TSM.BX   = TSM.B;               % b_tau based on Jordan form
            % ... now rotate to PCA form
            TSM.A    = (( eye(TSM.nTau) - TSM.BX*(TSM.W*TSM.BX)^(-1)*TSM.W )*TSM.AX);
            TSM.B    = (TSM.BX*(TSM.W*TSM.BX)^(-1));     
            TSM.Yfit = 100.*(TSM.A + TSM.B*TSM.beta)';
            SSE_JSZ  = sum(sum( (TSM.yields-TSM.Yfit).^2 ));
        end
        
        function [ TSM ] = An_Bn_SRB(TSM)
            % 
            % yield loadings for the AFSRB model
            %
            An  = zeros( max(TSM.nMax), 1 );
            Bn  = NaN( TSM.nF, max(TSM.nMax) );
            mat = TSM.nMax;
            g_  = TSM.gamma;
            i_  = ones(max(TSM.nMax),1);
            switch TSM.nF
                case 2
                    Bn = -[ mat  mat-(i_-g_.^mat)./(i_-g_) ];
                case 3
                    Bn = -[ mat  mat-(i_-g_.^mat)./(i_-g_)  -mat.*g_.^(mat-1) + ( i_-g_.^mat)./ (1-g_) ];   
                case 4
                    Bn = -[ mat  mat-(i_-g_.^mat)./(i_-g_)  -mat.*g_.^(mat-1) + ( i_-g_.^mat)./ (1-g_) ...
                             -(mat./2).*(mat-i_).*(g_-1).*g_.^(mat-2) ];  
            end
            TSM.An = An;
            TSM.Bn = Bn;
            TSM.B = -Bn(TSM.tau,:)./repmat(TSM.tau,1,TSM.nF);
            for ( j=2:max(TSM.nMax) )
                An(j,1) = An(j-1,1) + Bn(j,:)* TSM.mQ(1:TSM.nF,1) + 1/2.*Bn(j,:)*(TSM.Omega_beta(1:TSM.nF,1:TSM.nF))*Bn(j,:)';      % d0=0 in this model
            end
            TSM.A = -(1/(100*TSM.DataFreq)).*An(TSM.tau,1)./TSM.tau;
        end
        
        function [ SSE_AFSRB, TSM ] = AFSRB_minSSE(p0,TSM)
            % finds mQ of the ARSRB model
            %        
            TSM.mQ    = p0;
            [ TSM ]   = An_Bn_SRB(TSM);
            SSE_AFSRB = sum(sum((TSM.yields - (TSM.A + TSM.B*TSM.beta(1:TSM.nF,:))').^2));
        end
        
        function [ err2, TSM ] = Yshadow( p0,TSM ) 
        %
        % calculating the sum of squared residuals from the static 
        %    shadow short rate model set-up 
        %
            Y_   = TSM.Y_;
            B_   = TSM.B_;
            rL   = TSM.rL;
            
            nObs = size(Y_,1);

        % ... Defining the shadow rate transformations
        %
            alfa_  = @(Xshdw,zz) ( tanh(zz(1,1).*Xshdw(2,:)+zz(2,1)) ...
                   +3 )./2 .*( tanh( zz(3,1).*Xshdw(3,:)+zz(4,1) )+3 )./2;  

            yFit_  = @(yS_,alpha_,rL_) rL_+(yS_-rL_)./(1-exp(-alpha_.*(yS_-rL_)));

        % ... fixing some of the free parameters
        %
            zz_ = [ -5.00; 1.00; 4.00; 1.00 ];

        % ... calculating shadow yields
        %
            X_shadow = reshape(p0,nObs,3)';
            y_shadow = B_*X_shadow;
            alpha    = alfa_(X_shadow,zz_);
            yFit     = yFit_(y_shadow,alpha,rL)';
            err      = Y_-yFit;
            err2     = sum(sum(err.^2));
        
            TSM.beta_Shadow = X_shadow;
            TSM.Yfit_Shadow = y_shadow;
            TSM.err      = err;
        end
    end
    
    methods ( Access=public )
        %
        % ... The Dynamic Nelson-Siegel Model
        %
        function [TSM] = getDNS(TSM)
            disp('... DNS ...')
            TSM       = chkInput(TSM);
            TSM.model = 'DNS';
            TSM.A     = zeros(TSM.nTau,1);
            TSM.rho1  = [1;1;0;zeros(TSM.nVarExo,1)];
            Bfunc = @(lambda_,tau_) [ ones(TSM.nTau,1) (1-exp(-lambda_.*tau_))./(lambda_.*tau_) ...
                    (1-exp(-lambda_.*tau_))./(lambda_.*tau_)-exp(-lambda_.*tau_) ];
            %
            % ... Finding the optimal lambda value on a grid of possible values
            %
            list_ = (0.01:0.001:0.30)';
            SSE_  = NaN(length(list_),2);
            disp('... Finding optimal lamda parameter')
            for ( j=1:1:length(list_) )     
                X         = Bfunc(list_(j,1),TSM.tau)\TSM.yields';
                SSE_(j,:) = [ list_(j,1) sum(sum((TSM.yields-X'*Bfunc(list_(j,1),TSM.tau)').^2)) ];
            end
            indx       = find(SSE_(:,2)==min(SSE_(:,2)),1,'first');
            TSM.lambda = SSE_(indx,1);
            TSM.B      = Bfunc(TSM.lambda,TSM.tau);
            TSM.beta   = (TSM.B\TSM.yields');
            TSM.Yfit   = (TSM.B*TSM.beta)';
            TSM.e_t    = TSM.yields-TSM.Yfit;
            TSM.RMSE   = 100.*(mean((TSM.e_t).^2)).^(0.5); % multiplied by 100 to get in basis points
            TSM.Omega_yields = 1/(TSM.nObs-1).*(TSM.e_t'*TSM.e_t);
            
            %
            % ... Estimating the VAR(1) dynamics of the extracted factors
            %
            disp('... Estimating VAR(1) dynamics ')
            TSM = vare(TSM); 
            
            %            
            % ... Bias correcting - if specified to be done 
            %
            if (TSM.biasCorrect==1)
                disp('... Bias correcting the VAR parameters')
                TSM = pope(TSM);
            end
            
            %
            % ... Calculating Term Premia
            %
            disp('... Calculating expectations component and term premia ')
            TSM = TermPremia(TSM);
            disp('... Done ...')
        end
    
        %
        % ... The Dynamic Svensson-Soderlind Model
        %
        function [TSM] = getDSS(TSM)
            warning off all;
            disp('... DSS ...')
            TSM       = chkInput(TSM);
            TSM.model = 'DSS';
            TSM.A     = zeros(TSM.nTau,1);
            TSM.rho1  = [1;1;0;0;zeros(TSM.nVarExo,1)];
            Bfunc = @(lambda_,tau_) [ ones(TSM.nTau,1) (1-exp(-lambda_(1,1).*tau_))./(lambda_(1,1).*tau_) ...
                    (1-exp(-lambda_(1,1).*tau_))./(lambda_(1,1).*tau_)-exp(-lambda_(1,1).*tau_)           ... 
                    (1-exp(-lambda_(2,1).*tau_))./(lambda_(2,1).*tau_)-exp(-lambda_(2,1).*tau_) ];
            
            %
            % ... Finding the optimal lambda value on a grid of possible values
            %
            list_ = (0.01:0.01:0.30)';
            SSE_  = NaN(length(list_)^2,3);
            disp('... Finding optimal lamda parameters')
            z=1;
            for ( j=1:1:length(list_) )     
                for (k=1:1:length(list_))
                    L         = [list_(j,1); list_(k,1)];
                    X         = Bfunc(L,TSM.tau)\TSM.yields';
                    SSE_(z,:) = [ list_(j,1) list_(k,1) sum(sum((TSM.yields-X'*Bfunc(L,TSM.tau)').^2)) ];
                    z=z+1;
                end
            end
            indx       = find(SSE_(:,3)==min(SSE_(:,3)),1,'first');
            TSM.lambda = SSE_(indx,1:2)';
            TSM.B      = Bfunc(TSM.lambda,TSM.tau);
            TSM.beta   = (TSM.B\TSM.yields');
            TSM.Yfit   = (TSM.B*TSM.beta)';
            TSM.e_t    = TSM.yields-TSM.Yfit;
            TSM.RMSE   = 100.*(mean((TSM.e_t).^2)).^(0.5); % multiplied by 100 to get in basis points
            TSM.Omega_yields = 1/(TSM.nObs-1).*(TSM.e_t'*TSM.e_t);
            
            %
            % ... Estimating the VAR(1) dynamics of the extracted factors
            %
            disp('... Estimating VAR(1) dynamics ')
            TSM = vare(TSM); 
            
            %            
            % ... Bias correcting - if specified to be done 
            %
            if (TSM.biasCorrect==1)
                disp('... Bias correcting the VAR parameters')
                TSM = pope(TSM);
            end
            
            %
            % ... Calculating Term Premia
            %
            disp('... Calculating expectations component and term premia ')
            TSM = TermPremia(TSM);
            disp('... Done ...')
        end
        
        %
        % ... The Short-Rate Based Model with 3 factors
        %
        function [TSM] = getSRB3(TSM)
            disp('... SRB3 ...')
            TSM       = chkInput(TSM);
            TSM.model = 'SRB3';
            TSM.A     = zeros(TSM.nTau,1);
            TSM.rho1  = [1;0;0;zeros(TSM.nVarExo,1)];
            Bfunc = @(lambda_,tau_) [ ones(TSM.nTau,1) 1-(1-lambda_.^tau_)./((1-lambda_).*tau_) ...
                                                       -(lambda_.^(tau_-1))+(1-lambda_.^tau_)./((1-lambda_).*tau_) ];
            %
            % ... Finding the optimal lambda value on a grid of possible values
            %
            list_ = (0.70:0.001:0.999)';
            SSE_  = NaN(length(list_),2);
            disp('... Finding optimal lamda parameter')
            for ( j=1:1:length(list_) )     
                X         = Bfunc(list_(j,1),TSM.tau)\TSM.yields';
                SSE_(j,:) = [ list_(j,1) sum(sum((TSM.yields-X'*Bfunc(list_(j,1),TSM.tau)').^2)) ];
            end
            indx       = find(SSE_(:,2)==min(SSE_(:,2)),1,'first');
            TSM.lambda = SSE_(indx,1);
            TSM.B      = Bfunc(TSM.lambda,TSM.tau);
            TSM.beta   = (TSM.B\TSM.yields');
            TSM.Yfit   = (TSM.B*TSM.beta)';
            TSM.e_t    = TSM.yields-TSM.Yfit;
            TSM.RMSE   = 100.*(mean((TSM.e_t).^2)).^(0.5); % multiplied by 100 to get in basis points
            TSM.Omega_yields = 1/(TSM.nObs-1).*(TSM.e_t'*TSM.e_t);
            
            %
            % ... Estimating the VAR(1) dynamics of the extracted factors
            %
            disp('... Estimating VAR(1) dynamics ')
            TSM = vare(TSM); 
            
            %            
            % ... Bias correcting - if specified to be done 
            %
            if (TSM.biasCorrect==1)
                disp('... Bias correcting the VAR parameters')
                TSM = pope(TSM);
            end
            
            %
            % ... Calculating Term Premia
            %
            disp('... Calculating expectations component and term premia ')
            TSM = TermPremia(TSM);
            disp('... Done ...')
        end
        
        %
        % ... The Short-Rate Based Model with 4 factors
        %
        function [TSM] = getSRB4(TSM)
            disp('... SRB4 ...')
            TSM       = chkInput(TSM);
            TSM.model = 'SRB4';
            TSM.A     = zeros(TSM.nTau,1);
            TSM.rho1  = [1;0;0;0;zeros(TSM.nVarExo,1)];
            Bfunc = @(lambda_,tau_) [ ones(TSM.nTau,1) 1-(1-lambda_.^tau_)./((1-lambda_).*tau_) ...
                                                       -(lambda_.^(tau_-1))+(1-lambda_.^tau_)./((1-lambda_).*tau_) ...
                                                       -0.5.*(tau_-1).*(lambda_-1).*lambda_.^(tau_-2)];
            %
            % ... Finding the optimal lambda value on a grid of possible values
            %
            list_ = (0.70:0.001:0.999)';
            SSE_  = NaN(length(list_),2);
            disp('... Finding optimal lamda parameter')
            for ( j=1:1:length(list_) )     
                X         = Bfunc(list_(j,1),TSM.tau)\TSM.yields';
                SSE_(j,:) = [ list_(j,1) sum(sum((TSM.yields-X'*Bfunc(list_(j,1),TSM.tau)').^2)) ];
            end
            indx       = find(SSE_(:,2)==min(SSE_(:,2)),1,'first');
            TSM.lambda = SSE_(indx,1);
            TSM.B      = Bfunc(TSM.lambda,TSM.tau);
            TSM.beta   = (TSM.B\TSM.yields');
            TSM.Yfit   = (TSM.B*TSM.beta)';
            TSM.e_t    = TSM.yields-TSM.Yfit;
            TSM.RMSE   = 100.*(mean((TSM.e_t).^2)).^(0.5); % multiplied by 100 to get in basis points
            TSM.Omega_yields = 1/(TSM.nObs-1).*(TSM.e_t'*TSM.e_t);
            
            %
            % ... Estimating the VAR(1) dynamics of the extracted factors
            %
            disp('... Estimating VAR(1) dynamics ')
            TSM = vare(TSM); 
            
            %            
            % ... Bias correcting - if specified to be done 
            %
            if (TSM.biasCorrect==1)
                disp('... Bias correcting the VAR parameters')
                TSM = pope(TSM);
            end
            
            %
            % ... Calculating Term Premia
            %
            disp('... Calculating expectations component and term premia ')
            TSM = TermPremia(TSM);
            disp('... Done ...')
        end
        
        %
        % ... The JSZ model
        %
        function [TSM] = getJSZ(TSM)
            disp('... JSZ ...')
            [TSM]     = chkInput(TSM);
            TSM.rho0x = 0;                        % normalisation constr
            TSM.rho1x = ones(TSM.nF,1);           % normalisation constr
            TSM.nMax  = ( 1:1:max(TSM.tau)*TSM.DataFreq/12 )';
            TSM.W     = pcacov(cov(TSM.yields./100));
            TSM.W     = TSM.W(:,1:TSM.nF)';        % PCA weights
            TSM.beta  = TSM.W'\(TSM.yields./100)'; % PCA factors
           
            %
            % ... Estimating the VAR(1) dynamics of the extracted factors
            %
            disp('... Estimating VAR(1) dynamics under the P-measure ')
            TSM = vare(TSM); 
            
            %            
            % ... Bias correcting - if specified to be done 
            %
            if (TSM.biasCorrect==1)
                disp('... Bias correcting the VAR parameters')
                TSM = pope(TSM);
            end
            %
            % ... estimating the JSZ model
            %
            [fSSE_JSZ] = @(p0) JSZ_minSSE(p0,TSM);
            % ... finding good starting values
            disp('... Trying to find good starting values for the model estimation')
            nI   = 1000;
            tmp0 = NaN(nI,TSM.nF+2);
            for (zz=1:nI)
                g0          = 0.90 + (0.98-0.90).*rand(TSM.nF,1);
                p0          = [g0.*(ones(TSM.nF,1)); 0]; 
                etmp        = fSSE_JSZ(p0);
                tmp0(zz,:)  = [etmp p0'];
            end
            indx = find(min(tmp0(:,1))==tmp0(:,1),1,'first'); 
            p0   = tmp0(indx,2:end)';
            nP   = size(p0,1);        
            
            lb_ =  zeros(nP,1); lb_(1:TSM.nF,1) = 0.50.*ones(TSM.nF,1);
            ub_ =  inf(nP,1); ub_(1:TSM.nF,1) = 0.9999.*ones(TSM.nF,1);
            Aeq_ = [];
            beq_ = [];
            
            options = optimoptions(@fmincon,'Algorithm','interior-point',...
                                            'MaxIterations',1e6, ...
                                            'MaxFunctionEvaluations',1e6, ...
                                            'TolFun', 1e-12, 'TolX', 1e-12, ...
                                            'display','iter');
            
            disp('... Estimating the model ')                            
            [pHat] = fmincon(fSSE_JSZ,p0,[],[],Aeq_,beq_,lb_,ub_,[],options);   
            
            % ... generating the output
            [~,TSM]   = JSZ_minSSE(pHat,TSM);
            TSM.gamma = diag(TSM.PhiQ); 
            TSM.rho0  = (100/TSM.DataFreq)*(TSM.rho0x - TSM.rho1x'*(TSM.W*TSM.BX)^(-1)*TSM.W*TSM.AX);
            TSM.rho1  = ((100/TSM.DataFreq)*(TSM.rho1x'*(TSM.W*TSM.BX)^(-1)))';
            TSM.e_t   = TSM.yields-TSM.Yfit;
            TSM.RMSE  = 100*(mean(TSM.e_t.^2)).^(0.5);
            TSM.model = 'JSZ';
            [TSM]     = TermPremia(TSM);
            TSM.Omega_yields = 1/TSM.nObs*(TSM.e_t'*TSM.e_t);
        end
        
        %
        % ... The Arbitrage Free Short Rate Based Model with 2,3, or 4 Factors
        %
        function [TSM] = getAFSRB(TSM)
            disp('... AFSRB ...')
            [TSM]          = chkInput(TSM);
            TSM.model      = 'AFSRB';
            if ( ~ismember(TSM.nF,[2;3;4]) )
                disp('Error: the AFSRB model can only deal with 2,3, or 4 factors')
                disp('       please adjust the value of the .nF variable.')
            end
            TSM.rho0       = 0;                    % as dictated by the model
            TSM.rho1       = zeros(TSM.nF,1);
            TSM.rho1(1,1)  = 1;                    % as dictated by the model
            TSM.nMax       = ( 1:1:max(TSM.tau) )';            
            
            %
            % ... Finding the optimal shape parameter
            %
            g1 = ( 0.90:0.0001:0.99 )';
            RR = zeros(length(g1),2);
            disp('... Finding the shape parameter')
            for (k=1:length(g1))
                TSM.gamma = g1(k,1);
                [ TSM ]   = An_Bn_SRB(TSM);
                RR(k,:)  = [ TSM.gamma sum(sum((TSM.yields-(TSM.B*(TSM.B\TSM.yields'))').^2)) ];
            end    
            indx      = find(RR(:,2)==min(RR(:,2)),1,'first');
            TSM.gamma = RR(indx,1); 
            [ TSM ]   = An_Bn_SRB(TSM);
            TSM.beta = TSM.B\TSM.yields';
            %
            % ... Estimating the VAR(1) dynamics of the extracted factors
            %
            disp('... Estimating VAR(1) dynamics under the P-measure ')
            [ TSM ] = vare(TSM); 
            
            %            
            % ... Bias correcting - if specified to be done 
            %
            if (TSM.biasCorrect==1)
                disp('... Bias correcting the VAR parameters')
                TSM = pope(TSM);
            end
            
            %
            % ... Estimating mQ the remaining parameter
            %
            [fSSE_AFSRB] = @(p0) AFSRB_minSSE(p0,TSM);
            p0           = zeros(TSM.nF,1);
            lb_          = -10*ones(TSM.nF,1);
            ub_          =  10*ones(TSM.nF,1);
            Aeq_         = [zeros(TSM.nF-1,1) eye(TSM.nF-1,TSM.nF-1)];
            beq_         = zeros(TSM.nF-1,1); 
            options      = optimoptions(@fmincon,'Algorithm','interior-point',...
                                                 'MaxIterations',1e6, ...
                                                 'MaxFunctionEvaluations',1e6, ...
                                                 'TolFun', 1e-12, 'TolX', 1e-12, ...
                                                 'display','iter');
            
            disp('... Estimating the model ')                            
            [pHat] = fmincon(fSSE_AFSRB,p0,[],[],Aeq_,beq_,lb_,ub_,[],options);   
            [ SSE_AFSRB, TSM ] = AFSRB_minSSE(pHat,TSM);         
            % ... generating the output
            switch TSM.nF
                case 2
                    TSM.PhiQ = [ 1 1-TSM.gamma; 0 TSM.gamma];
                case 3
                    TSM.PhiQ = [ 1   1-TSM.gamma   1-TSM.gamma    ; ...
                                 0     TSM.gamma     TSM.gamma-1  ; ...
                                 0      0            TSM.gamma   ];
                case 4
                    TSM.PhiQ = [ 1   1-TSM.gamma   1-TSM.gamma    1-TSM.gamma    ; ...
                                 0     TSM.gamma     TSM.gamma-1    TSM.gamma-1  ; ...
                                 0      0            TSM.gamma      TSM.gamma-1  ;
                                 0      0             0             TSM.gamma   ];
                     
            end
            TSM.Yfit = (TSM.A + TSM.B*TSM.beta(1:TSM.nF,:))';
            TSM.e_t   = TSM.yields-TSM.Yfit;
            TSM.RMSE  = 100*(mean(TSM.e_t.^2)).^(0.5);
            [TSM]     = TermPremia(TSM);
            TSM.Omega_yields = 1/TSM.nObs*(TSM.e_t'*TSM.e_t);
        end
        
        %
        % ... The empirical model with factors: short rate, term premium 10Y,
        %                                   curvature 1 and 2
        function [TSM] = getSRTPC1C2(TSM)
            disp('... Short rate, TP-10Y, C1, C2 ...')
            [TSM] = chkInput(TSM);    
            TSM.model      = 'SRTPC1C2';
            TSM.nF         = 4;
            TSM.rho0       = 0;                    % as dictated by the model
            TSM.rho1       = zeros(TSM.nF,1);
            TSM.rho1(1,1)  = 1;                    % as dictated by the model
            TSM.nMax       = ( 1:1:max(TSM.tau) )';   
            TSM.A = zeros(TSM.nTau,1);
            SRB3  = TSM;
            SRB3  = SRB3.getSRB3;
            X1    = [ TSM.yields(:,1) SRB3.TP(:,11) ];
            b1    = X1\TSM.yields;
            err   = TSM.yields - X1*b1;
            mErr  = mean(err);
            [W,L] = pcacov( (err-mErr)'*(err-mErr) );
            beta_err = W(:,1:2)\err';                     % updated 2/11/2018
            TSM.beta = [X1 beta_err']';                   % updated 2/11/2018
            TSM.B    = [ b1' W(:,1:2)];
            TSM.model= 'SRTPC1C2';
            %TSM.beta = TSM.B\TSM.yields';
            %TSM.B    = (TSM.beta'\TSM.yields)';          
            %
            % ... Estimating the VAR(1) dynamics of the extracted factors
            %
            disp('... Estimating VAR(1) dynamics under the P-measure ')
            [ TSM ] = vare(TSM); 
            
            %            
            % ... Bias correcting - if specified to be done 
            %
            if (TSM.biasCorrect==1)
                disp('... Bias correcting the VAR parameters')
                TSM = pope(TSM);
            end
            TSM.Yfit = (TSM.A + TSM.B*TSM.beta)';
            TSM.e_t   = TSM.yields-TSM.Yfit;
            TSM.RMSE  = 100*(mean(TSM.e_t.^2)).^(0.5);
            [TSM]     = TermPremia(TSM);
            TSM.Omega_yields = 1/TSM.nObs*(TSM.e_t'*TSM.e_t); 
        end
        
        %
        % ... The empirical model with factors: short rate, term premium 10Y,
        %                                   curvature 1
        function [TSM] = getSRTPC1(TSM)
            disp('... Short rate, TP-10Y, C1 ...')
            [TSM] = chkInput(TSM);    
            TSM.model      = 'SRTPC1';
            TSM.nF         = 3;
            TSM.rho0       = 0;                    % as dictated by the model
            TSM.rho1       = zeros(TSM.nF,1);
            TSM.rho1(1,1)  = 1;                    % as dictated by the model
            TSM.nMax       = ( 1:1:max(TSM.tau) )';   
            TSM.A = zeros(TSM.nTau,1);
            SRB3  = TSM;
            SRB3  = SRB3.getSRB3;           
            X1    = [ TSM.yields(:,1) SRB3.TP(:,11) ];
            b1    = X1\TSM.yields;
            err   = TSM.yields - X1*b1;
            mErr  = mean(err);
            [W,L] = pcacov( (err-mErr)'*(err-mErr) );
            beta_err = W(:,1)\err';                     % updated 2/11/2018
            TSM.beta = [X1 beta_err']';                   % updated 2/11/2018
            TSM.B    = [ b1' W(:,1)];         
            %
            % ... Estimating the VAR(1) dynamics of the extracted factors
            %
            disp('... Estimating VAR(1) dynamics under the P-measure ')
            [ TSM ] = vare(TSM); 
            %            
            % ... Bias correcting - if specified to be done 
            %
            if (TSM.biasCorrect==1)
                disp('... Bias correcting the VAR parameters')
                TSM = pope(TSM);
            end
            TSM.Yfit = (TSM.A + TSM.B*TSM.beta)';
            TSM.e_t   = TSM.yields-TSM.Yfit;
            TSM.RMSE  = 100*(mean(TSM.e_t.^2)).^(0.5);
            [TSM]     = TermPremia(TSM);
            TSM.Omega_yields = 1/TSM.nObs*(TSM.e_t'*TSM.e_t); 
        end        
        %
        % ... The shadow-short rate model with three factors
        %
        function [TSM] = getSSR3(TSM)
            %
            TSM.model = 'SSR3';
            
            % ... step 0: fix the parameters that need to be fixed
            %
            rL = 0.00;               % preset effective lower bound  

            % ... step 1: estimate the 3-factors from the short rate based model
            %
            TSM  = TSM.getSRB3;                   
            X_   = TSM.beta;          % factors: short rate, slope, curvature
            B_   = TSM.B;             % loading structure
            Y_   = TSM.yields;        % observed yields (also contained in Y)
            nObs = TSM.nObs;
            % ... step2: estimate the shadow short rate and the shadow slope
            %
            X_tmp = X_';
            p0    = X_tmp(:);
            Aeq   = zeros(nObs,3*nObs);
            Aeq(1:nObs,2*nObs+1:end) = eye(nObs);
            beq = X_(3,:)';
            
            TSM.Y_ = Y_;
            TSM.B_ = B_;
            TSM.rL = rL;
            %X_shadow = NaN(size(X_));
            options_ = optimoptions(@fmincon,'Algorithm','sqp',...
                                         'MaxIterations',1e8, ...
                                         'MaxFunctionEvaluations',1e8, ...
                                         'TolFun', 1e-4, 'TolX', 1e-4, ...
                                        'display','iter');

            FX_min         = @(p) Yshadow( p, TSM );
            [ pHat_sr ]    = fmincon(FX_min, p0,[],[],Aeq,beq,[],[],[], options_);
            alpha_         = pHat_sr(1:4,1);
            X_shadow_hat   = reshape(pHat_sr,nObs,3);
            
            %
            % generating output
            %
            [ ~, TSM ]   = Yshadow(pHat_sr,TSM);
            TSM.TP = [];
            TSM.Er = [];
        end   
    end
end

