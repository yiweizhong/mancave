%% Nelson-Siegel type yield curve models
% This script shows how to use a couple of newly developed MATLAB 
% (object-oriented) classes. These are:
%
%          - GSW      (Gurkaynak, sack, and Wright) conversion of
%                      Svennson-Soderlind yield curve factors into yields
%          - TSM      (A set of dynamic Nelson-Siegel type models)
%          - yieldFit (basically a wrapper around the MATLAB built-in
%                      functionalities to fit single yield curves, i.e.
%                      making the 'IRFunctionCurve' functionality easier
%                      to use)
%
% Please send discovered bugs to: Ken.Nyholm@ECB.int
% June 2018
%
% The document is organised in the following way: (1) GSW factors are 
% loaded from a pre-stored MATLAB workspace file, then these factors are 
% converted into yield curves for a set of specified maturities. (2) The
% available members of the dynamic Nelson-Siegel model family are estimated
% and the resulting term-premium estimates are compared. (3) Term
% structures are fitted to a couple of the found Expectations Components
% and Term Premia curves (i.e. curves are fitted to a given day's risk free
% term structure and term premium structure) and corresponding instantaneous 
% forward curves are calculated.
%
%% The GSW Class
% GSW factors are loaded from an existing MATLAB workspace [EX_data]. The
% GSW factors are called [Factors].
load EX_data;

%%
% Information on the GSW class can be obtained using the command [help GSW],
% and a neat summary is seen when clicking the "Reference page for GSW" 
% link - this summary helps in finding the key words that need to be used
% when populating the class instance with data and to know which functions 
% (called methods) that can be envoked. 
%
help GSW
%
GSW1  = GSW;  % create instance of the GSW class called GSW1. Going forward,
              % it is therefore the GSW1 instance that we will work with.

dates = x2mdate(Factors(:,1));  % the first column contains the date vector.  
              
GSW1.beta = Factors(:,2:5);   % the first 4 columns of Factors contain the 
                              % Svensson-Soderlind yield curve factors:
                              % level, -slope, curvature1, curvature2

GSW1.lambda = Factors(:,6:7); % columns 5 and 6 contain the values of the 
                              % time-decay parameters 
                              
% Note that the maturities at which Svensson-Soderling yields are calculated,
% are specified in GSW1.tau. This input is pre-filled with typical values,
% but it can naturally be changed if needed. For now, we stick with the 
% standard values of (denominated in months):
GSW1.tau

% The yield curve for each date covered by the yield curve factors, and 
% maturity spectrum, can now be calculated:
GSW1 = GSW1.getYields;

% And, the output is plotted. Note that only data from 1980 and onwards are
% included here.
figure
    plot(dates,GSW1.yields), title('US yields'), ylabel('Percentage')
    datetick('x','mmm-yy')

%% The TSM class
% Four term structure models within the family of Dynamic Nelson-Siegel
% models are implemented: Nelson-Siegel, Svensson-Soderlind, SRB-3, and 
% SRB-4. The latter two are Short Rate Based models with 3 and 4 factors:
% these are models where the factors are rotated to represent the short 
% rate and the slope of the yield curve (and the curvature).
%
% As above, [help TSM] will provide information on this class.
%
TSM1 = TSM;    % four instances of the TSM class are created.
TSM2 = TSM;    % And, each instance is populated with the relevant data
TSM3 = TSM;    % and option settings.
TSM4 = TSM;

TSM1.yields   = GSW1.yields;   % only two inputs need to be specified for
TSM2.yields   = GSW1.yields;   % the TSM class: yield data and data frequency
TSM3.yields   = GSW1.yields;  
TSM4.yields   = GSW1.yields;  
TSM1.DataFreq = 12;            % Data frequency:  12=monthly
TSM2.DataFreq = 12;  
TSM3.DataFreq = 12;  
TSM4.DataFreq = 12;  
TSM1.nF       = 3;             % Number of yield curve factors to include
TSM2.nF       = 4;
TSM3.nF       = 3;
TSM4.nF       = 4;

% To illustrate the available options, we let the mean of the VAR(1) model 
% in TSM1 be pre-specified to some ad-hoc values. A vector with 3 inputs 
% is specified, because we want to use TSM1 for estimating a DNS model. 
% By pre-specifying means, corresponding constraints are imposed on the 
% estimation of the VAR parameters.
% 
TSM1.mP_pre = [ 4.25;-1.5;-1.25 ];

% TSM2 is intended for the estimation of a DSS model, and to see the impact
% of bias correction of the VAR model, we set a flag for bias correction in
% the model instance.
TSM2.biasCorrect=1;

% The models can now be estimated.
TSM1 = TSM1.getDNS;
TSM2 = TSM2.getDSS;
TSM3 = TSM3.getSRB3;
TSM4 = TSM4.getSRB4;

% And, again for illustrative purposes only, the 10y term premium estimates 
% from each model is compared. The ACM 10y term premium is included for 
% comparison. 
%
figure
    plot(dates,[ TSM1.TP(:,11) TSM2.TP(:,11) TSM3.TP(:,11) TSM4.TP(:,11) ]),
    hold on
    plot(dates,ACM_TP10(:,2))
    datetick('x','mmm-yy')
    legend({TSM1.model TSM2.model TSM3.model TSM4.model 'ACM'}), 
    title('Term Premia, 10Y')
    







