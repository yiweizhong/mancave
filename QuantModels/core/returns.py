""" A collection of return calculations
"""
import numpy as np
import pandas as pd

def simple_level_diff(data, offset):
    """ calculate the n period/disposition level changes
    Args:
        data (pandas series, pandas dataframe or numpy 1d array): The data is
        expected to be sorted in ascending order based on time. Oldes at the
        front and newest at the end
        offset (int):
    Returns:
        an object with same type as data representing the difference
    """
    if (isinstance(data, pd.Series) or isinstance(data, pd.DataFrame)) \
        and len(data.index) > offset:
        return (data - data.shift(offset)).reindex(data.index)
    elif (isinstance(data, np.ndarray) & data.ndim == 1) \
        and len(data) > offset:
        return np.append(np.array([np.NaN] * offset),
                         data[offset:] - data[:-1 * offset])
    else:
        raise TypeError("The data input needs to be a pandas time series,"
                        + "pandas dataframe or 1d numpy array"
                        + "the offset value needs to be smaller than"
                        + "the lengh of the data")


def simple_pct_return(data, offset):
    """ calculate the n period/disposition pct return
    Args:
        data (pandas series, pandas dataframe or numpy 1d array): The data is
        expected to be sorted in ascending order based on time. Oldes at the
        front and newest at the end
        offset (int):
    Returns:
        an object with same type as data representing the difference
    """
    if (isinstance(data, pd.Series) or isinstance(data, pd.DataFrame)) \
        and len(data.index) > offset:
        return ((data - data.shift(offset))/data.shift(offset)).reindex(data.index)
    elif (isinstance(data, np.ndarray) & data.ndim == 1) \
        and len(data) > offset:
        return np.append(np.array([np.NaN] * offset),
                         (data[offset:] - data[:-1 * offset])/data[:-1 * offset])
    else:
        raise TypeError("The data input needs to be a pandas time series,"
                        + "pandas dataframe or 1d numpy array"
                        + "the offset value needs to be smaller than"
                        + "the lengh of the data")



