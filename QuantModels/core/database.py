import pandas as pd
import pyodbc
from sqlalchemy import create_engine
import datatools as ind
import urllib
import logging
import numpy as np
import datetime as dt
import unicodedata
import common.dirs as dirs
import os, sys, inspect

TIMESTAMP = r"%Y-%m-%d %H:%M:%S.%f"


QRY_INSERT_JOB_STATUS = r"INSERT INTO [dbo].[Macro_Batch_Job_History] ([TimeStampKey],[TaskID],[TaskName],[RunDate],[ReturnCode],[LogFile]) " + \
                        r"VALUES (?, ?, ?, ?, ?, ?)"

DATE = r"DATE"


class Database(object):

    def __init__(self, raw_odbc_cstr, debug_folder = None, output_folder = None):
        logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
        self._cstr = raw_odbc_cstr
        self._odbc_cstr_prefix = r"mssql+pyodbc:///?odbc_connect=%s"
        self._debug_folder = debug_folder
        self._db_engine = self._create_db_engine()


    def _create_db_engine(self):
        params = urllib.quote_plus(self._cstr)
        engine = create_engine(self._odbc_cstr_prefix % params, echo=False)
        return engine


    #
    def update_job_status(self, task_id,task_name, run_date, return_code, log_file, time_stamp = dt.datetime.now().strftime(TIMESTAMP)):
        logging.debug("about to updated_job_status to db")
        qstring = r"INSERT INTO [dbo].[Macro_Batch_Job_History] ([TimeStampKey],[TaskID],[TaskName],[RunDate],[ReturnCode],[LogFile]) " +\
            r"VALUES ('{0}', '{1}', '{2}', '{3}', {4}, '{5}')".format(time_stamp, task_id,task_name, run_date, return_code, log_file)
        self.execute_nonquery(qstring)
        logging.debug("finish updated_job_status to db")
        return


    @classmethod
#------------------------------------------------------------------------------------------------
    def get_db_instance(cls, raw_odbc_cstr, debug_folder = None):
#------------------------------------------------------------------------------------------------
        """
        Factory method to get a initialised Database class object
        :param raw_odbc_cstr: a string representing the connection string
        :param debug_folder: a string representing the folder where the log and debug info will go
        :return: the class object
        """
        db_instance = Database(raw_odbc_cstr,debug_folder=debug_folder)
        return db_instance



#------------------------------------------------------------------------------------------------
    def query(self, query, key_col=None):
#------------------------------------------------------------------------------------------------
        def unicode_to_str(unicode_val): return unicodedata.normalize('NFKD', unicode_val).encode('ascii','ignore')
        df = pd.read_sql_query(query, self._db_engine)
        str_col_names = [unicode_to_str(c) for c in df.columns.tolist()]
        df_new = df.copy()
        df_new.columns = str_col_names
        if key_col is not None:
            df_new = df_new.set_index(key_col)
        logging.info("Executed query %s" % query)
        return df_new


#------------------------------------------------------------------------------------------------
    def load_multi_tables_from_db(self,
                                  input_source,
                                  expected_cols=None,
                                  key_col=DATE,
                                  debug_file_name_token ="",
                                  debug=False):
#------------------------------------------------------------------------------------------------
        """
        load the data from database. This function updates the self._market_data_raw dict with the loaded market data
        :param input_source: a list of tuple containing {"The name of the object pointing to df", "database table name", "a function that will be applied to the raw df queried from the database"}
        :param expected_cols: a list of column names
        :param key_col: The key column name of the df. This column name should be "DATE"
        :return: a dict of market data keyed by data name label
        """
        logging.info("===> Start to load market data from db <===")
        result_dict = {data_key : self._load_straight_from_db(data_key,
                                                             query,
                                                             debug_file_path= self.db_data_dump(debug_file_name_token + data_key),
                                                             key_col = key_col, #this will break Macro risk code
                                                             expected_cols = expected_cols,
                                                             post_load_func = post_load_func,
                                                             debug=debug)
                       for data_key, query, post_load_func in input_source}

        logging.info("===> Finish loading market data from db <===")
        return result_dict



#------------------------------------------------------------------------------------------------
    def load_single_table_from_db(self,
                                  query,
                                  data_key,
                                  post_load_func=None,
                                  expected_cols=None,
                                  key_col=DATE,
                                  debug_file_name_token ="",
                                  debug=False):
#------------------------------------------------------------------------------------------------
        """
        load the market data from database via a single query.
        :param db: an object of the database class
        :param query: an string containing the query
        :param expected_cols: a collection of strings representing the column header in the output df. if a column does not exist
                              in the df produced by the query an empty column will be inserted into the df as a place holder
        :param key_col: a string containing the column name that is the key for the output df. For most of the progam it is "DATE"
        :return: a dataframe
        """
        logging.info("===> Start to load_single_table_from_db <===")

        result = self._load_straight_from_db(data_key, query,
                                            debug_file_path= self.db_data_dump(debug_file_name_token + data_key),
                                            key_col = key_col,
                                            expected_cols = expected_cols,
                                            post_load_func = post_load_func,
                                            debug=debug)

        logging.info("===> Finish loading load_single_table_from_db <===")

        return result



#------------------------------------------------------------------------------------------------
    def _load_straight_from_db(self,
                               data_key,
                               query,
                               debug_file_path = None,
                               key_col=None,
                               expected_cols=None,
                               post_load_func =None,
                               debug = False):
#------------------------------------------------------------------------------------------------

        df = self.query(query,key_col=key_col) if key_col is not None else self.query(query)

        df = ind.recolumn_df_data(df.copy(), cols = expected_cols) if expected_cols is not None else df

        df = post_load_func(df.copy()) if post_load_func is not None else df

        if debug and (debug_file_path is not None):
            #df.to_csv(debug_file_path,  float_format='%.2f')
            df.to_csv(debug_file_path)
            logging.debug("%s - data load debug file dump: %s" % (data_key,debug_file_path))
        else:
            logging.debug("%s - data load debug file dump is skipped" % (data_key))
        return df



#------------------------------------------------------------------------------------------------
    def load_data_from_csvs(self, input_source, debug_file_name=None, key_col=DATE, get_file_path = None):
#------------------------------------------------------------------------------------------------
        """
        load the market data from database via a single query.
        :param path:
        :param input_source:  a list of tuple containing {"The name of the input file", "database table name", "a function that will be applied to the raw df queried from the database"}
        :param debug_file_name:
        :param key_col: a string containing the column name that is the key for the output df. For most of the progam it is "DATE"
        :param get_file_path: a function that will derive the file path using the the file name from the input_source.
               if it is None the file name from the input source is assumed to be the full file path
        :return: a dataframe
        """
        logging.info("===> Start to load market data from files <===")

        result_dict = { data_key: self._load_straight_from_csv(data_key,
                                                               datafile_path = (data_key if get_file_path is None else get_file_path(data_key)),
                                                               key_col= key_col,  # this will break Macro risk code
                                                               post_load_func = post_load_func)
                        for (data_key, query, post_load_func) in input_source}

        logging.info("===> Finish loading market data from files <===")
        return result_dict



#------------------------------------------------------------------------------------------------
    def _load_straight_from_csv(self, data_key, datafile_path = None, debug_file_name = None, key_col = DATE, expected_cols=None, post_load_func = None, debug=False):
#------------------------------------------------------------------------------------------------
        """
        :param data_key:
        :param file_path:
        :param debug_file_name:
        :param key_col:
        :return: a dataframe containing the csv data
        """
        df = pd.read_csv(datafile_path)

        if key_col == DATE:
            df[key_col] = pd.to_datetime(df[key_col])

        if key_col is not None:
            df = df.set_index(key_col)

        df = ind.recolumn_df_data(df.copy(), cols = expected_cols) if expected_cols is not None else df

        df = post_load_func(df.copy()) if post_load_func is not None else df

        if debug and (debug_file_name is not None):
            df.to_csv(debug_file_name, index=True, index_label=DATE)
            logging.debug("%s - data load debug file dump: %s" % (data_key,debug_file_name))
        else:
            logging.debug("%s - data load debug file dump is skipped" % (data_key))
        return df




#------------------------------------------------------------------------------------------------
    def db_data_dump(self, file_name):
#------------------------------------------------------------------------------------------------
        #return MConfig.get_output_file_name(MConfig.get_debug_folder(cls._debug_folder), file_name + Meta.CSV)
        return os.path.join(self._debug_folder,  "%s.%s" % (file_name,"CSV"))


#------------------------------------------------------------------------------------------------
    def execute_nonquery(self, statement):
#------------------------------------------------------------------------------------------------
        #e = db_engine
        """
        :param noquery statement:
        :return:
        """

        conn = self._db_engine.connect()
        t = conn.begin()
        try:
            conn.execute(statement)
            t.commit()
            conn.close()
        except:
            t = t.rollback()
            raise
        finally:
            conn.close()
            



    #------------------------------------------------------------------------------------------------
    def create_ddl_from_panda_df(self, df_in, db_name, db_schema, db_table_name, include_index = True, index_column_name_override=None):
    #------------------------------------------------------------------------------------------------
        """
        :param df: the dataframe containing the data to be loaded into the db
        :param df_name: the database name
        :param db_schema: the database schema name
        :param db_table_name: the database table name
        :param include_index: optional flag to include index as a column for the ddl
        :param index_column_name_override: optional name of the index column. if it is provided the index will be used as the column name for the index column
        :return: a dataframe containing the csv data
        """

        df = df_in.copy()

        # if index_column_name_override is not None:
        #     #df.loc[:,(convert_index_to_column_name)] = df.index
        #     df.insert(0, index_column_name_override, df.index)



        index_tuple = ([(df.index.name, df.index.dtype)] if index_column_name_override is None else [(index_column_name_override, df.index.dtype)])
        column_tuple = [(column_name, df[column_name].dtype) for column_name in df.columns.tolist()]
        column_tuple =  (index_tuple + column_tuple) if include_index else column_tuple

        def translate_data_type(column_dtype):
            switcher = {
                np.dtype(r"datetime64[ns]") : "[datetime] null",
                np.dtype(r"<M8[ns]"): "[datetime] null",
                np.dtype(r"O"): "[varchar] (100) null",
                np.dtype(r"float64"): "float null",
                np.dtype(r"int64"): "int null"
                }
            result = switcher.get(column_dtype)
            if result is None:
                raise ValueError("{0} has not configured dll type translation".format(column_dtype.__name__))
            return result

        ddl1 = "USE [{0}]  ".format(db_name)
        ddl2 = "IF OBJECT_ID('[{0}].[{1}]', 'U') IS NOT NULL DROP TABLE [{0}].[{1}] ".format(db_schema, db_table_name)
        ddl3 = "CREATE TABLE  [{0}].[{1}]( ".format(db_schema, db_table_name)
        ddl4 = ["[{0}] {1} ".format(column_name, translate_data_type(column_dtype)) for column_name, column_dtype in column_tuple]
        ddl5 = " ) ON [PRIMARY]"

        return " ".join([ddl1, ddl2, ddl3, ", ".join(ddl4), ddl5])




    @property
    def connection_string(self):
        return self._cstr


    @property
    def connection_string(self):
        return self._db_engine
