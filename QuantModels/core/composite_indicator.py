# from __future__ import division
# import pandas as pd
# import numpy as np
# import datetime as dt
# from dateutil.relativedelta import relativedelta
# from data import datatools as tools
# from common import dirs as dirs
# import logging
#
# MIN_WINDOW = r"min_window"
# DEBUG = r"DEBUG"
# CSV = r".CSV"
# OUTER = r"outer"
# FFILL= r"ffill"
# DATE = r"DATE"
#
# #ROW_AVG_COL_NAME = "ROW_AVG_COL_NAME"
#
#
# def validate_composite_indicator_raw_data(raw_data, country_list, sec_list):
#     """This function validates the raw_data input:
#
#     raw_data is expected to be a dictionary object.
#     The keys are the name of the security.
#     The columns of each dataframe are expected to be the name of the countries.
#     Each dataframe is expected to have the same number of columns
#
#     Args:
#         raw_data (dictionary of dataframe): The raw market data
#
#     Returns:
#         a tuple of a list of securities included in the raw data and a list of countries for each of security.
#
#     Raises:
#         ValueError: If raw_data is not in the
#     """
#
#     if isinstance(raw_data, dict):
#         logging.info("raw_data is a dict obj")
#     else:
#         logging.error("raw_data is expected to be a dict obj")
#         raise TypeError("raw_data is expected to be a dict obj")
#
#     if len(raw_data) > 0:
#         logging.info("raw_data is NOT empty")
#     else:
#         logging.error("raw_data is expected to have at least 1 df")
#         raise TypeError("raw_data is expected to have at least 1 df")
#
#
#     # pick the first df out of the dict to be used as the template for the empty df
#     dummy_df = tools.create_empty_numeric_df(raw_data.values()[0].index, country_list)
#
#     dict_out = {sec: tools.recolumn_df_data(df,country_list) for sec, df in raw_data.iteritems()}
#
#     for sec in sec_list:
#         if sec not in dict_out.keys():
#             dict_out[sec] = dummy_df.copy()
#
#     return dict_out
#
# def validate_list(input):
#     if not (isinstance(input, list) and len(input) >0):
#         raise TypeError("The input is expected to be a NONE empty list object")
#
# class CompositeIndicatorProcessor(object):
#     """ CompositeIndicator data structure
#     A composite Indicator has 3 dimensions: dates, securities and countries(regions). Each composite indicator contains different viewes made of the combinations the dimensions:
#     Args:
#         debug_folder (string): A string var holding the folder to dump the temp file for debuggging purpose
#         raw_data (dict[df]): A dict of dataframes containing the raw input data. Each entry is keyed by the name of the security/dataframe. Each column of the dataframe is a country
#                  {
#                     equity: df['AUD', ... 'USD' ...]
#                     .....
#                  }
#         dictkey_list(list(string)): The key pointing to the dataframe made up the rawdata.
#         For example it is the security type while the dataframe column is the country
#         column_list (list(string)): The columns from the dataframe stored in the raw_Data dictionary
#         For example it is the security type while the dataframe column is the country
#     Attributes:
#         code (int): Exception error code.
#     """
#
#     def __init__(self, debug_folder, raw_data, column_list, dictkey_list, debug=False):
#
#         validate_list(column_list)
#         validate_list(dictkey_list)
#
#         # make sure the data structure have right space holder for all the country and security
#         self._raw_data = validate_composite_indicator_raw_data(raw_data, column_list, dictkey_list)
#
#         self._dictkey_list = dictkey_list
#         self._keyed_df_column_list = column_list
#         self._debug_folder = debug_folder
#         self._debug = debug
#
#         logging.info("Done creating CompositeIndicator object")
#
#     def calc_country_based_security_composite_level_indicator(self, window, min_window, **kwargs):
#         """ Standardise each of the dataframe from the input dict
#         Args:
#             window (int): the siz of the rolling window.
#             min_window (int): the siz of the minimal amount of valid data need to be in the rolling window.
#             *args: Variable length argument list.
#             **kwargs: Arbitrary keyword arguments.
#
#         Returns:
#             a dict keyed by the country and the value is a single column dataframe containing weighted average of the standardised security series.
#         """
#         _min_window = kwargs[MIN_WINDOW] if MIN_WINDOW in kwargs else window
#
#         _standardised_security_keyed_country_columned_df_dict = self.rolling_standardise_data(self._raw_data,
#                                                                                              window, min_window=_min_window,
#                                                                                              debug_file_name_token= r"RAW_DATA_{0}D_ZSC".format(window)
#                                                                                              )
#         _output = self.reshape_data_swap_dictkey_with_dfcols(_standardised_security_keyed_country_columned_df_dict,
#                                                                                              debug_file_name_token= r"{0}DAY_ZSC_RESHAPED".format(window)
#
#                                                                                              )
#
#         return _output
#
#     def rolling_standardise_data(self, raw_data, window, min_window = None, fill_missing_method=r"ffill", debug_file_name_token =r"RAW_DATA_ZSC_D", debug_override = None):
#         """ Standardise each of the dataframe from the input dict
#         Args:
#             raw_data: a dict of dfs. For example a dict of dataframe while the key is the security and df columns are the countries
#             window (int): the siz of the rolling window.
#             min_window (Optional[int]): The min number of valid data points in a window
#             debug_file_name_token(string): a string token to identify the debug file
#             debug_override (bool): a local debug flag to override the class level debug settings
#
#         Returns:
#             a dict containing standardised dataframe: True if successful, False otherwise.
#         """
#
#         _debug = self._debug if debug_override is None else debug_override
#
#         def std(df, w, mw, data_label):
#             logging.debug("Standardise [{0}] df".format(data_label))
#             rolling_zcs_df = tools.rolling_standardise_table(df, window_size=window, min_window_size=mw, fill_missing_method = fill_missing_method)
#             if _debug:
#                 f = dirs.get_file_path(self._debug_folder, data_label + "_" + debug_file_name_token + CSV )
#                 rolling_zcs_df.to_csv(f)
#             return rolling_zcs_df
#
#         _ouput =  {data_label : std(df, window, min_window, data_label)  for data_label, df in raw_data.iteritems()}
#
#         return _ouput
#
#
#
#
#     def reshape_data_swap_dictkey_with_dfcols(self, dictofdfs, debug_file_name_token =r"", debug_override = None):
#         """ This function turns the takes in a dict of dataframes. For example a dicted keyed by security and each underline df is columned by the countries
#          This function will swap the dict key with the df columns. So the output data structure will be a dict keyed on country and underline df pointed at by
#          the keys are columned by security type
#         Args:
#             dictofdfs: a dict of dfs
#             debug_file_name_token(string): a string token to identify the debug file
#             debug_override (bool): a local debug flag to override the class level debug settings
#         Returns:
#             a dict of dfs, after switching the dict key with the underline df columns
#         """
#
#         _debug = self._debug if debug_override is None else debug_override
#
#         def find_col_data_series(df, column, dict_key):
#             if column in df.columns.tolist():
#                 logging.debug(" Col(country) %s has %d dps for %s (security)" % (column, len(df[column].dropna()), dict_key))
#                 return pd.DataFrame({dict_key : df[column]})
#             else:
#                 logging.debug(" Col %s has NO dps for %s" % (column, dict_key))
#                 # return an empty df as the place holder
#                 return pd.DataFrame(index = df.index, columns=[dict_key]).astype("float64")
#
#
#         def create_per_col_df(dictofdfs, column):
#             country_security_data = [find_col_data_series(df, column, dict_key) for dict_key, df in dictofdfs.iteritems()]
#             country_security_df = pd.concat(country_security_data, join=OUTER, axis=1).fillna(method=FFILL)[self._dictkey_list]
#
#             if _debug:
#                 f = dirs.get_file_path(self._debug_folder, column + "_" + debug_file_name_token + CSV)
#                 country_security_df.to_csv(f)
#
#             return country_security_df
#
#         _output = {column:create_per_col_df(dictofdfs, column) for column in self._keyed_df_column_list}
#
#         return _output
#
#     def weighted_aggregate_cols_within_df(self, input_data, columns_weights, ignore_raw_wtih_missing_data = False, debug_file_name_token =r"wavg_of_cols", debug_override = None):
#         """ This function weighted aggregates the columns of the dfs. The dfs are stored in a dict
#         Args:
#             input_data (dict(df)): A dictionary of dataframes. There are two level of indexers:
#                                    The dict key level indexer - dict key (like a security).
#                                    And the df level indexer - df columns (like a country).
#             columns_weights (list(int)): a list containing the weights used to aggregate the df columns
#             debug_override (bool): a local debug flag to override the class level debug settings
#
#         Returns:
#             a dict with its underline dfs containing single column dfs. The single column is the result of aggregating all the
#             the columns
#         """
#
#         _debug = self._debug if debug_override is None else debug_override
#
#         validate_list(columns_weights)
#
#         def func(df, name_of_avged_col, column_weights, ignore_raw_wtih_missing_data):
#
#             if (len(df.columns) != len(columns_weights)): raise ValueError("Different sizes between df [{0}] and df columns weights".format(name_of_avged_col))
#
#             _func_result = tools.weighted_avg_df_columns(df,
#                                                          name_of_avged_col,
#                                                          weights = columns_weights,
#                                                          use_full_row_only = (not ignore_raw_wtih_missing_data)  # this is pretty bad!
#                                                          )
#
#             if _debug:
#                 f = dirs.get_file_path(self._debug_folder, name_of_avged_col + "_" + debug_file_name_token + CSV)
#                 _func_result.to_csv(f)
#
#             return _func_result
#
#         _output = {key : func(df, key, columns_weights, ignore_raw_wtih_missing_data) for key, df in input_data.iteritems()}
#
#         return _output
#
#     def weighted_aggregate_dfs_within_dict(self, input_data, columns_weights, dict_item_weights, debug_file_name_token =r"wavg_of_dfs", ignore_raw_wtih_missing_data=False, debug_override = None):
#
#         """ This function first weighted-aggregates the columns of the dfs.
#         Then it weighted aggregates all the single column dfs
#         This function is a step further of the weighted_aggregate_cols_within_df
#
#         Args:
#             input_data (dict(df)): A dictionary of dataframes. There are two level of indexers:
#                                    The dict key level indexer - dict key (like a security).
#                                    And the df level indexer - df columns (like a country).
#             columns_weights (list(int)): a list containing the weights used to aggregate the df columns
#             dict_item_weights (list(int)): a list containing the weights used to aggregate all the items (dfs) in the dict
#             debug_override (bool): a local debug flag to override the class level debug settings
#
#         Returns:
#             a dict with a containing a single column df. The single column is the result of aggregating all the dfs (which are the aggregation of all the columns in
#             each df)
#         """
#
#         _debug = self._debug if debug_override is None else debug_override
#
#         validate_list(dict_item_weights)
#
#         if (len(input_data) != len(dict_item_weights)): raise ValueError("Different sizes between dict and dict item weights")
#
#         _dfwavg = self.weighted_aggregate_cols_within_df(input_data, columns_weights, ignore_raw_wtih_missing_data, debug_override= _debug)
#         # _dfwavg - a dict of single column dfs
#         _df_comb_single_cols =  pd.concat(_dfwavg.values(), join='outer', axis=1).fillna(method=FFILL)
#         _df_comb_single_cols.index.name = DATE
#
#         if _debug:
#             f = dirs.get_file_path(self._debug_folder, debug_file_name_token + CSV )
#             _df_comb_single_cols.to_csv(f)
#
#         # call weighted_aggregate_cols_within_df a second time with a dummy dict that contains only 1 df which is the combined single columns from previous weighted_aggregate_cols_within_df call
#         # the weights being used this time is dict_item_weights
#
#         _output = self.weighted_aggregate_cols_within_df({ debug_file_name_token : _df_comb_single_cols}, dict_item_weights, ignore_raw_wtih_missing_data, debug_override= _debug)
#         return _output
#
#
#     def unpivot_data(self,
#                      data_frame_dict,
#                      id_column_name,
#                      unpvted_column_names,
#                      particiating_column_names = None,
#                      derive_id_column_from_row_idx = True,
#                      debug_file_name_token =r"UNPVT_DATA",
#                      debug_override = None):
#         """ Unpivot each of the dataframe from the input dict
#         Args:
#             data_frame_dict (a dict of dataframe): a dict of dfs which will be unpvioted
#             id_column_name (string): the column name from the origianl dataframe that will be used unpivot each row. It will be part of the unpivoted join key
#             unpvted_column_names (a list of string): the column names will be used to name the unpvted df. they need to be database friendly
#             particiating_column_names (Optional[a list of string]): optional parameter to select participating columns from the original df into the unpvt process.
#                                                           if it is ignored all columns will be used.
#             derive_id_column_from_row_idx(Optional[bool]): optional parameter to copy the index column back into the df as a normal column
#             debug_file_name_token(string): a string token to identify the debug file
#             debug_override (bool): a local debug flag to override the class level debug settings
#
#         Returns:
#             a dict containing unpivoted dataframe.
#         """
#
#         _debug = self._debug if debug_override is None else debug_override
#
#         def unpivot(df, data_label):
#             logging.debug("Unpivot [{0}] df".format(data_label))
#             unpivoted_df = tools.unpivot_df(df,id_column_name, unpvted_column_names,particiating_column_names=particiating_column_names,derive_id_column_from_row_idx=derive_id_column_from_row_idx)
#
#             if _debug:
#                 f = dirs.get_file_path(self._debug_folder, data_label + "_" + debug_file_name_token + CSV )
#                 unpivoted_df.to_csv(f)
#             return unpivoted_df
#
#         _ouput =  {data_label : unpivot(df, data_label) for data_label, df in data_frame_dict.iteritems()}
#
#         return _ouput
#
#
#
#     @property
#     def raw_data(self, debug_override=None):
#
#         _debug = self._debug if debug_override is None else debug_override
#
#         if _debug:
#             logging.info("Raw data:\n")
#             for sec, df in self._raw_data.iteritems():
#                 logging.debug("\n Sec: {0}:".format(sec))
#                 logging.debug(df)
#
#         return self._raw_data
#
#     @property
#     def country_constituents(self):
#         return self._keyed_df_column_list
#
#     @property
#     def security_constituents(self):
#         return self._dictkey_list
#
#
