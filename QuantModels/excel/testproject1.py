import xlwings as xw


def main():
    wb = xw.Book.caller()
    sheet = wb.sheets["Test"]
    if sheet["A1"].value == "Hello xlwings!":
        sheet["A1"].value = "Bye xlwings!"
    else:
        sheet["A1"].value = "Hello xlwings!"


@xw.func
def hello(name):
    return f"Hello {name}!"


if __name__ == "__main__":
    xw.Book("Vol Cube front end delta V0.1.xlsm").set_mock_caller()
    main()
