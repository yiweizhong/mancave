import pandas as pd
import numpy as np
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations
from pandas.tseries.offsets import DateOffset

_p = 'C:\Dev\mancave\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import sbins
from statistics.mr import stochastic as stoch
from statistics.mr import helper


from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
from scipy.spatial.distance import pdist, squareform

from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from pandas.plotting import autocorrelation_plot

import seaborn as sns
import numpy as np
import scipy
import scipy.stats as st




filedate = '2021-07-01'


data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_ESI_historical.csv')

data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


ticker_exp = [c for c in data.columns if '_EXP' in c]
ticker_data = [c for c in data.columns if '_DATA' in c]
ticker_surprise = [c for c in data.columns if '_SURPRISE' in c]
ticker_us = [c for c in data.columns if c.startswith('US') and ('_SURPRISE' not in c)]
ticker_eu = [c for c in data.columns if c.startswith('EU') and ('_SURPRISE' not in c)]




data_exp = data[['USD_EXP','EUR_EXP','CNY_EXP', 'EMM_EXP', 'CAD_EXP', 'JPY_EXP',
 'GBP_EXP']]

data_actual = data[['USD_DATA','EUR_DATA','CNY_DATA', 'EMM_DATA', 'CAD_DATA', 
                    'JPY_DATA', 'GBP_DATA']]



'''Overview'''

'''Data'''
pca.PCA(data[['USD_DATA', 'EMX_DATA', 'EUR_DATA', 'CNY_DATA', 
              'AUD_DATA', 'APA_DATA', 'LTA_DATA']].dropna(axis=0).iloc[-2500:]
        ,rates_pca=True, matrixtype='CORR',
        sample_range = ('2013-01-01', '2019-12-01')).plot_loadings(n=6)


'''EXP'''
pca.PCA(data[ticker_us].dropna(axis=0).iloc[-2500:]
        ,rates_pca=True, matrixtype='COV',
        sample_range = ('2013-01-01', '2019-12-01')).plot_loadings(n=6)


'''USD'''
pca.PCA(data[['USD_EXP', 'EMX_EXP', 'EUR_EXP', 'CNY_EXP', 
              'AUD_EXP', 'APA_EXP', 'LTA_EXP']].dropna(axis=0).iloc[-2500:]
        ,rates_pca=True, matrixtype='CORR',
        sample_range = ('2013-01-01', '2019-12-01')).plot_loadings(n=6)


'''EUR'''
pca.PCA(data[ticker_eu].dropna(axis=0).iloc[-2500:]
        ,rates_pca=True, matrixtype='CORR',
        sample_range = ('2013-01-01', '2019-12-01')).plot_loadings(n=6)








pca.PCA(data[ticker_data].dropna(axis=0),rates_pca=True, 
        sample_range = ('2013-01-01', '2019-12-01')).plot_loadings(n=6)
    
pca.PCA(data_actual.dropna(axis=0),rates_pca=True, 
        sample_range = ('2013-01-01', '2019-12-01')).plot_loadings(n=6)
    






#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)

cross_countries_pca_history = data.index[-1] - DateOffset(years = 0, months=cross_countries_pca_history, days=0)
pair_pca_history = data.index[-1] - DateOffset(years = 0, months=pair_pca_history, days=0)


data_per_country = {}

for i in np.arange(0, len(ticker_exp)):
    data_per_country[ticker_exp[i][:3]] = pd.DataFrame({ticker_exp[i]: data[ticker_exp[i]], ticker_data: data[ticker_data[i]]})

per_country_mrs = {} 

for c_label, c_data in data_per_country.items():
    per_country_mrs = {**per_country_mrs, **create_combo_mr(c_data, data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True)}

for mr_desc, (mr_obj, pca_obj) in per_country_mrs.items():
    print(mr_desc)
    mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
    plt.savefig('C:/Temp/test/market_watch/output/global_esi_overview/%s_%s_%s.png' % (filedate, modeldate, mr_desc), bbox_inches='tight')
    plt.close()


