from __future__ import division
import _config_historical as cfg
import logging
import os, sys, inspect
import time, datetime
import pandas as pd
from pandas.tseries.offsets import *
import numpy as np
import argparse

import market_watch_historical_components as comp
from core import tools
from core import returns
import core.dirs as dirs


def load_market_data_files(from_dir, fileName_filter):
    def filter(from_dir, fileName_filter):
        for subdir, dirs, files in os.walk(from_dir):
            for f in files:
                if (fileName_filter in f):
                    f_path = os.path.join(from_dir, f)
                    df = tools.tframe(pd.read_csv(f_path),key ='date',utc=False)
                    yield df
    return filter(from_dir, fileName_filter)

def output_processed_data(to_dir, df, file_name_prefix= None, include_index = True):
    to_dir = dirs.get_create_absolute_folder(to_dir)
    file_name = file_name_prefix + '_' + df.name + '.csv' if file_name_prefix else df.name + '.csv'   
    df.to_csv(to_dir + '/' + file_name, index_label='row', index = include_index)

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='---- market watch ----')
    parser.add_argument('components', metavar='components', type=str, nargs='+', help='Separate component by a space')
    args = parser.parse_args()
    print(args.components)

    if (cfg.ARCHIVE_FILES):
        dirs.archive_files(cfg.OUTPUT_FOLDER, cfg.ARCHIVE_OUTPUT_FOLDER)        

    data_components = cfg.MARKET_COMPONENTS

    exit_code = 0


    if("STIRFUT" in args.components):
        stirfut_data = load_market_data_files(cfg.INPUT_FOLDER,'_STIRFUT_historical.csv').next()
        instruments = ['ED','BA', 'L','FF','IR']

        for instrument in instruments:
            instrument_data = stirfut_data[[col for col in stirfut_data.columns if (instrument in col)]] 
            processed_instruments =  comp.process_stirfut(instrument_data, instrument)
            output_folder = dirs.get_creat_sub_folder('STIRFUT',cfg.OUTPUT_FOLDER)
            output_processed_data(output_folder, processed_instruments, include_index = False)
            data_components[processed_instruments.name] = processed_instruments
    
    


    if("BETA" in args.components):
        pass

    print data_components.keys()

    

        

    


    

