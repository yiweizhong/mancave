import pandas as pd
import numpy as np
import os, sys, inspect
from operator import itemgetter
import collections
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations, chain
from pykalman import KalmanFilter
import xlwings as xw
from pandas.tseries.offsets import DateOffset
from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
import scipy.cluster.hierarchy as cl
from scipy.spatial.distance import pdist, squareform
import QuantLib as ql


_p = 'C:\Dev\mancave\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\statistics\derivative\pysabr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\core'
if _p not in sys.path: sys.path.insert(0, _p)


from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper
from statistics.derivative import bs as bs
from statistics.derivative import quantlibutils as qlu
from statistics.derivative.pysabr import hagan_2002_normal_sabr as nsabr
from statistics.mr import tools as mrt
from core import dirs

pd.set_option('display.expand_frame_repr', False)


##############################################################################
####
#### functions
####
##############################################################################


def reformat_data(data:pd.core.frame.DataFrame) -> dict:
    cs = list(set([c[:3] for c in data.columns]))
    output = {c: data[[col for col in data.columns if c in col]]
                  .rename(lambda col: col.replace('{}_0Y_'.format(c),'') , axis=1)  
              for c in cs}
    return output



def to_ql_date(date: pd._libs.tslibs.timestamps.Timestamp) -> ql.QuantLib.Date:
    return ql.Date(date.day, date.month, date.year)
    


##############################################################################
####  load data
##############################################################################

rundate = '2021-07-21'


ois_data_all = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + 
                           '_OIS_ALL_historical.csv')  
ois_data_all['date'] = pd.to_datetime(ois_data_all['date'])
ois_data_all = ois_data_all.set_index('date').ffill()

ois_date = ql.Date(int(rundate.split('-')[2]), 
                   int(rundate.split('-')[1]), 
                   int(rundate.split('-')[0]))
ois_country_data = reformat_data(ois_data_all)


##############################################################################
#### process
##############################################################################


# different country different fwd dates

if (2>3):
    offset = -20
    #use the first element of the dict to deduce the date
    offset_date = list(ois_country_data.values())[0].index[offset] 
    ql.Settings.instance().evaluationDate = to_ql_date(offset_date)
    
    ois_spot_curves = {c: qlu.create_ois_curve(c_data.iloc[offset,:].dropna(), c) 
                      for c, c_data in ois_country_data.items()}  
    
    ois_1d_fwd_curves = {c: qlu.create_1d_fwd_ois_rate_curve(c_curve)
                         for c, c_curve in ois_spot_curves.items()}
    
    named_ois_1d_fwd_curves = [c_curve.rename(c) for c, c_curve 
                               in ois_1d_fwd_curves.items() if c != 'CAD1']
    
    
    qlu.plot_mutli_curve_series(named_ois_1d_fwd_curves, 
                                    title = 'Implied policy rates {}'.format(offset_date.strftime('%Y-%m-%d')))
    


# same country with different price dates projected as if starting from the 
# date

if (2>3):
    
    
    from statistics.derivative import quantlibutils as qlu
    
    country = 'USD'
    offsets = [-1] + np.arange(-1400,-2000, -70).tolist()
    offsets = [-1] + np.arange(-800,-1400, -70).tolist()
    
    
    #use the first element of the dict to deduce the date
    ois_country_data_for_country = ois_country_data[country]
    offset_date = ois_country_data_for_country.index[-1] 
    ql.Settings.instance().evaluationDate = to_ql_date(offset_date)
    
    
    ois_spot_curves = {ois_country_data_for_country.iloc[offset,:].name.strftime('%Y-%m-%d'): \
                       qlu.create_ois_curve(ois_country_data_for_country.iloc[offset,:].dropna(), 
                                            country) 
                      for offset in offsets}  
    
    
    ois_1d_fwd_curves = {c: qlu.create_1d_fwd_ois_rate_curve(c_curve)
                         for c, c_curve in ois_spot_curves.items()}
    
    named_ois_1d_fwd_curves = [c_curve.rename(c) for c, c_curve 
                               in ois_1d_fwd_curves.items()]
    
    
    qlu.plot_mutli_curve_series(named_ois_1d_fwd_curves, 
                                title = '{1} Implied policy rates {0}'.format(offset_date.strftime('%Y-%m-%d'), country),
                                highlight_series= ['2021-07-06'])
    

########################################################
# same country with different price dates projected 
# as if starting from the date
# --- 
# an example of pre and post 2015 hikes
########################################################
 
if (2 > 3): 
    country = 'USD'
    ois_country_data_for_country = ois_country_data[country]
    offset_date = ois_country_data_for_country.index[-1] 
    ql.Settings.instance().evaluationDate = to_ql_date(offset_date)
    
    d1 = ['2021-07-06','2021-05-12', '2021-04-12', '2021-03-12', 
          '2021-02-12', '2021-01-12', '2020-12-14', '2020-11-12', 
          '2020-10-12']    
    
    d2 =  ['2021-07-06', '2021-05-12', '2021-05-17', '2021-05-24', 
          '2021-05-31', '2021-06-07', '2021-06-14', '2021-06-21', 
          '2021-06-28']
    
    pre_2021may_offset = [ois_country_data_for_country.index.get_loc(d) for
                             d in d1]

    post_2021may_offsett = [ois_country_data_for_country.index.get_loc(d) for
                             d in d2]
    
    
    
    plt.figure(figsize = (15, 8))
    gs = gridspec.GridSpec(1, 2, height_ratios=[1], width_ratios=[1,1])
    ax1 = plt.subplot(gs[0])
    ax2 = plt.subplot(gs[1])
    
    offsets = pre_2021may_offset
    
    ois_country_data_for_country = ois_country_data[country]
    offset_date = ois_country_data_for_country.index[-1] 
    ql.Settings.instance().evaluationDate = to_ql_date(offset_date)
    
    
    ois_spot_curves = {ois_country_data_for_country.iloc[offset,:].name.strftime('%Y-%m-%d'): \
                       qlu.create_ois_curve(ois_country_data_for_country.iloc[offset,:].dropna(), 
                                            country) 
                      for offset in offsets}  
    
    
    ois_1d_fwd_curves = {c: qlu.create_1d_fwd_ois_rate_curve(c_curve)
                         for c, c_curve in ois_spot_curves.items()}
    
    named_ois_1d_fwd_curves = [c_curve.rename(c) for c, c_curve 
                               in ois_1d_fwd_curves.items()]
    
    
    qlu.plot_mutli_curve_series(named_ois_1d_fwd_curves, ax=ax1, 
                                title = '{0} OIS Implied policy rate path pre 2015 hike'.format(country),
                                highlight_series= [offset_date.strftime('%Y-%m-%d')])
    
    offsets = post_2021may_offsett
    
    ois_country_data_for_country = ois_country_data[country]
    offset_date = ois_country_data_for_country.index[-1] 
    ql.Settings.instance().evaluationDate = to_ql_date(offset_date)
    
    
    ois_spot_curves = {ois_country_data_for_country.iloc[offset,:].name.strftime('%Y-%m-%d'): \
                       qlu.create_ois_curve(ois_country_data_for_country.iloc[offset,:].dropna(), 
                                            country) 
                      for offset in offsets}  
    
    
    ois_1d_fwd_curves = {c: qlu.create_1d_fwd_ois_rate_curve(c_curve)
                         for c, c_curve in ois_spot_curves.items()}
    
    named_ois_1d_fwd_curves = [c_curve.rename(c) for c, c_curve 
                               in ois_1d_fwd_curves.items()]
    
    
    qlu.plot_mutli_curve_series(named_ois_1d_fwd_curves, ax=ax2, 
                                title = '{0} OIS Implied policy rate path post 2015 hike'.format(country),
                                highlight_series= [offset_date.strftime('%Y-%m-%d')])
    
    
    #ax2.set_ylim(ax1.get_ylim()[0]*100,ax1.get_ylim()[0]*100)



########################################################
# same country with different price dates projected 
# as they dated
# --- 
# an example of pre and post 2015 hikes
########################################################
 
if (2 > 3): 
    country = 'USD'
    ois_country_data_for_country = ois_country_data[country]
    offset_date = ois_country_data_for_country.index[-1] 
    ql.Settings.instance().evaluationDate = to_ql_date(offset_date)
    
    d1 = [offset_date.strftime('%Y-%m-%d')] + ['2018-06-13', '2018-03-07', '2017-11-29', 
          '2017-08-23', '2017-05-17', '2017-02-08', '2016-11-02', 
          '2016-07-27', '2016-04-20']    
    
    d2 = [offset_date.strftime('%Y-%m-%d')] + [ '2016-02-24', '2015-11-18', '2015-08-12', 
          '2015-05-06', '2015-01-28', '2014-10-22', '2014-07-16', 
          '2014-04-09', '2014-01-01']
    
    post_2015_hike_offset = [ois_country_data_for_country.index.get_loc(d) for
                             d in d1]

    pre_2015_hike_offset = [ois_country_data_for_country.index.get_loc(d) for
                             d in d2]
    
    
    
    plt.figure(figsize = (15, 8))
    gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
    ax1 = plt.subplot(gs[0])

    offsets = post_2015_hike_offset
    
    ois_country_data_for_country = ois_country_data[country]
    offset_date = ois_country_data_for_country.index[-1] 
    ql.Settings.instance().evaluationDate = to_ql_date(offset_date)
    
    
    ois_spot_curves = {ois_country_data_for_country.iloc[offset,:].name.strftime('%Y-%m-%d'): \
                       qlu.create_ois_curve(ois_country_data_for_country.iloc[offset,:].dropna(), 
                                            country) 
                      for offset in offsets}  
    
    
    ois_1d_fwd_curves = {c: qlu.create_1d_fwd_ois_rate_curve(c_curve)
                         for c, c_curve in ois_spot_curves.items()}
    
    named_ois_1d_fwd_curves = [c_curve.rename(c) for c, c_curve 
                               in ois_1d_fwd_curves.items()]
    
    
    qlu.plot_mutli_curve_series(named_ois_1d_fwd_curves, ax=ax1, 
                                title = '{0} OIS Implied policy rate path pre 2015 hike'.format(country),
                                highlight_series= [offset_date.strftime('%Y-%m-%d')])
    
    
    for offset in post_2015_hike_offset:
        
        offset_date = list(ois_country_data.values())[0].index[offset] 
        ql.Settings.instance().evaluationDate = to_ql_date(offset_date)
        
        ois_spot_curves = {c: qlu.create_ois_curve(c_data.iloc[offset,:].dropna(), c) 
                          for c, c_data in ois_country_data.items()}  
        
        ois_1d_fwd_curves = {c: qlu.create_1d_fwd_ois_rate_curve(c_curve)
                             for c, c_curve in ois_spot_curves.items()}
        
        named_ois_1d_fwd_curves = [c_curve.rename(c) for c, c_curve 
                                   in ois_1d_fwd_curves.items() if c != 'CAD1']
        
        
        qlu.plot_mutli_curve_series(named_ois_1d_fwd_curves, 
                                        title = 'Implied policy rates {}'.format(offset_date.strftime('%Y-%m-%d')))
        


    
    
    


if (2>3):
    ois_data_all.iloc[[-1,-100, -300, -450],:][[c for c in ois_data_all.columns if 'NZD' in c]].transpose().plot()
    plt.figure()
    ois_data_all.iloc[[-1,-100, -300, -450],:][[c for c in ois_data_all.columns if 'AUD' in c]].transpose().plot()
    ois_data_all.iloc[[-1,-100, -300, -450],:][[c for c in ois_data_all.columns if 'USD' in c]].transpose().plot()


