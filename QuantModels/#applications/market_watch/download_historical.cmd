echo off
SET localPath=%~dp0%
echo %localPath%
pushd %localPath%

C:\Anaconda2\python.exe download_historical.py
if errorlevel 1 (
   echo Process failed with code %errorlevel%
   exit /b %errorlevel%
)

popd
echo on
pause