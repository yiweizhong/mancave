import market_watch_commons

import datetime
import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns

from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
import xlwings as xw

from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet
from scipy.spatial.distance import pdist, squareform


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


pd.set_option('display.expand_frame_repr', False)




def set_date_index(df, col):    
    df[col] = pd.to_datetime(df[col])
    df = df.set_index(col).ffill()
    return df
    



def get_flattened_cov(df):
    covmatx = df.cov()  
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def get_flattened_corr(df):
    covmatx = df.corr()  
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result

def get_mean(df):
    return df.mean()

    
def rolling_apply(df, window_size, func, min_window_size=None):
    if min_window_size is None:
        min_window_size = window_size
    result = {}
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here

        if len(sub_df) >= min_window_size:            
            idx = sub_df.index[-1]
            result[idx] = func(sub_df)
    return result




### data

rundate = '2021-02-01'


atm_vol_p1 = pd.read_excel('./data/US_ATM_Vol_Fwd_P1_20200122.xls', sheet_name='Data Table')  
atm_vol_p2 = pd.read_excel('./data/US_ATM_Vol_Fwd_P2_20200122.xls', sheet_name='Data Table')  

atm_vol_p1 = set_date_index(atm_vol_p1, 'Date')
atm_vol_p2 = set_date_index(atm_vol_p2, 'Date')




data_swap = pd.read_csv('C:/Temp/test/market_watch/staging/'  + rundate +  '_SWAP_ALL_V2_historical.csv')  
data_swap['date'] = pd.to_datetime(data_swap['date'])
data_swap = data_swap.set_index('date').ffill()


data_usd_all = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_USD_historical.csv')  
data_usd_all['date'] = pd.to_datetime(data_usd_all['date'])
data_usd_all = data_usd_all.set_index('date').ffill()


# data_bcs = pd.read_csv(r'C:/Dev/Models/QuantModels/#test/BCS_2020-07-29.csv')  
# data_bcs['date'] = pd.to_datetime(data_bcs['date'])
# data_bcs = data_bcs.set_index('date').ffill()
# data_bcs_rtn = (data_bcs - data_bcs.shift(1))/100

data_ifs = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SWIT_historical.csv')  
data_ifs['date'] = pd.to_datetime(data_ifs['date'])
data_ifs = data_ifs.set_index('date').ffill()


data_ccy_basis = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_BS_ALL_historical.csv')  
data_ccy_basis['date'] = pd.to_datetime(data_ccy_basis['date'])
data_ccy_basis = data_ccy_basis.set_index('date').ffill()
data_ccy_basis = data_ccy_basis[[c for c in data_ccy_basis.columns if '3M' not in c]]



data_sovcds = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SOVCDS_historical.csv')  
data_sovcds['date'] = pd.to_datetime(data_sovcds['date'])
data_sovcds = data_sovcds.set_index('date').ffill() 



data_gov = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_GOV_historical.csv')  
data_gov['date'] = pd.to_datetime(data_gov['date'])
data_gov = data_gov.set_index('date').ffill()


data_eq_px = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_EQUITY_historical.csv')  
data_eq_px['date'] = pd.to_datetime(data_eq_px['date'])
data_eq_px = data_eq_px.set_index('date').ffill()


data_mvo = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_MVO_historical.csv')  
data_mvo['date'] = pd.to_datetime(data_mvo['date'])
data_mvo = data_mvo.set_index('date').ffill().dropna(axis=0)
data_mvo = data_mvo.drop(['AU_IG2'], axis=1)

data_mvo_rtns_daily = (data_mvo/data_mvo.shift(1) - 1).dropna()

data_mvo_rtns_daily['US_10'] = data_mvo['US_10'] - data_mvo['US_10'].shift(1)
data_mvo_rtns_daily['US_30'] = data_mvo['US_30'] - data_mvo['US_30'].shift(1)
 




#test_case1 
# US10 and 30 mean reversion


resample_freq = 'B' #'W'

tc1_data_usgg10 = data_usd_all['USGG10YR'].iloc[-20:]
tc1_data_usgg30 = data_usd_all['USGG30YR'].iloc[-20:]


bins.create_mr_diagnosis(tc1_data_usgg10, 20, min_num_of_bins = 10, resample_freq = resample_freq).create_mr_info_fig((15,10))
bins.create_mr_diagnosis(tc1_data_usgg30, 20, min_num_of_bins = 10, resample_freq = resample_freq).create_mr_info_fig((15,10))




#test case2


#rolling corr

assets = data_mvo_rtns_daily.columns.tolist()
assets = [ a + '_vs_' + b for a,b in set([comb for comb in combinations(assets + assets, 2)]) if a != b]

data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(data_mvo_rtns_daily, 22, get_flattened_corr)).transpose()

assets = ['US_10_vs_US_EQ', 'USD_vs_US_EQ', 'US_30_vs_US_EQ' , 'US_30_vs_US_HY', 'US_30_vs_US_IG' , 'US_10_vs_US_HY', 'US_10_vs_US_IG' ]

data_rtns_d_1m_rolling_corrs[assets].iloc[-250:].plot()




#

pca.PCA(data_swap[['CNH_0Y_3M',
             'CNH_0Y_1Y',
             'CNH_0Y_2Y',
             'CNH_0Y_3Y',
             'CNH_0Y_4Y',
             'CNH_0Y_5Y',
             'CNH_0Y_7Y',
             'CNH_0Y_10Y']].iloc[-2500:].dropna(axis=1)
        , rates_pca = True, momentum_size = None).plot_loadings(n=6)




'''

data_usd_swap_curve = data_swap[['USD_0Y_3M', 'USD_0Y_1Y', 'USD_1Y_1Y', 'USD_3Y_3Y', 'USD_5Y_5Y', 'USD_10Y_10Y']]

data_usd_swap_curve_rtns = (data_usd_swap_curve - data_usd_swap_curve.shift(-1)).resample('5B', label='right').sum()


pca.PCA(data_usd_swap_curve.iloc[-3500:].dropna(axis=1), rates_pca = True, momentum_size = None).plot_loadings(n=6)

pca.PCA(data_usd_swap_curve.iloc[-2500:-1500].dropna(axis=1), rates_pca = True, momentum_size = None).plot_loadings(n=6)


pca.PCA(data_usd_swap_curve_rtns.iloc[-500:].dropna(axis=1), rates_pca = True, momentum_size = None).plot_loadings(n=6)
'''


