import pandas as pd
import numpy as np
import os, sys, inspect
from operator import itemgetter
import collections
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations, chain
from pykalman import KalmanFilter
import xlwings as xw
from pandas.tseries.offsets import DateOffset

_p = 'C:\Dev\Mancave\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\statistics\derivative\pysabr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\core'
if _p not in sys.path: sys.path.insert(0, _p)


from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper
from statistics.derivative import bs as bs
from statistics.derivative import quantlibutils as qlutils
from statistics.derivative.pysabr import hagan_2002_normal_sabr as nsabr
from statistics.mr import tools as mrt
from core import dirs
from core import tools as ctls



from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
import scipy.cluster.hierarchy as cl
from scipy.spatial.distance import pdist, squareform

import QuantLib as ql

pd.set_option('display.expand_frame_repr', False)



########################################################################################
### local functions
########################################################################################


def clean_citi_data(df, col_rename_func, col_name_filter_func=None):
    #col 0 is the date
    _df = df[(~ df.iloc[:,0].str.contains('---')) & \
             (~ df.iloc[:,0].str.contains('confidential'))]
    #name the date column
    _df = _df.rename(columns={_df.columns[0]: 'date'})  
    _df['date'] = pd.to_datetime(_df['date'])
    _df = _df.set_index('date')
    # filter col  
    _dfc = _df.columns if col_name_filter_func is None else [ c for c in _df.columns if col_name_filter_func(c)]
    _df = _df[_dfc]
    # rename col    
    _df = _df.rename(col_rename_func, axis=1)    
    return _df


def otm_vol_col_name_cleaner(c):    
    cs = c.split(' ')
    keys = [c.upper() for c in itemgetter(*[5,6,7])(cs)]
    new_c = '{}_{}_{}'.format(*keys)
    return new_c
    

def atm_vol_col_name_cleaner(c):    
    cs = c.split(' ')
    keys = [c.upper() for c in itemgetter(*[0,2])(cs)]
    new_c = '{}_{}'.format(*keys)
    return new_c



def swap_col_name_cleaner(c):
    if 'PAR' in c.upper():
        cs = c.split(' ')        
        new_c = '0Y_{}'.format(cs[1])
        return new_c
    else:            
        cs = c.split(' ')
        keys = [c.upper() for c in itemgetter(*[1,3])(cs)]
        new_c = '{}_{}'.format(*keys)
        return new_c



def calc_ann_rvol(data: pd.core.frame.DataFrame, expiries:dict, 
                  annualizer=252, bp_multiplier=100)->pd.core.frame.DataFrame:
    output = {}
    for c in data.columns:
        for k,v in expiries.items():
          output['{}_{}'.format(k,c)] = data[c].diff(1).rolling(v).std() \
          * np.sqrt(annualizer) * bp_multiplier           
    return pd.DataFrame(output)


def calc_trading_range(data: pd.core.frame.DataFrame, expiries:dict,
                       bp_multiplier=100)->pd.core.frame.DataFrame:
    output = {}
    dt = data.index[-1:] # a dateindex with last element only
    for c in data.columns:
        for k,v in expiries.items():
            output['{}_{}'.format(k,c)] = \
                pd.Series((data[c].iloc[-v:].max() - data[c].iloc[-v:].min()) * bp_multiplier,dt)           
    return pd.DataFrame(output)



def filter_data(data, exps=[], tails=[], strikes=[], 
                cin_f = lambda e, t, s: None, 
                cout_f = lambda e, t, s: None, 
                coutexp_f = lambda e, t, s: f'{e}_{t}_{s}'):
    output = {}    
    for exp in exps:
        for tail in tails:
            for strike in strikes:
                # cin_func turns e, t, s into the input col format
                c_in = cin_f(exp, tail, strike)
                # cout_func turns e, t, s into the input col format
                c_out = cout_f(exp, tail, strike)
                c_out_expected = coutexp_f(exp, tail, strike)

                #make empty column if there is no equivalent data from import
                output[c_out_expected] = data[c_in] \
                if (c_in in data.columns and c_out == c_out_expected) \
                else pd.Series(data= np.nan, index=data.index)
    return pd.DataFrame(output)

def filter_rvol(data, fwds=[], tails=[], exps=[]):
    return filter_data(data, exps=fwds, tails=tails, strikes=exps, 
                cin_f = lambda e, t, s: f'{s}_{e}_{t}', 
                cout_f = lambda e, t, s: f'{e}_{t}', 
                coutexp_f = lambda e, t, s: f'{e}_{t}')

def filter_atm(data, exps=[], tails=[], dummy_strike=False):
    return filter_data(data, exps=exps, tails=tails, strikes=['+0' if dummy_strike else ''],
                cin_f = lambda e, t, s: f'{e}_{t}', 
                cout_f = lambda e, t, s: f'{e}_{t}_{s}' if dummy_strike else f'{e}_{t}', 
                coutexp_f = lambda e, t, s: f'{e}_{t}_{s}' if dummy_strike else f'{e}_{t}')


def filter_otm(data, exps=[], tails=[], strikes=[]):
    return filter_data(data, exps=exps, tails=tails, strikes=strikes,
                cin_f = lambda e, t, s: f'{e}_{t}_{s}', 
                cout_f = lambda e, t, s: f'{e}_{t}_{s}', 
                coutexp_f = lambda e, t, s: f'{e}_{t}_{s}')



def calc_premium(vol_df, fwd_df):
    #vol col name is expected to be like '5Y_10Y_+10' indicating 5y exp 10y tail and atm +10bps
    def call_pricer(row):
        #not going to price when vol or fwd is not available or vol is less equal than 0 or strike is negative        
        if (np.isnan(row['vol']) or np.isnan(row['fwd']) or np.isnan(row['otm_spread']) \
            or row['fwd'] + row['otm_spread'] <= 0):
            return np.nan
        else:
            _premium = bs.price_swaption(row['fwd'],
                                        row['fwd'] + row['otm_spread'], #strike 
                                        exp, # t in years
                                        row['vol'], # vol in bps
                                        payer=True if otm_spread >= 0 else False, 
                                        normal=True)
            return _premium * 100
                                  
    premium_df = {}
    
    for c in vol_df.columns:
        exp, tail, otm_spread = itemgetter(*[0,1,2])(c.split('_'))
        fwd_col = f'{exp}_{tail}'
        exp = bs.convert_to_year(exp)
        tail = bs.convert_to_year(tail)
        otm_spread = float(otm_spread)/100 # in %
        
        #get vol history and fwd history
        
        vol = vol_df[c]
        fwd = fwd_df[fwd_col].reindex(vol.index, axis =0).ffill()
        #strike = fwd + otm_spread
        vol_fwd_df = pd.DataFrame({
            'vol': vol,
            'fwd': fwd,
            })
        vol_fwd_df['otm_spread'] = otm_spread
        vol_fwd_df['exp'] = exp 
                
        premium = vol_fwd_df.apply(call_pricer, axis = 1)        
        
        premium_df[c] = pd.Series(premium.values, vol.index)
               
    return pd.DataFrame(premium_df)



def calc_fwd_roll(vol_df, fwd_df):
    #vol col name is expected to be like '5Y_10Y_+10' indicating 5y exp 10y tail and atm +10bps
    #use the vol tenor to find the fwd and spot
                                      
    roll_df = {}
    
    for c in vol_df.columns:
        exp, tail, otm_spread = itemgetter(*[0,1,2])(c.split('_'))
        fwd_col = f'{exp}_{tail}'
        spot_col = f'0Y_{tail}'
        #exp = bs.convert_to_year(exp)
        #tail = bs.convert_to_year(tail)
        
        otm_spread = float(otm_spread) # in bps
        
        #get vol history and fwd history
        vol = vol_df[c]
        spot = fwd_df[spot_col].reindex(vol.index, axis =0).ffill()
        fwd = fwd_df[fwd_col].reindex(vol.index, axis =0).ffill()
        roll = (fwd - spot)*100 + otm_spread #bps 
         
        roll_df[c] = pd.Series(roll.values, vol.index)
               
    return pd.DataFrame(roll_df)






def pivot(data, l1s =[] , l2s =[], 
          filter_f = lambda a, b:  f'{a}_{b}', 
          filtered_col_rename_f = lambda c: c):
    '''
    Parameters
    ----------
    data : dataframe 
        data to be pivoted
    l1s : list, optional
        a list of available values to construct the filter for pivot with 
        other levels of filter. The default is [].
    l2s : list, optional
        a list of available values to construct the filter for pivot with
        other levels of filter.. The default is [].
    filter_f : func, optional
        used to construct the filter with l1s and l2s value. 
    filtered_col_rename_f : func, optional
        func to rename the pivoted columns. The default is lambda c: c.

    Returns
    -------
    output : TYPE
        DESCRIPTION.

    '''
    output = {}
    for l2 in l2s:
        for l1 in l1s:
            filter_str = filter_f(l1, l2)
            cols = [c for c in data.columns if filter_str in c]
            if len(cols) > 0:
               output[filter_str] = data[cols].rename(filtered_col_rename_f, axis = 1) 
    return output


def pivot_ds_to_table(ds, ridx, cidx, 
                      idx_filter=lambda x, y: f'{x}_{y}',
                      ridx_rfmt=lambda x: f'{x}',
                      cidx_rfmt=lambda x: f'{x}',
                      ridx_name=None, cidx_name=None):
    '''
    Parameters
    ----------
    ds : ds 
        1M_3M      15.6616
        1M_1Y      15.7298
        1M_2Y      28.9470
        1M_3Y      42.4078
        1M_5Y      62.0783
    l1s : list, optional
        ['1M', '3M', '6M'...]
    l2s : list, optional
         ['3M', '1Y', '2Y'...].
    filter_f : func, optional
        used to construct the ds index key with ridx and cidx value. 
    filtered_col_rename_f : func, optional
        func to rename the pivoted columns. The default is lambda c: c.

    Returns
    -------
    output : TYPE
        DESCRIPTION.

    '''
    c_s = {}
    for i in ridx:
        r_s = {}        
        for j in cidx:
            c_name = idx_filter(i, j)
            if c_name in ds.index:
                
                r_s[cidx_rfmt(j)] = ds[c_name]
        if len(r_s.keys()) > 0:
            c_s[ridx_rfmt(i)] = pd.Series(r_s)
    pvt_table = pd.DataFrame(c_s).transpose()
    if ridx_name is not None:
        pvt_table.index.name= ridx_name
    if cidx_name is not None:
        pvt_table.columns.name= cidx_name        
    return pvt_table


def output_df(string_range, dict_df, dict_key, sht):
    cell = string_range
    name = dict_key
    sht.range(cell).value = dict_key
    sht.range(cell).offset(1).value = output[dict_key].round(2)


def dumptoDatabase(db_conn, schema, dfs_dict, dropblanks=False):
    for key in dfs_dict.keys():
        df = dfs_dict[key].dropna() if dropblanks else dfs_dict[key] 
        df.to_sql(key, db_conn.engine, schema=schema, if_exists='replace', index=True, index_label ='Date')


def unpivot_df(data, date, name):
    result = pd.melt(data, value_vars=tails, 
                     var_name='col', 
                     value_name='value',ignore_index = False)
    result = result.reset_index().rename({'index':'row'}, axis=1)
    result['date'] = date
    result['type'] = name
    return result[['date', 'type', 'row', 'col', 'value']]


def unpivot_df2(data, date, name):
    result = pd.melt(data, value_vars=data.columns.tolist(), 
                     var_name='col', 
                     value_name='value',ignore_index = False)
    result = result.reset_index().rename({'index':'row'}, axis=1)
    result['date'] = date
    result['type'] = name
    return result[['date', 'type', 'row', 'col', 'value']]



########################################################################################
### configs
########################################################################################


citi_input_root = r'C:/Users/zngyiq/OneDrivePersonal/OneDrive/Library/Data/Citi/'
excel_output_dir = r'C:/Dev/mancave/QuantModels/#applications/market_watch/ratesmonitor/'

ivol_expiries = {
 '1M':20,'3M':60,'6M':120,'9M':180,'1Y':255,'2Y':510,'3Y':765,'4Y':255*4,
 '5Y':255*5,'7Y':255*7,'10Y':255*10,'12Y':255*12,'15Y':255*15,'20Y':255*20,
 '30Y':255*30
 }

ivol_expiries_v2 = {'1M':20,'3M':60,'6M':120,'1Y':252,'2Y':252*2,
                    '3Y':252*3,'4Y':252*4, '5Y':252*5,'10Y':252*10}

ivol_expiries_v3 = {'1M':20,'3M':60,'6M':120}


country = ['usd', 'aud', 'gbp', 'eur']
exp = ['1M', '3M', '6M', '1Y',  '2Y', '3Y', '4Y', '5Y', '10Y'] # not all exp are available in citi
tails = ['1Y','2Y', '3Y', '5Y','7Y', '10Y', '20Y', '30Y']  
strikes = ['+50', '+25','+10','+0','-10', '-25','-50']
spreads = [10, 25, 50]
otm_exps =  ['1M', '3M', '6M', '1Y', '2Y', '3Y', '5Y', '10Y']
otm_tails = ['1Y', '2Y','3Y', '5Y', '7Y','10Y', '30Y']


########################################################################################
### load data
########################################################################################

'''
#    load new data
'''

''' USD '''

usd_citi_atm_data_new = pd.read_csv(citi_input_root + r'raw/USD_ATM.csv', skiprows=2)  
usd_citi_atm_data_new = clean_citi_data(usd_citi_atm_data_new, atm_vol_col_name_cleaner).ffill()

usd_citi_otm_data_new = pd.read_csv(citi_input_root + r'raw/USD_OTM.csv', skiprows=2)  
usd_citi_otm_data_new = clean_citi_data(usd_citi_otm_data_new, otm_vol_col_name_cleaner).ffill()

usd_citi_swap_data_new = pd.read_csv(citi_input_root + r'raw/USD_SWAP.csv', skiprows=2)  
usd_citi_swap_data_new = clean_citi_data(usd_citi_swap_data_new, 
                                         swap_col_name_cleaner, 
                                         col_name_filter_func=lambda c: 'Swap Rate'.upper() in c.upper()).ffill()


''' AUD '''

aud_citi_atm_data_new = pd.read_csv(citi_input_root + r'raw/AUD_ATM.csv', skiprows=2)  
aud_citi_atm_data_new = clean_citi_data(aud_citi_atm_data_new, atm_vol_col_name_cleaner).ffill()

aud_citi_otm_data_new = pd.read_csv(citi_input_root + r'raw/AUD_OTM.csv', skiprows=2)  
aud_citi_otm_data_new = clean_citi_data(aud_citi_otm_data_new, otm_vol_col_name_cleaner).ffill()
aud_citi_otm_data_prt2_new = pd.read_csv(citi_input_root + r'raw/AUD_OTM_PRT2.csv', skiprows=2)  
aud_citi_otm_data_prt2_new = clean_citi_data(aud_citi_otm_data_prt2_new, otm_vol_col_name_cleaner).ffill()
aud_citi_otm_data_new = mrt.merge_dataframess(aud_citi_otm_data_new, aud_citi_otm_data_prt2_new, axis=1)

aud_citi_swap_data_new = pd.read_csv(citi_input_root + r'raw/AUD_SWAP.csv', skiprows=2)  
aud_citi_swap_data_new = clean_citi_data(aud_citi_swap_data_new, 
                                     swap_col_name_cleaner, 
                                     col_name_filter_func=lambda c: 'Swap Rate'.upper() in c.upper()).ffill()

'''GBP'''

gbp_citi_atm_data_new = pd.read_csv(citi_input_root + r'raw/GBP_ATM.csv', skiprows=2)  
gbp_citi_atm_data_new = clean_citi_data(gbp_citi_atm_data_new, atm_vol_col_name_cleaner).ffill()

#no otm yet

gbp_citi_swap_data_new = pd.read_csv(citi_input_root + 'raw/GBP_SWAP.csv', skiprows=2)  
gbp_citi_swap_data_new = clean_citi_data(gbp_citi_swap_data_new, 
                                         swap_col_name_cleaner, 
                                         col_name_filter_func=lambda c: 'Swap Rate'.upper() in c.upper()).ffill()

gbp_citi_swapois_data_new = pd.read_csv(citi_input_root + 'raw/GBP_SWAPOIS.csv', skiprows=2)  
gbp_citi_swapois_data_new = clean_citi_data(gbp_citi_swapois_data_new, 
                                         swap_col_name_cleaner, 
                                         col_name_filter_func=lambda c: 'OIS Rate'.upper() in c.upper()).ffill()


''' EUR '''

eur_citi_atm_data_new = pd.read_csv(citi_input_root + r'raw/EUR_ATM.csv', skiprows=2)  
eur_citi_atm_data_new = clean_citi_data(eur_citi_atm_data_new, atm_vol_col_name_cleaner).ffill()

# no otm yet

eur_citi_swap_data_new = pd.read_csv(citi_input_root + 'raw/EUR_SWAP.csv', skiprows=2)  
eur_citi_swap_data_new = clean_citi_data(eur_citi_swap_data_new, 
                                         swap_col_name_cleaner, 
                                         col_name_filter_func=lambda c: 'Swap Rate'.upper() in c.upper()).ffill()




'''
#    archive main before merging
'''


dirs.archive_files(citi_input_root + "main", citi_input_root + "archive", copy_only=True)

'''
#   merge new with main and save main
'''

'''load main'''
usd_citi_otm_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/USD_OTM.csv'), key='date')  
usd_citi_swap_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/USD_SWAP.csv'), key='date')  
aud_citi_otm_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/AUD_OTM.csv'), key='date')  
aud_citi_swap_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/AUD_SWAP.csv'), key='date')  
aud_citi_atm_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/AUD_ATM.csv'), key='date')
usd_citi_atm_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/USD_ATM.csv'), key='date')
gbp_citi_atm_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/GBP_ATM.csv'), key='date')
gbp_citi_swap_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/GBP_SWAP.csv'), key='date')
gbp_citi_swapois_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/GBP_SWAPOIS.csv'), key='date')
eur_citi_atm_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/EUR_ATM.csv'), key='date')
eur_citi_swap_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/EUR_SWAP.csv'), key='date')

'''load main - previously calced history'''
usd_otm_premium_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/USD_OTM_PREMIUM.csv'), key='date')
aud_otm_premium_data = ctls.tframe(pd.read_csv(citi_input_root + 'main/AUD_OTM_PREMIUM.csv'), key='date')




'''merge with main'''
usd_citi_atm_data = ctls.merge_dataframess(usd_citi_atm_data, usd_citi_atm_data_new, axis=0)
usd_citi_otm_data = ctls.merge_dataframess(usd_citi_otm_data, usd_citi_otm_data_new, axis=0)
usd_citi_swap_data = ctls.merge_dataframess(usd_citi_swap_data, usd_citi_swap_data_new, axis=0)

aud_citi_atm_data = ctls.merge_dataframess(aud_citi_atm_data, aud_citi_atm_data_new, axis=0)
aud_citi_otm_data = ctls.merge_dataframess(aud_citi_otm_data, aud_citi_otm_data_new, axis=0)
aud_citi_swap_data = ctls.merge_dataframess(aud_citi_swap_data, aud_citi_swap_data_new, axis=0)

gbp_citi_atm_data = ctls.merge_dataframess(gbp_citi_atm_data, gbp_citi_atm_data_new, axis=0)
gbp_citi_swap_data = ctls.merge_dataframess(gbp_citi_swap_data, gbp_citi_swap_data_new, axis=0)
gbp_citi_swapois_data = ctls.merge_dataframess(gbp_citi_swapois_data, gbp_citi_swapois_data_new, axis=0)

eur_citi_atm_data = ctls.merge_dataframess(eur_citi_atm_data, eur_citi_atm_data_new, axis=0)
eur_citi_swap_data = ctls.merge_dataframess(eur_citi_swap_data, eur_citi_swap_data_new, axis=0)


'''arcchive main'''
usd_citi_atm_data.to_csv(citi_input_root +'main/USD_ATM.csv')
usd_citi_otm_data.to_csv(citi_input_root + 'main/USD_OTM.csv')
usd_citi_swap_data.to_csv(citi_input_root +'main/USD_SWAP.csv')  

aud_citi_atm_data.to_csv(citi_input_root + 'main/AUD_ATM.csv')
aud_citi_otm_data.to_csv(citi_input_root + 'main/AUD_OTM.csv')  
aud_citi_swap_data.to_csv(citi_input_root +'main/AUD_SWAP.csv')  

gbp_citi_atm_data.to_csv(citi_input_root + 'main/GBP_ATM.csv')
gbp_citi_swap_data.to_csv(citi_input_root + 'main/GBP_SWAP.csv')
gbp_citi_swapois_data.to_csv(citi_input_root + 'main/GBP_SWAPOIS.csv')

eur_citi_atm_data.to_csv(citi_input_root + 'main/EUR_ATM.csv')
eur_citi_swap_data.to_csv(citi_input_root + 'main/EUR_SWAP.csv')


data = {
        'usd_atm':usd_citi_atm_data, 'usd_otm':usd_citi_otm_data, 'usd_swap':usd_citi_swap_data,
        'aud_atm':aud_citi_atm_data, 'aud_otm':aud_citi_otm_data, 'aud_swap':aud_citi_swap_data,
        'gbp_atm':gbp_citi_atm_data, 'gbp_swap':gbp_citi_swap_data, 'gbp_swapois':gbp_citi_swapois_data,
        'eur_atm':eur_citi_atm_data, 'eur_swap':eur_citi_swap_data 
        }


'''data integrity check'''
for k,v in data.items():
    print(r'{0}: {1} ~ {2}'.format(k,v.dropna().index[0],v.dropna().index[-1]))



'''clean up some files'''

if(3>2):
    del usd_citi_atm_data, usd_citi_otm_data, usd_citi_swap_data,
    del aud_citi_atm_data, aud_citi_otm_data, aud_citi_swap_data,
    del gbp_citi_atm_data, gbp_citi_swap_data,
    del eur_citi_atm_data, eur_citi_swap_data,

if(3>2):
    del usd_citi_atm_data_new, usd_citi_otm_data_new, usd_citi_swap_data_new,
    del aud_citi_atm_data_new, aud_citi_otm_data_new, aud_citi_swap_data_new,
    del aud_citi_otm_data_prt2_new,
    del gbp_citi_atm_data_new, gbp_citi_swap_data_new,
    del eur_citi_atm_data_new, eur_citi_swap_data_new,
    


########################################################################################
### process data
########################################################################################


''' create rvol and trading range in bps and merge with data'''

data = {**data, 
        **{k + '_rvol': calc_ann_rvol(data[k], ivol_expiries_v2) \
           for k in data.keys() if k in  ['usd_swap','aud_swap','gbp_swapois','eur_swap']}}

''' to speed up the trading range processing only 1 record per trading range is calced to represent current status'''
data = {**data, 
        **{k + '_range': calc_trading_range(data[k], ivol_expiries_v2) \
           for k in data.keys() if k in  ['usd_swap','aud_swap','gbp_swapois','eur_swap']}}


'''atm / rvol ts'''
data = {**data, 
        **{c + '_atm/fwds_1m_rvol_ts': filter_atm(data[c + '_atm'], exps=exp, tails=tails)/filter_rvol(data[c + '_swap_rvol' if c != 'gbp' else c + '_swapois_rvol'], fwds=exp, tails=tails,exps=['1M']) \
           for c in country}}

data = {**data, 
        **{c + '_atm/fwds_3m_rvol_ts': filter_atm(data[c + '_atm'], exps=exp, tails=tails)/filter_rvol(data[c + '_swap_rvol' if c != 'gbp' else c + '_swapois_rvol'], fwds=exp, tails=tails,exps=['3M']) \
           for c in country}}



# data['usd_atm/fwds_1m_rvol_ts'] = filter_atm(data['usd_atm'], exps=exp, tails=tails)/filter_rvol(data['usd_swap_rvol'],fwds=exp, tails=tails,exps=['1M'])
# data['aud_atm/fwds_1m_rvol_ts'] = filter_atm(data['aud_atm'], exps=exp, tails=tails)/filter_rvol(data['aud_swap_rvol'],fwds=exp, tails=tails,exps=['1M'])
# data['gbp_atm/fwds_1m_rvol_ts'] = filter_atm(data['gbp_atm'], exps=exp, tails=tails)/filter_rvol(data['gbp_swapois_rvol'],fwds=exp, tails=tails,exps=['1M'])
# data['eur_atm/fwds_1m_rvol_ts'] = filter_atm(data['eur_atm'], exps=exp, tails=tails)/filter_rvol(data['eur_swap_rvol'],fwds=exp, tails=tails,exps=['1M'])

# data['usd_atm/fwds_3m_rvol_ts'] = filter_atm(data['usd_atm'], exps=exp, tails=tails)/filter_rvol(data['usd_swap_rvol'],fwds=exp, tails=tails,exps=['3M'])
# data['aud_atm/fwds_3m_rvol_ts'] = filter_atm(data['aud_atm'], exps=exp, tails=tails)/filter_rvol(data['aud_swap_rvol'],fwds=exp, tails=tails,exps=['3M'])
# data['gbp_atm/fwds_3m_rvol_ts'] = filter_atm(data['gbp_atm'], exps=exp, tails=tails)/filter_rvol(data['gbp_swapois_rvol'],fwds=exp, tails=tails,exps=['3M'])
# data['eur_atm/fwds_3m_rvol_ts'] = filter_atm(data['eur_atm'], exps=exp, tails=tails)/filter_rvol(data['eur_swap_rvol'],fwds=exp, tails=tails,exps=['3M'])


'''calc atm vol premium ts '''    
    
data = {**data,
        **{c +'_atm_premium': calc_premium(filter_atm(data[c + '_atm'], exps=exp, tails=tails, dummy_strike=True).iloc[-1250:], data[c + '_swap' if  c != 'gbp' else c + '_swapois'].iloc[-1250:]) \
           for c in country}}
    

# data['usd_atm_premium'] = calc_premium(filter_atm(data['usd_atm'], exps=exp, tails=tails, dummy_strike=True).iloc[-1250:], data['usd_swap'].iloc[-1250:])
# data['aud_atm_premium'] = calc_premium(filter_atm(data['aud_atm'], exps=exp, tails=tails, dummy_strike=True).iloc[-1250:], data['aud_swap'].iloc[-1250:])
# data['gbp_atm_premium'] = calc_premium(filter_atm(data['gbp_atm'], exps=exp, tails=tails, dummy_strike=True).iloc[-1250:], data['gbp_swapois'].iloc[-1250:])
# data['eur_atm_premium'] = calc_premium(filter_atm(data['eur_atm'], exps=exp, tails=tails, dummy_strike=True).iloc[-1250:], data['eur_swap'].iloc[-1250:])
    
    
'''calc otm vol premium ts '''    

data = {**data,
        **{c +'_otm_premium': calc_premium(filter_otm(data[c + '_otm'], exps=exp, tails=tails, strikes=strikes).iloc[-1250:], data[c +'_swap'].iloc[-1250:]) \
           for c in ['usd', 'aud']}}


'''merge'''
usd_otm_premium_data = ctls.merge_dataframess(usd_otm_premium_data, data['usd_otm_premium'], axis=0)
aud_otm_premium_data = ctls.merge_dataframess(aud_otm_premium_data, data['aud_otm_premium'], axis=0)

usd_otm_premium_data.to_csv(citi_input_root +'main/USD_OTM_PREMIUM.csv')
aud_otm_premium_data.to_csv(citi_input_root +'main/AUD_OTM_PREMIUM.csv')
  
        


# data['usd_otm_premium'] = calc_premium(filter_otm(data['usd_otm'], exps=exp, tails=tails, strikes=strikes).iloc[-1250:], data['usd_swap'].iloc[-1250:])
# data['aud_otm_premium'] = calc_premium(filter_otm(data['aud_otm'], exps=exp, tails=tails, strikes=strikes).iloc[-1250:], data['aud_swap'].iloc[-1250:])

'''calc swap eqv swap roll to spots''' 

data = {**data,
        **{c + '_vol_fwd_roll': calc_fwd_roll(filter_atm(data[c + '_atm'], exps=exp, tails=tails, dummy_strike=True).iloc[-1250:], data[c + '_swap' if  c != 'gbp' else c + '_swapois'].iloc[-1250:]) \
           for c in country}}

    
# data['usd_vol_fwd_roll'] = calc_fwd_roll(filter_atm(data['usd_atm'], exps=exp, tails=tails, dummy_strike=True).iloc[-1250:], data['usd_swap'].iloc[-1250:])
# data['aud_vol_fwd_roll'] = calc_fwd_roll(filter_atm(data['aud_atm'], exps=exp, tails=tails, dummy_strike=True).iloc[-1250:], data['aud_swap'].iloc[-1250:])
# data['gbp_vol_fwd_roll'] = calc_fwd_roll(filter_atm(data['gbp_atm'], exps=exp, tails=tails, dummy_strike=True).iloc[-1250:], data['gbp_swapois'].iloc[-1250:])
# data['eur_vol_fwd_roll'] = calc_fwd_roll(filter_atm(data['eur_atm'], exps=exp, tails=tails, dummy_strike=True).iloc[-1250:], data['eur_swap'].iloc[-1250:])




########################################################################################
### process output
########################################################################################




'''output'''

process_date = data['usd_swap'].index[-1].strftime('%Y-%m-%d')

output = {}


'''ivol'''
output = {**output, 
          **{k: pivot_ds_to_table(data[k].iloc[-1],exp,tails, ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if 'atm' in k  and 'atm_premium' not in k}}

'''ivol momentum'''
output = {**output, 
          **{f'{k}_1m_mmt': pivot_ds_to_table(data[k].iloc[-1],exp,tails, ridx_rfmt=lambda x: f'{x}') \
             - pivot_ds_to_table(data[k].iloc[-20],exp,tails, ridx_rfmt=lambda x: f'{x}')
             for k in data.keys() if 'atm' in k and 'atm_premium' not in k}}

output = {**output, 
          **{f'{k}_3m_mmt': pivot_ds_to_table(data[k].iloc[-1],exp,tails, ridx_rfmt=lambda x: f'{x}') \
             - pivot_ds_to_table(data[k].iloc[-20*3],exp,tails, ridx_rfmt=lambda x: f'{x}')
             for k in data.keys() if 'atm' in k and 'atm_premium' not in k}}

    
output = {**output, 
          **{f'{k}_6m_mmt': pivot_ds_to_table(data[k].iloc[-1],exp,tails, ridx_rfmt=lambda x: f'{x}') \
             - pivot_ds_to_table(data[k].iloc[-20*6],exp,tails, ridx_rfmt=lambda x: f'{x}')
             for k in data.keys() if 'atm' in k and 'atm_premium' not in k}}
    
    
'''ivol zsc'''
output = {**output, 
          **{f'{k}_3m_zsc': pivot_ds_to_table(ctls.zsc_df(data[k].iloc[-20*3:]).iloc[-1],exp,tails, ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if 'atm' in k and 'atm_premium' not in k}}

output = {**output, 
          **{f'{k}_1y_zsc': pivot_ds_to_table(ctls.zsc_df(data[k].iloc[-255:]).iloc[-1],exp,tails, ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if 'atm' in k and 'atm_premium' not in k}}


'''spot trading range for different legnth '''
output = {**output,
          **{k.split('_')[0] + '_spot_range' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'{x}_0Y_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if k in ['usd_swap_range', 'aud_swap_range', 'gbp_swapois_range', 'eur_swap_range']}}
      

'''avg of rvol for the spot rate for different legnth '''
output = {**output,
          **{k.split('_')[0] + '_avg_spot_rvol' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'{x}_0Y_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if k in ['usd_swap_rvol','aud_swap_rvol','gbp_swapois_rvol','eur_swap_rvol']}}


'''1m rvol for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_1m_rvol' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'1M_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in ['usd_swap_rvol','aud_swap_rvol','gbp_swapois_rvol','eur_swap_rvol']}}

'''1m range for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_1m_range' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'1M_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if k in ['usd_swap_range', 'aud_swap_range', 'gbp_swapois_range', 'eur_swap_range']}}
    

'''3m rvol for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_3m_rvol' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'3M_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in ['usd_swap_rvol','aud_swap_rvol','gbp_swapois_rvol','eur_swap_rvol']}}


'''3m range for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_3m_range' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'3M_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if k in ['usd_swap_range', 'aud_swap_range', 'gbp_swapois_range', 'eur_swap_range']}}
    
    

'''6m rvol for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_6m_rvol' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'6M_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in ['usd_swap_rvol','aud_swap_rvol','gbp_swapois_rvol','eur_swap_rvol']}}

    

'''6m range for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_6m_range' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'6M_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if k in ['usd_swap_range', 'aud_swap_range', 'gbp_swapois_range', 'eur_swap_range']}}
        


'''1y rvol for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_1y_rvol' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'1Y_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in ['usd_swap_rvol','aud_swap_rvol','gbp_swapois_rvol','eur_swap_rvol']}}


'''1y range for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_1y_range' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'1Y_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if k in ['usd_swap_range', 'aud_swap_range', 'gbp_swapois_range', 'eur_swap_range'] }}
    

    
'''2y rvol for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_2y_rvol' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'2Y_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for  k in ['usd_swap_rvol','aud_swap_rvol','gbp_swapois_rvol','eur_swap_rvol']}}

    
'''2y range for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_2y_range' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'2Y_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if k in ['usd_swap_range', 'aud_swap_range', 'gbp_swapois_range', 'eur_swap_range'] }}
    

    
'''5y rvol for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_5y_rvol' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'5Y_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for  k in ['usd_swap_rvol','aud_swap_rvol','gbp_swapois_rvol','eur_swap_rvol']}}


'''5y range for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_5y_range' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'5Y_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if k in ['usd_swap_range', 'aud_swap_range', 'gbp_swapois_range', 'eur_swap_range'] }}
    

'''10y rvol for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_10y_rvol' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'10Y_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for  k in ['usd_swap_rvol','aud_swap_rvol','gbp_swapois_rvol','eur_swap_rvol']}}


'''10y range for ivol matching fwds'''
output = {**output,
          **{k.split('_')[0] + '_fwds_10y_range' : pivot_ds_to_table(data[k].iloc[-1],exp,tails,idx_filter=lambda x, y:f'10Y_{x}_{y}', ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if k in ['usd_swap_range', 'aud_swap_range', 'gbp_swapois_range', 'eur_swap_range'] }}
    
    
    
'''atm/avg_spot_rvol'''
output = {**output,
          **{f'{c}_atm/avg_spot_rvol':output[f'{c}_atm']/output[f'{c}_avg_spot_rvol'] for c in country}}

'''atm/fwds_1m_rvol'''
output = {**output,
          **{f'{c}_atm/fwds_1m_rvol':output[f'{c}_atm']/output[f'{c}_fwds_1m_rvol'] for c in country}}


'''atm/fwds_3m_rvol'''
output = {**output,
          **{f'{c}_atm/fwds_3m_rvol':output[f'{c}_atm']/output[f'{c}_fwds_3m_rvol'] for c in country}}

'''atm/fwds_6m_rvol'''
output = {**output,
          **{f'{c}_atm/fwds_6m_rvol':output[f'{c}_atm']/output[f'{c}_fwds_6m_rvol'] for c in country}}


'''atm/fwds_1y_rvol'''
output = {**output,
          **{f'{c}_atm/fwds_1y_rvol':output[f'{c}_atm']/output[f'{c}_fwds_1y_rvol'] for c in country}}


'''atm/fwds_3m_rvol zsc'''

output = {**output, 
          **{k.replace('_ts','_zsc'): pivot_ds_to_table(ctls.zsc_df(data[k]).iloc[-1], exp, tails, ridx_rfmt=lambda x: f'{x}') \
             for k in data.keys() if '_atm/fwds_3m_rvol_ts' in k}}


'''ivol in yield premium'''
output = {**output, 
          **{k: pivot_ds_to_table(data[k].iloc[-1], exp, tails,
                                  idx_filter=lambda x, y: f'{x}_{y}_+0',
                                  ridx_rfmt=lambda x: f'{x}') for k in data.keys() if 'atm_premium' in k}}
    


'''swaption eqv fwd roll to spot'''
output = {**output, 
          **{k: pivot_ds_to_table(data[k].iloc[-1], exp, tails,
                                  idx_filter=lambda x, y: f'{x}_{y}_+0',
                                  ridx_rfmt=lambda x: f'{x}') for k in data.keys() if 'vol_fwd_roll' in k}}
  
  

'''swaption otm vol'''

output['usd_otm_vol'] = pd.DataFrame({k : v.iloc[-1] \
                                          for k,v in pivot(data['usd_otm'], l1s= otm_exps, 
                                                                          l2s= otm_tails, filter_f = lambda a,b:f'{a}_{b}', 
                                                                          filtered_col_rename_f = lambda c: c.split('_')[2]).items()}).transpose()[strikes].transpose()


output['aud_otm_vol'] = pd.DataFrame({k : v.iloc[-1] \
                                          for k,v in pivot(data['aud_otm'], l1s= otm_exps, 
                                                                          l2s= otm_tails, filter_f = lambda a,b:f'{a}_{b}', 
                                                                          filtered_col_rename_f = lambda c: c.split('_')[2]).items()}).transpose()[strikes].transpose()


'''swaption otm vol'''

output['usd_otm_premium'] = pd.DataFrame({k : v.iloc[-1] \
                                          for k,v in pivot(data['usd_otm_premium'], l1s= otm_exps, 
                                                                          l2s= otm_tails, filter_f = lambda a,b:f'{a}_{b}', 
                                                                          filtered_col_rename_f = lambda c: c.split('_')[2]).items()}).transpose()[strikes].transpose()


output['aud_otm_premium'] = pd.DataFrame({k : v.iloc[-1] \
                                          for k,v in pivot(data['aud_otm_premium'], l1s= otm_exps, 
                                                                          l2s= otm_tails, filter_f = lambda a,b:f'{a}_{b}', 
                                                                          filtered_col_rename_f = lambda c: c.split('_')[2]).items()}).transpose()[strikes].transpose()
    
    

output['usd_otm_vol_ts_ditct'] = pivot(data['usd_otm'], l1s= otm_exps, l2s= otm_tails, filter_f = lambda a,b:f'{a}_{b}', filtered_col_rename_f = lambda c: c.split('_')[2])
    

output['aud_otm_vol_ts_ditct'] = pivot(data['aud_otm'], l1s= otm_exps, l2s= otm_tails, filter_f = lambda a,b:f'{a}_{b}', filtered_col_rename_f = lambda c: c.split('_')[2])
    

output['usd_otm_vol_prem_ts_ditct'] = pivot(data['usd_otm_premium'], l1s= otm_exps, l2s= otm_tails, filter_f = lambda a,b:f'{a}_{b}', filtered_col_rename_f = lambda c: c.split('_')[2])
    

output['aud_otm_vol_prem_ts_ditct'] = pivot(data['aud_otm_premium'], l1s= otm_exps, l2s= otm_tails, filter_f = lambda a,b:f'{a}_{b}', filtered_col_rename_f = lambda c: c.split('_')[2])




########################################################################################
### output to csv
########################################################################################

    
if (3 > 2):
    pd.concat([unpivot_df(v, process_date, k) if k not in ['usd_otm_vol', 'aud_otm_vol', 'usd_otm_premium', 'aud_otm_premium'] else unpivot_df2(v, process_date, k) \
               for k,v in output.items() if '_ditct' not in k], axis=0).to_csv(excel_output_dir + 'vol_' + process_date + '.csv')



########################################################################################
### output to spreadsheet
### output to spreadsheet
########################################################################################



wb1 = xw.Book(excel_output_dir + r'ratesmonitor.xlsm')
sht1 = wb1.sheets('global_vol')


sht1.range((1,1),(10000,300)).clear_contents()



sht1.range('d2').value = process_date

sht1.range('b2').value = "vega"
    
output_df('B3', output, 'usd_atm', sht1)
output_df('L3', output, 'aud_atm', sht1)
output_df('v3', output, 'gbp_atm', sht1)
output_df('AF3', output, 'eur_atm', sht1)

sht1.range('AP2').value = "premiumium vs roll"
    
output_df('AP3', output, 'usd_atm_premium', sht1)
output_df('AZ3', output, 'aud_atm_premium', sht1)
output_df('bj3', output, 'gbp_atm_premium', sht1)
output_df('bt3', output, 'eur_atm_premium', sht1)

output_df('AP15', output, 'usd_vol_fwd_roll', sht1)
output_df('AZ15', output, 'aud_vol_fwd_roll', sht1)
output_df('bj15', output, 'gbp_vol_fwd_roll', sht1)
output_df('bt15', output, 'eur_vol_fwd_roll', sht1)

output_df('AP27', output, 'usd_spot_range', sht1)
output_df('AZ27', output, 'aud_spot_range', sht1)
output_df('bj27', output, 'gbp_spot_range', sht1)
output_df('bt27', output, 'eur_spot_range', sht1)

output_df('AP39', output, 'usd_fwds_1m_rvol', sht1)
output_df('AZ39', output, 'aud_fwds_1m_rvol', sht1)
output_df('BJ39', output, 'gbp_fwds_1m_rvol', sht1)
output_df('BT39', output, 'eur_fwds_1m_rvol', sht1)




sht1.range('b15').value =  '''vega mmt'''
    
output_df('B16', output, 'usd_atm_1m_mmt', sht1)
output_df('L16', output, 'aud_atm_1m_mmt', sht1)
output_df('v16', output, 'gbp_atm_1m_mmt', sht1)
output_df('AF16', output, 'eur_atm_1m_mmt', sht1)

output_df('B28', output, 'usd_atm_3m_mmt', sht1)
output_df('L28', output, 'aud_atm_3m_mmt', sht1)
output_df('v28', output, 'gbp_atm_3m_mmt', sht1)
output_df('AF28', output, 'eur_atm_3m_mmt', sht1)

output_df('B40', output, 'usd_atm_6m_mmt', sht1)
output_df('L40', output, 'aud_atm_6m_mmt', sht1)
output_df('v40', output, 'gbp_atm_6m_mmt', sht1)
output_df('AF40', output, 'eur_atm_6m_mmt', sht1)


sht1.range('b52').value = '''vega distribution'''

output_df('B53', output, 'usd_atm_3m_zsc', sht1)
output_df('L53', output, 'aud_atm_3m_zsc', sht1)
output_df('v53', output, 'gbp_atm_3m_zsc', sht1)
output_df('AF53', output, 'eur_atm_3m_zsc', sht1)

output_df('B65', output, 'usd_atm_1y_zsc', sht1)
output_df('L65', output, 'aud_atm_1y_zsc', sht1)
output_df('v65', output, 'gbp_atm_1y_zsc', sht1)
output_df('AF65', output, 'eur_atm_1y_zsc', sht1)
    

sht1.range('b77').value = '''gamma'''

output_df('B78', output, 'usd_avg_spot_rvol', sht1)
output_df('L78', output, 'aud_avg_spot_rvol', sht1)
output_df('v78', output, 'gbp_avg_spot_rvol', sht1)
output_df('AF78', output, 'eur_avg_spot_rvol', sht1)

output_df('B90', output, 'usd_fwds_1m_rvol', sht1)
output_df('L90', output, 'aud_fwds_1m_rvol', sht1)
output_df('v90', output, 'gbp_fwds_1m_rvol', sht1)
output_df('AF90', output, 'eur_fwds_1m_rvol', sht1)

output_df('B102', output, 'usd_fwds_3m_rvol', sht1)
output_df('L102', output, 'aud_fwds_3m_rvol', sht1)
output_df('v102', output, 'gbp_fwds_3m_rvol', sht1)
output_df('AF102', output, 'eur_fwds_3m_rvol', sht1)

output_df('B114', output, 'usd_fwds_6m_rvol', sht1)
output_df('L114', output, 'aud_fwds_6m_rvol', sht1)
output_df('v114', output, 'gbp_fwds_6m_rvol', sht1)
output_df('AF114', output, 'eur_fwds_6m_rvol', sht1)

output_df('B126', output, 'usd_fwds_1y_rvol', sht1)
output_df('L126', output, 'aud_fwds_1y_rvol', sht1)
output_df('v126', output, 'gbp_fwds_1y_rvol', sht1)
output_df('AF126', output, 'eur_fwds_1y_rvol', sht1)

output_df('B138', output, 'usd_fwds_5y_rvol', sht1)
output_df('L138', output, 'aud_fwds_5y_rvol', sht1)
output_df('v138', output, 'gbp_fwds_5y_rvol', sht1)
output_df('AF138', output, 'eur_fwds_5y_rvol', sht1)

output_df('B150', output, 'usd_fwds_10y_rvol', sht1)
output_df('L150', output, 'aud_fwds_10y_rvol', sht1)
output_df('v150', output, 'gbp_fwds_10y_rvol', sht1)
output_df('AF150', output, 'eur_fwds_10y_rvol', sht1)

sht1.range('b162').value = '''i/r ratio'''
output_df('B163', output, 'usd_atm/avg_spot_rvol', sht1)
output_df('L163', output, 'aud_atm/avg_spot_rvol', sht1)
output_df('v163', output, 'gbp_atm/avg_spot_rvol', sht1)
output_df('AF163', output, 'eur_atm/avg_spot_rvol', sht1)

output_df('B175', output, 'usd_atm/fwds_1m_rvol', sht1)
output_df('L175', output, 'aud_atm/fwds_1m_rvol', sht1)
output_df('v175', output, 'gbp_atm/fwds_1m_rvol', sht1)
output_df('AF175', output, 'eur_atm/fwds_1m_rvol', sht1)

output_df('B187', output, 'usd_atm/fwds_3m_rvol', sht1)
output_df('L187', output, 'aud_atm/fwds_3m_rvol', sht1)
output_df('v187', output, 'gbp_atm/fwds_3m_rvol', sht1)
output_df('AF187', output, 'eur_atm/fwds_3m_rvol', sht1)

output_df('B199', output, 'usd_atm/fwds_6m_rvol', sht1)
output_df('L199', output, 'aud_atm/fwds_6m_rvol', sht1)
output_df('v199', output, 'gbp_atm/fwds_6m_rvol', sht1)
output_df('AF199', output, 'eur_atm/fwds_6m_rvol', sht1)

output_df('B211', output, 'usd_atm/fwds_1y_rvol', sht1)
output_df('L211', output, 'aud_atm/fwds_1y_rvol', sht1)
output_df('v211', output, 'gbp_atm/fwds_1y_rvol', sht1)
output_df('AF211', output, 'eur_atm/fwds_1y_rvol', sht1)


output_df('B223', output, 'usd_atm/fwds_3m_rvol_zsc', sht1)
output_df('L223', output, 'aud_atm/fwds_3m_rvol_zsc', sht1)
output_df('v223', output, 'gbp_atm/fwds_3m_rvol_zsc', sht1)
output_df('AF223', output, 'eur_atm/fwds_3m_rvol_zsc', sht1)




sht1.range('CD2').value = "otm vol"
    
output_df('CD3', output, 'usd_otm_vol', sht1)
output_df('CD13', output, 'aud_otm_vol', sht1)


sht1.range('CD23').value = "otm premi"
    
output_df('CD24', output, 'usd_otm_premium', sht1)
output_df('CD34', output, 'aud_otm_premium', sht1)









if (2 > 3):
    
    output['usd_otm_vol_prem_ts_ditct']['3M_2Y']['+0'].plot()
    
    
    test_df = pd.DataFrame({'REALISED_ROLL_1M_USSW3': (data['usd_swap']['1M_3Y'].shift(20) - data['usd_swap']['0Y_3Y'])})
    test_df['REALISED_VOL_1M_USSW3'] = data['usd_swap']['0Y_3Y'].diff().rolling(20).std()
    
    test_df_rvol_rroll['AVG_NEXT_MONTH_REALISED_ROLL_1M_USSW3'] = test_df_rvol_rroll['REALISED_ROLL_1M_USSW3'].rolling(20).mean().shift(-20)
    
    sbins.sbins(test_df_rvol_rroll.dropna(), 'REALISED_VOL_1M_USSW3', 10)
    test_df_rvol_rroll_bins = sbins.sbins(test_df_rvol_rroll.dropna(), 'REALISED_VOL_1M_USSW3', 10)
    test_df_rvol_rroll_bins.plot_bin_mean_variance_for_col('AVG_NEXT_MONTH_REALISED_ROLL_1M_USSW3')











