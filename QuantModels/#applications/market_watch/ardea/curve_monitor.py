import pandas as pd
import numpy as np
import os, sys, inspect
from operator import itemgetter
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations
from pandas.tseries.offsets import DateOffset
import xlwings as xw


_p = 'C:\Dev\mancave\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import sbins
from statistics.mr import stochastic as stoch
from statistics.mr import helper
from statistics.mr import tools as mrt


from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
from scipy.spatial.distance import pdist, squareform

from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from pandas.plotting import autocorrelation_plot

import seaborn as sns
import numpy as np
import scipy
import scipy.stats as st



pd.set_option('display.expand_frame_repr', False)



def convert_atmf_vol_to_yield(option, vols):

    base = 1/np.sqrt(2*3.14*12)
    if option.upper() == '1M':
        return base * np.sqrt(1) * vols
    elif option.upper() == '2M':
        return base * np.sqrt(2) * vols     
    elif option.upper() == '3M':
        return base * np.sqrt(3) * vols
    elif option.upper() == '6M':
        return base * np.sqrt(6)* vols
    elif option.upper() == '1Y':
        return base * np.sqrt(12)* vols
    elif option.upper() == '2Y':
        return base * np.sqrt(24)* vols
    elif option.upper() == '3Y':
        return base * np.sqrt(36)* vols
    elif option.upper() == '4Y':
        return base * np.sqrt(48)* vols
    elif option.upper() == '5Y':
        return base * np.sqrt(60)* vols
    elif option.upper() == '6Y':
        return base * np.sqrt(72)* vols
    elif option.upper() == '7Y':
        return base * np.sqrt(84)* vols
    elif option.upper() == '8Y':
        return base * np.sqrt(96)* vols
    elif option.upper() == '9Y':
        return base * np.sqrt(108)* vols
    elif option.upper() == '10Y':
        return base * np.sqrt(120)* vols    
    
    else:
        raise ValueError("unknown optin tenor {0}".format(option))


def plot_scatter_main(ax, xs, ys, colors, cmaps, ticklabelsize=8):
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 9)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    return mapable


def plot_scatter_endpoints(ax, xs, ys, colors, sizes, labels, markers, legend_loc=None,ncol=1, fontsize=8, lw =3):
    ds = [ax.scatter(xs[i], ys[i], c=colors[i], s=sizes[i], label=labels[i], marker=markers[i], linewidth= lw) for i in np.arange(len(xs))]
    if legend_loc is not None:
        lg =ax.legend(ds, labels, scatterpoints=1, fancybox=True, framealpha=0, loc=legend_loc, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lg)
    return ds
    


def plot_scatter_fits(ax, xss, yss, label_prefixes, colors, legend_loc=None, ncol=1, fontsize=8):
    #ploting multiple fits
    pcs = []
    for (xs, ys, label_prefix, color) in zip(xss, yss, label_prefixes, colors): 
        (beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(xs, ys)
        label = r'{1} = {3:.2f} + {2:.2f} * {0} with stdev {4:.2f} '.format(xs.name, ys.name, beta_x, beta_0, residual_ys_std)
        label = label_prefix + ', ' + label if label_prefix is not None else label
        pcs = pcs + ax.plot(xs, fitted_ys, lw=1, c=color, ls = '-', marker = '.', ms = 0, label=label)
        ax.plot(xs, residual_ys_p_1std, lw=0.5, c=color, ls = ':', ms = 0)
        ax.plot(xs, residual_ys_m_1std, lw=0.5, c=color, ls = ':', ms = 0)

    if legend_loc is not None:
        lgd = ax.legend(handles=pcs, loc=legend_loc, fancybox=True, framealpha=0, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lgd)
    return pcs






def get_linear_betas(xs, ys):
    betas, _, _, _, _ = np.polyfit(list(xs), list(ys), deg = 1, full=True)
    (beta_x, beta_0) = betas
    #xs = df[df.columns[0]]
    #ys = df[df.columns[1]]
    #start_x = xs.min() 
    #last_x = xs.max()
    #step = (last_x - start_x)/100 
    #xs = np.arange(start_x,last_x, step)
    fitted_ys = [round(beta_0 + beta_x * x, 4) for x in list(xs)]
    residual_ys = [y - fitted_y for y, fitted_y in  zip(ys, fitted_ys)]
    residual_ys_std = np.std(residual_ys, ddof=1) 
    residual_ys_p_1std =  [fitted_y + residual_ys_std for fitted_y in  fitted_ys]
    residual_ys_m_1std =  [fitted_y - residual_ys_std for fitted_y in  fitted_ys]
    
    return betas, (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std)
   


def bootstrap_ifs_fwd(data):
    nc =  { c: int( c[4:].replace('_SWIT','').replace('Y','') ) for c in data.columns}
    data = data.rename(columns=nc)
    a = data.columns.tolist()[1:]
    b = data.columns.tolist()[:-1]
    fwd = {}
    fwd['{0}Y'.format(data.columns[0])] = data[data.columns[0]]
    for (ca,cb) in zip(a, b):
        term = ca - cb
        forward = cb
        cca = (1 + data[ca]/100).pow(ca)
        ccb = (1 + data[cb]/100).pow(cb)
        fwd['{0}Y{1}Y'.format(forward, term)] = np.log(cca/ccb)/term * 100
    fwd = pd.DataFrame(fwd)
    return fwd


def plot_main(ax, xs, ys, xlegend, ylegend, colors, cmaps, ticklabelsize=8, mirror_limit_x = False, mirror_limit_y = False):
    
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 6)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    #for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
    #    label.set_fontsize(ticklabelsize)
    #    if tick < 0:
    #        label.set_color("red")
    #    else:
    #        label.set_color("black")
    
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()    
    
    ymax_abs = max([abs(ymin), abs(ymax)])
    xmax_abs = max([abs(xmin), abs(xmax)])
    
    xymax = max([abs(ymin), abs(ymax), abs(xmin), abs(xmax)])
    
    if mirror_limit_x:
        ax.set_xlim(-1 * xmax_abs, xmax_abs)
    if mirror_limit_y:
        ax.set_ylim(-1 * ymax_abs, ymax_abs)
    
    # Move left y-axis and bottim x-axis to centre, passing through (0,0)
    # ax.spines['left'].set_position('center')
    
    
    #ax.spines['bottom'].set_position('center')
    # Eliminate upper and right axes
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    # Show ticks in the left and lower axes only
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    
    ax.xaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_major_locator(MultipleLocator(1))
    ax.xaxis.grid(True, which='major')
    #ax.xaxis.grid(True, which='minor')
    ax.yaxis.grid(True, which='major')
    
    for label in ax.get_xticklabels(minor =False):
        label.set_fontsize(7)

    for label in ax.get_yticklabels(minor =False):
        label.set_fontsize(7)
        
    ax.set_ylabel(ylegend, fontsize=9)
    ax.yaxis.set_label_coords(0, 0.12)

    ax.set_xlabel(xlegend, fontsize=9)
    ax.xaxis.set_label_coords(0.85, 0.045)
    
    # end points
    start_x = xs.loc[xs.first_valid_index()]
    start_y = ys.loc[ys.first_valid_index()]
    start_date = xs.first_valid_index().strftime(r'%Y-%m-%d')
    
    ds1 = ax.scatter(start_x, start_y, c='#f5d9c8', s=90, alpha=0.5, label=start_date, edgecolors='b')
    
    last_x = xs.loc[xs.last_valid_index()]
    last_y = ys.loc[ys.last_valid_index()]
    last_date = xs.last_valid_index().strftime(r'%Y-%m-%d')

    ds2 = ax.scatter(last_x, last_y, c='darkred', s=90, alpha=1, label=last_date)
    
    
    ax.legend([ds1, ds2], [start_date, last_date], scatterpoints=1, fancybox=True, framealpha=0, loc=1, ncol=1, fontsize=9)
    
    return mapable


def plot_multi_lines(data, cols=None, spacing=.1, **kwargs):

    from pandas import plotting

    # Get default color style from pandas - can be changed to any other color list
    if cols is None: cols = data.columns
    if len(cols) == 0: return
    colors = getattr(getattr(plotting, '_matplotlib').style, '_get_standard_colors')(num_colors=len(cols))

    # First axis
    ax = data.loc[:, cols[0]].plot(label=cols[0], color=colors[0], **kwargs)
    ax.set_ylabel(ylabel=cols[0])
    lines, labels = ax.get_legend_handles_labels()

    for n in range(1, len(cols)):
        # Multiple y-axes
        ax_new = ax.twinx()
        ax_new.spines['right'].set_position(('axes', 1 + spacing * (n - 1)))
        data.loc[:, cols[n]].plot(ax=ax_new, label=cols[n], color=colors[n % len(colors)], **kwargs)
        ax_new.set_ylabel(ylabel=cols[n])

        # Proper legend position
        line, label = ax_new.get_legend_handles_labels()
        lines += line
        labels += label

    ax.legend(lines, labels, loc=0)
    return ax


def rolling_apply(df, window_size, func, min_window_size=None):
    if min_window_size is None:
        min_window_size = window_size
    result = {}
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here

        if len(sub_df) >= min_window_size:            
            idx = sub_df.index[-1]
            result[idx] = func(sub_df)
    return result


def get_flattened_cov(df):
    covmatx = df.cov()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def get_flattened_corr(df):
    covmatx = df.corr()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def func(df): 
    return pd.DataFrame({
                      'Now':df.iloc[-2:].mean(),
                      #'2020-09-11':df[(df.index >= '2020-09-08') & (df.index <= '2020-09-12')].mean(),
                      #'2020-09-04':df[(df.index >= '2020-09-02') & (df.index <= '2020-09-05')].mean(),
                      #'2020-08-12':df[(df.index >= '2020-08-10') & (df.index <= '2020-08-13')].mean(),
                      #'Pre speech':df.iloc[-3],
                      '7d avg':df.iloc[-7:].mean(), 
                      '7d avg 7d ago':df.iloc[-14:-7].mean(), 
                      #'7d avg 14d ago':df.iloc[-21:-14].mean(), 
                      '7d avg 21d ago':df.iloc[-28:-21].mean(), 
                      '90d avg 30d ago':df.iloc[-120:-30].mean(),
                      '90d avg 90d ago':df.iloc[-180:-90].mean(),
                      #'avg last 3 years':df.iloc[-97:-90].mean(),
                      #'7d avg 180 day ago':df.iloc[-187:-180].mean(),
                      #'7d avg 360 day ago':df.iloc[-367:-360].mean(),
                      #'2010-2011 avg': df[(df.index > '2010-01-01') & (df.index < '2011-12-31')].mean(),
                      #'2010-Oct-2011-Jul': df[(df.index > '2010-10-01') & (df.index < '2011-07-01')].mean(),
                      #'2012-2013 avg': df[(df.index > '2012-01-01') & (df.index < '2013-12-31')].mean(),
                      #'2016-2019 avg': df[(df.index > '2016-01-01') & (df.index < '2019-12-31')].mean(),
                      #'2016-08': df[(df.index > '2016-08-01') & (df.index < '2016-09-01')].mean(),
                      #'2016-09': df[(df.index > '2016-09-01') & (df.index < '2016-10-01')].mean(),
                      #'2016-10': df[(df.index > '2016-10-01') & (df.index < '2016-11-01')].mean()
                      }) 


def vol_snapshot(df,n=20):
    
    rtn = df - df.shift(1)    
    
    return pd.DataFrame({
                      'Now':rtn.iloc[-n:].std(),
                      '1m ago':rtn.iloc[-2*n:-n].std(), 
                      '2m ago':rtn.iloc[-3*n:-2*n].std(), 
                      '3m ago':rtn.iloc[-4*n:-3*n].std()                      
                      }) * np.sqrt(252)




def create_combo_mr(data, elements = 2, resample_freq= 'W', 
                    include_normal_weighted_series=True, 
                    momentum_size=None, matrixoveride = None,
                    sample_range = None):
    mrs = {}
    data_sub = data
    combos = list(combinations(data.columns, elements))
    instrument = combos[0][0][3:]
    
    for combo in combos:
        _sub_data = data_sub[list(combo)].resample(resample_freq).last() if resample_freq is not None else data_sub[list(combo)]
        data_sub_pca = pca.PCA(_sub_data.dropna(how='all',axis=1).dropna(how='all',axis=0), 
                               rates_pca = True, momentum_size = momentum_size,matrixoveride = matrixoveride,
                               sample_range = sample_range)
        hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_pca.pc_hedge
        mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
        mrs[('_').join(combo) + '_PCW'] = (mr_obj, data_sub_pca)
        if include_normal_weighted_series:
            if len(_sub_data.columns) == 2:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] - _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s - %s' % (_sub_data.columns[0], _sub_data.columns[1])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)
            elif len(_sub_data.columns) == 3:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] + _sub_data[_sub_data.columns[2]] - 2 * _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s -2 x %s + %s' % (_sub_data.columns[0], _sub_data.columns[1], _sub_data.columns[2])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)

    return mrs




def generate_returns(data,periods, val_zsc=750):
    rts = {}
    spots = {}
    distr = {}
    spot_dist = {} #get the value zscore for 5years
    spots[0] = data
    spot_dist[0] = (spots[0].iloc[-val_zsc:] - spots[0].iloc[-val_zsc:].mean())/spots[0].iloc[-val_zsc:].std() 
    rts1d = data - data.shift(1)
    for p in periods:
        rts[p] = data - data.shift(p)
        spots[p] = data.shift(p)
        spot_dist[p] = (spots[p].iloc[-val_zsc:] - spots[p].iloc[-val_zsc:].mean())/spots[p].iloc[-val_zsc:].std()
        if (p > 1):  
            distr[p] = rts1d.resample('{}D'.format(p), closed='right', label='right').std()
    
    #week to date
    
    rts['WTD'] = (data.resample('W', convention='end').last().iloc[-1] - data.resample('W', convention='end').last())[:-1]
    spots['WTD'] = data.resample('W', convention='end').last()[:-1]
    rts['MTD'] = (data.resample('M', convention='end').last().iloc[-1] - data.resample('M', convention='end').last())[:-1]
    spots['MTD'] = data.resample('W', convention='end').last()[:-1]
        
    #return (rts, spots, distr)
    return (rts, spots, spot_dist)


def generate_zscore(data,periods):
    #rts = {}
    #spots = {}
    #spots[0] = data
    zsc = {}
    for p in periods:
        #rts[p] = data - data.shift(p)
        zsc[p] = (data - data.rolling(p).mean()) / data.rolling(p).std()
        #spots[p] = data.shift(p)
        #zsc[p] = pd.DataFrame({"{} days zsc".format(p): (data.iloc[-1] - data.iloc[-30:].mean()) / data.iloc[-30:].std()}).transpose()
    #week to date        
    return zsc





def refmt_returns_by_p(rts, tickers, date_offset):
    rfmt_rts_by_p = {}
    for rt_key, rt in rts.items():
        rfmt_rts = []
        for ticker in tickers:
                           
                tmp = rt.iloc[date_offset][ticker].rename(lambda n: n[:3]).rename(ticker[0][3:]).to_frame()
                #print(tmp)
                rfmt_rts.append(tmp)
                #if len(rfmt_rts) > 1:
                #    print(pd.concat(rfmt_rts, axis=1, join='outer',sort=False))

        tmp = pd.concat(rfmt_rts, axis=1, join='outer',sort=False).transpose()
        
        if 'AUD' in tmp.columns and  'USD' in tmp.columns:
            tmp['AUDUSD'] = tmp['AUD'] - tmp['USD']
        if 'CAD' in tmp.columns and  'USD' in tmp.columns:
            tmp['CADUSD'] = tmp['CAD'] - tmp['USD']
        if 'EUR' in tmp.columns and  'USD' in tmp.columns:
            tmp['EURUSD'] = tmp['EUR'] - tmp['USD']
        if 'AUD' in tmp.columns and  'NZD' in tmp.columns:
            tmp['AUDNZD'] = tmp['AUD'] - tmp['NZD']
        rfmt_rts_by_p[rt_key] = tmp
    return rfmt_rts_by_p


def clean_citi_data(df, col_rename_func, col_name_filter_func=None):
    #col 0 is the date
    _df = df[(~ df.iloc[:,0].str.contains('---', na=False)) & \
             (~ df.iloc[:,0].str.contains('confidential', na=False))  & \
             (~ df.iloc[:,0].str.contains('ource', na=False)) & \
             (df.iloc[:,0].str.len() > 0)]
    #name the date column
    _df = _df.rename(columns={_df.columns[0]: 'date'})  
    
    _df['date'] = pd.to_datetime(_df['date'])
    _df = _df.set_index('date')
    # filter col  
    _dfc = _df.columns if col_name_filter_func is None else [ c for c in _df.columns if col_name_filter_func(c)]
    _df = _df[_dfc].astype('double')
    
    # rename col    
    _df = _df.rename(col_rename_func, axis=1)  
    
    return _df


def vol_col_name_cleaner(c):    
    cs = c.split(' ')
    keys = [c.upper() for c in itemgetter(*[3,0,2])(cs)]
    new_c = '{}_{}_{}'.format(*keys)
    return new_c
    

def basis_col_name_cleaner(c):    
    cs = c.split(' ')
    keys = [c.upper() for c in itemgetter(*[1,0,2])(cs)]
    new_c = '{}_{}_{}'.format(*keys)
    return new_c



def asw_col_name_cleaner(c):    
    cs = c.split(' ')
    keys = [c.upper() for c in itemgetter(*[0,3,1])(cs)]
    new_c = '{}_{}_{}'.format(*keys)
    return new_c




rundate = '2024-03-01'
runVol = True





##########################################################
#
#  
# pp section    
#
#
##########################################################





data_cad_swap = pd.read_csv('C:/Temp/test/market_watch/staging/'  + rundate +  '_CADSWAP_ALL_V3_historical.csv')  
data_cad_swap['date'] = pd.to_datetime(data_cad_swap['date'])
data_cad_swap = data_cad_swap.set_index('date').ffill()




data_gov = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_GOV_historical.csv')  
data_gov['date'] = pd.to_datetime(data_gov['date'])
data_gov = data_gov.set_index('date').ffill()


data_ivol = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_VOL_historical.csv')  
data_ivol['date'] = pd.to_datetime(data_ivol['date'])
data_ivol = data_ivol.set_index('date').ffill()


data_swap = pd.read_csv('C:/Temp/test/market_watch/staging/'  + rundate +  '_SWAP_ALL_V3_historical.csv')  
data_swap['date'] = pd.to_datetime(data_swap['date'])
data_swap = data_swap.set_index('date').ffill()
#data_swap = data_swap.drop('AUD_0Y_1D', 1) #need to remove from bbg input file


data_beirr = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_BEIRR_historical.csv')  
data_beirr['date'] = pd.to_datetime(data_beirr['date'])
data_beirr = data_beirr.set_index('date').ffill()



data_ifs = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SWIT_historical.csv')  
data_ifs['date'] = pd.to_datetime(data_ifs['date'])
data_ifs = data_ifs.set_index('date').ffill()



data_ccy_basis = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_BS_ALL_historical.csv')  
data_ccy_basis['date'] = pd.to_datetime(data_ccy_basis['date'])
data_ccy_basis = data_ccy_basis.set_index('date').ffill()
data_ccy_basis = data_ccy_basis[[c for c in data_ccy_basis.columns if '3M' not in c]]



data_sovcds = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SOVCDS_historical.csv')  
data_sovcds['date'] = pd.to_datetime(data_sovcds['date'])
data_sovcds = data_sovcds.set_index('date').ffill() #.drop(['IND','GRC'],axis=1)



data_stirfut = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_STIRFUT_historical.csv')  
data_stirfut['date'] = pd.to_datetime(data_stirfut['date'])
data_stirfut = data_stirfut.set_index('date').ffill()


data_beirr = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_BEIRR_historical.csv')  
data_beirr['date'] = pd.to_datetime(data_beirr['date'])
data_beirr = data_beirr.set_index('date').ffill()



data_ss = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SWAP_SPREAD_historical.csv')  
data_ss['date'] = pd.to_datetime(data_ss['date'])
data_ss = data_ss.set_index('date').ffill()


data_ss.loc['2022-08-02','AUD_1Y_SS'] = 60
data_ss.loc['2022-08-02','AUD_2Y_SS'] = 52



data_linker = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_linker_historical.csv')  
data_linker['date'] = pd.to_datetime(data_linker['date'])
data_linker = data_linker.set_index('date').ffill()


if runVol:
    data_citivol = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_CITIVOL_historical.csv', skiprows=2, skip_blank_lines=True)  
    data_citivol = clean_citi_data(data_citivol, vol_col_name_cleaner).ffill()
    
    data_citibasis = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_CITIBASIS_historical.csv', skiprows=2, skip_blank_lines=True)  
    data_citibasis = clean_citi_data(data_citibasis, basis_col_name_cleaner).ffill()
    
    data_citiasw = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_CITIASW_historical.csv', skiprows=2, skip_blank_lines=True)  
    data_citiasw = clean_citi_data(data_citiasw, asw_col_name_cleaner).ffill()




data_swaprr = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_SWAP_RR_historical.csv')  
data_swaprr['date'] = pd.to_datetime(data_swaprr['date'])
data_swaprr = data_swaprr.set_index('date').ffill()

data_bob = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_BOB_ALL_historical.csv')  
data_bob['date'] = pd.to_datetime(data_bob['date'])
data_bob = data_bob.set_index('date').ffill()


data_semi = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_SEMI_ALL_historical.csv')  
data_semi['date'] = pd.to_datetime(data_semi['date'])
data_semi = data_semi.set_index('date').ffill()






#temp logic to clear data post june for month end


if(0 > 1):
    
# =============================================================================
#     start_offset = -3570 # lb gfc fci peak
#     data_gov = data_gov.iloc[:start_offset]
#     data_ivol = data_ivol.iloc[:start_offset]
#     data_swap = data_swap.iloc[:start_offset]
#     data_beirr = data_beirr.iloc[:start_offset]
#     data_ifs = data_ifs.iloc[:start_offset]
#     data_ccy_basis = data_ccy_basis.iloc[:start_offset]
#     data_sovcds = data_sovcds.iloc[:start_offset]
#     data_stirfut = data_stirfut.iloc[:start_offset]
#     data_ss = data_ss.iloc[:start_offset]
#     data_linker = data_linker.iloc[:start_offset]
#     
# =============================================================================
    end_date = '2008-11-11'
    data_gov = data_gov.iloc[data_gov.index < end_date,:]
    data_ivol = data_ivol.iloc[data_ivol.index< end_date,:]
    data_swap = data_swap.iloc[data_swap.index < end_date,:]
    data_beirr = data_beirr.iloc[data_beirr.index<end_date,:]
    data_ifs = data_ifs.iloc[data_ifs.index<end_date,:]
    data_ccy_basis = data_ccy_basis.iloc[data_ccy_basis.index<end_date,:]
    data_sovcds = data_sovcds.iloc[data_sovcds.index < end_date,:]
    data_stirfut = data_stirfut.iloc[data_stirfut.index <end_date,:]
    data_ss = data_ss.iloc[data_ss.index<end_date,:]
    data_linker = data_linker.iloc[data_linker.index<end_date,:]
    






##########################################################
# PP - create swap ticker
##########################################################


swap_ticker_3m = [c for c in data_swap.columns if '_0Y_3M' in c and  'NOK' not in c and  'NOK' not in c]
swap_ticker_1y = [c for c in data_swap.columns if '_0Y_1Y' in c and  'NOK' not in c]
swap_ticker_2y = [c for c in data_swap.columns if '_0Y_2Y' in c and  'NOK' not in c]
swap_ticker_3y = [c for c in data_swap.columns if '_0Y_3Y' in c and  'NOK' not in c]
swap_ticker_4y = [c for c in data_swap.columns if '_0Y_4Y' in c and  'NOK' not in c]
swap_ticker_5y = [c for c in data_swap.columns if '_0Y_5Y' in c and  'NOK' not in c]
swap_ticker_7y = [c for c in data_swap.columns if '_0Y_7Y' in c and  'NOK' not in c]
swap_ticker_10y = [c for c in data_swap.columns if '_0Y_10Y' in c and  'NOK' not in c]
swap_ticker_1y1y = [c for c in data_swap.columns if '_1Y_1Y' in c and  'NOK' not in c]
swap_ticker_1y2y = [c for c in data_swap.columns if '_1Y_2Y' in c and  'NOK' not in c]
swap_ticker_1y5y = [c for c in data_swap.columns if '_1Y_5Y' in c and  'NOK' not in c]
swap_ticker_1y10y = [c for c in data_swap.columns if '_1Y_10Y' in c and  'NOK' not in c]
swap_ticker_2y1y = [c for c in data_swap.columns if '_2Y_1Y' in c and  'NOK' not in c]
swap_ticker_2y2y = [c for c in data_swap.columns if '_2Y_2Y' in c and  'NOK' not in c]
swap_ticker_2y5y = [c for c in data_swap.columns if '_2Y_5Y' in c and  'NOK' not in c]
swap_ticker_2y10y = [c for c in data_swap.columns if '_2Y_10Y' in c and  'NOK' not in c]
swap_ticker_3y2y = [c for c in data_swap.columns if '_3Y_2Y' in c and  'NOK' not in c]
swap_ticker_5y5y = [c for c in data_swap.columns if '_5Y_5Y' in c and  'NOK' not in c]
swap_ticker_2y2y = [c for c in data_swap.columns if '_2Y_2Y' in c and  'NOK' not in c]
swap_ticker_10y5y = [c for c in data_swap.columns if '_10Y_5Y' in c and  'NOK' not in c]
swap_ticker_10y10y = [c for c in data_swap.columns if '_10Y_10Y' in c and  'NOK' not in c]
swap_ticker_30y = [c for c in data_swap.columns if '_0Y_30Y' in c and  'NOK' not in c and 'THB' not in c and 'HKD' not in c and 'NZD' not in c and 'SGD' not in c]




##########################################################
# PP - create swap curve fly data and ticker
##########################################################


data_swap_3ms2s = {}
if True:
    for i in np.arange(0, len(swap_ticker_2y)):
         data_swap_3ms2s[swap_ticker_2y[i][:3] + '3ms2s'] =  data_swap[swap_ticker_2y[i]] - data_swap[swap_ticker_3m[i]]
    data_swap_3ms2s = pd.DataFrame(data_swap_3ms2s)



data_swap_2s10s = {}
if True:
    for i in np.arange(0, len(swap_ticker_2y)):
         data_swap_2s10s[swap_ticker_10y[i][:3] + '2s10s'] =  data_swap[swap_ticker_10y[i]] - data_swap[swap_ticker_2y[i]]
    data_swap_2s10s = pd.DataFrame(data_swap_2s10s)

data_swap_2s5s = {}
if True:
    for i in np.arange(0, len(swap_ticker_2y)):
         data_swap_2s5s[swap_ticker_5y[i][:3] + '2s5s'] =  data_swap[swap_ticker_5y[i]] - data_swap[swap_ticker_2y[i]]
    data_swap_2s5s = pd.DataFrame(data_swap_2s5s)

data_swap_5s10s = {}
if True:
    for i in np.arange(0, len(swap_ticker_10y)):
         data_swap_5s10s[swap_ticker_5y[i][:3] + '5s10s'] =  data_swap[swap_ticker_10y[i]] - data_swap[swap_ticker_5y[i]]
    data_swap_5s10s = pd.DataFrame(data_swap_5s10s)
    
data_swap_10s30s = {}
swap_ticker_temp = [c.replace('_0Y_30Y', '_0Y_10Y') for c in swap_ticker_30y]
if True:
    for i in np.arange(0, len(swap_ticker_30y)):
         data_swap_10s30s[swap_ticker_30y[i][:3] + '10s30s'] =  data_swap[swap_ticker_30y[i]] - data_swap[swap_ticker_temp[i]]
    data_swap_10s30s = pd.DataFrame(data_swap_10s30s)
    

data_swap_2s30s = {}
swap_ticker_temp = [c.replace('_0Y_30Y', '_0Y_2Y') for c in swap_ticker_30y]
if True:
    for i in np.arange(0, len(swap_ticker_30y)):
         data_swap_2s30s[swap_ticker_30y[i][:3] + '2s30s'] =  data_swap[swap_ticker_30y[i]] - data_swap[swap_ticker_temp[i]]
    data_swap_2s30s = pd.DataFrame(data_swap_2s30s)


data_swap_2s5s10s = {}
if True:
    for i in np.arange(0, len(swap_ticker_2y)):
         data_swap_2s5s10s[swap_ticker_2y[i][:3] + 'P2s5s10s'] =  2 * data_swap[swap_ticker_5y[i]] - data_swap[swap_ticker_2y[i]] - data_swap[swap_ticker_10y[i]]
    data_swap_2s5s10s = pd.DataFrame(data_swap_2s5s10s)
    

data_swap_5s30s = {}
swap_ticker_temp = [c.replace('_0Y_30Y', '_0Y_5Y') for c in swap_ticker_30y]
if True:
    for i in np.arange(0, len(swap_ticker_30y)):
         data_swap_5s30s[swap_ticker_30y[i][:3] + '5s30s'] =  data_swap[swap_ticker_30y[i]] - data_swap[swap_ticker_temp[i]]
    data_swap_5s30s = pd.DataFrame(data_swap_5s30s)



data_swap_5s10s30s = {}
swap_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in swap_ticker_30y]
swap_ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in swap_ticker_30y]
if True:
    for i in np.arange(0, len(swap_ticker_30y)):
         data_swap_5s10s30s[swap_ticker_30y[i][:3] + 'P5s10s30s'] =  2 * data_swap[swap_ticker_temp2[i]] - data_swap[swap_ticker_temp1[i]] - data_swap[swap_ticker_30y[i]]
    data_swap_5s10s30s = pd.DataFrame(data_swap_5s10s30s)
    

data_swap_2s10s30s = {}
swap_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in swap_ticker_30y]
swap_ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in swap_ticker_30y]
if True:
    for i in np.arange(0, len(swap_ticker_30y)):
         data_swap_2s10s30s[swap_ticker_30y[i][:3] + 'P2s10s30s'] =  2 * data_swap[swap_ticker_temp2[i]] - data_swap[swap_ticker_temp1[i]] - data_swap[swap_ticker_30y[i]]
    data_swap_2s10s30s = pd.DataFrame(data_swap_2s10s30s)



data_swap_22s55s = {}
if True:
    for i in np.arange(0, len(swap_ticker_2y2y)):
         data_swap_22s55s[swap_ticker_2y2y[i][:3] + '22s55s'] =  data_swap[swap_ticker_5y5y[i]] - data_swap[swap_ticker_2y2y[i]]
    data_swap_22s55s = pd.DataFrame(data_swap_22s55s)



data_swap_12s110s = {}
if True:
    for i in np.arange(0, len(swap_ticker_1y2y)):
         data_swap_12s110s[swap_ticker_1y2y[i][:3] + '12s110s'] =  data_swap[swap_ticker_1y10y[i]] - data_swap[swap_ticker_1y2y[i]]
    data_swap_12s110s = pd.DataFrame(data_swap_12s110s)



data_swap_2s5y5s = {}
if True:
    for i in np.arange(0, len(swap_ticker_5y5y)):
         data_swap_2s5y5s[swap_ticker_5y5y[i][:3] + '2s55s'] =  data_swap[swap_ticker_5y5y[i]] - data_swap[swap_ticker_5y5y[i][:3] + '_0Y_2Y']
    data_swap_2s5y5s = pd.DataFrame(data_swap_2s5y5s)


data_swap_2s55s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in swap_ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in swap_ticker_30y]
if True:
    for i in np.arange(0, len(swap_ticker_30y)):
         if (ticker_temp2[i] in data_swap.columns and ticker_temp1[i] in data_swap.columns): 
             data_swap_2s55s30s[swap_ticker_30y[i][:3] + 'P2s55s30s'] =  2 * data_swap[ticker_temp2[i]] - data_swap[ticker_temp1[i]] - data_swap[swap_ticker_30y[i]]
    data_swap_2s55s30s = pd.DataFrame(data_swap_2s55s30s)



data_swap_55s105s = {}
ticker_temp2 = [c.replace('_10Y_5Y', '_5Y_5Y') for c in swap_ticker_10y5y]
if True:
    for i in np.arange(0, len(swap_ticker_10y5y)):
        if (ticker_temp2[i] in data_swap.columns):
            data_swap_55s105s[swap_ticker_10y5y[i][:3] + '55s105s'] = data_swap[swap_ticker_10y5y[i]] - data_swap[ticker_temp2[i]]  
    data_swap_55s105s = pd.DataFrame(data_swap_55s105s)



data_swap_55s30s = {}
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in swap_ticker_30y]
if True:
    for i in np.arange(0, len(swap_ticker_30y)):
        if (ticker_temp2[i] in data_swap.columns):
            data_swap_55s30s[swap_ticker_30y[i][:3] + '55s30s'] = data_swap[swap_ticker_30y[i]] - data_swap[ticker_temp2[i]]  
    data_swap_55s30s = pd.DataFrame(data_swap_55s30s)


data_swap_55s1010s = {}

if True:
    for i in np.arange(0, len(swap_ticker_5y5y)):
         data_swap_55s1010s[swap_ticker_5y5y[i][:3] + '55s1010s'] = data_swap[swap_ticker_10y10y[i]] - data_swap[swap_ticker_5y5y[i]]  
    data_swap_55s1010s = pd.DataFrame(data_swap_55s1010s)


data_swap_11s55s1010s = {}
if True:
    for i in np.arange(0, len(swap_ticker_1y1y)):
         data_swap_11s55s1010s[swap_ticker_1y1y[i][:3] + 'P11s55s1010s'] =  2 * data_swap[swap_ticker_5y5y[i]] - data_swap[swap_ticker_1y1y[i]] - data_swap[swap_ticker_10y10y[i]]
    data_swap_11s55s1010s = pd.DataFrame(data_swap_11s55s1010s)


data_swap_11s22s55s = {}
if True:
    for i in np.arange(0, len(swap_ticker_1y1y)):
         data_swap_11s22s55s[swap_ticker_1y1y[i][:3] + 'P11s22s55s'] =  2 * data_swap[swap_ticker_2y2y[i]] - data_swap[swap_ticker_1y1y[i]] - data_swap[swap_ticker_5y5y[i]]
    data_swap_11s22s55s = pd.DataFrame(data_swap_11s22s55s)


data_swap_22s25s = {}
if True:
    for i in np.arange(0, len(swap_ticker_2y2y)):
         data_swap_22s25s[swap_ticker_2y2y[i][:3] + '22s25s'] =  data_swap[swap_ticker_2y5y[i]] - data_swap[swap_ticker_2y2y[i]]
    data_swap_22s25s = pd.DataFrame(data_swap_22s25s)


data_swap_11s55s = {}
if True:
    for i in np.arange(0, len(swap_ticker_1y1y)):
         data_swap_11s55s[swap_ticker_1y1y[i][:3] + '11s55s'] =  data_swap[swap_ticker_5y5y[i]] - data_swap[swap_ticker_1y1y[i]]
    data_swap_11s55s = pd.DataFrame(data_swap_11s55s)



data_swap_2s22s55s = {}
if True:
    for i in np.arange(0, len(swap_ticker_5y5y)):
         data_swap_2s22s55s[swap_ticker_5y5y[i][:3] + 'P2s22s55s'] =  2 * data_swap[swap_ticker_5y5y[i][:3] + '_2Y_2Y'] - data_swap[swap_ticker_5y5y[i][:3] + '_0Y_2Y'] - data_swap[swap_ticker_5y5y[i]]
    data_swap_2s22s55s = pd.DataFrame(data_swap_2s22s55s)



data_swap_22s210s = {}
if True:
    for i in np.arange(0, len(swap_ticker_2y2y)):
         data_swap_22s210s[swap_ticker_2y2y[i][:3] + '22s210s'] =  data_swap[swap_ticker_2y10y[i]] - data_swap[swap_ticker_2y2y[i]]
    data_swap_22s210s = pd.DataFrame(data_swap_22s210s)


data_swap_25s210s = {}
if True:
    for i in np.arange(0, len(swap_ticker_2y5y)):
         data_swap_25s210s[swap_ticker_2y5y[i][:3] + '25s210s'] =  data_swap[swap_ticker_2y10y[i]] - data_swap[swap_ticker_2y5y[i]]
    data_swap_25s210s = pd.DataFrame(data_swap_25s210s)



data_swap_12s15s = {}
if True:
    for i in np.arange(0, len(swap_ticker_1y2y)):
         data_swap_12s15s[swap_ticker_1y2y[i][:3] + '12s15s'] =  data_swap[swap_ticker_1y5y[i]] - data_swap[swap_ticker_1y2y[i]]
    data_swap_12s15s = pd.DataFrame(data_swap_12s15s)



data_swap_12s110s = {}
if True:
    for i in np.arange(0, len(swap_ticker_1y2y)):
         data_swap_12s110s[swap_ticker_1y2y[i][:3] + '12s110s'] =  data_swap[swap_ticker_1y10y[i]] - data_swap[swap_ticker_1y2y[i]]
    data_swap_12s110s = pd.DataFrame(data_swap_12s110s)


data_swap_15s110s = {}
if True:
    for i in np.arange(0, len(swap_ticker_1y5y)):
         data_swap_15s110s[swap_ticker_1y5y[i][:3] + '15s110s'] =  data_swap[swap_ticker_1y10y[i]] - data_swap[swap_ticker_1y5y[i]]
    data_swap_15s110s = pd.DataFrame(data_swap_15s110s)
    
    

data_swap_12s15s110s = {}
if True:
    for i in np.arange(0, len(swap_ticker_1y2y)):
         data_swap_12s15s110s[swap_ticker_1y2y[i][:3] + 'P12s15s110s'] =  2 * data_swap[swap_ticker_1y5y[i]] - data_swap[swap_ticker_1y2y[i]] - data_swap[swap_ticker_1y10y[i]]
    data_swap_12s15s110s = pd.DataFrame(data_swap_12s15s110s)

data_swap_22s25s210s = {}
if True:
    for i in np.arange(0, len(swap_ticker_2y2y)):
         data_swap_22s25s210s[swap_ticker_2y2y[i][:3] + 'P22s25s210s'] =  2 * data_swap[swap_ticker_2y5y[i]] - data_swap[swap_ticker_2y2y[i]] - data_swap[swap_ticker_2y10y[i]]
    data_swap_22s25s210s = pd.DataFrame(data_swap_22s25s210s)





data_swap_combined = pd.concat([data_swap, data_swap_3ms2s, data_swap_2s10s, data_swap_5s10s, data_swap_2s5s, data_swap_10s30s, data_swap_22s55s,
                                data_swap_5s30s, data_swap_2s30s, data_swap_2s5s10s, data_swap_5s10s30s, data_swap_2s10s30s, data_swap_12s110s,
                                data_swap_2s5y5s, data_swap_2s55s30s, data_swap_55s105s, data_swap_55s30s,data_swap_11s22s55s, data_swap_22s25s, 
                                data_swap_2s22s55s, data_swap_22s210s, data_swap_25s210s, data_swap_12s15s, data_swap_15s110s,
                                data_swap_12s15s110s, data_swap_22s25s210s, data_swap_11s55s, data_swap_55s1010s, data_swap_11s55s1010s], 
                                axis=1, join='outer',sort=False)




data_swap_combined_tickers = [swap_ticker_3m, swap_ticker_1y, swap_ticker_2y, swap_ticker_3y, swap_ticker_5y, swap_ticker_10y, 
                              swap_ticker_1y1y, swap_ticker_1y2y, swap_ticker_1y5y, swap_ticker_1y10y, swap_ticker_2y1y, 
                              swap_ticker_2y2y, swap_ticker_2y5y, swap_ticker_2y10y, swap_ticker_5y5y, swap_ticker_10y5y, swap_ticker_10y10y,
                              swap_ticker_30y, data_swap_3ms2s.columns, data_swap_2s10s.columns, data_swap_5s10s.columns, data_swap_2s5s.columns,
                              data_swap_22s55s.columns, data_swap_10s30s.columns, data_swap_5s30s.columns, data_swap_2s30s.columns, data_swap_2s5s10s.columns, 
                              data_swap_5s10s30s.columns, data_swap_2s10s30s.columns, data_swap_12s110s.columns, data_swap_2s5y5s.columns,data_swap_2s55s30s.columns, 
                              data_swap_55s105s.columns, data_swap_55s30s.columns,data_swap_11s22s55s.columns, data_swap_22s25s.columns, data_swap_2s22s55s.columns, 
                              data_swap_22s210s.columns, data_swap_25s210s.columns,data_swap_12s15s.columns, data_swap_15s110s.columns, 
                              data_swap_12s15s110s.columns, data_swap_22s25s210s.columns, data_swap_11s55s.columns,  data_swap_55s1010s.columns, data_swap_11s55s1010s.columns]







##########################################################
# PP - create gov curve fly data and ticker
##########################################################



rate_gov_ticker_1y = [c for c in data_gov.columns if '_0Y_1Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_2y = [c for c in data_gov.columns if '_0Y_2Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_3y = [c for c in data_gov.columns if '_0Y_3Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_4y = [c for c in data_gov.columns if '_0Y_4Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_5y = [c for c in data_gov.columns if '_0Y_5Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_7y = [c for c in data_gov.columns if '_0Y_7Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_8y = [c for c in data_gov.columns if '_0Y_8Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_10y = [c for c in data_gov.columns if '_0Y_10Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_15y = [c for c in data_gov.columns if '_0Y_15Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_20y = [c for c in data_gov.columns if '_0Y_20Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c]
rate_gov_ticker_30y = [c for c in data_gov.columns if '_0Y_30Y' in c and  'NOK' not in c and  'ZAR' not in c and  'BRL' not in c and  'GRC' not in c  and  'SEK' not in c] 






data_gov_2s10s = {}
if True:
    for i in np.arange(0, len(rate_gov_ticker_2y)):
         data_gov_2s10s[rate_gov_ticker_10y[i][:3] + '2s10s'] =  data_gov[rate_gov_ticker_10y[i]] - data_gov[rate_gov_ticker_2y[i]]
    data_gov_2s10s = pd.DataFrame(data_gov_2s10s)


data_gov_2s5s = {}
if True:
    for i in np.arange(0, len(rate_gov_ticker_2y)):
         data_gov_2s5s[rate_gov_ticker_5y[i][:3] + '2s5s'] =  data_gov[rate_gov_ticker_5y[i]] - data_gov[rate_gov_ticker_2y[i]]
    data_gov_2s5s = pd.DataFrame(data_gov_2s5s)


data_gov_5s10s = {}
if True:
    for i in np.arange(0, len(rate_gov_ticker_5y)):
         data_gov_5s10s[rate_gov_ticker_10y[i][:3] + '5s10s'] =  data_gov[rate_gov_ticker_10y[i]] - data_gov[rate_gov_ticker_5y[i]]
    data_gov_5s10s = pd.DataFrame(data_gov_5s10s)


data_gov_2s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in rate_gov_ticker_30y]
if True:
    for i in np.arange(0, len(rate_gov_ticker_30y)):
         data_gov_2s30s[rate_gov_ticker_30y[i][:3] + '2s30s'] =  data_gov[rate_gov_ticker_30y[i]] - data_gov[rate_ticker_temp1[i]]
    data_gov_2s30s = pd.DataFrame(data_gov_2s30s)



data_gov_5s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in rate_gov_ticker_30y]
if True:
    for i in np.arange(0, len(rate_gov_ticker_30y)):
         data_gov_5s30s[rate_gov_ticker_30y[i][:3] + '5s30s'] =  data_gov[rate_gov_ticker_30y[i]] - data_gov[rate_ticker_temp1[i]]
    data_gov_5s30s = pd.DataFrame(data_gov_5s30s)

data_gov_10s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_gov_ticker_30y]
if True:
    for i in np.arange(0, len(rate_gov_ticker_30y)):
         data_gov_10s30s[rate_gov_ticker_30y[i][:3] + '10s30s'] =  data_gov[rate_gov_ticker_30y[i]] - data_gov[rate_ticker_temp1[i]]
    data_gov_10s30s = pd.DataFrame(data_gov_10s30s)



data_gov_5s10s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in rate_gov_ticker_30y]
rate_ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_gov_ticker_30y]
if True:
    for i in np.arange(0, len(rate_gov_ticker_30y)):
         data_gov_5s10s30s[rate_gov_ticker_30y[i][:3] + 'P5s10s30s'] =  2 * data_gov[rate_ticker_temp2[i]] - data_gov[rate_ticker_temp1[i]] - data_gov[rate_gov_ticker_30y[i]]
    data_gov_5s10s30s = pd.DataFrame(data_gov_5s10s30s)


data_gov_2s10s30s = {}
rate_ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in rate_gov_ticker_30y]
rate_ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in rate_gov_ticker_30y]
if True:
    for i in np.arange(0, len(rate_gov_ticker_30y)):
         data_gov_2s10s30s[rate_gov_ticker_30y[i][:3] + 'P2s10s30s'] =  2 * data_gov[rate_ticker_temp2[i]] - data_gov[rate_ticker_temp1[i]] - data_gov[rate_gov_ticker_30y[i]]
    data_gov_2s10s30s = pd.DataFrame(data_gov_2s10s30s)



data_gov_2s5s10s = {}
rate_ticker_temp1 = [c.replace('_0Y_10Y', '_0Y_2Y') for c in rate_gov_ticker_10y]
rate_ticker_temp2 = [c.replace('_0Y_10Y', '_0Y_5Y') for c in rate_gov_ticker_10y]
if True:
    for i in np.arange(0, len(rate_gov_ticker_10y)):
         data_gov_2s5s10s[rate_gov_ticker_10y[i][:3] + 'P2s5s10s'] =  2 * data_gov[rate_ticker_temp2[i]] - data_gov[rate_ticker_temp1[i]] - data_gov[rate_gov_ticker_10y[i]]
    data_gov_2s5s10s = pd.DataFrame(data_gov_2s5s10s)




data_gov_combined = pd.concat([data_gov, data_gov_2s10s, data_gov_2s5s, data_gov_5s10s, data_gov_2s30s, data_gov_5s30s, data_gov_10s30s, data_gov_5s10s30s,
                           data_gov_2s10s30s, data_gov_2s5s10s], 
                          axis=1, join='outer',sort=False)



data_gov_combined_tickers = [
    rate_gov_ticker_1y, rate_gov_ticker_2y, rate_gov_ticker_3y, rate_gov_ticker_4y, rate_gov_ticker_5y, rate_gov_ticker_7y, 
    rate_gov_ticker_8y, rate_gov_ticker_10y, rate_gov_ticker_15y, rate_gov_ticker_20y, rate_gov_ticker_30y,
    data_gov_2s10s.columns, data_gov_2s5s.columns, data_gov_5s10s.columns, data_gov_2s30s.columns, data_gov_5s30s.columns, 
    data_gov_10s30s.columns, data_gov_5s10s30s.columns, data_gov_2s10s30s.columns, data_gov_2s5s10s.columns]
    





##########################################################
# PP - create swap spread curve fly data and ticker
##########################################################



#needs to sort the tenor as int
data_ss_tickers = [ [c for c in data_ss.columns if '_{0}Y_'.format(unique_tenor) in c] 
                            for unique_tenor 
                            in sorted(list(set([ int(c.split('_')[1].replace('Y','')) for c in data_ss.columns])))]







##########################################################
# PP - create IFS swap curve fly data and ticker
##########################################################



rate_ifs_ticker_1y = [c for c in data_ifs.columns if '_1Y' in c]
rate_ifs_ticker_2y = [c for c in data_ifs.columns if '_2Y' in c]
rate_ifs_ticker_5y = [c for c in data_ifs.columns if '_5Y' in c]
rate_ifs_ticker_10y = [c for c in data_ifs.columns if '_10Y' in c]
rate_ifs_ticker_30y = [c for c in data_ifs.columns if '_30Y' in c]




data_ifs_2s10s = {}
if True:
    for i in np.arange(0, len(rate_ifs_ticker_2y)):
         data_ifs_2s10s[rate_ifs_ticker_10y[i][:3] + '2s10s'] =  data_ifs[rate_ifs_ticker_10y[i]] - data_ifs[rate_ifs_ticker_2y[i]]
    data_ifs_2s10s = pd.DataFrame(data_ifs_2s10s)
    
  

data_ifs_2s5s = {}
if True:
    for i in np.arange(0, len(rate_ifs_ticker_2y)):
         data_ifs_2s5s[rate_ifs_ticker_5y[i][:3] + '2s5s'] =  data_ifs[rate_ifs_ticker_5y[i]] - data_ifs[rate_ifs_ticker_2y[i]]
    data_ifs_2s5s = pd.DataFrame(data_ifs_2s5s)
  

data_ifs_5s10s = {}
if True:
    for i in np.arange(0, len(rate_ifs_ticker_5y)):
         data_ifs_5s10s[rate_ifs_ticker_10y[i][:3] + '5s10s'] =  data_ifs[rate_ifs_ticker_10y[i]] - data_ifs[rate_ifs_ticker_5y[i]]
    data_ifs_5s10s = pd.DataFrame(data_ifs_5s10s)


data_ifs_5s30s = {}
if True:
    for i in np.arange(0, len(rate_ifs_ticker_5y)):
         data_ifs_5s30s[rate_ifs_ticker_30y[i][:3] + '5s30s'] =  data_ifs[rate_ifs_ticker_30y[i]] - data_ifs[rate_ifs_ticker_5y[i]]
    data_ifs_5s30s = pd.DataFrame(data_ifs_5s30s)


data_ifs_10s30s = {}
if True:
    for i in np.arange(0, len(rate_ifs_ticker_10y)):
         data_ifs_10s30s[rate_ifs_ticker_30y[i][:3] + '10s30s'] =  data_ifs[rate_ifs_ticker_30y[i]] - data_ifs[rate_ifs_ticker_10y[i]]
    data_ifs_10s30s = pd.DataFrame(data_ifs_10s30s)



data_ifs_2s30s = {}
if True:
    for i in np.arange(0, len(rate_ifs_ticker_2y)):
         data_ifs_2s30s[rate_ifs_ticker_30y[i][:3] + '2s30s'] =  data_ifs[rate_ifs_ticker_30y[i]] - data_ifs[rate_ifs_ticker_2y[i]]
    data_ifs_2s30s = pd.DataFrame(data_ifs_2s30s)



data_ifs_2s5s10s = {}
rate_ticker_temp1 = [c.replace('_10Y', '_2Y') for c in rate_ifs_ticker_10y]
rate_ticker_temp2 = [c.replace('_10Y', '_5Y') for c in rate_ifs_ticker_10y]
if True:
    for i in np.arange(0, len(rate_ifs_ticker_10y)):
         data_ifs_2s5s10s[rate_ifs_ticker_10y[i][:3] + 'P2s5s10s'] =  2 * data_ifs[rate_ticker_temp2[i]] - data_ifs[rate_ticker_temp1[i]] - data_ifs[rate_ifs_ticker_10y[i]]
    data_ifs_2s5s10s = pd.DataFrame(data_ifs_2s5s10s)


data_ifs_5s10s30s = {}
rate_ticker_temp1 = [c.replace('_30Y', '_5Y') for c in rate_ifs_ticker_30y]
rate_ticker_temp2 = [c.replace('_30Y', '_10Y') for c in rate_ifs_ticker_30y]
if True:
    for i in np.arange(0, len(rate_ifs_ticker_30y)):
         data_ifs_5s10s30s[rate_ifs_ticker_30y[i][:3] + 'P5s10s30s'] =  2 * data_ifs[rate_ticker_temp2[i]] - data_ifs[rate_ticker_temp1[i]] - data_ifs[rate_ifs_ticker_30y[i]]
    data_ifs_5s10s30s = pd.DataFrame(data_ifs_5s10s30s)



data_ifs_combined = pd.concat([data_ifs, data_ifs_2s5s, data_ifs_2s10s, data_ifs_5s10s, data_ifs_5s30s, data_ifs_10s30s, data_ifs_2s30s, data_ifs_2s5s10s, data_ifs_5s10s30s], 
                          axis=1, join='outer',sort=False)



data_ifs_combined_tickers = [ [c for c in data_ifs.columns if '_{0}Y_'.format(unique_tenor) in c] 
                            for unique_tenor 
                            in sorted(list(set([ int(c.split('_')[1].replace('Y','')) for c in data_ifs.columns])))]



data_ifs_combined_tickers = data_ifs_combined_tickers + [data_ifs_2s5s.columns, data_ifs_2s10s.columns, data_ifs_5s10s.columns, 
                              data_ifs_5s30s.columns, data_ifs_10s30s.columns, data_ifs_2s30s.columns, data_ifs_2s5s10s.columns, 
                              data_ifs_5s10s30s.columns]









##########################################################
# PP - convert iovl to yield value
##########################################################


#data_ivol_in_yields_daily = pd.DataFrame({c: convert_atmf_vol_to_yield(c.split('_')[1], data_vol[c]) for c in data_vol.columns})
data_ivol_in_yields = pd.DataFrame({c: convert_atmf_vol_to_yield(c.split('_')[1], data_ivol[c]) for c in data_ivol.columns})


# calculate rvol (underline is daily return)

# data_rvol_swap_1m_daily = (data_swap - data_swap.shift(1)).rolling(20).std() * 100
# data_rvol_swap_3m_daily = (data_swap - data_swap.shift(1)).rolling(60).std() * 100

# data_rvol_swap2_1m_daily = (data_swap2 - data_swap2.shift(1)).rolling(20).std() * 100
# data_rvol_swap2_3m_daily = (data_swap2 - data_swap2.shift(1)).rolling(60).std() * 100


##########################################################
# PP - calc rvol
##########################################################



data_rvol_swap_3m = (data_swap - data_swap.shift(60)).rolling(800).std() * 100 * np.sqrt(4)
data_rvol_swap_6m = (data_swap - data_swap.shift(122)).rolling(800).std() * 100 * np.sqrt(2)
data_rvol_swap_1y = (data_swap - data_swap.shift(255)).rolling(800).std() * 100 * np.sqrt(1)




#sanity check




##########################################################
# PP - convert spot ifs to fwds.
##########################################################


ifs_c = list(set([c[:3] for c in data_ifs.columns]))
ifs_cc = {cnty:[c for c in data_ifs.columns if cnty in c] for cnty in ifs_c}

data_fwd_ifs_dict = {c: bootstrap_ifs_fwd(data_ifs[cc]) for (c,cc) in ifs_cc.items()}

# =============================================================================
# to be double checked 2022-07-03 
# ##################
# # needs to be changed
# ##################
# 
# US_cols =  [c for c in data.columns if 'US' in c]
# US_cols.sort()
# data_usd = data[US_cols].dropna()
# 
# 
# 
# data_usd['USD_NM_02Y'] = data_usd['USD_BE_02Y'] + data_usd['USD_RR_02Y'] 
# data_usd['USD_NM_05Y'] = data_usd['USD_BE_05Y'] + data_usd['USD_RR_05Y']
# data_usd['USD_NM_10Y'] = data_usd['USD_BE_10Y'] + data_usd['USD_RR_10Y']
# data_usd['USD_NM_30Y'] = data_usd['USD_BE_30Y'] + data_usd['USD_RR_30Y']
# 
# 
# 
# data_usd['USD_NM_210'] = data_usd['USD_NM_10Y'] - data_usd['USD_NM_02Y']
# data_usd['USD_NM_510'] = data_usd['USD_NM_10Y'] - data_usd['USD_NM_05Y']
# data_usd['USD_NM_530'] = data_usd['USD_NM_30Y'] - data_usd['USD_NM_05Y']
# data_usd['USD_NM_1030'] = data_usd['USD_NM_30Y'] - data_usd['USD_NM_10Y']
# data_usd['USD_NM_020510'] = data_usd['USD_NM_10Y'] - 2 * data_usd['USD_NM_05Y'] + data_usd['USD_NM_02Y']
# data_usd['USD_NM_051030'] = data_usd['USD_NM_30Y'] - 2 * data_usd['USD_NM_10Y'] + data_usd['USD_NM_05Y']
# 
# 
# data_usd['USD_RR_210'] = data_usd['USD_RR_10Y'] - data_usd['USD_RR_02Y']
# data_usd['USD_RR_510'] = data_usd['USD_RR_10Y'] - data_usd['USD_RR_05Y']
# data_usd['USD_RR_530'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_05Y']
# data_usd['USD_RR_230'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_02Y']
# data_usd['USD_RR_1030'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_10Y']
# data_usd['USD_RR_020510'] = data_usd['USD_RR_10Y'] - 2 * data_usd['USD_RR_05Y'] + data_usd['USD_RR_02Y']
# data_usd['USD_RR_051030'] = data_usd['USD_RR_30Y'] - 2 * data_usd['USD_RR_10Y'] + data_usd['USD_RR_05Y']
# 
# data_usd['USD_BE_210'] = data_usd['USD_BE_10Y'] - data_usd['USD_BE_02Y']
# data_usd['USD_BE_510'] = data_usd['USD_BE_10Y'] - data_usd['USD_BE_05Y']
# data_usd['USD_BE_530'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_05Y']
# data_usd['USD_BE_230'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_02Y']
# data_usd['USD_BE_1030'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_10Y']
# data_usd['USD_BE_020510'] = data_usd['USD_BE_10Y'] - 2 * data_usd['USD_BE_05Y'] + data_usd['USD_BE_02Y']
# data_usd['USD_BE_051030'] = data_usd['USD_BE_30Y'] - 2 * data_usd['USD_BE_10Y'] + data_usd['USD_BE_05Y']
# 
# data_usd['USD_BE2NM30'] = data_usd['USD_NM_30Y'] - data_usd['USD_BE_02Y']
# 
# data_usd_rtn = data_usd - data_usd.shift(1)
# 
# =============================================================================





##########################################################
#
#  
# Analysis    
#
#
##########################################################




time_periods = [24,47,68]
time_periods = [26,47,69] # for 2023 dec 



##########################################################
# study 1 output return for swaps
##########################################################


if (0 > 1):
        
    #(rts, spots, dist) = generate_returns(data_swap_combined, [1,2,3,5,10,23,60,120,240])
    
    (rts, spots, dist) = generate_returns(data_swap_combined, time_periods)
    

    rfmt_rts = refmt_returns_by_p(rts, data_swap_combined_tickers, -1)
    
    rfmt_spots = refmt_returns_by_p(spots, data_swap_combined_tickers, -1)
    rfmt_dist = refmt_returns_by_p(dist, data_swap_combined_tickers, -1)
    
    
    
    ##########################
    # output to a new workbook
    ##########################
    wb1 = xw.Book(r'C:\Dev\mancave\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
    sht1 = wb1.sheets('Swap')
    sht1.range((1,1),(10000,300)).clear_contents()
    
    row = 1
    col = 2
    #output returns
    for rts_key, rts in rfmt_rts.items():
        sht1.range((row, col)).value = (rts * 100).round(1)
        sht1.range((row, col)).value = "{0} days rts".format(rts_key)
        row = row + 2 + len(rts.index)
    
    #output spots
    row = 1
    col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2
    
    #sht1.range((row,col)).value = rfmt_spots[list(rfmt_spots.keys())[0]]
    
    for rts_key, rts in rfmt_rts.items():
        sht1.range((row,col)).value = rfmt_spots[list(rfmt_spots.keys())[0]]
        sht1.range((row, col)).value = "current spot"
        row = row + 2 + len(rts.index)
    
    
    
    row = 1
    col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2 + len(rfmt_spots[list(rfmt_spots.keys())[0]].columns) + 2 
    
    for dist_key, dist in rfmt_dist.items():
        if dist_key != 0:
            sht1.range((row, col)).value = dist.round(2)
            sht1.range((row, col)).value = "{0} days spot zsc".format(dist_key)
            row = row + 2 + len(dist.index)




##########################################################
# study 2 output return for govs
##########################################################


if(0>1):


    #(rts, spots, dist) = generate_returns(data_gov_combined, [1,2,3,5,10,23,60,120,240])
    
    (rts, spots, dist) = generate_returns(data_gov_combined, time_periods)
    
    
    rfmt_rts = refmt_returns_by_p(rts, data_gov_combined_tickers, -1)
    rfmt_spots = refmt_returns_by_p(spots, data_gov_combined_tickers, -1)
    rfmt_dist = refmt_returns_by_p(dist, data_gov_combined_tickers, -1)
    
    ##########################
    # output to a new workbook
    ##########################
    wb1 = xw.Book(r'C:\Dev\mancave\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
    sht1 = wb1.sheets('Gov')
    sht1.range((1,1),(10000,300)).clear_contents()
    
    row = 1
    col = 2
    #output returns
    for rts_key, rts in rfmt_rts.items():
        sht1.range((row, col)).value = (rts * 100).round(1)
        sht1.range((row, col)).value = "{0} days rts".format(rts_key)
        row = row + 2 + len(rts.index)
    
    #output spots
    row = 1
    col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2
    
    for rts_key, rts in rfmt_rts.items():
        sht1.range((row,col)).value = rfmt_spots[list(rfmt_spots.keys())[0]]
        sht1.range((row, col)).value = "current spot"
        row = row + 2 + len(rts.index)
    
    
    row = 1
    col = 2 + len(rfmt_rts[list(rfmt_rts.keys())[0]].columns) + 2 + len(rfmt_spots[list(rfmt_spots.keys())[0]].columns) + 2 
    for dist_key, dist in rfmt_dist.items():
        if dist_key != 0:
            sht1.range((row, col)).value = (dist).round(2)
            sht1.range((row, col)).value = "{0} days spot".format(dist_key)
            row = row + 2 + len(dist.index)
    






##########################################################
# study 3 output return for swap spread
##########################################################


if(0>1):

    
    
  #  (rts_ss, spots_ss, dist_ss) = generate_returns(data_ss, [1,2,3,5,10,23,60,120,240])
    
    (rts_ss, spots_ss, dist_ss) = generate_returns(data_ss, time_periods)
  
    
    rfmt_rts_ss = refmt_returns_by_p(rts_ss, data_ss_tickers, -1)   
    rfmt_spots_ss = refmt_returns_by_p(spots_ss, data_ss_tickers, -1)
    rfmt_dist_ss = refmt_returns_by_p(dist_ss, data_ss_tickers, -1)
    
    
    zsc_ss = generate_zscore(data_ss, [10,20,60,120,240,500,1250,2500])
    rfmt_zsc_ss = refmt_returns_by_p(zsc_ss, data_ss_tickers, -1)
    
    
    

    wb1 = xw.Book(r'C:\Dev\mancave\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
    sht1 = wb1.sheets('SS')
    sht1.range((1,1),(10000,300)).clear_contents()
    
    row = 1
    col = 2
    #output returns
    for rts_key, rts in rfmt_rts_ss.items():
        sht1.range((row, col)).value = rts.round(1)
        sht1.range((row, col)).value = "{0} days rts".format(rts_key)
        row = row + 2 + len(rts.index)
   
    #output spots
    row = 1
    col = 2 + len(rfmt_rts_ss[list(rfmt_rts_ss.keys())[0]].columns) + 2
    
    #sht1.range((row,col)).value = rfmt_spots_ss[list(rfmt_spots_ss.keys())[0]].round(1)
    
    #return has 1 less item in the dict. in order to match return with spot use the ret dict item as the driver for loop
    for rts_key, rts in rfmt_rts_ss.items(): 
        sht1.range((row,col)).value = rfmt_spots_ss[list(rfmt_spots_ss.keys())[0]]
        sht1.range((row, col)).value = "current spot"
        row = row + 2 + len(rts.index)
    

    
    row = 1
    col = 2 + len(rfmt_rts_ss[list(rfmt_rts_ss.keys())[0]].columns) + 2 + len(rfmt_spots_ss[list(rfmt_spots_ss.keys())[0]].columns) + 2 
    
    
    for zsc_key, zsc in rfmt_dist_ss.items():
        if zsc_key != 0:
            sht1.range((row, col)).value = zsc.round(2)
            sht1.range((row, col)).value = "{0} days zscore".format(zsc_key)
            row = row + 2 + len(zsc.index)
    
    





##########################################################
# study 4 output return for ifs
##########################################################


if(0>1):

    
    #(rts_ifs, spots_ifs, dist_ifs) = generate_returns(data_ifs_combined, [1,2,3,5,10,23,60,120,240])
    
    (rts_ifs, spots_ifs, dist_ifs) = generate_returns(data_ifs_combined,time_periods)
    
    
    rfmt_rts_ifs = refmt_returns_by_p(rts_ifs, data_ifs_combined_tickers, -1)
    rfmt_spots_ifs = refmt_returns_by_p(spots_ifs, data_ifs_combined_tickers, -1)
    rfmt_dist_ifs = refmt_returns_by_p(dist_ifs, data_ifs_combined_tickers, -1)
    
    

    wb1 = xw.Book(r'C:\Dev\mancave\QuantModels\#applications\market_watch\global_rate_curve_valuation.xlsx')
    sht1 = wb1.sheets('ifs')
    sht1.range((1,1),(10000,300)).clear_contents()
    
    row = 1
    col = 2
    #output returns
    for rts_key, rts in rfmt_rts_ifs.items():
        sht1.range((row, col)).value = (rts*100).round(2)
        sht1.range((row, col)).value = "{0} days rts".format(rts_key)
        row = row + 2 + len(rts.index)
    #output spots
    row = 1
    col = 2 + len(rfmt_rts_ifs[list(rfmt_rts_ifs.keys())[0]].columns) + 2
    
    
    #return has 1 less item in the dict. in order to match return with spot use the ret dict item as the driver for loop
    for rts_key, rts in rfmt_rts_ifs.items():
        sht1.range((row,col)).value = rfmt_spots_ifs[list(rfmt_spots_ifs.keys())[0]]
        sht1.range((row, col)).value = "current spot"
        row = row + 2 + len(rts.index)
    
    
    
    row = 1
    col = 2 + len(rfmt_rts_ifs[list(rfmt_rts_ifs.keys())[0]].columns) + 2 + len(rfmt_spots_ifs[list(rfmt_spots_ifs.keys())[0]].columns) + 2 
    
    for dist_key, dist in rfmt_dist_ifs.items():
        if dist_key != 0:
            sht1.range((row, col)).value = (dist).round(2)
            sht1.range((row, col)).value = "{0} days spot".format(dist_key)
            row = row + 2 + len(dist.index)
    




    
##########################################################
# study 4.9 spot gov pca
##########################################################


if(0>1):
    
    data_4model = data_gov #not yet use the combined data which has curve
    
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
    
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = list(set(data_4model.columns.map(lambda x: x[3:])))
    tenors.remove('_0Y_25Y')
    

    #countries spot data
    #country_spot_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in spot_tenors],[])] for cn in countries}
    country_spot_data = {country: data_4model[[c for c in data_4model.columns if country in c]] for country in countries}  
    


    n=-73
     
    _s5_spot_gov = country_spot_data['USD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")    
    #s5_usd_spot_gov_pca = pca.PCA(s5_usd_spot_gov.iloc[-120:], rates_pca = True, momentum_size = None, sample_range = ('2013-01-01', '2015-11-01'))
    s5_usd_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)
    

    _s5_spot_gov = country_spot_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_eur_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_gov = country_spot_data['GBP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_gbp_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_gov = country_spot_data['FRA'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_fra_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_gov = country_spot_data['ITA'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_ita_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_gov = country_spot_data['ESP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_esp_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_gov = country_spot_data['AUT'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_aut_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_gov = country_spot_data['BEL'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_bel_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_gov = country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_aud_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)

    _s5_spot_gov = country_spot_data['CAD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_cad_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_gov = country_spot_data['JPY'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_jpy_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_gov = country_spot_data['NZD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_nzd_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1).drop(["NZD_1Y"],axis=1), rates_pca = True, momentum_size = None)



    # EUR core vs Peripheral
    pca.PCA(pd.concat([country_spot_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['FRA'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['ITA'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['ESP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['AUT'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['BEL'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")],                           
            axis=1).iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None).plot_loadings(n=4,w=40, h=15)


    # global
    pca.PCA(pd.concat([country_spot_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['USD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['NZD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['JPY'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['FRA'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
            country_spot_data['GBP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")],                           
            axis=1).iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None).plot_loadings(n=4,w=40, h=15)





    s5_global_spot_gov_pca = pca.PCA(pd.concat([s5_usd_spot_gov_pca.data, 
                                             s5_eur_spot_gov_pca.data,
                                             s5_gbp_spot_gov_pca.data,
                                             s5_fra_spot_gov_pca.data,
                                             s5_ita_spot_gov_pca.data,
                                             s5_esp_spot_gov_pca.data,
                                             s5_aud_spot_gov_pca.data,
                                             s5_cad_spot_gov_pca.data,
                                             s5_jpy_spot_gov_pca.data,
                                             s5_nzd_spot_gov_pca.data
                                             ], axis=1), rates_pca = True, momentum_size = None)
        



    s5_usd_spot_gov_pca.plot_loadings(n=4)
    s5_eur_spot_gov_pca.plot_loadings(n=4)
    s5_gbp_spot_gov_pca.plot_loadings(n=4)
    s5_fra_spot_gov_pca.plot_loadings(n=4)
    s5_ita_spot_gov_pca.plot_loadings(n=4)    
    s5_esp_spot_gov_pca.plot_loadings(n=4)    
    s5_aud_spot_gov_pca.plot_loadings(n=4)
    s5_cad_spot_gov_pca.plot_loadings(n=4)
    s5_jpy_spot_gov_pca.plot_loadings(n=4)
    s5_nzd_spot_gov_pca.plot_loadings(n=4)




    pd.DataFrame({'USD_PC1':s5_usd_spot_gov_pca.pcs['PC1'],
                  'EUR_PC1':s5_eur_spot_gov_pca.pcs['PC1'],
                  'GBP_PC1':s5_gbp_spot_gov_pca.pcs['PC1'],
                  'FRA_PC1':s5_fra_spot_gov_pca.pcs['PC1'],
                  'ITA_PC1':s5_ita_spot_gov_pca.pcs['PC1'],
                  'ESP_PC1':s5_esp_spot_gov_pca.pcs['PC1'],
                  'JPY_PC1':s5_jpy_spot_gov_pca.pcs['PC1'],
                  'NZD_PC1':s5_nzd_spot_gov_pca.pcs['PC1'],
                  'CAD_PC1':s5_cad_spot_gov_pca.pcs['PC1'],
                  'AUD_PC1':s5_aud_spot_gov_pca.pcs['PC1'],
                  }).iloc[n:].plot(grid=True, title = 'GOV PC1')
    
    pd.DataFrame({'USD_PC2':s5_usd_spot_gov_pca.pcs['PC2'],
                  'EUR_PC2':s5_eur_spot_gov_pca.pcs['PC2'],
                  'GBP_PC2':s5_gbp_spot_gov_pca.pcs['PC2'],
                  'FRA_PC2':s5_fra_spot_gov_pca.pcs['PC2'],
                  'ITA_PC2':s5_ita_spot_gov_pca.pcs['PC2'],
                  'ESP_PC2':s5_esp_spot_gov_pca.pcs['PC2'],
                  'JPY_PC2':s5_jpy_spot_gov_pca.pcs['PC2'],
                  'NZD_PC2':s5_nzd_spot_gov_pca.pcs['PC2'],
                  'CAD_PC2':s5_cad_spot_gov_pca.pcs['PC2'],
                  'AUD_PC2':s5_aud_spot_gov_pca.pcs['PC2'],
                  }).iloc[n:].plot(grid=True, title = 'GOV PC2')


    pca.PCA(pd.DataFrame({'USD_PC1':s5_usd_spot_gov_pca.pcs['PC1'],
                  'EUR_PC1':s5_eur_spot_gov_pca.pcs['PC1'],
                  'GBP_PC1':s5_gbp_spot_gov_pca.pcs['PC1'],
                  #'RFA_PC1':s5_fra_spot_gov_pca.pcs['PC1'],
                  #'ITA_PC1':s5_ita_spot_gov_pca.pcs['PC1'],
                  #'ESP_PC1':s5_esp_spot_gov_pca.pcs['PC1'],
                  'JPY_PC1':s5_jpy_spot_gov_pca.pcs['PC1'],
                  'NZD_PC1':s5_nzd_spot_gov_pca.pcs['PC1'],
                  'CAD_PC1':s5_cad_spot_gov_pca.pcs['PC1'],
                  'AUD_PC1':s5_aud_spot_gov_pca.pcs['PC1'],
                  }).iloc[n:], rates_pca = True, momentum_size = None).plot_loadings(n=4)

    
    pd.DataFrame({'USD_PC1':s5_usd_spot_gov_pca.pcs['PC1'],
                  'EUR_PC1':s5_eur_spot_gov_pca.pcs['PC1'],
                  'GBP_PC1':s5_gbp_spot_gov_pca.pcs['PC1'],
                  #'RFA_PC1':s5_fra_spot_gov_pca.pcs['PC1'],
                  #'ITA_PC1':s5_ita_spot_gov_pca.pcs['PC1'],
                  #'ESP_PC1':s5_esp_spot_gov_pca.pcs['PC1'],
                  'JPY_PC1':s5_jpy_spot_gov_pca.pcs['PC1'],
                  'NZD_PC1':s5_nzd_spot_gov_pca.pcs['PC1'],
                  'CAD_PC1':s5_cad_spot_gov_pca.pcs['PC1'],
                  'AUD_PC1':s5_aud_spot_gov_pca.pcs['PC1'],
                  }).iloc[-73:].cov().round(2)
    
    pd.DataFrame({'USD_PC1':s5_usd_spot_gov_pca.pcs['PC1'],
                  'EUR_PC1':s5_eur_spot_gov_pca.pcs['PC1'],
                  'GBP_PC1':s5_gbp_spot_gov_pca.pcs['PC1'],
                  #'RFA_PC1':s5_fra_spot_gov_pca.pcs['PC1'],
                  #'ITA_PC1':s5_ita_spot_gov_pca.pcs['PC1'],
                  #'ESP_PC1':s5_esp_spot_gov_pca.pcs['PC1'],
                  'JPY_PC1':s5_jpy_spot_gov_pca.pcs['PC1'],
                  'NZD_PC1':s5_nzd_spot_gov_pca.pcs['PC1'],
                  'CAD_PC1':s5_cad_spot_gov_pca.pcs['PC1'],
                  'AUD_PC1':s5_aud_spot_gov_pca.pcs['PC1'],
                  }).iloc[-25:].cov().round(2)
    
    
    
    pca.PCA(pd.DataFrame({'USD_PC2':s5_usd_spot_gov_pca.pcs['PC2'],
                  'EUR_PC2':s5_eur_spot_gov_pca.pcs['PC2'],
                  'GBP_PC2':s5_gbp_spot_gov_pca.pcs['PC2'],
                  'JPY_PC2':s5_jpy_spot_gov_pca.pcs['PC2'],
                  'NZD_PC2':s5_nzd_spot_gov_pca.pcs['PC2'],
                  'CAD_PC2':s5_cad_spot_gov_pca.pcs['PC2'],
                  'AUD_PC2':s5_aud_spot_gov_pca.pcs['PC2'],
                  }).iloc[n:], rates_pca = True, momentum_size = None).plot_loadings(n=4)


    pd.DataFrame({'USD_PC2':s5_usd_spot_gov_pca.pcs['PC2'],
              'EUR_PC2':s5_eur_spot_gov_pca.pcs['PC2'],
              'GBP_PC2':s5_gbp_spot_gov_pca.pcs['PC2'],
              'JPY_PC2':s5_jpy_spot_gov_pca.pcs['PC2'],
              'NZD_PC2':s5_nzd_spot_gov_pca.pcs['PC2'],
              'CAD_PC2':s5_cad_spot_gov_pca.pcs['PC2'],
              'AUD_PC2':s5_aud_spot_gov_pca.pcs['PC2'],
              }).iloc[-73:].cov().round(2)


    pd.DataFrame({'USD_PC2':s5_usd_spot_gov_pca.pcs['PC2'],
              'EUR_PC2':s5_eur_spot_gov_pca.pcs['PC2'],
              'GBP_PC2':s5_gbp_spot_gov_pca.pcs['PC2'],
              'JPY_PC2':s5_jpy_spot_gov_pca.pcs['PC2'],
              'NZD_PC2':s5_nzd_spot_gov_pca.pcs['PC2'],
              'CAD_PC2':s5_cad_spot_gov_pca.pcs['PC2'],
              'AUD_PC2':s5_aud_spot_gov_pca.pcs['PC2'],
              }).iloc[-25:].cov().round(2)


    mrt.plotCurvehist(country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'AUD gov')
    mrt.plotCurvehist(country_spot_data['USD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'USD gov')
    mrt.plotCurvehist(country_spot_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'EUR gov')
    mrt.plotCurvehist(country_spot_data['GBP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'GBP gov')
    mrt.plotCurvehist(country_spot_data['JPY'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'JPY gov')
    mrt.plotCurvehist(country_spot_data['CAD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'CAD gov')
    mrt.plotCurvehist(country_spot_data['NZD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'NZD gov')

    mrt.plotCurvehist(country_spot_data['FRA'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'FRA gov')
    mrt.plotCurvehist(country_spot_data['ITA'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'ITA gov')
    mrt.plotCurvehist(country_spot_data['ESP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'ESP gov')


    n=-60
    l_lim = [2, 6]
    r_lim = [-1, 0.5]

    for c in countries:
        
        mrt.plotCurvehist(country_spot_data[c].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = c +' gov', ax1_ylim=(l_lim if c not in ('JPY', 'CHF') else [-0.2, 5.8]),  ax2_ylim=r_lim )



  
##########################################################
# study 4.9.1 EUR spot gov core vs peripheral pca
##########################################################


if(0>1):
    
    data_4model = data_gov #not yet use the combined data which has curve
    
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
    
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = list(set(data_4model.columns.map(lambda x: x[3:])))
    tenors.remove('_0Y_25Y')
    

    #spot tenors
    spot_tenors = sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' in c) and ('M' in c)], key=lambda x: int(x.split('_')[2].replace('M','')))
    spot_tenors = spot_tenors + sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' in c) and ('M' not in c)], key=lambda x: int(x.split('_')[2].replace('Y','')))

    #fwd_tenors = sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' not in c) and ('M' not in c)], key=lambda x: int(x.split('_')[1].replace('Y','')) * 100 + int(x.split('_')[2].replace('Y','')))

    fwd_tenors = [c for c in set(data_swap_combined.columns.map(lambda x: x[3:])) if ('Y_1Y' in c) and ('15Y_1Y' not in c)]
    fwd_tenors.sort(key = lambda x : int(x.replace('Y_1Y','').replace('_','')))
    fwd_tenors = fwd_tenors + ['_10Y_5Y', '_15Y_5Y', '_15Y_10Y', '_15Y_30Y']



    #countries spot data
    country_spot_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in spot_tenors],[])] for cn in countries}

    #countries fwd data use data_swap_combined and filter out chf
    country_fwd_data = {cn:data_swap_combined[sum([[c for c in data_swap_combined.columns if (st in c) and (cn in c)] for st in fwd_tenors],[])] for cn in [c for c in countries if c != 'CHF' ]}

    

    #from statistics.factor import pca


    n=-3500
     
    _s5_spot_gov = country_spot_data['USD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")    
    #s5_usd_spot_gov_pca = pca.PCA(s5_usd_spot_gov.iloc[-120:], rates_pca = True, momentum_size = None, sample_range = ('2013-01-01', '2015-11-01'))
    s5_usd_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)
    


    _s5_spot_gov = country_spot_data['GBP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_gbp_spot_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_gov = pd.concat([country_spot_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
                              country_spot_data['FRA'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
                              country_spot_data['ITA'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
                              country_spot_data['ESP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
                              country_spot_data['AUT'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
                              country_spot_data['BEL'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")],                           
                              axis=1)
                            
    s5_LT_core_vs_peripheral_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)

    m = -300    
    s5_ST_core_vs_peripheral_gov_pca = pca.PCA(_s5_spot_gov.iloc[m:].dropna(axis=1), rates_pca = True, momentum_size = None)
    
    
    s5_Old_core_vs_peripheral_gov_pca = pca.PCA(_s5_spot_gov.iloc[n+2750:n+3250].dropna(axis=1), rates_pca = True, momentum_size = None)


    s5_LT_core_vs_peripheral_gov_pca.plot_loading_evolutions(secondPcaObj=s5_ST_core_vs_peripheral_gov_pca, 
                                                             #thirdPcaObj=s5_Old_core_vs_peripheral_gov_pca,
                                                             n=6, alignData=False)


    
    ''''''
    
    n=-3500 


    _s5_spot_gov = pd.concat([country_spot_data['USD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
                              country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
                              country_spot_data['GBP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
                              country_spot_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
                              country_spot_data['JPY'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),
                              country_spot_data['NZD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")],                           
                               axis=1)
                            
    s5_LT_core_vs_peripheral_gov_pca = pca.PCA(_s5_spot_gov.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)

    m = -200    
    s5_ST_core_vs_peripheral_gov_pca = pca.PCA(_s5_spot_gov.iloc[m:].dropna(axis=1), rates_pca = True, momentum_size = None)
    
    
    s5_Old_core_vs_peripheral_gov_pca = pca.PCA(_s5_spot_gov.iloc[n+2750:n+3250].dropna(axis=1), rates_pca = True, momentum_size = None)


    s5_LT_core_vs_peripheral_gov_pca.plot_loading_evolutions(secondPcaObj=s5_ST_core_vs_peripheral_gov_pca, 
                                                             #thirdPcaObj=s5_Old_core_vs_peripheral_gov_pca,
                                                             n=6, alignData=False)









    pd.DataFrame({'EUR_LT_PC1':s5_LT_core_vs_peripheral_gov_pca.pcs['PC1'],
                  'EUR_ST_PC1':s5_ST_core_vs_peripheral_gov_pca.pcs['PC1'],
                  'USD_PC1':s5_usd_spot_gov_pca.pcs['PC1'],
                  'GBP_PC1':s5_gbp_spot_gov_pca.pcs['PC1'],
                  }).iloc[n:].plot(grid=True, title = 'EUR core vs peripheral PC1')
    
    
    
    
    '''shcok the PCs'''
    
    
    ''' Bull steepening - A long term rate normalization to the pre covid level
        
        under this case ECB has to maintain its stance in fighting inflation while sacrificing growth
        
        EUR/FRA bull steepened, german has slight bigger beta
        ITA and ESP also steepened but Core/Peripheral will widen and the spread curve will steepen up. (80bps and 130bps) 
        ESP will out perform ITA at the short end. ESP pretty much rallied in the front end as the EUR but at the backend end the gap opened up by 80bps
    '''
    
    (selectedPC, newPCLevels, unlerlineNewLevelDecompedIntoSecLoadingMatrix, unlerlineNewLevelDecompedIntoSecLoadingMatrixChanges, impliedUnderlineLevelChangesFromNewPC ) = s5_LT_core_vs_peripheral_gov_pca.convertPCLevelsToUnderlines(selectedPCIdxs=['PC1', 'PC2', 'PC3'], newPCLevels=[-2, 0, -5])
    
    (selectedPC, newPCLevels, unlerlineNewLevelDecompedIntoSecLoadingMatrix, unlerlineNewLevelDecompedIntoSecLoadingMatrixChanges, impliedUnderlineLevelChangesFromNewPC ) = s5_ST_core_vs_peripheral_gov_pca.convertPCLevelsToUnderlines(selectedPCIdxs=['PC1', 'PC2'], newPCLevels=[2.7, 0])
    print(unlerlineNewLevelDecompedIntoSecLoadingMatrixChanges)
    print(impliedUnderlineLevelChangesFromNewPC)
    
    
    ''' Bear flattening - ECB keeps going
        
        under this case ECB has to maintain its stance in fighting inflation while sacrificing growth
        
        EUR front end 3 more hikes the 2s30s flattens for 20bps from here
        Core peripheral spread curves widen by 20bps roughly pararrell
        
        It depends on the momentum market priced in if it is near term then higher vol otherwise growth holds up the rise will be discounted. 
        
    '''
    
    (selectedPC, newPCLevels, unlerlineNewLevelDecompedIntoSecLoadingMatrix, unlerlineNewLevelDecompedIntoSecLoadingMatrixChanges, impliedUnderlineLevelChangesFromNewPC ) = s5_ST_core_vs_peripheral_gov_pca.convertPCLevelsToUnderlines(selectedPCIdxs=['PC1', 'PC2', 'PC5'], newPCLevels=[10, 0, -0.5])
    
    
    
    ''' Bull steepening - A long term rate normalization to the pre covid level
        
        Using USD, AUD and EUR as the risk 
        
    '''
    
    
    (selectedPC, newPCLevels, unlerlineNewLevelDecompedIntoSecLoadingMatrix, unlerlineNewLevelDecompedIntoSecLoadingMatrixChanges, impliedUnderlineLevelChangesFromNewPC ) = s5_ST_core_vs_peripheral_gov_pca.convertPCLevelsToUnderlines(selectedPCIdxs=['PC1'], newPCLevels=[-2])
    print(unlerlineNewLevelDecompedIntoSecLoadingMatrixChanges)
    print(impliedUnderlineLevelChangesFromNewPC)
   
    
    
    




##########################################################
# study 5 spot swap pca
##########################################################


if(0>1):
    
    data_4model = data_swap #not yet use the combined data which has curve
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
    
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = list(set(data_4model.columns.map(lambda x: x[3:])))
    tenors.remove('_0Y_25Y')
    

    #spot tenors
    spot_tenors = sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' in c) and ('M' in c)], key=lambda x: int(x.split('_')[2].replace('M','')))
    spot_tenors = spot_tenors + sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' in c) and ('M' not in c)], key=lambda x: int(x.split('_')[2].replace('Y','')))

    #fwd_tenors = sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' not in c) and ('M' not in c)], key=lambda x: int(x.split('_')[1].replace('Y','')) * 100 + int(x.split('_')[2].replace('Y','')))

    fwd_tenors = [c for c in set(data_swap_combined.columns.map(lambda x: x[3:])) if ('Y_1Y' in c) and ('15Y_1Y' not in c)]
    fwd_tenors.sort(key = lambda x : int(x.replace('Y_1Y','').replace('_','')))
    fwd_tenors = fwd_tenors + ['_10Y_5Y', '_15Y_5Y', '_15Y_10Y', '_15Y_30Y']



    #countries spot data
    country_spot_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in spot_tenors],[])] for cn in countries}

    #countries fwd data use data_swap_combined and filter out chf
    country_fwd_data = {cn:data_swap_combined[sum([[c for c in data_swap_combined.columns if (st in c) and (cn in c)] for st in fwd_tenors],[])] for cn in [c for c in countries if c != 'CHF' ]}

    



    n=-73
    
     
    
    #s5_usd_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[-9000:], rates_pca = True, momentum_size = None, sample_range = ('2007-01-01', '2019-12-01'))
    #s5_usd_spot_swap_pca.plot_loadings(n=5)
    
    
    _s5_spot_swap = country_spot_data['USD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")        
    s5_usd_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)
    

    _s5_spot_swap = country_spot_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_eur_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_swap = country_spot_data['GBP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_gbp_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_swap = country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_aud_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)

    _s5_spot_swap = country_spot_data['CAD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_cad_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_swap = country_spot_data['JPY'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_jpy_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)



    _s5_spot_swap = country_spot_data['NZD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    _s5_spot_swap = _s5_spot_swap[[c for c in _s5_spot_swap.columns if 'NZD_1Y' not in c]]
    s5_nzd_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)
    
    
    
    s5_global_spot_swap_pca = pca.PCA(pd.concat([s5_usd_spot_swap_pca.data, 
                                             s5_eur_spot_swap_pca.data,
                                             s5_gbp_spot_swap_pca.data,
                                             s5_aud_spot_swap_pca.data,
                                             s5_cad_spot_swap_pca.data,
                                             s5_jpy_spot_swap_pca.data,
                                             s5_nzd_spot_swap_pca.data
                                             ], axis=1), rates_pca = True, momentum_size = None)
        

    s5_global_spot_swap_pca.plot_loadings(n=4)


    s5_usd_spot_swap_pca.plot_loadings(n=4)
    s5_eur_spot_swap_pca.plot_loadings(n=4)
    s5_gbp_spot_swap_pca.plot_loadings(n=4)
    s5_aud_spot_swap_pca.plot_loadings(n=4)
    s5_cad_spot_swap_pca.plot_loadings(n=4)
    s5_jpy_spot_swap_pca.plot_loadings(n=4)
    s5_nzd_spot_swap_pca.plot_loadings(n=4)



    pd.DataFrame({'USD_PC1':s5_usd_spot_swap_pca.pcs['PC1'],
                  'EUR_PC1':s5_eur_spot_swap_pca.pcs['PC1'],
                  'GBP_PC1':s5_gbp_spot_swap_pca.pcs['PC1'],
                  'JPY_PC1':s5_jpy_spot_swap_pca.pcs['PC1'],
                  'NZD_PC1':s5_nzd_spot_swap_pca.pcs['PC1'],
                  'CAD_PC1':s5_cad_spot_swap_pca.pcs['PC1'],
                  'AUD_PC1':s5_aud_spot_swap_pca.pcs['PC1'],
                  }).plot(grid=True, title = 'Swap PC1')


    pd.DataFrame({'USD_PC2':s5_usd_spot_swap_pca.pcs['PC2'],
                  'EUR_PC2':s5_eur_spot_swap_pca.pcs['PC2'],
                  'GBP_PC2':s5_gbp_spot_swap_pca.pcs['PC2'],
                  'JPY_PC2':s5_jpy_spot_swap_pca.pcs['PC2'],
                  'NZD_PC2':s5_nzd_spot_swap_pca.pcs['PC2'],
                  'CAD_PC2':s5_cad_spot_swap_pca.pcs['PC2'],
                  'AUD_PC2':s5_aud_spot_swap_pca.pcs['PC2'],
                  }).plot(grid=True, title = 'Swap PC2')


    pca.PCA(pd.DataFrame({'USD_PC1':s5_usd_spot_swap_pca.pcs['PC1'],
                  'EUR_PC1':s5_eur_spot_swap_pca.pcs['PC1'],
                  'GBP_PC1':s5_gbp_spot_swap_pca.pcs['PC1'],
                  'JPY_PC1':s5_jpy_spot_swap_pca.pcs['PC1'],
                  'NZD_PC1':s5_nzd_spot_swap_pca.pcs['PC1'],
                  'CAD_PC1':s5_cad_spot_swap_pca.pcs['PC1'],
                  'AUD_PC1':s5_aud_spot_swap_pca.pcs['PC1'],
                  }).iloc[n:], rates_pca = True, momentum_size = None).plot_loadings(n=4)

    

    pca.PCA(pd.DataFrame({'USD_PC2':s5_usd_spot_swap_pca.pcs['PC2'],
                  'EUR_PC2':s5_eur_spot_swap_pca.pcs['PC2'],
                  'GBP_PC2':s5_gbp_spot_swap_pca.pcs['PC2'],
                  'JPY_PC2':s5_jpy_spot_swap_pca.pcs['PC2'],
                  'NZD_PC2':s5_nzd_spot_swap_pca.pcs['PC2'],
                  'CAD_PC2':s5_cad_spot_swap_pca.pcs['PC2'],
                  'AUD_PC2':s5_aud_spot_swap_pca.pcs['PC2'],
                  }).iloc[n:], rates_pca = True, momentum_size = None).plot_loadings(n=4)


    n=-250
  
    _s5_fwd_swap = country_fwd_data['USD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")    
    s5_usd_spot_swap_pca = pca.PCA(_s5_fwd_swap.iloc[n:], rates_pca = True, momentum_size = None)


    _s5_fwd_swap = country_fwd_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_eur_spot_swap_pca = pca.PCA(_s5_fwd_swap.iloc[n:], rates_pca = True, momentum_size = None)


    _s5_fwd_swap = country_fwd_data['GBP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_gbp_spot_swap_pca = pca.PCA(_s5_fwd_swap.iloc[n:], rates_pca = True, momentum_size = None)


    _s5_fwd_swap = country_fwd_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_aud_spot_swap_pca = pca.PCA(_s5_fwd_swap.iloc[n:], rates_pca = True, momentum_size = None)

    _s5_fwd_swap = country_fwd_data['CAD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_cad_spot_swap_pca = pca.PCA(_s5_fwd_swap.iloc[n:], rates_pca = True, momentum_size = None)


    _s5_fwd_swap = country_fwd_data['JPY'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_jpy_spot_swap_pca = pca.PCA(_s5_fwd_swap.iloc[n:], rates_pca = True, momentum_size = None)



    s5_usd_spot_swap_pca.plot_loadings(n=4)
    s5_eur_spot_swap_pca.plot_loadings(n=4)
    s5_gbp_spot_swap_pca.plot_loadings(n=4)
    s5_aud_spot_swap_pca.plot_loadings(n=4)
    s5_cad_spot_swap_pca.plot_loadings(n=4)
    s5_jpy_spot_swap_pca.plot_loadings(n=4)
    

    mrt.plotCurvehist(country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'AUD swap')
    mrt.plotCurvehist(country_spot_data['USD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'USD swap')
    mrt.plotCurvehist(country_spot_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'EUR swap')
    mrt.plotCurvehist(country_spot_data['GBP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'GBP swap')
    mrt.plotCurvehist(country_spot_data['JPY'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'JPY swap')
    mrt.plotCurvehist(country_spot_data['CAD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'CAD swap')
    mrt.plotCurvehist(country_spot_data['NZD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = 'NZD swap')






    n=-60
    l_lim = [1, 5]
    r_lim = [-1, 0.5]

    for c in ['AUD', 'USD', 'EUR', 'GBP', 'JPY', 'CAD', 'NZD']:
        
        mrt.plotCurvehist(country_spot_data[c].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = c +' swap', ax1_ylim=(l_lim if c not in ('JPY') else [-0.2, 5.8]),  ax2_ylim=r_lim )




##########################################################
# study 5.1 spot swap pca ALEX
##########################################################


if(0>1):
    
    data_4model = data_swap #not yet use the combined data which has curve
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
    
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = list(set(data_4model.columns.map(lambda x: x[3:])))
    tenors.remove('_0Y_25Y')
    

    #spot tenors
    spot_tenors = sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' in c) and ('M' in c)], key=lambda x: int(x.split('_')[2].replace('M','')))
    spot_tenors = spot_tenors + sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' in c) and ('M' not in c)], key=lambda x: int(x.split('_')[2].replace('Y','')))

    #fwd_tenors = sorted([c for c in set(data_4model.columns.map(lambda x: x[3:])) if ('_0Y_' not in c) and ('M' not in c)], key=lambda x: int(x.split('_')[1].replace('Y','')) * 100 + int(x.split('_')[2].replace('Y','')))

    fwd_tenors = [c for c in set(data_swap_combined.columns.map(lambda x: x[3:])) if ('Y_1Y' in c) and ('15Y_1Y' not in c)]
    fwd_tenors.sort(key = lambda x : int(x.replace('Y_1Y','').replace('_','')))
    fwd_tenors = fwd_tenors + ['_10Y_5Y', '_15Y_5Y', '_15Y_10Y', '_15Y_30Y']



    #countries spot data
    country_spot_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in spot_tenors],[])] for cn in countries}

    #countries fwd data use data_swap_combined and filter out chf
    country_fwd_data = {cn:data_swap_combined[sum([[c for c in data_swap_combined.columns if (st in c) and (cn in c)] for st in fwd_tenors],[])] for cn in [c for c in countries if c != 'CHF' ]}
    
    
    
    


    n=-4500
     
    _s5_spot_swap = country_spot_data['USD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")    
    #s5_usd_spot_swap_pca = pca.PCA(s5_usd_spot_swap.iloc[-120:], rates_pca = True, momentum_size = None, sample_range = ('2013-01-01', '2015-11-01'))
    s5_usd_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)
    

    _s5_spot_swap = country_spot_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_eur_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_swap = country_spot_data['GBP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_gbp_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=0), rates_pca = True, momentum_size = None)


    _s5_spot_swap = country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_aud_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)

    _s5_spot_swap = country_spot_data['CAD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_cad_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)


    _s5_spot_swap = country_spot_data['JPY'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_jpy_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=0), rates_pca = True, momentum_size = None)



    _s5_spot_swap = country_spot_data['NZD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s5_nzd_spot_swap_pca = pca.PCA(_s5_spot_swap.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)




    pd.concat({'USD': s5_usd_spot_swap_pca.pcs.PC4,
               'EUR':s5_eur_spot_swap_pca.pcs.PC4 ,
               'GBP': s5_gbp_spot_swap_pca.pcs.PC4,
               'AUD': s5_aud_spot_swap_pca.pcs.PC4,
               'CAD': s5_cad_spot_swap_pca.pcs.PC4,
               'JPY': s5_jpy_spot_swap_pca.pcs.PC4,
               'NZD': s5_nzd_spot_swap_pca.pcs.PC4},
               axis=1).to_csv('c://temp/pc4.csv')
    

    s5_usd_spot_swap_pca.pcs.to_csv('c://temp/usd_pcs.csv')    
    s5_eur_spot_swap_pca.pcs.to_csv('c://temp/eur_pcs.csv')
    s5_gbp_spot_swap_pca.pcs.to_csv('c://temp/gbp_pcs.csv')
    s5_aud_spot_swap_pca.pcs.to_csv('c://temp/aud_pcs.csv')
    s5_cad_spot_swap_pca.pcs.to_csv('c://temp/cad_pcs.csv')
    s5_jpy_spot_swap_pca.pcs.to_csv('c://temp/jpy_pcs.csv')
    s5_nzd_spot_swap_pca.pcs.to_csv('c://temp/nzd_pcs.csv')


    




##########################################################
# study 6 spot ifs swap
##########################################################


if(0>1):
    
    data_4model = data_ifs
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
    
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = sorted(list(set(data_4model.columns.map(lambda x: x[3:]))))
    
    tenors.sort(key = lambda x : int(x.replace('Y_SWIT','').replace('_','')))


    #countries spot data
    country_spot_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in tenors],[])] for cn in countries}

    
    n=-2300

 
    _s6_spot_ss = country_spot_data['USD'].rename(lambda c: c.replace("_SWIT",""), axis="columns")    
    s6_usd_spot_ss_pca = pca.PCA(_s6_spot_ss.iloc[n:], rates_pca = True, momentum_size = None)
    s6_usd_spot_ss_pca.plot_loadings(n=4)
    
 
    _s6_spot_ss = country_spot_data['AUD'].rename(lambda c: c.replace("_SWIT",""), axis="columns")    
    s6_aud_spot_ss_pca = pca.PCA(_s6_spot_ss.iloc[n:], rates_pca = True, momentum_size = None)
    s6_aud_spot_ss_pca.plot_loadings(n=4)
    


    _s6_spot_ss = country_spot_data['GBP'].rename(lambda c: c.replace("_SWIT",""), axis="columns")    
    s6_gbp_spot_ss_pca = pca.PCA(_s6_spot_ss.iloc[n:], rates_pca = True, momentum_size = None)
    s6_gbp_spot_ss_pca.plot_loadings(n=4)
 

    _s6_spot_ss = country_spot_data['EUR'].rename(lambda c: c.replace("_SWIT",""), axis="columns")    
    s6_eur_spot_ss_pca = pca.PCA(_s6_spot_ss.iloc[n:], rates_pca = True, momentum_size = None)
    s6_eur_spot_ss_pca.plot_loadings(n=4)
 
    
     
    pd.DataFrame({'USD':s6_usd_spot_ss_pca.pcs['PC1'],
                  'EUR':s6_eur_spot_ss_pca.pcs['PC1'],
                  'GBP':s6_gbp_spot_ss_pca.pcs['PC1'],
                  'AUD':s6_aud_spot_ss_pca.pcs['PC1'],
                  }).iloc[n:].plot(title = 'inf swap pc1')



    mrt.plotCurvehist(country_spot_data['AUD'].rename(lambda c: c.replace("_SWIT",""), axis="columns"),lookback=5, numberOfHistory=6, title = 'AUD ifs')
    mrt.plotCurvehist(country_spot_data['USD'].rename(lambda c: c.replace("_SWIT",""), axis="columns"),lookback=5, numberOfHistory=6, title = 'USD ifs')
    mrt.plotCurvehist(country_spot_data['EUR'].rename(lambda c: c.replace("_SWIT",""), axis="columns"),lookback=5, numberOfHistory=6, title = 'EUR ifs')
    mrt.plotCurvehist(country_spot_data['GBP'].rename(lambda c: c.replace("_SWIT",""), axis="columns"),lookback=5, numberOfHistory=6, title = 'GBP ifs')


    n =-146
    pd.DataFrame({'USD':s6_usd_spot_ss_pca.pcs['PC1'],
                  'EUR':s6_eur_spot_ss_pca.pcs['PC1'],
                  'GBP':s6_gbp_spot_ss_pca.pcs['PC1'],
                  'AUD':s6_aud_spot_ss_pca.pcs['PC1'],
                  }).iloc[n:].plot(title = 'inf swap pc1')









##########################################################
# study 6.1 swap spread in relationship to rates
##########################################################




if(0>1):
    
    fltCol = lambda df, strp: df[df.columns[df.columns.str.contains(strp)]] 
    fltT = lambda df, tr: df[(df.index >= tr[0]) & (df.index <= tr[1])] 
    
    def plot_ss_rate(df, w, h, tGridType='both', title=None):
        
        f = plt.figure(figsize = (w, h))
        gs = gridspec.GridSpec(4,1,height_ratios=[1,1,1,1])
        ax0 = plt.subplot(gs[0])
        ax1 = plt.subplot(gs[1])
        ax2 = plt.subplot(gs[2])
        ax3 = plt.subplot(gs[3])

        fltCol(df,'SS').plot(ax=ax0 , fontsize = 9)
        ax0.grid()
        
        fltCol(df,'GOV').plot(ax=ax1 , fontsize = 9)
        ax1.grid(True,which=tGridType)

        (fltCol(df,'SS')-fltCol(df,'SS').iloc[0]).plot(ax=ax2, fontsize = 9)
        ax2.grid(True,which=tGridType)
        
        (fltCol(df,'GOV')-fltCol(df,'GOV').iloc[1]).plot(ax=ax3, fontsize = 9)
        ax3.grid(True,which=tGridType)
        
        f.suptitle(title)
        
        f.tight_layout()
        return f
    
    
    
    ### Citi ###
    ### Citi ###
    ### Citi ###
    
    
    data_citiasw = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_CITIASW_historical.csv', skiprows=2, skip_blank_lines=True)  
    data_citiasw = clean_citi_data(data_citiasw, asw_col_name_cleaner).ffill()
    fn_citi_filter = lambda p, cols: [c for c in cols if p in c]
    

    
    ### BBG ###
    ### BBG ###
    ### BBG ###
    
    #SS
    data_4model = data_ss #not yet use the combined data which has curve
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
    
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = sorted(list(set(data_4model.columns.map(lambda x: x[3:]))))
    tenors.sort(key = lambda x : int(x.replace('Y_SS','').replace('_','')))
    tenors = ['_2Y_SS', '_3Y_SS', '_5Y_SS',  '_10Y_SS', '_20Y_SS', '_30Y_SS']

    #countries spot data
    country_ss_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in tenors],[])] for cn in countries}

    
    #rate x 100
    data_4model = data_gov * 100  #not yet use the combined data which has curve 
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
     
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = list(set(data_4model.columns.map(lambda x: x[3:])))
    tenors = ['_0Y_2Y', '_0Y_3Y', '_0Y_5Y', '0Y_10Y','0Y_20Y', '0Y_30Y']
     
    #countries spot data
    country_rate_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in tenors],[])] for cn in countries}
    
    
    #AUD bbg
    
    cn = 'AUS'
    _s600_spot_ss = country_ss_data[cn].rename(lambda c: c.replace("_SS","").replace(cn,"SS"), axis="columns")
    cn = 'AUD'
    _s600_spot_rate = country_rate_data[cn].rename(lambda c: c.replace("_0Y_","_").replace(cn,"GOV"), axis="columns")
    _s600_spot = pd.concat([_s600_spot_ss, _s600_spot_rate], axis=1)   
    _s600_bbg_spot_rate_ss_aud = _s600_spot.dropna(axis=0).dropna(axis=1)
    

  
    #USD SOFR bbg
    
    cn = 'USD'
    _s600_spot_ss = country_ss_data[cn].rename(lambda c: c.replace("_SS","").replace(cn,"SS"), axis="columns")
    cn = 'USD'
    _s600_spot_rate = country_rate_data[cn].rename(lambda c: c.replace("_0Y_","_").replace(cn,"GOV"), axis="columns")
    _s600_spot = pd.concat([_s600_spot_ss, _s600_spot_rate], axis=1)   
    #_s600_spot = _s600_spot.loc[:, ~_s600_spot.columns.str.contains('30Y')]
    _s600_bbg_spot_usdsofr_rate_ss = _s600_spot.dropna(axis=0).dropna(axis=1)
   
    
   #FRA bbg
    
    cn = 'FRA'
    _s600_spot_ss = country_ss_data[cn].rename(lambda c: c.replace("_SS","").replace(cn,"SS"), axis="columns")
    cn = 'FRA'
    _s600_spot_rate = country_rate_data[cn].rename(lambda c: c.replace("_0Y_","_").replace(cn,"GOV"), axis="columns")
    _s600_spot = pd.concat([_s600_spot_ss, _s600_spot_rate], axis=1)   
    #_s600_spot = _s600_spot.loc[:, ~_s600_spot.columns.str.contains('30Y')]
    _s600_bbg_spot_FRA_rate_ss = _s600_spot.dropna(axis=0).dropna(axis=1)
   
   
   #EUR bbg
    
    cn = 'EUR'
    _s600_spot_ss = country_ss_data[cn].rename(lambda c: c.replace("_SS","").replace(cn,"SS"), axis="columns")
    cn = 'EUR'
    _s600_spot_rate = country_rate_data[cn].rename(lambda c: c.replace("_0Y_","_").replace(cn,"GOV"), axis="columns")
    _s600_spot = pd.concat([_s600_spot_ss, _s600_spot_rate], axis=1)   
    #_s600_spot = _s600_spot.loc[:, ~_s600_spot.columns.str.contains('30Y')]
    _s600_bbg_spot_EUR_rate_ss = _s600_spot.dropna(axis=0).dropna(axis=1)
   
    
   #CAD bbg
    
    cn = 'CAD'
    _s600_spot_ss = country_ss_data[cn].rename(lambda c: c.replace("_SS","").replace(cn,"SS"), axis="columns")
    cn = 'CAD'
    _s600_spot_rate = country_rate_data[cn].rename(lambda c: c.replace("_0Y_","_").replace(cn,"GOV"), axis="columns")
    _s600_spot = pd.concat([_s600_spot_ss, _s600_spot_rate], axis=1)   
    #_s600_spot = _s600_spot.loc[:, ~_s600_spot.columns.str.contains('30Y')]
    _s600_bbg_spot_CAD_rate_ss = _s600_spot.dropna(axis=0).dropna(axis=1)
   
   
   
    #GBR Citi 
    
    cn = 'GBR'
    asw_cols_ptn = cn + '_ASW'
    yld_cols_ptn = cn + '_YIELD' 
    cn_cols = fn_citi_filter(cn, data_citiasw.columns)
    _s600_citi_spot_gbr = data_citiasw[cn_cols].rename(lambda c: c.replace(asw_cols_ptn,"SS").replace(yld_cols_ptn,"GOV"), axis="columns") \
                        .dropna(axis=0).dropna(axis=1)
    _s600_citi_spot_gbr[fn_citi_filter("GOV", _s600_citi_spot_gbr.columns)] = \
        _s600_citi_spot_gbr[fn_citi_filter("GOV", _s600_citi_spot_gbr.columns)] * 100
    _s600_citi_spot_gbr[fn_citi_filter("SS", _s600_citi_spot_gbr.columns)] = \
        _s600_citi_spot_gbr[fn_citi_filter("SS", _s600_citi_spot_gbr.columns)] * -1
        #-100 means upscale to bp and turn the ASW direction to swap spread

    _s600_citi_spot_gbr_rate_ss = _s600_citi_spot_gbr
   

    #JPN Citi 
    
    cn = 'JPN'
    asw_cols_ptn = cn + '_ASW'
    yld_cols_ptn = cn + '_YIELD' 
    cn_cols = fn_citi_filter(cn, data_citiasw.columns)
    _s600_citi_spot_jpn = data_citiasw[cn_cols].rename(lambda c: c.replace(asw_cols_ptn,"SS").replace(yld_cols_ptn,"GOV"), axis="columns") \
                        .dropna(axis=0).dropna(axis=1)
    _s600_citi_spot_jpn[fn_citi_filter("GOV", _s600_citi_spot_jpn.columns)] = \
        _s600_citi_spot_jpn[fn_citi_filter("GOV", _s600_citi_spot_jpn.columns)] * 100
    _s600_citi_spot_jpn[fn_citi_filter("SS", _s600_citi_spot_jpn.columns)] = \
        _s600_citi_spot_jpn[fn_citi_filter("SS", _s600_citi_spot_jpn.columns)] * -1
        #-100 means upscale to bp and turn the ASW direction to swap spread
 
    _s600_citi_spot_jpn_rate_ss = _s600_citi_spot_jpn
   
   
   

    ################# plotting ###################
    
    
    sr = ('2023-01-01', '2024-12-01')
    sr_mth = ('2023-12-01', '2024-12-01')
    tp = 'COV'
    
    #AUD BBG 
    
    pca.PCA(fltT(_s600_bbg_spot_rate_ss_aud, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 16)
    plot_ss_rate(fltT(_s600_bbg_spot_rate_ss_aud, sr_mth), 8, 16, tGridType='major', title='AUD BBG')    
    mrt.plotCurvehist(_s600_bbg_spot_rate_ss_aud,lookback=5, numberOfHistory=6, title = 'AUD BBG')


    #USD SOFR BBG 
    
    pca.PCA(fltT(_s600_bbg_spot_usdsofr_rate_ss, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 16)
    plot_ss_rate(fltT(_s600_bbg_spot_usdsofr_rate_ss, sr_mth), 8, 16, tGridType='major', title='USD SOFR BBG')    
    mrt.plotCurvehist(_s600_bbg_spot_usdsofr_rate_ss,lookback=5, numberOfHistory=6, title = 'USD SOFR BBG')
              

    #FRA BBG 
    
    pca.PCA(fltT(_s600_bbg_spot_FRA_rate_ss, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 16)
    plot_ss_rate(fltT(_s600_bbg_spot_FRA_rate_ss, sr_mth), 8, 16, tGridType='major', title='FRA BBG')    
    mrt.plotCurvehist(_s600_bbg_spot_FRA_rate_ss,lookback=5, numberOfHistory=6, title = 'FRA BBG')
              
    #EUR BBG 
    
    pca.PCA(fltT(_s600_bbg_spot_EUR_rate_ss, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 16)
    plot_ss_rate(fltT(_s600_bbg_spot_EUR_rate_ss, sr_mth), 8, 16, tGridType='major', title='EUR BBG')    
    mrt.plotCurvehist(_s600_bbg_spot_EUR_rate_ss,lookback=5, numberOfHistory=6, title = 'EUR BBG')
              

    #CAD BBG 
    
    pca.PCA(fltT(_s600_bbg_spot_CAD_rate_ss, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 16)
    plot_ss_rate(fltT(_s600_bbg_spot_CAD_rate_ss, sr_mth), 8, 16, tGridType='major', title='CAD BBG')    
    mrt.plotCurvehist(_s600_bbg_spot_CAD_rate_ss,lookback=5, numberOfHistory=6, title = 'CAD BBG')
              


    #GBP citi
    
    pca.PCA(fltT(_s600_citi_spot_gbr_rate_ss, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 16)
    plot_ss_rate(fltT(_s600_citi_spot_gbr_rate_ss, sr_mth), 8, 16, tGridType='major', title='GBP citi')    
    mrt.plotCurvehist(_s600_citi_spot_gbr_rate_ss,lookback=5, numberOfHistory=6, title = 'GBP citi')



    #JPY citi
    
    pca.PCA(fltT(_s600_citi_spot_jpn_rate_ss, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 16)
    plot_ss_rate(fltT(_s600_citi_spot_jpn_rate_ss, sr_mth), 8, 16, tGridType='major', title='JPY citi')    
    mrt.plotCurvehist(_s600_citi_spot_jpn_rate_ss,lookback=5, numberOfHistory=6, title = 'JPY citi')








##########################################################
# study 6 spot bob
##########################################################


if(0>1):
    
    data_4model = data_bob #not yet use the combined data which has curve
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
    
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = sorted(list(set(data_4model.columns.map(lambda x: x[3:]))))
    #tenors.sort(key = lambda x : int(x.replace('_0Y','').replace('_','')))

    tenors = [
     '_0Y_1M',
     '_0Y_3M',
     '_0Y_6M',
     '_0Y_1Y',
     '_0Y_2Y',
     '_0Y_3Y',
     '_0Y_5Y',
     '_0Y_7Y',
     '_0Y_10Y',
     '_0Y_15Y',
     '_0Y_20Y',
     '_0Y_25Y',
     '_0Y_30Y']

    #countries spot data
    country_spot_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in tenors],[])] for cn in countries}

    
    n=-1060
      

    _s6_spot_ss = country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")    
    #s6_usd_spot_ss_pca = pca.PCA(s6_usd_spot_ss.iloc[-120:], rates_pca = True, momentum_size = None, sample_range = ('2013-01-01', '2015-11-01'))
    s6_usd_spot_ss_pca = pca.PCA(_s6_spot_ss.iloc[n:], rates_pca = True, momentum_size = None)
    
    
    s6_usd_spot_ss_pca.plot_loadings(n=4)
    
        
    mrt.plotCurvehist(country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns").iloc[:-1],lookback=5, numberOfHistory=6, title = 'AUD bob spread')


##########################################################
# study 6 spot semi
##########################################################


if(0>1):
    
    
    country_spot_data = data_gov.loc[pd.isnull(data_gov.index) == False]
    country_spot_data = country_spot_data[[c for c in country_spot_data.columns if 'AUD_0Y' in c]]
    
    

    
    
    data_4model = data_semi #not yet use the combined data which has curve
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
    
    # c
    states = list(set(data_4model.columns.map(lambda x: x.split('_')[0])))       
    state_spot_data = {state: 
                       pd.concat([country_spot_data, data_4model[[c for c in data_4model.columns if (state in c and c.endswith('Y'))]]], axis=1)                      
                                 for state in states}  
    
    
    

    
    n=-60
    l_lim = [2, 6]
    r_lim = [-1, 0.5]
    
    mrt.plotCurvehist(state_spot_data['TCORP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title =  ' semi spread', ax1_ylim =l_lim, ax2_ylim =r_lim )

    for state in states:
        if state != 'TCORP': 
            mrt.plotCurvehist(state_spot_data[state].rename(lambda c: c.replace("_0Y_","_"), axis="columns"),lookback=5, numberOfHistory=6, title = state + ' semi spread', ax1_ylim =l_lim, ax2_ylim =r_lim )





##########################################################
# study 7 bei vs rr
##########################################################


if(0>1):
    
    data_beirr = data_beirr.loc[pd.isnull(data_beirr.index) == False]
    
    n = -350    
    
    
    pca.PCA(data_beirr[['AUD_BE_05Y','AUD_BE_10Y',
                        'AUD_BE_20Y','AUD_RR_05Y',
                        'AUD_RR_10Y','AUD_RR_20Y']].iloc[n:], 
            rates_pca = True, momentum_size = None).plot_loadings(n=4)


    pca.PCA(data_beirr[['USD_BE_02Y','USD_BE_05Y',
                        'USD_BE_10Y','USD_BE_30Y',
                        'USD_RR_02Y','USD_RR_05Y',
                        'USD_RR_10Y','USD_RR_30Y']].iloc[n:], 
            rates_pca = True, momentum_size = None).plot_loadings(n=4)





    l_lim = [2, 3]
    r_lim = [-0.5, 0.5]
    mrt.plotCurvehist(data_beirr[['AUD_BE_05Y','AUD_BE_10Y','AUD_BE_20Y']],lookback=5, numberOfHistory=6, title = 'AUD BEI', ax1_ylim = l_lim, ax2_ylim = r_lim)
    mrt.plotCurvehist(data_beirr[['USD_BE_02Y','USD_BE_05Y','USD_BE_30Y']],lookback=5, numberOfHistory=6, title = 'USD BEI', ax1_ylim = l_lim, ax2_ylim = r_lim)
    
    
    
    
                              
    mrt.plotCurvehist(data_linker_BEI.dropna(axis=1, how='all'),lookback=5, numberOfHistory=6, title = 'AUD BEI')
    
    mrt.plotCurvehist(data_linker_nm,lookback=5, numberOfHistory=6, title = 'AUD NOM')
    mrt.plotCurvehist(data_linker_rr,lookback=5, numberOfHistory=6, title = 'AUD RR')
    
    
    





##########################################################
# study 8 xccy pca
##########################################################


if(0>1):
    
    data_4model = data_ccy_basis #not yet use the combined data which has curve
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
    
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = sorted(list(set(data_4model.columns.map(lambda x: x[3:]))))
    tenors.sort(key = lambda x : int(x.replace('_0Y_','').replace('Y','')))


    #countries spot data
    country_spot_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in tenors],[])] for cn in countries}

   

    n=-73
      
 
    _s8_spot_xccy = country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s8_aud_spot_ss_pca = pca.PCA(_s8_spot_xccy.iloc[n:], rates_pca = True, momentum_size = None)
    s8_aud_spot_ss_pca.plot_loadings(n=4)


    _s8_spot_xccy = country_spot_data['JPY'].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
    s8_jpy_spot_ss_pca = pca.PCA(_s8_spot_xccy.iloc[n:], rates_pca = True, momentum_size = None)
    s8_jpy_spot_ss_pca.plot_loadings(n=4)
    

    
    
    mrt.plotCurvehist(country_spot_data['AUD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns").iloc[:-1],lookback=5, numberOfHistory=6, title = 'AUD xccy')
    mrt.plotCurvehist(country_spot_data['CAD'].rename(lambda c: c.replace("_0Y_","_"), axis="columns").iloc[:-1],lookback=5, numberOfHistory=6, title = 'CAD xccy')
    mrt.plotCurvehist(country_spot_data['JPY'].rename(lambda c: c.replace("_0Y_","_"), axis="columns").iloc[:-1],lookback=5, numberOfHistory=6, title = 'JPY xccy')
    mrt.plotCurvehist(country_spot_data['EUR'].rename(lambda c: c.replace("_0Y_","_"), axis="columns").iloc[:-1],lookback=5, numberOfHistory=6, title = 'EUR xccy')
    mrt.plotCurvehist(country_spot_data['KRW'].rename(lambda c: c.replace("_0Y_","_"), axis="columns").iloc[:-1],lookback=5, numberOfHistory=6, title = 'KRW xccy')
    mrt.plotCurvehist(country_spot_data['GBP'].rename(lambda c: c.replace("_0Y_","_"), axis="columns").iloc[:-1],lookback=5, numberOfHistory=6, title = 'GBP xccy')







##########################################################
# study 9 stir fut pca
##########################################################


if(0>1):
    
    data_stirfut = data_stirfut.loc[pd.isnull(data_stirfut.index) == False]

    n = -90
    stirPCA = {}
    secTypes = list(set(data_stirfut.columns.map(lambda x: x.split('_')[0])))
    for secType in secTypes:
        secCols = [c for c in data_stirfut.columns if secType in c]
        secCols.sort(key = lambda x : int(x.split('_')[1]))
        secCols = filter(lambda x: int(x.split('_')[1]) <= 12, secCols)
        
        #print(secType)
        secData = data_stirfut[secCols].iloc[n:]
        stirPCA[secType] = pca.PCA(secData, rates_pca = True, momentum_size = None)
        stirPCA[secType].plot_loadings(n=6)
        


##########################################################
# study 10 au linkers
##########################################################


if(0>1):
    
    data_linker = data_linker.loc[pd.isnull(data_linker.index) == False]

    data_linker_rr = data_linker[[c for c in data_linker.columns if 'AUDRR' in c]].rename(lambda c: c.replace('RR',''),axis=1)
    data_linker_nm = data_linker[[c for c in data_linker.columns if 'AUDNM' in c]].rename(lambda c: c.replace('NM',''),axis=1)
    data_linker_BEI = data_linker_nm.drop(['AUD_9Y'], axis=1) - data_linker_rr
    data_linker_BEI = data_linker_BEI[data_linker_rr.columns]
 
        
    mrt.plotCurvehist(data_linker_rr,lookback=5, numberOfHistory=6, title = 'AUD RR')
    mrt.plotCurvehist(data_linker_BEI.dropna(axis=1, how='all'),lookback=5, numberOfHistory=6, title = 'AUD BEI')
    mrt.plotCurvehist(data_linker_nm,lookback=5, numberOfHistory=6, title = 'AUD NOM')
    
    n = -1500
    pca.PCA(data_linker_rr.iloc[n:], 
            rates_pca = True, momentum_size = None).plot_loadings(n=4)

    pca.PCA(data_linker_BEI.iloc[n:].dropna(axis=1), 
            rates_pca = True, momentum_size = None).plot_loadings(n=4)




    
        
=============================================================================
    f1 = plt.figure(figsize = (15, 10))
    gs = gridspec.GridSpec(1, 2, width_ratios=[1,1])
    f1ax0 = plt.subplot(gs[0])
    f1ax1 = plt.subplot(gs[1])

=============================================================================



##########################################################
# study 21 citi ivol
##########################################################


if(0>1):
    
    data_4model = data_citivol
    
    # 
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    
    #tenors
    tenors = {c:list( [c + exp + t for exp in  ['_3M_','_6M_','_1Y_','_3Y_','_5Y_'] for  t in ['2Y', '5Y', '10Y', '30Y'] ]) for c in countries}
    
    
    #countries spot data
    country_spot_data = {cn:data_4model[tenors[cn]] for cn in countries}
    
    
    
    
    l_lim = [70, 160]
    r_lim = [-40, 20]
    
    mrt.plotCurvehist(country_spot_data['AUD'].iloc[:-1],lookback=29, numberOfHistory=2, title = 'AUD ivol', ax1_ylim=l_lim, ax2_ylim = r_lim)
    mrt.plotCurvehist(country_spot_data['JPY'].iloc[:-1],lookback=29, numberOfHistory=2, title = 'JPY ivol', ax1_ylim=[0, 90], ax2_ylim = r_lim)
    mrt.plotCurvehist(country_spot_data['EUR'].iloc[:-1],lookback=29, numberOfHistory=2, title = 'EUR ivol', ax1_ylim=l_lim, ax2_ylim = r_lim)
    mrt.plotCurvehist(country_spot_data['GBP'].iloc[:-1],lookback=29, numberOfHistory=2, title = 'GBP ivol', ax1_ylim=[80, 200], ax2_ylim = [-10, 30])
    mrt.plotCurvehist(country_spot_data['USD'].iloc[:-1],lookback=29, numberOfHistory=2, title = 'USD ivol', ax1_ylim=l_lim, ax2_ylim = r_lim)




    
##########################################################
# study 22 citi basis
##########################################################

if(0>1):
    
    countries = ['AUD', 'EUR', 'USD']
    
    basisTypes = [ '3S-OIS', '3S6S', '3S1S']    
    
    tenors = {c: list(c+'_'+ t for t in  ['3M','6M','1Y','2Y']) for c in countries}
    
    
    
    for basisType in basisTypes:
        
        data_4model = data_citibasis[[c for c in data_citibasis.columns if basisType in c]].rename(lambda c:  c.replace('_'+basisType, ''), axis=1)
        country_spot_data = {cn:data_4model[tenors[cn]] for cn in countries}
        
        l_lim = [70, 160]
        r_lim = [-40, 20]
         
        mrt.plotCurvehist(country_spot_data['AUD'].iloc[:-1],lookback=29, numberOfHistory=2, title = basisType)
        mrt.plotCurvehist(country_spot_data['EUR'].iloc[:-1],lookback=29, numberOfHistory=2, title = basisType)
        mrt.plotCurvehist(country_spot_data['USD'].iloc[:-1],lookback=29, numberOfHistory=2, title = basisType) 



    










##########################################################
# study 10 vols
##########################################################


if(0>1):

        
    
    f1 = plt.figure(figsize = (15, 10))
    gs = gridspec.GridSpec(3, 4, height_ratios=[1,1,1], width_ratios=[1,1,1,1])
    f1ax11 = plt.subplot(gs[0])
    f1ax12 = plt.subplot(gs[1])
    f1ax13 = plt.subplot(gs[2])    
    f1ax14 = plt.subplot(gs[3])
    f1ax21 = plt.subplot(gs[4])
    f1ax22 = plt.subplot(gs[5])
    f1ax23 = plt.subplot(gs[6])
    f1ax24 = plt.subplot(gs[7])
    f1ax31 = plt.subplot(gs[8])
    f1ax32 = plt.subplot(gs[9])
    f1ax33 = plt.subplot(gs[10])
    f1ax34 = plt.subplot(gs[11])
    


    pd.DataFrame({'USD_3M_2Y_RVOL':data_rvol_swap_3m.USD_0Y_2Y, 
                  'USD_3M_2Y_IVOL':data_citivol.USD_3M_2Y}) \
            .tail(180).plot(ax=f1ax11)
    
            
    
    pd.DataFrame({'USD_3M_5Y_RVOL':data_rvol_swap_3m.USD_0Y_5Y, 
                  'USD_3M_5Y_IVOL':data_citivol.USD_3M_5Y}) \
            .tail(180).plot(ax=f1ax12)
    
            
    
    pd.DataFrame({'USD_3M_10Y_RVOL':data_rvol_swap_3m.USD_0Y_10Y, 
                  'USD_3M_10Y_IVOL':data_citivol.USD_3M_10Y}) \
            .tail(180).plot(ax=f1ax13)
    
            
    
    pd.DataFrame({'USD_3M_30Y_RVOL':data_rvol_swap_3m.USD_0Y_30Y, 
                  'USD_3M_30Y_IVOL':data_citivol.USD_3M_30Y}) \
            .tail(180).plot(ax=f1ax14)
    

    pd.DataFrame({'USD_6M_2Y_RVOL':data_rvol_swap_6m.USD_0Y_2Y, 
                  'USD_6M_2Y_IVOL':data_citivol.USD_6M_2Y}) \
            .tail(180).plot(ax=f1ax21)
    
            
    
    pd.DataFrame({'USD_6M_5Y_RVOL':data_rvol_swap_6m.USD_0Y_5Y, 
                  'USD_6M_5Y_IVOL':data_citivol.USD_6M_5Y}) \
            .tail(180).plot(ax=f1ax22)
    
    
    pd.DataFrame({'USD_6M_10Y_RVOL':data_rvol_swap_6m.USD_0Y_10Y, 
                  'USD_6M_10Y_IVOL':data_citivol.USD_6M_10Y}) \
            .tail(180).plot(ax=f1ax23)
    
    
    pd.DataFrame({'USD_6M_30Y_RVOL':data_rvol_swap_6m.USD_0Y_30Y, 
                  'USD_6M_30Y_IVOL':data_citivol.USD_6M_30Y}) \
            .tail(180).plot(ax=f1ax24)
    

  pd.DataFrame({'USD_1Y_2Y_RVOL':data_rvol_swap_1y.USD_0Y_2Y, 
                'USD_1Y_2Y_IVOL':data_citivol.USD_1Y_2Y}) \
          .tail(180).plot(ax=f1ax31)
  
  
  pd.DataFrame({'USD_1Y_5Y_RVOL':data_rvol_swap_1y.USD_0Y_5Y, 
                'USD_1Y_5Y_IVOL':data_citivol.USD_1Y_5Y}) \
          .tail(180).plot(ax=f1ax32)
  
  
  pd.DataFrame({'USD_1Y_10Y_RVOL':data_rvol_swap_1y.USD_0Y_10Y, 
                'USD_1Y_10Y_IVOL':data_citivol.USD_1Y_10Y}) \
          .tail(180).plot(ax=f1ax33)
  
  
  pd.DataFrame({'USD_1Y_30Y_RVOL':data_rvol_swap_1y.USD_0Y_30Y, 
                'USD_1Y_30Y_IVOL':data_citivol.USD_1Y_30Y}) \
          .tail(180).plot(ax=f1ax34)


            

    
    f1ax11.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax21.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax31.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax12.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax22.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax32.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax13.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax23.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax33.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax14.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax24.legend(fancybox=True, framealpha=0, prop={'size': 6})
    f1ax34.legend(fancybox=True, framealpha=0, prop={'size': 6})


    f1ax11.xaxis.label.set_size(6)
    f1ax21.xaxis.label.set_size(6)
    f1ax31.xaxis.label.set_size(6)
    f1ax12.xaxis.label.set_size(6)
    f1ax22.xaxis.label.set_size(6)
    f1ax32.xaxis.label.set_size(6)
    f1ax13.xaxis.label.set_size(6)
    f1ax23.xaxis.label.set_size(6)
    f1ax33.xaxis.label.set_size(6)
    f1ax14.xaxis.label.set_size(6)
    f1ax24.xaxis.label.set_size(6)
    f1ax34.xaxis.label.set_size(6)

    f1.tight_layout()










##########################################################
# study 11 Ivol only usd left
##########################################################


#### only usd left



if(0>1):

    
    usd_ivol_cols = [c for c in data_ivol_in_yields.columns if 'USD' in c and ('_3M_' in c ) and c.split('_')[-1] in ['1Y', '2Y', '5Y','10Y','30Y']]
     
    usd_ivol_in_yields = data_ivol_in_yields[usd_ivol_cols].dropna(how='all', axis=1).ffill()
    pca.PCA(usd_ivol_in_yields.iloc[-2500:], 
            rates_pca = True, momentum_size = None
           ,sample_range = ('2013-01-01', '2019-11-01')
            ).plot_loadings(n=3)
    
    
    
    usd_ivol_cols = [c for c in data_ivol_in_yields.columns if 'USD' in c and ('_1Y_' in c ) and c.split('_')[-1] in ['1Y', '2Y', '5Y','10Y','30Y']]
     
    usd_ivol_in_yields = data_ivol_in_yields[usd_ivol_cols].dropna(how='all', axis=1).ffill()
    pca.PCA(usd_ivol_in_yields.iloc[-2500:], 
            rates_pca = True, momentum_size = None
           ,sample_range = ('2013-01-01', '2019-11-01')
            ).plot_loadings(n=3)
    

    from statistics.mr import bins
    from statistics.mr import sbins  
    from statistics.mr import tools as mrt
    

    # terminal rate vs long term risk premium    
    pca_obj = pca.PCA(data_swap[['USD_1Y_1Y', 'USD_2Y_10Y']].iloc[-2500:],rates_pca=True, sample_range = ('2015-01-01', '2019-12-01'))
    pca_obj.plot_loadings()


    bins.create_mr_diagnosis((data_swap['USD_2Y_10Y'] - data_swap['USD_1Y_1Y']).iloc[-2500:], 
                             20, min_num_of_bins = 20, resample_freq = 'W'
                             ,sample_range = ('2015-01-01', '2019-06-01')
                             ).create_mr_info_fig((18, 10))
     
    
    bins.create_mr_diagnosis((data_swap['USD_2Y_10Y']).iloc[-2500:], 20, min_num_of_bins = 20, resample_freq = 'W').create_mr_info_fig((18, 7))
    
    
    
    
    
    # 6m1y vol vs 3y10y policy uncertainty vs long term risk premium uncertaintity   
    pca.PCA(data_ivol_in_yields[['USD_6M_1Y', 'USD_3Y_10Y']].iloc[-2500:],rates_pca=True, sample_range = ('2015-01-01', '2019-12-01')).plot_loadings()
    
    
    
    
    for k, (mr, p) in \
        create_combo_mr(data_swap[['USD_1Y_1Y', 'USD_10Y_1Y']].iloc[-5000:].dropna(axis=0), 
                        elements = 2, resample_freq= 'B', 
                        include_normal_weighted_series=False, 
                        matrixoveride = None,
                        sample_range = ('2015-01-01', '2019-12-01')).items():
            
        mr.create_mr_info_fig((18, 7), pca_obj = p)
    
    
    
    for k, (mr, p) in \
        create_combo_mr(data_swap[['USD_1Y_1Y', 'USD_2Y_10Y']].iloc[-2500:], 
                        elements = 2, resample_freq= 'B', 
                        include_normal_weighted_series=False, 
                        matrixoveride = None,
                        sample_range = ('2015-01-01', '2019-12-01')).items():
            
        mr.create_mr_info_fig((18, 7), pca_obj = p)
    
    
    for k, (mr, p) in \
        create_combo_mr(data_swap[['USD_1Y_1Y', 'USD_2Y_10Y']].iloc[-2500:], 
                        elements = 2, resample_freq= 'W', 
                        include_normal_weighted_series=False, 
                        matrixoveride = None,
                        sample_range = ('2013-01-01', '2016-12-01')).items():
            
        mr.create_mr_info_fig((18, 7), pca_obj = p)
    
    
    
    
    for k, (mr, p) in \
        create_combo_mr(data_ivol_in_yields[['USD_6M_1Y', 'USD_3Y_10Y']].iloc[-2500:], 
                        elements = 2, resample_freq= 'W', 
                        include_normal_weighted_series=False, 
                        matrixoveride = None,
                        sample_range = ('2015-01-01', '2019-12-01')).items():
            
        mr.create_mr_info_fig((18, 7), pca_obj = p)
    

    
    
    x = pca.PCA(data_swap[['USD_1Y_1Y', 'USD_2Y_10Y']].iloc[-2500:],rates_pca=True, sample_range = ('2015-01-01', '2019-12-01'))


    
    bins.create_mr_diagnosis(data_gov['USD_0Y_10Y'].iloc[-5000:], 20, min_num_of_bins = 20, resample_freq = 'W').plot_distribution_diagnosis((14, 8))
    
    bins.create_mr_diagnosis((data_gov['USD_0Y_30Y'] - data_gov['USD_0Y_5Y']).iloc[-5000:], 20, min_num_of_bins = 20, resample_freq = 'W').plot_distribution_diagnosis((14, 8))
    
    
    bins.create_mr_diagnosis(data_gov['USD_0Y_10Y'].iloc[-20:], 20, min_num_of_bins = 20, resample_freq = 'B').plot_distribution_diagnosis((14, 8))
    
    
    # first object is the mr 
    mrs['CAD_5Y_5Y_USD_0Y_30Y_PCW'][0].create_mr_info_fig((18, 7), pca_obj = mrs['CAD_5Y_5Y_USD_0Y_30Y_PCW'][1])
    
    # call its ou to simulate   
    sim, distrib = mrs['CAD_5Y_5Y_USD_5Y_5Y_PCW'][0].ou.simulate_with_model_parameters(k=5000, target=-0.25, stop=-0.9)
    
    ########
    c = [c for c in data_swap2.columns if 'USD_0Y' in c or 'CAD_0Y' in c]
    pca.PCA(data_swap2[c].iloc[-3000:],rates_pca=True, sample_range = ('2017-01-01', '2019-11-01')).plot_loadings(n=6)
    




##########################################################
# study 12 Ivol only usd left
##########################################################



    
    data_21 = data_gov[['USD_0Y_2Y', 'USD_0Y_5Y', 'USD_0Y_10Y', 'USD_0Y_30Y']].iloc[:-1]
    data_21['USD_2s10s'] = data_21.USD_0Y_10Y - data_21.USD_0Y_2Y 
    data_21['USD_10s30s'] = data_21.USD_0Y_30Y - data_21.USD_0Y_10Y 
    data_21['USD_5s30s'] = data_21.USD_0Y_30Y - data_21.USD_0Y_5Y 
    
    data_21_next_day_rtn = (data_21 - data_21.shift(1)).shift(-1).rename(lambda x: '{}_return'.format(x), axis=1)
    data_21  = pd.concat([data_21, data_21_next_day_rtn], axis=1).dropna()
    
    data_21_sbin = sbins.sbins(data_21.iloc[-3000:], 'USD_0Y_10Y',10)
    
    data_21_sbin.plot_bin_mean_variance_for_col('USD_10s30s_return')
    
    data_21_sbin.plot_bin_mean_variance_for_col('USD_0Y_30Y_return')
    
    
    
    







#test13 USD only

# swap
# ifs


usd_spot_swap_test13 = data_swap[['USD_0Y_1Y',
                         'USD_0Y_2Y',
                         'USD_0Y_3Y',
                         #'USD_0Y_4Y',
                         'USD_0Y_5Y',
                         #'USD_0Y_6Y',
                         #'USD_0Y_7Y',
                         #'USD_0Y_8Y',
                         #'USD_0Y_9Y',
                         'USD_0Y_10Y',
                         #'USD_0Y_20Y',
                         'USD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")


usd_spot_gov_test13 = data_gov[[ 'USD_0Y_1Y',
 'USD_0Y_2Y',
 'USD_0Y_3Y',
 'USD_0Y_5Y',
 #'USD_0Y_7Y',
 'USD_0Y_10Y',
 #'USD_0Y_20Y',
 'USD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")




usd_spot_ifs_test13 = data_ifs[['USD_1Y_SWIT',
                         'USD_2Y_SWIT',
                         'USD_3Y_SWIT',
                         #'USD_4Y_SWIT',
                         'USD_5Y_SWIT',
                         #'USD_6Y_SWIT',
                         #'USD_7Y_SWIT',
                         #'USD_8Y_SWIT',
                         #'USD_9Y_SWIT',
                         'USD_10Y_SWIT',
                         'USD_20Y_SWIT',
                         'USD_30Y_SWIT']]




# eur_spot_ifs_test13 = data_ifs[['EUR_1Y_SWIT',
#                          'EUR_2Y_SWIT',
#                          'EUR_3Y_SWIT',
#                          #'EUR_4Y_SWIT',
#                          'EUR_5Y_SWIT',
#                          #'EUR_6Y_SWIT',
#                          #'EUR_7Y_SWIT',
#                          #'EUR_8Y_SWIT',
#                          #'EUR_9Y_SWIT',
#                          'EUR_10Y_SWIT',
#                          'EUR_20Y_SWIT',
#                          'EUR_30Y_SWIT']]




usd_data_rvol_daily_1m_test13 = data_rvol_swap_1m_daily[[
 #'USD_0Y_3M',
 'USD_0Y_1Y',
 'USD_0Y_2Y',
 'USD_0Y_3Y',
 #'USD_0Y_4Y',
 'USD_0Y_5Y',
 #'USD_0Y_6Y',
 #'USD_0Y_7Y',
 #'USD_0Y_8Y',
 #'USD_0Y_9Y',
 'USD_0Y_10Y',
 'USD_0Y_20Y',
 'USD_0Y_30Y']].rename(lambda c: c.replace('_0Y',''), axis=1) #* np.sqrt(256)



usd_data_rvol_daily_3m_test13 = data_rvol_swap_3m_daily[[
 #'USD_0Y_3M',
 'USD_0Y_1Y',
 'USD_0Y_2Y',
 'USD_0Y_3Y',
 #'USD_0Y_4Y',
 'USD_0Y_5Y',
 #'USD_0Y_6Y',
 #'USD_0Y_7Y',
 #'USD_0Y_8Y',
 #'USD_0Y_9Y',
 'USD_0Y_10Y',
 'USD_0Y_20Y',
 'USD_0Y_30Y']].rename(lambda c: c.replace('_0Y',''), axis=1)  #* np.sqrt(256)




usd_data_ivol_1m_in_yields_yearly_test13 = data_ivol_in_yields_daily[[
 'USD_1M_1Y',
 'USD_1M_2Y',
 'USD_1M_3Y',
 'USD_1M_5Y',
 'USD_1M_10Y',
 'USD_1M_20Y',
 'USD_1M_30Y']].rename(lambda c: c.replace('_1M',''), axis=1)

usd_data_ivol_1m_in_bps_yearly_test13 = data_vol[[
 'USD_1M_1Y',
 'USD_1M_2Y',
 'USD_1M_3Y',
 'USD_1M_5Y',
 'USD_1M_10Y',
 'USD_1M_20Y',
 'USD_1M_30Y']].rename(lambda c: c.replace('_1M',''), axis=1)



usd_data_ivol_3m_in_yields_yearly_test13 = data_ivol_in_yields_daily[[
 'USD_3M_1Y',
 'USD_3M_2Y',
 'USD_3M_3Y',
 'USD_3M_5Y',
 'USD_3M_10Y',
 'USD_3M_20Y',
 'USD_3M_30Y']].rename(lambda c: c.replace('_3M',''), axis=1)


usd_data_ivol_3m_in_bps_yearly_test13 = data_vol[[
 'USD_3M_1Y',
 'USD_3M_2Y',
 'USD_3M_3Y',
 'USD_3M_5Y',
 'USD_3M_10Y',
 'USD_3M_20Y',
 'USD_3M_30Y']].rename(lambda c: c.replace('_3M',''), axis=1)



usd_data_ivol_1y_in_yields_daily_test13 = data_ivol_in_yields_daily[[
 'USD_1Y_1Y',
 'USD_1Y_2Y',
 'USD_1Y_3Y',
 'USD_1Y_5Y',
 'USD_1Y_10Y',
 'USD_1Y_20Y',
 'USD_1Y_30Y']].rename(lambda c: c.replace('_1Y_',''), axis=1)



usd_data_ivol_1y_in_bps_daily_test13 = data_vol[[
 'USD_1Y_1Y',
 'USD_1Y_2Y',
 'USD_1Y_3Y',
 'USD_1Y_5Y',
 'USD_1Y_10Y',
 'USD_1Y_20Y',
 'USD_1Y_30Y']].rename(lambda c: c.replace('_1Y_',''), axis=1)





# usd_data_ivol_rvol_1m_test13 = usd_data_ivol_1m_in_yields_yearly_test13 / (usd_data_rvol_daily_1m_test13 * np.sqrt(256))
usd_data_ivol_rvol_1m_test13 = usd_data_ivol_1m_in_bps_yearly_test13 / (usd_data_rvol_daily_1m_test13 * np.sqrt(256))


# usd_data_ivol_rvol_3m_test13 = usd_data_ivol_3m_in_yields_yearly_test13 / (usd_data_rvol_daily_3m_test13 * np.sqrt(256))
usd_data_ivol_rvol_3m_test13 = usd_data_ivol_3m_in_bps_yearly_test13 / (usd_data_rvol_daily_3m_test13 * np.sqrt(256))




#inflation swap date seems to be always 1day lag. check

# use swap as the nominal rate
usd_spot_rr_test13 = usd_spot_swap_test13 - usd_spot_ifs_test13.rename(lambda c: c.replace('_SWIT',''), axis=1).reindex(usd_spot_swap_test13.index).ffill()[['USD_1Y', 'USD_2Y', 'USD_3Y','USD_5Y', 'USD_10Y', 'USD_30Y']]


# use gov as the nominal rate
#usd_spot_rr_test13 = usd_spot_gov_test13 - usd_spot_ifs_test13.rename(lambda c: c.replace('_SWIT',''), axis=1).reindex(usd_spot_gov_test13.index).ffill()


#
usd_spot_ss_test13 = usd_spot_swap_test13.reindex(usd_spot_gov_test13.index).ffill() - usd_spot_gov_test13 


usd_fwd_swap_test13 = bootstrap_ifs_fwd(usd_spot_swap_test13)
usd_fwd_gov_test13 = bootstrap_ifs_fwd(usd_spot_gov_test13)
usd_fwd_ifs_test13 =  bootstrap_ifs_fwd(usd_spot_ifs_test13)
usd_fwd_rr_test13 =  bootstrap_ifs_fwd(usd_spot_rr_test13)
usd_fwd_ss_test13 =  usd_fwd_swap_test13.reindex(usd_fwd_gov_test13.index).ffill() - usd_fwd_gov_test13


#eur_fwd_ifs_test13 =  bootstrap_ifs_fwd(eur_spot_ifs_test13)


### Output charts

#ivol/rvol

f0 = plt.figure(figsize = (15, 10))
gs0 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f0ax0 = plt.subplot(gs0[0])
f0ax1 = plt.subplot(gs0[1])
f0ax2 = plt.subplot(gs0[2])
f0ax3 = plt.subplot(gs0[3])


func(usd_data_ivol_rvol_1m_test13).rename(lambda c: c.split('_')[1]).plot(ax=f0ax0)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f0ax0.set_title('1m ivol/1 stdev ratio swap curves')


usd_data_ivol_rvol_1m_test13.iloc[-200:].plot(ax=f0ax1)
f0ax1.set_title('1m ivol/1 stdev ratio swap curves')

usd_data_ivol_rvol_1m_test13.iloc[-1000:-200].plot(ax=f0ax2)
f0ax2.set_title('1m ivol/1 stdev ratio swap curves')

usd_data_ivol_rvol_1m_test13.iloc[-2000:-1000].plot(ax=f0ax3)
f0ax3.set_title('1m ivol/1 stdev ratio swap curves')


pca.PCA(usd_data_ivol_rvol_1m_test13.iloc[-2500:], 
        rates_pca = True, momentum_size = None, 
        sample_range = ('2013-01-01', '2019-11-01')).plot_loadings(n=6)



pca.PCA(usd_data_ivol_rvol_1m_test13.iloc[-2500:], 
        rates_pca = True, momentum_size = None, 
        sample_range = ('2013-01-01', '2019-11-01')).plot_valuation_history(750, n=[1], w=16, h=12, rows=3, cols=3, 
                                                                            normalise_valuation_yaxis = False, 
                                   normalise_data_yaxis=False, exp_standardise_val=True,
                                   selected_series=usd_data_ivol_rvol_1m_test13.columns.tolist())  
                                                                            
                                                                            


f0 = plt.figure(figsize = (15, 10))
gs0 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f0ax0 = plt.subplot(gs0[0])
f0ax1 = plt.subplot(gs0[1])
f0ax2 = plt.subplot(gs0[2])
f0ax3 = plt.subplot(gs0[3])


func(usd_data_ivol_rvol_3m_test13).rename(lambda c: c.split('_')[1]).plot(ax=f0ax0)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f0ax0.set_title('3m ivol/1 stdev ratio swap curves')

usd_data_ivol_rvol_3m_test13.iloc[-200:].plot(ax=f0ax1)
f0ax1.set_title('3m ivol/1 stdev ratio swap curves')

usd_data_ivol_rvol_3m_test13.iloc[-1000:-200].plot(ax=f0ax2)
f0ax2.set_title('3m ivol/1 stdev ratio swap curves')

usd_data_ivol_rvol_3m_test13.iloc[-2000:-1000].plot(ax=f0ax3)
f0ax3.set_title('3m ivol/1 stdev ratio swap curves')


pca.PCA(usd_data_ivol_rvol_1m_test13.iloc[-2500:], 
        rates_pca = True, momentum_size = None, 
        sample_range = ('2013-01-01', '2019-11-01')).plot_loadings(n=6)






usd_data_ivol_rvol_1m_long_cycle_pca_test13 = pca.PCA(usd_data_ivol_rvol_1m_test13.iloc[-2500:], 
                                                      rates_pca = True, momentum_size = None, 
                                                      sample_range = ('2013-01-01', '2019-11-01'))


usd_data_ivol_rvol_3m_long_cycle_pca_test13 = pca.PCA(usd_data_ivol_rvol_3m_test13.iloc[-2500:], 
                                                      rates_pca = True, momentum_size = None, 
                                                      sample_range = ('2013-01-01', '2019-11-01'))




#pick pc1 and pc2 as the input for market regime analysis

usd_rvol_1m_long_cycle_pcs_test13 = usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs[['PC1', 'PC2']].rename(lambda x: "vol_ratio_" + x, axis=1)


win = 40
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line1_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(usd_data_ivol_rvol_1m_long_cycle_pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()


win = 40
data_rtns_d_3m_rolling_corrs = pd.DataFrame(rolling_apply(usd_data_ivol_rvol_3m_long_cycle_pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line2_test13 = (data_rtns_d_3m_rolling_corrs['PC1_vs_PC2'] * np.sign(usd_data_ivol_rvol_3m_long_cycle_pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()

regime_test13 = pd.DataFrame({'1M i/r pc1pc2 corr': regime_line1_test13, 
                              '3M i/r pc1pc2 corr': regime_line2_test13,
                              'US10': data_swap2['USD_0Y_10Y'].reindex(regime_line1_test13.index),
                              'US2S10': (data_swap2['USD_0Y_10Y'] - data_swap2['USD_0Y_2Y']).reindex(regime_line1_test13.index)})

regime_test13.plot(title='US interest rate trading regime', secondary_y=['US10', 'US2S10'])



#inflation USD

if(3>2):
    
    def func(df): 
        return pd.DataFrame({
                          'Now':df.iloc[-2:].mean(),                      
                          #'7d avg':df.iloc[-7:].mean(), 
                          #'7d avg 7d ago':df.iloc[-14:-7].mean(), 
                          '7d avg 14d ago':df.iloc[-21:-14].mean(), 
                          '7d avg 28d ago':df.iloc[-35:-28].mean(),
                          '7d avg 60d ago':df.iloc[-67:-60].mean(),
                          #'2020 April avg': df[(df.index > '2020-03-31') & (df.index < '2020-05-01')].mean(),
                          #'2020 May avg': df[(df.index > '2020-04-30') & (df.index < '2020-06-01')].mean(),
                          #'2020 June avg': df[(df.index > '2020-05-31') & (df.index < '2020-07-01')].mean(),
                          #'2020 July avg': df[(df.index > '2020-06-30') & (df.index < '2020-08-01')].mean(),
                          #'2010-Oct-2011-Jul': df[(df.index > '2010-10-01') & (df.index < '2011-07-01')].mean(),
                          #'2012-2013 avg': df[(df.index > '2012-01-01') & (df.index < '2013-12-31')].mean(),
                          #'2016-2019 avg': df[(df.index > '2016-01-01') & (df.index < '2019-12-31')].mean(),
                          #'2016-08': df[(df.index > '2016-08-01') & (df.index < '2016-09-01')].mean(),
                          #'2016-09': df[(df.index > '2016-09-01') & (df.index < '2016-10-01')].mean(),
                          #'2016-10': df[(df.index > '2016-10-01') & (df.index < '2016-11-01')].mean()
                          }) 


f1 = plt.figure(figsize = (15, 10))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f1ax0 = plt.subplot(gs[0])
f1ax1 = plt.subplot(gs[1])
f1ax2 = plt.subplot(gs[2])
f1ax3 = plt.subplot(gs[3])


func(usd_spot_ifs_test13).rename(lambda c: c.split('_')[1]).plot(ax=f1ax0)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f1ax0.set_title('Spot inflation swap curves')

func(usd_fwd_ifs_test13).plot(ax=f1ax1)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f1ax1.set_title('Forward inflation swap curves')

vol_snapshot(usd_spot_ifs_test13).rename(lambda c: c.split('_')[1]).plot(ax=f1ax2)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f1ax2.set_title('Spot inflation swap curves vol')

vol_snapshot(usd_fwd_ifs_test13).plot(ax=f1ax3)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f1ax3.set_title('Forward inflation swap curves vol')



pca.PCA(usd_spot_ifs_test13.iloc[-2500:], 
        rates_pca = True, momentum_size = None, 
        sample_range = ('2013-01-01', '2019-12-01')).plot_loadings(n=6)



pca.PCA(usd_spot_ifs_test13.iloc[-2500:], 
        rates_pca = True, momentum_size = None, 
        sample_range = ('2013-01-01', '2019-12-01')).plot_valuation_history(2500, n=[1], w=16, h=12, rows=3, cols=3, 
                                                                            normalise_valuation_yaxis = False, 
                                   normalise_data_yaxis=False, exp_standardise_val=True,
                                   selected_series=usd_spot_ifs_test13.columns.tolist())  
                                                                            
         




pca.PCA(usd_spot_ifs_test13.iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_fwd_ifs_test13.iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(usd_fwd_ifs_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_spot_ifs_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

usd_spot_ifs_long_cycle_pca_test13 = pca.PCA(usd_spot_ifs_test13[usd_spot_ifs_test13.index > '2008-01-01'], rates_pca = True, momentum_size = None)



usd_spot_ifs_long_cycle_pcs_test13 = usd_spot_ifs_long_cycle_pca_test13.pcs[['PC1', 'PC2']].rename(lambda x: "ifs_" + x, axis=1)





#### correlation chart ##########

pca_test13 = pca.PCA(usd_spot_ifs_test13.iloc[-2500:], rates_pca = True, momentum_size = None, sample_range = ('2013-01-01', '2015-11-01'))

win = 40
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line1_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()


win = 20
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line2_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()

regime_test13 = pd.DataFrame({'1M implied': regime_line2_test13, '3M implied': regime_line1_test13})

regime_test13.plot(title='US ifs pc1 pc2 correlation')








# #inflation eur

# if(2>1):
    
#     def func(df): 
#         return pd.DataFrame({
#                           'Now':df.iloc[-2:].mean(),                      
#                           #'7d avg':df.iloc[-7:].mean(), 
#                           #'7d avg 7d ago':df.iloc[-14:-7].mean(), 
#                           #'7d avg 14d ago':df.iloc[-21:-14].mean(), 
#                           #'7d avg 21d ago':df.iloc[-28:-21].mean(),                      
#                           '2020 April avg': df[(df.index > '2020-03-31') & (df.index < '2020-05-01')].mean(),
#                           '2020 May avg': df[(df.index > '2020-04-30') & (df.index < '2020-06-01')].mean(),
#                           '2020 June avg': df[(df.index > '2020-05-31') & (df.index < '2020-07-01')].mean(),
#                           '2020 July avg': df[(df.index > '2020-06-30') & (df.index < '2020-08-01')].mean(),
#                           #'2010-Oct-2011-Jul': df[(df.index > '2010-10-01') & (df.index < '2011-07-01')].mean(),
#                           #'2012-2013 avg': df[(df.index > '2012-01-01') & (df.index < '2013-12-31')].mean(),
#                           #'2016-2019 avg': df[(df.index > '2016-01-01') & (df.index < '2019-12-31')].mean(),
#                           #'2016-08': df[(df.index > '2016-08-01') & (df.index < '2016-09-01')].mean(),
#                           #'2016-09': df[(df.index > '2016-09-01') & (df.index < '2016-10-01')].mean(),
#                           #'2016-10': df[(df.index > '2016-10-01') & (df.index < '2016-11-01')].mean()
#                           }) 


# f1 = plt.figure(figsize = (15, 10))
# gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
# f1ax0 = plt.subplot(gs[0])
# f1ax1 = plt.subplot(gs[1])
# f1ax2 = plt.subplot(gs[2])
# f1ax3 = plt.subplot(gs[3])


# func(eur_spot_ifs_test13).rename(lambda c: c.split('_')[1]).plot(ax=f1ax0)
# #plt.xticks(np.arange(len(func(eur_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(eur_spot_ifs_test13).index])
# f1ax0.set_title('Spot inflation swap curves')

# func(eur_fwd_ifs_test13).plot(ax=f1ax1)
# #plt.xticks(np.arange(len(func(eur_fwd_ifs_test13).index)), func(eur_fwd_ifs_test13).index.tolist())
# f1ax1.set_title('Forward inflation swap curves')

# vol_snapshot(eur_spot_ifs_test13).rename(lambda c: c.split('_')[1]).plot(ax=f1ax2)
# #plt.xticks(np.arange(len(func(eur_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(eur_spot_ifs_test13).index])
# f1ax2.set_title('Spot inflation swap curves vol')

# vol_snapshot(eur_fwd_ifs_test13).plot(ax=f1ax3)
# #plt.xticks(np.arange(len(func(eur_fwd_ifs_test13).index)), func(eur_fwd_ifs_test13).index.tolist())
# f1ax3.set_title('Forward inflation swap curves vol')

# pca.PCA(eur_spot_ifs_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
# pca.PCA(eur_spot_ifs_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

# pca.PCA(eur_fwd_ifs_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
# pca.PCA(eur_fwd_ifs_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)






#swap

f2 = plt.figure(figsize = (15, 10))
gs2 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f2ax0 = plt.subplot(gs2[0])
f2ax1 = plt.subplot(gs2[1])
f2ax2 = plt.subplot(gs2[2])
f2ax3 = plt.subplot(gs2[3])

func(usd_spot_swap_test13).rename(lambda c: c.split('_')[1]).plot(ax=f2ax0)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
f2ax0.set_title('Spot nominal swap curves')

func(usd_fwd_swap_test13).plot(ax=f2ax1)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
f2ax1.set_title('Forward nominal swap curves')


vol_snapshot(usd_spot_swap_test13).rename(lambda c: c.split('_')[1]).plot(ax=f2ax2)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f2ax2.set_title('Spot swap curves vol')

vol_snapshot(usd_fwd_swap_test13).plot(ax=f2ax3)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f2ax3.set_title('Forward swap curves vol')


pca.PCA(usd_spot_swap_test13.iloc[-20:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_spot_swap_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(usd_fwd_swap_test13.iloc[-20:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_fwd_swap_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)




#### correlation chart ##########


df = \
{
 '2007':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2007-01-01', '2008-01-01')).loadings_lambda,
 '2008':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2008-01-01', '2009-01-01')).loadings_lambda,
 '2009':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2009-01-01', '2010-01-01')).loadings_lambda,
 '2010':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2010-01-01', '2011-01-01')).loadings_lambda,
 '2011':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2011-01-01', '2012-01-01')).loadings_lambda,
 '2012':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2012-01-01', '2013-01-01')).loadings_lambda,
 '2013':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2013-01-01', '2014-01-01')).loadings_lambda,
 '2014':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2014-01-01', '2015-01-01')).loadings_lambda,
 '2015':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2015-01-01', '2016-01-01')).loadings_lambda,
 '2016':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2016-01-01', '2017-01-01')).loadings_lambda,
 '2017':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2017-01-01', '2018-01-01')).loadings_lambda,
 '2018':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2018-01-01', '2019-01-01')).loadings_lambda,
 '2019':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2019-01-01', '2020-01-01')).loadings_lambda,
 '2020':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2020-01-01', '2021-01-01')).loadings_lambda,
 '2021':pca.PCA(usd_spot_swap_test13.iloc[-4000:], rates_pca = True, momentum_size = None, sample_range = ('2021-01-01', '2022-01-01')).loadings_lambda 
}

df = pd.DataFrame(df)



pca_test13 = pca.PCA(usd_spot_swap_test13.iloc[-2500:], rates_pca = True, momentum_size = None, sample_range = ('2013-01-01', '2019-11-01'))

pca_test13.plot_loadings(n=6)




pca_test13.plot_valuation_history(2500, n=[1], w=16, h=12, rows=3, cols=3,
                                  normalise_valuation_yaxis = False,
                                  normalise_data_yaxis=False, exp_standardise_val=True,
                                  selected_series=usd_spot_ifs_test13.columns.tolist())  
                                                                            
         





win = 40
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line1_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()


win = 20
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line2_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()

regime_test13 = pd.DataFrame({'1M implied': regime_line2_test13, '3M implied': regime_line1_test13})

regime_test13.plot(title='US interest rate trading regime')









#gov

f3 = plt.figure(figsize = (15, 10))
gs3 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f3ax0 = plt.subplot(gs3[0])
f3ax1 = plt.subplot(gs3[1])
f3ax2 = plt.subplot(gs3[2])
f3ax3 = plt.subplot(gs3[3])

func(usd_spot_gov_test13).rename(lambda c: c.split('_')[1]).plot(ax=f3ax0)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
f3ax0.set_title('Spot nominal gov curves')

func(usd_fwd_gov_test13).plot(ax=f3ax1)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
f3ax1.set_title('Forward nominal gov curves')


vol_snapshot(usd_spot_gov_test13).rename(lambda c: c.split('_')[1]).plot(ax=f3ax2)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f3ax2.set_title('Spot gov curves vol')

vol_snapshot(usd_fwd_gov_test13).plot(ax=f3ax3)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f3ax3.set_title('Forward gov curves vol')



pca.PCA(usd_spot_gov_test13.drop(['USD_20Y'],axis=1).iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_spot_gov_test13.drop(['USD_20Y'],axis=1).iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)


pca.PCA(bootstrap_ifs_fwd(usd_spot_gov_test13.drop(['USD_20Y'],axis=1)).iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(bootstrap_ifs_fwd(usd_spot_gov_test13.drop(['USD_20Y'],axis=1)).iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)




#### correlation chart ##########

pca_test13 = pca.PCA(usd_spot_gov_test13.iloc[-2500:], rates_pca = True, momentum_size = None, sample_range = ('2013-01-01', '2015-11-01'))

pca_test13.plot_loadings(n=6)

win = 40
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line1_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()


win = 20
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line2_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()

regime_test13 = pd.DataFrame({'1M implied': regime_line2_test13, '3M implied': regime_line1_test13})

regime_test13.plot(title='US interest rate trading regime')









#RR


f4 = plt.figure(figsize = (15, 10))
gs4 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f4ax0 = plt.subplot(gs4[0])
f4ax1 = plt.subplot(gs4[1])
f4ax2 = plt.subplot(gs4[2])
f4ax3 = plt.subplot(gs4[3])

func(usd_spot_rr_test13).rename(lambda c: c.split('_')[1]).plot(ax=f4ax0)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
f4ax0.set_title('Spot nominal swap rr curves')

func(usd_fwd_rr_test13).plot(ax=f4ax1)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
f4ax1.set_title('Forward nominal swap rr curves')


vol_snapshot(usd_spot_rr_test13).rename(lambda c: c.split('_')[1]).plot(ax=f4ax2)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f4ax2.set_title('Spot swap rr curve vol')

vol_snapshot(usd_fwd_rr_test13).plot(ax=f4ax3)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f4ax3.set_title('Forward swap rr curves vol')

pca.PCA(usd_spot_rr_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_spot_rr_test13.iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(usd_fwd_rr_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_fwd_rr_test13.iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)


#### correlation chart ##########

pca_test13 = pca.PCA(usd_spot_rr_test13.iloc[-2500:], rates_pca = True, momentum_size = None, sample_range = ('2013-01-01', '2019-11-01'))

pca_test13.plot_loadings(n=6)

win = 40
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line1_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()


win = 20
data_rtns_d_1m_rolling_corrs = pd.DataFrame(rolling_apply(pca_test13.pcs[['PC1', 'PC2']], win, get_flattened_corr)).transpose()
regime_line2_test13 = (data_rtns_d_1m_rolling_corrs['PC1_vs_PC2'] * np.sign(pca_test13.pcs['PC1'].ewm(win).mean())).ewm(win).mean()

regime_test13 = pd.DataFrame({'1M implied': regime_line2_test13, '3M implied': regime_line1_test13})

regime_test13.plot(title='US interest rate trading regime')





#SS


f5 = plt.figure(figsize = (15, 10))
gs5 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f5ax0 = plt.subplot(gs5[0])
f5ax1 = plt.subplot(gs5[1])
f5ax2 = plt.subplot(gs5[2])
f5ax3 = plt.subplot(gs5[3])

func(usd_spot_ss_test13).rename(lambda c: c.split('_')[1]).plot(ax=f5ax0)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
f4ax0.set_title('Spot ss curves')

func(usd_fwd_ss_test13).plot(ax=f5ax1)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
f5ax1.set_title('Forward ss curves')


vol_snapshot(usd_spot_ss_test13).rename(lambda c: c.split('_')[1]).plot(ax=f5ax2)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f5ax2.set_title('Spot ss curve vol')

vol_snapshot(usd_fwd_ss_test13).plot(ax=f5ax3)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
f5ax3.set_title('Forward ss curves vol')



pca.PCA(usd_spot_ss_test13.drop(['USD_20Y'],axis=1).iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(usd_spot_ss_test13.drop(['USD_20Y'],axis=1).iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)


pca.PCA(bootstrap_ifs_fwd(usd_spot_ss_test13.drop(['USD_20Y'],axis=1)).iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(bootstrap_ifs_fwd(usd_spot_ss_test13.drop(['USD_20Y'],axis=1)).iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)






#test14

aud_spot_swap_test14 = data_swap[['AUD_0Y_1Y',
                         'AUD_0Y_2Y',
                         'AUD_0Y_3Y',
                         #'AUD_0Y_4Y',
                         'AUD_0Y_5Y',
                         #'AUD_0Y_6Y',
                         #'AUD_0Y_7Y',
                         #'AUD_0Y_8Y',
                         #'AUD_0Y_9Y',
                         'AUD_0Y_10Y',
                         'AUD_0Y_20Y',
                         'AUD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")


aud_spot_gov_test14 = data_gov[[ 'AUD_0Y_1Y',
 'AUD_0Y_2Y',
 'AUD_0Y_3Y',
 'AUD_0Y_5Y',
 #'USD_0Y_7Y',
 'AUD_0Y_10Y',
 'AUD_0Y_20Y',
 'AUD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")





aud_data_rvol_daily_1m_test14 = data_rvol_swap_1m_daily[[
 #'USD_0Y_3M',
 'AUD_0Y_1Y',
 'AUD_0Y_2Y',
 #'AUD_0Y_3Y',
 #'AUD_0Y_4Y',
 'AUD_0Y_5Y',
 #'AUD_0Y_6Y',
 #'AUD_0Y_7Y',
 #'AUD_0Y_8Y',
 #'AUD_0Y_9Y',
 'AUD_0Y_10Y',
 #'AUD_0Y_20Y',
 'AUD_0Y_30Y']].rename(lambda c: c.replace('_0Y',''), axis=1)


aud_data_ivol_1m_in_yields_daily_test14 = data_ivol_in_yields_daily[['AUD_1M_1Y',
 'AUD_1M_2Y',
 'AUD_1M_5Y',
 'AUD_1M_10Y',
 'AUD_1M_30Y']].rename(lambda c: c.replace('_1M',''), axis=1)


aud_data_ivol_rvol_1m_test14 = aud_data_ivol_1m_in_yields_daily_test14 / aud_data_rvol_daily_1m_test14








#ivol/rvol

f0 = plt.figure(figsize = (15, 10))
gs0 = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
f0ax0 = plt.subplot(gs0[0])
f0ax1 = plt.subplot(gs0[1])
f0ax2 = plt.subplot(gs0[2])
f0ax3 = plt.subplot(gs0[3])


func(aud_data_ivol_rvol_1m_test14).rename(lambda c: c.split('_')[1]).plot(ax=f0ax0)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
f0ax0.set_title('aud 1m ivol/rvol ratio swap curves')


aud_data_ivol_rvol_1m_test14.iloc[-1000:].plot(ax=f0ax1)
f0ax1.set_title('ivol/rvol ratio swap curves')

aud_data_ivol_rvol_1m_test14.iloc[-2000:-1000].plot(ax=f0ax2)
f0ax2.set_title('ivol/rvol ratio swap curves')

aud_data_ivol_rvol_1m_test14.iloc[-3000:-2000].plot(ax=f0ax3)
f0ax3.set_title('ivol/rvol ratio swap curves')


pca.PCA(data_ivol_rvol_1m_test13.iloc[-60:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(data_ivol_rvol_1m_test13.iloc[-500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)
pca.PCA(data_ivol_rvol_1m_test13.iloc[-3000:], rates_pca = True, momentum_size = None).plot_loadings(n=3)














usd_fwd_rr_test13['10Y10Y'].plot()





nzd_spot_swap_test13 = data_swap[['NZD_0Y_1Y',
                         'NZD_0Y_2Y',
                         'NZD_0Y_3Y',
                         'NZD_0Y_4Y',
                         'NZD_0Y_5Y',
                         'NZD_0Y_6Y',
                         'NZD_0Y_7Y',
                         'NZD_0Y_8Y',
                         'NZD_0Y_9Y',
                         'NZD_0Y_10Y',
                         'NZD_5Y_5Y'
                         ]].rename(lambda c: c.replace("_0Y_","_"), axis="columns")


nzd_spot_swap_test13 = data_swap[['NZD_1Y_1Y',
                         
                         'NZD_5Y_5Y'
                         ]].rename(lambda c: c.replace("_0Y_","_"), axis="columns")



pca.PCA(data_ccy_basis[[c for c in data_ccy_basis.columns if 'AUD' in c or 'NZD' in c or 'CAD' in c or 'EUR' in c or 'JPY' in c]].iloc[-750:], rates_pca = True, momentum_size = None).plot_loadings(n=3)



pca.PCA(data_ifs[['USD_5Y_SWIT', 'EUR_10Y_SWIT']].iloc[-750:], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(data_ifs[['USD_5Y_SWIT', 'EUR_10Y_SWIT']].iloc[-2500:], rates_pca = True, momentum_size = None).create_hedged_pcs_fig()

pca.PCA(data_ifs[['USD_10Y_SWIT', 'EUR_5Y_SWIT']].iloc[-2500:], rates_pca = True, momentum_size = None).create_hedged_pcs_fig()
pca.PCA(data_ifs[['USD_10Y_SWIT', 'EUR_5Y_SWIT']].iloc[-2500:], rates_pca = True, momentum_size = None).plot_loadings(n=3)




### how to hedge the US front inflation overshoot
### use the mid curve real rate (and nominal) 
                                                                                                                                             
pca.PCA(pd.concat([usd_spot_ifs_test13[['USD_2Y_SWIT']].rename(lambda x: "USD_2Y_IFS", axis=1),
                   usd_spot_ifs_test13[['USD_3Y_SWIT']].rename(lambda x: "USD_3Y_IFS", axis=1),
                   usd_fwd_rr_test13[['3Y2Y']].rename(lambda x: "USD_RR_3Y2Y", axis=1),
                   usd_fwd_rr_test13[['5Y5Y']].rename(lambda x: "USD_RR_5Y5Y", axis=1),
                   usd_fwd_swap_test13[['3Y2Y']].rename(lambda x: "USD_NOM_SWAP_3Y2Y", axis=1),
                   usd_fwd_swap_test13[['5Y5Y']].rename(lambda x: "USD_NOM_SWAP_5Y5Y", axis=1)], 
                  axis=1).iloc[-2500:].dropna(axis=1), 
        rates_pca = True, momentum_size = None).plot_loadings(n=6)



pca.PCA(pd.concat([usd_spot_ifs_test13[['USD_2Y_SWIT']].rename(lambda x: "USD_2Y_IFS", axis=1),
                   usd_spot_ifs_test13[['USD_3Y_SWIT']].rename(lambda x: "USD_3Y_IFS", axis=1),
                   usd_fwd_rr_test13[['3Y2Y']].rename(lambda x: "USD_RR_3Y2Y", axis=1),
                   usd_fwd_rr_test13[['5Y5Y']].rename(lambda x: "USD_RR_5Y5Y", axis=1),
                   usd_fwd_swap_test13[['3Y2Y']].rename(lambda x: "USD_NOM_SWAP_3Y2Y", axis=1),
                   usd_fwd_swap_test13[['5Y5Y']].rename(lambda x: "USD_NOM_SWAP_5Y5Y", axis=1)], 
                  axis=1).iloc[-2500:].dropna(axis=1), 
        rates_pca = True, momentum_size = None).plot_loadings(n=6)



                                                                      

rvol = (data_swap - data_swap.shift(1)).rolling(5).std() * 100

momentum = (data_swap - data_swap.shift(5)) * 100

rdm = rvol*momentum

rvol_samples = rvol[['AUD_1Y_2Y','AUD_3Y_3Y','AUD_5Y_5Y', 'AUD_0Y_10Y', 'USD_1Y_2Y','USD_3Y_3Y','USD_5Y_5Y', 'USD_0Y_10Y']] * np.sqrt(252)

rvol_samples.iloc[-60:].plot()
rdm[['AUD_1Y_2Y','AUD_3Y_3Y','AUD_5Y_5Y', 'AUD_0Y_10Y', 'USD_1Y_2Y','USD_3Y_3Y','USD_5Y_5Y', 'USD_0Y_10Y']].iloc[-60:].plot()



(data_swap - data_swap.shift(1))['AUD_3Y_3Y'].iloc[-90:].std()*100* np.sqrt(252)



##############################################
# test 15 - curve term premium
##############################################

test15_aud_term_premium_data = pd.DataFrame({'3Y_3S10s': data_swap['AUD_3Y_10Y'] - data_swap['AUD_3Y_3Y'],
                                            '0Y_3S10s': data_swap['AUD_0Y_10Y'] - data_swap['AUD_0Y_3Y'],
                                            '5Y5Y': data_swap['AUD_5Y_5Y'],
                                             '3Y':data_swap['AUD_0Y_3Y']}).ffill().dropna(axis=0)


#set up main history
xs = test15_aud_term_premium_data['3Y']
ys = test15_aud_term_premium_data['3Y_3S10s']
    
start_x = xs.loc[xs.first_valid_index()]
start_y = ys.loc[ys.first_valid_index()]
start_dt = xs.first_valid_index().strftime('%Y-%m-%d')

last_x = xs.loc[xs.last_valid_index()]
last_y = ys.loc[ys.last_valid_index()]
last_dt = xs.last_valid_index().strftime('%Y-%m-%d')

# get regression beta for post gfc history
post_gfc_xs = xs[xs.index > '2011-01-01']
post_gfc_ys = ys[ys.index > '2011-01-01']
(beta_x, beta_0), (unfitted_xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(post_gfc_xs, post_gfc_ys)
post_gfc_term_premium = pd.DataFrame({'3Y': unfitted_xs,
                                      'Term Premium': pd.Series(data= residual_ys, index=unfitted_xs.index),
                                      'Spot 3s10s':test15_aud_term_premium_data[test15_aud_term_premium_data.index >'2011-01-01'] ['0Y_3S10s'],
                                      '5Y5Y':test15_aud_term_premium_data[test15_aud_term_premium_data.index >'2011-01-01'] ['5Y5Y']})



#set up the figure
test15_aud_term_premium_fig = plt.figure(figsize = (45, 15))
gs = gridspec.GridSpec(1, 3, height_ratios=[1], width_ratios=[1,1,1])
ax0 = plt.subplot(gs[0])

colors0 = mpl.dates.date2num(xs.index.to_pydatetime())
colors1 = 'lightgrey'

cmap0 = plt.cm.get_cmap('binary')
cmap1 = plt.cm.get_cmap('Reds')


#plot main
main_history_mappable = plot_main(ax0, xs, ys, "AUD 3Y", "AUD 3Y FWD 3s10s", colors0, cmap1)


#plot fits
fit_mappables = plot_scatter_fits(ax0, 
                                  [post_gfc_xs], 
                                  [post_gfc_ys],
                                  [''], 
                                  ['black'], 
                                  legend_loc = 2)
    
#plot post gfc xs and term premium
ax1 = plt.subplot(gs[1])
# colors0 = mpl.dates.date2num(post_gfc_term_premium['3Y'].index.to_pydatetime())
# colors1 = 'lightgrey'

# cmap0 = plt.cm.get_cmap('binary')
# cmap1 = plt.cm.get_cmap('Reds')

# post_gfc_term_premium_history_mappable = plot_main(ax1, post_gfc_term_premium['3Y'], post_gfc_term_premium['Term Premium'], "AUD 3Y", "AUD Curve Term Premium", colors0, cmap1)

post_gfc_term_premium[['5Y5Y', 'Term Premium']].plot(ax=ax1, secondary_y=['Term Premium'])

ax2 = plt.subplot(gs[2])    
post_gfc_term_premium[['Spot 3s10s', 'Term Premium']].plot(ax=ax2, secondary_y=['Term Premium'])




##############################################
# test 16 - USD historical 
##############################################

def distinct(seq): 
    checked = []
    for e in seq:
        if e not in checked:
            checked.append(e)
    return checked


def convert_fwd_to_curve_df(data, substract_col_index = None):
    #data is a series
    output = {}
    fwds = distinct([c.split('_')[1] for c in data.index])
    for fwd in fwds:
        col_for_fwd = [c for c in data.index if "_{}_".format(fwd) in c]
        output[fwd] = data.loc[col_for_fwd].rename(lambda x:x.split('_')[2])
    output = pd.DataFrame(output)
    output.index.name = data.name.strftime('%Y-%m-%d')
    if(substract_col_index is not None):
        for c in output.columns:
           output[c] = output[c] - output[output.columns[substract_col_index]]   
    
    return pd.DataFrame(output)
      

usd_swap_curve_cols = [c for c in data_usd_all.columns if ('USD_' in c) and c.split('_')[2] != '3M' ]
usd_swap_curve = data_usd_all[usd_swap_curve_cols]

usd_swap_curve_rvol_daily = (usd_swap_curve - usd_swap_curve.shift(1)).rolling(20).std() * 100 * np.sqrt(365)
#usd_swap_curve_rvol_daily = (usd_swap_curve - usd_swap_curve.shift(1)).rolling(60).std() * 100 * np.sqrt(365) 


usd_swap_ivol_curve_cols = [c for c in data_usd_all.columns if ('IVOL_' in c) and c.split('_')[2] != '3M' and c.split('_')[2] != '11Y' and c.split('_')[2] != '12Y' ]
usd_swap_ivol_curve = data_usd_all[usd_swap_ivol_curve_cols]

usd_swap_ivol_curve_in_yields = pd.DataFrame({c: convert_atmf_vol_to_yield(c.split('_')[1], usd_swap_ivol_curve[c]) for c in usd_swap_ivol_curve.columns})



#swap

plt.figure(figsize = (20, 15))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[3])



colormap = plt.cm.nipy_spectral

swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve.iloc[-1], substract_col_index=None)
ax0.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax0.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax0, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve.iloc[-1000], substract_col_index=None)
ax1.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax1.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax1, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve.iloc[-1500], substract_col_index=None)
ax2.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax2.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax2, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve.iloc[-1800], substract_col_index=None)
ax3.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax3.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax3, grid=True, fontsize=8)


ymax = max([ax0.get_ylim()[1], ax1.get_ylim()[1], ax2.get_ylim()[1], ax3.get_ylim()[1]])
ax0.set_ylim(ax0.get_ylim()[0], ymax)
ax1.set_ylim(ax1.get_ylim()[0], ymax)
ax2.set_ylim(ax2.get_ylim()[0], ymax)
ax3.set_ylim(ax3.get_ylim()[0], ymax)

#plt.tight_layout()

#swap vol



plt.figure(figsize = (20, 15))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[3])



colormap = plt.cm.nipy_spectral

swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve_rvol_daily.iloc[-1], substract_col_index=None)
ax0.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax0.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax0, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve_rvol_daily.iloc[-30], substract_col_index=None)
ax1.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax1.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax1, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve_rvol_daily.iloc[-500], substract_col_index=None)
ax2.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax2.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax2, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_curve_rvol_daily.iloc[-1000], substract_col_index=None)
ax3.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax3.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax3, grid=True, fontsize=8)


ymax = max([ax0.get_ylim()[1], ax1.get_ylim()[1], ax2.get_ylim()[1], ax3.get_ylim()[1]])
ax0.set_ylim(ax0.get_ylim()[0], ymax)
ax1.set_ylim(ax1.get_ylim()[0], ymax)
ax2.set_ylim(ax2.get_ylim()[0], ymax)
ax3.set_ylim(ax3.get_ylim()[0], ymax)
plt.tight_layout()




#swap ivol

plt.figure(figsize = (20, 15))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[3])



colormap = plt.cm.nipy_spectral

swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve.iloc[-1], substract_col_index=None)

ax0.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax0.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax0, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve.iloc[-1000], substract_col_index=None)
ax1.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax1.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax1, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve.iloc[-1500], substract_col_index=None)
ax2.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax2.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax2, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve.iloc[-1800], substract_col_index=None)
ax3.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax3.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax3, grid=True, fontsize=8)


ymax = max([ax0.get_ylim()[1], ax1.get_ylim()[1], ax2.get_ylim()[1], ax3.get_ylim()[1]])
ax0.set_ylim(ax0.get_ylim()[0], ymax)
ax1.set_ylim(ax1.get_ylim()[0], ymax)
ax2.set_ylim(ax2.get_ylim()[0], ymax)
ax3.set_ylim(ax3.get_ylim()[0], ymax)

#plt.tight_layout()
   



#swap ivol in yields

plt.figure(figsize = (20, 15))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[3])



colormap = plt.cm.nipy_spectral

swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve_in_yields.iloc[-1], substract_col_index=None)

ax0.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax0.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax0, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve_in_yields.iloc[-7], substract_col_index=None)
ax1.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax1.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax1, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve_in_yields.iloc[-2000], substract_col_index=None)
ax2.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax2.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax2, grid=True, fontsize=8)


swap_matrix_0 = convert_fwd_to_curve_df(usd_swap_ivol_curve_in_yields.iloc[-1800], substract_col_index=None)
ax3.set_prop_cycle(cycler(color=[colormap(i) for i in np.linspace(0, 1, len(swap_matrix_0.columns))]))
ax3.set_xticks(range(len(swap_matrix_0.index)))
swap_matrix_0.plot(ax=ax3, grid=True, fontsize=8)


ymax = max([ax0.get_ylim()[1], ax1.get_ylim()[1], ax2.get_ylim()[1], ax3.get_ylim()[1]])
ax0.set_ylim(ax0.get_ylim()[0], ymax)
ax1.set_ylim(ax1.get_ylim()[0], ymax)
ax2.set_ylim(ax2.get_ylim()[0], ymax)
ax3.set_ylim(ax3.get_ylim()[0], ymax)

#plt.tight_layout()
 


usd_swap_curve['USD_3Y_3Y']
usd_swap_ivol_curve_in_yields['IVOL_3Y_3Y']




# t15 2y1y vs ifs 

t15_data = pd.DataFrame({"SW2Y1Y":usd_fwd_swap_test13['2Y1Y'],
                         "RR2Y1Y":usd_fwd_rr_test13['2Y1Y'],
                         "IFS3Y":usd_spot_ifs_test13['USD_3Y_SWIT'],
                         "IFS5Y":usd_spot_ifs_test13['USD_5Y_SWIT']                         
                         })


pca.PCA(t15_data[t15_data.index > '2008-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(t15_data[t15_data.index > '2019-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3)

pca.PCA(t15_data[t15_data.index > '2020-07-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3)


t15_data_corr = pd.DataFrame(rolling_apply(t15_data[["SW2Y1Y","IFS3Y","IFS5Y"]][t15_data.index > '2020-07-01'], 10, get_flattened_corr)).transpose()

t15_data_corr = t15_data_corr[['SW2Y1Y_vs_IFS3Y', 'SW2Y1Y_vs_IFS5Y']]



# test 20
if(2 > 3):
    
    AU_cols =  [c for c in data_swap2.columns if ('AUD_0Y' in c) and ('M' not in c)]
    
    
    ((data_swap2['AUD_0Y_10Y'] + data_swap2['AUD_0Y_3Y'])/2 - data_swap2['AUD_0Y_7Y']).plot()
    
    ((data_swap2['AUD_0Y_10Y'] + data_swap2['AUD_0Y_3Y'])/2).iloc[-1500:].plot()
    ((data_swap2['AUD_1Y_10Y'] + data_swap2['AUD_1Y_3Y'])/2).iloc[-1500:].plot()
    
    #AU_cols.sort()
    data_aud = data_swap2[AU_cols].dropna()
    
    pca.PCA(data_aud,rates_pca=True, 
            sample_range = ('2013-01-01', '2019-12-01')).plot_loadings(n=5)
    
    pca.PCA(data_aud,rates_pca=True, 
            sample_range = ('2021-01-01', '2021-06-23')).plot_loadings(n=5)
    

    pca.PCA(data_aud[['AUD_0Y_3Y', 'AUD_0Y_4Y', 'AUD_0Y_5Y','AUD_0Y_7Y','AUD_0Y_10Y']],rates_pca=True, 
        sample_range = ('2021-01-01', '2021-06-23')).plot_loadings(n=5)

    
    pca.PCA(data_aud[['AUD_0Y_3Y', 'AUD_0Y_4Y', 'AUD_0Y_5Y','AUD_0Y_7Y','AUD_0Y_10Y']],rates_pca=True, 
        sample_range = ('2013-01-01', '2019-12-01')).plot_loadings(n=5)
    
    
    for k, (mr, p) in \
        create_combo_mr(data_swap2[['AUD_0Y_3Y', 'AUD_0Y_5Y', 'AUD_0Y_10Y']], 
                            elements = 3, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2013-01-01', '2019-12-01')).items():
                
        mr.create_mr_info_fig((18, 7), pca_obj = p)
    
     for k, (mr, p) in \
        create_combo_mr(data_swap2[['AUD_0Y_3Y', 'AUD_0Y_5Y', 'AUD_0Y_10Y']], 
                            elements = 3, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2013-01-01', '2015-12-01')).items():
                
        mr.create_mr_info_fig((18, 7), pca_obj = p)
    
     for k, (mr, p) in \
        create_combo_mr(data_swap2[['AUD_0Y_3Y', 'AUD_0Y_5Y', 'AUD_0Y_10Y']].iloc[-3000:], 
                            elements = 3, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2021-01-01', '2021-06-20')).items():
                
        mr.create_mr_info_fig((18, 7), pca_obj = p)
 
     
     plt.contourf(data_swap2['AUD_0Y_5Y'].iloc[-1000:].values, 
                  ((data_swap2['AUD_0Y_10Y']+data_swap2['AUD_0Y_2Y'])/2).iloc[-1000:].values,
                  (data_swap2['AUD_0Y_10Y']-data_swap2['AUD_0Y_2Y']).iloc[-1000:].values)

#test 21 sbins

if (2> 3):
    data_21 = data_gov[['USD_0Y_2Y', 'USD_0Y_5Y', 'USD_0Y_10Y', 'USD_0Y_30Y']].iloc[:-1]
    data_21_next_day_rtn = (data_21 - data_21.shift(1)).shift(-1).rename(lambda x: '{}_return'.format(x), axis=1)
    data_21  = pd.concat([data_21, data_21_next_day_rtn], axis=1).dropna()
    
    from statistics.mr import sbins
    data_21_sbin = sbins.sbins(data_21.iloc[-90:], 'USD_0Y_10Y', 10)
    
    data_21_sbin.plot_bin_mean_variance_for_col('USD_0Y_10Y_return')
    data_21_sbin.plot_bin_mean_variance_for_col('USD_0Y_30Y_return')
    
    from statsmodels.distributions.empirical_distribution import ECDF
    
    
    ecdf_dist = ECDF(data_21['USD_0Y_10Y_return'].iloc[-30:])
    plt.plot(ecdf_dist.x, ecdf_dist.y)
    plt.show()
    
    pdf_dist_y = np.gradient(ecdf_dist.y, ecdf_dist.x)
    plt.plot(ecdf_dist.x, pdf_dist_y)    
    plt.show()

    
    
    #
    from statistics.mr import dist
    
    
    data_21['USD_0Y_10Y'].iloc[-40:].plot()
    
    data_21['USD_0Y_10Y'].iloc[-40:].diff().sum()
    
    d1 = data_21.iloc[-60:][data_21['USD_0Y_10Y']>1.6]['USD_0Y_10Y_return'].rename('above_150')
    d2 =data_21.iloc[-60:][(data_21['USD_0Y_10Y'] < 1.6) & (data_21['USD_0Y_10Y'] > 1.4) ]['USD_0Y_10Y_return'].rename('above_140to160')
    d2 =data_21.iloc[-60:][(data_21['USD_0Y_10Y'] > 1.4) ]['USD_0Y_10Y_return'].rename('above_140to160')
    
    
    dist.plot_best_fit_pdf(d1,bin_size=20,xaxis_labels='US10 daily return')
    
    dist.plot_best_fit_pdf(d2,bin_size=20,xaxis_labels='US10 daily return')
    
    # Load data from statsmodels datasets
    #data = pd.Series(sm.datasets.elnino.load_pandas().data.set_index('YEAR').values.ravel())
    
    
    cad_fly = data_swap[['CAD_0Y_2Y', 'CAD_2Y_2Y', 'CAD_5Y_5Y']]
    pca.PCA(cad_fly.iloc[-1250:],rates_pca = True).plot_loadings(n=3)
    pca.PCA(cad_fly.iloc[-90:],rates_pca = True).plot_loadings(n=3)
    pca.PCA(cad_fly.iloc[-2000:-1750],rates_pca = True).plot_loadings(n=3)
    pca.PCA(cad_fly.iloc[-2200:-1900],rates_pca = True).plot_loadings(n=3)
    pca.PCA(cad_fly.iloc[-90:],rates_pca = True).plot_loadings(n=3)
    
    cad_fly.iloc[-2200:-1900].apply(lambda x: x['CAD_0Y_2Y'] * -0.5 + x['CAD_2Y_2Y'] * -0.66 + x['CAD_5Y_5Y'] * + 0.56, axis=1).plot()
    cad_fly.iloc[-1900:-1700].apply(lambda x: x['CAD_0Y_2Y'] * -0.5 + x['CAD_2Y_2Y'] * -0.66 + x['CAD_5Y_5Y'] * + 0.56, axis=1).plot()
    cad_fly.iloc[-1900:-1700].apply(lambda x: - x['CAD_0Y_2Y'] + x['CAD_5Y_5Y'], axis=1).plot()
    cad_fly.iloc[-1900:-1700].apply(lambda x: - x['CAD_2Y_2Y'] + x['CAD_5Y_5Y'], axis=1).plot()
    
    cad_fly.iloc[-2200:].apply(lambda x: - x['CAD_2Y_2Y'] + x['CAD_5Y_5Y'], axis=1).plot()
    
    
    cad_curve = pd.DataFrame({'CAD_2Y_1Y':data_swap['CAD_0Y_3Y'] * 3  - data_swap['CAD_0Y_2Y'] *2,
                             'CAD_4s8s': data_swap['CAD_0Y_8Y'] - data_swap['CAD_0Y_4Y']})
    
    
    pca.PCA(cad_curve.iloc[-2000:-1750],rates_pca = True).plot_loadings(n=2)    
    pca.PCA(cad_curve.iloc[-1900:-1750],rates_pca = True).plot_loadings(n=2)
    pca.PCA(cad_curve.iloc[-2000:-1900],rates_pca = True).plot_loadings(n=2)
    pca.PCA(cad_curve.iloc[-2000:],rates_pca = True).plot_loadings(n=2)
    pca.PCA(cad_curve.iloc[-60:],rates_pca = True).plot_loadings(n=2)
    
    cad_curve.iloc[-2200:].plot(secondary_y=['CAD_2Y_1Y'])
    
    
    (data_swap['CAD_5Y_5Y'] - data_swap['CAD_3Y_2Y']).dropna().plot()    
    (data_swap['CAD_5Y_5Y'] - data_swap['CAD_2Y_2Y']).dropna().plot()
    
    (data_swap['CAD_0Y_8Y'] - data_swap['CAD_0Y_4Y']).dropna().plot()
    (data_swap['CAD_0Y_7Y'] - data_swap['CAD_0Y_4Y']).dropna().plot()
    (data_swap['CAD_0Y_6Y'] - data_swap['CAD_0Y_4Y']).dropna().plot()    
    
    pca.PCA(data_swap[['CAD_0Y_4Y', 'CAD_0Y_5Y', 'CAD_0Y_6Y', 'CAD_0Y_7Y', 'CAD_0Y_8Y', 'CAD_0Y_9Y', 'CAD_0Y_10Y']],rates_pca=True).plot_loadings(n=3)
    pca.PCA(data_swap[['CAD_0Y_4Y', 'CAD_0Y_5Y', 'CAD_0Y_6Y', 'CAD_0Y_7Y', 'CAD_0Y_8Y', 'CAD_0Y_9Y', 'CAD_0Y_10Y']].iloc[-90:],
            rates_pca=True).plot_loadings(n=3)
    
    
    
    cad_fly = pd.concat([data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna(), data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna().diff().pow(2).rename(lambda x: '{}_vol'.format(x), axis=1)], axis=1).dropna()
    cad_fly.plot(secondary_y=['CAD_2Y_1Y', 'CAD_0Y_8Y'])

    
    data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna().diff().pow(2).apply(lambda x: x['CAD_2Y_1Y']-x['CAD_0Y_8Y'], axis=1)


    pd.concat([data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna(), 
    
    
    ##
    from statistics.mr import sbins               
    
    CAD_2y1ys8s = data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna().apply(lambda x: - x['CAD_2Y_1Y'] + x['CAD_0Y_8Y'], axis=1)
    CAD_2y1ys8s_rtns = (CAD_2y1ys8s - CAD_2y1ys8s.shift(1)).shift(-1)
    
    cad_curve = pd.DataFrame({'VOL_2y1ys8s': data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna().diff().pow(2).apply(lambda x: - x['CAD_2Y_1Y'] + x['CAD_0Y_8Y'], axis=1),
                               'CAD_2y1ys8s_rtns': CAD_2y1ys8s_rtns                  
                               }).dropna()
    
    cad_curve['VOL_2y1ys8s'].plot.hist(bin=50)
    
    cad_curve_sbin = sbins.sbins(cad_curve[(cad_curve['VOL_2y1ys8s'] > -0.002) & (cad_curve['VOL_2y1ys8s'] < 0.002)], 'VOL_2y1ys8s', 20)
    cad_curve_sbin.plot_bin_mean_variance_for_col('CAD_2y1ys8s_rtns')

    
    #plot_curve()
    from statistics.mr import tools as mrt
    
    mrt.plot_fly(data_swap[['USD_0Y_5Y', 'USD_0Y_10Y', 'USD_0Y_30Y']].dropna().iloc[-2500:])
    
    
    
    mrt.plot_curve(data_swap[['USD_0Y_5Y', 'USD_0Y_10Y']].dropna().iloc[-1600:-1400], curve=True)
    mrt.plot_curve(data_swap[['USD_0Y_5Y', 'USD_0Y_10Y']].dropna().iloc[-1600:-1500], curve=True)
    mrt.plot_curve(data_swap[['USD_0Y_5Y', 'USD_0Y_30Y']].dropna().iloc[-125:], curve=True)
    
    
    mrt.plot_curve(data_swap[['USD_0Y_5Y', 'USD_0Y_10Y']].dropna().iloc[-1900:-1700], curve=True)
    mrt.plot_curve(data_swap[['USD_0Y_10Y', 'USD_0Y_30Y']].dropna().iloc[-1900:-1700], curve=True)
    
    mrt.plot_curve(data_swap[['USD_0Y_5Y', 'USD_0Y_10Y']].dropna().iloc[-270:], curve=True)
    
   
    pca.PCA(data_swap2[['AUD_5Y_5Y', 'GBP_5Y_5Y']].diff().iloc[-200:],rates_pca=True).plot_loadings(n=3)
    
    
    pca.PCA(data_swap2[['AUD_5Y_5Y', 'GBP_5Y_5Y']].iloc[-0:],rates_pca=True).plot_loadings(n=3)
    
    
    pca.PCA(data_swap2[['AUD_0Y_5Y', 'AUD_0Y_10Y']].diff().iloc[-50:],rates_pca=True).plot_loadings(n=3)
    
    mrt.plot_curve(data_swap[['AUD_0Y_5Y', 'AUD_0Y_10Y']].dropna().iloc[-100:], curve=True)
    
    ##
    from statistics.mr import bins
    from statistics.mr import sbins  
    from statistics.mr import tools as mrt
    
    pca.PCA(data_swap[['AUD_5Y_5Y', 'GBP_5Y_5Y']].diff().iloc[-200:],rates_pca=True).plot_loadings(n=3)
    pca.PCA(data_swap[['AUD_5Y_5Y', 'GBP_5Y_5Y']].iloc[-200:],rates_pca=True).plot_loadings(n=3)
    
    
    for k, (mr, p) in \
        create_combo_mr(data_swap[['AUD_5Y_5Y', 'USD_5Y_5Y']].iloc[-5000:].dropna(axis=0), 
                            elements = 2, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2018-01-01', '2019-11-01')).items():
                
        mr.create_mr_info_fig((18, 7), pca_obj = p)
  
    
  for k, (mr, p) in \
      create_combo_mr(data_swap[['JPY_1Y_1Y', 'JPY_3Y_3Y', 'JPY_10Y_10Y']].iloc[-3300:].dropna(axis=0), 
                          elements = 2, resample_freq= 'W', 
                          include_normal_weighted_series=False, 
                          matrixoveride = None,
                          sample_range = ('2010-01-01', '2019-11-01')).items():
              
      mr.create_mr_info_fig((18, 7), pca_obj = p)
  
    
        
    
    
    #########
    mrs = create_combo_mr(data_swap[['CAD_5Y_5Y', 'USD_0Y_30Y']].iloc[-3000:], 
                            elements = 2, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2013-01-01', '2016-11-01'))
    
    
    mrs = create_combo_mr(data_gov[['USD_0Y_10Y']].iloc[-5000:], 
                            elements = 2, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2013-01-01', '2016-11-01'))
    
    
    
    bins.create_mr_diagnosis(data_gov['USD_0Y_10Y'].iloc[-5000:], 20, min_num_of_bins = 20, resample_freq = 'W').plot_distribution_diagnosis((14, 8))
    
    bins.create_mr_diagnosis((data_gov['USD_0Y_30Y'] - data_gov['USD_0Y_5Y']).iloc[-5000:], 20, min_num_of_bins = 20, resample_freq = 'W').plot_distribution_diagnosis((14, 8))
    
    
    bins.create_mr_diagnosis(data_gov['USD_0Y_10Y'].iloc[-20:], 20, min_num_of_bins = 20, resample_freq = 'B').plot_distribution_diagnosis((14, 8))
    
    
    # first object is the mr 
    mrs['CAD_5Y_5Y_USD_0Y_30Y_PCW'][0].create_mr_info_fig((18, 7), pca_obj = mrs['CAD_5Y_5Y_USD_0Y_30Y_PCW'][1])
    
    # call its ou to simulate   
    sim, distrib = mrs['CAD_5Y_5Y_USD_5Y_5Y_PCW'][0].ou.simulate_with_model_parameters(k=5000, target=-0.25, stop=-0.9)
    
    ########
    c = [c for c in data_swap2.columns if 'USD_0Y' in c or 'CAD_0Y' in c]
    pca.PCA(data_swap2[c].iloc[-3000:],rates_pca=True, sample_range = ('2017-01-01', '2019-11-01')).plot_loadings(n=6)
    
    
    
    
        
    
    data_21 = data_gov[['USD_0Y_2Y', 'USD_0Y_5Y', 'USD_0Y_10Y', 'USD_0Y_30Y']].iloc[:-1]
    data_21_next_day_rtn = (data_21 - data_21.shift(1)).shift(-1).rename(lambda x: '{}_return'.format(x), axis=1)
    data_21  = pd.concat([data_21, data_21_next_day_rtn], axis=1).dropna()
    
    data_21_sbin = sbins.sbins(data_21.iloc[-90:], 'USD_0Y_10Y', 10)
    
    data_21_sbin.plot_bin_mean_variance_for_col('USD_0Y_10Y_return')
    data_21_sbin.plot_bin_mean_variance_for_col('USD_0Y_30Y_return')
    
    
    
    
    

#test 22

if (2> 3):    
    mrt.plot_curve(data_swap[['AUD_0Y_10Y', 'USD_0Y_30Y']].dropna().iloc[-60:-1], curve=True)    
    


#test 23

if(2>3):
    
    from statistics.mr import helper
    from statistics.mr import tools as mrt

    
    ds = (data_stirfut.ERU3 *0.28 -0.65 * data_stirfut.ERZ3 + 0.72 * data_stirfut.ERH4 -0.35 * data_stirfut.ERM4)*100
    
    ds.name = '0.28 * ERU3 -0.65 * ERZ3 + 0.72 * ERH4 -0.35 * ERM4'
    
    mr = bins.create_mr_diagnosis(ds.iloc[-90:], 20, min_num_of_bins = 10, resample_freq = 'B')
    mr.bin_diagnosis[['bin_l_edge','bin_r_edge']]
    
    mr.plot_distribution_diagnosis((14, 8))
    
    mr.create_mr_info_fig((18, 10))
    
    
    sim_path, sim_result = mr.simulate_paths_nonparametrically(n=60,k=2)
    mr.estimate_conditional_transition_denisty(n=60, k=2, dt=1, x0_override=None, target=None,stop=None, mm_factor=0, mm_window=5)
    mr.create_markov_info_fig((18, 10), horizons=[5,10,20])
    
   


##########################################################
# study 4.0.1 CAD analysis
##########################################################


if(0>1):

    
    cad_col = [c for c in data_cad_swap.columns if 'CAD' in c and ('_0Y_' not in c or '0Y_1Y' in c)]
    usd_col = [c for c in data_cad_swap.columns if 'USD' in c and ('_0Y_' not in c or '0Y_1Y' in c)]

    
    data_cad_swap_s401 = data_cad_swap[cad_col].rename(lambda c: c.replace("CAD_",""), axis="columns")
    data_usd_swap_s401 = data_cad_swap[usd_col].rename(lambda c: c.replace("USD_",""), axis="columns")
    data_cadusd_spread_s401 = data_cad_swap_s401 - data_usd_swap_s401
    

    mrt.plotCurvehist(data_cad_swap_s401, lookback=12, numberOfHistory=4, title = 'CAD Swap')
    mrt.plotCurvehist(data_usd_swap_s401,lookback=12, numberOfHistory=4, title = 'USD SOFR Swap')
    mrt.plotCurvehist(data_cadusd_spread_s401,lookback=12, numberOfHistory=4, title = 'CADUSD Spread')
    
        
    
    n = -48
    
    cad_swap_pca = pca.PCA(data_cad_swap_s401.iloc[n:].dropna(axis=1), rates_pca = True, momentum_size = None)
    cad_swap_pca.plot_loadings(n=4)    
    cad_swap_pca.plot_valuation_history(48)
        
    
    

    for k, (mr, p) in \
        create_combo_mr(data_cad_swap_s401[['10Y_2Y', '2Y_1Y']].iloc[-3300:].dropna(axis=0), 
                            elements = 2, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2010-01-01', '2019-11-01')).items():
                
        mr.create_mr_info_fig((18, 7), pca_obj = p)
    
    

    for k, (mr, p) in \
        create_combo_mr(data_cad_swap_s401[['10Y_2Y', '2Y_1Y']].iloc[-3300:].dropna(axis=0), 
                            elements = 2, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2022-01-01', '2022-11-01')).items():
                
        mr.create_mr_info_fig((18, 7), pca_obj = p)
        

    for k, (mr, p) in \
        create_combo_mr(data_cad_swap_s401[['1Y_1Y', '4Y_1Y', '10Y_2Y']].iloc[-3300:].dropna(axis=0), 
                            elements = 3, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2022-01-01', '2022-11-01')).items():
                
        mr.create_mr_info_fig((18, 7), pca_obj = p)



    for k, (mr, p) in \
        create_combo_mr(data_cad_swap_s401[['1Y_1Y', '4Y_1Y', '10Y_2Y']].iloc[-3300:].dropna(axis=0), 
                            elements = 3, resample_freq= 'W', 
                            include_normal_weighted_series=False, 
                            matrixoveride = None,
                            sample_range = ('2010-01-01', '2019-11-01')).items():
                
        mr.create_mr_info_fig((18, 7), pca_obj = p)




##########################################################
# study 600 spot swap spread scenario analysis
##########################################################

if(0>1):

    fltCol = lambda df, strp: df[df.columns[df.columns.str.contains(strp)]] 
    fltT = lambda df, tr: df[(df.index >= tr[0]) & (df.index <= tr[1])] 
    
    def plot_ss_rate(df, w, h, tGridType='both', title=None):
        
        f = plt.figure(figsize = (w, h))
        gs = gridspec.GridSpec(4,1,height_ratios=[1,1,1,1])
        ax0 = plt.subplot(gs[0])
        ax1 = plt.subplot(gs[1])
        ax2 = plt.subplot(gs[2])
        ax3 = plt.subplot(gs[3])

        fltCol(df,'SS').plot(ax=ax0 , fontsize = 9)
        ax0.xaxis.grid(True,which=tGridType)
        
        fltCol(df,'GOV').plot(ax=ax1 , fontsize = 9)
        ax1.xaxis.grid(True,which=tGridType)

        (fltCol(df,'SS')-fltCol(df,'SS').iloc[0]).plot(ax=ax2, fontsize = 9)
        ax2.xaxis.grid(True,which=tGridType)
        
        (fltCol(df,'GOV')-fltCol(df,'GOV').iloc[1]).plot(ax=ax3, fontsize = 9)
        ax3.xaxis.grid(True,which=tGridType)
        
        f.suptitle(title)
        
        f.tight_layout()
        return f
    
    
    
    #SS
    data_4model = data_ss #not yet use the combined data which has curve
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
    
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = sorted(list(set(data_4model.columns.map(lambda x: x[3:]))))
    tenors.sort(key = lambda x : int(x.replace('Y_SS','').replace('_','')))
    tenors = ['_2Y_SS', '_3Y_SS', '_5Y_SS',  '_10Y_SS', '_20Y_SS', '_30Y_SS']

    #countries spot data
    country_ss_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in tenors],[])] for cn in countries}

    
    #rate x 100
    data_4model = data_gov * 100  #not yet use the combined data which has curve 
    data_4model = data_4model.loc[pd.isnull(data_4model.index) == False]
     
    # c
    countries = list(set(data_4model.columns.map(lambda x: x[:3])))
    #tenors
    tenors = list(set(data_4model.columns.map(lambda x: x[3:])))
    tenors = ['_0Y_2Y', '_0Y_3Y', '_0Y_5Y', '0Y_10Y','0Y_20Y', '0Y_30Y']
     
    #countries spot data
    country_rate_data = {cn:data_4model[sum([[c for c in data_4model.columns if (st in c) and (cn in c)] for st in tenors],[])] for cn in countries}
    
    
    #AUD
    
    cn = 'AUS'
    _s600_spot_ss = country_ss_data[cn].rename(lambda c: c.replace("_SS","").replace(cn,"SS"), axis="columns")
    cn = 'AUD'
    _s600_spot_rate = country_rate_data[cn].rename(lambda c: c.replace("_0Y_","_").replace(cn,"GOV"), axis="columns")
    _s600_spot = pd.concat([_s600_spot_ss, _s600_spot_rate], axis=1)   
    #_s600_spot = _s600_spot.loc[:, ~_s600_spot.columns.str.contains('30Y')]
    _s600_spot_aud = _s600_spot.dropna(axis=0).dropna(axis=1)

  
    tp='COV'
    s6_aud_spot_pca_long = pca.PCA(_s600_spot_aud, matrixtype=tp, rates_pca = True, momentum_size = None)
    s6_aud_spot_pca_long.plot_loadings(n=6)
    
    
    #USA
    
    cn = 'USA'
    _s600_spot_ss = country_ss_data[cn].rename(lambda c: c.replace("_SS","").replace(cn,"SS"), axis="columns")
    cn = 'USD'
    _s600_spot_rate = country_rate_data[cn].rename(lambda c: c.replace("_0Y_","_").replace(cn,"GOV"), axis="columns")
    _s600_spot = pd.concat([_s600_spot_ss, _s600_spot_rate], axis=1)   
    #_s600_spot = _s600_spot.loc[:, ~_s600_spot.columns.str.contains('30Y')]
    _s600_spot_usd = _s600_spot.dropna(axis=0).dropna(axis=1)
    
  
    s6_usd_spot_pca_long = pca.PCA(_s600_spot_usd, matrixtype=tp, rates_pca = True, momentum_size = None)

  
    #USD SOFR
    
    cn = 'USD'
    _s600_spot_ss = country_ss_data[cn].rename(lambda c: c.replace("_SS","").replace(cn,"SS"), axis="columns")
    cn = 'USD'
    _s600_spot_rate = country_rate_data[cn].rename(lambda c: c.replace("_0Y_","_").replace(cn,"GOV"), axis="columns")
    _s600_spot = pd.concat([_s600_spot_ss, _s600_spot_rate], axis=1)   
    #_s600_spot = _s600_spot.loc[:, ~_s600_spot.columns.str.contains('30Y')]
    _s600_spot_usdsofr = _s600_spot.dropna(axis=0).dropna(axis=1)
    
    s6_usdsofr_spot_pca_long = pca.PCA(_s600_spot_usdsofr, matrixtype=tp, rates_pca = True, momentum_size = None)






    # citi supplymentary
    
    data_citiasw = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_CITIASW_historical.csv', skiprows=2, skip_blank_lines=True)  
    data_citiasw = clean_citi_data(data_citiasw, asw_col_name_cleaner).ffill()
    fn_citi_filter = lambda p, cols: [c for c in cols if p in c]
    
    #USD Citi
        
    cn = 'USA'
    asw_cols_ptn = cn + '_ASW'
    yld_cols_ptn = cn + '_YIELD' 
    cn_cols = fn_citi_filter(cn, data_citiasw.columns)
    _s600_citi_spot_usa = data_citiasw[cn_cols].rename(lambda c: c.replace(asw_cols_ptn,"SS").replace(yld_cols_ptn,"GOV"), axis="columns") \
                        .dropna(axis=0).dropna(axis=1)                       
    _s600_citi_spot_usa[fn_citi_filter("GOV", _s600_citi_spot_usa.columns)] = \
        _s600_citi_spot_usa[fn_citi_filter("GOV", _s600_citi_spot_usa.columns)] * 100
    _s600_citi_spot_usa[fn_citi_filter("SS", _s600_citi_spot_usa.columns)] = \
        _s600_citi_spot_usa[fn_citi_filter("SS", _s600_citi_spot_usa.columns)] * -1
                
        #-1 means the ASW direction to swap spread
    
    pca.PCA(_s600_citi_spot_usa[fn_citi_filter("SS", _s600_citi_spot_usa.columns)], matrixtype=tp, rates_pca = True, momentum_size = None) \
        .loadings['Loading1']
    
    
    

    #GBR Citi
    
    cn = 'GBR'
    asw_cols_ptn = cn + '_ASW'
    yld_cols_ptn = cn + '_YIELD' 
    cn_cols = fn_citi_filter(cn, data_citiasw.columns)
    _s600_citi_spot_gbr = data_citiasw[cn_cols].rename(lambda c: c.replace(asw_cols_ptn,"SS").replace(yld_cols_ptn,"GOV"), axis="columns") \
                        .dropna(axis=0).dropna(axis=1)
    _s600_citi_spot_gbr[fn_citi_filter("GOV", _s600_citi_spot_gbr.columns)] = \
        _s600_citi_spot_gbr[fn_citi_filter("GOV", _s600_citi_spot_gbr.columns)] * 100
    _s600_citi_spot_gbr[fn_citi_filter("SS", _s600_citi_spot_gbr.columns)] = \
        _s600_citi_spot_gbr[fn_citi_filter("SS", _s600_citi_spot_gbr.columns)] * -1
        #-100 means upscale to bp and turn the ASW direction to swap spread
    

    #citi
    cn = 'AUS'
    asw_cols_ptn = cn + '_ASW'
    yld_cols_ptn = cn + '_YIELD' 
    cn_cols = fn_citi_filter(cn, data_citiasw.columns)
    _s600_citi_spot_aus = data_citiasw[cn_cols].rename(lambda c: c.replace(asw_cols_ptn,"SS").replace(yld_cols_ptn,"GOV"), axis="columns") \
                        .dropna(axis=0).dropna(axis=1)
    _s600_citi_spot_aus[fn_citi_filter("GOV", _s600_citi_spot_aus.columns)] = \
        _s600_citi_spot_gbr[fn_citi_filter("GOV", _s600_citi_spot_aus.columns)] * 100
    _s600_citi_spot_aus[fn_citi_filter("SS", _s600_citi_spot_aus.columns)] = \
        _s600_citi_spot_aus[fn_citi_filter("SS", _s600_citi_spot_aus.columns)] * -1
        #-100 means upscale to bp and turn the ASW direction to swap spread

    pca.PCA(_s600_citi_spot_aus[fn_citi_filter("SS", _s600_citi_spot_aus.columns)], matrixtype=tp, rates_pca = True, momentum_size = None) \
        .loadings['Loading1']
    
    



    #FRA
    
    cn = 'FRA'
    _s600_spot_ss = country_ss_data[cn].rename(lambda c: c.replace("_SS","").replace(cn,"SS"), axis="columns")
    _s600_spot_ss = _s600_spot_ss.loc[:, ~_s600_spot_ss.columns.str.contains('3Y')]
    

    cn = 'FRA'
    _s600_spot_rate = country_rate_data[cn].rename(lambda c: c.replace("_0Y_","_").replace(cn,"GOV"), axis="columns")
    _s600_spot_rate = _s600_spot_rate.loc[:, ~_s600_spot_rate.columns.str.contains('3Y')]
    
    _s600_spot = pd.concat([_s600_spot_ss, _s600_spot_rate], axis=1)   
    
    _s600_spot_fra = _s600_spot.dropna(axis=0).dropna(axis=1)
    
  
    
    # AUS
    # total
    sr = ('2007-01-01', '2050-01-01')
    pca.PCA(fltT(_s600_spot_aud, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4)
    plot_ss_rate(fltT(_s600_spot_aud, sr), 20, 10, tGridType='major', title='AUD')
    
    
    # GFC AUD - risk off
    # SS: Lower flatter 
    # rates: lower steeper
    sr = ('2008-01-01', '2009-01-01')
    pca.PCA(fltT(_s600_spot_aud, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_aud, sr), 8, 10, tGridType='major', title='AUD')
              
    
    # GFC AUD 2 - intial recovery risk on
    # SS: twist steeper
    # rates: higher parallel
    sr = ('2009-01-01', '2010-01-01')
    pca.PCA(fltT(_s600_spot_aud, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_aud, sr), 8, 10, tGridType='major', title='AUD')
        
    # POST GFC taper tantrum AUD 3 
    # SS: lower flatter
    # Rates: higher steeper

    sr = ('2013-01-01', '2014-01-01')
    pca.PCA(fltT(_s600_spot_aud, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_aud, sr), 8, 10, tGridType='major', title='AUD')
        
    
    # Post tape tantrum secular stagnation AUD 
    #SS: lower steeper
    #Rates lower: flatter

    sr = ('2014-01-01', '2020-01-01')
    pca.PCA(fltT(_s600_spot_aud, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_aud, sr), 8, 10, tGridType='major', title='AUD')
  
  
    # Post covid recovery 
    #SS: Higher parallel	
    #Rate: Higher Flatter	

    sr = ('2021-10-01', '2024-01-01')
    pca.PCA(fltT(_s600_spot_aud, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_aud, sr), 8, 10, tGridType='major', title='AUD')
          


    #USD
    # total
    sr = ('2007-01-01', '2050-01-01')
    pca.PCA(fltT(_s600_spot_usd, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_usd, sr), 20, 10, tGridType='major', title='USD')
    
    # POST GFC taper tantrum USD
    # SS: twisted steeper
    # Rates: twisted steeper


    sr = ('2013-01-01', '2014-01-01')
    pca.PCA(fltT(_s600_spot_usd, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_usd, sr), 8, 10, tGridType='major', title='USD')
        
     
    
    # Post tape tantrum secular stagnation USD 
    #SS: lower flatter
    #rates: lower flatter


    sr = ('2014-01-01', '2017-01-01')
    pca.PCA(fltT(_s600_spot_usd, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_usd, sr), 8, 10, tGridType='major', title='USD')
  

    # 2018 late cycle USD 
    #SS: lower flatter
    #Rates: lower steeper


    sr = ('2018-06-01', '2020-01-01')
    pca.PCA(fltT(_s600_spot_usd, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_usd, sr), 8, 10, tGridType='major', title='USD')
  


    # 2020 march squeez 
    #SS: lower flatter
    #Rates: higher steeper

    sr = ('2020-01-01', '2020-05-01')
    pca.PCA(fltT(_s600_spot_usd, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_usd, sr), 8, 10, tGridType='major', title='USD')
  
  
    # 2022 hike 
    #SS: lower flatter
    #Rates: higher flatter

    sr = ('2022-01-01', '2024-01-01')
    pca.PCA(fltT(_s600_spot_usdsofr, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_usdsofr, sr), 8, 10, tGridType='major', title='USD')
  
  
    #FRA/EUR
    
    
    # total
    sr = ('2007-01-01', '2050-01-01')
    pca.PCA(fltT(_s600_spot_fra, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_fra, sr), 20, 10, tGridType='major', title='FRA')
         
    
    # GFC risk off - EUR
    # SS: twisted flatter (2s higher, 30s lower)  
    # rates: lower steeper
    sr = ('2008-01-01', '2009-01-01')
    pca.PCA(fltT(_s600_spot_fra, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_fra, sr), 8, 10, tGridType='major', title='FRA')
              
    
    # EUROZone sov criss risk off - EUR
    # SS: higher flatter  
    # rates: lower steeper
    sr = ('2011-09-01', '2012-01-01')
    pca.PCA(fltT(_s600_spot_fra, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_fra, sr), 8, 10, tGridType='major', title='FRA')
              
    
    # Low reflation
    #SS: higher steeper
    #rate: lower flatter
    sr = ('2014-01-01', '2020-01-01')
    pca.PCA(fltT(_s600_spot_fra, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_fra, sr), 8, 10, tGridType='major', title='FRA')
              
    
    # 2020 march squeez - EUR
    #SS: lower flatter
    #Rates: lower steeper

    sr = ('2020-01-01', '2020-05-01')
    pca.PCA(fltT(_s600_spot_fra, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_fra, sr), 8, 10, tGridType='major', title='FRA')
  
  
    # 2022 hike - EUR
    #SS: lower flatter
    #Rates: higher flatter

    sr = ('2022-01-01', '2024-01-01')
    pca.PCA(fltT(_s600_spot_fra, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_spot_fra, sr), 8, 10, tGridType='major', title='FRA')
  
    
  
    #citi USA
  
    #
    sr = ('2007-01-01', '2050-01-01')
    pca.PCA(fltT(_s600_citi_spot_usa, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_citi_spot_usa, sr), 8, 10, tGridType='major', title='citi usa asw')
  
    
  
    # GFC USD citi - risk off
    sr = ('2008-01-01', '2009-01-01')
    pca.PCA(fltT(_s600_citi_spot_usa, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_citi_spot_usa, sr), 8, 10, tGridType='major', title='citi usa asw')
              
    
    # GFC USD citi - intial recovery risk on
    sr = ('2009-01-01', '2010-01-01')
    pca.PCA(fltT(_s600_citi_spot_usa, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_citi_spot_usa, sr), 8, 10, tGridType='major', title='citi usa asw')

    # GFC USD citi - 2023 Higher for longer 
    sr = ('2023-08-10', '2024-01-01')
    pca.PCA(fltT(_s600_citi_spot_usa, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_citi_spot_usa, sr), 8, 10, tGridType='major', title='citi usa asw')

        
 
    
    #citi GBR
  
    # total
    sr = ('2007-01-01', '2050-01-01')
    pca.PCA(fltT(_s600_citi_spot_gbr, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_citi_spot_gbr, sr), 8, 10, tGridType='major', title='citi gbr asw')
  
  
    # mini budget LDI
    sr = ('2022-09-01', '2022-10-28')
    pca.PCA(fltT(_s600_citi_spot_gbr, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
    plot_ss_rate(fltT(_s600_citi_spot_gbr, sr), 8, 10, tGridType='major', title='citi gbr asw')
  
    
  # mini budget LDI recovery
  sr = ('2022-10-26', '2022-12-01')
  pca.PCA(fltT(_s600_citi_spot_gbr, sr), matrixtype=tp, rates_pca = True, momentum_size = None).plot_loadings(n=4, w=35, h = 12)
  plot_ss_rate(fltT(_s600_citi_spot_gbr, sr), 8, 10, tGridType='major', title='citi gbr asw')






    

