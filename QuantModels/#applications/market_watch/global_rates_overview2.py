from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper

import argparse



def smooth_series(ds, lim=2, half_window=1):

    def smooth(row):

        if np.isnan(row['ds_prev']) or np.isnan(row['ds_next']):
            return row['ds']

        if abs(row['ds_prev'] - row['ds']) > lim and abs(row['ds_next'] - row['ds']) > lim and abs((row['ds_prev'] + row['ds_next'])/2 - row['ds']) > lim:
            return (row['ds_prev'] + row['ds_next'])/2
        else:
            return row['ds']

    data = pd.DataFrame({'ds':ds, 'ds_prev': ds.shift(half_window), 'ds_next': ds.shift(-1*half_window)})

    return data.apply(smooth, axis=1)
 
    

def smooth_rates(data, limit, half_window): 
    out = {}
    for col, ds in data.iteritems():
        out[col] = smooth_series(ds, lim=limit, half_window=half_window)
    
    return pd.DataFrame(out)

def smooth_fwd_rates(data, limit, half_window): 
    
    out = {}
    
    for col, ds in data.iteritems():
        
        if len(col.split('_')) > 1 and int(col.split('_')[1].replace('Y','').replace('M','')) != 0:
            out[col] = smooth_series(ds, lim=limit, half_window=half_window)
        else:
            out[col] = ds
    
    return pd.DataFrame(out)


def restructure(data, tenors, curve_names):
    df_data = {}
    curve_names.sort()    
    for curve in curve_names:
        curve_columns = [curve + '_' + tenor for tenor in tenors]
        curve_data = data[curve_columns].rename(lambda x: x.split('_')[2], axis='columns')
        ds = curve_data.iloc[0]
        df_data[curve] = ds 
    df = pd.DataFrame(df_data)
    df.index.name = 'Tenor'
    return df


def create_pca(data, date_index, momentum_size=None, resample_freq= 'W'):

    data_sub = data[date_index].resample(resample_freq).last() if resample_freq is not None else data[date_index]    
    return pca.PCA(data_sub, rates_pca = True, momentum_size = momentum_size)


def create_combo_mr(data, date_index, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None):
    mrs = {}
    data_sub = data[date_index]
    combos = list(combinations(data.columns, elements))
    instrument = combos[0][0][3:]
    
    for combo in combos:
        _sub_data = data_sub[list(combo)].resample(resample_freq).last() if resample_freq is not None else data_sub[list(combo)]
        data_sub_pca = pca.PCA(_sub_data, rates_pca = True, momentum_size = momentum_size)
        hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_pca.pc_hedge
        mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
        mrs[('_').join(combo) + '_PCW'] = (mr_obj, data_sub_pca)
        if include_normal_weighted_series:
            if len(_sub_data.columns) == 2:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] - _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s - %s' % (_sub_data.columns[0], _sub_data.columns[1])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)
            elif len(_sub_data.columns) == 3:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] + _sub_data[_sub_data.columns[2]] - 2 * _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s -2 x %s + %s' % (_sub_data.columns[0], _sub_data.columns[1], _sub_data.columns[2])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)

    return mrs




#command line args
parser = argparse.ArgumentParser(description='---- global rates overview ----')
parser.add_argument('filedate', metavar='filedate', type=str, nargs=1, help='date in yyyy-mm-dd')
parser.add_argument('modeldate', metavar='modeldate', type=str, nargs=1, help='date in yyyy-mm-dd')
parser.add_argument('cross_countries_pca_history', metavar='cross_countries_pca_history', type=int, nargs=1, help='number of the months of history will be used for cross country pca')
parser.add_argument('pair_pca_history', metavar='pair_pca_history', type=int, nargs=1, help='number of the months of history will be used for cross country pca')

args = parser.parse_args()
print(args.filedate)
print(args.modeldate)
print(args.cross_countries_pca_history)
print(args.pair_pca_history)
# data
    
filedate = args.filedate[0]
modeldate = args.modeldate[0]
cross_countries_pca_history = args.cross_countries_pca_history[0]
pair_pca_history = args.pair_pca_history[0]

data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_SWAP_ALL_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()
data = smooth_fwd_rates(data, 0.2, 1)



#take out the history for model period
data = data[data.index < modeldate]

#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)

cross_countries_pca_history = data.index[-1] - DateOffset(years = 0, months=cross_countries_pca_history, days=0)
pair_pca_history = data.index[-1] - DateOffset(years = 0, months=pair_pca_history, days=0)

#countries
countries = set(data.columns.map(lambda x: x[:3]))

#tickers

ticker_1y = [c for c in data.columns if '_0Y_1Y' in c and  'NOK' not in c]
ticker_2y = [c for c in data.columns if '_0Y_2Y' in c and  'NOK' not in c]
ticker_3y = [c for c in data.columns if '_0Y_3Y' in c and  'NOK' not in c]
ticker_4y = [c for c in data.columns if '_0Y_4Y' in c and  'NOK' not in c]
ticker_5y = [c for c in data.columns if '_0Y_5Y' in c and  'NOK' not in c]
ticker_7y = [c for c in data.columns if '_0Y_7Y' in c and  'NOK' not in c]
ticker_10y = [c for c in data.columns if '_0Y_10Y' in c and  'NOK' not in c]
ticker_1y1y = [c for c in data.columns if '_1Y_1Y' in c and  'NOK' not in c]
ticker_1y2y = [c for c in data.columns if '_1Y_2Y' in c and  'NOK' not in c]
ticker_1y5y = [c for c in data.columns if '_1Y_5Y' in c and  'NOK' not in c]
ticker_1y10y = [c for c in data.columns if '_1Y_10Y' in c and  'NOK' not in c]
ticker_2y1y = [c for c in data.columns if '_2Y_1Y' in c and  'NOK' not in c]
ticker_2y2y = [c for c in data.columns if '_2Y_2Y' in c and  'NOK' not in c]
ticker_2y5y = [c for c in data.columns if '_2Y_5Y' in c and  'NOK' not in c]
ticker_2y10y = [c for c in data.columns if '_2Y_10Y' in c and  'NOK' not in c]
ticker_3y2y = [c for c in data.columns if '_3Y_2Y' in c and  'NOK' not in c]
ticker_5y5y = [c for c in data.columns if '_5Y_5Y' in c and  'NOK' not in c]
ticker_2y2y = [c for c in data.columns if '_2Y_2Y' in c and  'NOK' not in c]
ticker_30y = [c for c in data.columns if '_0Y_30Y' in c and  'NOK' not in c and 'THB' not in c and 'HKD' not in c and 'NZD' not in c and 'SGD' not in c]


#derived data

data_2s10s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s10s[ticker_10y[i][:3] + '2s10s'] =  data[ticker_10y[i]] - data[ticker_2y[i]]
    data_2s10s = pd.DataFrame(data_2s10s)

data_2s5s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s5s[ticker_5y[i][:3] + '2s5s'] =  data[ticker_5y[i]] - data[ticker_2y[i]]
    data_2s5s = pd.DataFrame(data_2s5s)


data_2s7s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s7s[ticker_2y[i][:3] + '2s7s'] =  data[ticker_7y[i]] - data[ticker_2y[i]]
    data_2s7s = pd.DataFrame(data_2s7s)


data_2s5y5s = {}
if True:
    for i in np.arange(0, len(ticker_5y5y)):
         data_2s5y5s[ticker_2y[i][:3] + '2s55s'] =  data[ticker_5y5y[i]] - data[ticker_2y[i]]
    data_2s5y5s = pd.DataFrame(data_2s5y5s)



data_2s2y5s = {}
if True:
    for i in np.arange(0, len(ticker_2y5y)):
         data_2s2y5s[ticker_2y[i][:3] + '2s25s'] =  data[ticker_2y5y[i]] - data[ticker_2y[i]]
    data_2s2y5s = pd.DataFrame(data_2s2y5s)


data_2s3y2s = {}
if True:
    for i in np.arange(0, len(ticker_3y2y)):
        data_2s3y2s[ticker_2y[i][:3] + '2s32s'] =  data[ticker_3y2y[i]] - data[ticker_2y[i]]
    data_2s3y2s = pd.DataFrame(data_2s3y2s)

data_5s10s = {}
if True:
    for i in np.arange(0, len(ticker_10y)):
         data_5s10s[ticker_5y[i][:3] + '5s10s'] =  data[ticker_10y[i]] - data[ticker_5y[i]]
    data_5s10s = pd.DataFrame(data_5s10s)

data_5s5y5s = {}
if True:
    for i in np.arange(0, len(ticker_5y5y)):
         data_5s5y5s[ticker_5y[i][:3] + '5s55s'] =  data[ticker_5y5y[i]] - data[ticker_5y[i]]
    data_5s5y5s = pd.DataFrame(data_5s5y5s)

data_5s30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_0Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_5s30s[ticker_30y[i][:3] + '5s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_5s30s = pd.DataFrame(data_5s30s)

data_10s30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_10s30s[ticker_30y[i][:3] + '10s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_10s30s = pd.DataFrame(data_10s30s)

data_5y5ys30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_5Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_5y5ys30s[ticker_30y[i][:3] + '55s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_5y5ys30s = pd.DataFrame(data_5y5ys30s)


data_2s30s = {}
ticker_temp = [c.replace('_0Y_30Y', '_0Y_2Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_2s30s[ticker_30y[i][:3] + '2s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
    data_2s30s = pd.DataFrame(data_2s30s)


data_2s5s10s = {}
if True:
    for i in np.arange(0, len(ticker_2y)):
         data_2s5s10s[ticker_2y[i][:3] + 'P2s5s10s'] =  2 * data[ticker_5y[i]] - data[ticker_2y[i]] - data[ticker_10y[i]]
    data_2s5s10s = pd.DataFrame(data_2s5s10s)


data_5s10s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_5s10s30s[ticker_30y[i][:3] + 'P5s10s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_5s10s30s = pd.DataFrame(data_5s10s30s)


data_2s10s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_2s10s30s[ticker_30y[i][:3] + 'P2s10s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_2s10s30s = pd.DataFrame(data_2s10s30s)


data_22s10s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_2Y_2Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_22s10s30s[ticker_30y[i][:3] + 'P22s10s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_22s10s30s = pd.DataFrame(data_22s10s30s)





data_2s55s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_2s55s30s[ticker_30y[i][:3] + 'P2s55s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_2s55s30s = pd.DataFrame(data_2s55s30s)


data_5s55s30s = {}
ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
         data_5s55s30s[ticker_30y[i][:3] + 'P5s55s30s'] =  2 * data[ticker_temp2[i]] - data[ticker_temp1[i]] - data[ticker_30y[i]]
    data_5s55s30s = pd.DataFrame(data_5s55s30s)


data_11s22s55s = {}
if True:
    for i in np.arange(0, len(ticker_1y1y)):
         data_11s22s55s[ticker_1y1y[i][:3] + 'P11s22s55s'] =  2 * data[ticker_2y2y[i]] - data[ticker_1y1y[i]] - data[ticker_5y5y[i]]
    data_11s22s55s = pd.DataFrame(data_11s22s55s)


data_22s25s = {}
if True:
    for i in np.arange(0, len(ticker_2y2y)):
         data_22s25s[ticker_2y2y[i][:3] + '22s25s'] =  data[ticker_2y5y[i]] - data[ticker_2y2y[i]]
    data_22s25s = pd.DataFrame(data_22s25s)


data_2s22s55s = {}
if True:
    for i in np.arange(0, len(ticker_5y5y) if len(ticker_5y5y) <= len(ticker_2y2y) else len(ticker_2y2y) ):
         data_2s22s55s[ticker_2y[i][:3] + 'P2s22s55s'] =  2 * data[ticker_2y2y[i]] - data[ticker_2y[i]] - data[ticker_5y5y[i]]
    data_2s22s55s = pd.DataFrame(data_2s22s55s)



data_22s210s = {}
if True:
    for i in np.arange(0, len(ticker_2y2y)):
         data_22s210s[ticker_2y2y[i][:3] + '22s210s'] =  data[ticker_2y10y[i]] - data[ticker_2y2y[i]]
    data_22s210s = pd.DataFrame(data_22s210s)


data_25s210s = {}
if True:
    for i in np.arange(0, len(ticker_2y5y)):
         data_25s210s[ticker_2y5y[i][:3] + '25s210s'] =  data[ticker_2y10y[i]] - data[ticker_2y5y[i]]
    data_25s210s = pd.DataFrame(data_25s210s)



data_12s15s = {}
if True:
    for i in np.arange(0, len(ticker_1y2y)):
         data_12s15s[ticker_1y2y[i][:3] + '12s15s'] =  data[ticker_1y5y[i]] - data[ticker_1y2y[i]]
    data_12s15s = pd.DataFrame(data_12s15s)



data_12s110s = {}
if True:
    for i in np.arange(0, len(ticker_1y2y)):
         data_12s110s[ticker_1y2y[i][:3] + '12s110s'] =  data[ticker_1y10y[i]] - data[ticker_1y2y[i]]
    data_12s110s = pd.DataFrame(data_12s110s)


data_15s110s = {}
if True:
    for i in np.arange(0, len(ticker_1y5y)):
         data_15s110s[ticker_1y5y[i][:3] + '15s110s'] =  data[ticker_1y10y[i]] - data[ticker_1y5y[i]]
    data_15s110s = pd.DataFrame(data_15s110s)
    
    

data_12s15s110s = {}
if True:
    for i in np.arange(0, len(ticker_1y2y)):
         data_12s15s110s[ticker_1y2y[i][:3] + 'P12s15s110s'] =  2 * data[ticker_1y5y[i]] - data[ticker_1y2y[i]] - data[ticker_1y10y[i]]
    data_12s15s110s = pd.DataFrame(data_12s15s110s)

data_22s25s210s = {}
if True:
    for i in np.arange(0, len(ticker_2y2y)):
         data_22s25s210s[ticker_2y2y[i][:3] + 'P22s25s210s'] =  2 * data[ticker_2y5y[i]] - data[ticker_2y2y[i]] - data[ticker_2y10y[i]]
    data_22s25s210s = pd.DataFrame(data_22s25s210s)


data_22s55s = {}
if True:
    for i in np.arange(0, len(ticker_2y2y)):
         data_22s55s[ticker_2y2y[i][:3] + '22s55s'] =  data[ticker_5y5y[i]] - data[ticker_2y2y[i]]
    data_22s55s = pd.DataFrame(data_22s55s)

data_12s55s = {}
if True:
    for i in np.arange(0, len(ticker_1y2y)):
         data_12s55s[ticker_2y2y[i][:3] + '12s55s'] =  data[ticker_5y5y[i]] - data[ticker_1y2y[i]]
    data_12s55s = pd.DataFrame(data_12s55s)



# per country data
data_per_country = {}

ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
        data_per_country[ticker_30y[i][:3] + 'PCW5s55s30s'] = pd.DataFrame({ticker_temp1[i]: data[ticker_temp1[i]], ticker_temp2[i]: data[ticker_temp2[i]], ticker_30y[i]: data[ticker_30y[i]]})
        

ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_5Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
        data_per_country[ticker_30y[i][:3] + 'PCW5s10s30s'] = pd.DataFrame({ticker_temp1[i]: data[ticker_temp1[i]], ticker_temp2[i]: data[ticker_temp2[i]], ticker_30y[i]: data[ticker_30y[i]]})


ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_5Y_5Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
        data_per_country[ticker_30y[i][:3] + 'PCW2s55s30s'] = pd.DataFrame({ticker_temp1[i]: data[ticker_temp1[i]], ticker_temp2[i]: data[ticker_temp2[i]], ticker_30y[i]: data[ticker_30y[i]]})

ticker_temp1 = [c.replace('_0Y_30Y', '_0Y_2Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
        data_per_country[ticker_30y[i][:3] + 'PCW2s10s30s'] = pd.DataFrame({ticker_temp1[i]: data[ticker_temp1[i]], ticker_temp2[i]: data[ticker_temp2[i]], ticker_30y[i]: data[ticker_30y[i]]})

ticker_temp1 = [c.replace('_0Y_30Y', '_2Y_2Y') for c in ticker_30y]
ticker_temp2 = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
if True:
    for i in np.arange(0, len(ticker_30y)):
        data_per_country[ticker_30y[i][:3] + 'PCW22s10s30s'] = pd.DataFrame({ticker_temp1[i]: data[ticker_temp1[i]], ticker_temp2[i]: data[ticker_temp2[i]], ticker_30y[i]: data[ticker_30y[i]]})

if True:
    for i in np.arange(0, len(ticker_2y)):
        data_per_country[ticker_2y[i][:3] + 'PCW2s5s10s'] =  pd.DataFrame({ticker_2y[i]: data[ticker_2y[i]], ticker_5y[i]: data[ticker_5y[i]], ticker_10y[i]: data[ticker_10y[i]]})

if True:
    for i in np.arange(0, len(ticker_2y)):
        data_per_country[ticker_2y[i][:3] + 'PCW2s5s10s'] =  pd.DataFrame({ticker_2y[i]: data[ticker_2y[i]], ticker_5y[i]: data[ticker_5y[i]], ticker_10y[i]: data[ticker_10y[i]]})

if True:
    for i in np.arange(0, len(ticker_1y1y)):
        data_per_country[ticker_1y1y[i][:3] + 'P11s22s55s'] = pd.DataFrame({ticker_1y1y[i]: data[ticker_1y1y[i]], ticker_2y2y[i]: data[ticker_2y2y[i]], ticker_5y5y[i]: data[ticker_5y5y[i]]}) 


if True:
    for i in np.arange(0, len(ticker_5y5y)):
        data_per_country[ticker_2y[i][:3] + 'P2s22s55s'] = pd.DataFrame({ticker_2y[i]: data[ticker_2y[i]], ticker_2y2y[i]: data[ticker_2y2y[i]], ticker_5y5y[i]: data[ticker_5y5y[i]]}) 


if True:
    for i in np.arange(0, len(ticker_1y2y)):
         data_per_country[ticker_1y2y[i][:3] + 'P12s15s110s'] = pd.DataFrame({ticker_1y2y[i]: data[ticker_1y2y[i]], ticker_1y5y[i]: data[ticker_1y5y[i]], ticker_1y10y[i]: data[ticker_1y10y[i]]})  



###shortcuts



print('Begin the shortcuts')

if(3>2):
 #   mrs = {}

 #   mrs = {**mrs, **create_combo_mr(data[ticker_1y1y], data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None)}
 #   mrs = {**mrs, **create_combo_mr(data_2s2y5s, data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None)}
 #   mrs = {**mrs, **create_combo_mr(data_5s5y5s, data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None)}
 #   mrs = {**mrs, **create_combo_mr( data_22s55s, data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None)}
 #   mrs = {**mrs, **create_combo_mr(data[ticker_5y5y], data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None)}
 #   mrs = {**mrs, **create_combo_mr(data[ticker_2y2y], data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None)}
 #   mrs = {**mrs, **create_combo_mr(data[ticker_2y5y], data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None)}
 #   mrs = {**mrs, **create_combo_mr(data_11s22s55s, data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None)}
 #   mrs = {**mrs, **create_combo_mr(data_2s22s55s, data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None)}

 #   for mr_desc, (mr_obj, pca_obj) in mrs.items():
 #       print(mr_desc)
 #       mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
 #       plt.savefig('C:/Temp/test/market_watch/output/global_overview/shortcut2/%s_%s_%s.png' % (filedate, modeldate, mr_desc), bbox_inches='tight')
 #       plt.close()

 ######## comment out per country MR to run the global ########
 ######## comment out per country MR to run the global ########

#    per_country_mrs = {} 

#    for c_label, c_data in data_per_country.items():
#        per_country_mrs = {**per_country_mrs, **create_combo_mr(c_data, data.index > pair_pca_history, elements = 3, resample_freq= 'B', include_normal_weighted_series=True, momentum_size=None)}
#        per_country_mrs = {**per_country_mrs, **create_combo_mr(c_data, data.index > pair_pca_history, elements = 2, resample_freq= 'B', include_normal_weighted_series=True, momentum_size=None)}


#    for mr_desc, (mr_obj, pca_obj) in per_country_mrs.items():
#        print(mr_desc)
#        mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
#        plt.savefig('C:/Temp/test/market_watch/output/global_overview/shortcut2/%s_%s_%s.png' % (filedate, modeldate, mr_desc), bbox_inches='tight')
#        plt.close()

######## comment out per country MR to run the global ########
######## comment out per country MR to run the global ########

    
    pcas = {}
    pcas['global_2s5s10s_pca'] = create_pca(data_2s5s10s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_5s30s_pca'] = create_pca(data_5s30s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_55s30s_pca'] = create_pca(data_5y5ys30s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_10s30s_pca'] = create_pca(data_10s30s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_1y1y_pca'] = create_pca(data[ticker_1y1y], data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_2s2y5s_pca'] = create_pca(data_2s2y5s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_5s5y5s_pca'] = create_pca(data_5s5y5s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_22s55s_pca'] = create_pca(data_22s55s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_5y5y_pca'] = create_pca(data[ticker_5y5y], data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_2y2y_pca'] = create_pca(data[ticker_2y2y], data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_2y5y_pca'] = create_pca(data[ticker_2y5y], data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_11s22s55s_pca'] = create_pca(data_11s22s55s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_2s22s55s_pca'] = create_pca(data_2s22s55s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_2s10s_pca'] = create_pca(data_2s10s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_2s5s_pca'] = create_pca(data_2s5s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
#    pcas['global_5s10s_pca'] = create_pca(data_5s10s, data.index> cross_countries_pca_history, resample_freq= 'B', momentum_size=None)
    

    

    for pca_description, pca_object in pcas.items():
        print(pca_description)
        loading_fig = pca_object.plot_loadings(n=5, m=7)
        plt.savefig('C:/Temp/test/market_watch/output/global_overview/shortcut2/%s_%s_7d_%s.png' % (filedate, modeldate, pca_description), bbox_inches='tight')
        plt.close()
        
        loading_fig = pca_object.plot_loadings(n=5, m=3)
        plt.savefig('C:/Temp/test/market_watch/output/global_overview/shortcut2/%s_%s_3d_%s.png' % (filedate, modeldate, pca_description), bbox_inches='tight')
        plt.close()

        val_history_fig = pca_object.plot_valuation_history(300, n=[1], w=16, h=12, rows=4, cols=4, normalise_valuation_yaxis = False,  normalise_data_yaxis = False )
        plt.savefig('C:/Temp/test/market_watch/output/global_overview/shortcut2/%s_%s_300d_%s_vals_hdgePC1.png' % (filedate, modeldate, pca_description), bbox_inches='tight')
        plt.close()

        val_history_fig = pca_object.plot_valuation_history(300, n=[1], w=16, h=12, rows=4, cols=4, normalise_valuation_yaxis = True,  normalise_data_yaxis = True )
        plt.savefig('C:/Temp/test/market_watch/output/global_overview/shortcut2/%s_%s_300d_%s_vals__hdgePC1_normlised.png' % (filedate, modeldate, pca_description), bbox_inches='tight')
        plt.close()

    #exit(0)
print('End the shortcuts')

#pcas 
pcas = {}


#temp comments start

#rates
pcas['global_1y_pca'] = create_pca(data[ticker_1y] , data.index> cross_countries_pca_history)
pcas['global_2y_pca'] = create_pca(data[ticker_2y], data.index> cross_countries_pca_history)
pcas['global_3y_pca'] = create_pca(data[ticker_3y], data.index> cross_countries_pca_history)
pcas['global_4y_pca'] = create_pca(data[ticker_4y], data.index> cross_countries_pca_history)
pcas['global_5y_pca'] = create_pca(data[ticker_5y], data.index> cross_countries_pca_history)
pcas['global_10y_pca'] = create_pca(data[ticker_10y], data.index> cross_countries_pca_history)
pcas['global_1y1y_pca'] = create_pca(data[ticker_1y1y], data.index> cross_countries_pca_history)
pcas['global_1y2y_pca'] = create_pca(data[ticker_1y2y], data.index> cross_countries_pca_history)
pcas['global_1y5y_pca'] = create_pca(data[ticker_1y5y], data.index> cross_countries_pca_history)
pcas['global_1y10y_pca'] = create_pca(data[ticker_1y10y], data.index> cross_countries_pca_history)
pcas['global_2y1y_pca'] = create_pca(data[ticker_2y1y], data.index> cross_countries_pca_history)
pcas['global_2y2y_pca'] = create_pca(data[ticker_2y2y], data.index> cross_countries_pca_history)
pcas['global_2y5y_pca'] = create_pca(data[ticker_2y5y], data.index> cross_countries_pca_history)
pcas['global_2y10y_pca'] = create_pca(data[ticker_2y10y], data.index> cross_countries_pca_history)
pcas['global_3y2y_pca'] = create_pca(data[ticker_3y2y], data.index> cross_countries_pca_history)
pcas['global_5y5y_pca'] = create_pca(data[ticker_5y5y], data.index> cross_countries_pca_history)
pcas['global_30y_pca'] = create_pca(data[ticker_30y], data.index> cross_countries_pca_history)



#spread
pcas['global_12s55s_pca'] = create_pca(data_12s55s, data.index> cross_countries_pca_history)
pcas['global_11s22s55s_pca'] = create_pca(data_11s22s55s, data.index> cross_countries_pca_history)
pcas['global_2s22s55s_pca'] = create_pca(data_2s22s55s, data.index> cross_countries_pca_history)


pcas['global_2s5s10s_pca'] = create_pca(data_2s5s10s, data.index> cross_countries_pca_history)
pcas['global_12s15s110s_pca'] = create_pca(data_12s15s110s, data.index> cross_countries_pca_history)
pcas['global_22s25s210s_pca'] = create_pca(data_22s25s210s, data.index> cross_countries_pca_history)

pcas['global_2s10s_pca'] = create_pca(data_2s10s, data.index> cross_countries_pca_history)
pcas['global_12s110s_pca'] = create_pca(data_12s110s, data.index> cross_countries_pca_history)
pcas['global_22s210s_pca'] = create_pca(data_22s210s, data.index> cross_countries_pca_history)


pcas['global_5s30s_pca'] = create_pca(data_5s30s, data.index> cross_countries_pca_history)
pcas['global_55s30s_pca'] = create_pca(data_5y5ys30s, data.index> cross_countries_pca_history)
pcas['global_10s30s_pca'] = create_pca(data_10s30s, data.index> cross_countries_pca_history)

pcas['global_2s5s_pca'] = create_pca(data_2s5s, data.index> cross_countries_pca_history)
pcas['global_12s15s_pca'] = create_pca(data_12s15s, data.index> cross_countries_pca_history)
pcas['global_22s25s_pca'] = create_pca(data_22s25s, data.index> cross_countries_pca_history)

pcas['global_5s10s_pca'] = create_pca(data_5s10s, data.index> cross_countries_pca_history)
pcas['global_15s110s_pca'] = create_pca(data_15s110s, data.index> cross_countries_pca_history)
pcas['global_25s210s_pca'] = create_pca(data_25s210s, data.index> cross_countries_pca_history)
pcas['global_5s55s_pca'] = create_pca(data_5s5y5s, data.index> cross_countries_pca_history)


pcas['global_2s55s_pca'] = create_pca(data_2s5y5s, data.index> cross_countries_pca_history)
pcas['global_22s55s_pca'] = create_pca(data_22s55s, data.index> cross_countries_pca_history)
pcas['global_2s30s_pca'] = create_pca(data_2s30s, data.index> cross_countries_pca_history)
pcas['global_2s10s30s_pca'] = create_pca(data_2s10s30s, data.index> cross_countries_pca_history)
pcas['global_22s10s30s_pca'] = create_pca(data_22s10s30s, data.index> cross_countries_pca_history)
pcas['global_2s55s30s_pca'] = create_pca(data_2s55s30s, data.index> cross_countries_pca_history)
pcas['global_5s10s30s_pca'] = create_pca(data_5s10s30s, data.index> cross_countries_pca_history)
pcas['global_5s55s30s_pca'] = create_pca(data_5s55s30s, data.index> cross_countries_pca_history)


for pca_description, pca_object in pcas.items():
    print(pca_description)
    loading_fig = pca_object.plot_loadings(n=5, m=7)
    plt.savefig('C:/Temp/test/market_watch/output/global_overview/%s_%s_7d_%s.png' % (filedate, modeldate, pca_description), bbox_inches='tight')
    plt.close()
    
    loading_fig = pca_object.plot_loadings(n=5, m=3)
    plt.savefig('C:/Temp/test/market_watch/output/global_overview/%s_%s_3d_%s.png' % (filedate, modeldate, pca_description), bbox_inches='tight')
    plt.close()

    val_history_fig = pca_object.plot_valuation_history(300, n=[1], w=16, h=12, rows=4, cols=4, normalise_valuation_yaxis = False,  normalise_data_yaxis = False )
    plt.savefig('C:/Temp/test/market_watch/output/global_overview/%s_%s_300d_%s_vals_hdgePC1.png' % (filedate, modeldate, pca_description), bbox_inches='tight')
    plt.close()

    val_history_fig = pca_object.plot_valuation_history(300, n=[1], w=16, h=12, rows=4, cols=4, normalise_valuation_yaxis = True,  normalise_data_yaxis = True )
    plt.savefig('C:/Temp/test/market_watch/output/global_overview/%s_%s_300d_%s_vals__hdgePC1_normlised.png' % (filedate, modeldate, pca_description), bbox_inches='tight')
    plt.close()


#temp comments ends   
#####################################################################################################################################################################



per_country_mrs = {} 

for c_label, c_data in data_per_country.items():
    per_country_mrs = {**per_country_mrs, **create_combo_mr(c_data, data.index > pair_pca_history, elements = 3)}
    per_country_mrs = {**per_country_mrs, **create_combo_mr(c_data, data.index > pair_pca_history, elements = 2)}

for mr_desc, (mr_obj, pca_obj) in per_country_mrs.items():
    print(mr_desc)
    mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
    plt.savefig('C:/Temp/test/market_watch/output/global_overview/country/%s_%s_%s.png' % (filedate, modeldate, mr_desc), bbox_inches='tight')
    plt.close()



#####################################################################################################################################################################



mrs = {}

#rates

mrs = {**mrs, **create_combo_mr(data[ticker_1y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_2y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_3y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_4y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_5y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_10y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_1y1y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_1y2y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_1y5y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_1y10y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_2y1y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_2y2y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_2y5y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_2y10y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_3y2y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_5y5y], data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data[ticker_30y], data.index > pair_pca_history)}

mrs = {**mrs, **create_combo_mr(data[ticker_5y], data.index > pair_pca_history, elements = 3, resample_freq= 'W', include_normal_weighted_series=True)}
mrs = {**mrs, **create_combo_mr(data[ticker_10y], data.index > pair_pca_history, elements = 3, resample_freq= 'W', include_normal_weighted_series=True)}


#spread

mrs = {**mrs, **create_combo_mr(data_2s2y5s, data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True)}
mrs = {**mrs, **create_combo_mr(data_2s7s, data.index > pair_pca_history, elements = 2, resample_freq= 'W', include_normal_weighted_series=True)}

mrs = {**mrs, **create_combo_mr(data_12s55s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_11s22s55s, data.index > pair_pca_history)}

mrs = {**mrs, **create_combo_mr(data_2s5s10s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_12s15s110s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_22s25s210s, data.index > pair_pca_history)}

mrs = {**mrs, **create_combo_mr(data_2s10s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_12s110s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_22s210s, data.index > pair_pca_history)}

mrs = {**mrs, **create_combo_mr(data_5s30s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_5y5ys30s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_10s30s, data.index > pair_pca_history)}


mrs = {**mrs, **create_combo_mr(data_2s5s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_12s15s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_22s25s, data.index > pair_pca_history)}


mrs = {**mrs, **create_combo_mr(data_5s10s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_15s110s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_25s210s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_5s5y5s, data.index > pair_pca_history)}


mrs = {**mrs, **create_combo_mr(data_2s5y5s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_22s55s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_2s30s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_2s10s30s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_2s55s30s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_5s10s30s, data.index > pair_pca_history)}
mrs = {**mrs, **create_combo_mr(data_5s55s30s, data.index > pair_pca_history)}



for mr_desc, (mr_obj, pca_obj) in mrs.items():
    print(mr_desc)
    mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
    plt.savefig('C:/Temp/test/market_watch/output/global_overview/crosscountry/%s_%s_%s.png' % (filedate, modeldate, mr_desc), bbox_inches='tight')
    plt.close()



