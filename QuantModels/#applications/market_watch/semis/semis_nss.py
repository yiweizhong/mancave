import pandas as pd
import numpy as np
import os, sys, inspect
from operator import itemgetter
import collections
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations, chain
from pykalman import KalmanFilter
import xlwings as xw
from pandas.tseries.offsets import DateOffset

_p = 'C:\Dev\Mancave\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\statistics\derivative\pysabr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\core'
if _p not in sys.path: sys.path.insert(0, _p)


from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper
from statistics.derivative import bs as bs
from statistics.derivative import quantlibutils as qlu
from statistics.derivative.pysabr import hagan_2002_normal_sabr as nsabr
from statistics.mr import tools as mrt
from statistics.factor import nssmodel
from core import dirs
from core import tools as ctls




from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
import scipy.cluster.hierarchy as cl
from scipy.spatial.distance import pdist, squareform

import QuantLib as ql

pd.set_option('display.expand_frame_repr', False)

      

# static configs
data_file = r'C:\Dev\mancave\QuantModels\#applications\market_watch\semis\semis_data.xlsm'
output_file = r'C:\Dev\mancave\QuantModels\#applications\market_watch\semis\semis_model_output_{}.csv'
output_fig = r'C:\Dev\mancave\QuantModels\#applications\market_watch\semis\{}.png'

cdr = ql.Australia()
cpn_freq = ql.Semiannual
schedule_tenor = ql.Period(cpn_freq)
states = ['NSWTC', 'NTTC', 'QTC', 'TCV', 'WATC', 'ACGB', 'AUSCAP', 'SAFA', 'TASCOR']




isin_exclusion = [
    #'AU3SG0002256', # TSV has this lone 16yr bond skew too much for the curve. take it out effectively make tsv to fit to 15y tenors
    'AU0000XVGHH6', # TSV has this lone 16yr bond skew too much for the curve. take it out effectively make tsv to fit to 15y tenors
                  ]




#function

def get_bond_interpolations(bond_df, settlement_date, isin_exclusion):
    
    maturity_exist = [settlement_date]
    
    instruments = []
    

    for idx, row in bond_df.sort_values(by=['mdl_MAT']).iterrows():
        
        print(r'Processing {} - {} - {}:'.format(row['TICKER'], row['ISIN'], row['mdl_MAT']))
        
        effective_date = qlu.to_ql_date(row['mdl_FIRST_CPN_DATE'])
        maturity_date = qlu.to_ql_date(row['mdl_MAT'])
        
        if(ql.ActualActual().yearFraction(settlement_date, maturity_date) <=30 and 
           ql.ActualActual().yearFraction(settlement_date, maturity_date) > 1 and 
           row['ISIN'] not in isin_exclusion):
            
            #
            # if (maturity_date in maturity_exist):
            #     maturity_date = cdr.advance(maturity_date, 10, ql.Days)
            #     effective_date = cdr.advance(effective_date, 10, ql.Days)
            #     print(r'advance {} - {} - {} maturity by 1 day to avoid duplication'.format(row['TICKER'], row['ISIN'], row['mdl_MAT']))
            
            px = ql.SimpleQuote(row['mdl_PX'])
            cpn = row['mdl_CPN']/100 #in decimal
            
            schedule = ql.Schedule(
                effective_date,
                maturity_date,
                schedule_tenor,
                cdr,
                ql.ModifiedFollowing,
                ql.ModifiedFollowing,
                ql.DateGeneration.Backward,
                False)
            
            bond = ql.FixedRateBondHelper(
                ql.QuoteHandle(px),
                0, #days to settle, same day settlement here
                100., #face amount
                schedule, #schedule
                [cpn], #cpn need to be a vector
                ql.ActualActual(),
                ql.ModifiedFollowing, #paymentConv
                100. #redemption. optional default to 100
                ) 
            #if (maturity_date not in maturity_exist):
            # if maturity gap is less than 3 months the latter security is ignored 
            if (abs(ql.ActualActual().yearFraction(maturity_exist[0], maturity_date)) > 0.25):
                instruments.append(bond)
                maturity_exist[0] = maturity_date
            else:
                print(r'Skipped {} - {} - {} avoid duplication'.format(row['TICKER'], row['ISIN'], row['mdl_MAT']))
        else:
                print(r'Skipped {} - {} - {} avoid miss fitting'.format(row['TICKER'], row['ISIN'], row['mdl_MAT']))
            
    params = [settlement_date, instruments, ql.ActualActual()]

    methods = {
        'logLinearDiscount': ql.PiecewiseLogLinearDiscount(*params),
        'logCubicDiscount': ql.PiecewiseLogCubicDiscount(*params),
        'linearZero': ql.PiecewiseLinearZero(*params),
        'cubicZero': ql.PiecewiseCubicZero(*params),
        'linearForward': ql.PiecewiseLinearForward(*params),
        'splineCubicDiscount': ql.PiecewiseSplineCubicDiscount(*params),
    }
    
    return methods, instruments


def get_zero_rates_from_interpolation(interpolation_obj, dates=None):
    _d1 = interpolation_obj.dates()[0] #get the first date from the interpolation obj it is used as the anchor for year fraction
    
    _dates = dates if dates is not None else [d[0] for d in interpolation_obj.nodes()]
    
    _zr = {qlu.to_datetime(dt): interpolation_obj.zeroRate(dt, ql.ActualActual(), 
                                                           ql.Compounded, ql.Semiannual, True).rate()
           for dt in _dates}
    _df = {qlu.to_datetime(dt): interpolation_obj.discount(dt, True) 
           for dt in _dates}
    _dts = {qlu.to_datetime(dt): ql.ActualActual().yearFraction(_d1, dt) 
           for dt in _dates}
    
    result = pd.DataFrame({
        'df':pd.Series(_df),
        'zr':pd.Series(_zr),
        'tenor':_dts
        })
    
    return result

    
def prep_tenors_in_years(start_date, max_tenor, calendar, frequency=ql.Months, day_count=ql.ActualActual()):
    '''
    Parameters
    ----------
    start_date: ql.Date
        representing the start of the tenor. It will be converted to 0 year fraction.
    max_tenor: float 
        a number represent max tenor in the unit of frequency
    calendar:
        ql.Australia()
    frequency: int
        ql.Months
    day_count: 
        ql.ActualActual().
    Returns
    -------
    output : List, list
        a list of tenor in fraction of years.
        a list tenor in ql.Date
    '''
    
    tenor_dates = [calendar.advance(start_date, int(i), frequency) for i in np.arange(0, max_tenor) + 1]
    tenors = [day_count.yearFraction(start_date, d) for d in tenor_dates]
    return tenors, tenor_dates

def get_spot_zero_discount_curve(fitted_rates, val_name='rates'):
    
    '''
     Parameters
    ----------
    fitted_rates: df    
        representing rates and tenor in ql.dates for the curve.
                                tenors     rates
        August 11th, 2021     0.000000  0.000000
        September 13th, 2021  0.090411  0.000478
        October 11th, 2021    0.167123  0.000317
    val_name: string
        represent the column name of the rate    
    '''
    
    spot_dates = fitted_rates.reset_index()['index'].to_list()
    spot_rates = fitted_rates[val_name].to_list()
    
    
    spot_curve = ql.ZeroCurve(spot_dates, spot_rates, 
                              ql.ActualActual(), ql.Australia(),
                              ql.Linear(), ql.Compounded, ql.Semiannual)
    spot_curve_handle = ql.YieldTermStructureHandle(spot_curve)
    
    return spot_curve_handle
    

def price_bonds(bond_df, settlement_days, pricing_curve_handle):
    pricing_engine = ql.DiscountingBondEngine(pricing_curve_handle)
    instruments = []
    for idx, row in bond_df.iterrows():
        effective_date = qlu.to_ql_date(row['mdl_FIRST_CPN_DATE'])
        maturity_date = qlu.to_ql_date(row['mdl_MAT'])
        #px = ql.SimpleQuote(row['Pxs_Dec_Only'])
        cpn = row['mdl_CPN']/100 #in decimal
        
        schedule = ql.Schedule(
            effective_date,
            maturity_date,
            schedule_tenor,
            cdr,
            ql.ModifiedFollowing,
            ql.ModifiedFollowing,
            ql.DateGeneration.Backward,
            False)
        
        bond = ql.FixedRateBond(
            settlement_days, # this the number of days to settle not dates of settle
            100.0, #face value
            schedule, #schedule
            [cpn], #cpn need to be a vector
            ql.ActualActual() #day count
            ) 
        bond.setPricingEngine(pricing_engine)
        instruments.append(bond)
        
    return instruments


def create_nss_model(zr_for_fit, state):
    
    nssm = nssmodel.NSCurveFamily(True) 
    nssm.estimateParam(zr_for_fit['tenor'], zr_for_fit['zr'])
    
    print(f'{state}:')
    print('Best fit param: (RSqr=%.3f)' % nssm.rsqr)
    print('tau1=%.2f tau2=%.2f beta0=%.3f beta1=%.3f beta2=%.3f beta3=%.3f' % (nssm.tau0, nssm.tau1, nssm.beta0, nssm.beta1, nssm.beta2, nssm.beta3) )
    
    zr_for_fit['nssm_zr'] = nssm.getSpot(zr_for_fit['tenor'])
    
    return nssm

def fit_spot_rates_using_model(nssm, spot_tenors, spot_tenor_dates):
    spot_rates = nssm.getSpot(spot_tenors)    
    fitted_spot_rates = pd.DataFrame({
        'tenors': pd.Series([0] + spot_tenors, index=[ql.Settings.instance().evaluationDate] + spot_tenor_dates),
        'rates': pd.Series([0] + spot_rates.tolist(), index= [ql.Settings.instance().evaluationDate] + spot_tenor_dates)
        })
    return fitted_spot_rates

def update_output(bond_df, repriced_bonds):
    output = bond_df.copy()
    repcied_ytm = [b.bondYield(ql.ActualActual(), ql.Compounded, ql.Semiannual) for b in repriced_bonds]
    repcied_celan_price = [b.cleanPrice() for b in repriced_bonds]
    repcied_dirty_price = [b.dirtyPrice() for b in repriced_bonds]
    output['nssm_YTM'] = np.array(repcied_ytm) * 100
    output['nssm_CLEAN_PRICE'] = repcied_celan_price
    output['nssm_DIRTY_PRICE'] = repcied_dirty_price
    return output


def plot_fits(line_series, dot_series, title, dot_series_label=None, figsize=(10, 10)):
    
    #from matplotlib.dates import DateFormatter, YearLocator
    
    error = dot_series - line_series
    error_std = error.std()
        
    fig = plt.figure(figsize=figsize)
    gs = fig.add_gridspec(ncols=1, nrows=2, width_ratios=[1], height_ratios=[1,1])
    
    ax1 = fig.add_subplot(gs[0, 0])
    ax2 = fig.add_subplot(gs[1, 0])
        
    ax1.plot(line_series.index, line_series.values, ':', label='Curve Fit', alpha=0.6)
    ax1.plot(dot_series.index, dot_series.values, 'o', label='Market Data')
    
    # Set the x-axis to do major ticks on the days and label them like '07/20'
    #ax.xaxis.set_major_locator(YearLocator())
    #ax.xaxis.set_major_formatter(DateFormatter("%b %y"))

    ax1.set_title(title)
    ax1.set_xlabel('Maturity')
    ax1.set_ylabel('rate (%)')
    ax1.grid(True)
    leg1 = ax1.legend(fancybox=True)
    leg1.get_frame().set_facecolor('none')
    leg1.get_frame().set_linewidth(0.0)
    
    ax2.plot(error.index, error.values, 'o', label='error')
    ax2.set_xlabel('Maturity')
    ax2.set_ylabel('rate valuation(%)')
    ax2.axhline(y=0, color='k', linestyle='-',  lw=0.5)
    ax2.axhline(y=error_std, color='grey', linestyle='--',  lw=0.5)
    ax2.axhline(y=-error_std, color='grey', linestyle='--',  lw=0.5)
    #ax2.grid(True)
    ax2.spines['top'].set_visible(False)
    ax2.spines['right'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
 
    if dot_series_label is not None:
        i = 0
        for x, y in zip(error.index, error.values):
            label = dot_series_label[i] 
            alpha = 1 if abs(y) > error_std else 0.2
            ax2.annotate(label, # this is the text
                         (x,y), # these are the coordinates to position the label
                         textcoords="offset points", # how to position the text
                         xytext=(0,10), # distance from text to points (x,y)
                         ha='left', # horizontal alignment can be left, right or center
                         rotation=45,
                         size=7,
                         alpha=alpha) 
                
            i = i + 1
    
    return fig
#

#data


semis_data = pd.read_excel(f'{data_file}', sheet_name='current', usecols='A:Z')
#valuation date from the database
vdate = qlu.to_ql_date(semis_data['ValuationDate'].iloc[0])
#valuation date from the database
vdate_live = qlu.to_ql_date(semis_data['LiveDate'].iloc[0])



#process

'''
True for real time data, false for using valuation date from the database
'''
if (True):

    #set ql valuation date accordingly
    ql.Settings.instance().evaluationDate = vdate_live
    semis_data['mdl_CPN'] = semis_data['CPN']
    semis_data['mdl_FIRST_CPN_DATE'] = semis_data['First_Cpn_Date']
    semis_data['mdl_MAT'] = semis_data['MAT']
    semis_data['mdl_PX'] = semis_data['Pxs_Dec_Only']
    semis_data['mdl_YTM'] = semis_data['YTM']
    semis_data['mdl_DATE'] = semis_data['LiveDate']
    
else:
    
    #set ql valuation date accordingly
    ql.Settings.instance().evaluationDate = vdate
    semis_data['mdl_CPN'] = semis_data['CPN']
    semis_data['mdl_FIRST_CPN_DATE'] = semis_data['First_Cpn_Date']
    semis_data['mdl_MAT'] = semis_data['MAT']
    semis_data['mdl_PX'] = semis_data['PX_CLEAN_MID']
    semis_data['mdl_YTM'] = semis_data['YLD_YTM_MID']
    semis_data['mdl_DATE'] = semis_data['ValuationDate']

#same date settlement
settlement_date = cdr.advance(ql.Settings.instance().evaluationDate, 0, ql.Days)
    


''' 1. seperate bonds for states'''
#--- semis_data_nsw =  semis_data[semis_data['TICKER']=='NSWTC']
state_data_dict = {state: semis_data[semis_data['TICKER']==state] for state in states}


''' 2. interpolate bonds '''
#--- interpolations_obj, interpolation_instruments  = get_bond_interpolations(semis_data_nsw, settlement_date)
state_bond_interpolation_obj_dict = {state:get_bond_interpolations(state_data_dict[state], settlement_date, isin_exclusion)[0]  for state in states}
#state_bond_interpolation_instrument_dict = {state:get_bond_interpolations(state_data_dict[state], settlement_date)[1]  for state in states}


''' 3. interpolated zero rates '''
#--- zr_for_fit = get_zero_rates_from_interpolation(interpolations_obj['logLinearDiscount'], semis_data_nsw['MAT'].apply(qlu.to_ql_date))
state_zr_for_fit_dict = {state: get_zero_rates_from_interpolation(state_bond_interpolation_obj_dict[state]['logLinearDiscount'], 
                                                             state_data_dict[state]['mdl_MAT'].apply(qlu.to_ql_date))  
                    for state in states}


'''4. create nss model and fit the zr rate'''
#--- nssm = nssmodel.NSCurveFamily(True) 
#--- nssm.estimateParam(zr_for_fit['tenor'], zr_for_fit['zr'])

state_nss_model_dict = {state: create_nss_model(state_zr_for_fit_dict[state], state) for state in states}


'''5. create a list of spot tenors in fraction of years and the correspoding ql date
    these dates only depends on the evaluation date, ie the start'''
    
spot_tenors, spot_tenor_dates = prep_tenors_in_years(vdate, 400, cdr)

'''6. fit the zr rates using caliberated nss model for the tenors specified'''

# spot_rates = nssm.getSpot(spot_tenors)
# fitted_spot_rates = pd.DataFrame({
#     'tenors': pd.Series([0] + spot_tenors, index= [vdate] + spot_tenor_dates),
#     'rates': pd.Series([0] + spot_rates.tolist(), index= [vdate] + spot_tenor_dates)
#     })

state_fitted_spot_rates_dict = {state:fit_spot_rates_using_model(state_nss_model_dict[state],
                                                                 spot_tenors,
                                                                 spot_tenor_dates)
                                for state in states}

# state_fitted_spot_rates_for_original_inputs_dict = {state:fit_spot_rates_using_model(state_nss_model_dict[state],
#                                                                  state_zr_for_fit_dict[state]['tenor'].values.tolist(),
#                                                                  state_zr_for_fit_dict[state].reset_index()['index'].apply(qlu.to_ql_date).values.tolist())
#                                 for state in states}



'''7. fit the zr rates using caliberated nss model for the tenors specified'''
# fitted_spot_zero_curve_handle = get_spot_zero_discount_curve(fitted_spot_rates)

state_fitted_spot_zero_curve_handle_dict = {state: get_spot_zero_discount_curve(state_fitted_spot_rates_dict[state]) 
                                            for state in states}

'''8. price the bonds using the fitted spot zero yield curve handle '''

#repriced_bonds = price_bonds(semis_data_nsw, 0, fitted_spot_zero_curve_handle)

state_repriced_bonds_dict ={state: price_bonds(state_data_dict[state], 0,
                                               state_fitted_spot_zero_curve_handle_dict[state])
                            for state in states}

'''9. price the bonds using the fitted spot zero yield curve handle '''

#repcied_ytm = [b.bondYield(ql.ActualActual(), ql.Compounded, ql.Semiannual) for b in repriced_bonds]
#semis_data_nsw['NSS_YTM'] = np.array(repcied_ytm) * 100
    
state_post_model_data_dict = {state: update_output(state_data_dict[state],
                                                   state_repriced_bonds_dict[state]) 
                              for state in states}


'''10. price the bonds using the fitted spot zero yield curve handle '''

#semis_data_nsw_plot_data = semis_data_nsw[['MAT', 'YTM', 'NSS_YTM', 'ISIN']].set_index('MAT')
state_plot_data_dict = {state: state_post_model_data_dict[state][['mdl_DATE', 'TICKER', 'mdl_MAT', 'mdl_YTM', 'nssm_YTM', 'ISIN']].set_index('mdl_MAT')
                        for state in states}

#plot_fits(semis_data_nsw_plot_data['NSS_YTM'], semis_data_nsw_plot_data['YTM'] , semis_data_nsw_plot_data['ISIN'])

output_figs = {state: plot_fits(state_plot_data_dict[state]['nssm_YTM'], 
                                state_plot_data_dict[state]['mdl_YTM'],
                                '{} ({})'.format(state, qlu.to_datetime(ql.Settings.instance().evaluationDate).strftime(r'%Y-%m-%d')),
                                dot_series_label=state_plot_data_dict[state]['ISIN']) 
               for state in states}

[output_figs[state].savefig(output_fig.format(state), dpi = 120, bbox_inches='tight') for state in states]


'''10. export outputs '''

pd.concat(state_plot_data_dict).reset_index().drop(['level_0'], axis=1).to_csv(output_file.format(qlu.to_datetime(ql.Settings.instance().evaluationDate).strftime(r'%Y-%m-%d')))















