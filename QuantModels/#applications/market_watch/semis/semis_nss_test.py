import pandas as pd
import numpy as np
import os, sys, inspect
from operator import itemgetter
import collections
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations, chain
from pykalman import KalmanFilter
import xlwings as xw
from pandas.tseries.offsets import DateOffset

_p = 'C:\Dev\Mancave\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\statistics\derivative\pysabr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Mancave\QuantModels\core'
if _p not in sys.path: sys.path.insert(0, _p)


from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper
from statistics.derivative import bs as bs
from statistics.derivative import quantlibutils as qlu
from statistics.derivative.pysabr import hagan_2002_normal_sabr as nsabr
from statistics.mr import tools as mrt
from statistics.factor import nssmodel
from core import dirs
from core import tools as ctls




from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
import scipy.cluster.hierarchy as cl
from scipy.spatial.distance import pdist, squareform

import QuantLib as ql

pd.set_option('display.expand_frame_repr', False)



'''Test 1

https://quant.stackexchange.com/questions/50370/quantlib-natural-cubic-spline-yield-curve

signature for the termstructure class. it has member function like discount and zeroRate
https://rkapl123.github.io/QLAnnotatedSource/d4/d11/class_quant_lib_1_1_yield_term_structure.html#a87d1773469d34245ca510dabc54b0070

schedule
https://quantlib-python-docs.readthedocs.io/en/latest/dates.html#schedule

'''



price = [96.60, 
         93.71,
         91.56,
         90.24,
         89.74,
         90.04,
         91.09,
         92.82,
         95.19,
         98.14,
         101.60,
         105.54,
         109.90,
         114.64,
         119.73]

cpn = [2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9]
maturiy = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

pgbs = pd.DataFrame({'maturity':maturiy, 'cpn':cpn, 'price':price})


calendar = ql.TARGET()
ql.Settings.instance().evaluationDate = ql.Date(12, 8, 2021)
bondSettlementDate = ql.Date(12, 8, 2021)



# create a list of bonds

instruments = []

frequency = ql.Annual
dc = ql.ActualActual(ql.ActualActual.ISMA)
accrualConvention = ql.ModifiedFollowing
convention = ql.ModifiedFollowing

for idx, row in pgbs.iterrows():
    maturity = calendar.advance(bondSettlementDate, int(row.maturity), ql.Years)
    px = ql.SimpleQuote(row.price)
    cpn = row.cpn/100 #in decimal
    
    schedule = ql.Schedule(
        bondSettlementDate,
        maturity,
        ql.Period(frequency),
        calendar,
        accrualConvention,
        accrualConvention,
        ql.DateGeneration.Backward,
        False)
    
    bond = ql.FixedRateBondHelper(
        ql.QuoteHandle(px),
        0, #days to settle, same day settlement here
        100., #face amount
        schedule, #schedule
        [cpn], #cpn need to be a vector
        dc,
        convention, #paymentConv
        100. #redemption. optional default to 100
        ) 
    instruments.append(bond)


#use the bonds to create interpolated curve

params = [bondSettlementDate, instruments, dc]

methods = {
    'logLinearDiscount': ql.PiecewiseLogLinearDiscount(*params),
    'logCubicDiscount': ql.PiecewiseLogCubicDiscount(*params),
    'linearZero': ql.PiecewiseLinearZero(*params),
    'cubicZero': ql.PiecewiseCubicZero(*params),
    'linearForward': ql.PiecewiseLinearForward(*params),
    'splineCubicDiscount': ql.PiecewiseSplineCubicDiscount(*params),
}

tenors = np.arange(0,205,5)/10.0
interpolated_curve = {}
for method in methods:
   # annual zero rate
   interpolated_curve[f'{method}_zr'] = \
   pd.Series([methods[method].zeroRate(calendar.advance(bondSettlementDate, int(x), ql.Years),
                                         dc, ql.Compounded, frequency, 
                                         True).rate()*100 
    for x in tenors], index=tenors)
   
   #df can be used to create continuous zero rate too
   interpolated_curve[f'{method}_df'] = \
   pd.Series([methods[method].discount(x, True) for x in tenors], index=tenors)

interpolated_curve = pd.DataFrame(interpolated_curve)

#use the interpolated zero rates to fit NSS and get the NSS zero rate

#build a zero curve using the NSS zero rates

#using the NSS parameter to get the zero rats for each bonds to be priced
'''
https://quant.stackexchange.com/questions/58758/nelson-siegel-model-calculation-of-the-zero-bound-price-at-time-zero-that-expire
'''

''' Testing semis bond schedule. state public holiday is off! 


test_s = ql.Schedule(
        ql.Date(1, 11, 2007),
        ql.Date(1, 5, 2023),
        ql.Period(ql.Semiannual),
        ql.Australia(),
        ql.ModifiedFollowing, #acru convention
        ql.ModifiedFollowing, #termination convention 
        ql.DateGeneration.Backward,
        False) 

list(test_s)


test_semi_bond = ql.FixedRateBond(
        0, #days to settle, same day settlement here
        100., #face amount
        test_s, #schedule
        [6.0/100], #cpn need to be a vector
        ql.ActualActual(), #acrual date counter
        ql.ModifiedFollowing, #BusinessDayConvention
        100. #redemption. optional default to 100
        ) 


'''


      

# static configs
data_file = r'C:\Dev\mancave\QuantModels\#applications\market_watch\semis\semis_data.xlsm'
cdr = ql.Australia()
cpn_freq = ql.Semiannual
schedule_tenor = ql.Period(cpn_freq)
semis_data = pd.read_excel(f'{data_file}', sheet_name='current', usecols='A:V')
#valuation date from the database
vdate = qlu.to_ql_date(semis_data['ValuationDate'].iloc[0])
#same date settlement
settlement_date = cdr.advance(vdate, 0, ql.Days)
#set ql valuation date accordingly
ql.Settings.instance().evaluationDate = vdate





#function

def get_bond_interpolations(bond_df, settlement_date):
    instruments = []

    for idx, row in bond_df.iterrows():
        effective_date = qlu.to_ql_date(row['First_Cpn_Date'])
        maturity_date = qlu.to_ql_date(row['MAT'])
        px = ql.SimpleQuote(row['Pxs_Dec_Only'])
        cpn = row['CPN']/100 #in decimal
        
        schedule = ql.Schedule(
            effective_date,
            maturity_date,
            schedule_tenor,
            cdr,
            ql.ModifiedFollowing,
            ql.ModifiedFollowing,
            ql.DateGeneration.Backward,
            False)
        
        bond = ql.FixedRateBondHelper(
            ql.QuoteHandle(px),
            0, #days to settle, same day settlement here
            100., #face amount
            schedule, #schedule
            [cpn], #cpn need to be a vector
            ql.ActualActual(),
            ql.ModifiedFollowing, #paymentConv
            100. #redemption. optional default to 100
            ) 
        instruments.append(bond)
    
    params = [settlement_date, instruments, ql.ActualActual()]

    methods = {
        'logLinearDiscount': ql.PiecewiseLogLinearDiscount(*params),
        'logCubicDiscount': ql.PiecewiseLogCubicDiscount(*params),
        'linearZero': ql.PiecewiseLinearZero(*params),
        'cubicZero': ql.PiecewiseCubicZero(*params),
        'linearForward': ql.PiecewiseLinearForward(*params),
        'splineCubicDiscount': ql.PiecewiseSplineCubicDiscount(*params),
    }
    
    return methods, instruments

def get_zero_rates_from_interpolation(interpolation_obj, dates=None):
    _d1 = interpolation_obj.dates()[0] #get the first date from the interpolation obj it is used as the anchor for year fraction
    
    _dates = dates if dates is not None else [d[0] for d in interpolation_obj.nodes()]
    
    _zr = {qlu.to_datetime(dt): interpolation_obj.zeroRate(dt, ql.ActualActual(), 
                                                           ql.Compounded, ql.Semiannual, True).rate()
           for dt in _dates}
    _df = {qlu.to_datetime(dt): interpolation_obj.discount(dt, True) 
           for dt in _dates}
    _dts = {qlu.to_datetime(dt): ql.ActualActual().yearFraction(_d1, dt) 
           for dt in _dates}
    
    result = pd.DataFrame({
        'df':pd.Series(_df),
        'zr':pd.Series(_zr),
        'tenor':_dts
        })
    
    return result

    
def prep_tenors_in_years(start_date, max_tenor, calendar, frequency=ql.Months, day_count=ql.ActualActual()):
    '''
    Parameters
    ----------
    start_date: ql.Date
        representing the start of the tenor. It will be converted to 0 year fraction.
    max_tenor: float 
        a number represent max tenor in the unit of frequency
    calendar:
        ql.Australia()
    frequency: int
        ql.Months
    day_count: 
        ql.ActualActual().
    Returns
    -------
    output : List, list
        a list of tenor in fraction of years.
        a list tenor in ql.Date
    '''
    
    tenor_dates = [calendar.advance(start_date, int(i), frequency) for i in np.arange(0, max_tenor) + 1]
    tenors = [day_count.yearFraction(start_date, d) for d in tenor_dates]
    return tenors, tenor_dates

def get_spot_zero_discount_curve(fitted_rates, val_name='rates'):
    
    '''
     Parameters
    ----------
    fitted_rates: df    
        representing rates and tenor in ql.dates for the curve.
                                tenors     rates
        August 11th, 2021     0.000000  0.000000
        September 13th, 2021  0.090411  0.000478
        October 11th, 2021    0.167123  0.000317
    val_name: string
        represent the column name of the rate    
    '''
    
    spot_dates = fitted_rates.reset_index()['index'].to_list()
    spot_rates = fitted_rates[val_name].to_list()
    
    
    spot_curve = ql.ZeroCurve(spot_dates, spot_rates, 
                              ql.ActualActual(), ql.Australia(),
                              ql.Linear(), ql.Compounded, ql.Semiannual)
    spot_curve_handle = ql.YieldTermStructureHandle(spot_curve)
    
    return spot_curve_handle
    

def price_bonds(bond_df, settlement_days, pricing_curve_handle):
    pricing_engine = ql.DiscountingBondEngine(pricing_curve_handle)
    instruments = []
    for idx, row in bond_df.iterrows():
        effective_date = qlu.to_ql_date(row['First_Cpn_Date'])
        maturity_date = qlu.to_ql_date(row['MAT'])
        px = ql.SimpleQuote(row['Pxs_Dec_Only'])
        cpn = row['CPN']/100 #in decimal
        
        schedule = ql.Schedule(
            effective_date,
            maturity_date,
            schedule_tenor,
            cdr,
            ql.ModifiedFollowing,
            ql.ModifiedFollowing,
            ql.DateGeneration.Backward,
            False)
        
        bond = ql.FixedRateBond(
            settlement_days, # this the number of days to settle not dates of settle
            100.0, #face value
            schedule, #schedule
            [cpn], #cpn need to be a vector
            ql.ActualActual() #day count
            ) 
        bond.setPricingEngine(pricing_engine)
        instruments.append(bond)
        
    return instruments


def plot_fits(line_series, dot_series, dot_series_label=None, figsize=(10, 10)):
    
    from matplotlib.dates import DateFormatter, YearLocator
    
    error = dot_series - line_series
    error_std = error.std()
        
    fig = plt.figure(figsize=figsize)
    gs = fig.add_gridspec(ncols=1, nrows=2, width_ratios=[1], height_ratios=[1,1])
    
    ax1 = fig.add_subplot(gs[0, 0])
    ax2 = fig.add_subplot(gs[1, 0])
        
    ax1.plot(line_series.index, line_series.values, '--', label='fitted')
    ax1.plot(dot_series.index, dot_series.values, 'o', label='Input Data')
    
    # Set the x-axis to do major ticks on the days and label them like '07/20'
    #ax.xaxis.set_major_locator(YearLocator())
    #ax.xaxis.set_major_formatter(DateFormatter("%b %y"))

    ax1.set_title('Data fit')
    ax1.set_xlabel(line_series.index.name)
    ax1.set_ylabel('rate rate(%)')
    ax1.grid(True)
    leg1 = ax1.legend(fancybox=True)
    leg1.get_frame().set_facecolor('none')
    leg1.get_frame().set_linewidth(0.0)
    
    ax2.plot(error.index, error.values, 'o', label='error')
    ax2.set_xlabel(line_series.index.name)
    ax2.set_ylabel('rate rate(%)')
    ax2.axhline(y=0, color='k', linestyle='-',  lw=0.5)
    ax2.axhline(y=error_std, color='grey', linestyle='--',  lw=0.5)
    ax2.axhline(y=-error_std, color='grey', linestyle='--',  lw=0.5)
    #ax2.grid(True)

        
    
    if dot_series_label is not None:
        i = 0
        for x, y in zip(error.index, error.values):
            label = dot_series_label[i] 
            alpha = 1 if abs(y) > error_std else 0.2
            ax2.annotate(label, # this is the text
                         (x,y), # these are the coordinates to position the label
                         textcoords="offset points", # how to position the text
                         xytext=(0,10), # distance from text to points (x,y)
                         ha='left', # horizontal alignment can be left, right or center
                         rotation=45,
                         size=7,
                         alpha=alpha) 
            
 




#

#data
semis_data_nsw =  semis_data[semis_data['TICKER']=='NSWTC']


#process
 
        
interpolations_obj, interpolation_instruments  = get_bond_interpolations(semis_data_nsw, settlement_date)
zr_for_fit = get_zero_rates_from_interpolation(interpolations_obj['logLinearDiscount'], semis_data_nsw['MAT'].apply(qlu.to_ql_date))



nssm = nssmodel.NSCurveFamily(True)
nssm.estimateParam(zr_for_fit['tenor'], zr_for_fit['zr'])

print('Best fit param: (RSqr=%.3f)' % nssm.rsqr)
print('tau1=%.2f tau2=%.2f intercept=%.3f beta1=%.3f beta2=%.3f beta3=%.3f' % (nssm.tau0, nssm.tau1, nssm.beta0, nssm.beta1, nssm.beta2, nssm.beta3) )



spot_tenors, spot_tenor_dates = prep_tenors_in_years(vdate, 400, cdr)
spot_rates = nssm.getSpot(spot_tenors)
fitted_spot_rates = pd.DataFrame({
    'tenors': pd.Series([0] + spot_tenors, index= [vdate] + spot_tenor_dates),
    'rates': pd.Series([0] + spot_rates.tolist(), index= [vdate] + spot_tenor_dates)
    })
 

fitted_spot_zero_curve_handle = get_spot_zero_discount_curve(fitted_spot_rates)

repriced_bonds = price_bonds(semis_data_nsw, 0, fitted_spot_zero_curve_handle)

repcied_ytm = [b.bondYield(ql.ActualActual(), ql.Compounded, ql.Semiannual) for b in repriced_bonds]

semis_data_nsw['NSS_YTM'] = np.array(repcied_ytm) * 100

semis_data_nsw_plot_data = semis_data_nsw[['MAT', 'YTM', 'NSS_YTM', 'ISIN']].set_index('MAT')

   

plot_fits(semis_data_nsw_plot_data['NSS_YTM'], semis_data_nsw_plot_data['YTM'] , semis_data_nsw_plot_data['ISIN'])

















