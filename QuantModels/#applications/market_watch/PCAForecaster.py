import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
import statsmodels.formula.api as smf
import Haver


y_var = Haver.data('USECON:NMOCNX', startdate='2006-01-01')
df = Haver.data(['USECON:NAPMC', 'USECON:NMFC', 'SURVEYS:BOCGX ', 'SURVEYS:EMGI', 'SURVEYS:KCMCA',
 'SURVEYS:RIMCX', 'SURVEYS:DBACTS', 'SURVEYS:BLAIN', 'SURVEYS:RISRX', 'SURVEYS:DSACTS'],
 frequency='m', startdate="2006-01-01")
# Log difference in CCG to be used as dependent variable
ccgo = (np.log(y_var['nmocnx']) - np.log(y_var['nmocnx'].shift(1))) * 100
ccgo_last = ccgo.last_valid_index() # Month of last observation of Core capital goods
forecast_month = ccgo_last + 1 # First month to be forecast
# Define three ranges: through the last obs of ccgo; through the forecast month; and just the forecast month
reg_samp = pd.period_range(pd.Period('2007-01-01'), ccgo_last, freq='M')
fore_samp = pd.period_range(pd.Period('2007-01-01'), forecast_month, freq='M')
fore_mo_samp = pd.period_range(forecast_month, forecast_month, freq='M')
# Reindex the survey series so that it goes through forecast_month populating nan where no data is available
df = df.reindex(fore_samp, fill_value=np.nan)

for survey in df.columns: # Loop through the surveys dataframe
     last_observed_period = df[survey].last_valid_index()
     if last_observed_period < forecast_month: # If the survey is missing in forecast_month
         # Create a sample which goes from January 2007 to the last month of the survey
         lag_samp = pd.period_range(pd.Period('2007-01-01', freq='M'), last_observed_period, freq='M')
         # Create a sample which starts from the last date of the series+1 and goes through to forecast_month
         forecast_samp = pd.period_range(last_observed_period + 1, forecast_month, freq='M')
         
         # Create dataframe which has the first 2 lags of the dependent variable and drop rows with any NA values
         dfX = pd.concat([df[survey].shift(1), df[survey].shift(2)], axis=1)
         
         dfX_train = dfX.reindex(lag_samp).dropna(axis=0, how='any')
         
         dfX_predict = dfX.reindex(forecast_samp)
         
         eq_survey = LinearRegression().fit(dfX_train, df[survey].loc[dfX_train.index]) # Run the regression
         
         # Forecast the series through to forecast_month and update the Surveys dataframe
         df.update(pd.Series(eq_survey.predict(dfX_predict), name=survey, index=dfX_predict.index))

# Create a pipeline to normalize and then run PCA
p = Pipeline([('normalize', StandardScaler()), ('pca', PCA(n_components=2))])

df_pc = pd.DataFrame(p.fit_transform(df), index=df.index, columns=['pca_{0}'.format(i) for i in range(2)])

# Create an dataframe with X vars: our principal components, and the lag of ccgo
dfX = df_pc.join(ccgo, how='outer')

dfX['ccgo_lag1'] = dfX.nmocnx.shift(1)

dfX = dfX.drop('nmocnx', axis=1)

ols_mean = LinearRegression().fit(dfX.loc[reg_samp], ccgo.loc[reg_samp]) # OLS Regression to predict mean growth

ccgo_gt1 = (ccgo > 1).astype(int) # Logistic Regression to predict probability of growth > 1%

ols_gt1 = LogisticRegression().fit(dfX.loc[reg_samp], ccgo_gt1.loc[reg_samp])

qreg_data = dfX.join(ccgo) # Quantile regression to predict 10th quantile growth

qreg_p10 = smf.quantreg('nmocnx ~ ccgo_lag1 + pca_0 + pca_1 ', qreg_data.loc[reg_samp]).fit(q=0.1)

ccgo_mean_forecast = ols_mean.predict(dfX.loc[fore_mo_samp]) # Make predictions for the forecast month

ccgo_prob_gt1 = ols_gt1.predict_proba(dfX.loc[fore_mo_samp])

ccgo_p10 = qreg_p10.predict(qreg_data.loc[fore_mo_samp])
