from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper

import argparse



def smooth_series(ds, lim=2, half_window=1):

    def smooth(row):

        if np.isnan(row['ds_prev']) or np.isnan(row['ds_next']):
            return row['ds']

        if abs(row['ds_prev'] - row['ds']) > lim and abs(row['ds_next'] - row['ds']) > lim and abs((row['ds_prev'] + row['ds_next'])/2 - row['ds']) > lim:
            return (row['ds_prev'] + row['ds_next'])/2
        else:
            return row['ds']

    data = pd.DataFrame({'ds':ds, 'ds_prev': ds.shift(half_window), 'ds_next': ds.shift(-1*half_window)})

    return data.apply(smooth, axis=1)
 
    

def smooth_rates(data, limit, half_window): 
    out = {}
    for col, ds in data.iteritems():
        out[col] = smooth_series(ds, lim=limit, half_window=half_window)
    
    return pd.DataFrame(out)

def smooth_fwd_rates(data, limit, half_window): 
    
    out = {}
    
    for col, ds in data.iteritems():
        
        if len(col.split('_')) > 1 and int(col.split('_')[1].replace('Y','')) != 0:
            out[col] = smooth_series(ds, lim=limit, half_window=half_window)
        else:
            out[col] = ds
    
    return pd.DataFrame(out)


def restructure(data, tenors, curve_names):
    df_data = {}
    curve_names.sort()    
    for curve in curve_names:
        curve_columns = [curve + '_' + tenor for tenor in tenors]
        curve_data = data[curve_columns].rename(lambda x: x.split('_')[2], axis='columns')
        ds = curve_data.iloc[0]
        df_data[curve] = ds 
    df = pd.DataFrame(df_data)
    df.index.name = 'Tenor'
    return df



def create_pca(data, date_index, momentum_size=None, resample_freq= 'W'):

    data_sub = data[date_index].resample(resample_freq).last() if resample_freq is not None else data[date_index]    
    return pca.PCA(data_sub, rates_pca = True, momentum_size = momentum_size)


def create_combo_mr(data, date_index, elements = 2, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None):
    mrs = {}
    data_sub = data[date_index]
    combos = list(combinations(data.columns, elements))
    instrument = combos[0][0][3:]
    
    for combo in combos:
        _sub_data = data_sub[list(combo)].resample(resample_freq).last() if resample_freq is not None else data_sub[list(combo)]
        data_sub_pca = pca.PCA(_sub_data, rates_pca = True, momentum_size = momentum_size)
        hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_pca.pc_hedge
        mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
        mrs[('_').join(combo) + '_PCW'] = (mr_obj, data_sub_pca)
        if include_normal_weighted_series:
            if len(_sub_data.columns) == 2:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] - _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s - %s' % (_sub_data.columns[0], _sub_data.columns[1])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)
            elif len(_sub_data.columns) == 3:
                hedge_weighted_data = _sub_data[_sub_data.columns[0]] + _sub_data[_sub_data.columns[2]] - 2 * _sub_data[_sub_data.columns[1]]
                hedge_weighted_data.name =   '%s -2 x %s + %s' % (_sub_data.columns[0], _sub_data.columns[1], _sub_data.columns[2])
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                mrs[('_').join(combo) + '_NMW'] = (mr_obj, data_sub_pca)

    return mrs





#command line args
parser = argparse.ArgumentParser(description='---- global rates overview ----')
parser.add_argument('filedate', metavar='filedate', type=str, nargs=1, help='date in yyyy-mm-dd')
parser.add_argument('modeldate', metavar='modeldate', type=str, nargs=1, help='date in yyyy-mm-dd')
parser.add_argument('cross_countries_pca_history', metavar='cross_countries_pca_history', type=int, nargs=1, help='number of the months of history will be used for cross country pca')
parser.add_argument('pair_pca_history', metavar='pair_pca_history', type=int, nargs=1, help='number of the months of history will be used for cross country pca')

args = parser.parse_args()
print(args.filedate)
print(args.modeldate)
print(args.cross_countries_pca_history)
print(args.pair_pca_history)

# data
    
filedate = args.filedate[0]
modeldate = args.modeldate[0]
cross_countries_pca_history = args.cross_countries_pca_history[0]
pair_pca_history = args.pair_pca_history[0]

data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_SWAP_ALL_historical.csv')
#data = pd.read_csv('C:/Temp/test/market_watch/staging/'+ filedate + '_Adhoc_historical.csv')

data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()
#speed up
#data = data[['USD_2Y_2Y','USD_5Y_5Y', 'USD_10Y_10Y']]
#data = smooth_fwd_rates(data, 0.2, 1)




#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)

cross_countries_pca_history = data.index[-1] - DateOffset(years = 0, months=cross_countries_pca_history, days=0)
pair_pca_history = data.index[-1] - DateOffset(years = 0, months=pair_pca_history, days=0)

#countries
#countries = set(data.columns.map(lambda x: x[:3]))

#tickers

#ticker_1y = [c for c in data.columns if '_0Y_1Y' in c and  'NOK' not in c]
#ticker_2y = [c for c in data.columns if '_0Y_2Y' in c and  'NOK' not in c]
#ticker_3y = [c for c in data.columns if '_0Y_3Y' in c and  'NOK' not in c]
#ticker_4y = [c for c in data.columns if '_0Y_4Y' in c and  'NOK' not in c]
#ticker_5y = [c for c in data.columns if '_0Y_5Y' in c and  'NOK' not in c]
#ticker_10y = [c for c in data.columns if '_0Y_10Y' in c and  'NOK' not in c]
#ticker_1y1y = [c for c in data.columns if '_1Y_1Y' in c and  'NOK' not in c]
#ticker_1y2y = [c for c in data.columns if '_1Y_2Y' in c and  'NOK' not in c]
#ticker_1y5y = [c for c in data.columns if '_1Y_5Y' in c and  'NOK' not in c]
#ticker_1y10y = [c for c in data.columns if '_1Y_10Y' in c and  'NOK' not in c]
#ticker_2y1y = [c for c in data.columns if '_2Y_1Y' in c and  'NOK' not in c]
#ticker_2y2y = [c for c in data.columns if '_2Y_2Y' in c and  'NOK' not in c]
#ticker_2y5y = [c for c in data.columns if '_2Y_5Y' in c and  'NOK' not in c]
#ticker_2y10y = [c for c in data.columns if '_2Y_10Y' in c and  'NOK' not in c]
#ticker_3y2y = [c for c in data.columns if '_3Y_2Y' in c and  'NOK' not in c]
#ticker_5y5y = [c for c in data.columns if '_5Y_5Y' in c and  'NOK' not in c]
#ticker_2y2y = [c for c in data.columns if '_2Y_2Y' in c and  'NOK' not in c]
#ticker_30y = [c for c in data.columns if '_0Y_30Y' in c and  'NOK' not in c and 'THB' not in c and 'HKD' not in c and 'NZD' not in c and 'SGD' not in c]


#derived data

#data_22s55s = {}
#if True:
#    for i in np.arange(0, len(ticker_2y2y)):
#         data_22s55s[ticker_2y2y[i][:3] + '22s55s'] =  data[ticker_5y5y[i]] - data[ticker_2y2y[i]]
#    data_22s55s = pd.DataFrame(data_22s55s)


#data_10s30s = {}
#ticker_temp = [c.replace('_0Y_30Y', '_0Y_10Y') for c in ticker_30y]
#if True:
#    for i in np.arange(0, len(ticker_30y)):
#         data_10s30s[ticker_30y[i][:3] + '10s30s'] =  data[ticker_30y[i]] - data[ticker_temp[i]]
#    data_10s30s = pd.DataFrame(data_10s30s)
#data_10s30ss = data_10s30s[['EUR10s30s','JPY10s30s']]



#per_country_mrs = {}


#per_country_mrs = {**per_country_mrs, **create_combo_mr(data_22s55s, data.index > pair_pca_history, elements = 2)}
#per_country_mrs = {**per_country_mrs, **create_combo_mr(data_10s30ss, data.index > pair_pca_history, elements = 2)}
 
#per_country_mrs = {**per_country_mrs, **create_combo_mr(data, data.index > pair_pca_history, elements = 2)}
 

#for mr_desc, (mr_obj, pca_obj) in per_country_mrs.items():
#    print(mr_desc)
#    mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
#    plt.savefig('C:/Temp/test/market_watch/output/global_overview/adhoc/%s_%s_%s.png' % (filedate,modeldate, mr_desc), bbox_inches='tight')
#    plt.close()


#per_country_mrs2 = {**per_country_mrs, **create_combo_mr(data_10s_rtns, data.index > pair_pca_history, elements = 2, resample_freq= 'B')}
 

#for mr_desc, (mr_obj, pca_obj) in per_country_mrs2.items():
#    print(mr_desc)
#    mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
#    plt.savefig('C:/Temp/test/market_watch/output/global_overview/adhoc/%s_%s_%s_returns.png' % (filedate,modeldate, mr_desc), bbox_inches='tight')
#    plt.close()



#####################################################################################################################################################################



#per_tenor_mrs = {}

#per_tenor_mrs = {**per_tenor_mrs, **create_combo_mr(data, data.index > pair_pca_history, elements = 2)}

#for mr_desc, (mr_obj, pca_obj) in per_tenor_mrs.items():
#    print(mr_desc)
#    mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
#    plt.savefig('C:/Temp/test/market_watch/output/global_overview/adhoc/%s_%s_%s.png' % (filedate,modeldate, mr_desc), bbox_inches='tight')
#    plt.close()

 


data_dcit = {}

pcas ={}

data_dcit['US_lvl'] = data[['USD_2Y_2Y','USD_5Y_5Y', 'USD_10Y_10Y']]
data_dcit['AU_lvl']  = data[['AUD_2Y_2Y','AUD_5Y_5Y', 'AUD_10Y_10Y']]
data_dcit['UK_lvl']  = data[['GBP_2Y_2Y','GBP_5Y_5Y', 'GBP_10Y_10Y']]
data_dcit['EU_lvl']  = data[['EUR_2Y_2Y','EUR_5Y_5Y', 'EUR_10Y_10Y']]
#data_dcit['US_rtn'] = data_dcit['US_lvl'] - data_dcit['US_lvl'].shift(1)
#data_dcit['AU_rtn']  = data_dcit['AU_lvl'] - data_dcit['AU_lvl'].shift(1)
#data_dcit['UK_rtn']  = data_dcit['UK_lvl'] - data_dcit['UK_lvl'].shift(1)
#data_dcit['EU_rtn']  = data_dcit['EU_lvl'] - data_dcit['EU_lvl'].shift(1)




for name, data in data_dcit.items():
    pcas = {**pcas, **create_combo_mr(data, data.index > pair_pca_history, elements = 3, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=None)}
    for mr_desc, (mr_obj, pca_obj) in pcas.items():
        print(mr_desc)
        print(mr_obj)
        print(pca_obj)
        mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
        plt.savefig('C:/Temp/test/market_watch/output/global_overview/adhoc/%s_%s_%s_%s.png' % ('lvl', filedate, modeldate, mr_desc), bbox_inches='tight')
        plt.close()
    pcas = {}
    pcas = {**pcas, **create_combo_mr(data, data.index > pair_pca_history, elements = 3, resample_freq= 'W', include_normal_weighted_series=True, momentum_size=1)}
    for mr_desc, (mr_obj, pca_obj) in pcas.items():
        print(mr_desc)
        print(mr_obj)
        print(pca_obj)
        mr_fig = mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
        plt.savefig('C:/Temp/test/market_watch/output/global_overview/adhoc/%s_%s_%s_%s.png' % ('rtn', filedate, modeldate, mr_desc), bbox_inches='tight')
        plt.close()
    pcas = {}