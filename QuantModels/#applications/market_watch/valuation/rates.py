import pandas as pd
import numpy as np
import os, sys, inspect
from operator import itemgetter
import collections
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations, chain
from pykalman import KalmanFilter
import xlwings as xw
from pandas.tseries.offsets import DateOffset
import statsmodels.api as sm
import statsmodels.formula.api as smf
from sklearn.cross_decomposition import PLSRegression

_p = 'C:\Dev\mancave\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\statistics\derivative\pysabr'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\mancave\QuantModels\core'
if _p not in sys.path: sys.path.insert(0, _p)



from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper
from statistics.derivative import bs as bs
from statistics.derivative import quantlibutils as qlutils
from statistics.derivative.pysabr import hagan_2002_normal_sabr as nsabr
from statistics.mr import tools as mrt
from core import dirs
from core import tools as ctls




from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
import scipy.cluster.hierarchy as cl
from scipy.spatial.distance import pdist, squareform

import QuantLib as ql

pd.set_option('display.expand_frame_repr', False)


##############################################################################
####  functions
##############################################################################

def make_filters(l:list)->dict:        
    result = {}
    result['usd_spot'] = [c for c in swap_data_all.columns if ('_0Y_' in c and c.startswith('USD'))]
    result['aud_spot'] = [c for c in swap_data_all.columns if ('_0Y_' in c and c.startswith('AUD'))]
    
    return result


def make_pcas(data:pd.core.frame.DataFrame)->dict:        
    result = {}
    result['usd_spot'] = [c for c in swap_data_all.columns if ('_0Y_' in c and c.startswith('USD'))]
    result['aud_spot'] = [c for c in swap_data_all.columns if ('_0Y_' in c and c.startswith('AUD'))]
    
    return result







    
# def get_1d_rsquared(xs, ys):
#     (b, a) = np.polyfit(xs, ys, deg=1)
#     fitter1d = np.poly1d((b,a))
#     yhat = fitter1d(xs)
#     ybar = ys.mean()
#     ss_tot = ((ys-ybar)**2).sum()
#     ss_res = ((ys-yhat)**2).sum()
#     r_squared = 1 - (ss_res/ss_tot)
#     return r_squared

# rsquared = pd.DataFrame({pc: pd.Series({d:get_1d_rsquared(self.pcs[pc], self.data[d]) for d in self.data.columns}) for pc in self.pcs.columns})

# return rsquared







##############################################################################
####  load data
##############################################################################

f_dt = '2021-07-21'
stg_p = r'C:/Temp/Test/market_watch/staging/'


swap_data_all = ctls.tframe(pd.read_csv(stg_p + f_dt + '_SWAP_ALL_V2_historical.csv'), key='date')
ifs_data_all = ctls.tframe(pd.read_csv(stg_p + f_dt + '_SWIT_historical.csv'), key='date')

sw_flts = make_filters(swap_data_all.columns)


usd_swap_spot = swap_data_all[sw_flts['usd_spot']].ffill()    
usd_swap_spot_pca = pca.PCA(usd_swap_spot.dropna(), rates_pca=True, sample_range = ('2013-01-01', '2019-12-01'))
    
reg_data = usd_swap_spot_pca.pcs.iloc[:,:2]
reg_data['RR'] = (swap_data_all['USD_0Y_2Y'] - ifs_data_all['USD_2Y_SWIT'])
reg_data_shift = reg_data.shift(10).ffill()
reg_data_shift['y'] =  usd_swap_spot['USD_0Y_5Y']




smf_model = smf.ols(formula=r'{0}~{1}+{2}+{3}'.format(reg_data_shift.columns[3], 
                                                  reg_data_shift.columns[0],
                                                  reg_data_shift.columns[1],
                                                  reg_data_shift.columns[2]), 
                    data=reg_data_shift.loc['2013-01-01':,:]).fit()


fitted_y = smf_model.params['Intercept'] + \
            smf_model.params['PC1'] * reg_data['PC1'] + \
            smf_model.params['PC2'] * reg_data['PC2'] + \
            smf_model.params['RR'] * reg_data['RR']


fitted_res2 = pd.DataFrame(fitted_y.values, index=fitted_y.index + BDay(10), columns=['fitted_y'])

fitted_res2['y'] = reg_data_shift['y']

fitted_res2.tail(500).plot()


## PLS

pls = PLSRegression(n_components = 3)
X = usd_swap_spot.ffill().loc['2013-01-01':]
y= (swap_data_all['USD_0Y_2Y'] - ifs_data_all['USD_2Y_SWIT']).ffill().loc['2013-01-01':]
pls.fit(X.values, y.values)

X_loadings = pd.DataFrame(pls.x_loadings_, index=X.columns, columns=['Loadings1','Loadings2','Loadings3'])
X_scores = pd.DataFrame(pls.x_scores_, index=X.index, columns=['Score1','Score2','Score3'])


pls.predict(reg_data.ffill().tail(500))

fitted_res_pls = pd.DataFrame(pls.predict(reg_data.ffill().tail(2000)), 
                              index=reg_data.tail(2000).index + BDay(0), columns=['fitted_y'])

fitted_res_pls['y'] = usd_swap_spot['USD_0Y_30Y']


fitted_res_pls.plot()
















