from __future__ import division
import os, sys, inspect
import logging
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
import matplotlib.cm as cm
import math as math
import numpy as np
import pandas as pd
from datetime import date
from dateutil.relativedelta import relativedelta

####################################################################################
#need to register the converter in order to fix the breaking change from pandas 0.21
# This is an issue as this fix is only required for latest pandas library. BBG machine is on pandas <0.21 so it doesn't need it
####################################################################################

#import pandas.plotting._converter as pandacnv
#pandacnv.register()




def translate_country_name(short_name):
    dict = {
        "US":"USA",
        "UK":"GBR",
        "TH":"THA",
        "TA":"TWN",
        "SK":"KOR",
        "SI":"SGP",
        "PH":"PHL",
        "NZ":"NZL",
        "MA":"MYS",
        "JN":"JPN",
        "IN":"IND",
        "ID":"IDN",
        "EU":"EUR",
        "CH":"CHN",
        "CA":"CAN",
        "AU":"AUS"
    }

    dict2 = {
        "USD":"USA",
        "GBP":"GBR",
        "THB":"THA",
        "TWD":"TWN",
        "SEK":"KOR",
        "SGD":"SGP",
        "PHP":"PHL",
        "NZ":"NZL",
        "MYR":"MYS",
        "JPY":"JPN",
        "INR":"IND",
        "IDR":"IDN",
        "EUR":"EUR",
        "CNY":"CHN",
        "CAD":"CAN",
        "AUD":"AUS"
    }
    if len(short_name) == 2:
        if short_name in dict:
            return dict[short_name]
        else:
            return short_name
    else:
        if short_name in dict2:
            return dict2[short_name]
        else:
            return short_name


class macro_weekly_chart_pack(object):
    def __init__(self,debug_folder, debug=False):
        self._debug_folder = debug_folder
        self._debug = debug
        logging.debug("Done creating macro_weekly_chart_pack object")

    def resample_df_back_weekly_from_latest(cls, df, freq):
        df = df.round(2)
        dt = df.index.max()
        dt_range = [dt - relativedelta(weeks= x) for x in xrange(freq -1, -1, -1)]
        df_out = df.reindex(df.index.union(dt_range)).ffill()
        df_out = df_out.reindex(dt_range)
        df_out.index = [c.strftime("%Y-%m-%d") for c in df_out.index]
        return df_out


    def resample_df_back_monthly_from_latest(cls, df, freq):
        df = df.round(2)
        dt = df.index.max()
        dt_range = [dt - relativedelta(months= x) for x in xrange(freq -1, -1, -1)]
        df_out = df.reindex(df.index.union(dt_range)).ffill()
        df_out = df_out.reindex(dt_range)
        df_out.index = [c.strftime("%Y-%m-%d") for c in df_out.index]
        return df_out


    def divid_by_sign(cls, s, pos=True):

        s = s.fillna(0)

        if pos:
            return s.apply(lambda v: v if v >= 0 else 0)
        else:
            return s.apply(lambda v: v if v <= 0 else 0)

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# api level functions
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    def plot_bar(cls, ax, series, left = None, width=0, color=None, edgecolor="none"):
        width = width
        left = np.arange(len(series)) + width/2.0 if left is None else left
        s = ax.bar(left, series, width, color= color,edgecolor=edgecolor)
        return s


    def plot_stacked_bar(cls, ax, series, left =None, width=0, color=None, edgecolor="none", bottom=None, **kwargs):
        width = width
        left = np.arange(len(series)) + width/2.0 if left is None else left
        s = ax.bar(left, series, width, color= color,edgecolor=edgecolor, bottom=bottom, **kwargs)
        return s


    def plot_dots(cls, x, y, c, s, label, alpha, ax, tick_override=True, legend_pos=None, legend_ncol = 1, legend_font_size = 10, **kwargs):


        #ds = ax.scatter(x, y, c=c, s=s, alpha=0.5, label=label, **kwargs)

        ds = [ax.scatter(x[i], y[i], c=c[i], s=s[i], alpha=alpha[i], label=label[i], **kwargs) for i in xrange(len(x))]

        ax.xaxis.grid(True)
        ax.yaxis.grid(True)

        if legend_pos is None:
            pass
        else:
            if len(x) > 0:
                #ax.legend(loc=legend_pos, fancybox=True, framealpha=0,  ncol=legend_ncol, prop={'size':legend_font_size})
                ax.legend(ds, label, scatterpoints=1, fancybox=True, framealpha=0, loc=legend_pos, ncol=legend_ncol, fontsize=legend_font_size)
            else:
                r = mpl.patches.Rectangle((0,0), 1, 1, fill=False, edgecolor='none',visible=False)
                ax.legend([r],[label], loc=legend_pos, fancybox=True, framealpha=0,  ncol=legend_ncol, prop={'size': legend_font_size})



    def plot_lines(cls, x, y, ax, label =None, legend_pos=None,  legend_ncol = 1, legend_font_size = 10, **kwargs):

        ax.plot(x, y,**kwargs)

        if legend_pos is None:
            pass
        else:
            #if len(x) > 0:
            #    ax.legend(loc=legend_pos, fancybox=True, framealpha=0,  ncol=legend_ncol, prop={'size':legend_font_size})
            #else:
            if label is not None:
                r = mpl.patches.Rectangle((0,0), 1, 1, fill=False, edgecolor='none',visible=False)
                ax.legend([r],[label], loc=legend_pos, fancybox=True, framealpha=0,  ncol=legend_ncol, prop={'size': legend_font_size})




    def plot_stack_area(cls, x, ys, ax, reset_color_cycle = False, series_names = None, legend_pos=None, legend_ncol=1, legend_font_size= 12, **kwargs):

        min_x = x.min()
        max_x = x.max()

        sp = ax.stackplot(x, ys,linewidth=0.0, baseline ='zero', **kwargs)

        ax.set_xlim([min_x, max_x])

        ax.xaxis.grid(True)
        ax.yaxis.grid(True)

        if reset_color_cycle:
            #ax.set_color_cycle(None)
            ax.set_prop_cycle(None)

        if series_names is not None:
            #ax.set_color_cycle(None)
            legend_proxy = [mpl.patches.Rectangle((0,0), 0,0, facecolor=pol.get_facecolor()[0]) for pol in sp]
            ax.legend(legend_proxy, series_names, loc=legend_pos, fancybox=True, framealpha=0,  ncol=legend_ncol, prop={'size':legend_font_size})



    def plot_ts_lines(cls, idx, data, label, fmt, ax, legend_pos=None, legend_ncol = 1, legend_font_size= 12, annotate_value = True, annotate_font_size = 18,  **kwargs):

            """
            A base function plots time series in lines!
            plot_pd_ts(df.index.values, df.col,'b-o')
            """
            min_x = idx.min()
            max_x = idx.max()

            ax.plot_date(idx, data, fmt, label=label,  **kwargs)
            ax.set_xlim([min_x, max_x])

            x = max_x
            y = data[x]

            if annotate_value:
                ax.annotate(str(round(y,2)),
                            (mdates.date2num(x), y),
                            xytext=(12, np.sign(y) * 5.0  + 0.5 * y ),
                            color=kwargs.get('color','black'),
                            textcoords='offset points',
                            arrowprops=dict(arrowstyle='-|>', color=kwargs.get('color','black')),
                            fontsize=annotate_font_size)

            # ax.text(x, y * (1 + 0.05 * y) , round(y,2), color=kwargs.get('color','black'))

            if legend_pos is None:
                pass
            else:
                if len(data.dropna()) > 0:
                    ax.legend(loc=legend_pos, fancybox=True, framealpha=0,  ncol=legend_ncol, prop={'size':legend_font_size})
                else:
                    r = mpl.patches.Rectangle((0,0), 1, 1, fill=False, edgecolor='none',visible=False)
                    ax.legend([r],[label], loc=legend_pos, fancybox=True, framealpha=0,  ncol=legend_ncol, prop={'size': legend_font_size})

#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# ax level functions
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    def plot_EPI_region_and_countries_history_column_chart_ax(cls, df, ax, chart_title, legend_pos = 9, legend_ncol = 5, legend_font_size =9, bottom=None):

        ax.set_ylim((-2.0,2.0))
        ax_twin = ax.twinx()
        ax_twin.set_ylim(-2.0, 2.0)
        ax.yaxis.grid(True)
        ax.yaxis.set_major_locator(MultipleLocator(0.5))
        ax_twin.yaxis.set_major_locator(MultipleLocator(0.5))
        ax.axhline(y=0, color="black")
        width = 0.5

        data = cls.resample_df_back_weekly_from_latest(df, 16)

        h4m = data.iloc[0]
        h1m= data.iloc[12]
        h1w= data.iloc[14]
        hnow= data.iloc[15]


        xaxis_labels = hnow.index.tolist()

        left = np.arange(len(hnow.values)) * 5.3 * width + width

        s4 = cls.plot_bar(ax, h4m.values, left = (left), width=width, color="#d8f0f5", edgecolor ="none")
        s3 = cls.plot_bar(ax, h1m.values, left = (left + width), width=width, color="#bee7ef", edgecolor ="none")
        s2 = cls.plot_bar(ax, h1w.values, left = (left + width*2), width=width, color="#7ED0E0", edgecolor ="none")
        s1 = cls.plot_bar(ax, hnow.values, left = (left + width*3), width=width, color="#0075AD", edgecolor ="none")

        ax.set_xticks(left + width*2)
        ax.set_xticklabels(xaxis_labels)

        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)


        for label,tick in zip(ax.get_yticklabels(),ax.get_yticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax.get_xticklabels(),ax.get_xticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax_twin.get_xticklabels(),ax_twin.get_xticks()):
            label.set_fontsize(10)



        ax.legend([s4,s3,s2,s1], [ h4m.name,h1m.name,h1w.name, hnow.name], fancybox=True, framealpha=0, loc=legend_pos, ncol=legend_ncol, prop={'size':legend_font_size})
        #ax.legend([s4,s3,s2], [ h4m.name, h1m.name,h1w.name], fancybox=True, framealpha=0, loc=legend_pos, ncol=legend_ncol, prop={'size':legend_font_size})

        if chart_title is not None:
            ax.set_title(chart_title,  fontsize = 14, y = 1.02,  fontweight='bold')


    def plot_fundamental_diff_chart_ax(cls, df, ax, chart_title, legend_pos = 9, legend_ncol = 5, legend_font_size = 10, bottom=None):

        ax.set_ylim((-1.0,1.0))
        ax_twin = ax.twinx()
        ax_twin.set_ylim(-1.0, 1.0)
        ax.yaxis.grid(True)
        ax.yaxis.set_major_locator(MultipleLocator(0.2))
        ax_twin.yaxis.set_major_locator(MultipleLocator(0.2))
        ax.axhline(y=0, color="black")
        width = 0.5

        data = cls.resample_df_back_weekly_from_latest(df, 4)
        data = data[["Fundamental", "Headline_EPI","Growth_ex_Inflation_EPI","FCI","PCI_Lookback","ESI","Macro_Pulse"]]
        data.columns = ["Fundamental \nIndicator", "EPI","Growth ex Inflation \nEPI","FCI","PCI Lookback","ESI","Macro Pulse"]



        diff = data.iloc[-1] - data
        diff1m = diff.iloc[0]
        diff1w = diff.iloc[2]

        xaxis_labels = diff1w.index.tolist()

        left = np.arange(len(diff1w.values)) * 2 + width

        s1 = cls.plot_bar(ax, diff1w.values, left = left, width=width, color="#0075AD", edgecolor ="none")
        s2 = cls.plot_bar(ax, diff1m.values, left = (left + width), width=width, color="#7ED0E0", edgecolor ="none")

        ax.set_xticks(left + width)
        ax.set_xticklabels(xaxis_labels)

        ax.legend([s1, s2], ["1 week change", "1 month change"], fancybox=True, framealpha=0,
                  loc=legend_pos, ncol=legend_ncol, prop={'size':legend_font_size})

        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)

        for label,tick in zip(ax.get_yticklabels(),ax.get_yticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax.get_xticklabels(),ax.get_xticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax_twin.get_xticklabels(),ax_twin.get_xticks()):
            label.set_fontsize(10)


        if chart_title is not None:
            ax.set_title(chart_title,  fontsize = 14, y = 1.02,  fontweight='bold')


    def plot_pci_component_cross_countries_chart_ax(cls, dfs, ax, chart_title, legend_pos = 9, legend_ncol = 6, legend_font_size =9, bottom=None):


        def plot_dots(x, y, c, s, label,
                      alpha, ax, tick_override=True,
                      legend_pos=None, legend_ncol = 1,
                      legend_font_size = 9, **kwargs):

            ds = ax.scatter(x, y, c=c, s=s, alpha=alpha, **kwargs)

            ax.xaxis.grid(True)
            ax.yaxis.grid(True)


            if legend_pos is None:
                pass
            else:
                if len(x) > 0:
                    ax.legend([ds], [label], scatterpoints=1, fancybox=True,
                              framealpha=0, loc=legend_pos, ncol=legend_ncol,
                              fontsize=legend_font_size)

                else:
                    r = mpl.patches.Rectangle((0,0), 1, 1, fill=False, edgecolor='none',visible=False)
                    ax.legend([r],[label], loc=legend_pos, fancybox=True,
                              framealpha=0,  ncol=legend_ncol, prop={'size': legend_font_size})
            return


        ax.set_ylim((-6.0,12.0))
        ax_twin = ax.twinx()
        ax_twin.set_ylim(-1.0, 2.0)
        ax.yaxis.grid(True)
        ax_twin.yaxis.grid(False)
        ax.yaxis.set_major_locator(MultipleLocator(2.0))
        ax.axhline(y=0, color="black")
        width = 0.5


        s1 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=True).values, width=width, color="#0075AD", edgecolor ="none")

        s2 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Govt 10yr"].iloc[0] * -1, pos=True).values, width=width, color="#7ED0E0", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=True).values)

        s3 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Budget balance as % of GDP"].iloc[0] * -1, pos=True).values, width=width, color="#91B9D0", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=True).values +
                     cls.divid_by_sign(dfs["Govt 10yr"].iloc[0] * -1, pos=True).values)

        s4 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Policy rate"].iloc[0] * -1, pos=True).values, width=width, color="#CA6E46", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=True).values +
                     cls.divid_by_sign(dfs["Govt 10yr"].iloc[0] * -1, pos=True).values +
                     cls.divid_by_sign(dfs["Budget balance as % of GDP"].iloc[0] * -1, pos=True).values)

        s5 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Reserve requirement"].iloc[0] * -1, pos=True).values, width=width, color="#BBAE78", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=True).values +
                     cls.divid_by_sign(dfs["Govt 10yr"].iloc[0] * -1, pos=True).values +
                     cls.divid_by_sign(dfs["Budget balance as % of GDP"].iloc[0] * -1, pos=True).values +
                     cls.divid_by_sign(dfs["Policy rate"].iloc[0] * -1, pos=True).values)

        s6 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Balance sheet"].iloc[0], pos=True).values, width=width, color="#252265", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=True).values +
                     cls.divid_by_sign(dfs["Govt 10yr"].iloc[0] * -1, pos=True).values +
                     cls.divid_by_sign(dfs["Budget balance as % of GDP"].iloc[0] * -1, pos=True).values +
                     cls.divid_by_sign(dfs["Policy rate"].iloc[0] * -1, pos=True).values +
                     cls.divid_by_sign(dfs["Reserve requirement"].iloc[0] * -1, pos=True).values)


        s1 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=False).values, width=width, color="#0075AD", edgecolor ="none")

        s2 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Govt 10yr"].iloc[0]  * -1, pos=False).values, width=width, color="#7ED0E0", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=False).values)

        s3 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Budget balance as % of GDP"].iloc[0] * -1, pos=False).values, width=width, color="#91B9D0", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=False).values +
                     cls.divid_by_sign(dfs["Govt 10yr"].iloc[0] * -1, pos=False).values)

        s4 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Policy rate"].iloc[0] * -1, pos=False).values, width=width, color="#CA6E46", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=False).values +
                     cls.divid_by_sign(dfs["Govt 10yr"].iloc[0] * -1, pos=False).values +
                     cls.divid_by_sign(dfs["Budget balance as % of GDP"].iloc[0] * -1, pos=False).values)

        s5 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Reserve requirement"].iloc[0] * -1, pos=False).values, width=width, color="#BBAE78", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=False).values +
                     cls.divid_by_sign(dfs["Govt 10yr"].iloc[0] * -1, pos=False).values +
                     cls.divid_by_sign(dfs["Budget balance as % of GDP"].iloc[0] * -1, pos=False).values +
                     cls.divid_by_sign(dfs["Policy rate"].iloc[0] * -1, pos=False).values)

        s6 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Balance sheet"].iloc[0], pos=False).values, width=width, color="#252265", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["ToT vs TWI"].iloc[0] * -1, pos=False).values +
                     cls.divid_by_sign(dfs["Govt 10yr"].iloc[0] * -1, pos=False).values +
                     cls.divid_by_sign(dfs["Budget balance as % of GDP"].iloc[0] * -1, pos=False).values +
                     cls.divid_by_sign(dfs["Policy rate"].iloc[0] * -1, pos=False).values +
                     cls.divid_by_sign(dfs["Reserve requirement"].iloc[0] * -1, pos=False).values)

        ax.set_xticks(np.arange(len(dfs["ToT vs TWI"].iloc[0].values)) + width)
        ax.set_xticklabels(dfs["ToT vs TWI"].columns.tolist())



        plot_dots(np.arange(len(dfs["PCI"].iloc[0].values)) + width,
                  cls.divid_by_sign(dfs["PCI"].iloc[0]).values,
                  ["grey"],
                  [120],
                  "PCI (RHS)",
                  0.5,
                  ax_twin,
                  legend_pos= 8, legend_ncol = 1, legend_font_size = legend_font_size)


        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)


        ax.legend([s1, s2, s3, s4, s5, s6], ["ToT vs TWI", "Govt 10yr", "Budget balance as % of GDP", "Policy rate","Reserve requirement","Balance sheet"],
                  fancybox=True, framealpha=0, loc=legend_pos, ncol=legend_ncol, prop={'size':legend_font_size})



        for label,tick in zip(ax.get_yticklabels(),ax.get_yticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax.get_xticklabels(),ax.get_xticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax_twin.get_xticklabels(),ax_twin.get_xticks()):
            label.set_fontsize(10)


        if chart_title is not None:
            ax.set_title(chart_title,  fontsize = 14, y = 1.02,  fontweight='bold')



    def plot_epi_component_cross_countries_chart_ax(cls, dfs, ax, chart_title, legend_pos = 9, legend_ncol = 5, legend_font_size =9, bottom=None):

        ax.set_ylim((-6.0,6.0))
        ax_twin = ax.twinx()
        ax_twin.set_ylim(-6.0, 6.0)
        ax.yaxis.grid(True)
        ax.yaxis.set_major_locator(MultipleLocator(2.0))
        ax.axhline(y=0, color="black")
        width = 0.5


        s1 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Business"].iloc[0], pos=True).values, width=width, color="#0075AD", edgecolor ="none")

        s2 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Consumer"].iloc[0], pos=True).values, width=width, color="#7ED0E0", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["Business"].iloc[0], pos=True).values)

        s3 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Employment"].iloc[0], pos=True).values, width=width, color="#91B9D0", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["Business"].iloc[0], pos=True).values +
                     cls.divid_by_sign(dfs["Consumer"].iloc[0], pos=True).values)

        s4 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Growth"].iloc[0], pos=True).values, width=width, color="#CA6E46", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["Business"].iloc[0], pos=True).values +
                     cls.divid_by_sign(dfs["Consumer"].iloc[0], pos=True).values +
                     cls.divid_by_sign(dfs["Employment"].iloc[0], pos=True).values)

        s5 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Inflation"].iloc[0], pos=True).values, width=width, color="#BBAE78", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["Business"].iloc[0], pos=True).values +
                     cls.divid_by_sign(dfs["Consumer"].iloc[0], pos=True).values +
                     cls.divid_by_sign(dfs["Employment"].iloc[0], pos=True).values +
                     cls.divid_by_sign(dfs["Growth"].iloc[0], pos=True).values)


        s1 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Business"].iloc[0], pos=False).values, width=width, color="#0075AD", edgecolor ="none")
        s2 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Consumer"].iloc[0], pos=False).values, width=width, color="#7ED0E0", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["Business"].iloc[0], pos=False).values)

        s3 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Employment"].iloc[0], pos=False).values, width=width, color="#91B9D0", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["Business"].iloc[0], pos=False).values +
                     cls.divid_by_sign(dfs["Consumer"].iloc[0], pos=False).values)

        s4 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Growth"].iloc[0], pos=False).values, width=width, color="#CA6E46", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["Business"].iloc[0], pos=False).values +
                     cls.divid_by_sign(dfs["Consumer"].iloc[0], pos=False).values +
                     cls.divid_by_sign(dfs["Employment"].iloc[0], pos=False).values)

        s5 = cls.plot_stacked_bar(ax, cls.divid_by_sign(dfs["Inflation"].iloc[0], pos=False).values, width=width, color="#BBAE78", edgecolor ="none",
                     bottom = cls.divid_by_sign(dfs["Business"].iloc[0], pos=False).values +
                     cls.divid_by_sign(dfs["Consumer"].iloc[0], pos=False).values +
                     cls.divid_by_sign(dfs["Employment"].iloc[0], pos=False).values +
                     cls.divid_by_sign(dfs["Growth"].iloc[0], pos=False).values)

        ax.set_xticks(np.arange(len(dfs["Consumer"].iloc[0].values)) + width)
        ax.set_xticklabels(dfs["Consumer"].columns.tolist())


        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)


        ax.legend([s1, s2, s3, s4, s5], ["Business", "Consumer", "Employment", "Growth", "Inflation"], fancybox=True, framealpha=0, loc=legend_pos, ncol=legend_ncol, prop={'size':legend_font_size})


        for label,tick in zip(ax.get_yticklabels(),ax.get_yticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax.get_xticklabels(),ax.get_xticks()):
            label.set_fontsize(10)

        for label,tick in zip(ax_twin.get_xticklabels(),ax_twin.get_xticks()):
            label.set_fontsize(10)


        if chart_title is not None:
            ax.set_title(chart_title,  fontsize = 14, y = 1.02,  fontweight='bold')



    def plot_stacked_area_fundamental_ax(cls, df, ax, chart_title):

        ax.set_ylim(-6, 9)
        ax.xaxis.grid(True, which='major')
        ax.xaxis.grid(False, which='minor')
        ax.yaxis.grid(True, which='major')
        ax.axhline(y=0, color="black")

        ax.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b %Y'))

        ax.set_ylabel('Components', fontsize= 10, labelpad = 0)

        ax_twin = ax.twinx()

        #ax_twin.set_ylabel('Zscore', fontsize=12, labelpad = 2)

        #ax_twin.yaxis.set_minor_locator(MultipleLocator(0.5))
        ax_twin.set_ylim(-1, 1.5)
        ax_twin.yaxis.grid(False, which='major')
        ax_twin.yaxis.grid(False, which='minor')


        colors = [ '#0075AD','#7ED0E0',  '#BBAE78', '#DCDEC3','#CD661D', '#000026']

        idx = df.index

        date = idx.max().strftime('%Y-%m-%d')

        Macro_Pulse = cls.divid_by_sign(df.Macro_Pulse, True)
        ESI = cls.divid_by_sign(df.ESI, True)
        Headline_EPI = cls.divid_by_sign(df.Headline_EPI, True)
        Growth_ex_Inflation_EPI = cls.divid_by_sign(df.Growth_ex_Inflation_EPI, True)
        FCI = cls.divid_by_sign(df.FCI, True)
        PCI_Lookback = cls.divid_by_sign(df.PCI_Lookback, True)


        vals = [Headline_EPI, Growth_ex_Inflation_EPI, FCI, PCI_Lookback, ESI , Macro_Pulse]

        names = ['Headline EPI', 'Growth_ex_Inflation_EPI', 'FCI',  'PCI_Lookback', 'ESI', 'Macro_Pulse' ]

        cls.plot_stack_area(idx,vals, ax, reset_color_cycle= True, series_names=names, colors=colors,
                            legend_font_size= 10, legend_pos=9, legend_ncol=7)

        logging.info("Finished drawing the postive half")

        Macro_Pulse = cls.divid_by_sign(df.Macro_Pulse, False)
        ESI = cls.divid_by_sign(df.ESI, False)
        Headline_EPI = cls.divid_by_sign(df.Headline_EPI, False)
        Growth_ex_Inflation_EPI = cls.divid_by_sign(df.Growth_ex_Inflation_EPI, False)
        FCI = cls.divid_by_sign(df.FCI, False)
        PCI_Lookback = cls.divid_by_sign(df.PCI_Lookback, False)

        vals = [Headline_EPI, Growth_ex_Inflation_EPI, FCI, PCI_Lookback, ESI , Macro_Pulse]

        cls.plot_stack_area(idx,vals, ax, reset_color_cycle= True, series_names=names, colors=colors,
                            legend_font_size= 10, legend_pos=9, legend_ncol=7)

        logging.info("Finished drawing the negative half")

        cls.plot_ts_lines(idx, df.Fundamental,"Fundamental Indicator (RHS)", "", ax_twin, legend_pos = 8, legend_ncol = 1, color="#CC0000",
                          legend_font_size = 10, linewidth = 2, ls='-', annotate_font_size = 10)

        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)

        #Make the negative axis tick lable red as per spreadsheet
        for label,tick in zip(ax.get_yticklabels(),ax.get_yticks()):
            label.set_fontsize(10)
            if tick < 0:
                 label.set_color("red")

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(10)
            if tick < 0:
                 label.set_color("red")

        #for label in ax1.get_xticklabels(minor =True):
        #    label.set_fontsize(16)

        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(10)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(8)

        if chart_title is not None:
            #fig1.subtitle(output_file_name, fontsize = 16, y = 1.01,  fontweight='bold', fontname="Times New Roman")
            #ax.set_title(chart_title + ' (' + date +')' , fontsize = 24, y = 1.02,  fontweight='bold', fontname="Times New Roman")
            ax.set_title(chart_title , fontsize = 14, y = 1.02,  fontweight='bold')


    def plot_Macro_Pulse_by_region_chart_ax(cls, df, ax, chart_title):

        ax.set_ylim(-2.5, 2.5)
        ax.xaxis.grid(True, which='major')
        ax.yaxis.grid(True, which='major')

        ax.axhline(y=0, color="black")

        ax.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b %Y'))

        #ax1.set_ylabel('ESIs', fontsize=18, labelpad = 0)

        ax_twin = ax.twinx()

        #ax_twin.set_ylabel('Zscore', fontsize=12, labelpad = 2)
        ax_twin.yaxis.set_minor_locator(MultipleLocator(0.5))

        ax_twin.yaxis.grid(True, which='major')

        ax_twin.set_ylim(-2.5,2.5)

        idx = df.index

        #date = idx.max().strftime('%Y-%m-%d')

        def check_epi_column_empty(df):
            df = df.replace(0,np.NaN)
            return df.dropna().empty


        if not check_epi_column_empty(df[['ASIA']]):
            cls.plot_ts_lines(idx, df['ASIA'], 'ASIA', "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#0075AD", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'ASIA' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'ASIA' else False,
                              zorder=10 if chart_title == 'ASIA' else 1,
                              alpha = 1 if chart_title == 'ASIA' else 0.5)

        if not check_epi_column_empty(df[['BRIC']]):
            cls.plot_ts_lines(idx, df['BRIC'], 'BRIC', "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#e08e7e", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'BRIC' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'BRIC' else False,
                              zorder=10 if chart_title == 'BRIC' else 1,
                              alpha = 1 if chart_title == 'BRIC' else 0.5)

        if not check_epi_column_empty(df[['EUR']]):
            cls.plot_ts_lines(idx, df.DM,"EUR", "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#d091b9", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'EUR' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'EUR' else False,
                              zorder=10 if chart_title == 'EUR' else 1,
                              alpha = 1 if chart_title == 'EUR' else 0.5)

        if not check_epi_column_empty(df[['EM']]):
            cls.plot_ts_lines(idx, df.DM,"EM", "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#182B49", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'EM' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'EM' else False,
                              zorder=10 if chart_title == 'EM' else 1,
                              alpha = 1 if chart_title == 'EM' else 0.5)

        if not check_epi_column_empty(df[['DM']]):
            cls.plot_ts_lines(idx, df.DM,"DM", "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#BBAE78", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'DM' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'DM' else False,
                              zorder=10 if chart_title == 'DM' else 1,
                              alpha = 1 if chart_title == 'DM' else 0.5)

        if not check_epi_column_empty(df[['GLOBAL']]):
            cls.plot_ts_lines(idx, df.GLOBAL,"GLOBAL", "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#859815", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'GLOBAL' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'GLOBAL' else False,
                              zorder=10 if chart_title == 'GLOBAL' else 1,
                              alpha = 1 if chart_title == 'GLOBAL' else 0.5)



        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)

        #Make the negative axis tick lable red as per spreadsheet
        for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
            label.set_fontsize(7)
            if tick < 0:
                 label.set_color("red")

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(7)
            if tick < 0:
                 label.set_color("red")

        #for label in ax1.get_xticklabels(minor =True):
        #    label.set_fontsize(16)

        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(7)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)
        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(7)

        if chart_title is not None:
            ax.set_title(translate_country_name(chart_title) + " Macro Pulse", fontsize = 7, y = 1.02, fontweight='bold')



    def plot_Macro_Pulse_by_G10_chart_ax(cls, df, ax, chart_title):

        ax.set_ylim(-2.5, 2.5)
        ax.xaxis.grid(True, which='major')
        ax.yaxis.grid(True, which='major')

        ax.axhline(y=0, color="black")

        ax.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b %Y'))

        #ax1.set_ylabel('ESIs', fontsize=18, labelpad = 0)

        ax_twin = ax.twinx()

        #ax_twin.set_ylabel('Zscore', fontsize=12, labelpad = 2)
        ax_twin.yaxis.set_minor_locator(MultipleLocator(0.5))

        ax_twin.yaxis.grid(True, which='major')

        ax_twin.set_ylim(-2.5,2.5)

        idx = df.index

        #date = idx.max().strftime('%Y-%m-%d')

        def check_epi_column_empty(df):
            df = df.replace(0,np.NaN)
            return df.dropna().empty


        if not check_epi_column_empty(df[['AUD']]):
            cls.plot_ts_lines(idx, df['AUD'], translate_country_name('AUD'), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#0075AD", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'AUD' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'AUD' else False,
                              zorder=10 if chart_title == 'AUD' else 1,
                              alpha = 1 if chart_title == 'AUD' else 0.5)

        if not check_epi_column_empty(df[['EUR']]):
            cls.plot_ts_lines(idx, df['EUR'], translate_country_name('EUR'), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#e08e7e", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'EUR' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'EUR' else False,
                              zorder=10 if chart_title == 'EUR' else 1,
                              alpha = 1 if chart_title == 'EUR' else 0.5)

        if not check_epi_column_empty(df[['JPY']]):
            cls.plot_ts_lines(idx, df.JPY, translate_country_name("JPY"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#d091b9", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'JPY' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'JPY' else False,
                              zorder=10 if chart_title == 'JPY' else 1,
                              alpha = 1 if chart_title == 'JPY' else 0.5)

        if not check_epi_column_empty(df[['NZD']]):
            cls.plot_ts_lines(idx, df.NZD, translate_country_name("NZD"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#182B49", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'NZD' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'NZD' else False,
                              zorder=10 if chart_title == 'NZD' else 1,
                              alpha = 1 if chart_title == 'NZD' else 0.5)

        if not check_epi_column_empty(df[['USD']]):
            cls.plot_ts_lines(idx, df.USD, translate_country_name("USD"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#BBAE78", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'USD' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'USD' else False,
                              zorder=10 if chart_title == 'USD' else 1,
                              alpha = 1 if chart_title == 'USD' else 0.5)

        if not check_epi_column_empty(df[['CAD']]):
            cls.plot_ts_lines(idx, df.CAD, translate_country_name("CAD"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#859815", legend_font_size = 7, ls='-',
                              annotate_font_size = 7,
                              linewidth = 2 if chart_title == 'CAD' else 1,
                              annotate_value=True if chart_title == 'CAD' else False,
                              zorder=10 if chart_title == 'CAD' else 1,
                              alpha = 1 if chart_title == 'CAD' else 0.5)

        if not check_epi_column_empty(df[['GBP']]):
            cls.plot_ts_lines(idx, df.GBP, translate_country_name("GBP"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#ec450d", legend_font_size = 7, ls='-',
                              annotate_font_size = 7,
                              linewidth = 2 if chart_title == 'GBP' else 1,
                              annotate_value=True if chart_title == 'GBP' else False,
                              zorder=10 if chart_title == 'GBP' else 1,
                              alpha = 1 if chart_title == 'GBP' else 0.5)

        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)


        #Make the negative axis tick lable red as per spreadsheet
        for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
            label.set_fontsize(7)
            if tick < 0:
                 label.set_color("red")

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(7)
            if tick < 0:
                 label.set_color("red")

        #for label in ax1.get_xticklabels(minor =True):
        #    label.set_fontsize(16)

        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(7)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)
        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(7)

        if chart_title is not None:
            ax.set_title(translate_country_name(chart_title) + " Macro Pulse", fontsize = 7, y = 1.02, fontweight='bold')


    def plot_Macro_Pulse_by_ASIA_chart_ax(cls, df, ax, chart_title):

        ax.set_ylim(-2.5, 2.5)
        ax.xaxis.grid(True, which='major')
        ax.yaxis.grid(True, which='major')

        ax.axhline(y=0, color="black")

        ax.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b %Y'))

        #ax1.set_ylabel('ESIs', fontsize=18, labelpad = 0)

        ax_twin = ax.twinx()

        #ax_twin.set_ylabel('Zscore', fontsize=12, labelpad = 2)
        ax_twin.yaxis.set_minor_locator(MultipleLocator(0.5))

        ax_twin.yaxis.grid(True, which='major')

        ax_twin.set_ylim(-2.5,2.5)

        idx = df.index

        #date = idx.max().strftime('%Y-%m-%d')

        def check_epi_column_empty(df):
            df = df.replace(0,np.NaN)
            return df.dropna().empty


        if not check_epi_column_empty(df[['CNY']]):
            cls.plot_ts_lines(idx, df['CNY'], translate_country_name('CNY'), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#0075AD", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'CNY' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'CNY' else False,
                              zorder=10 if chart_title == 'CNY' else 1,
                              alpha = 1 if chart_title == 'CNY' else 0.5)

        if not check_epi_column_empty(df[['HKD']]):
            cls.plot_ts_lines(idx, df['HKD'], translate_country_name('HKD'), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#e08e7e", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'HKD' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'HKD' else False,
                              zorder=10 if chart_title == 'HKD' else 1,
                              alpha = 1 if chart_title == 'HKD' else 0.5)

        if not check_epi_column_empty(df[['INR']]):
            cls.plot_ts_lines(idx, df.INR, translate_country_name("INR"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#d091b9", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'INR' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'INR' else False,
                              zorder=10 if chart_title == 'INR' else 1,
                              alpha = 1 if chart_title == 'INR' else 0.5)

        if not check_epi_column_empty(df[['IDR']]):
            cls.plot_ts_lines(idx, df.IDR, translate_country_name("IDR"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#182B49", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'IDR' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'IDR' else False,
                              zorder=10 if chart_title == 'IDR' else 1,
                              alpha = 1 if chart_title == 'IDR' else 0.5)

        if not check_epi_column_empty(df[['MYR']]):
            cls.plot_ts_lines(idx, df.MYR, translate_country_name("MYR"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#BBAE78", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'MYR' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'MYR' else False,
                              zorder=10 if chart_title == 'MYR' else 1,
                              alpha = 1 if chart_title == 'MYR' else 0.5)

        if not check_epi_column_empty(df[['PHP']]):
            cls.plot_ts_lines(idx, df.PHP, translate_country_name("PHP"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#859815", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'PHP' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'PHP' else False,
                              zorder=10 if chart_title == 'PHP' else 1,
                              alpha = 1 if chart_title == 'PHP' else 0.5)

        if not check_epi_column_empty(df[['SGD']]):
            cls.plot_ts_lines(idx, df.SGD, translate_country_name("SGD"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#ec450d", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'SGD' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'SGD' else False,
                              zorder=10 if chart_title == 'SGD' else 1,
                              alpha = 1 if chart_title == 'SGD' else 0.5)

        if not check_epi_column_empty(df[['TWD']]):
            cls.plot_ts_lines(idx, df.TWD, translate_country_name("TWD"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#ec450d", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'TWD' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'TWD' else False,
                              zorder=10 if chart_title == 'TWD' else 1,
                              alpha = 1 if chart_title == 'TWD' else 0.5)

        if not check_epi_column_empty(df[['KRW']]):
            cls.plot_ts_lines(idx, df.KRW, translate_country_name("KRW"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#0abc37", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'KRW' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'KRW' else False,
                              zorder=10 if chart_title == 'KRW' else 1,
                              alpha = 1 if chart_title == 'KRW' else 0.5)

        if not check_epi_column_empty(df[['THB']]):
            cls.plot_ts_lines(idx, df.THB, translate_country_name("THB"), "", ax, legend_pos = 9, legend_ncol = 4,
                              color="#bc0a36", legend_font_size = 7, ls='-',
                              linewidth = 2 if chart_title == 'THB' else 1,
                              annotate_font_size = 7,
                              annotate_value=True if chart_title == 'THB' else False,
                              zorder=10 if chart_title == 'THB' else 1,
                              alpha = 1 if chart_title == 'THB' else 0.5)


        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)


        #Make the negative axis tick lable red as per spreadsheet
        for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
            label.set_fontsize(7)
            if tick < 0:
                 label.set_color("red")

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(7)
            if tick < 0:
                 label.set_color("red")

        #for label in ax1.get_xticklabels(minor =True):
        #    label.set_fontsize(16)

        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(7)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)
        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(7)

        if chart_title is not None:
            ax.set_title(translate_country_name(chart_title) + " Macro Pulse", fontsize = 7, y = 1.02, fontweight='bold')



    def plot_fci_pci_chart_ax(cls, dfs, ax, chart_title):

        ax.set_ylim(-0.5, 2)

        ax.xaxis.grid(True, which='major')
        ax.xaxis.grid(False, which='minor')

        ax.yaxis.grid(True, which='major')

        #ax.axhline(y=0, color="black")

        ax.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b %Y'))

        #ax1.set_ylabel('ESIs', fontsize=18, labelpad = 0)

        ax_twin = ax.twinx()

        ax_twin.xaxis.grid(False, which='major')

        #ax_twin.set_ylabel('Zscore', fontsize=12, labelpad = 2)
        ax.yaxis.set_major_locator(MultipleLocator(0.5))

        ax_twin.yaxis.grid(True, which='major')

        ax_twin.set_ylim(-4, 1)

        idx = dfs["FCI"].index

        fcidf= dfs["FCI"]

        pcidf= dfs["PCI"]

        #date = idx.max().strftime('%Y-%m-%d')

        def check_column_empty(df):
            df = df.replace(0,np.NaN)
            return df.dropna().empty


        if not check_column_empty(pcidf[[chart_title]]):
            idx = pcidf[[chart_title]].index
            cls.plot_ts_lines(idx, pcidf[chart_title], translate_country_name(chart_title) + " Policy Condition (LHS)", "", ax, legend_pos = 9, legend_ncol = 2,
									 annotate_font_size = 9,
									 color="#00008B", legend_font_size = 10, linewidth = 1, ls='-')


        if not check_column_empty(fcidf[[chart_title]]):
            idx = fcidf[[chart_title]].index
            cls.plot_ts_lines(idx, fcidf[chart_title], translate_country_name(chart_title) + " Financial Condition (RHS)", "", ax_twin, legend_pos = 8, legend_ncol = 2,
									annotate_font_size = 9,
									color="#CA6E46", legend_font_size = 10, linewidth = 1, ls='-')

        ax.set_ylabel('Policy Conditions', fontsize= 10, labelpad = 0)
        ax_twin.set_ylabel('Financial Conditions', fontsize= 10, labelpad = 0)

        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)


        #Make the negative axis tick lable red as per spreadsheet
        for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
            label.set_fontsize(10)
            # if tick < 0:
            #      label.set_color("red")

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(10)
            # if tick < 0:
            #      label.set_color("red")

        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(10)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)
        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(8)

        if chart_title is not None:
            ax.set_title(translate_country_name(chart_title) + " PCI vs FCI", fontsize = 14, y = 1.02, fontweight='bold')






    def plot_esi_chart_ax(cls, df, ax, chart_title):

        ax.set_ylim(-2.5, 2.5)
        ax.xaxis.grid(True, which='major')
        ax.yaxis.grid(True, which='major')

        ax.axhline(y=0, color="black")

        ax.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b %Y'))

        #ax1.set_ylabel('ESIs', fontsize=18, labelpad = 0)

        ax_twin = ax.twinx()

        #ax_twin.set_ylabel('Zscore', fontsize=12, labelpad = 2)
        ax_twin.yaxis.set_minor_locator(MultipleLocator(0.5))

        ax_twin.yaxis.grid(True, which='major')

        ax_twin.set_ylim(-2.5,2.5)

        idx = df.index

        #date = idx.max().strftime('%Y-%m-%d')

        def check_epi_column_empty(df):
            df = df.replace(0,np.NaN)
            return df.dropna().empty

        if not check_epi_column_empty(df[[chart_title]]):
            cls.plot_ts_lines(idx, df[chart_title], translate_country_name(chart_title), "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 8,
                              color="#DC143C", legend_font_size = 8, linewidth = 2, ls='-', zorder=10)

        if not check_epi_column_empty(df[['DM']]):
            cls.plot_ts_lines(idx, df.DM,"DM", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 8,
                              color="#00008B", legend_font_size = 8, linewidth = 1, ls='-')

        if not check_epi_column_empty(df[['ASIA']]):
            cls.plot_ts_lines(idx, df.ASIA,"ASIA", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 8,
                              color="#B0C4DE", legend_font_size = 8, linewidth = 1, ls='-')

        if not check_epi_column_empty(df[['GLOBAL']]):
            cls.plot_ts_lines(idx, df.GLOBAL,"GLOBAL", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 8,
                              color="#008B8B", legend_font_size = 8, linewidth = 1, ls='-')

        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)

        #Make the negative axis tick lable red as per spreadsheet
        for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
            label.set_fontsize(8)
            if tick < 0:
                 label.set_color("red")

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(8)
            if tick < 0:
                 label.set_color("red")


        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(8)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)
        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(8)

        if chart_title is not None:
            ax.set_title(translate_country_name(chart_title) + " ESI", fontsize = 8, y = 1.02, fontweight='bold')


    def plot_Global_esi_chart_ax(cls, df, ax, chart_title):

        ax.set_ylim(-2.5, 2.5)
        ax.xaxis.grid(True, which='major')
        ax.yaxis.grid(True, which='major')

        ax.axhline(y=0, color="black")

        ax.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b %Y'))

        #ax1.set_ylabel('ESIs', fontsize=18, labelpad = 0)

        ax_twin = ax.twinx()

        #ax_twin.set_ylabel('Zscore', fontsize=12, labelpad = 2)
        ax_twin.yaxis.set_minor_locator(MultipleLocator(0.5))

        ax_twin.yaxis.grid(True, which='major')

        ax_twin.set_ylim(-2.5,2.5)

        idx = df.index

        #date = idx.max().strftime('%Y-%m-%d')

        def check_epi_column_empty(df):
            df = df.replace(0,np.NaN)
            return df.dropna().empty


        if not check_epi_column_empty(df[['DM']]):
            cls.plot_ts_lines(idx, df.DM,"DM", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#00008B", legend_font_size = 10, linewidth = 1, ls='-')

        if not check_epi_column_empty(df[['ASIA']]):
            cls.plot_ts_lines(idx, df.ASIA,"ASIA", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#B0C4DE", legend_font_size = 10, linewidth = 1, ls='-')

        if not check_epi_column_empty(df[['GLOBAL']]):
            cls.plot_ts_lines(idx, df.GLOBAL,"GLOBAL", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#008B8B", legend_font_size = 10, linewidth = 1, ls='-')

        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)

        #Make the negative axis tick lable red as per spreadsheet
        for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
            label.set_fontsize(8)
            if tick < 0:
                 label.set_color("red")

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(10)
            if tick < 0:
                 label.set_color("red")


        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(10)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)
        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(8)

        if chart_title is not None:
            ax.set_title(chart_title, fontsize = 14, y = 1.02, fontweight='bold')


    def plot_epi_component_ax(cls, df, ax, chart_title):

        ax.set_ylim(-2.5, 2.5)
        ax.xaxis.grid(True, which='major')
        ax.xaxis.grid(False, which='minor')
        ax.yaxis.grid(True, which='major')
        #ax1.yaxis.grid(False, which='minor', color="#848381")
        ax.axhline(y=0, color="black")

        #ax1.tick_params(axis='x', labelsize=3)

        #ax1.xaxis.set_minor_locator(mpd.DayLocator(bymonthday=(1)))
        #ax1.xaxis.set_minor_locator(mpd.MonthLocator(bymonth=(1,4,7,9)))
        #ax1.xaxis.set_minor_formatter(mpd.DateFormatter('%d'))
        #ax1.xaxis.set_minor_formatter(mpd.DateFormatter('\n%b \n%Y'))


        #ax1.xaxis.set_major_locator(mpd.YearLocator(1, month=1, day=1))
        ax.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b %Y'))



        #ax1.yaxis.set_minor_locator(MultipleLocator(0.5))
        ax.set_ylabel('EPI Components and headline', fontsize=9, labelpad = 0)

        ax_twin = ax.twinx()

        ax.yaxis.set_major_locator(MultipleLocator(0.5))
        ax_twin.yaxis.set_major_locator(MultipleLocator(1.0))

        ax_twin.yaxis.grid(True, which='major')
        ax_twin.yaxis.grid(False, which='minor')

        ax_twin.set_ylim(-2.5,2.5)

        #for tick in ax1.xaxis.get_minor_ticks():
        #        tick.label.set_fontsize(4)

        idx = df.index

        date = idx.max().strftime('%Y-%m-%d')


        def check_epi_column_empty(df):
            df = df.replace(0,np.NaN)
            return df.dropna().empty

        if not check_epi_column_empty(df[['Business']]):
            cls.plot_ts_lines(idx, df.Business,"Business", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#0075AD",
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['Consumer']]):
            cls.plot_ts_lines(idx, df.Consumer,"Consumer", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#7ED0E0",
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['Employment']]):
            cls.plot_ts_lines(idx, df.Employment,"Employment", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#91B9D0",
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['Growth']]):
            cls.plot_ts_lines(idx, df.Growth,"Growth", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#CA6E46",
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['Inflation']]):
            cls.plot_ts_lines(idx, df.Inflation,"Inflation", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#BBAE78",
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['Headline']]):
            cls.plot_ts_lines(idx, df.Headline,"Headline", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#182B49",
                              legend_font_size = 10, linewidth = 2, ls='--')


        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)


        #Make the negative axis tick lable red as per spreadsheet
        for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
            label.set_fontsize(10)
            if tick < 0:
                 label.set_color("red")

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(10)
            if tick < 0:
                 label.set_color("red")

        #for label in ax1.get_xticklabels(minor =True):
        #    label.set_fontsize(16)

        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(10)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)
        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(8)

        if chart_title is not None:
            #ax1.set_title(chart_title + ' (' + date +')', fontsize = 24, y = 1.02,  fontweight='bold', fontname="Times New Roman")
            ax.set_title(chart_title, fontsize = 14, y = 1.02, fontweight='bold')



    def plot_epi_by_region_ax(cls, df, ax, chart_title):

        ax.set_ylim(-2.5, 2.5)
        ax.xaxis.grid(True, which='major')
        ax.xaxis.grid(False, which='minor')
        ax.yaxis.grid(True, which='major')
        #ax1.yaxis.grid(False, which='minor', color="#848381")
        ax.axhline(y=0, color="black")

        #ax1.xaxis.set_major_locator(mpd.YearLocator(1, month=1, day=1))
        ax.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b %Y'))

        ax.set_ylabel('EPIs', fontsize=10, labelpad = 0)

        ax_twin = ax.twinx()

        #ax_twin.set_ylabel('Zscore', fontsize=12, labelpad = 2)
        ax.yaxis.set_major_locator(MultipleLocator(0.5))
        ax.yaxis.set_major_locator(MultipleLocator(1.0))
        ax_twin.yaxis.set_major_locator(MultipleLocator(1.0))

        ax_twin.yaxis.grid(True, which='major')
        ax_twin.yaxis.grid(False, which='minor')

        ax_twin.set_ylim(-2.5,2.5)

        #for tick in ax1.xaxis.get_minor_ticks():
        #        tick.label.set_fontsize(4)

        idx = df.index

        date = idx.max().strftime('%Y-%m-%d')

        def check_epi_column_empty(df):
            df = df.replace(0,np.NaN)
            return df.dropna().empty

        if not check_epi_column_empty(df[['G10']]):
            cls.plot_ts_lines(idx, df.G10,"G10", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 10,
                              color="#0075AD",
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['Asia']]):
            cls.plot_ts_lines(idx, df.Asia,"Asia", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 10,
                              color="#7ED0E0",
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['EEurope']]):
            cls.plot_ts_lines(idx, df.EEurope,"EEurope", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 10,
                              color="#91B9D0",
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['Latam']]):
            cls.plot_ts_lines(idx, df.Latam, "LATAM", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 10,
                              color="#182B49",
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['Global']]):
            cls.plot_ts_lines(idx, df.Global,"Global", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 10,
                              color="#BBAE78",
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['Global_Gdp_weighted']]):
            cls.plot_ts_lines(idx, df.Global_Gdp_weighted, "Global(GDP)", "", ax, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 10,
                              color="#CA6E46",
                              legend_font_size = 10, linewidth = 2, ls='-')

        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)


        #Make the negative axis tick lable red as per spreadsheet
        for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
            label.set_fontsize(10)
            if tick < 0:
                 label.set_color("red")

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(10)
            if tick < 0:
                 label.set_color("red")

        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(9)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(9)

        if chart_title is not None:
            #ax1.set_title(chart_title + ' (' + date +')', fontsize = 24, y = 1.02,  fontweight='bold', fontname="Times New Roman")
            ax.set_title(chart_title, fontsize = 14, y = 1.02, fontweight='bold')


    def plot_epi_by_g10_countries_ax(cls, df, ax, chart_title):

        ax.set_ylim(-2.5, 2.5)
        ax.xaxis.grid(True, which='major')
        ax.xaxis.grid(False, which='minor')
        ax.yaxis.grid(True, which='major')
        #ax1.yaxis.grid(False, which='minor', color="#848381")
        ax.axhline(y=0, color="black")

        #ax1.xaxis.set_major_locator(mpd.YearLocator(1, month=1, day=1))
        ax.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax.xaxis.set_major_formatter(mpd.DateFormatter('%b %Y'))

        ax.set_ylabel('EPIs', fontsize=10, labelpad = 0)

        ax_twin = ax.twinx()

        #ax_twin.set_ylabel('Zscore', fontsize=12, labelpad = 2)
        ax.yaxis.set_major_locator(MultipleLocator(0.5))
        ax_twin.yaxis.set_major_locator(MultipleLocator(1.0))
        ax.yaxis.set_major_locator(MultipleLocator(1.0))

        ax_twin.yaxis.grid(True, which='major')
        ax_twin.yaxis.grid(False, which='minor')

        ax_twin.set_ylim(-2.5,2.5)

        #for tick in ax1.xaxis.get_minor_ticks():
        #        tick.label.set_fontsize(4)

        idx = df.index

        date = idx.max().strftime('%Y-%m-%d')

        def check_epi_column_empty(df):
            df = df.replace(0,np.NaN)
            return df.dropna().empty

        if not check_epi_column_empty(df[['AUD']]):
            cls.plot_ts_lines(idx, df.AUD,"AUD", "", ax, legend_pos = 9, legend_ncol = 7, color="#0075AD",
                              annotate_font_size = 10,
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['CAD']]):
            cls.plot_ts_lines(idx, df.CAD,"CAD", "", ax, legend_pos = 9, legend_ncol = 7, color="#7ED0E0",
                              annotate_font_size = 10,
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['EUR']]):
            cls.plot_ts_lines(idx, df.EUR,"EUR", "", ax, legend_pos = 9, legend_ncol = 7, color="#91B9D0",
                              annotate_font_size = 10,
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['JPY']]):
            cls.plot_ts_lines(idx, df.JPY, "JPY", "", ax, legend_pos = 9, legend_ncol = 7, color="#182B49",
                              annotate_font_size = 10,
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['NZD']]):
            cls.plot_ts_lines(idx, df.NZD,"NZD", "", ax, legend_pos = 9, legend_ncol = 7, color="#BBAE78",
                              annotate_font_size = 10,
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['USD']]):
            cls.plot_ts_lines(idx, df.USD, "USD", "", ax, legend_pos = 9, legend_ncol = 7, color="#CA6E46",
                              annotate_font_size = 10,
                              legend_font_size = 10, linewidth = 2, ls='-')

        if not check_epi_column_empty(df[['GBP']]):
            cls.plot_ts_lines(idx, df.GBP, "GBP", "", ax, legend_pos = 9, legend_ncol = 7, color="#85B615",
                              annotate_font_size = 10,
                              legend_font_size = 10, linewidth = 2, ls='-')

        for i in ax.spines.itervalues():
            i.set_linewidth(0.5)

        for i in ax_twin.spines.itervalues():
            i.set_linewidth(0.5)

        #Make the negative axis tick lable red as per spreadsheet
        for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
            label.set_fontsize(10)
            if tick < 0:
                 label.set_color("red")

        for label,tick in zip(ax_twin.get_yticklabels(),ax_twin.get_yticks()):
            label.set_fontsize(10)
            if tick < 0:
                 label.set_color("red")

        #for label in ax1.get_xticklabels(minor =True):
        #    label.set_fontsize(16)

        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(10)

        for tick in ax.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        for tick in ax_twin.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)
        for tick in ax.get_xaxis().get_major_ticks():
            tick.set_pad(8)

        if chart_title is not None:
            #ax1.set_title(chart_title + ' (' + date +')', fontsize = 24, y = 1.02,  fontweight='bold', fontname="Times New Roman")
            ax.set_title(chart_title, fontsize = 14, y = 1.02, fontweight='bold')


    def plot_epi_quardrant_ax(cls, xs, ys, sizes, labels, alphas, colors, ax, chart_title = None):

        def plot_dots(x, y, c, s, label,
                      alpha, ax, tick_override=True,
                      legend_pos=None, legend_ncol = 1,
                      legend_font_size = 9, **kwargs):

            ds = [ax.scatter(x[i], y[i], c=c[i], s=s[i], alpha=alpha[i], label=label[i], **kwargs) for i in xrange(len(x))]

            ax.xaxis.grid(True)
            ax.yaxis.grid(True)

            for i, txt in enumerate(label):
                ax.annotate(txt,  xy = (x[i][0],y[i][0]), xycoords='data',
                            xytext=(4, 4), textcoords='offset points',
                            fontsize=9)

            if legend_pos is None:
                pass
            else:
                if len(x) > 0:
                    ax.legend(ds, label, scatterpoints=1, fancybox=True,
                              framealpha=0, loc=legend_pos, ncol=legend_ncol,
                              fontsize=legend_font_size,
                              bbox_to_anchor=(1.21, 0.5))

                else:
                    r = mpl.patches.Rectangle((0,0), 1, 1, fill=False, edgecolor='none',visible=False)
                    ax.legend([r],[label], loc=legend_pos, fancybox=True,
                              framealpha=0,  ncol=legend_ncol, prop={'size': legend_font_size},
                              bbox_to_anchor=(1.21, 0.5))



        # Move left y-axis and bottim x-axis to centre, passing through (0,0)
        ax.spines['left'].set_position('center')
        ax.spines['bottom'].set_position('center')
        # Eliminate upper and right axes
        ax.spines['right'].set_color('none')
        ax.spines['top'].set_color('none')
        # Show ticks in the left and lower axes only
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')

        ax.xaxis.set_major_locator(MultipleLocator(0.5))
        ax.yaxis.set_major_locator(MultipleLocator(0.5))

        ax.xaxis.grid(True, which='major')
        ax.yaxis.grid(True, which='major')

        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(9)

        for label in ax.get_yticklabels(minor =False):
            label.set_fontsize(9)

        ax.set_ylabel('EPI Momentun', fontsize=9)
        ax.yaxis.set_label_coords(0.05, 0.12)

        ax.set_xlabel('EPI Level', fontsize=9)
        ax.xaxis.set_label_coords(0.92, -0.025)


        #plot the end point
        plot_dots(xs,
                  ys,
                  colors,
                  sizes,
                  labels,
                  alphas,
                  ax, legend_pos= 5, legend_ncol = 1, legend_font_size = 9)


        if chart_title is not None:
            #fig1.subtitle(output_file_name, fontsize = 16, y = 1.01,  fontweight='bold', fontname="Times New Roman")
            ax.set_title(chart_title,  fontsize = 12, y = 1.02,  fontweight='bold')



    def plot_epi_snail_trail_ax(cls, df, ax, chart_title = None, dot_colors = None):

        # Move left y-axis and bottim x-axis to centre, passing through (0,0)
        ax.spines['left'].set_position('center')
        ax.spines['bottom'].set_position('center')
        # Eliminate upper and right axes
        ax.spines['right'].set_color('none')
        ax.spines['top'].set_color('none')
        # Show ticks in the left and lower axes only
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')


        ax.xaxis.set_major_locator(MultipleLocator(0.5))
        ax.yaxis.set_major_locator(MultipleLocator(0.5))
        ax.xaxis.grid(True, which='major')
        ax.yaxis.grid(True, which='major')

        for label in ax.get_xticklabels(minor =False):
            label.set_fontsize(9)

        for label in ax.get_yticklabels(minor =False):
            label.set_fontsize(9)

        ax.set_ylabel('EPI Momentun', fontsize=9)
        ax.yaxis.set_label_coords(0.05, 0.12)

        ax.set_xlabel('EPI Level', fontsize=9)
        ax.xaxis.set_label_coords(0.92, -0.025)

        start_x = df.x.loc[df.first_valid_index()]
        last_x = df.x.loc[df.last_valid_index()]
        start_y = df.y.loc[df.first_valid_index()]
        last_y = df.y.loc[df.last_valid_index()]


        #plot the line
        cls.plot_lines(df.x, df.y, ax,
                   label="Headline", legend_pos=None,  legend_ncol = 1, legend_font_size = 9,
                   color="grey", alpha=0.2, linestyle='-', markerfacecolor='blue', linewidth = 3
                    #marker='.', markersize=1,
                  )

        #plot the dots
        cmap = plt.cm.get_cmap('bwr_r')
        mapable = ax.scatter(df.x, df.y, c=dot_colors, marker='o', s = 16, lw = 0.1, cmap=cmap) if dot_colors is not None else None

        #plot the end point
        cls.plot_dots([[start_x],[last_x]],
                  [[start_y],[last_y]],
                  ["red","blue"],
                  [95,95],
                  ["2YR ago","Now"],
                  [0.8,0.8],
                  ax, legend_pos=1, legend_ncol = 1, legend_font_size = 9)


        if chart_title is not None:
            #fig1.subtitle(output_file_name, fontsize = 16, y = 1.01,  fontweight='bold', fontname="Times New Roman")
            ax.set_title(chart_title,  fontsize = 12, y = 1.02,  fontweight='bold')

        return mapable





#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# runner level functions
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    #---------------------------------------------------------------------------------------------------------------------
    def plot_country_credit_spread_chart(cls, df, width=12, height=6, output_dir=None, dpi = 100):
    #---------------------------------------------------------------------------------------------------------------------
    
        
        logging.info("===> start to plot_country_credit_spread_chart <===")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))

        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])

        ax1 = plt.subplot(gs[0])

        ax1.set_ylim(0, 700)
        ax1.xaxis.grid(True, which='major')
        ax1.xaxis.grid(False, which='minor')
        ax1.yaxis.grid(True, which='major')

        ax1.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax1.xaxis.set_major_formatter(mpd.DateFormatter('%Y'))

        idx = df.index
        date = idx.max().strftime('%Y-%m-%d')

        cls.plot_ts_lines(idx, df.AU,"Australia Credit Spread", "", ax1, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#bbae78",
                              legend_font_size = 10, linewidth = 2, ls='-')


        cls.plot_ts_lines(idx, df.EU,"European Credit Spread", "", ax1, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#8c4601",
                              legend_font_size = 10, linewidth = 2, ls='-')


        cls.plot_ts_lines(idx, df.US,"US Credit Spread", "", ax1, legend_pos = 9, legend_ncol = 6,
                              annotate_font_size = 9,
                              color="#0075ad",
                              legend_font_size = 10, linewidth = 2, ls='-')

        
        ax1.set_ylabel('Spread vs Government (bps)', fontsize= 10, labelpad = 0)
        ax1.spines['right'].set_visible(False)
        ax1.spines['top'].set_visible(False)
        

        for label in ax1.get_xticklabels(minor =False):
                label.set_fontsize(10)
                
        for label in ax1.get_yticklabels(minor =False):
            label.set_fontsize(10)

        for tick in ax1.get_yaxis().get_major_ticks():
            tick.set_pad(4)

        ax1.xaxis.set_ticks_position('bottom')
        ax1.yaxis.set_ticks_position('left')

        for tick in ax1.get_xaxis().get_major_ticks():
            tick.set_pad(8)


        if  output_dir is not None:
            f = os.path.join(output_dir, "Country_Credit_spreads.png")
            logging.info("Output chart as " + f)
            #fig1.tight_layout()
            #plt.tight_layout()
            plt.savefig(f, dpi = dpi, bbox_inches='tight')
            plt.close()


        logging.info("===> end plot_country_credit_spread_chart <===")




    #---------------------------------------------------------------------------------------------------------------------
    def plot_regime_blended_credit_spread_chart(cls, df, width=12, height=6, output_dir=None, dpi = 100):
    #---------------------------------------------------------------------------------------------------------------------
        logging.info("===> start to plot_regime_blended_credit_spread_chart <===")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))
        gs = gridspec.GridSpec(1, 1, height_ratios=[1])

        ax1 = plt.subplot(gs[0])

        idx = df.index
        date = idx.max().strftime('%Y-%m-%d')

        base = df.Base
        rockBottom = df.RockBottom
        midLatecycle = df.MidLateCycle
        transition = df.Transition
        reflationRecession = df.ReflationRecession

        stacked_area_colors = ['#F2F2F2', '#FDDDDF', '#fffad1', '#c6f2ef', '#3bb2dd']
        stacked_series = [base, rockBottom, midLatecycle, transition, reflationRecession]
        stacked_names = [None, 'Rock Buttom', 'Mid/Late Cycle', 'Transition', 'Reflation/Recession']

        sp = ax1.stackplot(idx, stacked_series, linewidth=0.0, baseline ='zero', colors=stacked_area_colors)
        ax1.set_xlim([idx.min(), idx.max()])
    

        legend_proxy = [mpl.patches.Rectangle((0,0), 0,0, facecolor=pol.get_facecolor()[0], edgecolor=None, lw=0) for pol in sp]
        ax1.legend(legend_proxy[1:], stacked_names[1:], loc=9, fancybox=True, framealpha=0,  ncol=4, prop={'size':10})

        ax1.set_ylim(0, 600)
        ax1.xaxis.grid(True, which='major')
        ax1.xaxis.grid(False, which='minor')
        #ax1.yaxis.grid(True, which='major')
        ax1.axhline(y=0, color="black")
        ax1.xaxis.set_major_locator(mpd.MonthLocator(bymonth=(1)))
        ax1.xaxis.set_major_formatter(mpd.DateFormatter('%Y'))
        ax1.set_ylabel('Blended Credit Spread', fontsize= 10, labelpad = 0)
        ax1.spines['right'].set_visible(False)
        ax1.spines['top'].set_visible(False)


        ax1_twin = ax1.twinx()
        ax1_twin.spines['right'].set_visible(False)
        ax1_twin.spines['top'].set_visible(False)


        cls.plot_ts_lines(idx, df.BlendedCreditSpread,"Blended Credit Spread", "", ax1_twin, legend_pos = 8, legend_ncol = 1, color="#000000",
                          legend_font_size = 10, linewidth = 2, ls='-', annotate_font_size = 10)

        ax1_twin.set_ylim(0, 600)
        ax1_twin.tick_params(labelbottom=False, labelright=False, right=False)    

        #for i in ax1.spines['left'].itervalues():
        #    i.set_linewidth(0.5)

        #for i in ax1_twin.spines.itervalues():
        #    i.set_linewidth(0.5)

        for label in ax1.get_xticklabels(minor =False):
            label.set_fontsize(10)
                
        for label in ax1.get_yticklabels(minor =False):
            label.set_fontsize(10)

        #for label in ax1_twin.get_yticklabels(minor =False):
        #    label.set_fontsize(10)

        for tick in ax1.get_yaxis().get_major_ticks():
            tick.set_pad(4)
            #tick.label.set_fontsize(6)

        #for tick in ax1_twin.get_yaxis().get_major_ticks():
        #    tick.set_pad(4)
            #tick.label.set_fontsize(6)

        ax1.xaxis.set_ticks_position('bottom')
        ax1.yaxis.set_ticks_position('left')

        for tick in ax1.get_xaxis().get_major_ticks():
            tick.set_pad(8)

        ax1.set_title("Blended Credit Spread" , fontsize = 14, y = 1.02,  fontweight='bold')

        #need to uncomment
        h = ax1.get_legend()
        h.get_frame().set_linewidth(0.0)


        if  output_dir is not None:
            f = os.path.join(output_dir, "Blended_Credit_spread_with_regimes.png")
            logging.info("Output chart as " + f)
            #fig1.tight_layout()
            #plt.tight_layout()
            plt.savefig(f, dpi = dpi, bbox_inches='tight')
            plt.close()


        logging.info("===> end plot_regime_blended_credit_spread_chart <===")

    

    
    
    def plot_Macro_Pulse_by_region_chart(cls, df, output_file_name = None, width=12, height=6, output_dir=None, dpi = 100 ):

        logging.info("===> start to plot_Macro_Pulse_by_region_chart <===")

        def plot_chart_batch(countries):
            plt.close()
            fig1 = plt.figure(figsize=(width,height))

            gs = gridspec.GridSpec(2, 3, height_ratios=[1,1], width_ratios=[1,1,1])

            axes = [plt.subplot(gs[i]) for i in np.arange(len(countries))]

            for idx, country in enumerate(countries):
                cls.plot_Macro_Pulse_by_region_chart_ax(df, axes[idx], country)

            if output_dir is not None:
                f = os.path.join(output_dir,  "Macro_Pulse_" +  "_".join(map(translate_country_name,countries)) + ".png")
                logging.info("Output chart as " + f)
                fig1.tight_layout()
                #plt.tight_layout()
                plt.savefig(f, dpi = dpi, bbox_inches='tight' )
                plt.close()

        #part 1
        plot_chart_batch(["ASIA","BRIC","EUR","EM","DM","GLOBAL"])

        logging.info("===> end to plot_Macro_Pulse_by_region_chart <===")


    def plot_Macro_Pulse_by_G10_chart(cls, df, output_file_name = None, width=12, height=8, output_dir=None, dpi = 240 ):

        logging.info("===> start to plot_Macro_Pulse_by_G10_chart <===")

        def plot_chart_batch(countries):
            plt.close()
            fig1 = plt.figure(figsize=(width,height))

            gs = gridspec.GridSpec(2, 4, height_ratios=[1,1], width_ratios=[1,1,1,1])

            axes = [plt.subplot(gs[i]) for i in np.arange(len(countries))]

            for idx, country in enumerate(countries):
                cls.plot_Macro_Pulse_by_G10_chart_ax(df, axes[idx], country)

            if output_dir is not None:
                f = os.path.join(output_dir,  "Macro_Pulse_" +  "_".join(map(translate_country_name,countries)) + ".png")
                logging.info("Output chart as " + f)
                fig1.tight_layout()
                #plt.tight_layout()
                plt.savefig(f, dpi = dpi, bbox_inches='tight' )
                plt.close()

        #part 1
        plot_chart_batch(["AUD","EUR","JPY","NZD","USD","GBP","CAD"])

        logging.info("===> end to plot_Macro_Pulse_by_G10_chart <===")

    def plot_Macro_Pulse_by_ASIA_chart(cls, df, output_file_name = None, width=12, height=6, output_dir=None, dpi = 100 ):

        logging.info("===> start to plot_Macro_Pulse_by_ASIA_chart <===")

        def plot_chart_batch(countries):
            plt.close()
            fig1 = plt.figure(figsize=(width,height))

            gs = gridspec.GridSpec(2, 3, height_ratios=[1,1], width_ratios=[1,1,1])

            axes = [plt.subplot(gs[i]) for i in np.arange(len(countries))]

            for idx, country in enumerate(countries):
                cls.plot_Macro_Pulse_by_ASIA_chart_ax(df, axes[idx], country)

            if output_dir is not None:
                f = os.path.join(output_dir, "Macro_Pulse_" +  "_".join(map(translate_country_name,countries)) + ".png")
                logging.info("Output chart as " + f)
                fig1.tight_layout()
                #plt.tight_layout()
                plt.savefig(f, dpi = dpi, bbox_inches='tight' )
                plt.close()

        #part 1
        plot_chart_batch(["CNY","HKD","INR","IDR","MYR","PHP"])

        #part 2
        plot_chart_batch(["SGD","KRW","TWD","THB"])


        logging.info("===> end to plot_Macro_Pulse_by_ASIA_chart <===")

    def plot_ESI_line_chart(cls, df, output_file_name = None, width=12, height=6, output_dir=None, dpi = 100 ):
        logging.info("===> start to plot_ESI_line_chart <===")


        def plot_chart_batch(countries):
            plt.close()
            fig1 = plt.figure(figsize=(width,height))
            gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])

            axes = [plt.subplot(gs[i]) for i in np.arange(len(countries))]

            for idx, country in enumerate(countries):
                cls.plot_esi_chart_ax(df, axes[idx], country)

            if output_dir is not None:
                f = os.path.join(output_dir, "ESI_" + "_".join(map(translate_country_name,countries)) + ".png")
                logging.info("Output chart as " + f)
                fig1.tight_layout()
                #plt.tight_layout()
                plt.savefig(f, dpi = dpi, bbox_inches='tight' )
                plt.close()

        #part 1
        plot_chart_batch(["AUD","CAD","EUR","JPY"])

        #part 2
        plot_chart_batch(["NZD","SEK","CHF","GBP"])

        #part 3
        plot_chart_batch(["USD","CNY","HKD","INR"])

        #part 4
        plot_chart_batch(["IDR","MYR","PHP","SGD"])

        #part 5
        plot_chart_batch(["KRW","TWD","THB"])

        logging.info("===> end to plot_ESI_line_chart <===")


    def plot_Global_ESI_line_chart(cls, df, output_file_name = None, width=12, height=6, output_dir=None, dpi = 100 ):

        logging.info("===> start to plot_Global_ESI_line_chart <===")

        def plot_chart_batch():
            plt.close()
            fig1 = plt.figure(figsize=(width,height))
            gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])

            axes = plt.subplot(gs[0])

            cls.plot_Global_esi_chart_ax(df, axes, output_file_name)

            if output_dir is not None:
                f = os.path.join(output_dir, output_file_name + ".png")
                logging.info("Output chart as " + f)
                fig1.tight_layout()
                #plt.tight_layout()
                plt.savefig(f, dpi = dpi, bbox_inches='tight' )
                plt.close()

        #part 1
        plot_chart_batch()

        logging.info("===> end to plot_Global_ESI_line_chart <===")


    def plot_epi_snail_trail_chart(cls, dfs, output_file_name = None, width=12, height=8, output_dir=None, dpi = 240 ):

        """
        """

        logging.info("===> start to plot_epi_snail_trail_chart <===")

        def process_df(dfs, ax,output_file_name, df_name):
            sub_df = dfs[df_name][[output_file_name, output_file_name + 'M']]
            sub_df.columns = ['x', 'y']
            cls.plot_epi_snail_trail_ax(sub_df, ax, df_name)

        plt.close()
        fig1 = plt.figure(figsize=(width,height))
        gs = gridspec.GridSpec(2, 3, height_ratios=[1,1], width_ratios=[1,1,1])

        ax0 = plt.subplot(gs[0])
        process_df(dfs, ax0, output_file_name, "Headline")
        ax1 = plt.subplot(gs[1])
        process_df(dfs, ax1, output_file_name, "Inflation")
        ax2 = plt.subplot(gs[2])
        process_df(dfs, ax2, output_file_name, "Business")
        ax3 = plt.subplot(gs[3])
        process_df(dfs, ax3, output_file_name, "Consumer")
        ax4 = plt.subplot(gs[4])
        process_df(dfs, ax4, output_file_name, "Growth")
        ax5 = plt.subplot(gs[5])
        process_df(dfs, ax5, output_file_name, "Employment")




        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, "EPI_Snail_" + output_file_name + ".png")
            logging.info("Output chart as " + f)
            fig1.tight_layout()
            #plt.tight_layout()
            plt.savefig(f, dpi = dpi, bbox_inches='tight' )
            plt.close()



        logging.info("===> end plot_epi_snail_trail_chart <===")


    def plot_fci_pci_timeseries_line_chart(cls, dfs, country_name = None, width=12, height=6, output_dir=None, dpi = 100):

        """
        """

        logging.info("===> start to plot_fci_pci_timeseries_line_chart <===")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))

        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])

        ax0 = plt.subplot(gs[0])

        #plot the fci part
        cls.plot_fci_pci_chart_ax(dfs, ax0, country_name)


        ax0.text(dfs["PCI"].index[450], 0.8, 'Condition Loosening', fontdict= {'color':  'black', 'weight': 'normal', 'size': 10})
        ax0.text(dfs["PCI"].index[450], 0.2, 'Condition Tightening', fontdict= {'color':  'black', 'weight': 'normal', 'size': 10})


        if country_name is not None and output_dir is not None:
            f = os.path.join(output_dir, "PCI_FCI_timeseries_" + translate_country_name(country_name) + ".png")
            logging.info("Output chart as " + f)
            #fig1.tight_layout()
            #plt.tight_layout()
            plt.savefig(f, dpi = dpi, bbox_inches='tight')
            plt.close()

        logging.info("===> end plot_fci_pci_timeseries_line_chart <===")


    def plot_epi_timeseries_line_chart(cls, df, country_name = None, width=12, height=6, output_dir=None, dpi = 100):

        """
        """

        logging.info("===> start to plot_epi_timeseries_line_chart <===")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))

        gs = gridspec.GridSpec(1, 1, height_ratios=[1])

        #gs = gridspec.GridSpec(1, 2, height_ratios=[1], width_ratios=[1,1])

        ax1 = plt.subplot(gs[0])
        #ax2 = plt.subplot(gs[1])

        cls.plot_epi_component_ax(df, ax1, translate_country_name(country_name) + " EPI")
        #plot_epi_component_ax(df.tail(300), ax2, output_file_name)


        if country_name is not None and output_dir is not None:
            f = os.path.join(output_dir, "EPI_timeseries_" + translate_country_name(country_name) + ".png")
            logging.info("Output chart as " + f)
            #fig1.tight_layout()
            #plt.tight_layout()
            plt.savefig(f, dpi = dpi, bbox_inches='tight')
            plt.close()



        logging.info("===> finish plot_epi_timeseries_line_chart <===")


    def plot_epi_by_region_timeseries_line_chart(cls, df, epi_type = None, width=12, height=6, output_dir=None, dpi = 100):

        logging.info("===> start to plot_epi_by_region_timeseries_line_chart <===")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))

        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])

        ax0 = plt.subplot(gs[0])

        cls.plot_epi_by_region_ax(df, ax0, epi_type + " EPI by region")

        if epi_type is not None and output_dir is not None:
            f = os.path.join(output_dir, epi_type + " EPI by region.png")
            logging.info("Output chart as " + f)
            plt.savefig(f, dpi = dpi, bbox_inches='tight')
            plt.close()


        logging.info("===> finish plot_epi_by_region_timeseries_line_chart <===")

    def plot_epi_by_G10_countries_timeseries_line_chart(cls, df, epi_type = None, width=12, height=6, output_dir=None, dpi = 100):

        logging.info("===> start to plot_epi_by_G10_countries_timeseries_line_chart <===")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))

        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])

        ax0 = plt.subplot(gs[0])

        cls.plot_epi_by_g10_countries_ax(df, ax0, epi_type + " EPI by G10 countries")

        if epi_type is not None and output_dir is not None:
            f = os.path.join(output_dir, epi_type + " EPI by G10 countries.png")
            logging.info("Output chart as " + f)
            plt.savefig(f, dpi = dpi, bbox_inches='tight')
            plt.close()


        logging.info("===> finish plot_epi_by_G10_countries_timeseries_line_chart <===")



    def plot_fundamental_area_chart(cls, df, country_name = None, width=12, height=6, output_dir=None, dpi = 100):

        logging.info("===> start to plot_fundamental_area_chart <===")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))
        gs = gridspec.GridSpec(1, 1, height_ratios=[1])

        ax1 = plt.subplot(gs[0])

        idx = df.index

        cls.plot_stacked_area_fundamental_ax(df, ax1, translate_country_name(country_name) + " Fundamental")


        h = ax1.get_legend()
        h.get_frame().set_linewidth(0.0)


        if country_name is not None and output_dir is not None:
            f = os.path.join(output_dir, "Fundamental_stackedarea_" + translate_country_name(country_name) + ".png")
            logging.info("Output chart as " + f)
            #fig1.tight_layout()
            #plt.tight_layout()
            plt.savefig(f, dpi = dpi, bbox_inches='tight')
            plt.close()


        logging.info("===> end plot_fundamental_area_chart <===")


    
    #------------------------------------------------------------------------------------------------
    def plot_epi_heatmap(cls, df, sampling_func, output_file_name = None, width = 12, height = 6, output_dir=None, dpi = 100 ):
    #------------------------------------------------------------------------------------------------

        logging.info("===> start to plot_epi_heatmap <===")
        import seaborn as sns

        sns.set()
        #sns.set_style("white")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))
        gs = gridspec.GridSpec(1, 2, height_ratios=[1], width_ratios =[100,1] , wspace= 0.01)
        ax1 = plt.subplot(gs[0])
        ax2 = plt.subplot(gs[1])

        #need this
        df = df.fillna(0)
        data = sampling_func(df, 12)

        ax_out = sns.heatmap(data, annot=True, linewidths=.5, vmin =-2, vmax =2, center =0, cmap ="RdBu", ax=ax1, cbar_ax = ax2,  annot_kws={"size": 8})

        ax2.tick_params(labelsize=8)

        ax1.set_title("12 months " +output_file_name,  fontsize = 14, y = 1.02, fontweight='bold')

        for label in ax1.get_xticklabels(minor =False):
            label.set_fontsize(8)
            label.set_fontweight('bold')

        for label in ax1.get_yticklabels(minor =False):
            label.set_fontsize(8)
            label.set_fontweight('bold')
            label.set_rotation('0')


        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, output_file_name + "_Heatmap.png")
            logging.info("Output chart as " + f)
            #fig1.tight_layout()
            #plt.tight_layout()
            plt.savefig(f, dpi = dpi, bbox_inches='tight')
            plt.close()


        logging.info("===> end plot_epi_heatmap <===")



    def plot_epi_quardrant_chart(cls, df, output_file_name = None, width=12, height=8, output_dir=None, dpi = 100 ):

        """

        """

        logging.info("===> end plot_epi_quardrant_chart <===")


        def process_df(df, ax, output_file_name, highlight_item = None, color_override = None, alpha_override = 1):

            x_arr = [[df.iloc[-1][col]] for col in df.columns if not col.endswith('M')]
            y_arr = [[df.iloc[-1][col]] for col in df.columns if col.endswith('M')]
            name_arr  = [col for col in df.columns if not col.endswith('M')]
            size_arr  = [70 for col in df.columns if not col.endswith('M')]
            #color_arr = ["blue" for col in df.columns if not col.endswith('M')]

            alpha_arr  = [alpha_override for col in df.columns if not col.endswith('M')] if alpha_override is not None \
            else [1 for col in df.columns if not col.endswith('M')]

            color_arr = [color_override for col in df.columns if not col.endswith('M') ] if color_override is not None \
            else ["#ee4035","#f37736","#fdf498","#7bc043","#0392cf","#d11141","#00b159","#00aedb",
                  "#011f4b","#03396c","#005b96","#6497b1","#b3cde0"] * 10

            if highlight_item is not None:
                highlight_item_idx = [col for col in df.columns if not col.endswith('M') ].index(highlight_item)
                if highlight_item_idx >= 0:
                    color_arr[highlight_item_idx] = "red"
                    alpha_arr[highlight_item_idx] = 1

            max_a = df.iloc[-1].abs().values.max() if df.iloc[-1].abs().values.max() != 0 else 2

            ax.set_ylim(-1.0 * math.ceil(max_a), math.ceil(max_a))
            ax.set_xlim(-1.0 * math.ceil(max_a), math.ceil(max_a))

            cls.plot_epi_quardrant_ax(x_arr, y_arr, size_arr, name_arr, alpha_arr, color_arr, ax, chart_title = output_file_name)


        plt.close()

        fig1 = plt.figure(figsize=(width,height))
        #gs = gridspec.GridSpec(2, 3, height_ratios=[1,1], width_ratios=[1,1,1])
        gs = gridspec.GridSpec(1, 2, height_ratios=[1], width_ratios=[1,1])

        ax0 = plt.subplot(gs[0])
        ax1 = plt.subplot(gs[1])


        process_df(df, ax0, output_file_name, color_override = "grey", alpha_override=0.2, highlight_item ="AU")

        process_df(df, ax1, output_file_name, color_override = "Blue")




        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, translate_country_name(output_file_name) + "_EPI_quardrant.png")
            logging.info("Output chart as " + f)
            fig1.tight_layout()
            #plt.tight_layout()
            plt.savefig(f, dpi = dpi)
            plt.close()


        logging.info("===> end plot_epi_quardrant_chart <===")



    def plot_epi_quardrant_snail_chart(cls, df, country_list, output_file_name = None, width=12, height=6, output_dir=None, dpi = 100 ):

        logging.info("===> end plot_epi_quardrant_chart <===")

        #country_list = ["AU","CA","CH", "EU", "ID", "IN", "JN", "MA", "NZ", "PH", "SI", "SK", "TA", "TH", "UK", "US","GLOBAL"]

        def process_quardrant_part(df, ax, output_file_name, highlight_item = None, color_override = None, alpha_override = 1):

            df = df.fillna(0).round(2)

            x_arr = [[df.iloc[-1][col]] for col in df.columns if not col.endswith('M')]
            y_arr = [[df.iloc[-1][col]] for col in df.columns if col.endswith('M')]
            name_arr  = [col for col in df.columns if not col.endswith('M')]
            size_arr  = [90 for col in df.columns if not col.endswith('M')]
            #color_arr = ["blue" for col in df.columns if not col.endswith('M')]

            alpha_arr  = [alpha_override for col in df.columns if not col.endswith('M')] if alpha_override is not None \
            else [1 for col in df.columns if not col.endswith('M')]

            color_arr = [color_override for col in df.columns if not col.endswith('M') ] if color_override is not None \
            else ["#ee4035","#f37736","#fdf498","#7bc043","#0392cf","#d11141","#00b159","#00aedb",
                  "#011f4b","#03396c","#005b96","#6497b1","#b3cde0"] * 10

            if highlight_item is not None:
                highlight_item_idx = [col for col in df.columns if not col.endswith('M') ].index(highlight_item)
                if highlight_item_idx >= 0:
                    color_arr[highlight_item_idx] = "red"
                    alpha_arr[highlight_item_idx] = 1


            max_a = df.iloc[-1].abs().values.max() if df.iloc[-1].abs().values.max() != 0 else 2

            ax.set_ylim(-1.0 * math.ceil(max_a), math.ceil(max_a))
            ax.set_xlim(-1.0 * math.ceil(max_a), math.ceil(max_a))

            cls.plot_epi_quardrant_ax(x_arr, y_arr, size_arr, name_arr, alpha_arr, color_arr, ax, chart_title = translate_country_name(highlight_item) + " " +  output_file_name)


        def process_snail_part(df,  ax, country_name,  chart_title):
            sub_df = df[[country_name, country_name + 'M']].fillna(0)
            sub_df.columns = ['x', 'y']

            max_a = sub_df.abs().values.max() if sub_df.abs().values.max() != 0 else 2

            ax.set_ylim(-1.0 * math.ceil(max_a), math.ceil(max_a))
            ax.set_xlim(-1.0 * math.ceil(max_a), math.ceil(max_a))
            colors =  mpl.dates.date2num(df.index.to_pydatetime())

            mapable = cls.plot_epi_snail_trail_ax(sub_df, ax, translate_country_name(country_name) + " " + chart_title,  dot_colors = colors)

            return mapable

        for country in country_list:

            logging.info("Process quardrand and snail for " + country)

            plt.close()

            fig1 = plt.figure(figsize=(width,height))

            #gs = gridspec.GridSpec(1, 2, height_ratios=[1], width_ratios=[1,1])
            gs = gridspec.GridSpec(1, 2, height_ratios=[1], width_ratios=[1,1])

            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])


            process_quardrant_part(df, ax0, output_file_name, color_override = "grey", alpha_override=0.2, highlight_item =country)

            logging.info("finish the quardrand quardrant half for " + country)

            mapable = process_snail_part(df,  ax1,  country,  output_file_name + " history")

            fig1.subplots_adjust(left = 0.03, right=0.93, top =0.90, bottom = 0.1)
            cbar_ax = fig1.add_axes([0.94, 0.16, 0.01, 0.7]) # left, bottom, width, height
            cbar = fig1.colorbar(mapable,
                                 ticks=mpl.dates.DayLocator(interval=200),
                                 format=mpl.dates.DateFormatter('%b\n%Y'), cax=cbar_ax)
            cbar.ax.tick_params(labelsize=8)


            logging.info("finish the snail half for " + country)

            if output_file_name is not None and output_dir is not None:
                f = os.path.join(output_dir, "EPI_quardrant_snail_" + translate_country_name(country) + "_" + output_file_name + ".png")
                logging.info("Output chart as " + f)
                #fig1.tight_layout()
                #plt.tight_layout()
                plt.savefig(f, dpi = dpi, bbox_inches='tight')
                plt.close()

        logging.info("===> end plot_epi_quardrant_chart <===")


    def plot_epi_component_cross_countries_chart(cls, dfs, output_file_name = None, width=12, height=6, output_dir=None, dpi = 100 ):

        logging.info("===> start plot_epi_component_cross_countries_chart <===")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))
        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])

        cls.plot_epi_component_cross_countries_chart_ax(dfs, ax0, output_file_name, legend_font_size = 10)


        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, output_file_name + "_EPI_Component_cross_countries.png")
            logging.info("Output chart as " + f)
            fig1.tight_layout()

            plt.savefig(f, dpi = dpi)
            plt.close()

        logging.info("===> end plot_epi_component_cross_countries_chart <===")


    def plot_pci_component_cross_countries_chart(cls, dfs, output_file_name = None, width=12, height=6, output_dir=None, dpi = 100 ):

            logging.info("===> start plot_pci_component_cross_countries_chart <===")

            plt.close()

            fig1 = plt.figure(figsize=(width,height))
            gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
            ax0 = plt.subplot(gs[0])

            cls.plot_pci_component_cross_countries_chart_ax(dfs, ax0, output_file_name, legend_font_size = 10)


            if output_file_name is not None and output_dir is not None:
                f = os.path.join(output_dir, output_file_name + ".png")
                logging.info("Output chart as " + f)
                fig1.tight_layout()

                plt.savefig(f, dpi = dpi)
                plt.close()

            logging.info("===> end plot_pci_component_cross_countries_chart <===")




    def plot_fundamental_movement_chart(cls, df, output_file_name = None, width=12, height=6, output_dir=None, dpi = 100 ):

        logging.info("===> start plot_fundamental_movement_chart <===")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))
        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])


        cls.plot_fundamental_diff_chart_ax(df, ax0, output_file_name, legend_font_size = 10)

        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, output_file_name + ".png")
            logging.info("Output chart as " + f)
            fig1.tight_layout()

            plt.savefig(f, dpi = dpi)
            plt.close()

        logging.info("===> end plot_fundamental_movement_chart <===")



    def plot_EPI_region_and_countries_history_column_chart(cls, df, output_file_name = None, width=12, height=6, output_dir=None, dpi = 100 ):

        logging.info("===> start plot_EPI_region_and_countries_history_column_chart <===")

        plt.close()

        fig1 = plt.figure(figsize=(width,height))
        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])


        cls.plot_EPI_region_and_countries_history_column_chart_ax(df, ax0, output_file_name, legend_font_size =10)

        if output_file_name is not None and output_dir is not None:
            f = os.path.join(output_dir, output_file_name + ".png")
            logging.info("Output chart as " + f)
            fig1.tight_layout()

            plt.savefig(f, dpi = dpi)
            plt.close()

        logging.info("===> end plot_EPI_region_and_countries_history_column_chart <===")




