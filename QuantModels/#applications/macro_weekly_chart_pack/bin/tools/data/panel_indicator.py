from __future__ import division
import pandas as pd
import numpy as np
import datetime as dt
from dateutil.relativedelta import relativedelta
from data import datatools as tools
from common import dirs as dirs
import logging

class panel_indicator(object):
    """RiskAersion data process contains a list of functions that used by producing the RiskAversion indicators
    Args: debug_folder (string): A string var holding the folder to dump the temp file for debuggging purpose
    """
    def __init__(self,debug_folder, debug=False):
        self._debug_folder = debug_folder
        self._debug = debug
        logging.debug("Done creating panel_indicator object")

    def validate_composite_indicator_raw_data(self,
                                              dictofdfs,
                                              inner_column_list,
                                              outer_dictkey_list,
                                              debug_file_name_token =r"VALIDATED",
                                              debug_override = None):
        """This function validates the raw_data input:

        raw_data is expected to be a dictionary object.
        The keys are the name of the security.
        The columns of each dataframe are expected to be the name of the countries.
        Each dataframe is expected to have the same number of columns

        Args:
            dictofdfs (dictionary of dataframe): The raw market data

        Returns:
            a tuple of a list of securities included in the raw data and a list of countries for each of security.

        Raises:
            ValueError: If raw_data is not in the
        """

        _debug = self._debug if debug_override is None else debug_override

        if isinstance(dictofdfs, dict):
            logging.info("raw_data is a dict obj")
        else:
            logging.error("raw_data is expected to be a dict obj")
            raise TypeError("raw_data is expected to be a dict obj")

        if len(dictofdfs) > 0:
            logging.info("raw_data is NOT empty")
        else:
            logging.error("raw_data is expected to have at least 1 df")
            raise TypeError("raw_data is expected to have at least 1 df")

        # pick the first df out of the dict to be used as the template for the empty df
        dummy_df = tools.create_empty_numeric_df(dictofdfs.values()[0].index, inner_column_list)
        dict_out = {sec: tools.recolumn_df_data(df, inner_column_list) for sec, df in dictofdfs.iteritems()}

        for dictkey in outer_dictkey_list:
            if dictkey not in dict_out.keys():
                dict_out[dictkey] = dummy_df.copy()
            else:
                dict_out[dictkey] = dict_out[dictkey]
            if _debug:
                f = dirs.get_file_path(self._debug_folder, dictkey + "_" + debug_file_name_token + ".CSV")
                dict_out[dictkey].to_csv(f)

        return dict_out


    def rolling_standardise_data(self,
                                 raw_data,
                                 window,
                                 min_window = None,
                                 fill_missing_method=r"ffill",
                                 debug_file_name_token =r"RAW_DATA_ZSC_D",
                                 debug_override = None):
        """ Standardise each of the dataframe from the input dict
        Args:
            raw_data: a dict of dfs. For example a dict of dataframe while the key is the security and df columns are the countries
            window (int): the siz of the rolling window.
            min_window (Optional[int]): The min number of valid data points in a window
            debug_file_name_token(string): a string token to identify the debug file
            debug_override (bool): a local debug flag to override the class level debug settings

        Returns:
            a dict containing standardised dataframe: True if successful, False otherwise.
        """
        _debug = self._debug if debug_override is None else debug_override
        def std(df, w, mw, data_label):
            logging.debug("Standardise [{0}] df".format(data_label))
            rolling_zcs_df = tools.rolling_standardise_table(df, window_size=window, min_window_size=mw, fill_missing_method = fill_missing_method)
            if _debug:
                f = dirs.get_file_path(self._debug_folder, data_label + "_" + debug_file_name_token + ".CSV" )
                rolling_zcs_df.to_csv(f)
            return rolling_zcs_df
        _ouput =  {data_label : std(df, window, min_window, data_label)  for data_label, df in raw_data.iteritems()}
        return _ouput


    def rolling_ecdf_data(self,
                         raw_data,
                         window,
                         min_window = None,
                         fill_missing_method=r"ffill",
                         debug_file_name_token =r"RAW_DATA_ECDF_D",
                         debug_override = None):
        """ ecdf the dataframes from the input dict
        Args:
            raw_data: a dict of dfs. For example a dict of dataframe while the key is the security and df columns are the countries
            window (int): the siz of the rolling window.
            min_window (Optional[int]): The min number of valid data points in a window
            debug_file_name_token(string): a string token to identify the debug file
            debug_override (bool): a local debug flag to override the class level debug settings

        Returns:
            a dict containing standardised dataframe: True if successful, False otherwise.
        """
        _debug = self._debug if debug_override is None else debug_override
        def std(df, w, mw, data_label):
            logging.debug("Standardise [{0}] df".format(data_label))
            rolling_zcs_df = tools.rolling_ECDF_table(df, window_size=window, min_window_size=mw, fill_missing_method = fill_missing_method)
            if _debug:
                f = dirs.get_file_path(self._debug_folder, data_label + "_" + debug_file_name_token + ".CSV" )
                rolling_zcs_df.to_csv(f)
            return rolling_zcs_df
        _ouput =  {data_label : std(df, window, min_window, data_label)  for data_label, df in raw_data.iteritems()}
        return _ouput


    def expanding_standardise_data(self,
                                 raw_data,
                                 min_window = None,
                                 fill_missing_method=r"ffill",
                                 debug_file_name_token =r"RAW_DATA_ZSC_D",
                                 debug_override = None):
        """ Expanding standardise each of the dataframe from the input dict
        Args:
            raw_data: a dict of dfs. For example a dict of dataframe while the key is the security and df columns are the countries
            min_window (Optional[int]): The min number of valid data points in a window
            debug_file_name_token(string): a string token to identify the debug file
            debug_override (bool): a local debug flag to override the class level debug settings

        Returns:
            a dict containing standardised dataframe: True if successful, False otherwise.
        """
        _debug = self._debug if debug_override is None else debug_override

        def std(df, data_label):
            logging.debug("Standardise [{0}] df".format(data_label))
            expanding_zcs_df = tools.expanding_standardise_table(df, fill_missing_method = fill_missing_method, fill_missing_at_the_end=False)
            if _debug:
                f = dirs.get_file_path(self._debug_folder, data_label + "_" + debug_file_name_token + ".CSV" )
                expanding_zcs_df.to_csv(f)
            return expanding_zcs_df
        _ouput =  {data_label : std(df, data_label)  for data_label, df in raw_data.iteritems()}
        return _ouput


    def reshape_data_swap_dictkey_with_dfcols(self,
                                              dictofdfs,
                                              columnlist = None,
                                              debug_file_name_token =r"",
                                              debug_override = None):
        """ This function takes in a dict of dataframes. For example a dicted keyed by security and each underline df is columned by the countries
         This function will swap the dict key with the df columns. So the output data structure will be a dict keyed on country and underline df pointed at by
         the keys are columned by security type
        Args:
            dictofdfs (dictionary(dataframe)): a dict of dfs
            columnlist optional(list(string)): a list of the column names from the dictionary(dataframe)
            debug_file_name_token(string): a string token to identify the debug file
            debug_override (bool): a local debug flag to override the class level debug settings
        Returns:
            a dict of dfs, after switching the dict key with the underline df columns
        """

        _debug = self._debug if debug_override is None else debug_override

        _dictkeylist = [key for key, dict in dictofdfs.iteritems()]


        # if the column list is not given, take the column list from any of the existing dataframe
        _columnlist = dictofdfs.values()[0].columns.tolist() if columnlist is None else columnlist
        tools.validate_list(_columnlist,allow_empty=True)

        def find_col_data_series(df, column, dict_key):
            if column in df.columns.tolist():
                logging.debug(" Col(country) %s has %d dps for %s (security)" % (column, len(df[column].dropna()), dict_key))
                return pd.DataFrame({dict_key : df[column]})
            else:
                logging.debug(" Col %s has NO dps for %s" % (column, dict_key))
                # return an empty df as the place holder
                return pd.DataFrame(index = df.index, columns=[dict_key]).astype("float64")


        def create_per_col_df(dictofdfs, column):
            singlecoldflist = [find_col_data_series(df, column, dict_key) for dict_key, df in dictofdfs.iteritems()]
            #reshapeddf = pd.concat(singlecoldflist, join="outer", axis=1).fillna(method="ffill")[_dictkeylist]
            reshapeddf = pd.concat(singlecoldflist, join="outer", axis=1)[_dictkeylist]

            if _debug:
                f = dirs.get_file_path(self._debug_folder, debug_file_name_token + column  + ".CSV")
                reshapeddf.to_csv(f)
            return reshapeddf

        _output = {column:create_per_col_df(dictofdfs, column) for column in _columnlist}

        return _output


    def weighted_aggregate_cols_within_df(self,
                                          input_data,
                                          columns_weights = None,
                                          use_full_row_only = False,
                                          debug_file_name_token =r"wavg_of_cols",
                                          debug_override = None):
        """ This function weighted aggregates the columns of the dfs. The dfs are stored in a dict
        Args:
            input_data (dict(df)): A dictionary of dataframes. There are two level of indexers:
                                   The dict key level indexer - dict key (like a security).
                                   And the df level indexer - df columns (like a country).
            columns_weights (list(int)): a list containing the weights used to aggregate the df columns
            debug_override (bool): a local debug flag to override the class level debug settings

        Returns:
            a tubple of
                dictionary(dataframe): with its underline dfs containing single column dfs. The single column is the result of aggregating all the the columns
                dataframe: aggregates above dictionary(dataframe) into a single dataframe
        """

        _debug = self._debug if debug_override is None else debug_override

        #only validate the weights when it is supplied
        if columns_weights is not None:
            tools.validate_list(columns_weights,allow_empty=False)

        def func(df, name_of_avged_col, column_weights, use_full_row_only):

            #only validate the weights when it is supplied
            if columns_weights is not None:
                if (len(df.columns) != len(column_weights)): raise ValueError("Different sizes between df [{0}] and df columns weights".format(name_of_avged_col))

            _func_result = tools.weighted_avg_df_columns(df,
                                                         name_of_avged_col,
                                                         weights = columns_weights,
                                                         use_full_row_only = use_full_row_only  # this is pretty bad!
                                                         )
            return _func_result

        _output_part1 = {key : func(df, key, columns_weights, use_full_row_only) for key, df in input_data.iteritems()}
        _output_part2 = {debug_file_name_token:pd.concat(_output_part1.values(), join="outer", axis=1).fillna(method="ffill")[sorted(_output_part1.keys())]}


        if _debug:
            f = dirs.get_file_path(self._debug_folder, debug_file_name_token + ".CSV")
            _output_part2.values()[0].to_csv(f)

        return _output_part2


    def dynamic_weighted_aggregate_cols_within_df(self,
                                                  input_data,
                                                  columns_weights_df = None,
                                                  use_full_row_only = False,
                                                  debug_file_name_token =r"wavg_of_cols",
                                                  debug_override = None):
        """ This function weighted aggregates the columns of the dfs. The dfs are stored in a dict
        Args:
            input_data (dict(df)): A dictionary of dataframes. There are two level of indexers:
                                   The dict key level indexer - dict key (like a security).
                                   And the df level indexer - df columns (like a country).
            columns_weights_df (dataframe(float)): a dataframe whose columns are the timeseries of weights, hence weights are dynamic as time passes
            debug_override (bool): a local debug flag to override the class level debug settings

        Returns:
            a tubple of
                dictionary(dataframe): with its underline dfs containing single column dfs. The single column is the result of aggregating all the the columns
                dataframe: aggregates above dictionary(dataframe) into a single dataframe
        """

        _debug = self._debug if debug_override is None else debug_override

        #only validate the weights when it is supplied
        if columns_weights_df is not None:
            tools.validate_timeseries_data(columns_weights_df,is_df=True)

        def func(df, name_of_avged_col, column_weights, use_full_row_only):


            #if columns_weights_df is not None:
                #if (len(df.columns) != len(column_weights.columns)): raise ValueError("Different sizes between df [{0}] and df columns weights".format(name_of_avged_col))

            #It is valid to have the weight dataframe columns differ the data dataframe. but it is crucial to have the data dataframe recolumned to
            # be the same as the weights columns. e.g constructing a aggregation for selected column

            _func_result = tools.dynamic_weighted_avg_df_columns(df[columns_weights_df.columns.tolist()],
                                                                 name_of_avged_col,
                                                                 weights_df = columns_weights_df.fillna(0),
                                                                 use_full_row_only = use_full_row_only
                                                                 )
            return _func_result

        _output_part1 = {key : func(df, key, columns_weights_df, use_full_row_only) for key, df in input_data.iteritems()}
        _output_part2 = {debug_file_name_token:pd.concat(_output_part1.values(), join="outer", axis=1).fillna(method="ffill")[sorted(_output_part1.keys())]}

        if _debug:
            f = dirs.get_file_path(self._debug_folder, debug_file_name_token + ".CSV")
            _output_part2.values()[0].to_csv(f)

        return _output_part2


    def weighted_aggregate_dfs_within_dict(self,
                                           input_data,
                                           columns_weights,
                                           dict_item_weights,
                                           debug_file_name_token =r"wavg_of_dfs",
                                           ignore_raw_wtih_missing_data=False,
                                           debug_override = None):

        """ This function first weighted-aggregates the columns of the dfs.
        Then it weighted aggregates all the single column dfs
        This function is a step further of the weighted_aggregate_cols_within_df

        Args:
            input_data (dict(df)): A dictionary of dataframes. There are two level of indexers:
                                   The dict key level indexer - dict key (like a security).
                                   And the df level indexer - df columns (like a country).
            columns_weights (list(int)): a list containing the weights used to aggregate the df columns
            dict_item_weights (list(int)): a list containing the weights used to aggregate all the items (dfs) in the dict
            debug_override (bool): a local debug flag to override the class level debug settings

        Returns:
            a dict with a containing a single column df. The single column is the result of aggregating all the dfs (which are the aggregation of all the columns in
            each df)
        """

        _debug = self._debug if debug_override is None else debug_override

        tools.validate_list(dict_item_weights,allow_empty=False)

        if (len(input_data) != len(dict_item_weights)):
            raise ValueError("Different sizes between dict and dict item weights")

        _dfwavg = self.weighted_aggregate_cols_within_df(input_data,
                                                         columns_weights,
                                                         ignore_raw_wtih_missing_data,
                                                         debug_override= _debug)
        # _dfwavg - a dict of single column dfs
        _df_comb_single_cols =  pd.concat(_dfwavg.values(), join='outer', axis=1).fillna(method="ffill")
        _df_comb_single_cols.index.name = r"DATE"

        if _debug:
            f = dirs.get_file_path(self._debug_folder, debug_file_name_token + ".CSV" )
            _df_comb_single_cols.to_csv(f)

        # call weighted_aggregate_cols_within_df a second time with a dummy dict that contains only 1 df
        # which is the combined single columns from previous weighted_aggregate_cols_within_df call
        # the weights being used this time is dict_item_weights

        _output = self.weighted_aggregate_cols_within_df({ debug_file_name_token : _df_comb_single_cols},
                                                         dict_item_weights,
                                                         ignore_raw_wtih_missing_data,
                                                         debug_override= _debug)
        return _output


    def unpivot_data(self,
                     data_frame_dict,
                     id_column_name,
                     unpvted_column_names,
                     particiating_column_names = None,
                     derive_id_column_from_row_idx = True,
                     subset = None,
                     drop_blanks=False,
                     debug_file_name_token =r"UNPIVOT",
                     debug_override = None):
        """ Unpivot each of the dataframe from the input dict
        Args:
            data_frame_dict (a dict of dataframe): a dict of dfs which will be unpvioted
            id_column_name (string): the column name from the origianl dataframe that will be used unpivot each row. It will be part of the unpivoted join key
            unpvted_column_names (a list of string): the column names will be used to name the unpvted df. they need to be database friendly
            particiating_column_names (Optional[a list of string]): optional parameter to select participating columns from the original df into the unpvt process.
                                                          if it is ignored all columns will be used.
            derive_id_column_from_row_idx(Optional[bool]): optional parameter to copy the index column back into the df as a normal column
            debug_file_name_token(string): a string token to identify the debug file
            subset (int): represent the number of records from top and bottom will be unpvited
            drop_blanks (False): flag to indicator whether to trim off the leading blank of the df
            debug_override (bool): a local debug flag to override the class level debug settings

        Returns:
            a dict containing unpivoted dataframe.
        """

        _debug = self._debug if debug_override is None else debug_override

        def unpivot(df, data_label):
            logging.debug("Unpivot [{0}] df".format(data_label))


            if _debug:
                f = dirs.get_file_path(self._debug_folder, data_label + "_" + debug_file_name_token + "_PRE_UNPIVOTED" + ".CSV" )
                df.to_csv(f)

            unpivoted_df = tools.unpivot_df(df,
                                            id_column_name,
                                            unpvted_column_names,
                                            particiating_column_names=particiating_column_names,
                                            derive_id_column_from_row_idx=derive_id_column_from_row_idx)

            if _debug:
                f = dirs.get_file_path(self._debug_folder, data_label + "_" + debug_file_name_token  + "_POST_UNPIVOTED" + ".CSV" )
                unpivoted_df.to_csv(f)

            if drop_blanks:
                unpivoted_df = unpivoted_df.dropna(subset=["VALUE"])

            return unpivoted_df

        _output =  {data_label : unpivot(df if subset is None else df.tail(subset) , data_label) for data_label, df in data_frame_dict.iteritems()}

        return _output




