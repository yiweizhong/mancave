from __future__ import division
import pandas as pd
import numpy as np
import logging
import datetime as dt
import collections
from func import each as f_each
from pandas.tseries.offsets import *
from statsmodels.distributions.empirical_distribution import ECDF


#------------------------------------------------------------------------------------------------
def validate_list(input, allow_empty=True):
#------------------------------------------------------------------------------------------------
    if not (isinstance(input, list)):
        raise TypeError("The input is expected to be a list object")
    if (not(allow_empty)) and len(input) <= 0:
        raise TypeError("The input is expected to be a non empty list object")


#------------------------------------------------------------------------------------------------
def validate_timeseries_data(ts, is_df=True):
#------------------------------------------------------------------------------------------------
    """
    This function is used to validate the dataframe object used to hold time series. In the Macro application context, a dataframe holds timeserieses as its column.
    :param timeseries: the dataframe to be validated
    :param is_df: the input is a series if this flag is False
    :return:
    """

    if is_df:
        data_type = "dataframe"
         # check 1
        if isinstance(ts, pd.DataFrame):
            logging.debug("Check1: The input timeseries obj is a valid %s" % (data_type))
        else:
            raise TypeError("The input timeseries obj is NOT a valid %s" % (data_type))
    else:
        data_type = "series"
         # check 1
        if isinstance(ts, pd.Series):
            logging.debug("Check1: The input timeseries obj is a valid %s" % (data_type))
        else:
            raise TypeError("The input timeseries obj is NOT a valid %s" % (data_type))

    #check 2
    if not ts.empty:
        logging.debug("Check2: The input timeseries obj is not empty")
    else:
        logging.warn("Check2: The input timeseries obj is empty!")

    #check 3
    if ts.index.name == "DATE":  #breaking change
        logging.debug("Check3: The input timeseries obj index column name is DATE")
    else:
        logging.error("The input timeseries obj index column name is expected to be upper case %s but received %s" % ("DATE",ts.index.name))
        raise ValueError("The input timeseries obj index column name is expected to be  upper case %s but received %s" % ("DATE",ts.index.name))

    #check 4
    if ts.index.dtype == np.dtype(r"datetime64[ns]"): #breaking change
        logging.debug("Check4: The input timeseries obj index column datatype is %s" % r"datetime64[ns]")
    else:
        logging.error("The input timeseries obj index column datatype is expected to be %s but received %s" % (r"datetime64[ns]",ts.index.dtype))
        raise TypeError("The input timeseries obj index column datatype is expected to be %s but received %s" % (r"datetime64[ns]",ts.index.dtype))

    return True

#------------------------------------------------------------------------------------------------
def weight_df_by_selected_columns(df, selected_cols = None):
#------------------------------------------------------------------------------------------------
    if selected_cols is None:
        return df.apply(lambda (r) : r * 1.0/np.sum(r), axis = 1)
    else:
        df = recolumn_df_data(df.copy(), cols = selected_cols)
        return df.apply(lambda (r) : r * 1.0/np.sum(r), axis = 1)


#------------------------------------------------------------------------------------------------
def weight_df_by_selected_columns_as_ratio_to_a_col(df, based_col, selected_cols = None):
#------------------------------------------------------------------------------------------------
    if selected_cols is None:
        return df.apply(lambda (r) : r * 1.0/r[based_col], axis = 1)
    else:
        cols = selected_cols[:]
        if based_col not in selected_cols: selected_cols.append(based_col)
        df = recolumn_df_data(df.copy(), cols = cols)
        return df.apply(lambda (r) : r * 1.0/r[based_col], axis = 1)


#------------------------------------------------------------------------------------------------
def recolumn_df_data(df, cols=None):
#------------------------------------------------------------------------------------------------
    #validate_timeseries_data(df)
    if cols is None:
        return df
    else:
        df_new = df.copy()
        for c in cols:
            if c in df_new.columns.tolist():
                pass
            else:
                df_new[c] =np.nan
        return df_new[list(cols)]


# #------------------------------------------------------------------------------------------------
# def rolling_standardise_table(df, window_size=1, min_window_size=1, fill_missing_method=r"ffill", start=None, end=None, bday_only=True):
# #------------------------------------------------------------------------------------------------
#     """
#     This function will rolling standardise a table of value according to the window size.
#
#     :param df: input dataframe/table
#     :param window_size: how many data points make up a rolling window
#     :param min_window_size: how many data points are the minimal requirements before the window stats is calcualted
#     :param fill_missing_method: ffill or bfill with last available values
#     :param start: row key for the start if it is provided
#     :param end: row key for the end if it is provided
#     :param bday_only: if the table rows are keyed on dates this option allows override to include none business day
#     :return: a dataframe that containg the rolling zscores.
#     """
#
#     validate_timeseries_data(df)
#     input_df_index_name = df.index.name
#
#     start_date = df.sort_index().index.min() if start is None else start
#     end_date = df.sort_index().index.max() if end is None else end
#     keys = pd.bdate_range(start=start_date,end=end_date) if bday_only else pd.date_range(start=start_date,end=end_date)
#     cols = df.columns.tolist()
#     raw_df = df.reindex(keys).fillna(method=fill_missing_method)
#     rolling_zscore_list = []
#     for col in df.columns.tolist():
#         raw_data = raw_df[col].sort_index().dropna()
#         rolling_avg = pd.rolling_mean(raw_data, window=window_size, min_periods=min_window_size)
#         rolling_stdev = pd.rolling_std(raw_data, window=window_size, min_periods=min_window_size)
#         rolling_zscore_series = ((raw_data - rolling_avg)/rolling_stdev) \
#                                 .replace([np.inf, -np.inf], np.nan) \
#                                 .reindex(keys) \
#                                 .fillna(method=fill_missing_method)
#         rolling_zscore_list.append(pd.DataFrame(rolling_zscore_series,columns=[col]))
#     rolling_zscore_df = pd.concat(rolling_zscore_list, join='outer', axis=1).fillna(method=fill_missing_method)[cols]
#
#     #reapply the index name in the output df
#     rolling_zscore_df.index.name = input_df_index_name
#
#     return rolling_zscore_df


#------------------------------------------------------------------------------------------------
def rolling_standardise_table(df,
                              window_size=1,
                              min_window_size=1,
                              fill_missing_method=r"ffill",
                              start=None,
                              end=None,
                              bday_only=True,
                              fill_missing_at_the_end=False):
#------------------------------------------------------------------------------------------------
    """
    Rolling_standardise a dataframe.

    This function makes sure if a time series ends earlier than other series in the dataframe it will not forward fill the last available values to the end
    of dataframe index. This is important as there are times we lost a a data series due to various reasons. if the last available value is forward filled.
    it will create a zscore decay and dilute the combined data.

    :param df: input dataframe/table
    :param window_size: how many data points make up a rolling window
    :param min_window_size: how many data points are the minimal requirements before the window stats is calcualted
    :param fill_missing_method: ffill or bfill with last available values
    :param start: row key for the start if it is provided
    :param end: row key for the end if it is provided
    :param bday_only: if the table rows are keyed on dates this option allows override to include none business day
    :param fill_missing_at_the_end: a flag decides whether to fill the missing value at the end AFTER the calculation
    :return: a dataframe that containg the rolling zscores.
    """

    validate_timeseries_data(df)
    input_df_index_name = df.index.name

    start_date = df.sort_index().index.min() if start is None else start
    end_date = df.sort_index().index.max() if end is None else end
    keys = pd.bdate_range(start=start_date,end=end_date) if bday_only else pd.date_range(start=start_date,end=end_date)
    cols = df.columns.tolist()
    raw_df = df.reindex(keys)
    rolling_zscore_list = []
    for col in df.columns.tolist():
        raw_data = trim_series (raw_df[col].sort_index()).fillna(method=fill_missing_method).dropna()
        rolling_avg = pd.rolling_mean(raw_data, window=window_size, min_periods=min_window_size)
        rolling_stdev = pd.rolling_std(raw_data, window=window_size, min_periods=min_window_size)
        rolling_zscore_series = ((raw_data - rolling_avg)/rolling_stdev) \
                                .replace([np.inf, -np.inf], np.nan) \
                                .reindex(keys)
        rolling_zscore_list.append(pd.DataFrame(rolling_zscore_series,columns=[col]))

    rolling_zscore_df = pd.concat(rolling_zscore_list, join='outer', axis=1)[cols]

    if fill_missing_at_the_end: rolling_zscore_df = rolling_zscore_df.fillna(method=fill_missing_method)[cols]

    #reapply the index name in the output df
    rolling_zscore_df.index.name = input_df_index_name

    return rolling_zscore_df



#------------------------------------------------------------------------------------------------
def rolling_ECDF_table(df,
                      window_size=1,
                      min_window_size=1,
                      fill_missing_method=r"ffill",
                      start=None,
                      end=None,
                      bday_only=True,
                      fill_missing_at_the_end=False):
#------------------------------------------------------------------------------------------------
    """
    Locate a data point's position on the rolling ECDF of each column of the dataframe.

    This function makes sure if a time series ends earlier than other series in the dataframe it will not forward fill the last available values to the end
    of dataframe index. This is important as there are times we lost a a data series due to various reasons.

    :param df: input dataframe/table
    :param window_size: how many data points make up a rolling window
    :param min_window_size: how many data points are the minimal requirements before the window stats is calcualted
    :param fill_missing_method: ffill or bfill with last available values
    :param start: row key for the start if it is provided
    :param end: row key for the end if it is provided
    :param bday_only: if the table rows are keyed on dates this option allows override to include none business day
    :param fill_missing_at_the_end: a flag decides whether to fill the missing value at the end AFTER the calculation
    :return: a dataframe that containg the rolling zscores.
    """

    validate_timeseries_data(df)
    input_df_index_name = df.index.name

    start_date = df.sort_index().index.min() if start is None else start
    end_date = df.sort_index().index.max() if end is None else end
    keys = pd.bdate_range(start=start_date,end=end_date) if bday_only else pd.date_range(start=start_date,end=end_date)
    cols = df.columns.tolist()
    raw_df = df.reindex(keys)
    rolling_result_list = []
    for col in df.columns.tolist():
        raw_data = trim_series (raw_df[col].sort_index())\
                   .fillna(method=fill_missing_method)\
                   .dropna()
        #trim the blank from the head and tail of the series
        #fill the potholes in the middle

        col_rolling_result = pd.rolling_apply(raw_data,window_size, lambda w: ECDF(w)(w[-1]), min_periods=min_window_size)\
                               .reindex(keys)

        rolling_result_list.append(pd.DataFrame(col_rolling_result,columns=[col]))

    rolling_result_df = pd.concat(rolling_result_list, join='outer', axis=1)[cols]

    if fill_missing_at_the_end: rolling_result_df = rolling_result_df.fillna(method=fill_missing_method)[cols]

    #reapply the index name in the output df
    rolling_result_df.index.name = input_df_index_name

    return rolling_result_df



#------------------------------------------------------------------------------------------------
def expanding_standardise_table(df,
                                fill_missing_method=r"ffill",
                                start=None,
                                end=None,
                                bday_only=True,
                                fill_missing_at_the_end =False
                                ):
#------------------------------------------------------------------------------------------------
    """
    :param df:
    :param fill_missing_method:
    :param start:
    :param end:
    :param bday_only:
    :param fill_missing_at_the_end: a flag decides whether to fill the missing value at the end AFTER the calculation
    :return:
    """

    validate_timeseries_data(df)
    input_df_index_name = df.index.name

    start_date = df.sort_index().index.min() if start is None else start
    end_date = df.sort_index().index.max() if end is None else end
    keys = pd.bdate_range(start=start_date,end=end_date) if bday_only else pd.date_range(start=start_date,end=end_date)
    cols = df.columns.tolist()
    raw_df = df.reindex(keys)
    expanding_zscore_list = []
    for col in df.columns.tolist():
        raw_data = raw_df[col].sort_index().dropna()
        raw_data = trim_series (raw_df[col].sort_index()).fillna(method=fill_missing_method).dropna()
        expanding_avg = pd.expanding_mean(raw_data)
        expanding_stdev = pd.expanding_std(raw_data)
        expanding_zscore_series = ((raw_data - expanding_avg)/expanding_stdev) \
                                .replace([np.inf, -np.inf], np.nan) \
                                .reindex(keys) \
                                .fillna(method=fill_missing_method)
        expanding_zscore_list.append(pd.DataFrame(expanding_zscore_series,columns=[col]))

    expanding_zscore_df = pd.concat(expanding_zscore_list, join='outer', axis=1)

    if fill_missing_at_the_end: expanding_zscore_df = expanding_zscore_df.fillna(method=fill_missing_method)[cols]

    # reapply the index name from the input df
    expanding_zscore_df.index.name = input_df_index_name

    return expanding_zscore_df

#------------------------------------------------------------------------------------------------
def weighted_sum(row,
                 weights,
                 use_full_row_only=False,
                 rescale_weights_to_1 = False):
#------------------------------------------------------------------------------------------------
    """ A proper weighted sum function that can re-weight the weights to ignore the missing data point !
    When the weights adds up to 1 or the rescale_weights_to_1 is set to True, this function is calculate the weighted average!!!!
    :param row:
    :param weights:
    :param use_full_row_only:
    :return:
    """
    #logging.info("process row: [%s] with weights [%s] " % (",".join(map(str,row)), ",".join(map(str,weights))))

    weights_sum = np.nansum(weights) * 1.0

    weights = weights if rescale_weights_to_1 == False \
                      else  [weight / weights_sum for weight in weights]

    if (use_full_row_only and len(row.dropna()) <> len(row)) :
        #logging.debug("ignore row with NAN as per input setting")
        return np.NAN
    else:
        if len(row.dropna()) == 0:
            #logging.debug("ignore row with all NAN")
            return np.NAN
        else:
            # re-weight the weights based on available data point in the row
            #logging.debug("re-weight the weights based on the available dps")

            ######################################
            ### Warning!
            ### if the series is not considered as a proper float64, it may have divided by zero error!
            ### pandas is full of trap. to_numeric on df dont work!
            ### Also the series can cause np.nansum to fail to return a number
            ######################################

            #r = row.copy()
            #r[r==0] = 1
            #dps = r/r
            dps=row/row

            #logging.debug(row)
            valid_weights = weights * dps
            #logging.debug("available weights : %s" % ",".join(map(str,valid_weights)))
            reweighted_weights = valid_weights / (valid_weights.sum() / np.nansum(weights))
            #logging.debug("re-weighted weights : %s" % ",".join(map(str,reweighted_weights)))
            reweighted_dps = row * reweighted_weights
            #logging.debug("re-weighted data points : %s" % ",".join(map(str,reweighted_dps)))
            #total = np.nansum(reweighted_dps)
            total = reweighted_dps.sum()
            #logging.debug("weighted sum %f" % total)
            return total

#------------------------------------------------------------------------------------------------
def weighted_avg_df_columns(df,
                            avg_col_name,
                            cols=None,
                            weights =None,
                            use_full_row_only = False):
#------------------------------------------------------------------------------------------------
    """ The function takes in a df and aggregates the selected columns into a single column using the weights provided.
    The cols and weights have to be the same length. The sum of the weights have to be 1.
    :param df: the input dataframe whose columns containing time series will be aggregated using row based weighted average
    :param avg_col_name: the name of the column of the output series
    :param cols: the selected columns from input df which will participate the weighted average
    :param weights: a interable of assigned weights to the selected columns. They have to be summed to 1 or None (equal weights)
    :param use_full_row_only: a True False flag that will decide whether to skip row if there is missing data from the columns
    :return:
    """

    logging.debug("Weight avg for df " + avg_col_name )

    if avg_col_name == 'AUS':
        print "AUS"

    validate_timeseries_data(df)

    df = recolumn_df_data(df.copy(), cols = cols)

    cols = df.columns.tolist() if cols is None else cols

    #if weights is not None and np.round(np.nansum(weights),6) <> 1:
    #    raise ValueError("The weights must be summed to 1 or None which means equal weights")

    if weights is not None:
        weights_sum = np.nansum(weights) * 1.0
        weights = [weight / weights_sum for weight in weights]

    weights = [1.0 / len(df.columns)] * len(df.columns) if weights is None else weights

    df1 = df[list(cols)]


    weighted_avg = df1.apply(lambda row, w = weights, f = use_full_row_only : weighted_sum(row.astype('float64'),w,use_full_row_only=f), axis = 1)

    df2 = pd.DataFrame({avg_col_name:weighted_avg},index=df.index)
    #logging.debug("weighted average:")
    #logging.debug(df2)

    return df2

#------------------------------------------------------------------------------------------------
def dynamic_weighted_avg_df_columns(df,
                                    avg_col_name,
                                    cols=None,
                                    weights_df =None,
                                    use_full_row_only = False):
#------------------------------------------------------------------------------------------------
    """ The function takes in a df and aggregates the selected columns into a single column using the weights provided.
    The cols and weights have to be the same length. The sum of the weights have to be 1.
    :param df: the input dataframe whose columns containing time series will be aggregated using row based weighted average
    :param avg_col_name: the name of the column of the output series
    :param cols: the selected columns from input df which will participate the weighted average
    :param weights_df: a dataframe whose columns are the weights interable of assigned weights to the selected columns. They have to be summed to 1 or None (equal weights)
    :param use_full_row_only: a True False flag that will decide whether to skip row if there is missing data from the columns
    :return:
    """

    logging.debug("dynamic weight avg for df " + avg_col_name )

    validate_timeseries_data(df)

    df = recolumn_df_data(df.copy(), cols = cols)
    cols = df.columns.tolist() if cols is None else cols

    #if weights is not None and np.round(np.nansum(weights),6) <> 1:
    #    raise ValueError("The weights must be summed to 1 or None which means equal weights")

    _eq_weights = [1.0 / len(df.columns)] * len(df.columns)
    df1 = df[list(cols)]

    def reweight(weights):
        weights_sum = np.nansum(weights) * 1.0
        weights = [weight / weights_sum for weight in weights]
        return weights

    def get_weight(lookup_key):
        sub_df = weights_df[weights_df.index <= lookup_key]
        weights = reweight(sub_df.iloc[-1].fillna(0).values) if len(sub_df.index) > 0 else _eq_weights
        return weights

    weighted_avg = df1.apply(lambda row, f = use_full_row_only : weighted_sum(row.astype('float64'), get_weight(row.name),use_full_row_only=f), axis = 1)

    df2 = pd.DataFrame({avg_col_name:weighted_avg},index=df.index)
    #logging.debug("weighted average:")
    #logging.debug(df2)

    return df2



#------------------------------------------------------------------------------------------------
def count_sign(row):
#------------------------------------------------------------------------------------------------
    """ The function takes a collection of numbers and work out the number of positive,negative  number
    :param row: a collection of numbers
    :return: a dictionary containing the number of positive/negative/zero/total number of value
    """
    pos_cnt = np.nansum(row > 0)
    neg_cnt = np.nansum(row < 0)
    zero_cnt = np.nansum(row == 0)
    ttl_cnt = np.count_nonzero( ~ np.isnan(row.tolist()))
    return pd.Series({"POSITIVE":pos_cnt/(ttl_cnt * 1.0),
                      "NEGATIVE":neg_cnt/(ttl_cnt * 1.0),
                      "ZERO":zero_cnt/(ttl_cnt * 1.0),
                      "TOTAL": ttl_cnt})


#------------------------------------------------------------------------------------------------
def breadth_model(df,
                  breadth_calc_func,
                  cols=None):
#------------------------------------------------------------------------------------------------
    validate_timeseries_data(df)
    df = recolumn_df_data(df.copy(), cols = cols)
    cols = df.columns.tolist() if cols is None else cols
    df1 = df[list(cols)]
    df2 = df1.apply(breadth_calc_func, axis = 1)
    return df2

#------------------------------------------------------------------------------------------------
def subset_df_by_columns(df,
                         subset_columns,
                         rename_columns):
#------------------------------------------------------------------------------------------------
    """ The function subsets a df using collections of columns name and rename the columns.
    :param df: original data frame
    :param subset_columns: columns used to subset a df
    :param rename_columns: columns used to replace the subset columns' name
    :return: a subset and renamed columns
    """
    if not isinstance(subset_columns, list): raise TypeError("subset_columns is expected to be a collection")
    if not isinstance(rename_columns, list): raise TypeError("rename_columns is expected to be a collection")
    if len(rename_columns) <> len(subset_columns) : raise ValueError("rename_columns and subset_columns must be the same length")

    df_out = df.copy()[subset_columns]
    df_out.columns = rename_columns
    return df_out

#------------------------------------------------------------------------------------------------
def unpivot_df(df,
               id_column_name,
               unpvted_column_names,
               particiating_column_names = None,
               derive_id_column_from_row_idx = True):
#------------------------------------------------------------------------------------------------
    """ This function will unpvt a df based on the input id column. the id column can be one of the existing column or recreated from idex column.
    :param df: original data frame
    :param id_column_name: the column name will be used to unpivot each row of original df
    :param unpvted_column_name: the column names will be used to name the unpvted df. they need to be database friendly
    :param particiating_column_names: optional parameter to select participating columns from the original df into the unpvt process. if it is ignored
    all columns will be used.
    :param derive_id_column_from_row_idx: optional parameter to copy the index column back into the df as a normal column
    :return: an unpvted df
    """

    if particiating_column_names is not None:
        if not isinstance(particiating_column_names, list):
            raise TypeError("particiating_column_names is either a list or None")

    if not isinstance(unpvted_column_names, list): raise TypeError("upvted_column_name is expected to be a list")

    df_in = df.copy()

    df_copy = df_in.loc[:, (particiating_column_names)] if (particiating_column_names is not None) and isinstance(particiating_column_names, list) else df_in

    if derive_id_column_from_row_idx:
        df_copy.loc[:,(id_column_name)] = df_copy.index

    unpvted_df = pd.melt(df_copy, id_vars=[id_column_name], value_vars=particiating_column_names)

    unpvted_df.columns = unpvted_column_names

    return unpvted_df


#------------------------------------------------------------------------------------------------
def realign_ts_to_weekdays(ts, start_date=None, end_date=None):
#------------------------------------------------------------------------------------------------
    date_range = pd.date_range(ts.index.min(), ts.index.max(), freq=BDay()) if (start_date is None or end_date is None) else pd.date_range(start_date, end_date, freq=BDay())
    ts_out = ts.reindex(date_range).fillna(method="ffill")
    return ts_out


#------------------------------------------------------------------------------------------------
def create_empty_numeric_df(index, cols):
#------------------------------------------------------------------------------------------------
    return pd.DataFrame(index=index, columns=cols).astype("float64")


#------------------------------------------------------------------------------------------------
def trim_series(ts):
#------------------------------------------------------------------------------------------------
    fi = ts.first_valid_index()
    li = ts.last_valid_index()
    return ts[fi:li]

#------------------------------------------------------------------------------------------------
def ltrim(pdobj):
#------------------------------------------------------------------------------------------------
    fi = pdobj.first_valid_index()
    return pdobj[fi:]

#------------------------------------------------------------------------------------------------
def rtrim(pdobj):
#------------------------------------------------------------------------------------------------
    li = pdobj.first_valid_index()
    return pdobj[:li]

#------------------------------------------------------------------------------------------------
def ltrim_n_from_series(ts, n, valid_dp_only=False):
#------------------------------------------------------------------------------------------------
    if valid_dp_only:
        ts = ltrim(ts)
        return ts [n:ts.size]
    else:
        return ts[n:ts.size]



