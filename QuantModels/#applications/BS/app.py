import pandas as pd
from pandas.tseries.offsets import DateOffset
import numpy as np
import pca as pca
%matplotlib inline



def smooth_series(ds, lim=2, half_window=1):

    def smooth(row):

        if np.isnan(row['ds_prev']) or np.isnan(row['ds_next']):
            return row['ds']

        if abs(row['ds_prev'] - row['ds']) > lim and abs(row['ds_next'] - row['ds']) > lim and abs((row['ds_prev'] + row['ds_next'])/2 - row['ds']) > lim:
            return (row['ds_prev'] + row['ds_next'])/2
        else:
            return row['ds']

    data = pd.DataFrame({'ds':ds, 'ds_prev': ds.shift(half_window), 'ds_next': ds.shift(-1*half_window)})

    return data.apply(smooth, axis=1)
 
    

def smooth_rates(data, limit, half_window): 
    out = {}
    for col, ds in data.iteritems():
        out[col] = smooth_series(ds, lim=limit, half_window=half_window)
    
    return pd.DataFrame(out)



def create_pca(data, date_index, momentum_size=None, resample_freq= 'W'):
    data_sub = data[date_index].resample(resample_freq).last() if resample_freq is not None else data[date_index]    
    return pca.PCA(data_sub, rates_pca = True, momentum_size = momentum_size)


file_date = '2019-07-18'
pca_history = 60 #month


data = pd.read_csv(file_date + '_BS_ALL_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


# you can see the data has lots of noises pre 2012. This one is difficult to smooth, as there are gaps. 
data.plot(figsize=(30,15))
#smooth_rates(data, 50, 1).plot(figsize=(30,15))

#filter out only last 60month
history_end = data.index[-1] - DateOffset(years = 0, months=pca_history, days=0)


aud = data[[c for c in data.columns if 'AUD' in c]]
jpy = data[[c for c in data.columns if 'JPY' in c]]
eur = data[[c for c in data.columns if 'EUR' in c]]

#create pca 
aud_pca = create_pca(aud, aud.index> history_end)
jpy_pca = create_pca(jpy, jpy.index> history_end)
eur_pca = create_pca(eur, jpy.index> history_end)

aud_pca.plot_loadings(n=5, m=7).show()



