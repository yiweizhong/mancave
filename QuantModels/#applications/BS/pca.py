from __future__ import division
import pandas as pd
import numpy as np
import scipy as sp
import math
import numbers
from datetime import datetime
from datetime import timedelta
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from itertools import permutations, combinations


np.set_printoptions(suppress=True)


class PCA(object):
    def __init__(self, data, matrixtype="COV", rates_pca=False, name=None, momentum_size=None):
        """ A generic Principle Component Analysis object
            parameter:
            ----------
                data:dataframe: 
                    Input data
                matrixtype:str
                    Whether to use covariance or correlation matrix for the eigen decomposition.
                    The valid values are "COV" or "CORR"  
                momentum_size:nullable integer
                    Whether to use the momentum of the data for eigen decomoposition. If the value is not None the Cov and Covariance will be based on the
                    specified momentum.   
        """
        
        if len(data.columns) < 2:
            raise ValueError("Minimal 2 underline data series are required for PCA calc ")
        if matrixtype not in ("COV", "CORR"):
            raise ValueError("The matrixtype must be either COV or CORR")
        if momentum_size is not None:
            if (isinstance(momentum_size,numbers.Integral)) and momentum_size > 0:
                pass
            else:
                raise ValueError("The momentum_size must be either None or a positive integer")



        self._name = name
        self._rates_pca = rates_pca
        self._data = data
        self._matrixtype = matrixtype
        self._momentum_size = momentum_size
        self._data_mean = self.data.mean()
        self._centered_data = self._data.apply(lambda col: col - col.mean(), axis=0)
        self._eigvals, self._eigvecs, self._eigvals_raw, self._eigvecs_roots = self.get_eigens()
        self._pcs = self.pcs
        (self._reconstructed_data, self._reconstruction_pcs) = self.reconstruct_data_by_n_pc(n=[1, 2, 3])
        self._hedge_weighted_data, self._hedge_w, self._hedge_corr_test, self._hedge_description = self.hedge_pcs()


    def get_eigens(self):
        """ calculate the eigen decomposition
            returns:
            --------
                result: tuple
                    1st element is the eigenvals expressed in pct of total variance (1d numpy array)
                    2nd element is the eigenvecs (2d numpy array). The 2d array eigen vectors are column based. 
                    in order to access an eigen value's eigen vector using a single Index the eigen vector 2d array needs 
                    to be transposed first.
                    3rd element is the eigenvals in value (1d numpy array)
        """

        def get_roots(eigvecs):
            def find_cross(a):
                if a[0] * a[1] < 0:
                    return 1
                else:
                    return 0
            for eigvec in eigvecs:
                arr1 = eigvec[:-1]
                arr2 = eigvec[1:]
                roots = np.apply_along_axis(find_cross, 0, np.array([arr1, arr2]))
                yield roots.sum()

        matrix = self.cov if self._matrixtype == "COV" else self.corr
        eigvals, eigvecs = np.linalg.eig(matrix)
        eigvecs = eigvecs[:, eigvals.argsort()[::-1]]

        #changing signs for rates pca
        if(self._rates_pca):
            eigvecs = eigvecs.T
            #pc1 (level is a positive function of pc1 )
            eigvecs[0] = -1 * eigvecs[0] if len(np.extract(eigvecs[0] > 0, eigvecs[0]))/len(eigvecs[0]) < 0.5 else eigvecs[0] 
            
            #PC2 (curve steepening is a positive function of pc2)
            #left_half = eigvecs[1][:int((len(eigvecs[1]) + 1)/2.0)]
            betas = np.polyfit(np.arange(len(eigvecs[1])), eigvecs[1], deg = 1, full=False)
            eigvecs[1] = -1 * eigvecs[1] if betas[0] < 0 else eigvecs[1]
            
            #PC3 (belly is a positive function of PC3)
            if(len(eigvecs) >2):
                chop_size = int((len(eigvecs[2]) + 1)/4.0)
                belly = eigvecs[2][chop_size+1:len(eigvecs[2]) - chop_size + 1]
                eigvecs[2] = -1 * eigvecs[2] if len(np.extract(belly > 0, belly))/len(belly) < 0.5 else eigvecs[2]
            
            eigvecs = eigvecs.T

        eigvals = eigvals[eigvals.argsort()[::-1]]
        eigvals_pct = eigvals / eigvals.sum()
        eigvecs_roots = list(get_roots(eigvecs.T))
        return eigvals_pct, eigvecs, eigvals, eigvecs_roots

    def reconstruct_data_by_n_pc(self, n=[1, 2, 3]):
        """After the original data are decomposed into principle components, they can be reconstructed
            via the principle components. One way to filter out noises are to ignore the eigen vectors
            whose eigen values' explainatory powers are low.

            parameters:
            ----------- 
                n an array of int: 
                    the eigen value index to be included in the value reconstruction
            Returns:
            -------
                result: dataframe 
                    principle components
                
        """
        assert isinstance(n, (list))
        assert not isinstance(n, str)

        selectedloadings = self.loadings.copy()
        pcs = self.pcs.copy()

        # pandas checks not only the dimension but also the column index name are matching!
        pcs.columns = [c.replace("PC", "Loading") for c in pcs.columns.tolist()]

        for col in selectedloadings.columns.tolist():
            if col not in ["Loading" + str(i) for i in n]:
                selectedloadings[col] = 0

        loadingsT = selectedloadings.transpose()

        self._reconstructed_data = pcs.dot(loadingsT)
        self._reconstruction_pcs = n

        return (self._reconstructed_data, self._reconstruction_pcs)


    def data_gaps_from_reconstruction(self, n=[1, 2, 3]):   
        """Calculate the valuation gaps from the actual data and restructed data using reduced number 
            of PCs
            parameters:
            ----------- 
                n an array of int: 
                    the eigen value index to be included in the value reconstruction
            Returns:
            -------
                result: dataframe 
                    valuation gap time series
        """
        (_reconstructed_data, _reconstruction_pcs) = self.reconstruct_data_by_n_pc(n=n)
        return self.centered_data - _reconstructed_data


    def hedge_pcs(self):
        """Calculate the weights that will hedge all the pcs except for the last one
            Returns:
            --------
                weighted data: series
                    for 2 securitues this series will be a pc1 hedged curve. for 3 securities this series will be
                    a pc1, pc2 hedged fly. 
                weights: series
                    the weights to produce the above weighted series
                test_corr: dataframe
                    a matrix shows the correlation between weighted the series and hedged pcs  
                description: string
                    describe the weighted series
        """
        #hedging all the pcs except for the last one
        #always anchoring the weights on the first instrument
        a = self.eigvecs[:-1]
        a_len = len(a[0])
        a_fixed =  np.zeros(a_len)
        a_fixed[0] = 1
        a = np.vstack((a, a_fixed))
        b_len = np.shape(a)[0]
        b = np.zeros(b_len)
        b[-1] = 1
        w = np.linalg.solve(a, b)
        w = pd.Series(data=w, index=self._data.columns)
        weighted_data = self._data.dot(w)
        weighted_data_components = (self._data * w).rename(lambda n: '{}_wc'.format(n), axis='columns')

        #corr_test = self.pcs.copy()
        
        # adding the original data into the corre test. 
        corr_test = pd.concat([self.pcs, self._data, weighted_data_components],axis=1)
        corr_test['weighted_data'] = weighted_data
        corr_test = corr_test.ffill()
        corr_test = corr_test.corr()
        description = ' '.join(['%+.3f x %s' % (weight, sec) for (sec, weight) in w.iteritems()])
        weighted_data.name = description
        return  (weighted_data, w, corr_test, description)

    def hedge_pcs_on_existing_strat(self, pc, strat_w, strat_w_anchor):
        """ hedge the pc effect on an existing strat. The existing strat will have a perceived 0 net weight
            parameters:
            ----------- 
                pc: string 
                    for example 'PC1'
                strat_w: np array. 
                    The weights of an existing strat. for example for a fly it is [-1, 2, -1] its size must be the same as the PC size
                strat_w_anchor: num
                    the position of the loading to be anchored on. for example 1 in [1, 2, 3] will point to 2
            Returns:
            -------
                result: dataframe 
                    valuation gap time series
        """
        if 'PC' not in pc: raise ValueError("the parameter needs to be PC1, PC2 and etc")
        if not isinstance(strat_w, np.ndarray): raise ValueError("the parameter strat needs to be np ndarray")
        if len(strat_w) != len(self.loadings['Loading1']): raise ValueError("the parameter strat needs to be the same size as the eigen vec")
        
        w = self.loadings[pc.replace('PC', 'Loading')]
        hedged_w = strat_w / (w / w[strat_w_anchor]) 
        weighted_data = self._data.dot(hedged_w)
        corr_test = self.pcs.copy()
        corr_test['weighted_data'] = weighted_data
        corr_test = corr_test.corr()
        description = ' '.join(['%+.3f * %s' % (weight, sec) for (sec, weight) in w.iteritems()])
        weighted_data.name = description
        return  (weighted_data, hedged_w, corr_test, description)


    def risk_in_a_pc(self,pc):
        """Rather than hedging pcs, this will return the weighted series if risks are take to exposure to a specific PC
            Returns:
            --------
                weighted data: series 
                weights: series
                    the weights to produce the above weighted series
                test_corr: dataframe
                    a matrix shows the correlation between weighted the series and hedged pcs  
                description: string
                    describe the weighted series
        """
        if 'PC' not in pc: raise ValueError("the parameter needs to be PC1, PC2 and etc")
        w = self.loadings[pc.replace('PC', 'Loading')]        
        weighted_data = self._data.dot(w)
        corr_test = self.pcs.copy()
        corr_test['weighted_data'] = weighted_data
        corr_test = corr_test.corr()
        description = ' '.join(['%+.3f * %s' % (weight, sec) for (sec, weight) in w.iteritems()])
        weighted_data.name = description
        return  (weighted_data, w, corr_test, description)


    def plot_residules(self, n = 3):
        """[summary]
        """
        n = len(self.eigvals) if n is None else n
        f = plt.figure(figsize = (5, n * 2))

        gs = gridspec.GridSpec(n,1,height_ratios=n * [1])

        for i in np.arange(n):
            gss = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gs[i])
            ax = plt.subplot(gss[0])
            #print list(np.arange(i+1) + 1)
            gaps = self.data_gaps_from_reconstruction(n=list(np.arange(i+1) + 1))
            gaps.iloc[-1,:].plot(kind='bar', ax = ax, fontsize = 6)  
            ax.axhline(y=0, color='black', linewidth = 0.5)
            ax.xaxis.set_tick_params(rotation=0)
            for p in ax.patches:
                ax.annotate(str(p.get_height().round(3)), 
                            (p.get_x() * 1.005, p.get_height() * 1.005), 
                            fontsize=6)
        f.tight_layout()
        return f


    def plot_loadings(self, n=None, m=None):
        """[summary]
         parameters:
            ----------- 
                n int: 
                    the number of pcs to chart
                m int:
                    the m days ago valuation. when set a forth column will be included 
        """
        n = len(self.eigvals) if n is None else n
        w = 15 if m is None else 18
        f = plt.figure(figsize = (w, n * 2))

        gs = gridspec.GridSpec(n,1,height_ratios=n * [1])

        ax_rr_x_lim = None
        ax_rr_y_lim = None
        ax_rrr_x_lim = None
        ax_rrr_y_lim = None
        
        
        for i in np.arange(n):
            gss = gridspec.GridSpecFromSubplotSpec(1, 3, subplot_spec=gs[i]) if m is None else gridspec.GridSpecFromSubplotSpec(1, 4, subplot_spec=gs[i])
            ax_l = plt.subplot(gss[0])
            ax_r = plt.subplot(gss[1])
            ax_rr = plt.subplot(gss[2])
            ax_rrr = None if m is None else plt.subplot(gss[3])

            self.loadings.iloc[:,i].plot(kind='bar', ax = ax_l, fontsize = 6)    
            self.pcs.iloc[:,i].plot(ax = ax_r, fontsize = 6)
            ax_l.xaxis.set_tick_params(rotation=45)
            
            ax_l.spines['right'].set_visible(False)
            ax_l.spines['top'].set_visible(False)
            ax_l.spines['bottom'].set_visible(False)
            ax_l.axhline(y=0, color='black', linewidth = 0.5)

            ax_r.xaxis.label.set_visible(False)
            ax_r.spines['right'].set_visible(False)
            ax_r.spines['top'].set_visible(False)
            ax_r.axhline(y=0, color='black', linewidth = 0.5)

            


            for p in ax_l.patches:
                ax_l.annotate(str(p.get_height().round(3)), 
                              #(p.get_x() * 1.005, p.get_height() * 1.005),
                              (p.get_x() * 1.005, ax_l.get_ylim()[1] * 0.95), 
                              fontsize=6)

            r = mpl.patches.Rectangle((0,0), 1, 1, fill=False, edgecolor='none',visible=False)
            ax_r.legend([r],['lambda %.2f' %self.loadings_lambda.iloc[i]], fancybox=True, framealpha=0, prop={'size': 6})  

            gaps = self.data_gaps_from_reconstruction(n=list(np.arange(i+1) + 1))
            gaps.iloc[-1,:].plot(kind='bar', ax = ax_rr, fontsize = 6)  
            ax_rr.axhline(y=0, color='black', linewidth = 0.5)
            ax_rr.xaxis.set_tick_params(rotation=45)
            ax_rr.spines['right'].set_visible(False)
            ax_rr.spines['top'].set_visible(False)
            ax_rr.spines['bottom'].set_visible(False)

            if ax_rrr is not None:
                ax_rr.set_ylim(ax_rr.get_ylim()[0], ax_rr.get_ylim()[1]*1.2)

            gaps_movement = gaps.iloc[-1,:] - gaps.iloc[-1 * m,:] if m is not None else None
            underline_movement = self.data.iloc[-1,:] - self.data.iloc[-1 * m,:] if m is not None else None

            pi = 0
            for p in ax_rr.patches:
                label = str(p.get_height().round(3))
                if ax_rrr is not None:
                    label = label + '\n' + str(gaps_movement[pi].round(3)) + '\n' + str(underline_movement[pi].round(3)) 
                
                ax_rr.annotate(label, 
                #(p.get_x() * 1.005, p.get_height() * 1.005),
                (p.get_x() * 1.005, ax_rr.get_ylim()[1] * 0.85) if ax_rr_y_lim is None else (p.get_x() * 1.005, ax_rr_y_lim * 0.85) , 
                fontsize=6)
                pi = pi + 1

            if(ax_rr_x_lim == None and ax_rr_y_lim == None):
                (ax_rr_x_lim, ax_rr_y_lim) = ax_rr.get_ylim()
            else:
                ax_rr.set_ylim(ax_rr_x_lim, ax_rr_y_lim)


            if ax_rrr is not None:
                
                self.pcs.iloc[-1*m:, i].plot(ax = ax_rrr, fontsize = 6)
                ax_rrr.xaxis.label.set_visible(False)
                #ax_rrr.axhline(y=0, color='black', linewidth = 0.5)
                ax_rrr.spines['right'].set_visible(False)
                ax_rrr.spines['top'].set_visible(False)

                ax_rrr.legend([r],['lambda %.2f' %self.loadings_lambda.iloc[i]], fancybox=True, framealpha=0, prop={'size': 6})

        f.tight_layout()

        return f


    def plot_valuation_history(self, days, n=[1, 2, 3], w=16, h=12, rows=4, cols=4, normalise_valuation_yaxis = True, normalise_data_yaxis=False):
        """[summary]
        parameters:
            ----------- 
                days int: 
                    the number of days of history
                n int array:
                    the pcs to hedge 
                w int:
                    width
                h int:
                    height
                rows int:
                    number of rows in the chart grid
                cols int: 
                    number of the cols in the chart grid
                normalise_valuation_yaxis bool:
                    use the movement of the valuation since the beginning of the history instead of actual valuation
                normalise_data_yaxis bool:
                    use the move of the data since the beginning of the history instead of the actual data
                
        """
        
        r1 = mpl.patches.Rectangle((0,0), 1, 0.01, fill=True, edgecolor='none', facecolor='C0')
        r2 = mpl.patches.Rectangle((0,0), 1, 0.01, fill=True, edgecolor='none', facecolor='C1')
        
        gaps = self.data_gaps_from_reconstruction(n=n)
        n = len(gaps.columns)
        f = plt.figure(figsize = (w, h))
        gs = gridspec.GridSpec(rows, cols, width_ratios=cols * [1], height_ratios=rows * [1])
        ylim_low = None
        ylim_high = None

        ylim_data_low = None
        ylim_data_high = None
        
        axs = {}
        ax_twins = {}
        for i in np.arange(n):
            gss = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gs[i])
            ax = plt.subplot(gss[0])
            ax_twin = ax.twinx()
            s = gaps.iloc[-1* days:, i] if not normalise_valuation_yaxis else gaps.iloc[-1* days:, i] - gaps.iloc[-1* days, i]
            datas = self.data[s.name][-1*days:] if not normalise_data_yaxis else self.data[s.name][-1*days:] - self.data[s.name][-1*days]
            s.plot(ax=ax, fontsize = 6, linewidth=1, color='C0')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            
            (curr_ylim_low, curr_ylim_high) = ax.get_ylim()
            ylim_low = curr_ylim_low if ylim_low is None else ylim_low if ylim_low <= curr_ylim_low else curr_ylim_low
            ylim_high = curr_ylim_high if ylim_high is None else ylim_high if ylim_high >= curr_ylim_high else curr_ylim_high
            
            ax.xaxis.label.set_visible(False)
            #ax.yaxis.grid(True, which='major', linestyle='-', linewidth=0.25)
            
            datas.plot(ax=ax_twin, fontsize=6, linewidth=1, color='C1')

            (curr_data_ylim_low, curr_data_ylim_high) = ax_twin.get_ylim()
            ylim_data_low = curr_data_ylim_low if ylim_data_low is None else ylim_data_low if ylim_data_low <= curr_data_ylim_low else curr_data_ylim_low
            ylim_data_high = curr_data_ylim_high if ylim_data_high is None else ylim_data_high if ylim_data_high >= curr_data_ylim_high else curr_data_ylim_high   

            
            lgd_data = '{0} @ {1:.2f}'.format(datas.name,  datas.iloc[-1]) if not normalise_data_yaxis else '{0} moved {1:.2f}'.format(datas.name,  datas.iloc[-1]) 
            lgd_valuation = 'valuation @ {0:.2f}'.format(s.iloc[-1]) if not normalise_valuation_yaxis else 'valuation moved {0:.2f}'.format(s.iloc[-1])
            
            #ax.legend([r1,r2],[lgd_data, lgd_valuation], fancybox=True, framealpha=0, prop={'size': 6})
            ax_twin.legend([r2],[lgd_data], fancybox=True, framealpha=0, prop={'size': 6}, loc='upper right')
            ax.legend([r1],[lgd_valuation], fancybox=True, framealpha=0, prop={'size': 6}, loc='upper left')
            
            axs[i] = ax
            ax_twins[i] = ax_twin
        
        #if normalise_valuation_yaxis:
        #always normalise the scale for valuation.
        for i in np.arange(n):
            axs[i].set_ylim([ylim_low, ylim_high])

        if normalise_data_yaxis:
            for i in np.arange(n):
                ax_twins[i].set_ylim([ylim_data_low, ylim_data_high])


        f.tight_layout()

        return f

    def plot_loadings_relationship(self, n=3):
        """[summary]
        """
        n = len(self.eigvals) if n is None else n
        lp = list(combinations(self.loadings.columns[:n], 2))
        n = int(len(lp)/2) + 1
        m = len(lp)
        f = plt.figure(figsize = (n * 3, n * 3))

        gs = gridspec.GridSpec(n,n, height_ratios= n * [1], width_ratios= n * [1])
        
        i=0
        for (a, b) in lp:
             gss = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gs[i])
             ax = plt.subplot(gss[0])
             for idx in self.loadings.index:
                 ax.scatter(self.loadings[a][idx], self.loadings[b][idx], c='blue', label=idx)
                 ax.annotate(idx, (self.loadings[a][idx], self.loadings[b][idx]),  fontsize=6)
                 ax.set_xlabel(a, fontsize=6, labelpad = 0)
                 ax.set_ylabel(b, fontsize=6, labelpad = 0)
             i = i + 1
             #print i
             ax.axhline(y=0, color='black', linewidth = 0.5)
             ax.axvline(x=0, color='black', linewidth = 0.5)
             #ax.set_xlim((-1,1))
             #ax.set_ylim((-1,1))
             
        f.tight_layout()
        return f

    
    def plot_hedged_weighted_data_ax(self, ax):

        def plot_df_lines(ax, idx, data, **kwargs):

            min_x = idx.min()
            max_x = idx.max()

            ln = ax.plot(idx, data, **kwargs)
            ax.set_xlim([min_x, max_x])

            return ln

        lines = []
        
        for col in self.data.columns:
            lines =  lines + plot_df_lines(ax, self.data.index, self.data[col], ls='-', linewidth=1, marker=None)
             
        # adding correlation info to the series name

        _, _, _hedge_corr_test, _ = self.pc_hedge
        _hedge_corr_test = _hedge_corr_test['weighted_data']

        l_labels = ['{0} corr: ({1})'.format(l.get_label(), round(_hedge_corr_test[l.get_label()], 2)) for l in lines]

        #
        ax_twinx = ax.twinx()
        ax_twinx._get_lines.prop_cycler = ax._get_lines.prop_cycler

        lr = plot_df_lines(ax_twinx, self._hedge_weighted_data.index, self._hedge_weighted_data, 
                            ls='-', linewidth=1, marker=None)

        lines = lines + lr
        l_labels = l_labels + ['weighted series (rhs)']
        
        ax.set_title(self._hedge_weighted_data.name, fontsize = 8)
        #ax.xaxis.label.set_visible(True)
        #ax.fmt_xdata = mdates.DateFormatter('%Y-%m-%d')
        all_years = mdates.YearLocator()   # every year
        qtr_end_months = mdates.MonthLocator(bymonth=(4,7,10,13))  # every month
        all_days = mdates.DayLocator() # every day
        every_mondays = mdates.WeekdayLocator(byweekday=mdates.MONDAY) #every monday	
        ax.xaxis.set_major_locator(all_years)
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
        ax.xaxis.set_minor_locator(qtr_end_months)

        for label in ax_twinx.get_yticklabels():
            label.set_fontsize(6)

        for label in ax.get_yticklabels():
            label.set_fontsize(6)

        for label in ax.get_xticklabels():
            label.set_fontsize(6)

        ax.xaxis.grid(True, which ='both', linewidth=0.2)

        leg = ax.legend(lines, l_labels, fancybox=True, prop={'size': 6})
        leg.get_frame().set_facecolor('none')
        leg.get_frame().set_linewidth(0.0)

        
    def plot_pcs_ax(self, ax):

        def plot_df_lines(ax, idx, data, **kwargs):
            min_x = idx.min()
            max_x = idx.max()
            ln = ax.plot(idx, data, **kwargs)
            ax.set_xlim([min_x, max_x])
            return ln
        lines = []

        for col in self.pcs.columns[:-1]:
            lines =  lines + plot_df_lines(ax, self.pcs.index, self.pcs[col], ls='-', linewidth=1, marker=None)
        
        l_labels = [l.get_label() for l in lines]

        #
        ax_twinx = ax.twinx()
        ax_twinx._get_lines.prop_cycler = ax._get_lines.prop_cycler

        lr = plot_df_lines(ax_twinx, self.pcs.index, self.pcs[self.pcs.columns[-1]], 
                            ls='-', linewidth=1, marker=None)

        lines = lines + lr
        l_labels = l_labels + [self.pcs.columns[-1] + ' (rhs)'] 

        ax.axhline(y=0, color='black', linewidth = 0.5)

        all_years = mdates.YearLocator()   # every year
        qtr_end_months = mdates.MonthLocator(bymonth=(4,7,10,13))  # every month
        all_days = mdates.DayLocator() # every day
        every_mondays = mdates.WeekdayLocator(byweekday=mdates.MONDAY) #every monday	
        ax.xaxis.set_major_locator(all_years)
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
        ax.xaxis.set_minor_locator(qtr_end_months)

        for label in ax_twinx.get_yticklabels():
            label.set_fontsize(6)

        for label in ax.get_yticklabels():
            label.set_fontsize(6)

        for label in ax.get_xticklabels():
            label.set_fontsize(6)

        ax.xaxis.grid(True, which ='both', linewidth=0.2)

        leg = ax.legend(lines, l_labels, fancybox=True, prop={'size': 6})
        leg.get_frame().set_facecolor('none')
        leg.get_frame().set_linewidth(0.0)



    def plot_loadings_ax(self, ax):

        # ax_d1 for loadings
        self.loadings.plot(kind='bar', ax = ax, fontsize = 6)
        ax.xaxis.set_tick_params(rotation=0)
        ax.axhline(y=0, color='black', linewidth = 0.5)

        for p in ax.patches:
            ax.annotate(str(p.get_height().round(3)), (p.get_x() * 1.005, p.get_height() * 1.005), fontsize=6)

        #leg = ax.legend(lines, l_labels, fancybox=True, prop={'size': 6})
        #ax.legend().get_frame().set_facecolor('none')
        #ax.legend().get_frame().set_linewidth(0.0)
        plt.setp(ax.legend().get_texts(), fontsize=6)


    def plot_loadings_lambda_ax(self, ax):

        self.loadings_lambda.plot(kind='bar', ax = ax, fontsize = 6)
        ax.xaxis.set_tick_params(rotation=0)
        ax.axhline(y=0, color='black', linewidth = 0.5)
        
        for p in ax.patches:
            ax.annotate(str(p.get_height().round(3)), (p.get_x() * 1.005, p.get_height() * 1.005), fontsize=6)

        #ax.legend().get_frame().set_facecolor('none')
        #ax.legend().get_frame().set_linewidth(0.0)
        plt.setp(ax.legend().get_texts(), fontsize=6)

    def create_hedged_pcs_fig(self):

        #weighted_data, w, corr_test, description = self.hedge_pcs()

        f = plt.figure(figsize = (9, 9))

        gs = gridspec.GridSpec(3, 1,height_ratios=[1,1,1])
        gs_u = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gs[0])
        gs_m = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=gs[1])
        gs_d = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=gs[2])

        ax_u = plt.subplot(gs_u[0])
        ax_m = plt.subplot(gs_m[0])
        ax_d1 = plt.subplot(gs_d[0])
        ax_d2 = plt.subplot(gs_d[1])

        self.plot_hedged_weighted_data_ax(ax_u)
        self.plot_pcs_ax(ax_m)
        self.plot_loadings_ax(ax_d1)
        self.plot_loadings_lambda_ax(ax_d2)


        f.tight_layout()
        return f


    @property
    def data(self):
        return self._data

    @property
    def centered_data(self):
        return self._centered_data
    
    @property
    def corr(self):
        if (self._momentum_size is None):
            return self._data.corr()
        else:
            return (self._data - self._data.shift(self._momentum_size)).corr()

    @property
    def cov(self):
        if (self._momentum_size is None):
            return self.centered_data.cov()
        else:
            return (self._data - self._data.shift(self._momentum_size)).cov()

    @property
    def pcs(self):
        """
        Returns:
            pcs: dataframe
                principle components 
        """
        pcdf = self.centered_data.dot(self._eigvecs)
        pcdf.columns = ["PC" + str(i + 1) for i in range(len(self._data.columns.tolist()))]
        return pcdf

    @property
    def loadings(self):
        loadings = {"Loading" + str(i + 1): pd.Series(self._eigvecs[:, i], index=self._data.columns.tolist())
                    for i in range(len(self._data.columns.tolist()))}
        colnames = ["Loading" + str(i + 1) for i in range(len(self._data.columns.tolist()))]
        return pd.DataFrame(loadings)[colnames]

    @property
    def eigvecs(self):
        return self._eigvecs.T

    @property
    def eigvecs_roots(self):
        return self._eigvecs_roots

    @property
    def loadings_lambda(self):
        lambda_name = ["Loading" + str(i + 1) for i in range(len(self._data.columns.tolist()))]
        return pd.Series(data = self.eigvals, name='lambda', index=lambda_name)

    @property
    def eigvals(self):
        """
        Returns:
            eigvals: 1d numpy array
                eigvals in percent 
        """
        #return np.vectorize(lambda x: '%.4f' % x)(self._eigvals)
        return self._eigvals

    @property
    def eigvals_raw(self):
        """
        Returns:
            eigvals: 1d numpy array
                eigvals in absolute value 
        """
        return self._eigvals_raw

    @property
    def pcs_used_data_reconstruction(self):
        return self._reconstruction_pcs

    
    @property
    def reconstructed_data(self):
        return self._reconstructed_data

    @property
    def reconstruction_data_gaps(self):   
        return (self.centered_data - self._reconstructed_data)
    
    @property
    def data_mean(self):
        return self._data_mean

    @property
    def name(self):
        return self._name

    @property
    def pc_hedge(self):
        return self._hedge_weighted_data, self._hedge_w, self._hedge_corr_test, self._hedge_description
    
    @property
    def pc_hedge_weighted_data(self):
        return self._hedge_weighted_data
        
    @property
    def pc_hedge_weights(self):
        return self._hedge_w
    
    @property
    def pc_hedge_weighted_data_corr_test(self):
        return self._hedge_corr_test
        
    @property
    def pc_hedge_weighted_data_desc(self):
        return self._hedge_description