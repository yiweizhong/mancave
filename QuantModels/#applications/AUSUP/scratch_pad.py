# -*- coding: utf-8 -*-
"""
Created on Sat Oct  1 08:16:07 2022

@author: zywcu
"""




import numpy as np
from scipy import stats
import pandas as pd
import os, sys, inspect
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations
import pylab
from cvxopt import matrix
import cvxopt as opt
from cvxopt import blas, solvers
from cvxopt import printing



_p = 'C:\Dev\ManCave\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\ManCave\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\ManCave\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper
from statistics.mr import tools as mrt
from statistics.mr import sbins

pd.set_option('display.expand_frame_repr', False)

printing.options['dformat'] = '%.8f'
printing.options['width'] = -1



######################################################################################## 
# misc funcs
########################################################################################



    
def reg95(xs, ys):
    r = stats.linregress(xs, y=ys)
    a = r.intercept
    a_l, a_r = a + np.array([-1* abs(r.intercept_stderr), abs(r.intercept_stderr)])* 1.96
    b = r.slope
    b_l, b_r = b + np.array([-1* abs(r.stderr), abs(r.stderr)])* 1.96
    return pd.Series({'alpha':a, 'alpha_l':a_l, 'alpha_r':a_r, 'beta':b, 'beta_l':b_l, 'beta_r':b_r})
   


def simple_risk(return_data, period_label=None):
    return pd.Series({
        'period': 'N/A' if period_label is None else period_label,
        'annual_rtn%': return_data.mean()*252*100,
        'vol%': return_data.std() * np.sqrt(252)*100,
        'sr_nocash': return_data.mean()/return_data.std()*np.sqrt(252),
        'es95%':  return_data[return_data < return_data.quantile(0.05)].mean()*100,   
        'es91%':  return_data[return_data < return_data.quantile(0.01)].mean()*100,   
        })
    
    


def convert_atmf_vol_to_yield(option, vols):

    base = 1/np.sqrt(2*3.14*12)
    if option.upper() == '1M':
        return base * np.sqrt(1) * vols
    elif option.upper() == '2M':
        return base * np.sqrt(2) * vols     
    elif option.upper() == '3M':
        return base * np.sqrt(3) * vols
    elif option.upper() == '6M':
        return base * np.sqrt(6)* vols
    elif option.upper() == '1Y':
        return base * np.sqrt(12)* vols
    elif option.upper() == '2Y':
        return base * np.sqrt(24)* vols
    elif option.upper() == '3Y':
        return base * np.sqrt(36)* vols
    elif option.upper() == '4Y':
        return base * np.sqrt(48)* vols
    elif option.upper() == '5Y':
        return base * np.sqrt(60)* vols
    elif option.upper() == '6Y':
        return base * np.sqrt(72)* vols
    elif option.upper() == '7Y':
        return base * np.sqrt(84)* vols
    elif option.upper() == '8Y':
        return base * np.sqrt(96)* vols
    elif option.upper() == '9Y':
        return base * np.sqrt(108)* vols
    elif option.upper() == '10Y':
        return base * np.sqrt(120)* vols    
    
    else:
        raise ValueError("unknown optin tenor {0}".format(option))




def plot_scatter_ts(ax, xs, ys, colors, cmaps, ticklabelsize=8):
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 9)
        
    ax.set_ylabel(ys.name, fontsize=10)
    ax.set_xlabel(xs.name, fontsize=10)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    return mapable


def plot_ts_endpoints(ax, xs, ys, colors, sizes, labels, markers, legend_loc=None,ncol=1, fontsize=10, lw =3):
    ds = [ax.scatter(xs[i], ys[i], c=colors[i], s=sizes[i], label=labels[i], marker=markers[i], linewidth= lw) for i in np.arange(len(xs))]
    if legend_loc is not None:
        lg =ax.legend(ds, labels, scatterpoints=1, fancybox=True, framealpha=0, loc=legend_loc, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lg)
    return ds
    

def plot_scatter(source_data, title = None, align_scale=False):
    
    
    xs = source_data[source_data.columns[0]]
    ys = source_data[source_data.columns[1]]
    
    start_x = xs.loc[xs.first_valid_index()]
    start_y = ys.loc[ys.first_valid_index()]
    start_dt = xs.first_valid_index().strftime('%Y-%m-%d')
    
    last_x = xs.loc[xs.last_valid_index()]
    last_y = ys.loc[ys.last_valid_index()]
    last_dt = xs.last_valid_index().strftime('%Y-%m-%d')
    
    
    f = plt.figure(figsize = (8, 8))
    gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
    ax0 = plt.subplot(gs[0])
    
    colors0 = mpl.dates.date2num(source_data.index.to_pydatetime())
    colors1 = 'lightgrey'
    
    cmap0 = plt.cm.get_cmap('binary')
    cmap1 = plt.cm.get_cmap('Reds')
    
    
    #plot main
    main_pc0 = plot_scatter_ts(ax0, xs, ys, colors0, cmap1)
    
    
    #plot end point
    endpt_pc0 = plot_ts_endpoints(ax0, [start_x, last_x], [start_y, last_y], ['lightgrey','black'], [30,30], [start_dt, last_dt], ['v','^'], legend_loc=1)
    
    if(title is not None):
        f.suptitle(title, fontsize=12)
        
    
    if align_scale:
        
        lim = source_data.abs().max().max()
        ax0.set_xlim([-lim, lim])
        ax0.set_ylim([-lim, lim])

    plt.tight_layout()
    
    
    
    
    
    
    



######################################################################################## 
# Data
########################################################################################





gbp_rate_data = pd.read_excel(r'C:\Dev\ManCave\QuantModels\#applications\AUSUP\GBP_rate.xlsx', 'Data')
gbp_rate_data['Date'] = pd.to_datetime(gbp_rate_data['Date'])
gbp_rate_data = gbp_rate_data.set_index('Date').ffill()
gbp_rate_data = gbp_rate_data[gbp_rate_data.index >= '2011-01-01']
gbp_rate_data = gbp_rate_data.rename(lambda c: '{0}_rate'.format(c), axis=1)



gbp_vol_data = pd.read_excel(r'C:\Dev\ManCave\QuantModels\#applications\AUSUP\GBP_Vol.xlsx', 'Data')
gbp_vol_data['Date'] = pd.to_datetime(gbp_vol_data['Date'])
gbp_vol_data = gbp_vol_data.set_index('Date')
gbp_vol_data = gbp_vol_data.reindex(gbp_rate_data.index).ffill()
gbp_vol_data = gbp_vol_data[gbp_vol_data.index >= '2011-01-01'] 
gbp_vol_data = pd.DataFrame({'{0}_vol'.format(c): convert_atmf_vol_to_yield(c.split('_')[0], gbp_vol_data[c]) for c in gbp_vol_data.columns})




gbp_vol_1y_exp_data = gbp_vol_data[[c for c in gbp_vol_data.columns if '1Y_' in c]]
gbp_vol_10y_tails_data = gbp_vol_data[[c for c in gbp_vol_data.columns if '_10Y' in c]]

gbp_rate_1y_fwd_data = gbp_rate_data[[c for c in gbp_rate_data.columns if '1Y_' in c]]
gbp_rate_10y_tails_data = gbp_rate_data[[c for c in gbp_rate_data.columns if '_10Y' in c]]




data_rtn = (data/data.shift(1) - 1).dropna()


data_rtn_pca = pca.PCA(data_rtn, rates_pca = False, momentum_size = None)
data_rtn_pca.plot_loadings(5)


data_pca = pca.PCA(data, rates_pca = True, momentum_size = None)
data_pca.plot_loadings(5)


 mrs['CAD_5Y_5Y_USD_0Y_30Y_PCW'][0].create_mr_info_fig((18, 7), pca_obj = mrs['CAD_5Y_5Y_USD_0Y_30Y_PCW'][1])
 
 
 
 CAD_2y1ys8s = data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna().apply(lambda x: - x['CAD_2Y_1Y'] + x['CAD_0Y_8Y'], axis=1)
 CAD_2y1ys8s_rtns = (CAD_2y1ys8s - CAD_2y1ys8s.shift(1)).shift(-1)
 
 cad_curve = pd.DataFrame({'VOL_2y1ys8s': data_swap[['CAD_2Y_1Y', 'CAD_0Y_8Y']].dropna().diff().pow(2).apply(lambda x: - x['CAD_2Y_1Y'] + x['CAD_0Y_8Y'], axis=1),
                            'CAD_2y1ys8s_rtns': CAD_2y1ys8s_rtns                  
                            }).dropna()
 
 cad_curve['VOL_2y1ys8s'].plot.hist(bin=50)


arp4_data = data[['ARP_4']] 
arp4_data['return'] = (arp4_data.ARP_4/arp4_data.ARP_4.shift(1) - 1)*100
arp4_data = arp4_data.dropna()


"""
arp4_data['return'].quantile(0.9999)
arp4_data['return'].quantile(0.0001)
arp4_data['return'][(arp4_data['return'] < arp4_data['return'].quantile(0.9999))
                     & (arp4_data['return'] > arp4_data['return'].quantile(0.0001))].plot.hist(bin=100)
"""


arp4_data = arp4_data[(arp4_data['return'] < arp4_data['return'].quantile(0.9999))
                     & (arp4_data['return'] > arp4_data['return'].quantile(0.0001))]


from statistics.mr import sbins
arp4_sbin = sbins.sbins(arp4_data, 'ARP_4', 20)
arp4_sbin.plot_bin_mean_variance_for_col('return')

 





'''

part 1

'''

if(1>2):

    

    # rates and vol landscape

    
    gbp_vol_1y_exp_pca = pca.PCA(gbp_vol_1y_exp_data, rates_pca = True, momentum_size = None)
    gbp_vol_1y_exp_pca.plot_loadings()
    
    gbp_rate_1y_fwd_pca = pca.PCA(gbp_rate_1y_fwd_data, rates_pca = True, momentum_size = None)
    gbp_rate_1y_fwd_pca.plot_loadings()
    
    
    
    '''
    gbp_6m_10s30s_vol_crv = pd.DataFrame({'gbp_6m_10s30s_vol_3m_ago': (gbp_vol_data['6M_30Y_vol'] - gbp_vol_data['6M_10Y_vol']).shift(66),
                                          'gbp_3m_10s30s_vol': (gbp_vol_data['3M_30Y_vol'] - gbp_vol_data['3M_10Y_vol'])})
    (gbp_6m_10s30s_vol_crv.gbp_3m_10s30s_vol - gbp_6m_10s30s_vol_crv.gbp_6m_10s30s_vol_3m_ago).plot()
    '''
    
    
    
    
    #scatter level vs curve
    
    gpb_rate_curve_level = pd.DataFrame({'curve': gbp_rate_data['1Y_30Y_rate'] - gbp_rate_data['3M_10Y_rate'],
                                    'avg_level': (gbp_rate_data['1Y_30Y_rate'] + gbp_rate_data['3M_10Y_rate'])/2})
    
    gpb_vol_curve_level = pd.DataFrame({'curve': gbp_vol_data['1Y_30Y_vol'] - gbp_vol_data['3M_10Y_vol'],
                                    'avg_level': (gbp_vol_data['1Y_30Y_vol'] + gbp_vol_data['3M_10Y_vol'])/2})


    plot_scatter(gpb_rate_curve_level, 'Rates 10s vs 30s')

    plot_scatter(gpb_vol_curve_level, 'Vol 10s vs 30s')
    


    # vol mean reversion
        
    
    gpb_vol_level = pd.DataFrame({'GBP_10Y_IVol':gbp_vol_data['1Y_10Y_vol'],
                                  'Return': gbp_vol_data['1Y_10Y_vol'] - gbp_vol_data['1Y_10Y_vol'].shift(22)})  
    gpb_vol_level = gpb_vol_level.dropna()
    
    gpb_vol_level = gpb_vol_level[gpb_vol_level.index < '2022-08-01']
    
    sbins.sbins(gpb_vol_level, 'GBP_10Y_IVol', 20).plot_bin_mean_variance_for_col('Return')


    
    
    '''
        
    #mixed
    pca.PCA(pd.concat([gbp_vol_1y_exp_data, gbp_rate_1y_fwd_data], axis=1), rates_pca = True, momentum_size = None).plot_loadings()
    
    pca.PCA(gbp_vol_10y_tails_data, rates_pca = True, momentum_size = None).plot_loadings()
        
    pca.PCA(gbp_vol_1y_exp_data, rates_pca = True, momentum_size = None).plot_loadings()
    
    pca.PCA(gbp_vol_10y_tails_data, rates_pca = True, momentum_size = None).plot_loadings()
    
    '''
    

'''

part 2

'''


if(1>2):
    
    ''' Data '''
    
    data_arps = pd.read_excel(r'C:\Dev\ManCave\QuantModels\#applications\AUSUP\QDS_Case_Study_Strategy_Data.xlsm', 'QDS_Case_Study_Strategy_Data')
    data_arps = data_arps[['Date','Equities','Bonds','ARP_1','ARP_2','ARP_3', 'ARP_4','prt6040_mth_rebal', 'prt6040_no_rebal']]
    data_arps['Date'] = pd.to_datetime(data_arps['Date'])
    data_arps = data_arps.set_index('Date').ffill()
    data_arps_rtn = (data_arps/data_arps.shift(1)-1).dropna() 



    '''smell tests'''


    #corr
    data_arps_rtn_corr = data_arps_rtn.corr().round(2)

    
    #simple risk metrics
    data_arps_rtn_riskmetrics = pd.DataFrame({c:simple_risk(data_arps_rtn[c], period_label='full_history') for c in data_arps_rtn.columns}).transpose() 
    


    # scatter
    [plot_scatter(data_arps_rtn[['prt6040_mth_rebal', c]], align_scale=True) for c in data_arps_rtn.columns if 'ARP_' in c or c in ['Equities', 'Bonds']]
    

    # 60/40 factor exposure
    prt6040_mth_rebal_ARP_exposure = pd.DataFrame({c:reg95(data_arps_rtn[c], data_arps_rtn.prt6040_mth_rebal) for c in data_arps_rtn.columns if 'ARP_' in c or c in ['Equities', 'Bonds']}).transpose()
    
    

    
    


    
    
    






