# -*- coding: utf-8 -*-
"""
Created on Sun Oct  2 08:16:07 2022

@author: zywcu
"""


import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
from cvxopt import matrix
import cvxopt as opt
from cvxopt import blas, solvers
from cvxopt import printing
printing.options['dformat'] = '%.8f'
printing.options['width'] = -1

import pandas as pd
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations
import pylab


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper

pd.set_option('display.expand_frame_repr', False)



    
def reg95(xs, ys):
    r = stats.linregress(xs, y=ys)
    a = r.intercept
    a_l, a_r = a + np.array([-1* abs(r.intercept_stderr), abs(r.intercept_stderr)])* 1.96
    b = r.slope
    b_l, b_r = b + np.array([-1* abs(r.stderr), abs(r.stderr)])* 1.96
    return pd.Series({'alpha':a, 'alpha_l':a_l, 'alpha_r':a_r, 'beta':b, 'beta_l':b_l, 'beta_r':b_r})
   



def simple_risk(return_data, period_label=None):
    return pd.Series({
        'period': 'N/A' if period_label is None else period_label,
        'annual_rtn%': return_data.mean()*252*100,
        'vol%': return_data.std() * np.sqrt(252)*100,
        'sr_nocash': return_data.mean()/return_data.std()*np.sqrt(252),
        'es95%':  return_data[return_data < return_data.quantile(0.05)].mean()*100,   
        'es91%':  return_data[return_data < return_data.quantile(0.01)].mean()*100,   
        })




def plot_scatter_ts(ax, xs, ys, colors, cmaps, ticklabelsize=8):
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 9)
        
    ax.set_ylabel(ys.name, fontsize=10)
    ax.set_xlabel(xs.name, fontsize=10)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    return mapable


def plot_ts_endpoints(ax, xs, ys, colors, sizes, labels, markers, legend_loc=None,ncol=1, fontsize=10, lw =3):
    ds = [ax.scatter(xs[i], ys[i], c=colors[i], s=sizes[i], label=labels[i], marker=markers[i], linewidth= lw) for i in np.arange(len(xs))]
    if legend_loc is not None:
        lg =ax.legend(ds, labels, scatterpoints=1, fancybox=True, framealpha=0, loc=legend_loc, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lg)
    return ds
    

def plot_scatter(source_data, title = None, align_scale=False):
    
    
    xs = source_data[source_data.columns[0]]
    ys = source_data[source_data.columns[1]]
    
    start_x = xs.loc[xs.first_valid_index()]
    start_y = ys.loc[ys.first_valid_index()]
    start_dt = xs.first_valid_index().strftime('%Y-%m-%d')
    
    last_x = xs.loc[xs.last_valid_index()]
    last_y = ys.loc[ys.last_valid_index()]
    last_dt = xs.last_valid_index().strftime('%Y-%m-%d')
    
    
    f = plt.figure(figsize = (8, 8))
    gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
    ax0 = plt.subplot(gs[0])
    
    colors0 = mpl.dates.date2num(source_data.index.to_pydatetime())
 
    cmap1 = plt.cm.get_cmap('Reds')
    
    
    #plot main
    main_pc0 = plot_scatter_ts(ax0, xs, ys, colors0, cmap1)
    
    
    #plot end point
    endpt_pc0 = plot_ts_endpoints(ax0, [start_x, last_x], [start_y, last_y], ['lightgrey','black'], [30,30], [start_dt, last_dt], ['v','^'], legend_loc=1)
    
    if(title is not None):
        f.suptitle(title, fontsize=12)
        
    
    if align_scale:
        
        lim = source_data.abs().max().max()
        ax0.set_xlim([-lim, lim])
        ax0.set_ylim([-lim, lim])

    plt.tight_layout()
    
    






def plot_stack_area(x, ys, ax, reset_color_cycle = False, series_names = None, legend_pos=None, legend_ncol=1, legend_font_size= 12, **kwargs):

        min_x = x.min()
        max_x = x.max()

        sp = ax.stackplot(x, ys,linewidth=0.0, baseline ='zero', **kwargs)

        ax.set_xlim([min_x, max_x])

        ax.xaxis.grid(True)
        ax.yaxis.grid(True)

        if reset_color_cycle:
            #ax.set_color_cycle(None)
            ax.set_prop_cycle(None)

        if series_names is not None:
            #ax.set_color_cycle(None)
            legend_proxy = [mpl.patches.Rectangle((0,0), 0,0, facecolor=pol.get_facecolor()[0]) for pol in sp]
            ax.legend(legend_proxy, series_names, loc=legend_pos, fancybox=True, framealpha=0,  ncol=legend_ncol, prop={'size':legend_font_size})




def plot_stacked_bar(ax, series, left =None, width=0, color=None, edgecolor="none", bottom=None, **kwargs):
    width = width
    left = np.arange(len(series)) + width/2.0 if left is None else left    
    s = ax.bar(left, series, width, color= color,edgecolor=edgecolor, bottom=bottom, **kwargs)
    return s

def plot_heatmap(tbl, ax):
    #mask = np.triu(np.ones_like(cov, dtype=np.bool))

    
    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(10, 240, as_cmap=True)
    
    # Draw the heatmap with the mask and correct aspect ratio
    #sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
    _ax = sns.heatmap(tbl, cmap=cmap, center=0,
                square=True, annot=True, linewidths=.5, cbar_kws={"shrink": .5},
                annot_kws={"size": 9}, ax=ax)
    _ax.set_yticklabels(ax.get_yticklabels(), rotation=90, horizontalalignment='right')
    
    return  _ax


def get_flattened_cov(df):
    covmatx = df.cov()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result

def get_mean(df):
    return df.mean()

    

def rolling_apply(df, window_size, func, min_window_size=None):
    if min_window_size is None:
        min_window_size = window_size
    result = {}
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here

        if len(sub_df) >= min_window_size:            
            idx = sub_df.index[-1]
            result[idx] = func(sub_df)
    return result

def partition_by_sign( s, pos=True):
    s = s.fillna(0)
    if pos:
        return s.apply(lambda v: v if v >= 0 else 0)
    else:
        return s.apply(lambda v: v if v <= 0 else 0)
    
    
def get_weighted_return(return_df, weight_mtx, name=None):
    wr = matrix(return_df.values) * weight_mtx
    return pd.Series(data=wr, index=return_df.index, name=name)
    
    
    
    






data_arps = pd.read_excel(r'C:\Dev\ManCave\QuantModels\#applications\AUSUP\QDS_Case_Study_Strategy_Data.xlsm', 'QDS_Case_Study_Strategy_Data')
data_arps = data_arps[['Date','Equities','Bonds','ARP_1','ARP_2','ARP_3', 'ARP_4','prt6040_mth_rebal', 'prt6040_no_rebal']]
data_arps['Date'] = pd.to_datetime(data_arps['Date'])
data_arps = data_arps.set_index('Date').ffill()
data_rtns_d = (data_arps/data_arps.shift(1)-1).dropna() 
assets = data_rtns_d_mvo.columns.tolist()





# PCA look on overall history

data_rtns_d_pca = pca.PCA(data_rtns_d.cumsum(), rates_pca = True, momentum_size = None)
data_rtns_d_pca.plot_loadings(n=10)




''' semll tests'''

# full history corr

data_rtns_d.corr().round(2)


'''
                       Equities  Bonds  ARP_1  ARP_2  ARP_3  ARP_4  prt6040_mth_rebal  prt6040_no_rebal
    Equities               1.00  -0.35   0.57   0.23   0.14   0.02               0.98              0.97
    Bonds                 -0.35   1.00  -0.18   0.24  -0.09  -0.02              -0.21             -0.22
    ARP_1                  0.57  -0.18   1.00   0.22   0.02   0.01               0.56              0.61
    ARP_2                  0.23   0.24   0.22   1.00  -0.15  -0.01               0.28              0.31
    ARP_3                  0.14  -0.09   0.02  -0.15   1.00  -0.01               0.12              0.08
    ARP_4                  0.02  -0.02   0.01  -0.01  -0.01   1.00               0.02              0.01
    prt6040_mth_rebal      0.98  -0.21   0.56   0.28   0.12   0.02               1.00              0.96
    prt6040_no_rebal       0.97  -0.22   0.61   0.31   0.08   0.01               0.96              1.00

'''


#simple risk metrics for the full history

pd.DataFrame({c:simple_risk(data_rtns_d[c], period_label='full_history') for c in data_rtns_d.columns}).transpose() 
    

'''

pd.DataFrame({c:simple_risk(data_rtns_d[c], period_label='full_history') for c in data_rtns_d.columns}).transpose()
Out[55]: 
                         period annual_rtn%       vol% sr_nocash     es95%     es91%
Equities           full_history   11.124749  20.394465  0.545479 -3.232056 -5.636002
Bonds              full_history    2.279842   4.546346  0.501467  -0.64508 -1.001506
ARP_1              full_history    2.877681  11.488661   0.25048 -1.692257 -4.008895
ARP_2              full_history    4.740947    4.73632  1.000977 -0.707895  -1.09845
ARP_3              full_history    2.663397   5.905362  0.451013 -0.808623 -1.684999
ARP_4              full_history    1.166611   6.972837  0.167308 -0.793481 -1.408839
prt6040_mth_rebal  full_history    6.114695  11.665193  0.524183 -1.835118 -3.233687
prt6040_no_rebal   full_history    7.784531   12.10383  0.643146 -1.908813 -3.303979

'''


# check 60/40's existing factor exposure. 

pd.DataFrame({c:reg95(data_rtns_d[c], data_rtns_d.prt6040_mth_rebal) for c in data_rtns_d.columns if 'ARP_' in c or c in ['Equities', 'Bonds']}).transpose()
    
'''
pd.DataFrame({c:reg95(data_rtns_d[c], data_rtns_d.prt6040_mth_rebal) for c in data_rtns_d.columns if 'ARP_' in c or c in ['Equities', 'Bonds']}).transpose()
Out[57]: 
             alpha   alpha_l   alpha_r      beta    beta_l    beta_r
Equities -0.000004 -0.000055  0.000048  0.558233  0.554237  0.562229
Bonds     0.000291  0.000060  0.000521 -0.532669 -0.613133 -0.452206
ARP_1     0.000177 -0.000017  0.000372  0.572182  0.545292  0.599072
ARP_2     0.000113 -0.000114  0.000339  0.691267  0.615484  0.767050
ARP_3     0.000217 -0.000017  0.000450  0.246291  0.183460  0.309123
ARP_4     0.000241  0.000006  0.000477  0.031663 -0.021959  0.085285
'''



# visual pair return check

[plot_scatter(data_rtns_d[['prt6040_mth_rebal', c]], align_scale=True) for c in data_rtns_d.columns if 'ARP_' in c or c in ['Equities', 'Bonds']]
    
    
# visual rolling vol check


data_rtns_d_1y_rolling_covs = pd.DataFrame(rolling_apply(data_rtns_d, 252, get_flattened_cov)).transpose()
data_rtns_d_1y_rolling_vol = data_rtns_d_1y_rolling_covs[[ a + '_' + b for a,b in set([comb for comb in combinations(assets + assets, 2)]) if a == b]] * 252
data_rtns_d_1y_rolling_vol = data_rtns_d_1y_rolling_vol.applymap(np.sqrt)

data_rtns_d_1y_rolling_vol.plot(grid=True)                    






''' 
===================================================

mvo tests 1

===================================================
''' 


risk_scaler = pd.Series({'prt6040_mth_rebal':1, 'ARP_1':1, 'ARP_2':1, 'ARP_3':1, 'ARP_4':1})
data_rtns_d_mvo = data_rtns_d[['prt6040_mth_rebal', 'ARP_1', 'ARP_2', 'ARP_3', 'ARP_4']]
data_rtns_d_mvo = data_rtns_d_mvo / risk_scaler
assets = data_rtns_d_mvo.columns.tolist()




#vol and return matrix object on overall history  
data_rtns_d_mean_mtx = matrix(data_rtns_d_mvo.mean())
data_rtns_d_var_mtx = matrix(data_rtns_d_mvo.cov().to_numpy())



#inequality


#g1 is numpy array. covexp wont flip the numpy array when creating matrix but will flip the same dimension list of list 

# weights > -0.3
g1 = np.array([
        [-1.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, -1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, -1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, -1.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, -1.0]
    ])
h1 = [ 0.3,  0.3,  0.3,  0.3,  0.3]

# max weights 
g2 = np.array([
        [1.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 1.0]
    ])
h2 = [ 0.3,  0.3,  0.3,  0.3, 0.3]



gs = np.concatenate((g1, g2),axis=0)
hs = h1 + h2

G = matrix(gs)

h = matrix(hs)

#equality

A = matrix(1.0, (1,5))
b = matrix(1.0)

#build portfolios

solvers.options['show_progress'] = False
_lambdas = [ 10**(5.0*t/100-1.0) for t in range(1000)] # looking for best return per unit increment of vol here
weights = [ solvers.qp((_lambda)*data_rtns_d_var_mtx, -data_rtns_d_mean_mtx, G, h, A, b)['x'] for _lambda in _lambdas]

expected_annual_returns = [ blas.dot(data_rtns_d_mean_mtx,x) * 250 for x in weights]
expected_annual_risks = [ np.sqrt(blas.dot(x, data_rtns_d_var_mtx*x)) * np.sqrt(250) for x in weights]
expected_annual_sharpes = [ rt/rs for rt, rs in zip(expected_annual_returns, expected_annual_risks)] 

unconstrainted_backtest_d_rtns = [get_weighted_return(data_rtns_d_mvo, x, name=s) for x, s in zip(weights, expected_annual_sharpes)]





# Plot trade-off curve and optimal allocations.

fig = plt.figure(figsize=(10,10))
ax0 = fig.add_subplot(221)
ax00 = fig.add_subplot(222)
ax1 = fig.add_subplot(223)
ax2 = fig.add_subplot(224)


data_rtns_d_mvo.cumsum().plot(ax=ax0)

plot_heatmap(data_rtns_d_mvo.cov() * np.sqrt(250), ax00)

ax1.plot(expected_annual_risks, expected_annual_returns)
ax1.set_xlabel('standard deviation')
ax1.set_ylabel('expected return')



c1 = pd.Series([ x[0] for x in weights ], name=assets[0])
c2 = pd.Series([ x[1] for x in weights ], name=assets[1])
c3 = pd.Series([ x[2] for x in weights ], name=assets[2])
c4 = pd.Series([ x[3] for x in weights ], name=assets[3])
c5 = pd.Series([ x[4] for x in weights ], name=assets[4])

cc = plt.rcParams['axes.prop_cycle'].by_key()['color']

s1 = plot_stacked_bar(ax2, partition_by_sign(c1, pos=True), left=expected_annual_risks, width = 0.001, color=cc[0], edgecolor ="none",  align="center")
s2 = plot_stacked_bar(ax2, partition_by_sign(c2, pos=True), left=expected_annual_risks, width = 0.001, color=cc[1], edgecolor ="none",  align="center", bottom=partition_by_sign(c1, pos=True).values)
s3 = plot_stacked_bar(ax2, partition_by_sign(c3, pos=True), left=expected_annual_risks, width = 0.001, color=cc[2], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values)
s4 = plot_stacked_bar(ax2, partition_by_sign(c4, pos=True), left=expected_annual_risks, width = 0.001, color=cc[3], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values + partition_by_sign(c3, pos=True).values)
s5 = plot_stacked_bar(ax2, partition_by_sign(c5, pos=True), left=expected_annual_risks, width = 0.001, color=cc[4], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=True).values + partition_by_sign(c2, pos=True).values + partition_by_sign(c3, pos=True).values + partition_by_sign(c4, pos=True).values)

s1 = plot_stacked_bar(ax2, partition_by_sign(c1, pos=False), left=expected_annual_risks, width = 0.001, color=cc[0], edgecolor ="none",  align="center")
s2 = plot_stacked_bar(ax2, partition_by_sign(c2, pos=False), left=expected_annual_risks, width = 0.001, color=cc[1], edgecolor ="none",  align="center", bottom=partition_by_sign(c1, pos=False).values)
s3 = plot_stacked_bar(ax2, partition_by_sign(c3, pos=False), left=expected_annual_risks, width = 0.001, color=cc[2], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values)
s4 = plot_stacked_bar(ax2, partition_by_sign(c4, pos=False), left=expected_annual_risks, width = 0.001, color=cc[3], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values + partition_by_sign(c3, pos=False).values)
s5 = plot_stacked_bar(ax2, partition_by_sign(c5, pos=False), left=expected_annual_risks, width = 0.001, color=cc[4], edgecolor ="none",  align="center", 
                      bottom=partition_by_sign(c1, pos=False).values + partition_by_sign(c2, pos=False).values + partition_by_sign(c3, pos=False).values + partition_by_sign(c4, pos=False).values)


ax2.set_xlabel('standard deviation')
ax2.set_ylabel('allocation')
ax2.legend([s1, s2, s3, s4, s5], assets, fancybox=True, framealpha=0, loc=9, ncol=3, prop={'size':10}, bbox_to_anchor=(0.5, 1.12))

plt.tight_layout()
    





''' 
===================================================

mvo tests 2

===================================================
''' 


solvers.options['show_progress'] = False





data_rtns_d_mvo = data_rtns_d[['prt6040_mth_rebal', 'ARP_1', 'ARP_2', 'ARP_3', 'ARP_4']]
assets = data_rtns_d_mvo.columns.tolist()


#rolling 1y covs and mean
data_rtns_d_1y_rolling_covs = rolling_apply(data_rtns_d_mvo, 250, lambda df: df.cov())
data_rtns_d_1y_rolling_mean = rolling_apply(data_rtns_d_mvo, 250, lambda df: df.mean())
eom_dates = data_rtns_d_mvo.resample('M').last().index



#re optimise for the month end


# weights > -0.3
g1 = np.array([
        [-1.0, 0.0, 0.0, 0.0, 0.0],
        [0.0,-1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0,-1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, -1.0,0.0],
        [0.0, 0.0, 0.0, 0.0,-1.0]
    ])
h1 = [-0.5,  0.3,  0.3,  0.3,  0.3]

# max weights 
g2 = np.array([
        [1.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 1.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 1.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 0.0, 1.0]
    ])
h2 = [ 1,  0.1,  0.3,  0.3,  0.3]


gs = np.concatenate((g1, g2),axis=0)
hs = h1 + h2

G = matrix(gs)

h = matrix(hs)


#equality

A = matrix(1.0, (1,5))
b = matrix(1.0)



#build portfolios
_lambdas = [ 10**(5.0*t/1000-1.0) for t in range(1000)]




optimizations = {}


# cpu heavy optimizations

for dt in eom_dates:
    if dt in data_rtns_d_1y_rolling_mean:
        data_rtns_d_var_mtx = matrix(data_rtns_d_1y_rolling_covs[dt].to_numpy())
        data_rtns_d_mean_mtx = matrix(data_rtns_d_1y_rolling_mean[dt])
        
        weights = [ solvers.qp((_lambda)*data_rtns_d_var_mtx, -data_rtns_d_mean_mtx, G, h, A, b)['x'] for _lambda in _lambdas]
        expected_annual_returns = [ blas.dot(data_rtns_d_mean_mtx,x) * 250 for x in weights]
        expected_annual_risks = [ np.sqrt(blas.dot(x, data_rtns_d_var_mtx*x)) * np.sqrt(250) for x in weights]
        expected_annual_sharpes = [ rt/rs for rt, rs in zip(expected_annual_returns, expected_annual_risks)] 
        optimizations[dt] = {'weights':weights, 'expected_annual_returns':expected_annual_returns, 'expected_annual_risks':expected_annual_risks, 'expected_annual_sharpes':expected_annual_sharpes}
        



# find threshold portfolio

return_threshhold = 0.04        
selected_optimization = {}
for dt in optimizations.keys():
        weights = optimizations[dt]['weights']
        expected_annual_returns = optimizations[dt]['expected_annual_returns']
        expected_annual_risks = optimizations[dt]['expected_annual_risks']
        expected_annual_sharpes = optimizations[dt]['expected_annual_sharpes'] 
     
        selected_returns = [x for x in expected_annual_returns if x >= return_threshhold]
        selected_return = max(expected_annual_returns) if len(selected_returns) == 0 else min(selected_returns)
        seleted_index = expected_annual_returns.index(selected_return)
        
        #print(dt)
        #print(seleted_index)
        #print(len(expected_annual_returns))
        #print(selected_return)
        
        selected_optimization[dt] = {'weights':weights[seleted_index], 
                                     'expected_annual_returns':expected_annual_returns[seleted_index], 
                                     'expected_annual_risks':expected_annual_risks[seleted_index], 
                                     'expected_annual_sharpes':expected_annual_sharpes[seleted_index]}
        

# set up the data for chart

threshhold_returns ={}
threshhold_risks ={}
threshhold = {}

p6040_wt = {}
arp1_wt = {}
arp2_wt = {}
arp3_wt = {}
arp4_wt = {}



 

for dt in selected_optimization.keys():
    threshhold_returns[dt] = selected_optimization[dt]['expected_annual_returns']
    threshhold_risks[dt] = selected_optimization[dt]['expected_annual_risks']
    threshhold[dt] = return_threshhold 

    
    p6040_wt[dt] = selected_optimization[dt]['weights'][0]
    arp1_wt[dt] = selected_optimization[dt]['weights'][1]
    arp2_wt[dt] = selected_optimization[dt]['weights'][2]
    arp3_wt[dt] = selected_optimization[dt]['weights'][3]
    arp4_wt[dt] = selected_optimization[dt]['weights'][4]
    
    
    
threshhold_risk_rewards = pd.DataFrame({'return':pd.Series(threshhold_returns, name='return'), 
                                        'risk': pd.Series(threshhold_risks, name='rik'), 
                                        'return_threshhold':pd.Series(threshhold, name='rik') })



p6040_wt = pd.Series(p6040_wt, name='prt6040_mth_rebal')
arp1_wt = pd.Series(arp1_wt, name='ARP_1')
arp2_wt = pd.Series(arp2_wt, name='ARP_2')
arp3_wt = pd.Series(arp3_wt, name='ARP_3')
arp4_wt = pd.Series(arp4_wt, name='ARP_4')





portfolio_wt = pd.DataFrame([p6040_wt, arp1_wt, arp2_wt, arp3_wt, arp4_wt]).transpose().reindex(data_rtns_d_mvo.index, axis='index').ffill().dropna()

porfolio_d_rtn = (portfolio_wt * data_rtns_d_mvo).dropna() 

porfolio_backtest = porfolio_d_rtn.sum(axis=1)


porfolio_backtest_2001 = porfolio_backtest[(porfolio_backtest.index > '2000-12-31') & (porfolio_backtest.index <= '2001-12-31')].cumsum()
porfolio_backtest_2002 = porfolio_backtest[(porfolio_backtest.index > '2001-12-31') & (porfolio_backtest.index <= '2002-12-31')].cumsum()
porfolio_backtest_2003 = porfolio_backtest[(porfolio_backtest.index > '2002-12-31') & (porfolio_backtest.index <= '2003-12-31')].cumsum()
porfolio_backtest_2004 = porfolio_backtest[(porfolio_backtest.index > '2003-12-31') & (porfolio_backtest.index <= '2004-12-31')].cumsum()
porfolio_backtest_2005 = porfolio_backtest[(porfolio_backtest.index > '2004-12-31') & (porfolio_backtest.index <= '2005-12-31')].cumsum()
porfolio_backtest_2006 = porfolio_backtest[(porfolio_backtest.index > '2005-12-31') & (porfolio_backtest.index <= '2006-12-31')].cumsum()
porfolio_backtest_2007 = porfolio_backtest[(porfolio_backtest.index > '2006-12-31') & (porfolio_backtest.index <= '2007-12-31')].cumsum()
porfolio_backtest_2008 = porfolio_backtest[(porfolio_backtest.index > '2007-12-31') & (porfolio_backtest.index <= '2008-12-31')].cumsum()
porfolio_backtest_2009 = porfolio_backtest[(porfolio_backtest.index > '2008-12-31') & (porfolio_backtest.index <= '2009-12-31')].cumsum()
porfolio_backtest_2010 = porfolio_backtest[(porfolio_backtest.index > '2009-12-31') & (porfolio_backtest.index <= '2010-12-31')].cumsum()
porfolio_backtest_2011 = porfolio_backtest[(porfolio_backtest.index > '2010-12-31') & (porfolio_backtest.index <= '2011-12-31')].cumsum()
porfolio_backtest_2012 = porfolio_backtest[(porfolio_backtest.index > '2011-12-31') & (porfolio_backtest.index <= '2012-12-31')].cumsum()
porfolio_backtest_2013 = porfolio_backtest[(porfolio_backtest.index > '2012-12-31') & (porfolio_backtest.index <= '2013-12-31')].cumsum()
porfolio_backtest_2014 = porfolio_backtest[(porfolio_backtest.index > '2013-12-31') & (porfolio_backtest.index <= '2014-12-31')].cumsum()
porfolio_backtest_2015 = porfolio_backtest[(porfolio_backtest.index > '2014-12-31') & (porfolio_backtest.index <= '2015-12-31')].cumsum()
porfolio_backtest_2016 = porfolio_backtest[(porfolio_backtest.index > '2015-12-31') & (porfolio_backtest.index <= '2016-12-31')].cumsum()
porfolio_backtest_2017 = porfolio_backtest[(porfolio_backtest.index > '2016-12-31') & (porfolio_backtest.index <= '2017-12-31')].cumsum()
porfolio_backtest_2018 = porfolio_backtest[(porfolio_backtest.index > '2017-12-31') & (porfolio_backtest.index <= '2018-12-31')].cumsum()
porfolio_backtest_2019 = porfolio_backtest[(porfolio_backtest.index > '2018-12-31') & (porfolio_backtest.index <= '2019-12-31')].cumsum()
porfolio_backtest_2020 = porfolio_backtest[(porfolio_backtest.index > '2019-12-31') & (porfolio_backtest.index <= '2020-12-31')].cumsum()
porfolio_backtest_2021 = porfolio_backtest[(porfolio_backtest.index > '2020-12-31') & (porfolio_backtest.index <= '2021-12-31')].cumsum()
porfolio_backtest_2022 = porfolio_backtest[(porfolio_backtest.index > '2021-12-31') & (porfolio_backtest.index <= '2022-12-31')].cumsum()


porfolio_backtest_anchor = pd.Series({'2002-01-01':0,'2003-01-01':0, '2004-01-01':0, '2005-01-01':0, '2006-01-01':0, '2007-01-01':0,
                                      '2008-01-01':0, '2009-01-01':0, '2010-01-01':0, '2011-01-01':0, '2012-01-01':0,
                                      '2013-01-01':0, '2014-01-01':0, '2015-01-01':0, '2016-01-01':0, '2017-01-01':0,
                                      '2018-01-01':0, '2019-01-01':0, '2020-01-01':0, '2021-01-01':0, '2022-01-01':0})

porfolio_backtest_anchor.index = pd.to_datetime(porfolio_backtest_anchor.index)


porfolio_rolling_cumulative_performance = pd.concat([ 
    porfolio_backtest_2001,
    porfolio_backtest_2002,
    porfolio_backtest_2003,
    porfolio_backtest_2004,
    porfolio_backtest_2005,
    porfolio_backtest_2006,
    porfolio_backtest_2007,
    porfolio_backtest_2008,
    porfolio_backtest_2009,
    porfolio_backtest_2010,
    porfolio_backtest_2011,
    porfolio_backtest_2012,
    porfolio_backtest_2013,
    porfolio_backtest_2014,
    porfolio_backtest_2015,
    porfolio_backtest_2016,
    porfolio_backtest_2017,
    porfolio_backtest_2018,
    porfolio_backtest_2019,
    porfolio_backtest_2020,
    porfolio_backtest_2021,
    porfolio_backtest_2022
    ])

    
#plot

fig = plt.figure(figsize=(15,8))

ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212)




plot_stack_area(p6040_wt.index,[partition_by_sign(p6040_wt,True), 
                                 partition_by_sign(arp1_wt,True), 
                                 partition_by_sign(arp2_wt,True), 
                                 partition_by_sign(arp3_wt,True), 
                                 partition_by_sign(arp4_wt,True)
                                 ], 
                                 ax1, 
                reset_color_cycle= True, series_names=data_rtns_d_mvo.columns.tolist(), colors=plt.rcParams['axes.prop_cycle'].by_key()['color'][:7],
                            legend_font_size= 10, legend_pos=9, legend_ncol=7)


plot_stack_area(p6040_wt.index,[partition_by_sign(p6040_wt,False), 
                                 partition_by_sign(arp1_wt,False), 
                                 partition_by_sign(arp2_wt,False), 
                                 partition_by_sign(arp3_wt,False), 
                                 partition_by_sign(arp4_wt,False)
                                 ], 
                                ax1, 
                reset_color_cycle= True, series_names=data_rtns_d_mvo.columns.tolist(), colors=plt.rcParams['axes.prop_cycle'].by_key()['color'][:7],
                            legend_font_size= 10, legend_pos=9, legend_ncol=7)



ax2 = porfolio_rolling_cumulative_performance.plot.area(ax=ax2, title='back test', stacked=False, grid=True)
ax2.xaxis.grid(True, which='minor', linestyle='-', linewidth=0.25)


