# -*- coding: utf-8 -*-
"""
Created on Sat Oct  1 17:16:07 2022

@author: zywcu
"""

from Tools import *
import pandas as pd
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mpd
from matplotlib.lines import Line2D
from matplotlib.ticker import MultipleLocator, PercentFormatter
import matplotlib.dates as mdates
from datetime import datetime
import os
import numpy as np
import random
import cvxopt as opt
from cvxopt import blas, solvers




def rand_weights(n):
    k = np.random.rand(n)
#     k = np.random.uniform(-1, 1, size=n)
#     if sum(k) == 0:
#         return np.random.uniform(-1, 1, size=n)
#     else:
#         return k / sum(k)
    return k / sum(k)


def random_portfolio(expected_return, cov, num_assets, sigma_treashold = 100):
                
    p = np.asmatrix(expected_return)
    w = np.asmatrix(rand_weights(num_assets))
    C = np.asmatrix(cov)
    mu = w * p.T    
    
    sigma = np.sqrt(w * C * w.T)
    
    if sigma > sigma_treashold:
         return random_portfolio(expected_return, cov, num_assets)

    return mu, sigma, w #,) + tuple(w)


def generate_random_portfolios(portfolios, expected_return, cov, num_assets, asset_names, sigma_treashold = 100, dayConversionFactor = 1):
    def derive_colnames(i):
        if i == 0: 
            return "MEAN"
        elif i == 1:
            return "STD"
        else:
            return "WEIGHT_" + asset_names[i - 2]
    
    result = [random_portfolio(expected_return, cov, num_assets) for _ in np.arange(portfolios)]
    means, stds  = np.column_stack([(r[0], r[1]) for r in result])
    ws  = np.row_stack([(r[2]) for r in result])
    output_df = pd.DataFrame(np.concatenate((means, stds, ws), axis = 1))
    output_df.columns = [derive_colnames(i) for i in np.arange(len(output_df.columns))]
    output_df["STD"] = output_df["STD"] * np.sqrt(dayConversionFactor)
    output_df["MEAN"] = output_df["MEAN"] * dayConversionFactor
    return output_df

    
def get_frontier_weights(portfolio_df, func, rounding = 4, dayConversionFactor = 1):
    port_df = portfolio_df.copy()
    port_df["STD"] = port_df["STD"] * np.sqrt(dayConversionFactor)
    port_df["MEAN"] = port_df["MEAN"] * dayConversionFactor
    STD_filters = [round(item[0], rounding) for item in port_df.groupby(port_df["STD"].round(rounding))["MEAN"].agg(func).iteritems()]
    MEAN_filters = [item[1] for item in port_df.groupby(port_df["STD"].round(rounding))["MEAN"].agg(func).iteritems()]
    filter_df = pd.DataFrame({"STD_filters" : STD_filters, "MEAN_filters":MEAN_filters })
    port_df["STD_filters"] = port_df["STD"].round(rounding)
    port_df["MEAN_filters"] = port_df["MEAN"]
    weights_df = pd.merge(port_df,filter_df, on=["STD_filters","MEAN_filters"]).sort_values(["STD_filters"], ascending=[True])
    return weights_df

  
def solve_efficient_frontier(expected_return, cov, return_increment, dayConversionFactor = 1):    

    asset_names = return_data.columns.tolist()        
    
    def derive_colnames(i):
        if i == 0: 
            return "MEAN"
        elif i == 1:
            return "STD"
        else:
            return "WEIGHT_" + asset_names[i - 2]        
    
    n = len(return_data.columns)
    
    exp_rets = expected_return        
    
    mus = np.arange(np.min(exp_rets), np.max(exp_rets), return_increment)
    
    S = opt.matrix(cov)
    
    
    P = opt.matrix(return_data.cov().values)
    q = opt.matrix(0.0, (n, 1))
    
    G = opt.matrix(np.vstack((-exp_rets,-np.identity(n))))
    
    # because h will be generated on the flight, need a function here
    def get_h(target_ret):
        #print target_ret
        return opt.matrix(np.vstack(
            (-target_ret,  # minimal return target
             +np.zeros((n, 1)) # no short selling
             )
            )) 
    
     # Create equal constraint matrices
    A = opt.matrix(1.0, (1, n))    #all weights add up to 1
    b = opt.matrix(1.0)
    
    solvers.options['show_progress'] = False
    # Calculate efficient frontier weights using quadratic programming
    weights = [solvers.qp(P, q, G, get_h(mu), A, b)['x']  for mu in mus]
    
    # CALCULATE RISKS AND RETURNS FOR FRONTIER
    #returns = [blas.dot(pbar, x) for x in weights]
    returns = [blas.dot(opt.matrix(expected_return), x) for x in weights]    
    risks = [np.sqrt(blas.dot(x, P*x)) for x in weights]
    weights = [np.concatenate(np.array(x))  for x in weights ]
    
    output_df = pd.DataFrame(np.column_stack((returns, risks, weights)))
    output_df.columns = [derive_colnames(i) for i in np.arange(len(output_df.columns))]
    
    output_df["STD"] = output_df["STD"] * np.sqrt(dayConversionFactor)
    output_df["MEAN"] = output_df["MEAN"] * dayConversionFactor
        
    #print returns
    #print risks
    #return returns, risks, weights
    return output_df


def create_frontier_weights(return_data, simulations, return_increment = 0.0000001, volFilterRounding = 4, returnPeriodConversionFactor=250):
    asset_names = return_data.columns.tolist()
    cov = return_data.cov().values
    expected_return = np.asmatrix(np.mean(return_data, axis=0))    
    return_std = np.std(return_data, axis=0) 
    num_assets = return_data.shape[1]  
    
    #anything is that greater than 100 stdev is too extreme to chart
    simulated_portfolio_df = generate_random_portfolios(simulations, expected_return, cov, num_assets, asset_names, sigma_treashold = 100, dayConversionFactor=returnPeriodConversionFactor)

    #solved_frontier_df = solve_efficient_frontier(return_data)
    solved_frontier_df = solve_efficient_frontier(expected_return, cov, return_increment, dayConversionFactor=returnPeriodConversionFactor)
    
    frontier_by_rounded_weights = get_frontier_weights(solved_frontier_df, np.max, rounding= volFilterRounding, dayConversionFactor=1) # use 1 to make the chart look prettier
    
    return solved_frontier_df, frontier_by_rounded_weights, expected_return * returnPeriodConversionFactor, \
        return_std* np.sqrt(returnPeriodConversionFactor), cov, simulated_portfolio_df,asset_names


def get_asset_weights_dict_for_selected_vol(frontier_weights_df, vol):
    if(frontier_weights_df.STD_filters.min() > vol): raise TypeError("the vol input is too small for the period. It is not attainable on the efficient frontier")
    wstr = "WEIGHT_" 
    filtered_weight = frontier_weights_df[frontier_weights_df.STD_filters == vol] if vol <= frontier_weights_df.STD_filters.max() else frontier_weights_df[frontier_weights_df.STD_filters == frontier_weights_df.STD_filters.max()]
    result = {name.replace(wstr,"") : value.values[0] for name, value  in filtered_weight.iteritems() if wstr in name}
    return result


def calculate_positions_from_daily_return(assets_daily_returns, asset_starting_positions):
    if not isinstance(assets_daily_returns, pd.DataFrame): raise TypeError("assets_daily_returns must be a date x asset names dataframe")
    if not isinstance(asset_starting_positions, dict): raise TypeError("asset_starting_positions must be a list")
    if len(assets_daily_returns.columns) != len(asset_starting_positions): raise TypeError("assets don't match between asset_starting_positions and assets_daily_returns")        
    result = {col + "_positions" : (assets_daily_returns[col] + 1).cumprod() * asset_starting_positions[col] for col in assets_daily_returns.columns.tolist()}            
    result = pd.DataFrame(result)        
    result["Portfolio_positions"]  = result.apply(lambda r: np.sum(r), axis=1)
    return result


def rebalance_portfolio(assets_daily_returns, rebalance_schedules):
    if not isinstance(rebalance_schedules, list): raise TypeError("rebalance_schedule must be a list")
    
    # a tuple of postion value dataframe, rebalance date, next rebalance date if available
    result = []
    default_next_rebalance_date = assets_daily_returns.tail(1).index.strftime('%Y-%m-%d')[0]
    
    for schedule in rebalance_schedules:
        rebalance_date = schedule [0]
        reblance_weights = schedule [1] # a dictionary of weights
        print(rebalance_date)
        asset_daily_return_subset = assets_daily_returns.ix[rebalance_date:,]       
        new_portfolio_asset_values_after_rebalance = None
        
        if len(result) > 0:
            prev_rebalance_detail = result[len(result) -1]
            #update the end of the real next rebalance date
            result[len(result) -1] = (prev_rebalance_detail[0], prev_rebalance_detail[1], rebalance_date)
            #add new rebalance period
            portfolio_value_before_reblance = prev_rebalance_detail[0].ix[rebalance_date]
            print("Subsequent rebalance========>")
            print("Asset value before rebalancing:")
            print(portfolio_value_before_reblance)
            
            new_portfolio_asset_values_after_rebalance = { asset: weight * portfolio_value_before_reblance["Portfolio_positions"] for asset, weight in reblance_weights.iteritems()}
            
            print("Asset value after rebalancing:")
            print(new_portfolio_asset_values_after_rebalance)
            print("Portfolio_positions: " + str(sum(new_portfolio_asset_values_after_rebalance.values())))
            print("Asset weights after rebalancing:")
            print({ asset: weight for asset, weight in reblance_weights.iteritems()})
            
        else:
            print("Initial rebalance========>")
            new_portfolio_asset_values_after_rebalance = { asset: weight * 1 for asset, weight in reblance_weights.iteritems()}
            print("Asset value after rebalancing:")
            print(reblance_weights)
            print("Portfolio_positions: " + str(sum(reblance_weights.values())))

        rebalanced_portfolio_position_valuations = calculate_positions_from_daily_return(asset_daily_return_subset, new_portfolio_asset_values_after_rebalance)
        result.append((rebalanced_portfolio_position_valuations, rebalance_date, default_next_rebalance_date))    
    
    return result
    

def plot_stacked_bar(ax, series, left =None, width=0, color=None, edgecolor="none", bottom=None, **kwargs):
    width = width
    left = np.arange(len(series)) + width/2.0 if left is None else left    
    s = ax.bar(left, series, width, color= color,edgecolor=edgecolor, bottom=bottom, **kwargs)
    return s


def calc_return(dfin, period=1):
    return (dfin/dfin.shift(period) - 1)



def chart_mvo_for_regime(simulated_portfolio_df, expected_return, return_std, asset_names, solved_frontier_df, solved_frontier_df_weights, title ):
    
#     simulated_portfolio_df = US_taper_tantrum_simulated_portfolio_df
#     expected_return = US_taper_tantrum_expected_return
#     return_std = US_taper_tantrum_return_std
#     asset_names = asset_names
#     solved_frontier_df = US_taper_tantrum_solved_frontier_df
#     solved_frontier_df_weights = US_taper_tantrum_solved_frontier_df_weights
#     title = "US Taper Tantrum"


    fig = plt.figure(figsize=(18,8))
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax2.set_ylim((0.0,1.0))

    fig.suptitle(title, fontsize=16)

    ## plot randomly simulated portfolios
    ax1.plot(simulated_portfolio_df.STD, simulated_portfolio_df.MEAN, 'o', markersize= 5, alpha=0.01, color="blue")
    ax1.set_xlabel("Return volatilities from " + str(simulations) +" randomly generated portfolios")
    ax1.set_ylabel("Returns from " + str(simulations) +" randomly generated portfolios")

    ## plot actual assets
    colors = np.random.rand(len(asset_names))
    return_mean = np.asarray(expected_return).flatten()
    ax1.scatter(return_std, return_mean, s = 50, edgecolors='none', alpha=0.5, c = "red")
    for i, txt in enumerate(asset_names):
        ax1.annotate(txt, (return_std[i], return_mean[i]),  textcoords='data', fontsize=10)

    ### plot efficient frontier
    ax1.plot(solved_frontier_df.STD , solved_frontier_df.MEAN, 'yo', markersize= 3, alpha=0.04, markeredgewidth=0.0)

    ### plot the ledgend of the 
    simulated_port_legend_patch = Line2D([0], [0], linestyle="none", marker="o", alpha=0.4, markersize=5, markerfacecolor="blue")
    frontier_port_legend_patch = Line2D([0], [0], linestyle="none", marker="o", alpha=0.4, markersize=5, markerfacecolor="yellow")

    ax1.legend([simulated_port_legend_patch, frontier_port_legend_patch], ['Simulated Portfolios', 'Efficient Frontier'], fancybox=True, framealpha=0, loc=9, ncol=3, prop={'size':10}, \
               bbox_to_anchor=(0.5, 1.08), numpoints=1)


    ### plot the efficient frontier weights
    ax2.set_xlim(solved_frontier_df_weights.STD_filters.min(), solved_frontier_df_weights.STD_filters.max())


    s1 = plot_stacked_bar(ax2, solved_frontier_df_weights.WEIGHT_ARP_1.values, left=solved_frontier_df_weights.STD_filters.values, width = 0.001, 
                          color="#7bc043", edgecolor ="none",  align="center")


    s2 = plot_stacked_bar(ax2, solved_frontier_df_weights.WEIGHT_ARP_2.values,left=solved_frontier_df_weights.STD_filters.values, width = 0.001, 
                          color="#f37736", edgecolor ="none", align="center" 
                          ,bottom = solved_frontier_df_weights.WEIGHT_ARP_1.values
                          ) 

    s3 = plot_stacked_bar(ax2, solved_frontier_df_weights.WEIGHT_ARP_3.values,left=solved_frontier_df_weights.STD_filters.values, width = 0.001, 
                          color="#fdf498", edgecolor ="none", align="center"
                          ,bottom = solved_frontier_df_weights.WEIGHT_ARP_1.values + solved_frontier_df_weights.WEIGHT_ARP_2.values
                          ) 
    
    s4 = plot_stacked_bar(ax2, solved_frontier_df_weights.WEIGHT_ARP_4.values,left=solved_frontier_df_weights.STD_filters.values, width = 0.001, 
                          color="#ee4035", edgecolor ="none", align="center"
                          ,bottom = solved_frontier_df_weights.WEIGHT_ARP_1.values + solved_frontier_df_weights.WEIGHT_ARP_2.values + solved_frontier_df_weights.WEIGHT_ARP_3.values
                          ) 
    s5 = plot_stacked_bar(ax2, solved_frontier_df_weights.WEIGHT_prt6040_mth_rebal.values,left=solved_frontier_df_weights.STD_filters.values, width = 0.001, 
                          color="#0392cf", edgecolor ="none", align="center"
                          ,bottom = solved_frontier_df_weights.WEIGHT_ARP_1.values + solved_frontier_df_weights.WEIGHT_ARP_2.values + solved_frontier_df_weights.WEIGHT_ARP_3.values + solved_frontier_df_weights.WEIGHT_ARP_4.values
                          ) 


    ax2.set_ylabel("Weights")
    ax2.set_xlabel("Return volatilites from the efficient frontier portfolios: " + str(solved_frontier_df_weights.STD_filters.min()) + " to " + str(solved_frontier_df_weights.STD_filters.max()))
    ax2.legend([s1, s2, s3, s4, s5], asset_names, fancybox=True, framealpha=0, loc=9, ncol=3, prop={'size':10}, bbox_to_anchor=(0.5, 1.08))

    xtvals = ax1.get_xticks()
    ax1.set_xticklabels(['{:,.1%}'.format(x) for x in xtvals])

    ytvals = ax1.get_yticks()
    ax1.set_yticklabels(['{:,.1%}'.format(x) for x in ytvals])

    xtvals = ax2.get_xticks()
    #ax2.set_xticklabels(['{:,.1%}'.format(x * np.sqrt(250)) for x in xtvals])
    ax2.set_xticklabels(['{:,.1%}'.format(x) for x in xtvals])






########
# Data
########




data_arps = pd.read_excel(r'C:\Dev\ManCave\QuantModels\#applications\AUSUP\QDS_Case_Study_Strategy_Data.xlsm', 'QDS_Case_Study_Strategy_Data')
data_arps = data_arps[['Date', 'ARP_1','ARP_2','ARP_3','ARP_4','prt6040_mth_rebal']]
data_arps['Date'] = pd.to_datetime(data_arps['Date'])
data_arps = data_arps.set_index('Date').ffill()
data_rtns_d = (data_arps/data_arps.shift(1)-1).dropna() 
#assets = data_rtns_d_mvo.columns.tolist()





if ( 1 > 2):
    

    title = "ARP allocation simulation - long only"
    return_data = data_rtns_d
    simulations = 30000
    solved_frontier_df, solved_frontier_df_weights,expected_return, return_std, cov, simulated_portfolio_df, asset_names = create_frontier_weights(return_data, simulations)
    
    chart_mvo_for_regime(simulated_portfolio_df, expected_return, return_std, asset_names, solved_frontier_df, solved_frontier_df_weights, title )






