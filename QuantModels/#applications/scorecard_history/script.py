import datetime
import os, sys, inspect
import numpy as np
import pandas as pd


pd.set_option('display.expand_frame_repr', False)




#data_dates = pd.date_range(start='2018-01-01', end='2020-05-23',freq='W-SAT').sort_values(ascending=False)

data_dates = pd.date_range(start='2016-05-07', end='2018-01-13',freq='W-SAT').sort_values(ascending=False)

release_dates = pd.read_csv(r'./Data/release_dates.csv').applymap(lambda x: x if x != "#N/A Field Not Applicable" else np.NAN)
historical_data = pd.read_csv(r'./Data/ScorecardIndice_20200530_Data.csv')
historical_data['Data_Date'] = pd.to_datetime(historical_data['Data_Date'])
indices = historical_data['Security'].unique()

#df = pd.DataFrame((historical_data['Security'] + '@' + historical_data['Field']).unique(),columns=['A'])['A'].str.split('@', 1, expand=True)
#df = df.groupby([0]).agg(['count'])[1]
#df.sort_values(['count']).tail(20)

# TPX Index           2
# MXWD Index          2
# UKX Index           2
# NZSE50FG Index      2
# AS51 Index          2
# SHSZ300 Index       2
# SPX Index           2
# SXXP Index          2


#removed release dates for these ind as they are incomplete 

#BZPBNO% Index
#CNNGPQ$ Index
#EUBDFRAN Index
#EUBDITAL Index
#EUUCEMU Index








for data_date in data_dates:
    print("========================================================")
    print("===================={0}==========================".format(data_date))
    print("========================================================")

    output = []

    for ind in indices:
        #print(ind)
        if ind in release_dates.columns:
            ind_current_release_date = data_date
            if (len(release_dates[ind].dropna()) > 0):
                            
                ind_releas_dates = pd.to_datetime(release_dates[ind].dropna())                            
                max_release_date = ind_releas_dates[ind_releas_dates <= data_date ].max()
                #some data has incomplete release date
                if (data_date - max_release_date)/np.timedelta64(1, 'M') < 6: 
                    ind_current_release_date = max_release_date
                    print("release date {1} exists for {0}, {2}".format(data_date, ind_current_release_date, ind))                        
                else:
                    print("release date {1} is too old for {0}, {2}".format(data_date, max_release_date, ind))
                    
        ind_current_release_data = historical_data[(historical_data['Security'] == ind) & (historical_data['Data_Date'] <= ind_current_release_date)]
        ind_current_release_data['DataCollectionDate'] = data_date
        ind_current_release_data['DataReleaseDate'] = ind_current_release_date
        
        output = output + [ind_current_release_data]                 
        
    output_df = pd.concat(output, ignore_index=True, axis=0)
    file_name = (r'./output/SCORECARD@HistoricalScorecardIndice~Normalize_Result_{0}.csv'.format(data_date.strftime('%Y%m%d')))
    output_df.to_csv(file_name, index=False)