echo off
SET localPath=%~dp0%
echo %localPath%
pushd %localPath%
C:\Anaconda\python.exe macro_epi_attribution_runner.py
echo Exit:( %ERRORLEVEL% )
popd
echo on
pause