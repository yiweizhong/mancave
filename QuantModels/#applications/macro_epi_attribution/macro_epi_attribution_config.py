APP_ROOT = r"\\capricorn\ausfi\Macro_Dev\Data\macro_epi_attribution"
APP_NAME = r"macro_epi_attribution"
DATE = r"DATE"
SENDER = "YIWEI.ZHONG@ampcapital.com"
RECEIVERS = ["YIWEI.ZHONG@ampcapital.com"]
CC = []
DTYPE_DATEIME64 = r"datetime64[ns]"
ODBC_STYLE_CSTR_PREFIX = r"mssql+pyodbc:///?odbc_connect=%s"
LOG_FORMAT = r"%(asctime)s - %(name)s - %(levelname)s - %(message)s"
TIMESTAMP_FMT = r"%Y-%m-%d %H:%M:%S.%f"
CURRENT_EPI_DATA = r"\\capricorn\ausfi\Macro_Dev\Data\EPIBatch\Archive\EPIINDICE_RESULT_20180623_20180623231345.CSV"
EPI_DATA_BASE1 = r"\\capricorn\ausfi\Macro_Dev\Data\EPIBatch\Archive\EPIINDICE_RESULT_20180617_20180617181818.CSV"
EPI_DATA_BASE2 = r"\\capricorn\ausfi\Macro_Dev\Data\EPIBatch\Archive\EPIINDICE_RESULT_20180603_20180603201425.CSV"
EPI_DATA_BASE3 = r"\\capricorn\ausfi\Macro_Dev\Data\EPIBatch\Archive\EPIINDICE_RESULT_20180524_20180524090335.CSV"
COUNTRIES = ['AU', 'US', 'UK', 'NZ', 'EU', 'CH', 'JN', 'SK', 'TA', 'TH', 'HK', 'MA','IN', 'ID','CA']
COUNTRIES = ['AU']
