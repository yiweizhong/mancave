import _path
import _config as cfg
import core.dirs as dirs
import core.emails as emails



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if (cfg.ARCHIVE_FILES):
    dirs.archive_files(cfg.OUTPUT_FOLDER, cfg.ARCHIVE_OUTPUT_FOLDER) 
    dirs.archive_files(cfg.DEBUG_FOLDER, cfg.ARCHIVE_DEBUG_FOLDER, copy_only = False)       
    #dirs.archive_files(cfg.LOG_FOLDER, cfg.ARCHIVE_LOG_FOLDER)
















_result = dirs.check_log_file_result(cfg.LOG_PATH)
_title = "{0} process - {1}".format(cfg.APP_NAME, _result)
_email_body = dirs.htmlfy_log_file(cfg.LOG_PATH)
emails.send_SMTP_mail(cfg.SENDER, cfg.RECEIVERS, _title, _email_body, cfg.CC, [cfg.LOG_PATH])
   