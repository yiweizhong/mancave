from __future__ import division
import _config as cfg
import logging
import os, sys, inspect
import time, datetime
import pandas as pd
from itertools import permutations, combinations
import argparse
import datetime
import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st


#
FILE_DATE = '2018-03-20'


###########################################################################################
data = pd.read_csv(cfg.INPUT_FOLDER + FILE_DATE + '_FWD_G10_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')

#dates
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)


############################################################################################
# 3y2y cross countries spread, 3y2y 5y5y cross countries curve, 5y5y cross countries spread 
############################################################################################

logging.info('3y2y cross countries spread, 3y2y 5y5y cross countries curve, 5y5y cross countries spread')

data_tenor = [d for d in data.columns if '_3Y_2Y' in d]  + \
             [d for d in data.columns if '_5Y_5Y' in d]
data_tenor = [ d for d in data_tenor if 'NOK' not in d]

data_sub = data[data.index> dt_2yrs_ago].ffill()
data_sub_narrow = data_sub[data_tenor]
#data_sub_narrow.columns = [c.replace('_', '') for c in data_sub_narrow.columns]
data_sub_narrow_pca = pca.PCA(data_sub_narrow, rates_pca = True)


gaps = data_sub_narrow_pca.data_gaps_from_reconstruction(n=[1, 2]).iloc[-1,:]
combos = [((a,b), abs(gaps[a] - gaps[b])) for (a,b) in list(combinations(gaps.index, 2))]
combos.sort(key = lambda a: a[1], reverse=True)

for combo in combos:
     logging.info(combo)


i = 1
for (combo,_) in combos[:]: 
    
    logging.info('process %s vs %s' %(combo[0], combo[1]))

    if ('GBP' in combo[0] or 'GBP' in combo[1]):
        data_sub_narrow_pair = data[data.index> dt_18mths_ago].ffill()[[combo[0],combo[1]]]
    else:
        data_sub_narrow_pair = data[data.index> dt_2yrs_ago].ffill()[[combo[0],combo[1]]]


    combo_type = 'spread' if combo[0][3:] == combo[1][3:] else 'curve' 
    data_sub_narrow_pair.columns = [c.replace('_', '') for c in data_sub_narrow_pair.columns]
    data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
    hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_sub_narrow_pair_pca.pc_hedge
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20)

    #logging.info(mr_obj.mr_attributes)

    tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M')
    fig = data_sub_narrow_pair_pca.create_hedged_pcs_fig()
    plt.savefig(cfg.OUTPUT_FOLDER + '/%s_%s_%s_%s_pca_info_%s.png' % (combo_type, i, combo[0], combo[1], tstmp), bbox_inches='tight')
    plt.close()
    fig2 = mr_obj.create_mr_info_fig()
    plt.savefig(cfg.OUTPUT_FOLDER + '/%s_%s_%s_%s_mr_info_%s.png' % (combo_type, i, combo[0], combo[1], tstmp), bbox_inches='tight')
    plt.close()
    
    i = i + 1

###############################################################################################


