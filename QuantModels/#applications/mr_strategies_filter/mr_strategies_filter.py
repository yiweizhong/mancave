""" 
import _config as cfg
import os, sys, inspect
import time, datetime
import pandas as pd
from itertools import permutations, combinations
import argparse

from core import tools
import core.dirs as dirs
from statistics.mr import bins


def _now(msg=''):
    ts = time.time()
    print datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S') + ': ' + msg


def _next_prd_moves(vals, n=1): 
    return (vals.shift(-1 * n) - vals).reindex(vals.index)

def _ffill_cleanna_keep_name(df, last = 100000):
    clean_df = df.ffill().dropna().tail(last)
    clean_df.name = df.name
    return clean_df


def load_market_data_files(from_dir, filters = []):
    _now(msg = 'start load_market_data_files')
    for subdir, dirs, files in os.walk(from_dir):
        for f in files:
            f_path = os.path.join(from_dir, f)
            df = tools.tframe(pd.read_csv(f_path),key ="date",utc=False)
            if len(filters) == 0:
                for col in df.columns.tolist(): 
                    yield df[col]
            else:
                for col in df.columns.tolist():
                    if col[:3] in filters:    
                        yield df[col]
                        
                
def generate_changes_from_levels(market_data):  
    _now(msg = 'start generate_changes_from_levels')
    def fn(l):
        c = _next_prd_moves(l)
        d = pd.DataFrame({'level':l, 'change':c})[:-1]
        d.name = l.name
        yield d
    return  [fn(d) for d in market_data]


def generate_spreads_from_outright(vals): 
    _now(msg = 'start generate_spreads_from_outright')
    def fn(vals):
        for (a, b) in combinations(vals, 2):
            c = pd.DataFrame({'level':(a['level'] - b['level']), 'change': a['change'] - b['change']})
            c.name = a.name + '_vs_' + b.name
            yield c
    
    return list(fn(vals))

def produce_outputs(mr_attribute=None, bin_profiles=[], data_histories=[], datestring=''):
     
    if mr_attribute is not None:
        
        output_folder = dirs.get_creat_sub_folder(folder_name=datestring, parent_dir=cfg.OUTPUT_FOLDER)
        
        filters = mr_attribute['name']
    
        bin_profile_filters = set(('bin_' + filters).values)
    
        data_history_filters = set(filters.values)
    
        def filter_output_df(df, filters):
            if df.name in filters:
                df.to_csv(output_folder + '/' + df.name + '.csv', index_label = 'id')
        
        mr_attribute.to_csv(output_folder + '/mr_attribute.csv', index_label = 'id')
        
        if len(bin_profiles) > 0:
            map(lambda bin_profile: filter_output_df(bin_profile, bin_profile_filters), bin_profiles)
    
        if len(data_histories) > 0:
             map(lambda data_history: filter_output_df(data_history, data_history_filters), data_histories)

    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='---- market watch ----')
    parser.add_argument('countries', metavar='countries', type=str, nargs='+', 
        help='If there are more than 1 country seperate them by a space')
    args = parser.parse_args()
    print(args.countries)

    exit_code = 0
     
    countries = set(args.countries)

    market_data = load_market_data_files(cfg.INPUT_FOLDER, filters=countries)

    outright = generate_changes_from_levels(market_data)

    spreads = generate_spreads_from_outright(outright)

    clean_spreads = [_ffill_cleanna_keep_name(spread, last=300) for spread in spreads]

    _now(msg = 'start create_equal_width_bins_with_high_evenness')
    bin_profiles =  [bins.create_equal_width_bins_with_high_evenness(d, cfg.BIN_QTY, min_size_limit=0.05, reduction_size = 2, min_num_of_bins = cfg.MIN_BIN_QTY) for d in clean_spreads]


    _now(msg = 'start derive_mr_attributes')    
    mr_attributes = pd.DataFrame(map(bins.derive_mr_attributes, clean_spreads, bin_profiles))
    mr_attributes_0_fidelity = mr_attributes.loc[mr_attributes['mr_score'] <= 0]
    mr_attributes_high_fidelity = mr_attributes.loc[mr_attributes['mr_score'] > 0]



    _now(msg = 'start produce outputs')

    produce_outputs(mr_attribute=mr_attributes_high_fidelity, bin_profiles=bin_profiles, data_histories=clean_spreads, datestring='2017-06-30')

    _now(msg = 'stop')




 """