from __future__ import division
import numpy as np 
import pandas as pd   
import logging
import copy

def diff(ts, window):
    return (ts - ts.shift(window)).reindex(ts.index)

def zscore(ts, window):
    sub_ts = ts.iloc[-window:]
    return (sub_ts.iloc[-1:] - sub_ts.mean())/sub_ts.std()

def momentum(ts, diff_window, zscore_window):
    diff_ts = diff(ts,diff_window)
    return zscore(diff_ts, zscore_window)


def compare(ts1, ts2):
    return (ts1 - ts2)

def systematic_gamma(gamma_components,configs):
    weights = pd.Series({
                'ATM1W_VS_SPOT_RVOL10BD_{0}BDZCS'.format(configs['STZW']): -0.05,
                'ATM1W_VS_SPOT_INTRADAY_RVOL10BD_{0}BDZCS'.format(configs['STZW']): -0.1,
                'ATM1M_{0}BDZCS'.format(configs['STZW']): 0.25,
                'ATM3M_{0}BDZCS'.format(configs['STZW']): 0.15,
                'ATM1Y_{0}BDZCS'.format(configs['STZW']): 0.05,
                'SPOT_INTRADAY_RVOL10BD_{0}BDZCS'.format(configs['STZW']): 0.2,
                'UNDERLINE_{0}BD_TREND'.format(configs['TRNDW']):0.05,
                'POSITIONING':0.05,
                'RR253M_{0}BDZCS'.format(configs['LTZW']):0.1,
                'ATM1M_VS_SPOT_RVOL180BD_{0}BDZCS'.format(configs['LTZW']):-0.05
            })
    return gamma_components.dot(weights)

def systematic_vega(vega_components,configs):
    weights = pd.Series({
                'ATM1M_{0}BDZCS'.format(configs['STZW']): 0.25,
                'ATM3M_{0}BDZCS'.format(configs['STZW']): 0.2,
                'ATM1Y_{0}BDZCS'.format(configs['STZW']): 0.05,
                'ATM1W_{0}BDDIFF_{1}BDZCS'.format(configs['MMTDFW'], configs['MMTZW']):0.05,
                'ATM3M_{0}BDDIFF_{1}BDZCS'.format(configs['MMTDFW'], configs['MMTZW']):0.05,
                'ATM1M_VS_SPOT_RVOL60BD_{0}BDZCS'.format(configs['MTZW']):-0.3,
                'RR251M_{0}BDZCS'.format(configs['LTZW']):0.05,
                'BF251M_{0}BDZCS'.format(configs['LTZW']):0.0,
                'UNDERLINE_{0}BD_TREND'.format(configs['TRNDW']):0.05  
            })
    return vega_components.dot(weights)
    

def gamma_pnl(vega_return, systematic_gamma):
    return vega_return * systematic_gamma  


def generate_scorecard_components(data, configs):
    output = {}

    output['O_SPOT'] = data['SPOT'].iloc[-1:]
    ##
    output['O_ATM1W_{0}BDZCS'.format(configs['STZW'])] = zscore(data['ATM1W'], configs['STZW'])
    output['O_ATM1M_{0}BDZCS'.format(configs['STZW'])] = zscore(data['ATM1M'], configs['STZW'])
    output['O_ATM3M_{0}BDZCS'.format(configs['STZW'])] = zscore(data['ATM3M'], configs['STZW'])
    output['O_ATM6M_{0}BDZCS'.format(configs['STZW'])] = zscore(data['ATM6M'], configs['STZW'])
    output['O_ATM1Y_{0}BDZCS'.format(configs['STZW'])] = zscore(data['ATM1Y'], configs['STZW'])
    ##
    output['O_ATM1W_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['ATM1W'], configs['LTZW'])
    output['O_ATM1M_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['ATM1M'], configs['LTZW'])
    output['O_ATM3M_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['ATM3M'], configs['LTZW'])
    output['O_ATM6M_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['ATM6M'], configs['LTZW'])
    output['O_ATM1Y_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['ATM1Y'], configs['LTZW'])
    ##
    output['O_ATM1W_{0}BDDIFF'.format(configs['MMTDFW'])] = diff(data['ATM1W'], configs['MMTDFW']).iloc[-1:]
    output['O_ATM1M_{0}BDDIFF'.format(configs['MMTDFW'])] = diff(data['ATM1M'], configs['MMTDFW']).iloc[-1:]
    output['O_ATM3M_{0}BDDIFF'.format(configs['MMTDFW'])] = diff(data['ATM3M'], configs['MMTDFW']).iloc[-1:]
    output['O_ATM6M_{0}BDDIFF'.format(configs['MMTDFW'])] = diff(data['ATM6M'], configs['MMTDFW']).iloc[-1:]
    output['O_ATM1Y_{0}BDDIFF'.format(configs['MMTDFW'])] = diff(data['ATM1Y'], configs['MMTDFW']).iloc[-1:]
    ##
    output['O_ATM1W_{0}BDDIFF_{1}BDZCS'.format(configs['MMTDFW'], configs['MMTZW'])] = momentum(data['ATM1W'], configs['MMTDFW'], configs['MMTZW'])
    output['O_ATM1M_{0}BDDIFF_{1}BDZCS'.format(configs['MMTDFW'], configs['MMTZW'])] = momentum(data['ATM1M'], configs['MMTDFW'], configs['MMTZW'])
    output['O_ATM3M_{0}BDDIFF_{1}BDZCS'.format(configs['MMTDFW'], configs['MMTZW'])] = momentum(data['ATM3M'], configs['MMTDFW'], configs['MMTZW'])
    output['O_ATM6M_{0}BDDIFF_{1}BDZCS'.format(configs['MMTDFW'], configs['MMTZW'])] = momentum(data['ATM6M'], configs['MMTDFW'], configs['MMTZW'])
    output['O_ATM1Y_{0}BDDIFF_{1}BDZCS'.format(configs['MMTDFW'], configs['MMTZW'])] = momentum(data['ATM1Y'], configs['MMTDFW'], configs['MMTZW'])
    ##
    vol_diff = compare(data['ATM1W'], data['SPOT_RVOL10BD'])
    output['O_ATM1W_VS_SPOT_RVOL10BD'] = vol_diff.iloc[-1:]
    output['O_ATM1W_VS_SPOT_RVOL10BD_{0}BDZCS'.format(configs['STZW'])] = zscore(vol_diff, configs['STZW'])
    vol_diff = compare(data['ATM1W'], data['SPOT_INTRADAY_RVOL10BD'])
    output['O_ATM1W_VS_SPOT_INTRADAY_RVOL10BD'] = vol_diff.iloc[-1:]
    output['O_ATM1W_VS_SPOT_INTRADAY_RVOL10BD_{0}BDZCS'.format(configs['STZW'])] = zscore(vol_diff, configs['STZW'])
    output['O_SPOT_RVOL10BD_{0}BDZCS'.format(configs['STZW'])] = zscore(data['SPOT_RVOL10BD'], configs['STZW'])
    output['O_SPOT_INTRADAY_RVOL10BD_{0}BDZCS'.format(configs['STZW'])] = zscore(data['SPOT_INTRADAY_RVOL10BD'], configs['STZW'])
    vol_diff = compare(data['ATM1M'], data['SPOT_RVOL60BD'])
    output['O_ATM1M_VS_SPOT_RVOL60BD_{0}BDZCS'.format(configs['MTZW'])] = zscore(vol_diff, configs['MTZW'])
    vol_diff = compare(data['ATM1M'], data['SPOT_RVOL180BD'])
    output['O_ATM1M_VS_SPOT_RVOL180BD_{0}BDZCS'.format(configs['LTZW'])] = zscore(vol_diff, configs['LTZW'])
    
    ##
    output['O_RR251W_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['RR251W'], configs['LTZW'])
    output['O_RR251M_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['RR251M'], configs['LTZW'])
    output['O_RR253M_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['RR253M'], configs['LTZW'])
    output['O_RR256M_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['RR256M'], configs['LTZW'])
    output['O_RR251Y_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['RR251Y'], configs['LTZW'])	
    
    ##
    output['O_BF251W_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['BF251W'], configs['LTZW'])
    output['O_BF251M_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['BF251M'], configs['LTZW'])
    output['O_BF253M_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['BF253M'], configs['LTZW'])
    output['O_BF256M_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['BF256M'], configs['LTZW'])
    output['O_BF251Y_{0}BDZCS'.format(configs['LTZW'])] = zscore(data['BF251Y'], configs['LTZW'])	

    ##
    output['O_UNDERLINE_{0}BD_MOMENTUM'.format(configs['MMTDFW'])] = (100 * diff(data['SPOT'], configs['MMTDFW'])/data['SPOT']/((data['SPOT_RVOL10BD'].pow(2)/50).pow(0.5))).iloc[-1:]
    output['O_UNDERLINE_{0}BD_TREND'.format(configs['TRNDW'])] = (100 * diff(data['SPOT'], configs['TRNDW'])/data['SPOT']).iloc[-1:] 
    output['O_POSITIONING'] = data['POSITIONING'].iloc[-1:]
    output['O_REVERSION_TENDENCY'] = (data['SPOT_INTRADAY_RVOL10BD'] - data['SPOT_RVOL10BD']).iloc[-1:]
    output['O_SPOT_INTRADAY_RVOL10BD_RVOL180BD'] = data['SPOT_INTRADAY_RVOL10BD_RVOL180BD'].iloc[-1:]

    ##
    output['O_SPOT_RVOL10BD_VS_ATM1M_{0}BD_AGO_AKA_GAMMACLOSING'.format(configs['VEGADLYW'])] = (data['SPOT_RVOL10BD'] - data['ATM1M'].shift(configs['VEGADLYW'])).iloc[-1:]
    output['O_SPOT_INTRADAY_RVOL10BD_VS_ATM1M_{0}BD_AGO_AKA_GAMMAINTRADAY'.format(configs['VEGADLYW'])] = (data['SPOT_INTRADAY_RVOL10BD'] - data['ATM1M'].shift(configs['VEGADLYW'])).iloc[-1:]
    output['O_ATM1M_AKA_VEGA'] = data['ATM1M'].iloc[-1:]
    
    ##
    gamma_components = [
            'ATM1W_VS_SPOT_RVOL10BD_{0}BDZCS'.format(configs['STZW']),
            'ATM1W_VS_SPOT_INTRADAY_RVOL10BD_{0}BDZCS'.format(configs['STZW']),
            'ATM1M_{0}BDZCS'.format(configs['STZW']),
            'ATM3M_{0}BDZCS'.format(configs['STZW']),
            'ATM1Y_{0}BDZCS'.format(configs['STZW']),
            'SPOT_INTRADAY_RVOL10BD_{0}BDZCS'.format(configs['STZW']),
            'UNDERLINE_{0}BD_TREND'.format(configs['TRNDW']),
            'POSITIONING',
            'RR253M_{0}BDZCS'.format(configs['LTZW']),
            'ATM1M_VS_SPOT_RVOL180BD_{0}BDZCS'.format(configs['LTZW'])
    ]
    gamma_components = pd.DataFrame({k:output['O_{0}'.format(k)] for k in (gamma_components)})[gamma_components]
    output['O_SYSTEMATIC_GAMMA'] = systematic_gamma(gamma_components, configs) 
    output['O_GAMMA_PNL'] = output['O_SYSTEMATIC_GAMMA'] * (output['O_SPOT_RVOL10BD_VS_ATM1M_{0}BD_AGO_AKA_GAMMACLOSING'.format(configs['VEGADLYW'])] - 0.2)/100/2 + 1


    vega_components = [
            'ATM1M_{0}BDZCS'.format(configs['STZW']),
            'ATM3M_{0}BDZCS'.format(configs['STZW']),
            'ATM1Y_{0}BDZCS'.format(configs['STZW']),
            'ATM1W_{0}BDDIFF_{1}BDZCS'.format(configs['MMTDFW'], configs['MMTZW']),
            'ATM3M_{0}BDDIFF_{1}BDZCS'.format(configs['MMTDFW'], configs['MMTZW']),
            'ATM1M_VS_SPOT_RVOL60BD_{0}BDZCS'.format(configs['MTZW']),
            'RR251M_{0}BDZCS'.format(configs['LTZW']),
            'BF251M_{0}BDZCS'.format(configs['LTZW']),
            'UNDERLINE_{0}BD_TREND'.format(configs['TRNDW'])  
    ]
    vega_components = pd.DataFrame({k:output['O_{0}'.format(k)] for k in (vega_components)})[vega_components]
    output['O_SYSTEMATIC_VEGA'] = systematic_vega(vega_components, configs)
    output['O_VEGA_PNL'] = output['O_SYSTEMATIC_VEGA'] * (output['O_ATM1M_AKA_VEGA'] - 0.2)/100/2 + 1


    ##
    return pd.DataFrame(output)