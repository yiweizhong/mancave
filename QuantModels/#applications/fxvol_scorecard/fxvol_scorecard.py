from __future__ import division
import _config as cfg
import logging
import os, sys, inspect
import time, datetime
import pandas as pd
from pandas.tseries.offsets import *
import numpy as np
import argparse

from core import tools
from core import returns
import core.dirs as dirs
import component


def _now(msg=''):
    ts = time.time()
    print datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S') + ': ' + msg

def load_market_data_files(from_dir, country_filter):
    def filter_generator():
        for subdir, dirs, files in os.walk(from_dir):
            for f in files:
                f_path = os.path.join(from_dir, f)
                df = tools.tframe(pd.read_csv(f_path),key ="date",utc=False)
                if country_filter in df.columns:    
                    col = f.replace('_historical.csv', '')
                    col = col[11:] if len(col.split('_')[0]) == 10 else col 
                    yield (col, df[country_filter])
    return pd.DataFrame({k:v for (k,v) in filter_generator()})
    

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='---- fxvol scorecard ----')
    parser.add_argument('countries', metavar='countries', type=str, nargs='+', 
        help='whose fxvol score will be calculated. If there are more than 1 country seperate them by a space')
    args = parser.parse_args()
    print(args.countries)

    exit_code = 0
    for country in args.countries:
        logging.info("Start to process fxvol scorecard for {0}".format(country))
        output = []
        country_data = load_market_data_files(cfg.INPUT_FOLDER, country)
        
        #print country_data.tail(20)
        
        country_config = cfg.COUNTRY_CONFIGS[country]
        days_to_run = country_config['DAYS_TO_RUN']
        lt_window = country_config['LTZW']

        if len(country_data.index) >= (days_to_run + lt_window): 
            for offset in np.arange(days_to_run):
                country_data_subset = country_data.iloc[-(lt_window + offset):,:] if offset == 0 else country_data.iloc[-(lt_window + offset):- offset,:]
                country_data_subset = country_data_subset.fillna(method='ffill')
                output.append(component.generate_scorecard_components(country_data_subset, country_config))
        else:
            logging.error("Not enough data to run fxvol_scorecard for {0} days with {1} days of data. Process is skipped.".format(days_to_run, len(country_data.index))) 
            exit_code = 1

        if (exit_code == 0):
            #print pd.concat(output).sort_index()
            
            file = os.path.join(cfg.OUTPUT_FOLDER, "fxvol_scorecard_{0}.csv".format(country))
            file_t = os.path.join(cfg.OUTPUT_FOLDER, "fxvol_scorecard_t_{0}.csv".format(country))
            #pd.concat(output).sort_index().to_csv(file)
            output_df = pd.concat(output)
            output_df = pd.concat([country_data, output_df], axis=1).reindex(country_data.index).sort_index()
            output_df.to_csv(file)
            output_df.transpose().to_csv(file_t)
            logging.info("Finish processing fxvol scorecard for {0}. Output is {1}".format(country, file))



    sys.exit(exit_code)



 





