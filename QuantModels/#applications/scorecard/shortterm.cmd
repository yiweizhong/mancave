echo off
SET localPath=%~dp0%
echo %localPath%
pushd %localPath%

rem the existing email logic needs python 2, python 3 wont work
C:\Anaconda2\python.exe scorecard.py Shortterm

rem the existing email logic needs python 2, python 3 wont work
rem C:\Anaconda3\python.exe scorecard.py Shortterm

if errorlevel 1 (
   echo Process failed with code %errorlevel%
   exit /b %errorlevel%
)

popd
echo on
pause