import os, sys, inspect
import pandas as pd 
import numpy as np
from scipy.stats import kurtosis, skew
import _path
import os, sys, inspect
import logging
from collections import namedtuple
import common.dirs as dirs
import common.emails3 as emails
import data.datatools as datatools
import data.database3 as db
import _config as cfg


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper

pd.set_option('display.expand_frame_repr', False)




query = r"""
SELECT comp.[RecDate] [DATE]
      ,comp.[Cusip]
      ,comp.[ISIN]
      ,comp.[Desc]
      ,comp.[Ticker]
      ,comp.[Par_Wtd_Coupon]
      ,comp.[Maturity_Date]
      ,comp.[Rating]
      ,comp.[ISO_Currency_Code]
      ,comp.[ISO_Country_Code]
      ,comp.[Industry_Desc]
      ,comp.[Face_Value]
      ,comp.[OAS]
      ,comp.[Asset_Swap_Spread]
      ,comp.[Effective_Yield]
      ,comp.[Total_Return_Pct_MTD_LOC]
      ,comp.[Excess_Return_Pct_MTD]
      ,comp.[Effective_Duration]
	  ,hl.OAS [Hl_OAS]
  FROM [Credit].[dbo].[BAML_G0BC] comp
  WHERE comp.[OAS] is not null
  Left outer join  
  (
		select * 
		from [Credit].[dbo].[BAML_Index_Level]
		where [IndexName] = 'G0BC'
  ) hl on comp.RecDate = hl.RecDate
  order by comp.[RecDate] asc
"""

"""
def setup():
    """Log settings"""
    _log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=cfg.APP_ROOT)
    _debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=cfg.APP_ROOT)
    _output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=cfg.APP_ROOT)
    _archive_folder = dirs.get_creat_sub_folder(folder_name="Archive", parent_dir=cfg.APP_ROOT)

    _archive_log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=_archive_folder)
    _archive_debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=_archive_folder)
    _archive_output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=_archive_folder)

    _log_file_name = dirs.get_log_file_name(logfile_name_prefix=cfg.APP_NAME,log_folder=_log_folder)

    return _log_folder, _debug_folder, _output_folder, _archive_folder, _log_file_name, _archive_log_folder, _archive_debug_folder, _archive_output_folder




_log_folder, _debug_folder, _output_folder, _archive_folder, _log_file_name, _archive_log_folder, _archive_debug_folder, _archive_output_folder = setup()


logging.basicConfig(filename= _log_file_name, format=cfg.LOG_FORMAT, level=logging.DEBUG)
logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))

#dirs.archive_files(_debug_folder, _archive_debug_folder, copy_only = True)
#dirs.archive_files(_output_folder, _archive_output_folder, copy_only = True)

_db = db.Database(cfg.FI_PROD_DB_CONN_STRING,_debug_folder)

g0bc_data = _db.load_single_table_from_db(query, 'g0bc', key_col="DATE", debug=True)

"""

g0bc_data.to_csv(os.path.join(r"c:/temp/test/credit_risk_premium/input/",  "%s.%s" % ("g0bc_20201101","CSV")))

g0bc_data_component_stats = pd.DataFrame({'Mean':g0bc_data['OAS'].resample('B').mean(),
                                          'Stdev':g0bc_data['OAS'].resample('B').std(),
                                          'Skew':g0bc_data['OAS'].resample('B').apply(skew),
                                          'Kurtosis':g0bc_data['OAS'].resample('B').apply(kurtosis),
                                          })

g0bc_data_component_stats.to_csv(os.path.join(r"c:/temp/test/credit_risk_premium/input/",  "%s.%s" % ("g0bc_component_stats_20201101","CSV")))


pca.PCA(g0bc_data_component_stats.tail(4000), rates_pca = False, momentum_size = None).plot_loadings(n=3)


q2 ='''
select a.[RecDate], [G0BC], [HW00]
from
(
select [RecDate] , OAS [G0BC] 
from [Credit].[dbo].[BAML_Index_Level]
where [IndexName] = 'G0BC'
) a
left outer join 
(
select [RecDate], OAS [HW00]
from [Credit].[dbo].[BAML_Index_Level]
where [IndexName] = 'HW00')
b on a.[RecDate] = b.[RecDate]
order by a.[RecDate] asc
'''

_log_folder, _debug_folder, _output_folder, _archive_folder, _log_file_name, _archive_log_folder, _archive_debug_folder, _archive_output_folder = setup()


logging.basicConfig(filename= _log_file_name, format=cfg.LOG_FORMAT, level=logging.DEBUG)
logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))


_db = db.Database(cfg.FI_PROD_DB_CONN_STRING,_debug_folder)

g0bcwh00_data = _db.load_single_table_from_db(q2, 'g0bcwh00', key_col="RecDate", debug=True)


pca.PCA(g0bcwh00_data.tail(4000), rates_pca = False, momentum_size = None).plot_loadings(n=3)

pca.PCA(g0bcwh00_data.tail(150).head(50), rates_pca = False, momentum_size = None).plot_loadings(n=3)
