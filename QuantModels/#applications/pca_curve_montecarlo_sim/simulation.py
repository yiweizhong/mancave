from __future__ import division
import _config as cfg
import pandas as pd
from pandas.tseries.offsets import *
import numpy as np
from core import tools
from core import returns
import core.dirs as dirs
from statistics.mr import bins
from statistics.factor import pca
#
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
import matplotlib.cm as cm
#

def imply_from_simulated_pcs(pc1_s, pc2_s, pc3_s, loadings, means, i):
    simued_loadings = pd.DataFrame(
                                    {'Loading1':pd.Series(np.array(pc1_s[i])[0],index=future_dates),
                                     'Loading2':pd.Series(np.array(pc2_s[i])[0],index=future_dates),
                                     'Loading3':pd.Series(np.array(pc3_s[i])[0],index=future_dates)})
    implied_curves = simued_loadings.dot(loadings) + means
    implied_curves['US2s5s'] = implied_curves['USGG5YR'] -  implied_curves['USGG2YR']
    implied_curves['US2s10s'] = implied_curves['USGG10YR'] -  implied_curves['USGG2YR']
    implied_curves['US5s10s'] = implied_curves['USGG10YR'] -  implied_curves['USGG5YR']
    implied_curves['US10s30s'] = implied_curves['USGG30YR'] -  implied_curves['USGG10YR']
    
    return implied_curves



cfg.INPUT_FOLDER = """C:\Dev\Models\QuantModels\data"""
data_raw = pd.read_csv(cfg.INPUT_FOLDER + "/2017-12-14_USD_mixed_historical.csv")
data_raw = tools.tframe(data_raw, key ="date",utc=False).loc['2016-06-01':].ffill()

data_raw['ED1'] = 100 - data_raw['ED1']
data_raw['ED2'] = 100 - data_raw['ED2']
data_raw['ED3'] = 100 - data_raw['ED3']
data_raw['ED4'] = 100 - data_raw['ED4']
data_raw['ED5'] = 100 - data_raw['ED5']
data_raw['ED6'] = 100 - data_raw['ED6']
data_raw['ED7'] = 100 - data_raw['ED7']
data_raw['ED8'] = 100 - data_raw['ED8']
data_raw['ED9'] = 100 - data_raw['ED9']
data_raw['ED10'] = 100 - data_raw['ED10']
data_raw['ED11'] = 100 - data_raw['ED11']
data_raw['ED12'] = 100 - data_raw['ED12']


return_1d = returns.simple_level_diff(data_raw,1).dropna()
return_5d = returns.simple_level_diff(data_raw,5).dropna()

return_1d_pca = pca.PCA(return_1d)
return_5d_pca = pca.PCA(return_5d)

pca_obj = return_1d_pca

eigvals_pct, eigvecs, eigvals = pca_obj.get_eigens()
eigvals_cum_pct = eigvals_pct.cumsum()

np.savetxt(cfg.OUTPUT_FOLDER + "/eigenvals_pct.csv", eigvals_pct, delimiter=",")
np.savetxt(cfg.OUTPUT_FOLDER + "/eigenvals_cum_pct.csv", eigvals_cum_pct, delimiter=",")

pc1_vol = np.sqrt(eigvals[0])
pc2_vol = np.sqrt(eigvals[1])
pc3_vol = np.sqrt(eigvals[2])


sims = 200

n = 60

future_dates = pd.date_range('2017-12-14', '2018-12-14',freq=BDay())[0:n]

pc1_simus = np.matrix([np.random.normal(0, pc1_vol, n) * np.sqrt(i) for i in np.arange(1,sims + 1)])
pc2_simus = np.matrix([np.random.normal(0, pc2_vol, n) * np.sqrt(i) for i in np.arange(1,sims + 1)])
pc3_simus = np.matrix([np.random.normal(0, pc3_vol, n) * np.sqrt(i) for i in np.arange(1,sims + 1)])

pc1_simus_mean = np.array(pc1_simus.mean(axis=0))[0]
pc2_simus_mean = np.array(pc2_simus.mean(axis=0))[0]
pc3_simus_mean = np.array(pc3_simus.mean(axis=0))[0]

pc1_simus_std = np.array(pc1_simus.std(axis=0, ddof =1))[0]
pc2_simus_std = np.array(pc2_simus.std(axis=0, ddof =1))[0]
pc3_simus_std = np.array(pc3_simus.std(axis=0, ddof =1))[0]


top3LoadingsT = pca_obj.loadings[['Loading1','Loading2', 'Loading3']].transpose()


implied_curve_moves = [imply_from_simulated_pcs(pc1_simus, pc2_simus, pc3_simus, top3LoadingsT, pca_obj.data_mean, i) for i in np.arange(0,sims)]


implied_curve_moves_concat = pd.concat(implied_curve_moves)

implied_curve_moves_concat_grp = implied_curve_moves_concat.groupby(implied_curve_moves_concat.index)

implied_curve_moves_mean = implied_curve_moves_concat_grp.mean()

implied_curve_moves_std = implied_curve_moves_concat_grp.std(ddof = 1)



#plt.plot(implied_curve_moves_mean['US2s5s'].cumsum())
#plt.plot(implied_curve_moves_mean['US5s10s'].cumsum())

implied_curve_moves_mean.to_csv(cfg.OUTPUT_FOLDER + "implied_curves_mean.csv", index = 'date')
implied_curve_moves_std.to_csv(cfg.OUTPUT_FOLDER + "implied_curves_std.csv", index = 'date')










