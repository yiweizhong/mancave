from __future__ import division
import os, datetime
import pandas as pd 
from pandas.tseries.offsets import DateOffset
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
_p = 'C:\Dev\Models\QuantModels\#applications\epi_diffusion'
if _p not in sys.path: sys.path.insert(0, _p)
import _path
import os, sys, inspect
import logging
from collections import namedtuple
import common.dirs as dirs
import common.emails as emails
import data.database as db
import _config as cfg


 def diffusion(row):
        count = row.dropna().count()
        increase = row.map(lambda i: 1 if i > 0 else 0).sum()
        return 0 if count == 0 else increase/count


def collect_diffusion_components(epi_type, epi_countries=[], epi_regions=[]):
    
    dataTypeFilter = cfg.DataType.map(lambda i: i in ('Business', 'Consumer', 'Employment', 'Employment', 'Inflation', 'Headline')) if epi_type == 'Headline' else epi_cfg.DataType == epi_type
    
    diffusion =  {  
            
        epi_country : epi_cfg[(epi_cfg.Country == epi_country) & (dataTypeFilter) & (cfg.FwdLagging.map(lambda i: i in ('Lead', 'Lag')))][['BBGTickerCode','GrowthCorrelation']]
        for epi_country in epi_countries        
        }
    
    diffusion.update(
            {  
                epi_region : epi_cfg[(epi_cfg.Region == epi_region) & (dataTypeFilter) & (cfg.FwdLagging.map(lambda i: i in ('Lead', 'Lag')))][['BBGTickerCode','GrowthCorrelation']]
                for epi_region in epi_regions        
            }
        )
    
    diffusion.update(
            {  
                'Global' : epi_cfg[(dataTypeFilter) & (cfg.FwdLagging.map(lambda i: i in ('Lead', 'Lag')))][['BBGTickerCode','GrowthCorrelation']]
            }
        )
    
    diffusion = { k: v.set_index('BBGTickerCode') for (k,v) in diffusion.iteritems() if len(v.index > 0)}
    
    return diffusion    


epi_cfg = pd.read_csv('C:\Dev\Models\QuantModels\#applications\epi_diffusion\EPI_Indice_Categorisation_full_705.csv')                      
epi_dir = '//capricorn/AUSFI/Macro_Dev/Data/EPIBatch/Archive/'
epi_diffusion_out_dir = '//capricorn/AUSFI/FXNEW/FX Model v clean THIS ONE/Regime Scorecard/data/'                       
epi_types = epi_cfg.DataType.unique()
epi_countries = ['AU', 'US', 'CA', 'EU', 'UK', 'GE', 'NZ', 'CH']
epi_regions = ['G10','Asia']


business_diffusion = collect_diffusion_components('Business', epi_countries = epi_countries, epi_regions = epi_regions)
consumer_diffusion = collect_diffusion_components('Consumer', epi_countries = epi_countries, epi_regions = epi_regions)
employment_diffusion = collect_diffusion_components('Employment', epi_countries = epi_countries, epi_regions = epi_regions)
growth_diffusion = collect_diffusion_components('Employment', epi_countries = epi_countries, epi_regions = epi_regions)
inflation_diffusion = collect_diffusion_components('Inflation', epi_countries = epi_countries, epi_regions = epi_regions)
headline_diffusion = collect_diffusion_components('Headline', epi_countries = epi_countries, epi_regions = epi_regions)


####################################################### Data

epi_files = map(lambda f: (datetime.datetime.fromtimestamp(os.stat(os.path.join(epi_dir,f)).st_ctime), os.path.join(epi_dir,f)), os.listdir(epi_dir))
epi_files = pd.Series({pd.to_datetime(epi_time.date()): epi_files for (epi_time, epi_files) in epi_files})
epi_files_full = epi_files.resample('D').asfreq().ffill()
epi_files_full = pd.DataFrame({'New':epi_files_full, 'Old': epi_files_full.shift(90)})
epi_files_matched = epi_files_full.reindex(epi_files.index).dropna()

epi_data_changes = \
        { 
            idx: (pd.read_csv(epi_file.loc['New'])[['Security','PX_LAST']].groupby('Security').last() -
                  pd.read_csv(epi_file.loc['Old'])[['Security','PX_LAST']].groupby('Security').last())['PX_LAST']
            for idx, epi_file in epi_files_matched.iterrows()
        }
epi_data_changes_df = pd.DataFrame(epi_data_changes).T



business_diffusion_compchgs = { k: epi_data_changes_df.reindex(list(v.index), axis=1).mul(v['GrowthCorrelation'], axis='columns')  for k, v in business_diffusion.iteritems()}
consumer_diffusion_compchgs = { k: epi_data_changes_df.reindex(list(v.index), axis=1).mul(v['GrowthCorrelation'], axis='columns')  for k, v in consumer_diffusion.iteritems()}
employment_diffusion_compchgs = { k: epi_data_changes_df.reindex(list(v.index), axis=1).mul(v['GrowthCorrelation'], axis='columns')  for k, v in employment_diffusion.iteritems()}
growth_diffusion_compchgs = { k: epi_data_changes_df.reindex(list(v.index), axis=1).mul(v['GrowthCorrelation'], axis='columns')  for k, v in growth_diffusion.iteritems()}
inflation_diffusion_compchgs = { k: epi_data_changes_df.reindex(list(v.index), axis=1).mul(v['GrowthCorrelation'], axis='columns')  for k, v in inflation_diffusion.iteritems()}
headline_diffusion_compchgs = { k: epi_data_changes_df.reindex(list(v.index), axis=1).mul(v['GrowthCorrelation'], axis='columns')  for k, v in headline_diffusion.iteritems()}


business_diffusion_output = pd.DataFrame({ k: v.apply(diffusion, axis=1) for (k,v) in business_diffusion_compchgs.iteritems()})
consumer_diffusion_output = pd.DataFrame({ k: v.apply(diffusion, axis=1) for (k,v) in consumer_diffusion_compchgs.iteritems()})
employment_diffusion_output = pd.DataFrame({ k: v.apply(diffusion, axis=1) for (k,v) in employment_diffusion_compchgs.iteritems()})
growth_diffusion_output = pd.DataFrame({ k: v.apply(diffusion, axis=1) for (k,v) in growth_diffusion_compchgs.iteritems()})
inflation_diffusion_output = pd.DataFrame({ k: v.apply(diffusion, axis=1) for (k,v) in inflation_diffusion_compchgs.iteritems()})
headline_diffusion_output = pd.DataFrame({ k: v.apply(diffusion, axis=1) for (k,v) in headline_diffusion_compchgs.iteritems()})


business_diffusion_output.to_csv(epi_diffusion_out_dir + 'real_business_epi_diffusion.csv',index_label='Date')
consumer_diffusion_output.to_csv(epi_diffusion_out_dir + 'real_consumer_epi_diffusion.csv',index_label='Date')
employment_diffusion_output.to_csv(epi_diffusion_out_dir + 'real_employment_epi_diffusion.csv',index_label='Date')
growth_diffusion_output.to_csv(epi_diffusion_out_dir + 'real_growth_epi_diffusion.csv',index_label='Date')
inflation_diffusion_output.to_csv(epi_diffusion_out_dir + 'real_inlfation_epi_diffusion.csv',index_label='Date')
headline_diffusion_output.to_csv(epi_diffusion_out_dir + 'real_headline_epi_diffusion.csv',index_label='Date')




