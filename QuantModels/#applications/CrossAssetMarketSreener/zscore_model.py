import pandas as pd
import numpy as np
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations

from pandas.tseries.offsets import DateOffset


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper




rundate = '2020-09-03'


########## Equity


equity_hisotry_starting_point = '2005-01-01'

data_equity = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_EQUITY_historical.csv')  
data_equity['date'] = pd.to_datetime(data_equity['date'])
data_equity = data_equity.set_index('date').ffill()

data_equity_pe = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_EQUITY_PE_historical.csv')  
data_equity_pe['date'] = pd.to_datetime(data_equity_pe['date'])
data_equity_pe = data_equity_pe.set_index('date').ffill()


data_fut_equity_pe = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_EQUITY_FUT_PE_historical.csv')  
data_fut_equity_pe['date'] = pd.to_datetime(data_fut_equity_pe['date'])
data_fut_equity_pe = data_fut_equity_pe.set_index('date').ffill()

data_equity_pe_count = (data_fut_equity_pe.applymap(lambda x: 0 if np.isnan(x) else 1) + data_equity_pe.applymap(lambda x: 0 if np.isnan(x) else 1))
data_equity_avg_pe = (data_equity_pe.fillna(0) + data_fut_equity_pe.fillna(0))/data_equity_pe_count
data_equity_avg_pe_history = data_equity_avg_pe[data_equity_avg_pe.index > equity_hisotry_starting_point]


#pca.PCA(data_equity_avg_pe[data_equity_avg_pe.index > '2005-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3, m=5)
# pca suggest the cycle is around 3-4 year peak to bottom


def generate_v_score(ds, valuation_proxy_window, valuation_std_window = 0):
    valulation = ds - ds.rolling(valuation_proxy_window).mean()
    v_mean = valulation.rolling(valuation_std_window).mean() if valuation_std_window != 0 else valulation.expanding().mean()
    v_std = valulation.rolling(valuation_std_window).std() if valuation_std_window != 0 else valulation.expanding().std()
    v_scores = (valulation - v_mean)/v_std 
    return v_scores

def generate_m_score(ds, m_window, m_std_window = 0):
    mmtn = ds - ds.shift(m_window)
    m_mean = mmtn.rolling(m_std_window).mean() if m_std_window != 0 else mmtn.expanding().mean()
    m_std = mmtn.rolling(m_std_window).std() if m_std_window != 0 else mmtn.expanding().std()
    m_scores = (mmtn - m_mean)/m_std 
    return m_scores



def generate_country_v_scores(country_data, proxy_windows, valuation_std_windows):
    c_v_scores ={c:
                 {proxy_window:
                  {valuation_std_window:generate_v_score(country_data[c], proxy_window, valuation_std_window = valuation_std_window) 
                        for valuation_std_window in valuation_std_windows} 
                     for proxy_window in proxy_windows}
                 for c in country_data.columns} 
    return c_v_scores



def generate_country_m_scores(country_data, m_windows, m_std_windows):
    c_m_scores ={c:
                 {m_window:
                  {m_std_window: generate_m_score(country_data[c], m_window, m_std_window = m_std_window)
                   for m_std_window in m_std_windows}
                  for m_window in m_windows}
                for c in country_data.columns} 
    return c_m_scores



def equity_investment_strategiser(v_scores, m_scores):
    score_std = v_scores.std()
    risk_allocations = {}
    for date, v_score in v_scores.items():
        if v_score >= 2 * score_std: 
            risk_allocations[date] = -2
        elif v_score >= 1 * score_std: 
            risk_allocations[date] = -1
        elif v_score < -2 * score_std:
            risk_allocations[date] = 2
        elif v_score < -1 * score_std:
            risk_allocations[date] = 1
        else:
            risk_allocations[date] = 0
    return pd.Series(risk_allocations)



def stimulate_investment_strategy(country_valuation_scores, strategiser):
    c_strategy_risk={c:
                     {proxy_window:
                      {valuation_std_window: strategiser(country_valuation_scores[c][proxy_window][valuation_std_window]) 
                       for valuation_std_window in country_valuation_scores[c][proxy_window]} 
                      for proxy_window in country_valuation_scores[c]} 
                    for c in country_valuation_scores} 
    return c_strategy_risk




equity_v_score_proxy_windows = np.arange(200,2650,50)
equity_v_score_std_windows = [0]
equity_m_score_windows = [25]
equity_m_score_std_windows = [75]

#[country][valuation_proxy_window_size][valuation_std_window_size]
equity_country_v_scores_for_different_windows = generate_country_v_scores(data_equity_avg_pe_history, equity_v_score_proxy_windows, equity_v_score_std_windows) 
equity_country_m_scores_for_different_windows = generate_country_m_scores(data_equity_avg_pe_history, equity_m_score_windows,  equity_m_score_std_windows) 




data_equity_return = (data_equity - data_equity.shift(1))/data_equity.shift(1)


country_valuation_scores_based_strategies = stimulate_investment_strategy(country_v_scores_for_different_windows, equity_investment_strategiser)



##########



