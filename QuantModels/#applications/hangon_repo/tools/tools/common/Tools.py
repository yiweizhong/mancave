import numpy as np
import pandas as pd



def get_function_name():
    import traceback
    return traceback.extract_stack(None, 2)[0][2]

def get_function_parameters_and_values():
    import inspect
    frame = inspect.currentframe().f_back
    args, _, _, values = inspect.getargvalues(frame)
    return ([(i, values[i]) for i in args])


def RollingSampleDf(dataframein, windowsize=1):
    rows = len(dataframein)
    for i in np.arange(rows - windowsize):
         yield(dataframein.ix[i:i+windowsize,])

def load_raw_csv(file, key):
    df = pd.read_csv(file)
    df2 = df.set_index(key)
    df2.index = pd.to_datetime(df2.index)
    for col in df2.columns.tolist():
        df2[col] = pd.to_numeric(df2[col], errors="coerce")
    return df2

def re_key_df_with_dates(dfin, key):
    dfout = dfin.set_index(key)
    dfout.index = pd.to_datetime(dfout.index)
    for col in dfout.columns.tolist():
        dfout[col] = pd.to_numeric(dfout[col], errors="coerce")
    return dfout