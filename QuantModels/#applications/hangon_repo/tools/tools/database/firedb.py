import pyrebase
import pandas as pd
#import firedb_config as fcfg
import datetime

CONFIG = {
    "apiKey": "AIzaSyC0Ti0uqUax9evBeaPhvMUBxdvpOLUxRC4",
    "authDomain": "mmacro-922ff.firebaseapp.com",
    "databaseURL": "https://macro-922ff.firebaseio.com",
    "storageBucket": "macro-922ff.appspot.com"
}

ACCT_EMAIL = "testuser@gmail.com"
ACCT_PWD = "123456"

#print retrieve("CUSTOMDATA/AUD")
class FireDatabase(object):
    def __init__(self, account=ACCT_EMAIL, pwd=ACCT_PWD, config=CONFIG):
        self.account = account
        self.pwd = pwd
        self.config = config

    def retrieve(self, path):
        """This function will retrieve data from firebase
        Args:
            path (string): The path to the data segment.

        returns:
            (string) : a json string representing the data. 
            if the path is not found None will be returned.
        """
        firebase = pyrebase.initialize_app(self.config)
        #authenticate a user
        user = firebase.auth().sign_in_with_email_and_password(self.account, self.pwd)
        token = user['idToken']
        db = firebase.database()
        result = db.child(path).get(user['idToken'])
        return result.val()

    def persist(self, parent_path, key, data_jsonstr):
        """This function will persist data to firebase

        persist('Status', 'Accessed', datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y"))

        Args:
            parent_path (string): The firebase parent_path.
            key (string): The key to the data object.
            data_jsonstr(string): The json string representation of the data

        returns:
            (string) : It looks it will have the value "Something" when the operation
            is successful
        """
        firebase = pyrebase.initialize_app(self.config)
        #authenticate a user
        user = firebase.auth().sign_in_with_email_and_password(self.account, self.pwd)
        token = user['idToken']
        db = firebase.database()
        result = db.child(parent_path).child(key).set(data_jsonstr, user['idToken'])
        return result




    