import pandas as pd
import numpy as np
import logging
import urllib
import unicodedata
from sqlalchemy import create_engine

class Database(object):

    #__metaclass__ = Singleton

    def __init__(self, raw_odbc_cstr):
        self._cstr = raw_odbc_cstr

    def _db_engine(self):
        params = urllib.quote_plus(self._cstr)
        engine = create_engine(r"mssql+pyodbc:///?odbc_connect=%s" % params)
        return engine

    def query(self,query, key_col=None):

        def unicode_to_str(unicode_val): return unicodedata.normalize('NFKD', unicode_val).encode('ascii','ignore')

        df = pd.read_sql_query(query,self._db_engine())

        str_col_names = [unicode_to_str(c) for c in df.columns.tolist()]
        df_new = df.copy()
        df_new.columns = str_col_names

        if key_col is not None:
            df_new = df_new.set_index(key_col)
        logging.info("Executed query %s" % query)
        return df_new

