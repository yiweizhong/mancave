#%%
from __future__ import division
import datetime
import xlwings as xw
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.patches import Circle
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm
from pca import *
from matrices_comparer import *
np.set_printoptions(formatter={'float_kind':'{:f}'.format})


class Regime(object):
    def __init__(self, regime_id, regime_returns):
        self._regime_id = regime_id
        self._regime_returns = regime_returns
        self._regime_pca = PCA(regime_returns).calc()
    @property
    def id(self):
        return self._regime_id
    @property
    def returns(self):
        return self._regime_returns
    @property
    def pca(self):
        return self._regime_pca

########################################################################
########################################################################

def get_empty_regimes():
    return {'1_1_1':None,'1_1_2':None,'1_1_3':None,
            '1_2_1':None,'1_2_2':None,'1_2_3':None,
            '1_3_1':None,'1_3_2':None,'2_1_1':None,
            '2_1_2':None,'2_1_3':None,'2_2_1':None,
            '2_2_2':None,'2_2_3':None,'2_3_1':None,
            '2_3_2':None,'2_3_3':None,'3_1_2':None,
            '3_1_3':None,'3_2_1':None,'3_2_2':None,
            '3_2_3':None,'3_3_1':None,'3_3_2':None,
            '3_3_3':None}.copy()

########################################################################
########################################################################

def get_regime(return_history, identified_regimes):
    regime_combined = get_empty_regimes()
    for combination, regime_group in identified_regimes.groupby('Combination'):
        regime_return_data = None
        for i, row in regime_group.iterrows():
            regime_start_date = row['Start Date']
            regime_end_date = row['End Date']
            return_filtered = return_history.loc[(return_history.index >= regime_start_date) 
                                                & (return_history.index <= regime_end_date)]
            if regime_return_data is not None:
                regime_return_data = pd.concat([regime_return_data, return_filtered])
            else:
                regime_return_data = return_filtered
        regime_combined[combination] = Regime(combination, regime_return_data.copy())
    return regime_combined

########################################################################
########################################################################

def debug_print_indentified_regimes(identified_regimes):
    for k, r in identified_regimes.iteritems():
        print k
        print r

########################################################################
########################################################################

def print_regime_detail(regime_id, regime, country, output_wks, 
                        col_width, main_regime=True, starting_row=2, starting_col = 2):    
    row_offset = 0
    col_offset = 0

    if main_regime == False:
        starting_col = starting_col + col_width + 2
    
    output_anchor_range = output_wks.range((starting_row,starting_col))

    if (regime is None):
        output_anchor_range.value = "regime not available in {}".format(country)
    else:
        output_anchor_range.value = country

        row_offset = 5
        col_offset = 0
        output_anchor_range.offset(row_offset,col_offset).value = regime.pca.get_covariance()
        output_anchor_range.offset(row_offset,col_offset).value = "CovMatrix"

        row_offset = row_offset + len( regime.pca.get_covariance().index) + 1 + 1
        col_offset = col_offset
        output_anchor_range.offset(row_offset,col_offset).value = regime.pca.get_eigenval_as_tbl()
        output_anchor_range.offset(row_offset,col_offset).value = "Eigen Value"

        row_offset = row_offset + len( regime.pca.get_eigenval_as_tbl().index) + 1 + 1
        col_offset = col_offset
        output_anchor_range.offset(row_offset,col_offset).value = regime.pca.loadings
        output_anchor_range.offset(row_offset,col_offset).value = "Eigen Vector"
        output_anchor_range.offset(row_offset + 1,col_offset + 1).expand(mode='table').name =  "eigen_vector" + "_" + country + "_" + regime_id

        row_offset = row_offset + len( regime.pca.loadings.index) + 1 + 1 
        col_offset = col_offset
        output_anchor_range.offset(row_offset,col_offset).value = regime.pca.get_pcs()
        output_anchor_range.offset(row_offset,col_offset).value = "Principal component"
        output_anchor_range.offset(row_offset + 1,col_offset + 1).expand(mode='table').name =  "Principal_component" + "_" + country + "_" + regime_id


        row_offset = row_offset + len(regime.pca.get_pcs().index) + 1 + 1 
        col_offset = col_offset
        output_anchor_range.offset(row_offset,col_offset).value = regime.pca.get_centered_data()
        output_anchor_range.offset(row_offset,col_offset).value = "Combined Return (Centered)"

        end_col_num = output_anchor_range.offset(row_offset,col_offset).expand(mode='table').end('right').column

        return end_col_num

########################################################################
########################################################################

def print_comparer_details(title, s1s2s3_stats, output_wks, starting_row, starting_col):    
    output_anchor_range = output_wks.range((starting_row,starting_col))
    row_offset = 0
    col_offset = 0
    output_anchor_range.offset(row_offset,col_offset).value = s1s2s3_stats
    output_anchor_range.value = title
    end_col_num = output_anchor_range.expand(mode='table').end('right').column
    end_row_num = output_anchor_range.expand(mode='table').end('down').row
    return (end_row_num, end_col_num)
########################################################################
########################################################################


def get_fig(xlim = (0,1), ylim = (0, 1)):    
    fig = plt.figure(figsize=(10,10))
    gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
    ax = plt.subplot(gs[0])
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.xaxis.grid(True)
    ax.yaxis.grid(True)
    #ax.spines['left'].set_position('center')
    #ax.spines['bottom'].set_position('center')
    ax.xaxis.set_major_locator(MultipleLocator(xlim[1]/2))
    ax.yaxis.set_major_locator(MultipleLocator(ylim[1]/2))
    #ax.spines['right'].set_color('none')
    #ax.spines['top'].set_color('none')
    # Show ticks in the left and lower axes only
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.set_ylabel('S3_Shape_Contribution', fontsize=9)
    ax.yaxis.set_label_coords(-0.02, 0.18)
    ax.set_xlabel('S2_Orientation_Contribution', fontsize=9)
    ax.xaxis.set_label_coords(0.85, -0.022)
    return ax

########################################################################
########################################################################

def plot_comparer_s_stats(ax, s_stats, annotation):
    
    
    #s_stats is a series with 3 items: total, shape and orientation contribution
    #the chart x axis <- shape
    #the chart y axis <- orientation
    #dot size total

    dot = Circle((s_stats['S2_Orientation_Contribution'],s_stats['S3_Shape_Contribution']),
                 s_stats['S1_Total_Diff']/50,
                 color='blue')
    ax.add_artist(dot)
    ax.annotate(annotation, 
               (s_stats['S2_Orientation_Contribution'],s_stats['S3_Shape_Contribution']),
                xytext=(4, 4), xycoords='data', textcoords='offset points')


########################################################################
########################################################################


def output_regime_analysis( main_regime_country, other_regime_country, 
                            main_regimes, other_regimes, 
                            folder = r"C:\Dev\hangon_repo\CovMatrixComparison\output",
                            summary_plot_xlim = (0,1),  summary_plot_ylim = (0,1)):
    outputWkb = xw.Book()
    wksnames = get_empty_regimes().keys()
    wksnames.sort(reverse=True)

    col_width = 0

     #work out how many columns to move for comparing country's data

    for name in wksnames:
        current_wks = outputWkb.sheets.add(name=name)
        current_wks.clear()
        main_regime = main_regimes[name]
        other_regime = other_regimes[name]

        if main_regime is not None:
            col_width = len(main_regime.returns.columns)
        elif other_regime is not None:
            ol_width = len(other_regime.returns.columns)

        print_regime_detail(name, main_regime, main_regime_country, current_wks, col_width, main_regime=True)
        end_col = print_regime_detail(name, other_regime, other_regime_country, current_wks, col_width, main_regime=False)

        if (main_regime is not None and other_regime is not None):
            comparer = Matrices_Comparer(main_regime_country, main_regime.pca, other_regime_country, other_regime.pca)
            title = "{}_{} regime {} differences".format(main_regime_country, other_regime_country, name)
            print_comparer_details(title, comparer.s1s2s3_stats,current_wks, 5, end_col + 2)

        current_wks.autofit('c')

    summary_wks = outputWkb.sheets.add("Summary")

    summary_wks_starting_row = 5
    summary_wks_starting_col = 2
    end_row = -99
    end_col = -99

    wksnames.sort()

    ax = get_fig(summary_plot_xlim, summary_plot_ylim)

    for name in wksnames:
        main_regime = main_regimes[name]
        other_regime = other_regimes[name]

        if (main_regime is not None and other_regime is not None):
            comparer = Matrices_Comparer(main_regime_country, main_regime.pca, other_regime_country, other_regime.pca)
            title = "{}_{} regime {} differences".format(main_regime_country, other_regime_country, name)
            end_row, end_col = print_comparer_details(title, comparer.s1s2s3_stats, summary_wks, summary_wks_starting_row, summary_wks_starting_col)
            summary_wks_starting_row = end_row + 2
            plot_comparer_s_stats(ax, comparer.s1s2s3_stats['Total'], name)

        elif (main_regime is None):
            summary_wks.range((summary_wks_starting_row,summary_wks_starting_col)).value = "Comparison unavailable for {} due to missing regime in {}".format(name, main_regime_country)
            if (end_row == -99):
                summary_wks_starting_row = summary_wks_starting_row + 2
            else:
                summary_wks_starting_row = end_row + 2
                end_row = summary_wks_starting_row

        elif (other_regime is None):
            summary_wks.range((summary_wks_starting_row,summary_wks_starting_col)).value = "Comparison unavailable for {} due to missing regime in {}".format(name, other_regime_country)
            if (end_row == -99):
                    summary_wks_starting_row = summary_wks_starting_row + 2
            else:
                summary_wks_starting_row = end_row + 2
                end_row = summary_wks_starting_row
        
    summary_wks.autofit('c')

    plt.show()

    outputWkb.save("{}\RegimeAnalysisOutput_{}{}_{}.xlsx".format(folder,main_regime_country, other_regime_country, datetime.now().strftime("%Y%m%d_%M%S")))
    outputWkb.close()


########################################################################
########################################################################


#######################################
#
# Entry from here
#
#######################################

wb = xw.Book("Regime_Data.xlsx")


#%%

def run_au_us():

    #######################################
    # AU Regimes
    #######################################

    regime_wks = wb.sheets("AustraliaRegimeChange")
    AU_return_data = regime_wks.range("AU_RETURNS").options(pd.DataFrame, header=True).value.dropna()
    AU_identified_regimes = regime_wks.range("AU_REGIMES").options(pd.DataFrame, header=True).value
    AU_identified_regimes = AU_identified_regimes.dropna() #for AU regime 2_1_1 there is no end date, hence drop it for now 
    #print AU_identified_regimes

    ##%%
    #######################################
    # AU vs US Regimes             =======>
    #######################################

    US_cols = ["SPX Index","USGG2YR Index","USGG5YR Index",	"USGG10YR Index","LD18OAS Index","LD26OAS Index"]
    AU_US_col = ["AS51 Index","GACGB3 Index","ADSW5Q Curncy","GACGB10 Index","AU Cred 0_3",	"AU Cred 5_10"]
    AU_US_col_name = ["Equity",	"Rate_Short","Rate_Mid","Rate_Long","Credit_Short","Credit_Long"]


    # US
    regime_wks = wb.sheets("US_regimeSMA_Change")
    US_return_data = regime_wks.range("US_RETURNS").options(pd.DataFrame, header=True).value.dropna()
    US_return_data = US_return_data[US_cols]
    US_return_data.columns = AU_US_col_name
    US_identified_regimes = regime_wks.range("US_REGIMES").options(pd.DataFrame, header=True).value
    US_regimes = get_regime(US_return_data, US_identified_regimes)
    #print us_regimes['2_3_2'].pca.loadings.iloc[:,:]
    ##print US_return_data.head()
    #debug_print_indentified_regimes(US_regimes)


    # AU_US
    AU_US_return_data = AU_return_data[AU_US_col]
    AU_US_return_data.columns = AU_US_col_name
    AU_US_regimes = get_regime(AU_US_return_data, AU_identified_regimes)
    #print AU_US_return_data.head()
    #debug_print_indentified_regimes(AU_US_regimes)


    output_regime_analysis( main_regime_country = 'AU', other_regime_country = 'US', 
                                main_regimes = AU_US_regimes, other_regimes = US_regimes, 
                                folder = r"C:\Dev\hangon_repo\CovMatrixComparison\output",
                                summary_plot_xlim = (0, 1), summary_plot_ylim = (0, 1))

run_au_us()

#######################################
#AU vs US Regimes              <=======
#######################################

#%%

def run_au_eu():


    #######################################
    # AU Regimes
    #######################################

    regime_wks = wb.sheets("AustraliaRegimeChange")
    AU_return_data = regime_wks.range("AU_RETURNS").options(pd.DataFrame, header=True).value.dropna()
    AU_identified_regimes = regime_wks.range("AU_REGIMES").options(pd.DataFrame, header=True).value
    AU_identified_regimes = AU_identified_regimes.dropna() #for AU regime 2_1_1 there is no end date, hence drop it for now 
    #print AU_identified_regimes

    #######################################
    # AU vs EU Regimes             =======>
    #######################################

    EU_cols = ['SX5E Index','GDBR2 Index','GDBR5 Index','GDBR10 Index','LEC1OAS Index','LEC3OAS Index','LEC7OAS Index']
    AU_EU_col = ['AS51 Index','GACGB3 Index','ADSW5Q Curncy','GACGB10 Index','AU Cred 0_3','AU Cred 3_5','AU Cred 5_10']
    AU_EU_col_name = ['Equity','Rate_Short','Rate_Mid','Rate_Long','Credit_Short','Credit_Mid','Credit_Long']


    # EU
    regime_wks = wb.sheets("EuropeRegimeChange")
    EU_return_data = regime_wks.range("EU_RETURNS").options(pd.DataFrame, header=True).value.dropna()
    EU_return_data = EU_return_data[EU_cols]
    EU_return_data.columns = AU_EU_col_name
    EU_identified_regimes = regime_wks.range("EU_REGIMES").options(pd.DataFrame, header=True).value
    EU_regimes = get_regime(EU_return_data, EU_identified_regimes)


    # AU_EU
    AU_EU_return_data = AU_return_data[AU_EU_col]
    AU_EU_return_data.columns = AU_EU_col_name
    AU_EU_regimes = get_regime(AU_EU_return_data, AU_identified_regimes)

    output_regime_analysis( main_regime_country = 'AU', other_regime_country = 'EU', 
                                main_regimes = AU_EU_regimes, other_regimes = EU_regimes, 
                                folder = r"C:\Dev\hangon_repo\CovMatrixComparison\output",
                                summary_plot_xlim = (0, 1), summary_plot_ylim = (0, 1))

    #######################################
    #AU vs EU Regimes              <=======
    #######################################

run_au_eu()



#%%
def run_au_jp():
    

    #######################################
    # AU Regimes
    #######################################

    regime_wks = wb.sheets("AustraliaRegimeChange")
    AU_return_data = regime_wks.range("AU_RETURNS").options(pd.DataFrame, header=True).value.dropna()
    AU_identified_regimes = regime_wks.range("AU_REGIMES").options(pd.DataFrame, header=True).value
    AU_identified_regimes = AU_identified_regimes.dropna() #for AU regime 2_1_1 there is no end date, hence drop it for now 
    #print AU_identified_regimes

    #######################################
    # AU vs JP Regimes             =======>
    #######################################

    JP_cols = ['NKY Index','GJGB2 Index','GJGB5 Index','GJGB10 Index','Japan Credit']
    AU_JP_cols = ['AS51 Index','GACGB3 Index','ADSW5Q Curncy','GACGB10 Index','AU Cred 0_3']
    AU_JP_col_names = ['Equity','Rate_Short','Rate_Mid','Rate_Long','Credit_Short']


    # JP
    regime_wks = wb.sheets("JapanRegimeChange")
    JP_return_data = regime_wks.range("JP_RETURNS").options(pd.DataFrame, header=True).value.dropna()
    JP_return_data = JP_return_data[JP_cols]
    JP_return_data.columns = AU_JP_col_names
    JP_identified_regimes = regime_wks.range("JP_REGIMES").options(pd.DataFrame, header=True).value
    JP_regimes = get_regime(JP_return_data, JP_identified_regimes)


    # AU_JP
    AU_JP_return_data = AU_return_data[AU_JP_cols]
    AU_JP_return_data.columns = AU_JP_col_names
    AU_JP_regimes = get_regime(AU_JP_return_data, AU_identified_regimes)

    output_regime_analysis( main_regime_country = 'AU', other_regime_country = 'JP', 
                                main_regimes = AU_JP_regimes, other_regimes = JP_regimes, 
                                folder = r"C:\Dev\hangon_repo\CovMatrixComparison\output",
                                summary_plot_xlim = (0, 0.2), summary_plot_ylim = (0, 0.2))

    #######################################
    #AU vs JP Regimes              <=======
    #######################################

run_au_jp()





#%%


#main_regime_country = 'AU'
#other_regime_country = 'US'
#main_regimes = AU_US_regimes
#other_regimes = US_regimes
#comparer1 = Matrices_Comparer(main_regime_country, main_regimes['3_2_3'].pca, other_regime_country, other_regimes['3_2_3'].pca)
#comparer2 = Matrices_Comparer(other_regime_country, other_regimes['3_2_3'].pca, main_regime_country, main_regimes['3_2_3'].pca)


#print comparer._eigvecs1
#print comparer._eigenval_raw1
#print comparer.loadings1
#print comparer.loadings2

#print comparer.pcs11_as_pct_of_total_pca1_variance
#print comparer.pcs11_as_pct_of_total_pca1_variance.sum()

#print comparer.pcs12_as_pct_of_total_pca1_variance
#print comparer.pcs12_as_pct_of_total_pca1_variance.sum()

#print comparer.pcs22_as_pct_of_total_pca2_variance
#print comparer.pcs22_as_pct_of_total_pca2_variance.sum()

#print comparer.pcs21_as_pct_of_total_pca2_variance
#print comparer.pcs21_as_pct_of_total_pca2_variance.sum()

#print comparer.diff_diagonal
#print comparer.diff_diagonal.sum()

#print comparer.diff_horizontal
#print comparer.diff_horizontal.sum()

#print comparer1.s1s2s3_stats
#print comparer2.s1s2s3_stats

