from __future__ import division
import pandas as pd
import numpy as np
from datetime import datetime


class Matrices_Comparer(object):
    """A data structure holds and compares 2 matrices object
    """
    def __init__(self, name1, pca1, name2, pca2):
        self._name1 = name1
        self._pca1 = pca1
        self._name2 = name2
        self._pca2 = pca2

        self._eigenval_raw1 = pca1.get_eigenval_as_tbl().iloc[0]
        self._eigenval_raw2 = pca2.get_eigenval_as_tbl().iloc[0]
        self._eigenval_variance_total1 = self._eigenval_raw1.sum()
        self._eigenval_variance_total2 = self._eigenval_raw2.sum() 

        self._centereddata1 = pca1.centereddata
        self._centereddata2 = pca2.centereddata

        self._eigvecs1 = pca1._eigvecs
        self._eigvecs2 = pca2._eigvecs


        self._pcs11 =  self._centereddata1.dot(self._eigvecs1)
        self._pcs11.columns = ["PC" + str(i + 1) for i in range(len(self._centereddata1.columns.tolist()))]
   
        self._pcs22 =  self._centereddata2.dot(self._eigvecs2)
        self._pcs22.columns = ["PC" + str(i + 1) for i in range(len(self._centereddata2.columns.tolist()))]

        self._pcs12 =  self._centereddata1.dot(self._eigvecs2)
        self._pcs12.columns = ["PC" + str(i + 1) for i in range(len(self._centereddata1.columns.tolist()))]

        self._pcs21 =  self._centereddata2.dot(self._eigvecs1)
        self._pcs21.columns = ["PC" + str(i + 1) for i in range(len(self._centereddata2.columns.tolist()))]



    @staticmethod
    def get_variance_explained(pcs, total_variance):
        return pcs.apply(lambda col: np.var(col,ddof=1), axis = 0)/total_variance
    
    
    @property
    def s1s2s3_stats(self):
        s1_for_pcs = (self.diff_in_1.apply(lambda x: x*x) + self.diff_in_2.apply(lambda x: x*x))/4
        s1_for_pcs['Total'] = s1_for_pcs.sum()

        s2_for_pcs = self.diff_diagonal.apply(lambda x: x*x)/8
        s2_for_pcs['Total'] = s2_for_pcs.sum()

        s3_for_pcs = self.diff_horizontal.apply(lambda x: x*x)/8
        s3_for_pcs['Total'] = s3_for_pcs.sum()

        df = pd.DataFrame(columns=s1_for_pcs.index, index=['S1_Total_Diff', 'S2_Orientation_Contribution','S3_Shape_Contribution'])
        df.name = "Matrices Comparison Stats"
        df.loc['S1_Total_Diff'] = s1_for_pcs
        df.loc['S2_Orientation_Contribution'] = s2_for_pcs
        df.loc['S3_Shape_Contribution'] = s3_for_pcs

        return df

    @property
    def pcs11(self): 
        return self._pcs11

    @property
    def pcs22(self): 
        return self._pcs22

    @property
    def pcs12(self): 
        return self._pcs12

    @property
    def pcs21(self): 
        return self._pcs21

    @property
    def pcs11_as_pct_of_total_pca1_variance(self): 
        return self.get_variance_explained(self._pcs11,self._eigenval_variance_total1)

    @property
    def pcs12_as_pct_of_total_pca1_variance(self): 
        return self.get_variance_explained(self._pcs12,self._eigenval_variance_total1)

    @property
    def pcs22_as_pct_of_total_pca2_variance(self): 
        return self.get_variance_explained(self._pcs22,self._eigenval_variance_total2)

    @property
    def pcs21_as_pct_of_total_pca2_variance(self): 
        return self.get_variance_explained(self._pcs21,self._eigenval_variance_total2)

    @property
    def diff_in_1(self):
        return  self.pcs21_as_pct_of_total_pca2_variance - self.pcs11_as_pct_of_total_pca1_variance

    @property
    def diff_in_2(self):
        return  self.pcs12_as_pct_of_total_pca1_variance - self.pcs22_as_pct_of_total_pca2_variance

    @property
    def diff_horizontal(self):
        return (self.pcs11_as_pct_of_total_pca1_variance + self.pcs12_as_pct_of_total_pca1_variance) - \
               (self.pcs21_as_pct_of_total_pca2_variance + self.pcs22_as_pct_of_total_pca2_variance)

    @property
    def diff_diagonal(self):
        return (self.pcs11_as_pct_of_total_pca1_variance + self.pcs22_as_pct_of_total_pca2_variance) - \
               (self.pcs21_as_pct_of_total_pca2_variance + self.pcs12_as_pct_of_total_pca1_variance)


    @property
    def loadings1(self):
        loadings = {"Loading" + str(i + 1): pd.Series(self._eigvecs1[:, i], index=self._centereddata1.columns.tolist())
                    for i in range(len(self._centereddata1.columns.tolist()))}
        colnames = ["Loading" + str(i + 1) for i in range(len(self._centereddata1.columns.tolist()))]
        return pd.DataFrame(loadings)[colnames]

    @property
    def loadings2(self):
        loadings = {"Loading" + str(i + 1): pd.Series(self._eigvecs2[:, i], index=self._centereddata2.columns.tolist())
                    for i in range(len(self._centereddata2.columns.tolist()))}
        colnames = ["Loading" + str(i + 1) for i in range(len(self._centereddata2.columns.tolist()))]
        return pd.DataFrame(loadings)[colnames]


