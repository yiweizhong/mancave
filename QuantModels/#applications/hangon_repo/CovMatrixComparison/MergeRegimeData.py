
### Line up the data
# lets start from 2002-03-20 the start of AUS history
# (done) linear interpolation for Credit Mid for US using US Credit Short and US Credit Long. 
#        Also applies the fly shapes implied from US rates curve
# (done) The only japanese credit series is used represent short/mid/long credit. needs to find out from steve
#        what exactly is this series and adjust accordingly

### merge the data
# Concat all the returns from all countries together and check the length of data for each regime for reweight 
# equal the weighting for each regime (vector you can change)

### generate cov matrix
#%%
from __future__ import division
import datetime
import xlwings as xw
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.patches import Circle
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm
from pca import *
from matrices_comparer import *
np.set_printoptions(formatter={'float_kind':'{:f}'.format})

##################
# regime class
##################
class Regime(object):
    def __init__(self, regime_id, regime_returns):
        self._regime_id = regime_id
        self._regime_returns = regime_returns
        #self._regime_pca = PCA(regime_returns).calc()
        self._regime_returns_tagged = regime_returns
    
    def add_regime_tagged(self,  tagged_returns):
        self._regime_returns_tagged = tagged_returns

    @property
    def id(self):
        return self._regime_id
    @property
    def returns(self):
        return self._regime_returns
    @property
    def regime_returns_tagged(self):
        return self._regime_returns_tagged

    @regime_returns_tagged.setter
    def regime_returns_tagged(self, value):
        self._regime_returns_tagged = value

    @property
    def pca(self):
        return self._regime_pca

##################


 
##################
def get_empty_regimes():
    return {'1_1_1':None,'1_1_2':None,'1_1_3':None,
            '1_2_1':None,'1_2_2':None,'1_2_3':None,
            '1_3_1':None,'1_3_2':None,'2_1_1':None,
            '2_1_2':None,'2_1_3':None,'2_2_1':None,
            '2_2_2':None,'2_2_3':None,'2_3_1':None,
            '2_3_2':None,'2_3_3':None,'3_1_2':None,
            '3_1_3':None,'3_2_1':None,'3_2_2':None,
            '3_2_3':None,'3_3_1':None,'3_3_2':None,
            '3_3_3':None}.copy()
##################

########################################################################
def debug_print_indentified_regimes(identified_regimes):
    for k, r in identified_regimes.iteritems():
        print k
        print r
########################################################################


########################################################################
def get_regime(return_history, identified_regimes, country = None):
    regime_combined = get_empty_regimes()
    for combination, regime_group in identified_regimes.groupby('Combination'):
        regime_return_data = None
        for i, row in regime_group.iterrows():
            regime_start_date = row['Start Date']
            regime_end_date = row['End Date']
            return_filtered = return_history.loc[(return_history.index >= regime_start_date) 
                                                & (return_history.index <= regime_end_date)]
            if regime_return_data is not None:
                regime_return_data = pd.concat([regime_return_data, return_filtered])
            else:
                regime_return_data = return_filtered
        
        output_regime_return_data = regime_return_data.copy()
        regime = Regime(combination, output_regime_return_data)

        output_regime_return_data['Combination'] = [combination] * len(output_regime_return_data.index)
        regime.regime_returns_tagged = output_regime_return_data     


        if country is not None:
            output_regime_return_data['Country'] = [country] * len(output_regime_return_data.index)
            regime.regime_returns_tagged = output_regime_return_data     

        
        regime_combined[combination] = regime

    return regime_combined
########################################################################



#%%
print "================= Main ====================="

wb = xw.Book("Regime_Data_For_CovMatrix_Merge.xlsx")
print "opened input data"

regime_wks = wb.sheets("AustraliaRegimeChange")
return_data = regime_wks.range("AU_RETURNS").options(pd.DataFrame, header=True).value.dropna()
identified_regimes = regime_wks.range("AU_REGIMES").options(pd.DataFrame, header=True).value
AU_regime_returns =get_regime(return_data, identified_regimes, country = 'AU')
AU_cols = list(AU_regime_returns['2_3_2'].returns.columns)
#debug_print_indentified_regimes(AU_regime_returns)
print "loaded AU"

regime_wks = wb.sheets("US_regimeSMA_Change")
return_data = regime_wks.range("US_RETURNS").options(pd.DataFrame, header=True).value.dropna()
identified_regimes = regime_wks.range("US_REGIMES").options(pd.DataFrame, header=True).value
US_regime_returns =get_regime(return_data, identified_regimes, country = 'US')
US_cols = list(US_regime_returns['2_3_2'].returns.columns)
#debug_print_indentified_regimes(US_regime_returns)
print "loaded US"

regime_wks = wb.sheets("EuropeRegimeChange")
return_data = regime_wks.range("EU_RETURNS").options(pd.DataFrame, header=True).value.dropna()
identified_regimes = regime_wks.range("EU_REGIMES").options(pd.DataFrame, header=True).value
EU_regime_returns =get_regime(return_data, identified_regimes, country = 'EU')
EU_cols = list(EU_regime_returns['2_3_2'].returns.columns)
#debug_print_indentified_regimes(EU_regime_returns)
print "loaded EU"

regime_wks = wb.sheets("JapanRegimeChange")
return_data = regime_wks.range("JP_RETURNS").options(pd.DataFrame, header=True).value.dropna()
identified_regimes = regime_wks.range("JP_REGIMES").options(pd.DataFrame, header=True).value
JP_regime_returns =get_regime(return_data, identified_regimes, country = 'JP')
JP_cols = list(JP_regime_returns['2_3_2'].returns.columns)
#debug_print_indentified_regimes(JP_regime_returns)
print "loaded JP"




#%%
#print total_combined_returns['index']


#%%

regimes = get_empty_regimes()
total_combined_returns = None
def add_regimes_return(totalrtn, rtn):
    if totalrtn is None:
        totalrtn = rtn.copy()
    else:
        totalrtn = pd.concat([totalrtn, rtn])
    return totalrtn

for name in regimes:

    if JP_regime_returns[name] is not None:
        total_combined_returns = add_regimes_return(total_combined_returns, JP_regime_returns[name].regime_returns_tagged)
    if AU_regime_returns[name] is not None:
        total_combined_returns = add_regimes_return(total_combined_returns, AU_regime_returns[name].regime_returns_tagged)
    if US_regime_returns[name] is not None:
        total_combined_returns = add_regimes_return(total_combined_returns, US_regime_returns[name].regime_returns_tagged)
    if EU_regime_returns[name] is not None:
        total_combined_returns = add_regimes_return(total_combined_returns, EU_regime_returns[name].regime_returns_tagged)

total_combined_returns = total_combined_returns.reset_index()

total_combined_returns = total_combined_returns.drop('index',1) 

total_row = len(total_combined_returns.index)

def pct(x): return np.count_nonzero(x)/total_row        
grp = total_combined_returns.groupby(['Combination', 'Country'])
grp_lvl2_df = grp['Equity'].agg(pct).unstack(level=-1)

grp = total_combined_returns.groupby(['Combination'])
grp_lvl1_series = grp['Equity'].agg(pct)

grp_lvl2_df['historical_weight'] = grp_lvl1_series 
equal_weight_regime= 1 / len(grp_lvl2_df.index)
grp_lvl2_df['reweighting_factor'] = (equal_weight_regime / grp_lvl1_series * 100).astype(int)

reweighting_frame =  grp_lvl2_df.copy().drop(['AU', 'US', 'EU', 'JP', 'historical_weight'], 1).unstack().dropna() \
                 .reset_index().drop('Country', axis=1)

reweighting_frame.rename(columns={0:'reweighting_factor'}, inplace=True)


#%%

combinations = total_combined_returns.dropna()
combinations_with_weights = pd.merge(combinations, reweighting_frame, on='Combination', how='outer')
weights = combinations_with_weights['reweighting_factor']

cols = ['Equity', 'Rate_Short', 'Rate_Mid','Rate_Long', 'Rate_Longer', 'Credit_Short', 
        'Credit_Mid','Credit_Long','Semi_Short','Semi_Mid', 'Semi_Long']
print total_combined_returns[cols].head()
merged_cov = np.cov(total_combined_returns[cols], rowvar=False,fweights=weights)
merged_cov_df = pd.DataFrame(data=merged_cov,index=cols, columns=cols)



#%%
path = r"C:\Dev\hangon_repo\CovMatrixComparison\output\MergedRegimes.xlsx"
outputWkb = xw.Book()
summary_wks = outputWkb.sheets.add(name="Summary")
output_anchor_range = summary_wks.range((1,1))
output_anchor_range.value = grp_lvl2_df 
output_anchor_range.offset(0,10).value = merged_cov_df

data_wks = outputWkb.sheets.add(name="Data")
output_anchor_range = data_wks.range((1,1))
output_anchor_range.value = combinations_with_weights












#%%
print JP_regime_returns['1_1_2'].returns['Semi_Short']

