"""
unitTest
"""
from __future__ import division
import numpy as np
import pandas as pd
import unittest
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm
import MergeRegimeData as mrd

    
class MergeRegimeDataTest(unittest.TestCase):
    """The main test file for mean reversion package
    """
    def setUp(self):
        """
        """
        self.date_idx = "DATE"
        plt.ion()

    @unittest.skip('')
    def test_env_setup(self):
        """make sure testenv is ready"""
        self.assertEqual(1, 1)


    def test_linear_interpolation(self):
        print mrd.linear_interpolation()        
        
        

