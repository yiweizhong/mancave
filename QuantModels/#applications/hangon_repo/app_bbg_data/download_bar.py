from __future__ import division
import test_path
import bar_data_config as cfg
import datetime
import os, sys, inspect, time
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from collections import Counter
import csv
from tia.bbg import LocalTerminal as bbgterminal
import tools.common.dirs as dirs
import tools.database.firedb as fdb
import statistics.data.extension as ext
from StringIO import StringIO


def setup():
    """Log settings"""
    _input_folder = dirs.get_creat_sub_folder(folder_name="Input", parent_dir=cfg.APP_ROOT)
    _log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=cfg.APP_ROOT)
    _debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=cfg.APP_ROOT)
    _output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=cfg.APP_ROOT)
    _output_folder = dirs.get_creat_sub_folder(folder_name="Bar", parent_dir=_output_folder)

    _archive_folder = dirs.get_creat_sub_folder(folder_name="Archive", parent_dir=cfg.APP_ROOT)
    _archive_input_folder = dirs.get_creat_sub_folder(folder_name="Input", parent_dir=_archive_folder)
    _archive_log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=_archive_folder)
    _archive_debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=_archive_folder)
    _archive_output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=_archive_folder)
    _archive_output_folder = dirs.get_creat_sub_folder(folder_name="Bar", parent_dir=_archive_output_folder)

    _log_file_name = dirs.get_log_file_name(logfile_name_prefix=cfg.APP_NAME,log_folder=_log_folder)

    return _log_folder, _debug_folder, _output_folder, _archive_folder, \
           _log_file_name, _archive_log_folder, _archive_debug_folder, \
           _archive_output_folder, _input_folder, _archive_input_folder

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':

    _log_folder, _debug_folder, _output_folder, _archive_folder, \
    _log_file_name, _archive_log_folder, _archive_debug_folder, \
    _archive_output_folder, _input_folder, _archive_input_folder = setup()
    _exit_code = 0

    try:
        logging.basicConfig(filename=_log_file_name, format=cfg.LOG_FORMAT, level=logging.INFO)
        logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))

        dirs.archive_files(_output_folder, _archive_output_folder)

        now = pd.datetime.now()
        today = now.strftime('%Y-%m-%d')
        today_UTC = now.utcnow()
        logging.info("Now <{}>, UTC <{}>".format(today,today_UTC))
        end = today_UTC
        start = today_UTC + pd.DateOffset(hours= -24 * cfg.START_DATE_OFFSET)
        logging.info("data period: <{}> to <{}>".format(start, end))

        for file in os.listdir(_input_folder):
            if file.endswith(cfg.FILE_SUFFIX):
                try:
                    logging.info(file)

                    fileName = file.replace(cfg.FILE_SUFFIX,'')

                    with open(os.path.join(_input_folder, file), 'r') as myfile:
                        content = list(csv.reader(myfile))

                        for line in content:
                            if (len(line) == 2 and line[0].strip() != "" and line[0].strip()[0] != "#"):
                                ticker  = line[0].strip()
                                name = line[1].strip()
                                logging.info("Download bar data for <{}>".format(ticker))
                                if (len(ticker)>0):
                                    try:
                                        bar_data = bbgterminal.get_intraday_bar(ticker, cfg.EVENT, start, end, interval = cfg.BAR_INTERVAL).as_frame()
                                        if "time" in bar_data.columns:
                                            bar_data = bar_data.set_index("time")
                                        else:
                                            bar_data = pd.DataFrame(columns=["time","close","high","low","numEvents","open","value","volume"])
                                        
                                        bar_data.to_csv(os.path.join(_output_folder, today + "_" + name + "_" + file ))

                                        #save to database
                                        try:
                                            fire_database = fdb.FireDatabase()
                                            if name in cfg.SAVE_TO_FDB:
                                                logging.info("Save {} to firedb".format(name))
                                                if fileName in cfg.OVERRIDE_EXISTING_DATA:                                                    
                                                    logging.info("Replace the existing data")
                                                    s = StringIO()
                                                    bar_data.to_csv(s)
                                                    fire_database.persist(cfg.FDB_ROOT, name, s.getvalue())
                                                else:
                                                    existing_data = fire_database.retrieve(cfg.FDB_ROOT + "/" + name + "/")
                                                    #logging.info(existing_data)
                                                    if existing_data != None:
                                                        logging.info("Append to the existing data node")
                                                        #existing_data = pd.read_json(existing_data)
                                                        existing_data = pd.read_csv(StringIO(existing_data),sep=',')
                                                        existing_data = existing_data.set_index("time")
                                                        existing_data.index = pd.to_datetime(existing_data.index)                                            
                                                        new_data = ext.merge_dataframess(existing_data, bar_data)
                                                        #logging.info(new_data)
                                                        s = StringIO()
                                                        new_data.to_csv(s)
                                                        fire_database.persist(cfg.FDB_ROOT, name, s.getvalue())
                                                    else:                                                                                            
                                                        logging.info("Create new data node")
                                                        s = StringIO()
                                                        bar_data.to_csv(s)
                                                        fire_database.persist(cfg.FDB_ROOT, name, s.getvalue())
                                        except:
                                            logging.exception("!!! Error detected when updating the firedb for {0}. This file is skipped.".format(name))            
                                        
                                    except:
                                        logging.exception("!!! Error detected when trying to process <{}>. from file <{}> is skipped.".format(ticker, file))
                                        _exit_code = 1                           
                except:
                    logging.exception("!!! Error detected when trying to process <{}>. This file is skipped.".format(file))
                    _exit_code = 1
        if _exit_code == 0:
            logging.info("<<<< <{}> Ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))

    except:
        logging.exception("!!! Error detected in <{}>".format(cfg.APP_NAME))
        logging.info("<<<< <{}> Ends UNSUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 1
    finally:
        sys.exit(_exit_code)

