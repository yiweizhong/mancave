echo off
SET localPath=%~dp0%
echo %localPath%
pushd %localPath%
python.exe download_reference.py
if errorlevel 1 (
   echo Process failed with code %errorlevel%
   exit /b %errorlevel%
)

popd
echo on
pause