"""path"""
import os
import sys
import inspect


def add_path(p):
    if p not in sys.path:
        print "adding {}".format(p)
        sys.path.insert(0, p)

def config_path():
    """Config the path"""
    path = os.path.split(inspect.getfile(inspect.currentframe()))[0]
    path = os.path.realpath(os.path.abspath(path))
    parent_path = os.path.abspath(os.path.join(path, os.pardir))

    database_lib_path = os.path.abspath(os.path.join(parent_path, "tools"))
    add_path(database_lib_path)

    statistics_lib_path = os.path.abspath(os.path.join(parent_path, "statistics"))
    add_path(statistics_lib_path)

    print "sys.path:"
    for p in sys.path:
        print p

config_path()