"""Config for BBG"""
APP_ROOT = r""
APP_NAME = r"historical_data"

DTYPE_DATEIME64 = r"datetime64[ns]"
ODBC_STYLE_CSTR_PREFIX = r"mssql+pyodbc:///?odbc_connect=%s"
LOG_FORMAT = r"%(asctime)s - %(name)s - %(levelname)s - %(message)s"
TIMESTAMP_FMT = r"%Y-%m-%d %H:%M:%S.%f"

TICKER_FILE_FILED = {
	"SPOT_INTRADAY_RVOL10BD_RVOL180BD_historical.csv":"VOLATILITY_180D",
	"SPOT_RVOL180BD_historical.csv":"VOLATILITY_180D",
	"SPOT_RVOL60BD_historical.csv":"VOLATILITY_60D",
	"SPOT_RVOL10BD_historical.csv":"VOLATILITY_10D"
}
START_DATE = '2011-01-01'
START_DATE_OFFSET = 1600

FILE_SUFFIX = "_historical.csv"

#if there is any data object will need to be restart from fresh in firebase, specify the name here
FDB_ROOT = "CUSTOMDATA"
SAVE_TO_FDB = ['AUD', 'Flies', 'STIRFUT']
OVERRIDE_EXISTING_DATA = []
