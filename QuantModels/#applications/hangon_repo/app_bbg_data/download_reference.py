from __future__ import division
import test_path
import reference_data_config as cfg
import datetime
import os, sys, inspect, time
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from collections import Counter
import csv
from tia.bbg import LocalTerminal as bbgterminal
import tools.common.dirs as dirs
import tools.database.firedb as fdb
import statistics.data.extension as ext
from StringIO import StringIO




def setup():
    """Log settings"""
    _input_folder = dirs.get_creat_sub_folder(folder_name="Input", parent_dir=cfg.APP_ROOT)
    _log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=cfg.APP_ROOT)
    _debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=cfg.APP_ROOT)
    _output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=cfg.APP_ROOT)
    _output_folder = dirs.get_creat_sub_folder(folder_name="Reference", parent_dir=_output_folder)

    _archive_folder = dirs.get_creat_sub_folder(folder_name="Archive", parent_dir=cfg.APP_ROOT)	
    _archive_input_folder = dirs.get_creat_sub_folder(folder_name="Input", parent_dir=_archive_folder)
    _archive_log_folder = dirs.get_creat_sub_folder(folder_name="Log", parent_dir=_archive_folder)
    _archive_debug_folder = dirs.get_creat_sub_folder(folder_name="Debug", parent_dir=_archive_folder)
    _archive_output_folder = dirs.get_creat_sub_folder(folder_name="Output", parent_dir=_archive_folder)
    _archive_output_folder = dirs.get_creat_sub_folder(folder_name="Reference", parent_dir=_archive_output_folder)

    _log_file_name = dirs.get_log_file_name(logfile_name_prefix=cfg.APP_NAME,log_folder=_log_folder)

    return _log_folder, _debug_folder, _output_folder, _archive_folder, \
           _log_file_name, _archive_log_folder, _archive_debug_folder, \
           _archive_output_folder, _input_folder, _archive_input_folder



#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-- Main
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':

    _log_folder, _debug_folder, _output_folder, _archive_folder, \
    _log_file_name, _archive_log_folder, _archive_debug_folder, \
    _archive_output_folder, _input_folder, _archive_input_folder = setup()
    _exit_code = 0

    try:
        logging.basicConfig(filename=_log_file_name, format=cfg.LOG_FORMAT, level=logging.INFO)
        logging.info("<<<< {0} starts >>>>".format(cfg.APP_NAME))

        dirs.archive_files(_output_folder, _archive_output_folder)

        today = datetime.datetime.today().strftime('%Y-%m-%d')
        logging.info(today)
        
        #START_DATE = (datetime.datetime.today() + datetime.timedelta(days= -1 * cfg.START_DATE_OFFSET)).strftime('%Y-%m-%d')

        for file in os.listdir(_input_folder):
            if file.endswith(cfg.FILE_SUFFIX):
                try:
                    logging.info(file)

                    fileName = file.replace(cfg.FILE_SUFFIX,'')

                    with open(os.path.join(_input_folder, file), 'r') as myfile:
                        #content = myfile.read().replace('\n', ',')
                        tickers = []
                        fields = []
                        names = {}
                        content = list(csv.reader(myfile))

                        for line in content:
                            if (len(line) == 3 and 
                                (line[0].strip() != "" and line[0].strip()[0] != "#") and  #make sure the ticker and fields are not blank
                                (line[1].strip() != "" and line[1].strip()[0] != "#")):
                                
                                if line[0].strip() not in tickers:
                                    tickers.append(line[0].strip())
                                if line[1].strip() not in fields:
                                    fields.append(line[1].strip())

                                #rename the ticker to shorter name    
                                if (line[0].strip() != line[2].strip()):
                                    names[line[0].strip()] = line[2].strip()


                        
                        logging.info("{0} securities: ".format(len(tickers)))
                        logging.info(tickers)

                        logging.info("{0} securities: ".format(len(fields)))
                        logging.info(fields)

                        if(len(tickers) > 0 and len(fields) > 0):

                            secdf = bbgterminal.get_reference_data(tickers, fields).as_frame()

                            #if "date" in secdf.columns:
                            #    secdf.columns = secdf.columns.levels[0]
                            #else:
                            #    secdf = pd.DataFrame(columns=["date"])                                        

                            #if (set(tickers) == set(secdf.columns)):
                            #   secdf = secdf[tickers]

                            #rename
                            renamed_idx = [ idx if not names.has_key(idx) else names[idx] for idx in list(secdf.index.values)]
                            print renamed_idx
                            secdf['security'] = renamed_idx                            
                            secdf = secdf.reset_index(drop=True).set_index('security')

                            
                            secdf.to_csv(os.path.join(_output_folder, today + "_" +file ))

                            #save to database
                            """
                            try:
                                fire_database = fdb.FireDatabase()
                                if fileName in cfg.SAVE_TO_FDB:
                                    logging.info("Save to firedb")
                                    if fileName in cfg.OVERRIDE_EXISTING_DATA:
                                        #logging.info(secdf.to_json())                                  
                                        #fdb.persist(cfg.FDB_ROOT, fileName, secdf.to_json())
                                        logging.info("Replace the existing data")
                                        s = StringIO()
                                        secdf.to_csv(s)
                                        fire_database.persist(cfg.FDB_ROOT, fileName, s.getvalue())
                                    else:
                                        existing_data = fire_database.retrieve(cfg.FDB_ROOT + "/" + fileName + "/")
                                        #logging.info(existing_data)
                                        if existing_data != None:
                                            logging.info("Append to the existing data node")
                                            #existing_data = pd.read_json(existing_data)
                                            existing_data = pd.read_csv(StringIO(existing_data),sep=',')
                                            existing_data = existing_data.set_index("date")
                                            existing_data.index = pd.to_datetime(existing_data.index)                                            
                                            new_data = ext.merge_dataframess(existing_data, secdf)
                                            #logging.info(new_data)
                                            s = StringIO()
                                            new_data.to_csv(s)
                                            fire_database.persist(cfg.FDB_ROOT, fileName, s.getvalue())

                                        else:
                                            #fdb.persist(cfg.FDB_ROOT, fileName, secdf.to_json())                                    
                                            logging.info("Create new data node")
                                            s = StringIO()
                                            secdf.to_csv(s)
                                            fire_database.persist(cfg.FDB_ROOT, fileName, s.getvalue())
                            except:
                                logging.exception("!!! Error detected when updating the firedb for {0}. This file is skipped.".format(fileName))            
                            """

                        else:
                            logging.info("Skip empty file")
                except:
                    logging.exception("!!! Error detected when downloading {0}. This file is skipped.".format(file))
                    _exit_code = 1
        if _exit_code == 0:
            logging.info("<<<< {0} Ends SUCCESSFULLY >>>>".format(cfg.APP_NAME))
            #sys.exit(0)
        #sys.exit(1)
    except:
        logging.exception("!!! Error detected in {0}".format(cfg.APP_NAME))
        logging.info("<<<< {0} Ends UNSUCCESSFULLY >>>>".format(cfg.APP_NAME))
        _exit_code = 1
    finally:
        sys.exit(_exit_code)
