"""Config for BBG"""
APP_ROOT = r""
APP_NAME = r"reference_data"

DTYPE_DATEIME64 = r"datetime64[ns]"
ODBC_STYLE_CSTR_PREFIX = r"mssql+pyodbc:///?odbc_connect=%s"
LOG_FORMAT = r"%(asctime)s - %(name)s - %(levelname)s - %(message)s"
TIMESTAMP_FMT = r"%Y-%m-%d %H:%M:%S.%f"

TICKER_FILE_FILED = {}

FILE_SUFFIX = "_reference.csv"

#if there is any data object will need to be restart from fresh in firebase, specify the name here
FDB_ROOT = "CUSTOMDATA"
SAVE_TO_FDB = []
OVERRIDE_EXISTING_DATA = []
