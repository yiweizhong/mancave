"""Config for BBG"""
APP_ROOT = r""
APP_NAME = r"bar_data"

DTYPE_DATEIME64 = r"datetime64[ns]"
ODBC_STYLE_CSTR_PREFIX = r"mssql+pyodbc:///?odbc_connect=%s"
LOG_FORMAT = r"%(asctime)s - %(name)s - %(levelname)s - %(message)s"
TIMESTAMP_FMT = r"%Y-%m-%d %H:%M:%S.%f"

START_DATE_OFFSET = 30
FILE_SUFFIX = "_bar.csv"
EVENT = "TRADE"
BAR_INTERVAL = 1


#if there is any data object will need to be restart from fresh in firebase, specify the name here
FDB_ROOT = "BARDATA"
SAVE_TO_FDB = []
OVERRIDE_EXISTING_DATA = []
