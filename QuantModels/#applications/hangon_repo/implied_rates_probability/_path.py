import os, sys, inspect
import logging
import datetime
_path = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
_parent_path = os.path.abspath(os.path.join(_path, os.pardir))
_grand_parent_path = os.path.abspath(os.path.join(_parent_path, os.pardir))
_app = "ImpliedRatesProbability"
_log_path = os.path.join(_path, 'log\\' + _app + '_' + datetime.datetime.now().strftime("%Y%m%d_%H")+'.log')
