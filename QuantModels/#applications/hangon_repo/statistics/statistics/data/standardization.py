""" A Module provides standardization methonds
"""
from __future__ import division
import numpy as np
import pandas as pd
import logging
import datetime as dt
import collections
from func import each as f_each
from extension import *
from pandas.tseries.offsets import *
from statsmodels.tools.tools import ECDF


def rolling_standardise(data,
                        window_size=1,
                        min_window_size=1,
                        fill_missing_method=r"ffill",
                        start=None,
                        end=None,
                        bday_only=True,
                        fill_missing_at_the_end=False,
                        is_df=True):
    """
    Rolling_standardise a dataframe.

    This function makes sure if a time series ends earlier than other series in the dataframe it will not forward fill the last available values to the end
    of dataframe index. This is important as there are times we lost a a data series due to various reasons. if the last available value is forward filled.
    it will create a zscore decay and dilute the combined data.

    Args:

        df (pandas dataframe, pandas timeseries): input data
        window_size (int): how many data points make up a rolling window. If 0 is used then this is a normal zscore
        rather than rolling zscore
        min_window_size (int): how many data points are the minimal requirements before the window stats is calcualted
        fill_missing_method (string): ffill or bfill with last available values
        start (pandas series or dataframe key): row key for the start if it is provided
        end (pandas series or dataframe key): row key for the end if it is provided
        bday_only (bool): if the table rows are keyed on dates this option allows override to include none business day
        fill_missing_at_the_end (bool) : a flag decides whether to fill the missing value at the end AFTER the calculation
        is_df (bool): a flag de
        
    Returns: 
        a dataframe or series that containg the rolling zscores based on the input data type.    
    """

    df = data if is_df else pd.DataFrame({("DATA" if data.name == None else data.name):data})

    validate_timeseries_data(df)

    input_df_index_name = df.index.name

    start_date = df.sort_index().index.min() if start is None else start
    end_date = df.sort_index().index.max() if end is None else end
    keys = pd.bdate_range(start=start_date,end=end_date) if bday_only else pd.date_range(start=start_date,end=end_date)

    cols = df.columns.tolist()
    raw_df = df.reindex(keys)
    rolling_zscore_list = []

    if (window_size == 0 or window_size > len(raw_df)):
        window_size = len(raw_df)
        min_window_size =  len(raw_df)
        for col in df.columns.tolist():
            raw_data = trim_series (raw_df[col].sort_index()).fillna(method=fill_missing_method).dropna()        
            raw_data_avg = raw_data.mean()        
            raw_data_stdev = raw_data.std()
            rolling_zscore_series = ((raw_data - raw_data_avg)/raw_data_stdev) \
                                    .replace([np.inf, -np.inf], np.nan) \
                                    .reindex(keys)
            rolling_zscore_list.append(pd.DataFrame(rolling_zscore_series,columns=[col]))
    else:
        for col in df.columns.tolist():
            raw_data = trim_series (raw_df[col].sort_index()).fillna(method=fill_missing_method).dropna()        
            rolling_avg = raw_data.rolling(window=window_size, min_periods=min_window_size, center=False).mean()        
            rolling_stdev = raw_data.rolling(window=window_size, min_periods=min_window_size).std()
            rolling_zscore_series = ((raw_data - rolling_avg)/rolling_stdev) \
                                    .replace([np.inf, -np.inf], np.nan) \
                                    .reindex(keys)
            rolling_zscore_list.append(pd.DataFrame(rolling_zscore_series,columns=[col]))



    rolling_zscore_df = pd.concat(rolling_zscore_list, join='outer', axis=1)[cols]

    if fill_missing_at_the_end: rolling_zscore_df = rolling_zscore_df.fillna(method=fill_missing_method)[cols]

    #reapply the index name in the output df
    rolling_zscore_df.index.name = input_df_index_name

    if is_df:
        return rolling_zscore_df
    else:
        if data.name == None:
            d = rolling_zscore_df["DATA"]
            d.name =None
            return d     
        else: 
            return rolling_zscore_df[data.name]



#------------------------------------------------------------------------------------------------
def rolling_ECDF_table(df,
                      window_size=1,
                      min_window_size=1,
                      fill_missing_method=r"ffill",
                      start=None,
                      end=None,
                      bday_only=True,
                      fill_missing_at_the_end=False):
#------------------------------------------------------------------------------------------------
    """
    rolling ECDF a dataframe

    :param df: input dataframe/table
    :param window_size: how many data points make up a rolling window
    :param min_window_size: how many data points are the minimal requirements before the window stats is calcualted
    :param fill_missing_method: ffill or bfill with last available values
    :param start: row key for the start if it is provided
    :param end: row key for the end if it is provided
    :param bday_only: if the table rows are keyed on dates this option allows override to include none business day
    :param fill_missing_at_the_end: a flag decides whether to fill the missing value at the end AFTER the calculation
    :return: a dataframe that containg the rolling ecdf.
    """

    validate_timeseries_data(df)
    input_df_index_name = df.index.name

    start_date = df.sort_index().index.min() if start is None else start
    end_date = df.sort_index().index.max() if end is None else end
    keys = pd.bdate_range(start=start_date,end=end_date) if bday_only else pd.date_range(start=start_date,end=end_date)
    cols = df.columns.tolist()
    raw_df = df.reindex(keys)
    rolling_result_list = []
    for col in df.columns.tolist():
        raw_data = trim_series (raw_df[col].sort_index())\
                   .fillna(method=fill_missing_method)\
                   .dropna()
        #trim the blank from the head and tail of the series
        #fill the potholes in the middle

        # col_rolling_result = pd.rolling_apply(raw_data,window_size, lambda w: ECDF(w)(w[-1]), min_periods=min_window_size)\
        #                        .reindex(keys)

        col_rolling_result = raw_data.rolling(window_size, min_periods=min_window_size).apply(lambda w: ECDF(w)(w[-1]))\
                               .reindex(keys)

        rolling_result_list.append(pd.DataFrame(col_rolling_result,columns=[col]))

    rolling_result_df = pd.concat(rolling_result_list, join='outer', axis=1)[cols]

    if fill_missing_at_the_end: rolling_result_df = rolling_result_df.fillna(method=fill_missing_method)[cols]

    #reapply the index name in the output df
    rolling_result_df.index.name = input_df_index_name

    return rolling_result_df



#------------------------------------------------------------------------------------------------
def expanding_standardise_table(df,
                                fill_missing_method=r"ffill",
                                start=None,
                                end=None,
                                bday_only=True,
                                fill_missing_at_the_end =False,
                                min_window=1
                                ):
#------------------------------------------------------------------------------------------------
    """
    :param df:
    :param fill_missing_method:
    :param start:
    :param end:
    :param bday_only:
    :param fill_missing_at_the_end: a flag decides whether to fill the missing value at the end AFTER the calculation
    :return:
    """

    validate_timeseries_data(df)
    input_df_index_name = df.index.name

    start_date = df.sort_index().index.min() if start is None else start
    end_date = df.sort_index().index.max() if end is None else end
    keys = pd.bdate_range(start=start_date,end=end_date) if bday_only else pd.date_range(start=start_date,end=end_date)
    cols = df.columns.tolist()
    raw_df = df.reindex(keys)
    expanding_zscore_list = []
    for col in df.columns.tolist():
        raw_data = raw_df[col].sort_index().dropna()
        raw_data = trim_series (raw_df[col].sort_index()).fillna(method=fill_missing_method).dropna()
        #expanding_avg = pd.expanding_mean(raw_data)
        #expanding_stdev = pd.expanding_std(raw_data)
        expanding_avg = raw_data.expanding(min_periods=min_window).mean()
        expanding_stdev = raw_data.expanding(min_periods=min_window).std()
        expanding_zscore_series = ((raw_data - expanding_avg)/expanding_stdev) \
                                .replace([np.inf, -np.inf], np.nan) \
                                .reindex(keys) \
                                .fillna(method=fill_missing_method)
        expanding_zscore_list.append(pd.DataFrame(expanding_zscore_series,columns=[col]))

    expanding_zscore_df = pd.concat(expanding_zscore_list, join='outer', axis=1)

    if fill_missing_at_the_end: expanding_zscore_df = expanding_zscore_df.fillna(method=fill_missing_method)[cols]

    # reapply the index name from the input df
    expanding_zscore_df.index.name = input_df_index_name

    return expanding_zscore_df


#------------------------------------------------------------------------------------------------
def expanding_ECDF_table(df,
                         fill_missing_method=r"ffill",
                         start=None,
                         end=None,
                         bday_only=True,
                         fill_missing_at_the_end=False,
                         min_window = 1):
#------------------------------------------------------------------------------------------------
    """
    Expanding ECDF a dataframe

    :param df: input dataframe/table
    :param fill_missing_method: ffill or bfill with last available values
    :param start: row key for the start if it is provided
    :param end: row key for the end if it is provided
    :param bday_only: if the table rows are keyed on dates this option allows override to include none business day
    :param fill_missing_at_the_end: a flag decides whether to fill the missing value at the end AFTER the calculation
    :param min_window: how many data points are the minimal requirements before the window stats is calcualted
    :return: a dataframe that containg the expanding ecdf.
    """

    validate_timeseries_data(df)
    input_df_index_name = df.index.name

    start_date = df.sort_index().index.min() if start is None else start
    end_date = df.sort_index().index.max() if end is None else end
    keys = pd.bdate_range(start=start_date,end=end_date) if bday_only else pd.date_range(start=start_date,end=end_date)
    cols = df.columns.tolist()
    raw_df = df.reindex(keys)
    expanding_result_list = []

    for col in df.columns.tolist():
        raw_data = trim_series (raw_df[col].sort_index())\
                   .fillna(method=fill_missing_method)\
                   .dropna()
        #trim the blank from the head and tail of the series
        #fill the potholes in the middle

        # col_expanding_result = pd.expanding_apply(raw_data, lambda w: ECDF(w)(w[-1]), min_periods=min_window)\
        #                        .reindex(keys)

        #ECDF returns the percentile for the entire list. it is the same as expanding. but i am too lazy to reindex
        #hence wasting some CPU
        col_expanding_result = raw_data.expanding( min_periods=min_window).apply(lambda w: ECDF(w)(w[-1]))

        expanding_result_list.append(pd.DataFrame(col_expanding_result, columns=[col]))

    rolling_result_df = pd.concat(expanding_result_list, join='outer', axis=1)[cols]

    if fill_missing_at_the_end: rolling_result_df = rolling_result_df.fillna(method=fill_missing_method)[cols]

    #reapply the index name in the output df
    rolling_result_df.index.name = input_df_index_name

    return rolling_result_df


