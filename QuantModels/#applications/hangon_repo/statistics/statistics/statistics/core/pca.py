from __future__ import division
import pandas as pd
import numpy as np
import scipy as sp
import math
from datetime import datetime
from datetime import timedelta
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates



class PCA(object):
    def __init__(self, data, matrixtype="COV"):
        """ A generic Principle Component Analysis object
            parameter:
            ----------
                data:dataframe: 
                    Input data
                matrixtype:str
                    Whether to use covariance or correlation matrix for the eigen decomposition.
                    The valid values are "COV" or "CORR"  
        """
        if matrixtype not in ("COV", "CORR"):
            raise ValueError("The matrixtype must be either COV or CORR")
        self._data = data
        self._matrixtype = matrixtype
        self._centered_data = self._data.apply(lambda col: col - col.mean(), axis=0)
        self._eigvals, self._eigvecs, self._eigvals_raw = self.get_eigens()
        self._pcs = self.pcs
        (self._reconstructed_data, self._reconstruction_pcs) = self.reconstruct_data_by_n_pc(n=[1, 2, 3])
    

    def get_eigens(self):
        """ calculate the eigen decomposition
            returns:
            --------
                result: tuple
                    1st element is the eigenvals expressed in pct of total variance (1d numpy array)
                    2nd element is the eigenvecs (2d numpy array). The 2d array eigen vectors are column based. 
                    in order to access an eigen value's eigen vector using a single Index the eigen vector 2d array needs 
                    to be transposed first.
                    3rd element is the eigenvals in value (1d numpy array)
        """
        matrix = self.cov if self._matrixtype == "COV" else self.corr
        eigvals, eigvecs = np.linalg.eig(matrix)
        eigvecs = eigvecs[:, eigvals.argsort()[::-1]]
        eigvals = eigvals[eigvals.argsort()[::-1]]
        eigvals_pct = eigvals / eigvals.sum()
        return eigvals_pct, eigvecs, eigvals

    def reconstruct_data_by_n_pc(self, n=[1, 2, 3]):
        """After the original data are decomposed into principle components, they can be reconstructed
            via the principle components. One way to filter out noises are to ignore the eigen vectors
            whose eigen values' explainatory powers are low.

            parameters:
            ----------- 
                n an array of int: 
                    the eigen value index to be included in the value reconstruction
            Returns:
            -------
                result: dataframe 
                    principle components
                
        """
        assert isinstance(n, (list))
        assert not isinstance(n, basestring)

        selectedloadings = self.loadings.copy()
        pcs = self.pcs.copy()

        # pandas checks not only the dimension but also the column index name are matching!
        pcs.columns = [c.replace("PC", "Loading") for c in pcs.columns.tolist()]

        for col in selectedloadings.columns.tolist():
            if col not in ["Loading" + str(i) for i in n]:
                selectedloadings[col] = 0

        loadingsT = selectedloadings.transpose()

        self._reconstructed_data = pcs.dot(loadingsT)
        self._reconstruction_pcs = n

        return (self._reconstructed_data, self._reconstruction_pcs)

    @property
    def data(self):
        return self._data

    @property
    def centered_data(self):
        return self._centered_data
    
    @property
    def corr(self):
        return self._data.corr()

    @property
    def cov(self):
        return self.centered_data.cov()

    @property
    def pcs(self):
        """
        Returns:
            pcs: dataframe
                principle components 
        """
        pcdf = self.centered_data.dot(self._eigvecs)
        pcdf.columns = ["PC" + str(i + 1) for i in range(len(self._data.columns.tolist()))]
        return pcdf

    @property
    def loadings(self):
        loadings = {"Loading" + str(i + 1): pd.Series(self._eigvecs[:, i], index=self._data.columns.tolist())
                    for i in range(len(self._data.columns.tolist()))}
        colnames = ["Loading" + str(i + 1) for i in range(len(self._data.columns.tolist()))]
        return pd.DataFrame(loadings)[colnames]

    @property
    def eigvecs(self):
        return self._eigvecs.T

    @property
    def eigvals(self):
        """
        Returns:
            eigvals: 1d numpy array
                eigvals in percent 
        """
        return self._eigvals

    @property
    def eigvals_raw(self):
        """
        Returns:
            eigvals: 1d numpy array
                eigvals in absolute value 
        """
        return self._eigvals_raw

    @property
    def pcs_used_data_reconstruction(self):
        return self._reconstruction_pcs

    
    @property
    def reconstructed_data(self):
        return self._reconstructed_data

    @property
    def reconstruction_data_gaps(self):   
        return (self.centered_data - self._reconstructed_data)

def hedge_risk_by_pcs(eigenvecs, risks, pcs):
    """
        parameters:
        -----------
        eigenvecs:an array of array
            an array of eigen vectors
        risks: array of float
            durations of the instrments
        pcs: array of int
            pcs whose risks will be hedged
        return:
        -------
        weights: array of float
            weights for the instrments
    """




    pass