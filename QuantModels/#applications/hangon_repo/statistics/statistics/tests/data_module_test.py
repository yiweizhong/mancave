"""
unitTest
"""
import unittest
import test_path
import statistics.data.extension as extension
import statistics.data.standardization as standardization
import numpy as np
import pandas as pd
import pandas.util.testing as pdt
import numpy.testing as npt


def bootstrap_ts(data, dates):
    """bootstrap ts"""
    data = pd.Series(data=data, index=dates)
    data.index = pd.to_datetime(data.index)
    data.index.name = "DATE"
    return data


class ExtensionModuleTest(unittest.TestCase):
    """test extension module
    """
    def setUp(self):
        """ setup
        """
        self.date_idx = "DATE"


    def test_validate_timeseries_data(self):        
        data = bootstrap_ts([1, 20, 300, 4000],
                            ['2017-02-01', '2017-02-02', '2017-02-03', '2017-02-04'])
        self.assertEqual(True, extension.validate_timeseries_data(data, is_df=False))

        self.assertEqual(True, extension.validate_timeseries_data(pd.DataFrame({'a':data}), 
                                                                                is_df=True))
        
        with self.assertRaisesRegexp(TypeError, "The input timeseries obj is NOT a valid"):
            self.assertEqual(True, extension.validate_timeseries_data(np.array([]), is_df=False))
        
        with self.assertRaisesRegexp(TypeError, "The input timeseries obj is NOT a valid"):
            self.assertEqual(True, extension.validate_timeseries_data(data, is_df=True))


    def test_validate_list(self):  
        data = [1, 20, 300, 4000]        
        self.assertEqual(True, extension.validate_list(data, allow_empty=True))
        self.assertEqual(True, extension.validate_list([], allow_empty=True))

        with self.assertRaisesRegexp(TypeError, "The input is expected to be a non empty list object"):
            self.assertEqual(True, extension.validate_list([], allow_empty=False))
            

class StandardizationModuleTest(unittest.TestCase):
    def setUp(self):
        """ setup
        """
        self.date_idx = "DATE"
n

    def test_rolling_standardise(self):
        print "Test all in"
        col1 = bootstrap_ts([1,10,20,40,100],
                            ['2017-01-01','2017-01-02','2017-01-03', '2017-01-04', '2017-01-05'])
        result1 = standardization.rolling_standardise(col1,window_size=0, is_df = False)

        exp_results = bootstrap_ts([-0.80622577483, -0.558156305651, -0.0620173672946, 1.42639944778],
                                   ['2017-01-02', '2017-01-03', '2017-01-04', '2017-01-05'])
        pdt.assert_series_equal(result1, exp_results)

        print "Test rolling"
        col1 = bootstrap_ts([1,10,20,40,100],
                            ['2017-01-01','2017-01-02','2017-01-03', '2017-01-04', '2017-01-05'])
        result1 = standardization.rolling_standardise(col1,window_size=2, is_df = False)

        exp_results = bootstrap_ts([np.NaN, 0.707106781187, 0.707106781187, 0.707106781187],
                                   ['2017-01-02', '2017-01-03', '2017-01-04', '2017-01-05'])
        pdt.assert_series_equal(result1, exp_results)





if __name__ == '__main__':    
    suite = unittest.TestLoader().loadTestsFromTestCase(ExtensionModuleTest)
    unittest.TextTestRunner(verbosity=2).run(suite)

    suite = unittest.TestLoader().loadTestsFromTestCase(StandardizationModuleTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
