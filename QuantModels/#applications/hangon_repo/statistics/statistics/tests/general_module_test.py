"""
unitTest
"""
import unittest
import test_path
import statistics.general.returns as returns
import numpy as np
import pandas as pd
import pandas.util.testing as pdt
import numpy.testing as npt


def bootstrap_ts(data, dates):
    """bootstrap ts"""
    data = pd.Series(data=data, index=dates)
    data.index = pd.to_datetime(data.index)
    data.index.name = "DATE"
    return data


class ReturnsModuleTest(unittest.TestCase):
    """test return module
    """
    def setUp(self):
        """ setup
        """
        self.date_idx = "DATE"


    def test_simple_level_diff_from_series(self):
        """test simple_level_diff"""
        data = bootstrap_ts([1, 20, 300, 4000],
                            ['2017-02-01', '2017-02-02', '2017-02-03', '2017-02-04'])
        rtns = returns.simple_level_diff(data, 2)
        exp_returns = bootstrap_ts([np.NAN, np.NAN, 299, 3980],
                                   ['2017-02-01', '2017-02-02', '2017-02-03', '2017-02-04'])
        pdt.assert_series_equal(rtns, exp_returns)

    def test_simple_level_diff_from_df(self):
        """test simple_level_diff"""
        data = bootstrap_ts([1, 20, 300, 4000],
                            ['2017-02-01', '2017-02-02', '2017-02-03', '2017-02-04'])
        data = pd.DataFrame({'a':data, 'b':data})
        rtns = returns.simple_level_diff(data, 2)
        exp_returns = bootstrap_ts([np.NAN, np.NAN, 299, 3980],
                                   ['2017-02-01', '2017-02-02', '2017-02-03', '2017-02-04'])
        exp_returns = pd.DataFrame({'a':exp_returns, 'b':exp_returns})
        pdt.assert_frame_equal(rtns, exp_returns)

    def test_simple_level_diff_from_array(self):
        """test simple_level_diff"""
        data = np.array([1, 20, 300, 4000])        
        rtns = returns.simple_level_diff(data, 2)
        exp_returns = np.array([np.NAN, np.NAN, 299, 3980])                                   
        npt.assert_array_equal(rtns, exp_returns)


if __name__ == '__main__':
    #unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(ReturnsModuleTest)
    unittest.TextTestRunner(verbosity=2).run(suite)

