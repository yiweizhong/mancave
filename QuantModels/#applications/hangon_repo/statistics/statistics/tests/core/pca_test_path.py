"""path"""
import os
import sys
import inspect

def config_path():
    """Config the path"""
    path = os.path.split(inspect.getfile(inspect.currentframe()))[0]
    path = os.path.realpath(os.path.abspath(path))
    parent_path = os.path.abspath(os.path.join(path, os.pardir))
    parent_path = os.path.abspath(os.path.join(parent_path, os.pardir))
    g_parent_path = os.path.abspath(os.path.join(parent_path, os.pardir))
    data_module_path = os.path.abspath(os.path.join(g_parent_path, "data/")) 


    for _path in [parent_path]:
        if _path not in sys.path:
            print "adding {}".format(_path)
            sys.path.insert(0, _path)

    print "configured sys.path:"
    for p in sys.path:
        print p

config_path()