#%%
import os, inspect
import pca_test_path
import numpy as np
import pandas as pd
import pandas.util.testing as pdt
import numpy.testing as npt
from statistics.core.pca import PCA
import data.extension as extn

%matplotlib inline

def bootstrap_ts(data = [1, 20, 3.3, 3.35, 5.5, 6, 7.7, 8, 9, 10.1], 
                 dates = ['2017-02-01','2017-02-02','2017-02-03','2017-02-04','2017-02-05',
                          '2017-02-06','2017-02-07','2017-02-08','2017-02-09','2017-02-10']):
    """bootstrap ts"""
    data = pd.Series(data=data, index=dates)
    data.index = pd.to_datetime(data.index)
    data.index.name = "DATE"
    return data

def load_bbg_historical_data(file_path, key_col = 'date'):
    data = extn.create_datetime_df(pd.read_csv(file_path),key = key_col,utc=False)
    return data
    


path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
path = os.path.realpath(os.path.abspath(path))
path = os.path.abspath(os.path.join(path, os.pardir))
file_path = os.path.abspath(os.path.join(path, "data/2017-08-24_FWD_historical.csv")) 
data = load_bbg_historical_data(file_path).tail(180)[["AUD_10Y_5Y","CAD_10Y_5Y"]].tail(180)
fwd_rates_pca = PCA(data)

#%%

#%%
#%%