"""path"""
import os
import sys
import inspect

def config_path():
    """Config the path"""
    path = os.path.split(inspect.getfile(inspect.currentframe()))[0]
    path = os.path.realpath(os.path.abspath(path))
    parent_path = os.path.abspath(os.path.join(path, os.pardir))
    parent_path = os.path.abspath(os.path.join(parent_path, os.pardir))

    if parent_path not in sys.path:
        print "adding {}".format(parent_path)
        sys.path.insert(0, parent_path)

    print "sys.path:"
    for p in sys.path:
        print p

config_path()