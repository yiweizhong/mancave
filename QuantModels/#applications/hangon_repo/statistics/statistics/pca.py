"""A model object representing a PCA Analysis
"""
from __future__ import division
import pandas as pd
import numpy as np
import math
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mpd
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from datetime import datetime
from datetime import timedelta

class PCA(object):
    """A generic Principle Component Analysis object
    """

    def __init__(self, data, matrixtype="COV"):
        """
        Args:
            data (pandas dataframe): The input data series stored in a pandas dataframe object
            matrixtype (str): A string value representing whether to use covariance or correlation
                matrix for the eigen decomposition.
        """
        if matrixtype not in ("COV", "CORR"):
            raise ValueError("The matrixtype must be either COV or CORR")
        self._data = data
        self._matrixtype = matrixtype


    def get_centered_data(self):
        """
        Returns:
            The input data after it is demeaned
        """
        return self._data.apply(lambda col: col - col.mean(), axis=0)

    def get_covariance(self):
        """
        Returns:
            The covariance matrix (pandas dataframe)
        """
        return self._centereddata.cov()

    def get_correlation(self):
        """
        Returns:
            The correlation matrix (pandas dataframe)
        """
        return self._data.corr()

    def get_eigens(self):
        """
        Returns:
            A tuple representing the eigen values and eigen vectors
            eigen values (1d numpy array): The eigen values and correspoding eigen
                vectors are sorted in descending order
            eigen vectors (2d numpy array): The 2d array eigen vectors are column based. 
                in order to access an eigen value's eigen vector using a single Index
                the eigen vector 2d array needs to be transposed first.
        """
        matrix = self.cov if self._matrixtype == "COV" else self.corr
        eigvals, eigvecs = np.linalg.eig(matrix)
        eigvecs = eigvecs[:, eigvals.argsort()[::-1]]
        eigvals = eigvals[eigvals.argsort()[::-1]]
        eigvals = eigvals / eigvals.sum()
        return eigvals, eigvecs

    def get_pc_explainatory_powers(self):
        """
        Returns:
            eigen values' explainatory powers (1d numpy array)
        """
        exp_power = self._eigvals.cumsum()
        idx = ["TOP_" + str(i + 1) + "_PCs" for i in range(len(self._eigvals))]
        return pd.Series(data=exp_power, index=idx)

    def get_eigvec_by_idx(self, idx):
        """
        Args:
            array index(int)
        Returns:
            eigen vector (1d numpy array)
        """
        return pd.Series(data=self._eigvecs.T[idx], index=self._data.columns)

    def plot_pc_eigens(self, w, h):
        """
        Args:
            w (int): The width of the chart
            h (int): The height of the chart
        """    

        grid_size = len(self._eigvals)
        grid_rows = int(math.ceil(grid_size / 3))
        grid_cols = 3
        grid_size = grid_rows * grid_cols

        #plt.close()
        fig1 = plt.figure(figsize=(w, h))
        gs = gridspec.GridSpec(grid_rows, grid_cols, height_ratios=[1] * grid_rows, width_ratios=[1] * grid_cols)
        axs = [plt.subplot(gs[i]) for i in range(grid_size)]


        for i in range(len(self._eigvals)):
            ax = axs[i]
            ax.set_ylim([-1, 1])
            self.get_eigvec_by_idx(i).plot(kind='bar', ax=ax)
            ax.set_title("PC{} eignen vectors - {:0.2f} %".format(i + 1, self._eigvals[i] * 100))
            ax.yaxis.grid(True, which='major', color='b', linestyle='--', alpha=0.3)
            ax.axhline(y=0, color="red", linewidth=0.5)

        fig1.tight_layout()

    def plot_pc_eigens2(self, w, h, row=2, column=3):
        """
        Args:
            w (int): The width of the chart
            h (int): The height of the chart
            row (int): The default number of row per grid
            column (int): The default number of column per grid
        """    
        fig_size = 1
        grid_size = len(self._eigvals)
        fig_size = int(math.ceil(grid_size/(row * column)))
        grid_rows= row
        grid_cols = column        

        for i in range(fig_size):                        

            fig1 = plt.figure(figsize=(w, h))            
            grid_idx_per_figure = 6 if i < fig_size else grid_size % 6
            gs = gridspec.GridSpec(grid_rows, grid_cols, height_ratios=[1] * grid_rows, width_ratios=[1] * grid_cols)
            axs = [plt.subplot(gs[k]) for k in range(grid_idx_per_figure)]
            
            for j in list(range(len(axs))):                

                ax = axs[i]
                ax.set_ylim([-1, 1])
                data_idx = i * len(axs) + (j + 1) - 1                          
                print "data_idx = {}".format(data_idx)
                data = self.get_eigvec_by_idx(data_idx)
                data.plot(kind='bar', ax=ax)
                ax.set_title("PC{} eignen vectors - {:0.2f} %".format(data_idx + 1, self._eigvals[data_idx] * 100))
                ax.yaxis.grid(True, which='major', color='b', linestyle='--', alpha=0.3)
                ax.axhline(y=0, color="red", linewidth=0.5)

            fig1.tight_layout()
                


    def plot_pcs(self, w, h, dps=200):
        """
        Args:
            w (int): The width of the chart
            h (int): The height of the chart
            dps (int): number of the data points from pcs to be charted
        """                

        grid_size = len(self._eigvals)
        grid_rows = int(math.ceil(grid_size / 3))
        grid_cols = 3
        grid_size = grid_rows * grid_cols
    
        
        fig1 = plt.figure(figsize=(w, h))
        gs = gridspec.GridSpec(grid_rows, grid_cols, height_ratios=[1] * grid_rows, width_ratios=[1] * grid_cols)
        axs = [plt.subplot(gs[i]) for i in range(grid_size)]

        for i in range(len(self._eigvals)):
            ax = axs[i]
            idx = i + 1
            pc='PC{}'.format(idx)                   
            self.get_pcs()[pc].tail(dps).plot(title=pc, ax=ax)                                
            ax.yaxis.grid(True, which='major', color='b', linestyle='--', alpha=0.3)
            ax.axhline(y=0, color="red", linewidth=0.5)
            
            lmt = abs(self.get_pcs()[pc].tail(dps).min()) * 1.05 
            if abs(self.get_pcs()[pc].tail(dps).max()) >= abs(self.get_pcs()[pc].tail(dps).min()):
                lmt = abs(self.get_pcs()[pc].tail(dps).max()) * 1.05 
            
            ax.set_ylim([-1 * lmt, lmt])
        
        fig1.tight_layout()

    def plot_reconstructed_value_gap(self, w, h, n=[1,2,3], dps=200):
        """ This method will plot out the gap between the actual data series and the synthetically reconstructed 
        data series using the selected using selected PCs. This means none selected PCs are hedged. 

        Args:
            w (int): The width of the chart
            h (int): The height of the chart
             n (an array of int): the eigen value index to be included in the value reconstruction 
            dps (int): number of the data points from pcs to be charted
        """                

        grid_size = len(self._eigvals)
        grid_rows = int(math.ceil(grid_size / 3))
        grid_cols = 3
        grid_size = grid_rows * grid_cols

        #plt.close()
        
        fig1 = plt.figure(figsize=(w, h))
        gs = gridspec.GridSpec(grid_rows, grid_cols, height_ratios=[1] * grid_rows, width_ratios=[1] * grid_cols)
        axs = [plt.subplot(gs[i]) for i in range(grid_size)]

        gaps = self.get_pca_implied_valuations_gaps(n)

        overalllmt = None

        title_suffix = ",".join([ '{}'.format(pc) for pc in n])

        for i in range(len(self._eigvals)):
            dname = gaps.columns[i]

            lmt = abs(gaps[dname].tail(dps).min()) * 1.05     
            if abs(gaps[dname].tail(dps).max()) >= abs(gaps[dname].tail(dps).min()):                
                lmt = abs(gaps[dname].tail(dps).max()) * 1.05 
            
            if overalllmt is None:
                overalllmt = lmt
            else:
                if overalllmt < lmt:
                    overalllmt = lmt 

        for i in range(len(self._eigvals)):
            ax = axs[i]
            idx = i + 1
            
            #pc='PC{}'.format(idx)    
            dname = gaps.columns[i]

            #self.get_pcs()[pc].tail(dps).plot(title=pc, ax=ax)                                
            gaps[dname].tail(dps).plot(title='{} reconsturcted with PC {}'.format(dname,title_suffix), ax=ax)                                
            
            ax.yaxis.grid(True, which='major', color='b', linestyle='--', alpha=0.3)
            ax.axhline(y=0, color="red", linewidth=0.5)
                                    
            ax.set_ylim([-1 * overalllmt, overalllmt])
        
        fig1.tight_layout()

    def get_pcs(self):
        """
        Returns:
            principle components (a pandas dataframe)
        """
        pcdf = self._centereddata.dot(self._eigvecs)
        pcdf.columns = ["PC" + str(i + 1) for i in range(len(self._data.columns.tolist()))]
        return pcdf

    def get_reconstruct_valuation_by_n_pc(self, n=[1, 2, 3]):
        """
        After the original data are decomposed into principle components, they can be reconstructed
        via the principle components. One way to filter out noises are to ignore the eigen vectors
        whose eigen values' explainatory powers are low.

        Args: 
            n (an array of int): the eigen value index to be included in the value reconstruction
        Returns:
            principle components (a pandas dataframe)
        """
        assert isinstance(n, (list))
        assert not isinstance(n, basestring)

        selectedloadings = self.loadings.copy()
        pcs = self.pcs.copy()

        # pandas checks not only the dimension but also the column index name are matching!
        pcs.columns = [c.replace("PC", "Loading") for c in pcs.columns.tolist()]

        for col in selectedloadings.columns.tolist():
            if col not in ["Loading" + str(i) for i in n]:
                selectedloadings[col] = 0

        loadingsT = selectedloadings.transpose()

        #self._reconstructed_valuations = pcs.dot(loadingsT)
        #self._reconstructed_valuations_pcs = n
        _reconstructed_valuations = pcs.dot(loadingsT)
        _reconstructed_valuations_pcs = n

        return (_reconstructed_valuations, _reconstructed_valuations_pcs)

    def calc(self):
        """An entry point function to be called after init
        """
        self._centereddata = self.get_centered_data()
        self._eigvals, self._eigvecs = self.get_eigens()
        self._pcs = self.get_pcs()
        self._reconstructed_valuations, self._reconstructed_valuations_pcs = self.get_reconstruct_valuation_by_n_pc(n=[1, 2, 3])
        return self

    def get_pca_implied_valuations_gaps(self, n=[1, 2, 3]):
        """ This function calculates the differences between the centered data and the synthetic values (based on selected pcs).
        The differences are considered as the valuation gaps for RV trading  
        
        Args: 
            n (an array of int): the eigen value index to be included in the value reconstruction
        Returns:
            The differences between the market value and the syntheticly created valuation based on selected pcs (a pandas dataframe)
        """
        return self._centereddata - self.get_reconstruct_valuation_by_n_pc(n=n)[0]


    @property
    def data(self):
        return self._data

    @property
    def centereddata(self):
        return self._centereddata

    @property
    def cov(self):
        return self.get_covariance()

    @property
    def corr(self):
        return self.get_correlation()

    @property
    def eigvals(self):
        return self._eigvals

    @property
    def eigvecs(self):
        return self._eigvecs.T

    @property
    def loadings(self):
        loadings = {"Loading" + str(i + 1): pd.Series(self._eigvecs[:, i], index=self._data.columns.tolist())
                    for i in range(len(self._data.columns.tolist()))}
        colnames = ["Loading" + str(i + 1) for i in range(len(self._data.columns.tolist()))]
        return pd.DataFrame(loadings)[colnames]

    @property
    def pcs(self):
        return self._pcs

    @property
    def reconstructed_valuations(self):
        return self._reconstructed_valuations

    @property
    def reconstructed_valuations_pcs(self):
        return self._reconstructed_valuations_pcs

















