#####################################
#
# This playground is to facilitate the high vol RV trade report
#
####################################
#%%
from __future__ import division
import numpy as np
import pandas as pd
import statistics.data.extension as extn
import statistics.relative_value as rv

file_path = r'C:/Dev/hangon_repo/statistics/playground/data/'
fwd_rates = 'FWD_historical.csv'
swaption_ivol = 'Swaption_IVol_historical.csv'
swap_file = file_path + fwd_rates
swaption_ivol_file = file_path + swaption_ivol

swap_data = extn.create_datetime_df(pd.read_csv(swap_file),key ="date",utc=False)
swaption_ivol_data = extn.create_datetime_df(pd.read_csv(swaption_ivol_file),key ="date",utc=False)

