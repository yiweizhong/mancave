#%%
from __future__ import division
import __builtin__
from IPython.lib import deepreload
__builtin__.reload = deepreload.reload
from playground._playground_setup import *
import numpy as np
import pandas as pd
import data.extension as extn
import general.returns as rtn
from pca import *
from playground._playground_market_watcher_setup import *





class Swaps(object):
    def __init__(self,data):
        """
        Args:
            data (pandas dataframe): loaded data. The column headers have the pattern "Country_FwdY_TenorY" 
        """
        self._data = data
    @staticmethod  
    def match_header_n_key(header, key, delimiter='_', pos=0):
        return (header.upper().split(delimiter)[pos] == key)
    
    @staticmethod  
    def match_header_n_keys(header, keys, delimiter='_', pos=0):
        _keys = [key.upper() for key in keys]
        return (header.upper().split(delimiter)[pos] in _keys)    

    def filter_by_countries(self, countries):
        _data = self._data[[col for col in self._data.columns if self.match_header_n_keys(col, countries, delimiter='_',pos=0)]]
        return Swaps(_data)

    def filter_by_fwd(self, term):
        _data = self._data[[col for col in self._data.columns if self.match_header_n_key(col,"{}Y".format(term).upper(), delimiter='_',pos=1)]]
        return Swaps(_data)
        
    def filter_by_tenor(self, term):
        _data = self._data[[col for col in self._data.columns if self.match_header_n_key(col,"{}Y".format(term).upper(), delimiter='_',pos=2)]]
        return Swaps(_data)
    
    def filter_by_fwds(self, terms):
        assert isinstance(terms, (list))        
        _data = pd.concat([self.filter_by_fwd(term).data for term in terms], axis=1)        
        return Swaps(_data)
    
    def filter_by_tenors(self, terms):
        assert isinstance(terms, (list))        
        _data = pd.concat([self.filter_by_tenor(term).data for term in terms], axis=1)        
        return Swaps(_data)
    
    @property
    def data(self):
        return self._data        



#load
#setup
file_date = "2017-06-15"
file_path = "C:/Dev/hangon_repo/statistics/relative_value/tests/data/"
input_file = file_path + file_date + "_FWD_historical.csv"
input_file = r"C:/Dev/hangon_repo/app_bbg_data/Output/Historical/2017-08-24_FWD_historical.csv"
input_file = r"C:/Dev/hangon_repo/app_bbg_data/Output/Historical/2017-08-29_US_FWD_historical.csv"
swap_data = Swaps(extn.create_datetime_df(pd.read_csv(input_file),key ="date",utc=False)) 


#%%

Devloped = ['AUD','CAD','EUR','USD','GBP','NZD','NOK','SEK','JPY','DKK','KRW','SGD']
Devloped = ['CAD','AUD']
Devloped = ['USD']

swap_data_level = swap_data.filter_by_fwds([5]) \
                     .filter_by_tenors([5]) \
                     .filter_by_countries(Devloped) \
                     .data.loc['2016-11-01':]

swap_data_level = swap_data \
                    .filter_by_fwds([10]) \
                    .filter_by_tenors([5]) \
                    .filter_by_countries(Devloped) \
                    .data.loc['2017-01-01':]

swap_data_level = swap_data \
                    .filter_by_countries(Devloped) \
                    .filter_by_fwds([0]) \
                    .filter_by_tenors([2, 5, 10]) \
                    .data.loc['2017-01-01':]

for i in  swap_data_level.columns.tolist():
    print i


swap_data_returns = rtn.simple_level_diff(swap_data_level,5)

swap_data_level_pca = PCA(swap_data_level).calc()

swap_data_returns_pca = PCA(swap_data_returns).calc()

#swap_data_level.columns


#%%
#pca_obj = swap_data_5y5y_pca
#pca_obj = swap_data_5y5y_returns_pca
#pca_obj = swap_data_CAD5y5y_SEK3y5y_pca
#pca_obj = swap_data_CAD5y5y_SEK3y5y_returns_pca

pca_obj = swap_data_level_pca


print "===========>corr<=============="
print pca_obj.corr
print "===========>corr<=============="

pca_obj.plot_pc_eigens(20,16)


plt.figure()

pca_obj.plot_reconstructed_value_gap(20, 16, n=[1,2, 3], dps=400)

plt.figure()

pca_obj.plot_pcs(20,16,dps=400)

plt.figure()

for i in range(len(pca_obj.eigvecs)):
    idx = i + 1
    print "Eigen Vector {}:".format(idx)
    print pca_obj.get_eigvec_by_idx(i)




#%%

pc1_level = pca_obj.get_pcs()["PC1"]
pc2_level = pca_obj.get_pcs()["PC2"]
x = pca_obj.data.USD_0Y_2Y *(-0.2024) + pca_obj.data.USD_0Y_5Y *(1) + pca_obj.data.USD_0Y_10Y * (-0.8299)    
check = pd.DataFrame({"x":x, "pc1":pc1_level, "pc2":pc2_level})

check.corr()



#%%

pc1_level = pca_obj.get_pcs()["PC1"]
x = pca_obj.data.AUD_10Y_5Y + pca_obj.data.CAD_10Y_5Y * (-1.27968597)
check = pd.DataFrame({"x":x, "pc1":pc1_level})
check.corr()
#%%
plt.figure()
x.plot()

