#%%
from __future__ import division
import __builtin__
from IPython.lib import deepreload
__builtin__.reload = deepreload.reload
from playground._playground_setup import *
import numpy as np
import pandas as pd
import data.extension as extn
import general.returns as rtn
from statistics.core.pca import *
from playground._playground_market_watcher_setup import *
import xlwings as xw


 

#load
#setup
file_date = "2017-06-15"
file_path = "C:/Dev/hangon_repo/statistics/relative_value/tests/data/"
input_file = file_path + file_date + "_FWD_historical.csv"
input_file = r"C:/Dev/hangon_repo/app_bbg_data/Output/Historical/2017-08-24_FWD_historical.csv"
input_file = r"C:/Dev/hangon_repo/statistics/playground/data/2017-09-06_US_GG_historical.csv"
usgg_data = extn.create_datetime_df(pd.read_csv(input_file),key ="date",utc=False)


#%%

def RollingSampleDf(dataframein, windowsize=1):
    rows = len(dataframein)
    for i in np.arange(rows - windowsize):
        #subdf = dataframein.iloc[i:i+windowsize,] 
        #tstamp = dataframein.index[i] 
        yield (dataframein.index[i], dataframein.iloc[i:i+windowsize,])


usgg_data = usgg_data.ffill()
if ('USD_7Y' in usgg_data.columns.tolist()):
    usgg_data = usgg_data.drop('USD_7Y',axis=1)
usgg_data = usgg_data.dropna()
print usgg_data.head()
usgg_data_returns = rtn.simple_level_diff(usgg_data,1)


window = 90

#usgg_data = usgg_data.head(200)
usgg_data_pcas = [(tstamp, PCA(data)) for (tstamp, data) in RollingSampleDf(usgg_data, windowsize=window)]
#usgg_data_return_pcas = [(tstamp, PCA(data)) for (tstamp, data) in RollingSampleDf(usgg_data_returns, windowsize=window)]


#eigen vectors
#%%
def rename(s, name):
    s.name = name 
    return s

loading1 =pd.DataFrame([rename(pca[1].loadings['Loading1'], pca[0]) for pca in usgg_data_pcas[:]])
loading2 =pd.DataFrame([rename(pca[1].loadings['Loading2'], pca[0]) for pca in usgg_data_pcas[:]])
loading3 =pd.DataFrame([rename(pca[1].loadings['Loading3'], pca[0]) for pca in usgg_data_pcas[:]])

#output

#%%
outputWkb = xw.Book()
pc1_wks = outputWkb.sheets.add(name="EigVec1")
output_anchor_range1 = pc1_wks.range((1,1))
output_anchor_range1.value = loading1

pc2_wks = outputWkb.sheets.add(name="EigVec2")
output_anchor_range2 = pc2_wks.range((1,1))
output_anchor_range2.value = loading2

pc3_wks = outputWkb.sheets.add(name="EigVec3")
output_anchor_range3 = pc3_wks.range((1,1))
output_anchor_range3.value = loading3



#eigen vals
#%%
def rename(evals, name):
    s = pd.Series(evals, index =['PC1','PC2','PC3', 'PC4', 'PC5'])
    s.name = name 
    return s

evals =pd.DataFrame([rename(pca[1].eigvals, pca[0]) for pca in usgg_data_pcas[:]])


outputWkb = xw.Book("nam.xlsx")

exppower_wks = outputWkb.sheets.add(name="ExpPower")
output_anchor_range1 = exppower_wks.range((1,1))
output_anchor_range1.value = evals



#%%
usgg_data_all_pca = PCA(usgg_data)
outputWkb = xw.Book("nam.xlsx")
pc_wks = outputWkb.sheets.add(name="PC")
output_anchor_range = pc_wks.range((1,1))
output_anchor_range.value = usgg_data_all_pca.pcs