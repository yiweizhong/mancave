#%%
import numpy as np
a = np.array([1,2,3])
isinstance(a,np.ndarray)
a.ndim

#%%
import pandas as pd
a = {'a':[1,2,3], 'b':[40,50,65]}
d = pd.DataFrame(a)
d - d.shift(1)


#%%
import numpy as np
n = 5
a = np.array([1,3,6,9,20, 34])
#np.append(np.array([np.NaN] * 2 ), np.diff(a, 2))
#print np.diff(a, 1)
#print np.diff(a, 2)
#print np.diff(a, 3)

print a
print a[n:]
print a[:-1*n]

print np.append(np.array([np.NaN] * n ), a[n:] - a[:-1 * n])
print len(a)

#np.roll(a, 1)

#%%
import pandas as pd
s = pd.Series(data=[1,2,3])
s.name = "a"
df = pd.DataFrame({("Data" if s.name == None else s.name):s})
print df
print s.name




#%%
import pandas as pd
s = pd.Series(data=[1,2,3])
s.name = "a"
fi = s.first_valid_index()
li = s.last_valid_index()
print s[fi:li+1]


#%%
import pandas as pd
now = pd.datetime.now()

print now
print now.utcnow()
print (now.utcnow()+ pd.DateOffset(hours=-24))


#%%
import pandas as pd
from .statistics.pca import *


#%%
import pandas as pd
s = pd.Series(data=[10,20,30])
s2 = pd.Series(data=[10,20,30])
df = pd.DataFrame(data=[s,s2])
df = df.transpose()
print df




