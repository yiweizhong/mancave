#%%
from __future__ import division
import __builtin__
from IPython.lib import deepreload
__builtin__.reload = deepreload.reload
from playground._playground_setup import *
import numpy as np
import pandas as pd
import statistics.data.extension as extn
import statistics.general.returns as rtn
from statistics.pca import *



class Swaps(object):
    def __init__(self,data):
        """
        Args:
            data (pandas dataframe): loaded data. The column headers have the pattern "Country_FwdY_TenorY" 
        """
        self._data = data
    @staticmethod  
    def match_header_n_key(header, key, delimiter='_', pos=0):
        return (header.upper().split(delimiter)[pos] == key)
    
    @staticmethod  
    def match_header_n_keys(header, keys, delimiter='_', pos=0):
        _keys = [key.upper() for key in keys]
        return (header.upper().split(delimiter)[pos] in _keys)    

    def filter_by_countries(self, countries):
        _data = self._data[[col for col in self._data.columns if self.match_header_n_keys(col, countries, delimiter='_',pos=0)]]
        return Swaps(_data)

    def filter_by_fwd(self, term):
        _data = self._data[[col for col in self._data.columns if self.match_header_n_key(col,"{}Y".format(term).upper(), delimiter='_',pos=1)]]
        return Swaps(_data)
        
    def filter_by_tenor(self, term):
        _data = self._data[[col for col in self._data.columns if self.match_header_n_key(col,"{}Y".format(term).upper(), delimiter='_',pos=2)]]
        return Swaps(_data)
    
    @property
    def data(self):
        return self._data        



#load
#setup
file_date = "2017-05-24"
file_path = "//capricorn/ausfi/yw/dumpingground/hangon_repo/app_bbg_data/Output/Historical/"
input_file = file_path + file_date + "_STIRFUT_historical.csv"
frontend_data = extn.create_datetime_df(pd.read_csv(input_file),key ="date",utc=False)
frontend_data = 100 - frontend_data
for i in  frontend_data.columns.tolist():
    print i

frontend_data.head()

#%%

instrument1 = 'ED'
instrument2 = 'BA'
instrument3 = 'L'
instrument4 = 'IR'
instrument5 = 'ZB'



instruments = [ instrument1 + str(i) for i in [1,2,3,4,5,6,7,8]]
#instruments = [ instrument2 + str(i) for i in [1,2,3,4,5,6,7,8]]
instruments = [instrument1 + str(i) for i in [5,6,7,8]] \
+ [ instrument2 + str(i) for i in [5,6,7,8] ] \
+ [ instrument3 + str(i) for i in [5,6,7,8]  ] \
+ [ instrument4 + str(i) for i in [5,6,7,8] ] \
+ [ instrument5 + str(i) for i in [5,6,7,8] ]  
               


selected_frontend_data = frontend_data[instruments].ix['2016-11-01':].ffill()
selected_frontend_data_returns = rtn.simple_level_diff(selected_frontend_data,5)
selected_frontend_data_pca = PCA(selected_frontend_data).calc()
selected_frontend_data_returns_pca = PCA(selected_frontend_data_returns).calc()



#%%
instrument1 = 'ED'
instrument2 = 'BA'

selected_frontend_spreads = {} 

for item in ({instrument1 + str(i) + instrument1 + str(i-1) :frontend_data[instrument1 + str(i)] - frontend_data[instrument1 + str(i-1)] for i in [5,6,7,8]}, 
             {instrument2 + str(i) + instrument2 + str(i-1) :frontend_data[instrument2 + str(i)] - frontend_data[instrument2 + str(i-1)] for i in [5,6,7,8]}):
    selected_frontend_spreads.update(item)


selected_frontend_spreads = pd.DataFrame(selected_frontend_spreads).ix['2016-11-01':].ffill()
selected_frontend_spreads_returns = rtn.simple_level_diff(selected_frontend_spreads,5)
selected_frontend_spreads_pca = PCA(selected_frontend_spreads).calc()
selected_frontend_spreads_returns_pca = PCA(selected_frontend_spreads_returns).calc()




#%%

pca_obj = selected_frontend_spreads_pca


print "===========>corr<=============="
print pca_obj.corr
print "===========>corr<=============="

pca_obj.plot_pc_eigens(20,20)

pca_obj.get_reconstruct_valuation_by_n_pc()

plt.figure()

pca_obj.plot_pcs(20,20,dps=150)

plt.figure()

for i in range(len(pca_obj.eigvecs)):
    idx = i + 1
    print "Eigen Vector {}:".format(idx)
    print pca_obj.get_eigvec_by_idx(i)



#%%


selected_frontend_spreads[['ED8ED7','ED5ED4','ED7ED6v ']].plot()


