#%%
from __future__ import division
from playground._playground_setup import *
import numpy as np
import pandas as pd
import statistics.data.extension as extn
import statistics.general.returns as rtn
from statistics.pca import *

#setup
file_date = "2017-05-03"
file_path = "//capricorn/ausfi/yw/dumpingground/hangon_repo/app_bbg_data/Output/Historical/"
input_file = file_path + file_date + "_AUUS_FUTBOX_historical.csv"


#load
fut_data = extn.create_datetime_df(pd.read_csv(input_file),key ="date",utc=False)
for i in  fut_data.columns.tolist():
    print i


#%%


futbox_data = fut_data.ffill().ix['2016-10-31':]

futbox_data = pd.DataFrame({'AU': futbox_data.AU10 - futbox_data.AU3, 
    'US': futbox_data.US10 - futbox_data.US2})

futbox_data_returns = rtn.simple_level_diff(futbox_data,1)

futbox_data_pca = PCA(futbox_data).calc()

futbox_data_returns_pca = PCA(futbox_data_returns).calc()



#%%
pca_obj = futbox_data_pca
pca_obj = futbox_data_returns_pca


print "===========>corr<=============="
print pca_obj.corr
print "===========>corr<=============="

pca_obj.plot_pc_eigens(10,5)

pca_obj.get_reconstruct_valuation_by_n_pc()

plt.figure()

pca_obj.plot_pcs(15,5,dps=50)

plt.figure()

for i in range(len(pca_obj.eigvecs)):
    idx = i + 1
    print "Eigen Vector {}:".format(idx)
    print pca_obj.get_eigvec_by_idx(i)



#%%

pc1_level = pca_obj.get_pcs()["PC1"]
x = pca_obj.data.CAD_5Y_5Y - pca_obj.data.SEK_5Y_5Y * (0.791 / 0.611)
check = pd.DataFrame({"x":x, "pc1":pc1_level})

check.corr()



#%%

pc1_level = pca_obj.get_pcs()["PC1"]
x = pca_obj.data.HKD_5Y_5Y - pca_obj.data.USD_5Y_5Y * (0.6554/0.7553)
check = pd.DataFrame({"x":x, "pc1":pc1_level})
check.tail(140).corr()
#%%
plt.figure()
x.plot()

