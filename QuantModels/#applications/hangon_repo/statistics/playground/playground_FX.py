
#%%
from __future__ import division
import os
import sys
import inspect
import pandas as pd
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
import matplotlib.cm as cm

def add_path(p):
    if p not in sys.path:
        print "adding {}".format(p)
        sys.path.insert(0, p)

def config_path():
    """Config the path"""
    path = os.path.split(inspect.getfile(inspect.currentframe()))[0]
    path = os.path.realpath(os.path.abspath(path))
    parent_path = os.path.abspath(os.path.join(path, os.pardir))

    statistics_lib_path = os.path.abspath(os.path.join(parent_path, "statistics"))
    add_path(statistics_lib_path)

    print "sys.path:"
    for p in sys.path:
        print p

def create_df(raw_data):  
    data = raw_data  
    data.date = pd.to_datetime(data.date)
    data = data.set_index("date")    
    return data

config_path()

from statistics.pca import *
import statistics.general.returns as rtn
from pytz import timezone
import pytz

p = "//capricorn/ausfi/yw/dumpingground/hangon_repo/app_bbg_data/Output/Historical/"
FX = create_df(pd.read_csv(p + "2017-04-07_FX_historical.csv")).ffill().ix['2015-12-31':]
FX_returns = rtn.simple_pct_return(FX,5)
FX_returns_pca = PCA(FX_returns).calc()
FX_levels_pca = PCA(FX).calc()


#%%
print "====================================="
print "===========>Return PCA<=============="
print "====================================="


pca_obj = swap10_returns_pca

print pca_obj.corr
print "===========><=============="

pca_obj.plot_pc_eigens(16,16)

pca_obj.get_reconstruct_valuation_by_n_pc()

print "Eigen Vector 1:"
print pca_obj.get_eigvec_by_idx(1)


print "Eigen Vector 2:"
print pca_obj.get_eigvec_by_idx(2)



pc='PC1'
plt.figure()
pca_obj.get_pcs()[pc].plot(title=pc)

pc='PC2'
plt.figure()
pca_obj.get_pcs()[pc].plot(title=pc)

pc='PC3'
plt.figure()
pca_obj.get_pcs()[pc].plot(title=pc)

plt.figure()
pca_obj.get_reconstruct_valuation_by_n_pc(n = [1,2])[0].tail(200).plot(figsize=(10,10))
