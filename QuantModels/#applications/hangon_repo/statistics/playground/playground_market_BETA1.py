#%%
from __future__ import division
import __builtin__
from IPython.lib import deepreload
__builtin__.reload = deepreload.reload
from playground._playground_setup import *
import numpy as np
import pandas as pd
import statistics.data.extension as extn
import statistics.general.returns as rtn
from statistics.pca import *
from playground._playground_market_watcher_setup import *



class Swaps(object):
    def __init__(self,data):
        """
        Args:
            data (pandas dataframe): loaded data. The column headers have the pattern "Country_FwdY_TenorY" 
        """
        self._data = data
    @staticmethod  
    def match_header_n_key(header, key, delimiter='_', pos=0):
        return (header.upper().split(delimiter)[pos] == key)
    
    @staticmethod  
    def match_header_n_keys(header, keys, delimiter='_', pos=0):
        _keys = [key.upper() for key in keys]
        return (header.upper().split(delimiter)[pos] in _keys)    

    def filter_by_countries(self, countries):
        _data = self._data[[col for col in self._data.columns if self.match_header_n_keys(col, countries, delimiter='_',pos=0)]]
        return Swaps(_data)

    def filter_by_fwd(self, term):
        _data = self._data[[col for col in self._data.columns if self.match_header_n_key(col,"{}Y".format(term).upper(), delimiter='_',pos=1)]]
        return Swaps(_data)
        
    def filter_by_tenor(self, term):
        _data = self._data[[col for col in self._data.columns if self.match_header_n_key(col,"{}Y".format(term).upper(), delimiter='_',pos=2)]]
        return Swaps(_data)
    
    def filter_by_fwds(self, terms):
        assert isinstance(terms, (list))        
        _data = pd.concat([self.filter_by_fwd(term).data for term in terms], axis=1)        
        return Swaps(_data)
    
    def filter_by_tenors(self, terms):
        assert isinstance(terms, (list))        
        _data = pd.concat([self.filter_by_tenor(term).data for term in terms], axis=1)        
        return Swaps(_data)
    
    @property
    def data(self):
        return self._data        



#load
#setup
file_date = "2017-07-05"
file_path = "C:/Dev/hangon_repo/app_bbg_data/Output/Historical/"
input_file = file_path + file_date + "_BETA1_historical.csv"
#swap_data = Swaps(extn.create_datetime_df(pd.read_csv(input_file),key ="date",utc=False))

beta1_data = extn.create_datetime_df(pd.read_csv(input_file),key ="date",utc=False)

for i in  beta1_data.columns.tolist():
    print i

print beta1_data.index.min()
print beta1_data.index.max()


#beta1_data = beta1_data[['EDH8','EDH9','USYC2Y5','EDH8EDH9','BE02','BE05','BE10']]
beta1_data = beta1_data[['LIBOR9M3M','LIBOR21M3M','US2Y','US5Y','BE02','BE05']]

beta1_data_level = beta1_data.ffill()
beta1_data_return = rtn.simple_level_diff(beta1_data_level,5) #weekly return

beta1_data_level_2y = beta1_data.ffill().loc['2015-07-01':]
beta1_data_return_2y = rtn.simple_level_diff(beta1_data_level,5).loc['2015-07-01':] #weekly return


beta1_data_level_pca = PCA(beta1_data_level).calc()
beta1_data_return_pca = PCA(beta1_data_return).calc()

beta1_data_level_2y_pca = PCA(beta1_data_level_2y).calc()
beta1_data_return_2y_pca = PCA(beta1_data_return_2y).calc()





#%%

pca_obj = beta1_data_level_pca
pca_obj = beta1_data_level_2y_pca



print "===========>corr<=============="
print pca_obj.corr
print "===========>corr<=============="

pca_obj.plot_pc_eigens(15,15)


plt.figure()

pca_obj.plot_reconstructed_value_gap(15, 10, n=[1,2], dps=500)

plt.figure()

pca_obj.plot_pcs(15,10,dps=150)

plt.figure()

for i in range(len(pca_obj.eigvecs)):
    idx = i + 1
    print "Eigen Vector {}:".format(idx)
    print pca_obj.get_eigvec_by_idx(i)



#%%
test = pca_obj.get_pca_implied_valuations_gaps(n=[2,3])
test.iloc[:,0].name
test[test.columns[0]]


#%%

pc1_level = pca_obj.get_pcs()["PC1"]
x = pca_obj.data.CAD_5Y_5Y - pca_obj.data.SEK_5Y_5Y * (0.791 / 0.611)
check = pd.DataFrame({"x":x, "pc1":pc1_level})

check.corr()



#%%

pc1_level = pca_obj.get_pcs()["PC1"]
x = pca_obj.data.HKD_5Y_5Y - pca_obj.data.USD_5Y_5Y * (0.6554/0.7553)
check = pd.DataFrame({"x":x, "pc1":pc1_level})
check.tail(140).corr()
#%%
plt.figure()
x.plot()

