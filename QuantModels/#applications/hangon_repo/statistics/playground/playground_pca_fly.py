
#%%
from __future__ import division
import os
import sys
import inspect
import pandas as pd
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
import matplotlib.cm as cm
import statsmodels.api as sm

def add_path(p):
    if p not in sys.path:
        print "adding {}".format(p)
        sys.path.insert(0, p)

def config_path():
    """Config the path"""
    path = os.path.split(inspect.getfile(inspect.currentframe()))[0]
    path = os.path.realpath(os.path.abspath(path))
    parent_path = os.path.abspath(os.path.join(path, os.pardir))

    statistics_lib_path = os.path.abspath(os.path.join(parent_path, "statistics"))
    add_path(statistics_lib_path)

    print "sys.path:"
    for p in sys.path:
        print p

def create_bar_df(raw_data):  
    data = raw_data  
    data.time = pd.to_datetime(data.time, utc=True)
    data = data.set_index("time")
    data.index.tz_localize('UTC').tz_convert('Australia/Sydney')
    return data

config_path()

from statistics.pca import *
import statistics.general.returns as rtn
from pytz import timezone
import pytz



#%%

country = "US"
date_str = "2017-04-19"

p = "//capricorn/ausfi/yw/dumpingground/hangon_repo/app_bbg_data/Output/Bar/"
sw02 = create_bar_df(pd.read_csv(p + date_str + "_" +country+ "SW02_Daily_bar.csv"))
sw05 = create_bar_df(pd.read_csv(p + date_str + "_" +country+ "SW05_Daily_bar.csv"))
sw10 = create_bar_df(pd.read_csv(p + date_str + "_" +country+ "SW10_Daily_bar.csv"))
sw30 = create_bar_df(pd.read_csv(p + date_str + "_" +country+ "SW30_Daily_bar.csv"))

freq = '1D'    
d_1H = pd.DataFrame({
    "sw02": sw02.close.resample(freq).apply(lambda d: d[-1] if len(d) > 0 else np.NaN),
    "sw05": sw05.close.resample(freq).apply(lambda d: d[-1] if len(d) > 0 else np.NaN),
    "sw10": sw10.close.resample(freq).apply(lambda d: d[-1] if len(d) > 0 else np.NaN),
    #"sw30": sw30.close.resample(freq).apply(lambda d: d[-1] if len(d) > 0 else np.NaN)
    }).dropna()
    #.ix['2016-12-12':]

d_1H_returns = rtn.simple_level_diff(d_1H,1)
#d_1H_returns = d_1H
d_1H_returns_pca = PCA(d_1H_returns).calc()

d_1H_levels_pca = PCA(d_1H).calc()


#%%
#d_1H.head()
#d_1H_returns.plot()
#print list(range(0,stop=2))



#fig_size = [0, 1, 2, 3, 4, 5, 7, 8, 9]

#for i in range(len(fig_size)):
#    grid_idx = 6 if i < (len(fig_size) - 1) else len(fig_size) % 6
#    print grid_idx
    

#%%
print "====================================="
print "===========>Return PCA<=============="
print "====================================="

print "===========><=============="

pca_obj = d_1H_returns_pca

print pca_obj.corr
print "===========><=============="

pca_obj.plot_pc_eigens(10,5)

pca_obj.get_reconstruct_valuation_by_n_pc()

plt.figure()

pca_obj.plot_pcs(15,5,dps=100)

plt.figure()

for i in range(len(pca_obj.eigvecs)):
    idx = i + 1
    print "Eigen Vector {}:".format(idx)
    print pca_obj.get_eigvec_by_idx(i)


for i in range(len(pca_obj.eigvecs)):
    idx = i + 1
    rng = list(range(1,idx+1)) # +1 again, as the right bound is not inclusive
    plt.figure()
    pca_obj.get_reconstruct_valuation_by_n_pc(n = rng)[0].tail(100).plot(figsize=(10,10))


#%%
print "====================================="
print "===========>Level PCA<=============="
print "====================================="

pca_obj = d_1H_levels_pca

print "===========><=============="

print pca_obj.corr
print "===========><=============="

pca_obj.plot_pc_eigens(10,3)

pca_obj.get_reconstruct_valuation_by_n_pc()

plt.figure()

pca_obj.plot_pcs(15,3,dps=100)

plt.figure()


for i in range(len(pca_obj.eigvecs)):
    idx = i + 1
    print "Eigen Vector {}:".format(idx)
    print pca_obj.get_eigvec_by_idx(i)



for i in range(len(pca_obj.eigvecs)):
    idx = i + 1
    rng = list(range(1,idx+1)) # +1 again, as the right bound is not inclusive
    plt.figure()
    pca_obj.get_reconstruct_valuation_by_n_pc(n = rng)[0].tail(200).plot(figsize=(10,10))




#%%


pca_fly = d_1H.sw05 * 4.815 + d_1H.sw02 * (-0.973028) * 1.977 +  d_1H.sw10 * (-0.3699) * 9.2  
bpv_neutral_fly = d_1H.sw05 *  - d_1H.sw02 -  d_1H.sw10
bpv_neutral_curve = d_1H.sw10 *  - d_1H.sw02



pc1_level = pca_obj.get_pcs()["PC1"]
pc2_slope = pca_obj.get_pcs()["PC2"]
pc3_curve = pca_obj.get_pcs()["PC3"]

pca_fly_analysis_df = pd.DataFrame({"pca_fly":pca_fly,
                                    "pc1_level":pc1_level,
                                    "pc2_slope":pc2_slope,
                                    "pc3_curve":pc3_curve
                                    })

corr_analysis_df    = pd.DataFrame({"bpv_neutral_fly":bpv_neutral_fly,  
                                    "bpv_neutral_curve":bpv_neutral_curve,
                                    "pca_fly":pca_fly,
                                    "level":pc1_level,
                                    "slope":pc2_slope,
                                    "US2":d_1H.sw02,
                                    "US5":d_1H.sw05,
                                    "US10":d_1H.sw10
                                    })

corr_analysis_result =corr_analysis_df.corr()

corr_analysis_result    = corr_analysis_result[["level","slope","US2","US5","US10"]]

corr_analysis_result    = corr_analysis_result[3:5]



print "====================================="
print corr_analysis_result

print "====================================="
print pca_fly_analysis_df.corr()

print "====================================="
print "====================================="
print "====================================="

print sm.OLS(pca_fly, pc1_level).fit().summary()

print "====================================="
print "====================================="
print "====================================="

print sm.OLS(pca_fly, pc2_slope).fit().summary()

print "====================================="
print "====================================="
print "====================================="
plt.figure()

plt.scatter(pca_fly, pc1_level)
#plt.axis([-1, 1, -1, 1])
plt.grid(True)

plt.figure()

plt.scatter(pca_fly, pc2_slope)
#plt.axis([-1, 1, -1, 1])
plt.grid(True)


#plt.axis([-1, 1, -1, 1])
#plt.grid(True)


#%%
#from pytz import all_timezones
#print len(all_timezones)
#for zone in all_timezones:
#    print zone