#%%
import pandas as pd
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
%matplotlib inline


def bootstrap_ts(data = None, dates = None):
    """bootstrap ts"""

    _data = data if data is not None else [1, 2, 3.3, 6.35, 9.5, 6, 7.7, 12, 20, 25.1]
    _dates = dates if dates is not None else ['2017-02-01','2017-02-02','2017-02-03','2017-02-04','2017-02-05',
                          '2017-02-06','2017-02-07','2017-02-08','2017-02-09','2017-02-10']

    data = pd.Series(data=_data, index=_dates)
    data.index = pd.to_datetime(data.index)
    data.index.name = "DATE"
    return data


def linear_autogregession(data, n=1):
    """ This function takes in an array and do an linear autoregression on lag of n
        Parameters
        ----------
        data: int
            an array of numbers
        n: int
            the lag. it is default to 1
        returns
        ----------
        a: float 
            the coefficeint for the constant
        b: float
            the coefficient for the first degree independant variable
        errs: an array of float
            the error term    
        xs: an array of float
            the independant variable sequence 
        ys:  an array of float
            the dependant variable sequence 
        y_hats: an array of float
            the estimated dependant variable sequence 
            
    """    
    xs = data[n:]    
    ys = data[:-n]
    (b, a), resi, _, _, _ =  np.polyfit(xs, ys, 1, full=True) 
    f = lambda x: a + b * x
    y_hats = [f(x) for x in xs]
    errs = [ y - y_hat for y, y_hat in zip(ys, y_hats)]
    return a, b, errs, xs, ys, y_hats


ts = bootstrap_ts()
data = ts.iloc[:-1].values

#%%


#%%

a, b, errs, xs, ys, y_hats = linear_autogregession(ts.values, n=1)
result = pd.DataFrame({"ys":ys, "xs":xs, "ys_hat":y_hats, "err":errs})
#result.plot(figsize=(15,15))
_delta_t = 1
lambda_hat = np.log(b)/_delta_t
mu_hat = a /(1 - b)
sigma_hat = np.std(errs, ddof=1) * np.sqrt((2 * lambda_hat)/(1 - np.exp(-2 * lambda_hat * _delta_t)))



#%%













