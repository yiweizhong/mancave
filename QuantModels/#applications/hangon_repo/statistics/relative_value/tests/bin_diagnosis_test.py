"""
unitTest
"""
from __future__ import division
import os
import sys
import inspect
import numpy as np
import pandas as pd
import unittest
import statistics.data.extension as extn
import relative_value.bin_diagnosis as bd
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm


def bootstrap_ts(data = [1, 20, 3.3, 3.35, 5.5, 6, 7.7, 8, 9, 10.1], 
                 dates = ['2017-02-01','2017-02-02','2017-02-03','2017-02-04','2017-02-05',
                          '2017-02-06','2017-02-07','2017-02-08','2017-02-09','2017-02-10']):
    """bootstrap ts"""
    data = pd.Series(data=data, index=dates)
    data.index = pd.to_datetime(data.index)
    data.index.name = "DATE"
    return data

def load_bbg_historical_data(file_path, key_col = 'date'):
    data = extn.create_datetime_df(pd.read_csv(file_path),key = key_col,utc=False)
    return data
    
class BinDiagnosisTest(unittest.TestCase):
    def setUp(self):
        """
        """
        self.date_idx = "DATE"
        plt.ion()

    @unittest.skip('')
    def test_env_setup(self):
        """make sure testenv is ready"""
        self.assertEqual(1, 1)



    def test_shift(self):
        data = bootstrap_ts()
        print data
        print data.shift(-1)
        print data.shift(-1) - data


    def test_run_bin_diagnositc(self):
        data = bootstrap_ts()
        mr_obj = bd.BinDiagnosis(data)
        
        print "============"
        #print mr_obj.bin_data
        print ""

        print "============"
        #print mr_obj.bin_diffusion_coefficients
        print ""

        print "============"
        print mr_obj.bin_drift_coefficients
        print ""


    def  test_run_bin_diagnositc_with_swap(self):
        
        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "data/FWD_historical.csv"))                                 
        
        fwd_rates = load_bbg_historical_data(file_path).tail(180)
        
        data = fwd_rates['CAD_5Y_5Y'] - fwd_rates['SEK_5Y_5Y']
        data.name = 'CDSK55' 
        
        #print fwd_rates.head()
        

        fig = plt.figure(figsize=(8,8))
        gs = gridspec.GridSpec(2, 1, height_ratios=[1, 1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])
        ax1 = plt.subplot(gs[1])
        mr_obj = bd.BinDiagnosis(data, offset = 1, shift_forward =True)
        mr_obj.plot_drift_coefficients(ax0, ignore_bin = [])
        mr_obj.plot_diffusion_coefficients(ax1, ignore_bin = [])
        plt.tight_layout()
        plt.show(block=True)

        mm_obj = bd.BinDiagnosis(data, offset = 5, shift_forward = False)

        fig = plt.figure(figsize=(8,8))
        gs = gridspec.GridSpec(2, 1, height_ratios=[1, 1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])
        ax1 = plt.subplot(gs[1])
        mm_obj = bd.BinDiagnosis(data, offset = 5, shift_forward =False)
        mm_obj.plot_drift_coefficients(ax0, ignore_bin = [])
        mm_obj.plot_diffusion_coefficients(ax1, ignore_bin = [])
        plt.tight_layout()
        plt.show(block=True)


        
    def test_create_bins_from_data(self):
        data = bootstrap_ts()
        mr_obj = bd.BinDiagnosis(data)
        print r"\n"
        print mr_obj.create_bins_from_data()

    
    def test_plot_drift_coefficients(self):
        file_path = r'C:/Dev/hangon_repo/statistics/statistics/tests/data/FWD_historical.csv'        
        fwd_rates = load_bbg_historical_data(file_path).tail(180)
        data = fwd_rates['CAD_5Y_5Y'] - fwd_rates['SEK_5Y_5Y']
        data.name = 'CDSK55' 
        fig = plt.figure(figsize=(8,4))
        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
        ax = plt.subplot(gs[0])

        mr_obj = bd.BinDiagnosis(data)
        mr_obj.plot_drift_coefficients(ax)
        plt.tight_layout()
        plt.show(block=True)

    def test_plot_diffusion_coefficients(self):
        file_path = r'C:/Dev/hangon_repo/statistics/statistics/tests/data/FWD_historical.csv'        
        fwd_rates = load_bbg_historical_data(file_path).tail(180)
        data = fwd_rates['CAD_5Y_5Y'] - fwd_rates['SEK_5Y_5Y']
        data.name = 'CDSK55' 
        fig = plt.figure(figsize=(8,4))
        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
        ax = plt.subplot(gs[0])

        mr_obj = bd.BinDiagnosis(data)
        mr_obj.plot_diffusion_coefficients(ax)
        plt.tight_layout()
        plt.show(block=True)

    
    def test_plot_drift_diffusion_coefficients(self):
        
        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "data/Swaption_IVol_historical.csv"))                         

        vol = load_bbg_historical_data(file_path).tail(2000)
        data = vol['USD1Y10Y'].dropna()
        data.name = 'USD1Y10YIVOL' 
        #plt.show(block=True)

        
        fig = plt.figure(figsize=(8,8))
        gs = gridspec.GridSpec(2, 1, height_ratios=[1, 1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])
        ax1 = plt.subplot(gs[1])
        mr_obj = bd.BinDiagnosis(data, offset = 30)
        mr_obj.plot_drift_coefficients(ax0, ignore_bin = [])
        mr_obj.plot_diffusion_coefficients(ax1, ignore_bin = [])
        plt.tight_layout()
        plt.show(block=True)
    

        

if __name__ == '__main__':
    unittest.main()
