"""
RV MV supplements
"""
from __future__ import division
import os
import sys
import inspect
import numpy as np
import pandas as pd
import unittest
import statistics.data.extension as extn
import relative_value.stochastic as st
import relative_value.bin_diagnosis as bd
import relative_value.nonparametric as npmr
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
import matplotlib.ticker as ticker
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm
import xlwings as xw



def load_bbg_historical_data(file_path, key_col = 'date'):
    data = extn.create_datetime_df(pd.read_csv(file_path),key = key_col,utc=False)
    return data

def match_header_n_keys(header, keys, delimiter='_', pos=0):
    _keys = [key.upper() for key in keys]
    return (header.upper().split(delimiter)[pos] in _keys)   

def match_header_n_key(header, key, delimiter='_', pos=0):
    return (header.upper().split(delimiter)[pos] == key) 

def filter_by_countries(countries):
    _data = self._data[[col for col in data.columns if match_header_n_keys(col, countries, delimiter='_',pos=0)]]
    return _data

def filter_by_fwd(data, term):
    _data = data[[col for col in data.columns if match_header_n_key(col,"{}Y".format(term).upper(), delimiter='_',pos=1)]]
    return _data
    
def filter_by_tenor(data, term):
    _data = data[[col for col in data.columns if match_header_n_key(col,"{}Y".format(term).upper(), delimiter='_',pos=2)]]
    return _data

def filter(data, fwd = None, tenor = None , countries = None):
    _d = data
    if fwd is not None:
        _d = filter_by_fwd(data, fwd)
    if tenor is not None:
        _d = filter_by_tenor(_d, tenor)
    if countries is not None:
        _d =filter_by_countries(countries)
    return _d

def get_country_combos(countries):
    import itertools
    combos = list(itertools.product(countries, repeat=2))
    exclude_self_combo = [(c[0], c[1]) for c in combos if c[0] != c[1]]
    return exclude_self_combo

def get_pair_series(data):
    combos = get_country_combos(data.columns)
    ds = {}
    for c in combos:
        df = pd.concat([data[c[0]], data[c[1]]], axis=1)
        df = df.dropna()
        s = df[c[0]] - df[c[1]]
        s.name = "{}_VS_{}".format(c[0],c[1])
        ds[s.name] = s
    return ds 

def plot_footer(ax,**kwargs):
        ax.axis('off')
        #date = kwargs.get('date','')
        ax.text(0.01,
                0.02,
                'Source: AMPCI Global Fixed Income - Macro Market, Bloomberg',
                verticalalignment='top',
                horizontalalignment='left',
                transform=ax.transAxes,
                color='black',
                fontsize=8,
                fontstyle='italic'
                )
        """
        ax.text(0.01,
                0.3,
                'As of: ' + date,
                verticalalignment='top',
                horizontalalignment='left',
                transform=ax.transAxes,
                color='black',
                fontsize=8,
                fontstyle='italic'
                )
        """

class RvMvSupplementTest(unittest.TestCase):
    def setUp(self):
        """"""

        
        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "data/2017-10-19_ben_historical.csv"))                         
        fwd_rates = load_bbg_historical_data(file_path).ffill()                
        #data = (fwd_rates['CAD_5Y_5Y'] - fwd_rates['SEK_5Y_5Y'])
        #data.name = 'CDSK55'   
        self.fwd_rates = fwd_rates

        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "output/RV_MV_framework.xlsm"))
        #self.outputWkb = xw.Book(file_path)
        
        self.output_path = os.path.abspath(os.path.join(path, "output/"))
        self.output_chart_path = os.path.abspath(os.path.join(self.output_path, "charts/"))

        #plt.rc('text', usetex=True)
        #plt.ion()


    

    #@unittest.skip('')    
    def test_create_bin_diagnosis_chart(self):

        history_length = 1000
        #fwd5y5y = filter(self.fwd_rates,5, 5).tail(history_length)
        #fwd5y5y = get_pair_series(fwd5y5y)
        #fwd5y5y = {k:v for k,v in fwd5y5y.iteritems() if "EURUSDV1Y" in k}

        #AUDCAD105H = (self.fwd_rates["AUD_10Y_5Y"] + (-1.27968597) * self.fwd_rates["CAD_10Y_5Y"]).loc['2017-01-01':]
        #AUDCAD105H.name = "AUDCAD105H"
        #draw
        #for ds in fwd5y5y.values()[:]:  
        #  
        #for ds in [AUDCAD105H]:  
        fwd5y5y = {'EURUSDV1Y':self.fwd_rates['EURUSDV1Y'].dropna()}

        for ds in fwd5y5y.values()[:]:     
            fig = plt.figure(figsize=(8,9))
            gs = gridspec.GridSpec(3, 1, height_ratios=[10, 10, 1], width_ratios=[1])

            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])
            ax2 = plt.subplot(gs[2])

            mr_obj = bd.BinDiagnosis(ds, offset = 1, shift_forward =True)
            mr_obj.plot_drift_coefficients(ax0, ignore_bin = [])
            mr_obj.plot_diffusion_coefficients(ax1, ignore_bin = [])
            plot_footer(ax2)
            plt.tight_layout()
            #plt.show(block=True)
            f = os.path.join(self.output_chart_path, ds.name + "_MR_Diagnosis.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()

        for ds in fwd5y5y.values()[:]:
            fig = plt.figure(figsize=(8,9))
            gs = gridspec.GridSpec(3, 1, height_ratios=[50, 50, 1], width_ratios=[1])
            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])
            ax2 = plt.subplot(gs[2])

            mm_obj = bd.BinDiagnosis(ds, offset = 5, shift_forward =False)
            mm_obj.plot_drift_coefficients(ax0, ignore_bin = [20])
            mm_obj.plot_diffusion_coefficients(ax1, ignore_bin = [20])
            plot_footer(ax2)
            plt.tight_layout()
            #plt.show(block=True)
            f = os.path.join(self.output_chart_path, ds.name + "_MM_Diagnosis.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()



##################################################################################################################################
        
    #@unittest.skip('')    
    




##################################################################################################################################

    #@unittest.skip('')
    def test_create_expected_moment_and_annualised_sharpratio_chart(self):
        
        history_length = 1000
        simulation_horizon = 60
        #fwd5y5y = filter(self.fwd_rates,5, 5).tail(history_length)
        #fwd5y5y = get_pair_series(fwd5y5y)
        fwd5y5y = {'EURUSDV1Y':self.fwd_rates['EURUSDV1Y'].dropna()}

        def pseudo_sharpe_ratio(x0, means, variances, days_in_a_year):
            f = lambda x : np.sqrt(x)
            result = []
            expanding_returns = means - x0 if means.mean() >= x0 else x0 - means
            pseudo_sr = expanding_returns/variances.apply(f)
            i = 1
            for idx, item in pseudo_sr.iteritems():
                result.append(item * np.sqrt(days_in_a_year/i)) 
                i = i + 1
            annualised_pseudo_sr = pd.Series(result, index = pseudo_sr.index)
            
            annualised_pseudo_sr.name = "Expected Sharpe ratio (annualised) for the holding horizon"

            return annualised_pseudo_sr

        def plot_expected_annualised_sharpe_ratio(ax, model, simulation_horizon, ax_note = None):
                
            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            name = model.data.name
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life
            
            title = "{} {} days horizon Sharpe ratio (annualised) estimation using {} days history ".format(name, model_horizon, history_length)

            means = model.estimate_conditional_means(include_history=True)
            variances =  model.estimate_conditional_variances(include_history=True)
            stdevs = model.conditional_stdevs

            conditional_means = model.estimate_conditional_means(n=simulation_horizon, include_history = False)["Conditional expected value"]
            conditional_variances = model.estimate_conditional_variances(n = simulation_horizon, include_history = False)["Conditional variance"]
            annualised_pseudo_sharpe = pseudo_sharpe_ratio(model.data.values[-1], conditional_means, conditional_variances, 252)            
            annualised_pseudo_sharpe.plot(ax=ax, lw=0.5)
            ax.set_title(title, y=1.02, fontsize = 9)

            #formatting
            ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            plt.setp(ax.xaxis.get_majorticklabels(), fontsize = 8)
            plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)
            ax.set_xlabel('Days', fontsize=8)
            ax.set_ylabel('Level', fontsize=8)
            leg = ax.legend(fancybox=True, prop={'size': 8})
            leg.get_frame().set_facecolor('none')
            leg.get_frame().set_linewidth(0.0)

            if ax_note is not None:
                ax_note.axis('off')
                font = {'color': 'black', 'weight': 'light', 'size': 9}
                font_group_title = {'color': 'black', 'weight': 'semibold', 'size': 9}


                conditional_expected_val = r"Conditional expected value:" 
                conditional_expected_val_form = r"$x_0e^{ \theta t} + \mu (1 - e^{ \theta t})$"
                conditional_variance = r"Conditional variance:"
                conditional_variance_form = r"$\frac{ \sigma ^2(1 - e^{-2 \theta t} )}{2 \theta}$"
                x0_txt = r"$x_0:$ most recent history [{0:.2f}]".format(x0)
                t_txt = r"t: each day within the horizon"
                theta_txt = r"$\hat \theta: $ mean reversion speed estimation [{0:.2f}]".format(theta)
                mu_txt =  r"$\hat \mu: $ mean estimation [{0:.2f}]".format(mu)
                sigma_txt =  r"$\hat \sigma: $ vol estimation [{0:.2f}]".format(sigma)
                half_life_txt =  r"$\hat \lambda: $ half life estimation [{0:.2f}]".format(half_life)

                ax_note.text(-0.01, 1, "Solutions to conditional moments: ", fontdict=font_group_title, verticalalignment = "top")
                ax_note.text(-0.01, 0.8, conditional_expected_val, fontdict=font, verticalalignment = "top")
                ax_note.text(0.25, 0.8, conditional_expected_val_form, fontdict=font,fontsize =9, verticalalignment = "top")
                ax_note.text(-0.01, 0.6, conditional_variance, fontdict=font, verticalalignment = "top")
                ax_note.text(0.2, 0.64, conditional_variance_form, fontdict=font, fontsize = 11, verticalalignment = "top")
                #col 2
                ax_note.text(0.5, 0.8, t_txt, fontdict=font, verticalalignment = "top")

                ax_note.text(-0.01, 0.35, "Mean reversion model parameter estimations:", fontdict=font_group_title, verticalalignment = "top")
                ax_note.text(-0.01, 0.15, mu_txt, fontdict=font, verticalalignment = "top")
                ax_note.text(-0.01, 0.0, x0_txt, fontdict=font, verticalalignment = "top")
                ax_note.text(-0.01, -0.15, theta_txt, fontdict=font, verticalalignment = "top")
                #col 2
                
                ax_note.text(0.5, 0.15, sigma_txt, fontdict=font, verticalalignment = "top")
                ax_note.text(0.5, 0.0, half_life_txt, fontdict=font, verticalalignment = "top")



        for ds in fwd5y5y.values()[:]:
            fig = plt.figure(figsize=(8,8))
            gs = gridspec.GridSpec(3, 1, height_ratios=[30,8,1], width_ratios=[1])

            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])
            ax2 = plt.subplot(gs[2])

            model = st.OrnsteinUhlenbeck(ds, dt = 1)
            plot_expected_annualised_sharpe_ratio(ax0, model, simulation_horizon, ax_note = ax1)
           
            plot_footer(ax2)
            plt.tight_layout()
            f = os.path.join(self.output_chart_path, ds.name + "_MR_Expected_Sharpe_Ratios.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()

        

    @unittest.skip('')
    def test_create_expected_annualised_sharpratio_comparison(self):
        # for 20, 60 and 90 n
        # data to excel
        history_length = 1000
        simulation_horizon = 60
        fwd5y5y = filter(self.fwd_rates, fwd = 5, tenor = 5).tail(history_length)
        fwd5y5y = get_pair_series(fwd5y5y)
        cad5y5y =  {k:v for k,v in fwd5y5y.iteritems() if "CAD_5Y_5Y_VS_" in k}
        cad5y5y =  {k:v for k,v in cad5y5y.iteritems() \
                    if any(["SEK" in k,
                           "NOK" in k,
                           "USD" in k,
                           "NZD" in k,
                           "AUD" in k,
                           "GBP" in k,
                           "DKK" in k])} 
        
        def pseudo_sharpe_ratio(x0, means, variances, name, days_in_a_year):
            f = lambda x : np.sqrt(x)
            result = []
            expanding_returns = means - x0 if means.mean() >= x0 else x0 - means
            pseudo_sr = expanding_returns/variances.apply(f)
            i = 1
            for idx, item in pseudo_sr.iteritems():
                result.append(item * np.sqrt(days_in_a_year/i)) 
                i = i + 1
            annualised_pseudo_sr = pd.Series(result, index = pseudo_sr.index)
            annualised_pseudo_sr.name = name
            return annualised_pseudo_sr

        def plot_expected_annualised_sharpe_ratio_comparison(ax, model, simulation_horizon, ax_note = None):
                
            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            name = model.data.name
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life
            
            title = "{} days horizon Sharpe ratio (annualised) estimation using {} days history ".format(model_horizon, history_length)

            conditional_means = model.estimate_conditional_means(n=simulation_horizon, include_history = False)["Conditional expected value"]
            conditional_variances = model.estimate_conditional_variances(n = simulation_horizon, include_history = False)["Conditional variance"]
            annualised_pseudo_sharpe = pseudo_sharpe_ratio(model.data.values[-1], conditional_means, conditional_variances, name, 252)            
            annualised_pseudo_sharpe.plot(ax=ax, lw=0.5)
            ax.set_title(title, y=1.02, fontsize = 9)

            #formatting
            ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            plt.setp(ax.xaxis.get_majorticklabels(), fontsize = 8)
            plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)
            ax.set_xlabel('Days', fontsize=8)
            ax.set_ylabel('Level', fontsize=8)
            leg = ax.legend(fancybox=True, prop={'size': 8})
            leg.get_frame().set_facecolor('none')
            leg.get_frame().set_linewidth(0.0)

            if ax_note is not None:
                ax_note.axis('off')
                font = {'color': 'black', 'weight': 'light', 'size': 9}
                font_group_title = {'color': 'black', 'weight': 'semibold', 'size': 9}


                conditional_expected_val = r"Conditional expected value:" 
                conditional_expected_val_form = r"$x_0e^{ \theta t} + \mu (1 - e^{ \theta t})$"
                conditional_variance = r"Conditional variance:"
                conditional_variance_form = r"$\frac{ \sigma ^2(1 - e^{-2 \theta t} )}{2 \theta}$"
                x0_txt = r"$x_0:$ most recent history [{0:.2f}]".format(x0)
                t_txt = r"t: each day within the horizon"
                theta_txt = r"$\hat \theta: $ mean reversion speed estimation [{0:.2f}]".format(theta)
                mu_txt =  r"$\hat \mu: $ mean estimation [{0:.2f}]".format(mu)
                sigma_txt =  r"$\hat \sigma: $ vol estimation [{0:.2f}]".format(sigma)
                half_life_txt =  r"$\hat \lambda: $ half life estimation [{0:.2f}]".format(half_life)

                ax_note.text(-0.01, 1, "Solutions to conditional moments: ", fontdict=font_group_title, verticalalignment = "top")
                ax_note.text(-0.01, 0.7, conditional_expected_val, fontdict=font, verticalalignment = "top")
                ax_note.text(0.25, 0.7, conditional_expected_val_form, fontdict=font,fontsize =9, verticalalignment = "top")
                ax_note.text(-0.01, 0.4, conditional_variance, fontdict=font, verticalalignment = "top")
                ax_note.text(0.2, 0.44, conditional_variance_form, fontdict=font, fontsize = 11, verticalalignment = "top")
                #col 2
                ax_note.text(0.5, 0.7, t_txt, fontdict=font, verticalalignment = "top")

        fig = plt.figure(figsize=(8,8))
        gs = gridspec.GridSpec(3, 1, height_ratios=[30,4,1], width_ratios=[1])

        ax0 = plt.subplot(gs[0])
        ax1 = plt.subplot(gs[1])
        ax2 = plt.subplot(gs[2])

        first = True
        for ds in cad5y5y.values()[:]:
            
            model = st.OrnsteinUhlenbeck(ds, dt = 1)
            if first:
                plot_expected_annualised_sharpe_ratio_comparison(ax0, model, simulation_horizon, ax_note = ax1)
                first = False
            else:
                plot_expected_annualised_sharpe_ratio_comparison(ax0, model, simulation_horizon)


        plot_footer(ax2)
        plt.tight_layout()
        f = os.path.join(self.output_chart_path, "MR_Expected_Sharpe_Rati0s_comparison.png")
        plt.savefig(f, dpi=100) #, bbox_inches='tight')
        plt.close()

        

    #@unittest.skip('')
    def test_simulation_target_stop_hit_ratio(self):
        # the simulation will record a series of terminal pnl for one comination of target and stop for 1 pair for a given n. 
        # It will be either the stoploss level or the end value. 
        # The output is a series of terminal values which represents a distribution
        # This should be a histogram with stats description. 
        history_length = 1000
        simulation_horizon = 60
        #fwd5y5y = filter(self.fwd_rates,5, 5).tail(history_length)
        #fwd5y5y = get_pair_series(fwd5y5y)
        fwd5y5y = {'EURUSDV1Y':self.fwd_rates['EURUSDV1Y'].dropna()}

        def plot_hit_count(ax, hit_count, model, k, target_level, stop_level):
            
            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life

            x0_txt = r"$x_0:$ [{0:.2f}]".format(x0)
            theta_txt = r"$\hat \theta: $ [{0:.2f}]".format(theta)
            mu_txt =  r"$\hat \mu: $ [{0:.2f}]".format(mu)
            sigma_txt =  r"$\hat \sigma: $ [{0:.2f}]".format(sigma)
            half_life_txt =  r"$\hat \lambda: $ [{0:.2f}]".format(half_life)
            target_txt =  r"target level: [{0:.2f}]".format(target_level)
            stop_txt =  r"stop level: [{0:.2f}]".format(stop_level)

            xlabel = r"{},  {},  {},  {},  {},  {},  {}".format(x0_txt, theta_txt, mu_txt, sigma_txt, half_life_txt, target_txt, stop_txt)

            hit_count.plot(ax=ax, kind="bar")
            ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            ax.xaxis.set_ticklabels([])
            ax.xaxis.set_ticks_position('none')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)
            ax.set_ylabel('Hit counts', fontsize=8)
            ax.set_xlabel(xlabel, fontsize=8)
            leg = ax.legend(fancybox=True, prop={'size': 8})
            leg.get_frame().set_facecolor('none')
            leg.get_frame().set_linewidth(0.0)
            
            

        def plot_hit_ratio(ax, hit_ratio, model, k, target_level, stop_level):
            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            name = model.data.name
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life
            
            hit_ratio.plot(ax=ax)
            ax.xaxis.grid(True, which='both', linestyle='-', linewidth='0.2')
            ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.minorticks_on()
            ax.xaxis.set_minor_locator(ticker.MultipleLocator(base=1))
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)
            ax.set_ylabel('Hit ratio (%)', fontsize=8)
            leg = ax.legend(fancybox=True, prop={'size': 8})
            leg.get_frame().set_facecolor('none')
            leg.get_frame().set_linewidth(0.0)
            ax.set_xlabel('Horizon day', fontsize=8)
            ax.set_ylim(0, 100)

        for ds in fwd5y5y.values()[:]:
            fig = plt.figure(figsize=(8,8))
            gs = gridspec.GridSpec(3, 1, height_ratios=[30,30,1], width_ratios=[1])

            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])
            ax2 = plt.subplot(gs[2])


            model = st.OrnsteinUhlenbeck(ds, dt = 1)

            target_level = model.mu_hat - 0.1 if model.data.values[-1] > model.mu_hat else model.mu_hat + 0.1
            target_level = 8
            # 10 bps from current level as the stop
            stop_level = model.data.values[-1] + 0.05 if model.data.values[-1] > target_level else model.data.values[-1] - 0.05   
            stop_level = 6


            print "x0 {}".format(model.data.values[-1])
            print "mu hat {}".format(model.mu_hat)
            print "target_level {}".format(target_level)
            print "stop_level {}".format(stop_level)
            k = 10000

            distributions = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = 60, k = k, momentum_factor = 0)

            plot_hit_count(ax0, distributions[["Target hit count","Stop hit count"]], model, k, target_level, stop_level)
            plot_hit_ratio(ax1, distributions[["Target hit ratio","Stop hit ratio","Sideway ratio"]], model, k, target_level, stop_level)

            print "target_level {}".format(target_level)
            
            plot_footer(ax2)
            name = model.data.name
            title = "{0} path simulation for {1}  ".format(k, name)
            fig.suptitle(title, fontsize = 9)
            fig.tight_layout()
            fig.subplots_adjust(top=0.94)
            f = os.path.join(self.output_chart_path, ds.name + "_MR_Target_Hit_Ratios.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()


    @unittest.skip('')
    def test_simulation_target_stop_hit_ratio_with_momentum(self):
     
        history_length = 180
        simulation_horizon = 60
        fwd5y5y = filter(self.fwd_rates,5, 5).tail(history_length)
        fwd5y5y = get_pair_series(fwd5y5y)
        cad5y5y =  {k:v for k,v in fwd5y5y.iteritems() if "CAD_5Y_5Y_VS_" in k}
        cad5y5y =  {k:v for k,v in cad5y5y.iteritems() \
                    if any(["SEK" in k,
                           "NOK" in k,
                           "USD" in k,
                           "NZD" in k,
                           "AUD" in k,
                           "GBP" in k,
                           "DKK" in k])} 
        
        def plot_model_details(ax, model, target_level, stop_level):
            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life

            ax.axis('off')
            x0_txt = r"$x_0:$ [{0:.2f}]".format(x0)
            theta_txt = r"$\hat \theta: $ [{0:.2f}]".format(theta)
            mu_txt =  r"$\hat \mu: $ [{0:.2f}]".format(mu)
            sigma_txt =  r"$\hat \sigma: $ [{0:.2f}]".format(sigma)
            half_life_txt =  r"$\hat \lambda: $ [{0:.2f}]".format(half_life)
            target_txt =  r"target level: [{0:.2f}]".format(target_level)
            stop_txt =  r"stop level: [{0:.2f}]".format(stop_level)

            font_group_title = {'color': 'black', 'weight': 'normal', 'size': 9}
            detail_txt = r"{},  {},  {},  {},  {}".format(x0_txt, theta_txt, mu_txt, sigma_txt, half_life_txt)
            ax.text(-0.01, 0.9, detail_txt, fontdict=font_group_title, verticalalignment = "top")
            detail_txt2 = r"{},  {}".format(target_txt, stop_txt)
            ax.text(-0.01, 0.8, detail_txt2, fontdict=font_group_title, verticalalignment = "top")
                


        def plot_hit_ratio(ax, hit_ratio, model, k, target_level, stop_level, title):
            
            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life

            x0_txt = r"$x_0:$ [{0:.2f}]".format(x0)
            theta_txt = r"$\hat \theta: $ [{0:.2f}]".format(theta)
            mu_txt =  r"$\hat \mu: $ [{0:.2f}]".format(mu)
            sigma_txt =  r"$\hat \sigma: $ [{0:.2f}]".format(sigma)
            half_life_txt =  r"$\hat \lambda: $ [{0:.2f}]".format(half_life)
            xlabel = r"{}, {}, {}, {}, {}".format(x0_txt, theta_txt, mu_txt, sigma_txt, half_life_txt)
            
            hit_ratio.plot(ax=ax)
            ax.xaxis.grid(True, which='both', linestyle='-', linewidth='0.2')
            ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.minorticks_on()
            ax.xaxis.set_minor_locator(ticker.MultipleLocator(base=1))
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)
            ax.set_ylabel('Hit ratio (%)', fontsize=8)
            leg = ax.legend(fancybox=True, prop={'size': 8})
            leg.get_frame().set_facecolor('none')
            leg.get_frame().set_linewidth(0.0)
            ax.set_xlabel('Horizon day ({0})'.format(title), fontsize=8)
            ax.set_ylim(0, 100)
            

        for ds in cad5y5y.values()[:]:
            fig = plt.figure(figsize=(12,8))
            gs = gridspec.GridSpec(3, 2, height_ratios=[30,30,1], width_ratios=[1,1])

            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])
            ax2 = plt.subplot(gs[2])
            ax3 = plt.subplot(gs[3])
            ax4 = plt.subplot(gs[4])
            ax5 = plt.subplot(gs[5])

            model = st.OrnsteinUhlenbeck(ds, dt = 1)

            target_level = model.mu_hat - 0.1 if model.data.values[-1] > model.mu_hat else model.mu_hat + 0.1
            # 10 bps from current level as the stop
            stop_level = model.data.values[-1] + 0.05 if model.data.values[-1] > target_level else model.data.values[-1] - 0.05   

            print "x0 {}".format(model.data.values[-1])
            print "mu hat {}".format(model.mu_hat)
            print "target_level {}".format(target_level)
            print "stop_level {}".format(stop_level)
            k = 10000

            ttl1 = "with no momentum factoed in"
            distributions = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = 60, k = k, momentum_factor = 0)
            plot_hit_ratio(ax0, distributions[["Target hit ratio","Stop hit ratio","Sideway ratio"]], model, k, target_level, stop_level, ttl1)

            plot_model_details(ax1, model, target_level, stop_level) 

            ttl2 = "with 80% momentum from last 5 days factored in"
            distributions1 = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = 60, k = k, 
                                                              momentum_factor = 0.8, momentum_lag = 5, downside_momentum_only = False)
            plot_hit_ratio(ax2, distributions1[["Target hit ratio","Stop hit ratio","Sideway ratio"]], model, k, target_level, stop_level, ttl2)


            ttl3 = "with 80% momentum from last 5 days factored in when it is against the trade"
            distributions2 = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = 60, k = k, 
                                                              momentum_factor = 0.8, momentum_lag = 5, downside_momentum_only = True)
            plot_hit_ratio(ax3, distributions2[["Target hit ratio","Stop hit ratio","Sideway ratio"]], model, k, target_level, stop_level, ttl3)

            plot_footer(ax4)
            ax5.axis('off')
            name = model.data.name
            title = "{0} path simulation for {1}  ".format(k, name)
            fig.suptitle(title, fontsize = 9)
            fig.tight_layout()
            fig.subplots_adjust(top=0.94)
            f = os.path.join(self.output_chart_path, ds.name + "_MR_Target_Hit_Ratios_with_momentum.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()

    @unittest.skip('')
    def test_simulation_target_stop_hit_ratio_with_stop_levels(self):
         
        history_length = 180
        simulation_horizon = 60
        fwd5y5y = filter(self.fwd_rates,5, 5).tail(history_length)
        fwd5y5y = get_pair_series(fwd5y5y)
        cad5y5y =  {k:v for k,v in fwd5y5y.iteritems() if "CAD_5Y_5Y_VS_" in k}
        cad5y5y =  {k:v for k,v in cad5y5y.iteritems() \
                    if any(["SEK" in k,
                           "NOK" in k,
                           "USD" in k,
                           "NZD" in k,
                           "AUD" in k,
                           "GBP" in k,
                           "DKK" in k])} 
        
        def plot_model_details(ax, model, target_level, stop_level):
            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life

            ax.axis('off')
            x0_txt = r"$x_0:$ [{0:.2f}]".format(x0)
            theta_txt = r"$\hat \theta: $ [{0:.2f}]".format(theta)
            mu_txt =  r"$\hat \mu: $ [{0:.2f}]".format(mu)
            sigma_txt =  r"$\hat \sigma: $ [{0:.2f}]".format(sigma)
            half_life_txt =  r"$\hat \lambda: $ [{0:.2f}]".format(half_life)
            target_txt =  r"target level: [{0:.2f}]".format(target_level)
            stop_txt =  r"stop level: [{0:.2f}]".format(stop_level)

            font_group_title = {'color': 'black', 'weight': 'normal', 'size': 9}
            detail_txt = r"{},  {},  {},  {},  {}".format(x0_txt, theta_txt, mu_txt, sigma_txt, half_life_txt)
            ax.text(-0.01, 0.9, detail_txt, fontdict=font_group_title, verticalalignment = "top")
            detail_txt2 = r"{},  {}".format(target_txt, stop_txt)
            ax.text(-0.01, 0.8, detail_txt2, fontdict=font_group_title, verticalalignment = "top")
                


        def plot_hit_ratio_for_stop(ax, stop_keyed_hit_ratios, model, legend_txt, color ):
            
            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life

            x0_txt = r"$x_0:$ [{0:.2f}]".format(x0)
            theta_txt = r"$\hat \theta: $ [{0:.2f}]".format(theta)
            mu_txt =  r"$\hat \mu: $ [{0:.2f}]".format(mu)
            sigma_txt =  r"$\hat \sigma: $ [{0:.2f}]".format(sigma)
            half_life_txt =  r"$\hat \lambda: $ [{0:.2f}]".format(half_life)
            xlabel = r"{}, {}, {}, {}, {}".format(x0_txt, theta_txt, mu_txt, sigma_txt, half_life_txt)
            
            xs = []
            ys = []
            stop_lvls = []
            for key, value in stop_keyed_hit_ratios.iteritems():
                stop_lvls.append(key)
                xs.append(value.loc["Stop hit ratio"])
                ys.append(value.loc["Target hit ratio"])

            ax.scatter(xs, ys, c = color, label= legend_txt, alpha = 0.3, edgecolors = 'none' )

            for i, txt in enumerate(stop_lvls):
                ax.annotate(txt, ("{0:.2f}".format(xs[i]), "{0:.2f}".format(ys[i])) , fontsize=8)


            ax.xaxis.grid(True, which='both', linestyle='-', linewidth='0.2')
            ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.minorticks_on()
            ax.xaxis.set_minor_locator(ticker.MultipleLocator(base=1))
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)
            
            ax.set_ylabel('Target hit ratio (%)', fontsize=8)
            ax.set_xlabel('Stop hit ratio (%)', fontsize=8)

            leg = ax.legend(fancybox=True, prop={'size': 8}, loc='upper center', bbox_to_anchor=(0.5, 1.05))
            leg.get_frame().set_facecolor('none')
            leg.get_frame().set_linewidth(0.0)        

        for ds in cad5y5y.values()[:]:
            fig = plt.figure(figsize=(9,9))
            gs = gridspec.GridSpec(3, 2, height_ratios=[30, 30, 1], width_ratios=[1, 1])

            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])
            ax2 = plt.subplot(gs[2])
            ax3 = plt.subplot(gs[3])
            ax4 = plt.subplot(gs[4])
            ax5 = plt.subplot(gs[5])

            model = st.OrnsteinUhlenbeck(ds, dt = 1)

            target_level = model.mu_hat - 0.1 if model.data.values[-1] > model.mu_hat else model.mu_hat + 0.1
            # 10 bps from current level as the stop
            stop_level = model.data.values[-1] + 0.05 if model.data.values[-1] > target_level else model.data.values[-1] - 0.05   

            print "x0 {}".format(model.data.values[-1])
            print "mu hat {}".format(model.mu_hat)
            print "target_level {}".format(target_level)
            print "stop_level {}".format(stop_level)
            k = 10000

            ttl1 = ""
            distributions = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = 60, k = k, momentum_factor = 0)[["Target hit ratio","Stop hit ratio"]]
            
            #plot_hit_ratio(ax0, distributions[["Target hit ratio","Stop hit ratio","Sideway ratio"]], model, k, target_level, stop_level, ttl1)

            stop_level2 = model.data.values[-1] + 0.06 if model.data.values[-1] > target_level else model.data.values[-1] - 0.06
            distributions2 = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = 60, k = k, momentum_factor = 0)[["Target hit ratio","Stop hit ratio"]]
            
            stop_level3 = model.data.values[-1] + 0.07 if model.data.values[-1] > target_level else model.data.values[-1] - 0.07
            distributions3 = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = 60, k = k, momentum_factor = 0)[["Target hit ratio","Stop hit ratio"]]
            
            stop_level4 = model.data.values[-1] + 0.08 if model.data.values[-1] > target_level else model.data.values[-1] - 0.08
            distributions4 = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = 60, k = k, momentum_factor = 0)[["Target hit ratio","Stop hit ratio"]]
            
            stop_level5 = model.data.values[-1] + 0.09 if model.data.values[-1] > target_level else model.data.values[-1] - 0.09
            distributions5 = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = 60, k = k, momentum_factor = 0)[["Target hit ratio","Stop hit ratio"]]
            
            stop_level6 = model.data.values[-1] + 0.1 if model.data.values[-1] > target_level else model.data.values[-1] - 0.1
            distributions6 = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = 60, k = k, momentum_factor = 0)[["Target hit ratio","Stop hit ratio"]]
            
            stop_levels = [stop_level, stop_level2, stop_level3, stop_level4, stop_level5, stop_level6]
            h20 =  [distribution.loc[20,:] for distribution in [distributions, distributions2, distributions3, distributions4, distributions5, distributions6]] 
            h20 = dict(zip(stop_levels, h20))
            plot_hit_ratio_for_stop(ax0, h20, model, "1m horizon", "blue")

            h40 =  [distribution.loc[40,:] for distribution in [distributions, distributions2, distributions3, distributions4, distributions5, distributions6]] 
            h40 = dict(zip(stop_levels, h40))
            plot_hit_ratio_for_stop(ax1, h40, model, "2m horizon", "red")

            h60 =  [distribution.loc[60,:] for distribution in [distributions, distributions2, distributions3, distributions4, distributions5, distributions6]] 
            h60 = dict(zip(stop_levels, h60))
            plot_hit_ratio_for_stop(ax2, h60, model, "3m horizon", "green")

            ax3.axis('off')
            ax5.axis('off')
            



            plot_footer(ax4)

            name = model.data.name
            title = "{0} path simulation for {1} with different stop levels ".format(k, name)
            fig.suptitle(title, fontsize = 9)
            fig.tight_layout()
            fig.subplots_adjust(top=0.94)
            f = os.path.join(self.output_chart_path, ds.name + "_MR_Target_Hit_Ratios_with_different_stop_levels.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()


    def test_dummy(self):
        for i in np.arange(0, 100):
            print np.random.normal(loc=0.0, scale=np.sqrt(1))

#############################################################################
    def test_nonparametric_mr_model(self):
        history_length = 1000
        simulation_horizon = 60

        cad5y5y = {'EURUSDV1Y':self.fwd_rates['EURUSDV1Y'].dropna()}


        k = 500
        dt = 1

        def plot_expected_moment(ax, bin, paths, dt, horizon_length, history_length, name):
                
            x0 = bin.raw_data.values[-1]        
            dt = dt
            title = "{} {} days horizon moment estimation using {} days history (nonparametric simulation)".format(name, horizon_length, history_length)

            means = paths["mean"]
            stdevs =  paths["std"]
            history = paths["history"]
            means_up2std = means + 2 * stdevs
            means_down2std = means - 2 * stdevs
            means_up2std.name = "+2 $\sigma$"
            means_down2std.name = "-2 $\sigma$"

            result = pd.concat([history, means_up2std, means, means_down2std], axis=1)

            result["A sample simulation"] = paths["s1"].values # use values because the index doesnt match
            result.plot(ax=ax, lw=0.5)
            ax.set_title(title, y=1.02, fontsize = 9)

            #formatting
            ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            plt.setp(ax.xaxis.get_majorticklabels(), fontsize = 8)
            plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)
            ax.set_xlabel('Days', fontsize=8)
            ax.set_ylabel('Level', fontsize=8)
            leg = ax.legend(fancybox=True, prop={'size': 8})
            leg.get_frame().set_facecolor('none')
            leg.get_frame().set_linewidth(0.0)



        for ds in cad5y5y.values()[:]:
            mr_bin = bd.BinDiagnosis(ds, offset = 1, shift_forward =True)
            paths_with_history = npmr.simulate_with_daignosis_bin(mr_bin, n = simulation_horizon, k=k, dt = dt, 
                                                                  include_history=True,
                                                                  target = None, stop = None, momentum_factor = 0, momentum_lag = 5)
            paths_to_plot = paths_with_history[["s1", "mean", "std","history"]]
            fig = plt.figure(figsize=(8,6))
            gs = gridspec.GridSpec(2, 1, height_ratios=[30,1], width_ratios=[1])

            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])

            plot_expected_moment(ax0, mr_bin, paths_to_plot, dt, simulation_horizon, history_length, ds.name)
            
            plot_footer(ax1)
            plt.tight_layout()
            f = os.path.join(self.output_chart_path, ds.name + "_MR_Expected_Moments.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()


    
    def test_btp32030_bin(self):
        
        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "data/2017-08-28_BTP_historical.csv"))                         

        history_length = 780
        data = load_bbg_historical_data(file_path).tail(history_length)
        data = data[['BTP3', 'BTP20', 'BTP30']].ffill().dropna()
        btp32030 = 2 * data['BTP20'] - data['BTP3'] - data['BTP30'] 
        btp32030.name = 'btp032030'
        #plt.show(block=True)



        for ds in [btp32030]:        
            fig = plt.figure(figsize=(8,9))
            gs = gridspec.GridSpec(3, 1, height_ratios=[10, 10, 1], width_ratios=[1])

            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])
            ax2 = plt.subplot(gs[2])

            mr_obj = bd.BinDiagnosis(ds, offset = 1, shift_forward =True)
            mr_obj.plot_drift_coefficients(ax0, ignore_bin = [])
            mr_obj.plot_diffusion_coefficients(ax1, ignore_bin = [])
            plot_footer(ax2)
            plt.tight_layout()
            #plt.show(block=True)
            f = os.path.join(self.output_chart_path, ds.name + "_MR_Diagnosis.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()

        for ds in [btp32030]:
            fig = plt.figure(figsize=(8,9))
            gs = gridspec.GridSpec(3, 1, height_ratios=[50, 50, 1], width_ratios=[1])
            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])
            ax2 = plt.subplot(gs[2])

            mm_obj = bd.BinDiagnosis(ds, offset = 5, shift_forward =False)
            mm_obj.plot_drift_coefficients(ax0, ignore_bin = [])
            mm_obj.plot_diffusion_coefficients(ax1, ignore_bin = [])
            plot_footer(ax2)
            plt.tight_layout()
            #plt.show(block=True)
            f = os.path.join(self.output_chart_path, ds.name + "_MM_Diagnosis.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()

    def test_btp32030_create_expected_moment_and_1_simumlation_chart(self):
    
        def plot_expected_moment(ax, model, ax_note = None):
            

            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            name = model.data.name
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life
            he = model.hurst_exp
            adf_test_stats, adf_t_stats = model.adf

            
            title = "{} {} days horizon moment estimation using {} days history ".format(name, model_horizon, history_length)
            

            means = model.estimate_conditional_means(include_history=True)
            variances =  model.estimate_conditional_variances(include_history=True)
            stdevs = model.conditional_stdevs

            history = means["History"]
            means = means["Conditional expected value"]
            means_up2std = means + 2 * stdevs
            means_down2std = means - 2 * stdevs
            means_up2std.name = "+2 $\sigma$"
            means_down2std.name = "-2 $\sigma$"
            result = pd.concat([history, means_up2std, means, means_down2std], axis=1)

            result["A sample simulation"] = model.simulated_paths["s1"].values # use values because the index doesnt match
            result.plot(ax=ax, lw=0.5)
            ax.set_title(title, y=1.02, fontsize = 9)

            #formatting
            ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            plt.setp(ax.xaxis.get_majorticklabels(), fontsize = 8)
            plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)
            ax.set_xlabel('Days', fontsize=8)
            ax.set_ylabel('Level', fontsize=8)
            leg = ax.legend(fancybox=True, prop={'size': 8})
            leg.get_frame().set_facecolor('none')
            leg.get_frame().set_linewidth(0.0)

            if ax_note is not None:
                ax_note.axis('off')
                font = {'color': 'black', 'weight': 'light', 'size': 8}
                font_group_title = {'color': 'black', 'weight': 'semibold', 'size': 8}


                conditional_expected_val = r"Conditional expected value:" 
                conditional_expected_val_form = r"$x_0e^{ \theta t} + \mu (1 - e^{ \theta t})$"
                conditional_variance = r"Conditional variance:"
                conditional_variance_form = r"$\frac{ \sigma ^2(1 - e^{-2 \theta t} )}{2 \theta}$"
                x0_txt = r"$x_0:$ most recent history [{0:.2f}]".format(x0)
                t_txt = r"t: each day within the horizon"
                theta_txt = r"$\hat \theta: $ mean reversion speed estimation [{0:.2f}]".format(theta)
                mu_txt =  r"$\hat \mu: $ mean estimation [{0:.2f}]".format(mu)
                sigma_txt =  r"$\hat \sigma: $ vol estimation [{0:.2f}]".format(sigma)
                half_life_txt =  r"$\hat \lambda: $ half life estimation [{0:.2f}]".format(half_life)
                hurst_exp_txt = r"hurst exponent [{0:.2f}]".format(he)
                adf_txt = r"adf [{0:.4f}] with ".format(adf_test_stats)
                adf_txt = adf_txt + r"[{0}@ {3:.4F}, {1}@ {4:.4F}, {2}@{5:.4F}]".format(adf_t_stats.keys()[0], adf_t_stats.keys()[1], adf_t_stats.keys()[2],
                                                                                       adf_t_stats.values()[0], adf_t_stats.values()[1], adf_t_stats.values()[2])

                ax_note.text(-0.01, 1, "Solutions to conditional moments: ", fontdict=font_group_title, verticalalignment = "top")
                ax_note.text(-0.01, 0.8, conditional_expected_val, fontdict=font, verticalalignment = "top")
                ax_note.text(0.25, 0.8, conditional_expected_val_form, fontdict=font,fontsize =9, verticalalignment = "top")
                ax_note.text(-0.01, 0.6, conditional_variance, fontdict=font, verticalalignment = "top")
                ax_note.text(0.2, 0.64, conditional_variance_form, fontdict=font, fontsize = 11, verticalalignment = "top")
                #col 2
                ax_note.text(0.5, 0.8, t_txt, fontdict=font, verticalalignment = "top")

                ax_note.text(-0.01, 0.35, "Mean reversion model parameter estimations:", fontdict=font_group_title, verticalalignment = "top")
                ax_note.text(-0.01, 0.15, mu_txt, fontdict=font, verticalalignment = "top")
                ax_note.text(-0.01, 0.0, x0_txt, fontdict=font, verticalalignment = "top")
                ax_note.text(-0.01, -0.15, theta_txt, fontdict=font, verticalalignment = "top")
                ax_note.text(-0.01, -0.35, adf_txt, fontdict=font, verticalalignment = "top")

                #col 2
                ax_note.text(0.5, 0.15, sigma_txt, fontdict=font, verticalalignment = "top")
                ax_note.text(0.5, 0.0, half_life_txt, fontdict=font, verticalalignment = "top")
                ax_note.text(0.5, -0.20, hurst_exp_txt, fontdict=font, verticalalignment = "top")


        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "data/2017-08-28_BTP_historical.csv"))                         

        history_length = 780
        simulation_horizon = 250
        data = load_bbg_historical_data(file_path).tail(history_length)
        data = data[['BTP3', 'BTP20', 'BTP30']].ffill().dropna()
        btp32030 = 2 * data['BTP20'] - data['BTP3'] - data['BTP30'] 
        btp32030.name = 'btp032030'

        for ds in [btp32030]:
            fig = plt.figure(figsize=(8,8))
            gs = gridspec.GridSpec(3, 1, height_ratios=[30,8,1], width_ratios=[1])

            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])
            ax2 = plt.subplot(gs[2])

            model = st.OrnsteinUhlenbeck(ds, dt = 1)
            model.simulate_with_model_parameters(n=simulation_horizon, k = 1, momentum_factor = 0, include_history=True)

            plot_expected_moment(ax0, model,ax_note = ax1)
            
            plot_footer(ax2)
            plt.tight_layout()
            f = os.path.join(self.output_chart_path, ds.name + "_MR_Expected_Moments.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()

    def test_btp32030_simulation_target_stop_hit_ratio(self):
        # the simulation will record a series of terminal pnl for one comination of target and stop for 1 pair for a given n. 
        # It will be either the stoploss level or the end value. 
        # The output is a series of terminal values which represents a distribution
        # This should be a histogram with stats description. 
        
        def plot_hit_count(ax, hit_count, model, k, target_level, stop_level):
            
            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life

            x0_txt = r"$x_0:$ [{0:.2f}]".format(x0)
            theta_txt = r"$\hat \theta: $ [{0:.2f}]".format(theta)
            mu_txt =  r"$\hat \mu: $ [{0:.2f}]".format(mu)
            sigma_txt =  r"$\hat \sigma: $ [{0:.2f}]".format(sigma)
            half_life_txt =  r"$\hat \lambda: $ [{0:.2f}]".format(half_life)
            target_txt =  r"target level: [{0:.2f}]".format(target_level)
            stop_txt =  r"stop level: [{0:.2f}]".format(stop_level)

            xlabel = r"{},  {},  {},  {},  {},  {},  {}".format(x0_txt, theta_txt, mu_txt, sigma_txt, half_life_txt, target_txt, stop_txt)

            hit_count.plot(ax=ax, kind="bar")
            ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            ax.xaxis.set_ticklabels([])
            ax.xaxis.set_ticks_position('none')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)
            ax.set_ylabel('Hit counts', fontsize=8)
            ax.set_xlabel(xlabel, fontsize=8)
            leg = ax.legend(fancybox=True, prop={'size': 8})
            leg.get_frame().set_facecolor('none')
            leg.get_frame().set_linewidth(0.0)
            
            

        def plot_hit_ratio(ax, hit_ratio, model, k, target_level, stop_level):
            x0 = model.data.values[-1]
            theta = model.theta_hat
            mu = model.mu_hat
            sigma = model.sigma_hat        
            dt = model.dt
            name = model.data.name
            history_length = len(model.data.index)
            model_horizon = model.n
            half_life = model.half_life
            
            hit_ratio.plot(ax=ax)
            ax.xaxis.grid(True, which='both', linestyle='-', linewidth='0.2')
            ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
            ax.minorticks_on()
            ax.xaxis.set_minor_locator(ticker.MultipleLocator(base=1))
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)
            ax.set_ylabel('Hit ratio (%)', fontsize=8)
            leg = ax.legend(fancybox=True, prop={'size': 8})
            leg.get_frame().set_facecolor('none')
            leg.get_frame().set_linewidth(0.0)
            ax.set_xlabel('Horizon day', fontsize=8)
            ax.set_ylim(0, 100)


        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "data/2017-08-28_BTP_historical.csv"))                         

        history_length = 780
        simulation_horizon = 250
        data = load_bbg_historical_data(file_path).tail(history_length)
        data = data[['BTP3', 'BTP20', 'BTP30']].ffill().dropna()
        btp32030 = 2 * data['BTP20'] - data['BTP3'] - data['BTP30'] 
        btp32030.name = 'btp032030'

        for ds in [btp32030]:
            fig = plt.figure(figsize=(8,8))
            gs = gridspec.GridSpec(3, 1, height_ratios=[30,30,1], width_ratios=[1])

            ax0 = plt.subplot(gs[0])
            ax1 = plt.subplot(gs[1])
            ax2 = plt.subplot(gs[2])


            model = st.OrnsteinUhlenbeck(ds, dt = 1)

            target_level = model.mu_hat + 0.0  if model.data.values[-1] > model.mu_hat else model.mu_hat -0.0
 
            # 10 bps from current level as the stop
            stop_level = model.data.values[-1] + 0.20 if model.data.values[-1] > target_level else model.data.values[-1] - 0.20   


            print "x0 {}".format(model.data.values[-1])
            print "mu hat {}".format(model.mu_hat)
            print "target_level {}".format(target_level)
            print "stop_level {}".format(stop_level)
            k = 10000

            distributions = model.simulate_first_passage_time(target_level = target_level, stop_level = stop_level, n = simulation_horizon, k = k, momentum_factor = 0)

            plot_hit_count(ax0, distributions[["Target hit count","Stop hit count"]], model, k, target_level, stop_level)
            plot_hit_ratio(ax1, distributions[["Target hit ratio","Stop hit ratio","Sideway ratio"]], model, k, target_level, stop_level)

            print "target_level {}".format(target_level)
            
            plot_footer(ax2)
            name = model.data.name
            title = "{0} path simulation for {1}  ".format(k, name)
            fig.suptitle(title, fontsize = 9)
            fig.tight_layout()
            fig.subplots_adjust(top=0.94)
            f = os.path.join(self.output_chart_path, ds.name + "_MR_Target_Hit_Ratios.png")
            plt.savefig(f, dpi=100) #, bbox_inches='tight')
            plt.close()



if __name__ == '__main__':
    unittest.main()