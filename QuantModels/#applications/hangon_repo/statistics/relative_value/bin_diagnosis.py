from __future__ import division
import pandas as pd
import numpy as np
import statsmodels.tsa.stattools as ts
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm
import matplotlib.ticker as ticker

class BinDiagnosis(object):
    
    def __init__(self, data, offset = 1, shift_forward = True):
        if not isinstance(data, pd.Series):
            raise TypeError("data arg needs to be a pandas time series")
        self._data = data
        self._offset = offset
        # -1 is shifting into future 1 is shifting into history
        # -1 is needed for mean reversion while 1 is needed for momentum 
        self._shift_direction = -1 if shift_forward else 1 
        self._model_type = "Mean reversion" if shift_forward else "Momentum"
        self._shift_type = "Next" if shift_forward else "previous"
        self._bin_diagnosis = self.run_bin_diagnose(offset = self._offset)

    def run_bin_diagnose(self, bin_size = 20, offset = 1):
        self._bin_size = bin_size

        diff = None

        if self._shift_direction == -1:
            # comparing to Next day not yesterday!
            diff = (self._data.shift(self._shift_direction * offset) - self._data).reindex(self._data.index)
        elif  self._shift_direction == 1:
            diff = (- self._data.shift(self._shift_direction * offset) + self._data).reindex(self._data.index)    
        df = pd.DataFrame({'Value':self._data, 'Change':diff}).sort_values(['Value'],ascending=[True])
        df['Bin'] = self.create_bins_from_data()
        bin_grps = df.groupby('Bin')
        self._bin_data = df
        
        def funcStdev(x): return np.std(x.dropna(), ddof=1)
        def funcMean(x): return np.average(x.dropna())
        def funcCount(x): return np.count_nonzero(x.dropna())

        self._bin_diffusion_coefficients = bin_grps['Change'].agg([funcStdev, funcCount ]).rename(columns={'funcStdev':'Vol', 'funcCount':'Count'})
        
        self._bin_drift_coefficients =  bin_grps['Change'].agg([funcMean, funcCount]).rename(columns={'funcMean':'Avg', 'funcCount':'Count'})

        self._bin_groups = bin_grps

        self.polyfit_drift_coefficients(dof=1)
    
    def create_bins_from_data(self):
        max = self._data.values.max()
        min = self._data.values.min()
        chunk = (max - min)/self._bin_size
        self._chunk = chunk
        bin_left_edges = [ min + chunk * (i) for i in np.arange(self._bin_size)] + [max]
        bin_names = [ min + chunk * (i) + chunk / 2.0  for i in np.arange(self._bin_size)]    
        bins = pd.cut(self._data, bin_left_edges, right=True, labels=bin_names,include_lowest =True)
        return bins


    def plot_drift_coefficients(self, ax, xlim = None, ylim = None, dot_size = 0.1, fit_1std = True, fit_2ndd = True, ignore_bin = []):
        
        if xlim is not None: ax.set_xlim(xlim)
        if ylim is not None: ax.set_ylim(ylim)
        ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')
 
        last_tick =  [self._bin_drift_coefficients.index.values[-1] + self._chunk]
        major_ticks = list(self._bin_drift_coefficients.index.values) + last_tick
        ax.set_xticks(major_ticks) 
        ax.xaxis.set_major_locator(ticker.FixedLocator(major_ticks))
        ax.set_xlim(major_ticks[0]- self._chunk, major_ticks[-1]+self._chunk)

        
        plt.setp(ax.xaxis.get_majorticklabels(), rotation=40, fontsize = 8)
        plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8)  
        ax.set_ylabel('Average {} {} day changes per bin'.format(self._shift_type, self._offset), fontsize=8)
        #ax.set_xlabel('Time series value grouped into {} bins'.format(self.bin_size), fontsize=8)                                     
        
        drift_coefficients = self.bin_drift_coefficients

        bin_number = 0
        dummy_val = drift_coefficients ['Avg'].min()

        for (idx, row)  in drift_coefficients.iterrows():
            
            bin_number = bin_number + 1
            v = dummy_val if np.isnan(row['Avg']) else row['Avg']
            alp = 0 if np.isnan(row['Avg']) else 0.4

            if bin_number not in ignore_bin:
                ax.scatter(idx, v, c='blue', alpha=alp)
                if v != dummy_val:
                    ax.annotate("{}".format(row['Count'] if np.isnan(row['Count']) else int(row['Count'])), (idx, row['Avg']), 
                                xytext=(6, 3), xycoords='data', textcoords='offset points', fontsize=6)
            else:
                ax.scatter(idx, np.nan, c='blue', alpha=alp)
                row['Avg'] = np.nan                
        

        # higher order coefficient first in this case it is the b from b*x
        if fit_1std:
            (b, a) = self.polyfit_drift_coefficients(dof=1)
            fitted_xs = major_ticks
            fitted_ys = [a + b * x for x in fitted_xs]
            ax.plot(fitted_xs, fitted_ys, lw=0.7, c='g', ls = ':')

        # higher order coefficient first in this case it is the c from c*x*x
        if fit_2ndd:
            (c, b, a) = self.polyfit_drift_coefficients(dof=2)
            fitted_xs = major_ticks
            fitted_ys = [a + b * x + c * x * x  for x in fitted_xs]
            ax.plot(fitted_xs, fitted_ys, lw=0.5, c='g', ls = '-')

        ax.axhline(y=0, color="black", lw=0.5)

        ax.set_title("{}{} drift [{} -{}]" \
          .format(self._data.name + " " if self._data.name is not None else "",
                  self._model_type,
                  self._data.index.min().strftime("%Y-%m-%d"),
                  self._data.index.max().strftime("%Y-%m-%d")
                  ), y = 1.04, fontsize = 8 )

        current_xs = [self._data.values[-1], self._data.values[-1]]
        current_ys = [ax.get_ylim()[0], ax.get_ylim()[1]] 
        ax.plot(current_xs, current_ys, lw=1.2, c='r', ls = '-')

        he = self.hurst_exp
        adf_test_stats, adf_t_stats = self.adf
        hurst_exp_txt = r"hurst exponent [{0:.2f}]".format(he)
        adf_txt = r"adf [{0:.4f}] with ".format(adf_test_stats)
        adf_txt = adf_txt + r"[{0}@ {3:.4F}, {1}@ {4:.4F}, {2}@{5:.4F}]".format(adf_t_stats.keys()[0], adf_t_stats.keys()[1], adf_t_stats.keys()[2],
                                                                               adf_t_stats.values()[0], adf_t_stats.values()[1], adf_t_stats.values()[2])
        font = {'color': 'black', 'weight': 'light', 'size': 9}
        
        ax.annotate(adf_txt, xy=(1, 1), xycoords='axes fraction', fontsize= 8,
                    xytext=(-5, -5), textcoords='offset points', 
                    horizontalalignment='right', verticalalignment='top')

        ax.annotate(hurst_exp_txt, xy=(1, 1), xycoords='axes fraction', fontsize= 8,
                    xytext=(-5, -20), textcoords='offset points', 
                    horizontalalignment='right', verticalalignment='top')


  
    def polyfit_drift_coefficients(self, dof=1):
        output = self._bin_drift_coefficients.copy().dropna()
        p = np.polyfit(list(output.index.values), output['Avg'].values, deg = dof)
        return p

    
    def plot_diffusion_coefficients(self, ax , xlim = None, ylim = None, dot_size = 0.1, ignore_bin = []):
          
        if xlim is not None: ax.set_xlim(xlim)
        if ylim is not None: ax.set_ylim(ylim)
        ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')

        last_tick =  [self._bin_diffusion_coefficients.index.values[-1] + self._chunk]
        major_ticks = list(self._bin_diffusion_coefficients.index.values) + last_tick 
        ax.set_xticks(major_ticks) 
        ax.xaxis.set_major_locator(ticker.FixedLocator(major_ticks))
        ax.set_xlim(major_ticks[0]- self._chunk, major_ticks[-1]+self._chunk)

        plt.setp(ax.xaxis.get_majorticklabels(), rotation=40, fontsize = 8)
        plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8) 


         
        ax.set_ylabel('Volatility of {} {} day changes per bin'.format(self._shift_type, self._offset), fontsize=8)
        ax.set_xlabel('Time series value grouped into {} bins'.format(self.bin_size), fontsize=8)                                     
        
        diffusion_coefficients = self.bin_diffusion_coefficients.copy()
        
        bin_number = 0
        dummy_val = diffusion_coefficients['Vol'].min()
        
        for (idx, row)  in diffusion_coefficients.iterrows():
           
            bin_number = bin_number + 1
            v = dummy_val if np.isnan(row['Vol']) else row['Vol']
            alp = 0 if np.isnan(row['Vol']) else 0.4

            if bin_number not in ignore_bin:
                
                ax.scatter(idx, v, c='Orange', alpha=alp)
                if v != dummy_val:
                    ax.annotate("{}".format(row['Count'] if np.isnan(row['Count']) else int(row['Count'])), (idx, row['Vol']), 
                                xytext=(6, 3), xycoords='data', textcoords='offset points', fontsize=6)
            else:
                ax.scatter(idx, v, c='Orange', alpha=alp)  
                row['Vol'] = np.nan                 
        
        ax.set_title("{}{} diffusion [{} -{}]"  \
          .format(self._data.name + " " if self._data.name is not None else "",
                  self._model_type,
                  self._data.index.min().strftime("%Y-%m-%d"),
                  self._data.index.max().strftime("%Y-%m-%d")
                  ), y = 1.04, fontsize = 8 )

        current_xs = [self._data.values[-1], self._data.values[-1]]
        current_ys = [ax.get_ylim()[0], ax.get_ylim()[1]] 
        ax.plot(current_xs, current_ys, lw=1.2, c='r', ls = '-')

    @staticmethod
    def calc_hurst_exp(ts):
            # Create the range of lag values
        lags = range(2, 100)
        # Calculate the array of the variances of the lagged differences
        tau = [np.sqrt(np.std(np.subtract(ts[lag:], ts[:-lag]))) for lag in lags]
        # Use a linear fit to estimate the Hurst Exponent
        poly = np.polyfit(np.log(lags), np.log(tau), 1)
        # Return the Hurst exponent from the polyfit output
        return poly[0]*2.0

    
    @property
    def bin_groups(self):
        return self._bin_groups
    
    @property
    def bin_size(self):
        return self._bin_size

    @property
    def bin_diffusion_coefficients(self):
        return self._bin_diffusion_coefficients

    @property
    def bin_drift_coefficients(self):
        return self._bin_drift_coefficients

    @property
    def raw_data(self):
        return self._data

    @property
    def bin_data(self):   
        return self._bin_data

    
    @property
    def hurst_exp(self):
        h = BinDiagnosis.calc_hurst_exp(self._data.values)
        return h
    
    @property
    def adf(self, lag=1):
        test_stats, _, _, _, t_stats, _ = ts.adfuller(self._data.values)
        return (test_stats, t_stats)

    
