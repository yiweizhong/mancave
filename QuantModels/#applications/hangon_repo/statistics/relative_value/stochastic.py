"""A set of stochastic process models 
"""
from __future__ import division
import numpy as np
import scipy as sp
import pandas as pd
import statsmodels.tsa.stattools as ts
from bin_diagnosis import BinDiagnosis


def calc_hurst_exp(ts):
	# Create the range of lag values
	lags = range(2, 100)
	# Calculate the array of the variances of the lagged differences
	tau = [np.sqrt(np.std(np.subtract(ts[lag:], ts[:-lag]))) for lag in lags]
	# Use a linear fit to estimate the Hurst Exponent
	poly = np.polyfit(np.log(lags), np.log(tau), 1)
	# Return the Hurst exponent from the polyfit output
	return poly[0]*2.0


def simulate_GBM_SDE(x0, mu, sigma, dt, n):
    # dXt = Xt * mu * dt + xt * sigma * dWt 
    # X(t) = X0 * exp(mu - sigma * sigma/2) * t +  sigma * Wt
    
    _t_delta = dt
    _xs = [np.nan] * n
    _xs[0] = x0
    _steps = np.arange(1, n, step=1)

    for step in _steps:
        _xs[step] = _xs[step - 1] * np.exp((mu - sigma*sigma/2) * _t_delta + sigma * np.random.normal(loc=0.0, scale=np.sqrt(_t_delta)))  
    return _xs

def simulate_OU_SDE_simple(x0, theta, mu, sigma, dt, n):
    # discretised using Euler-Maruyama method into  
    # dXt = theta * (mu - Xt)dt + sigma * dWt
    # dXt = theta * (mu - Xt)dt + sigma * dWt
    # dXt here refers to Xt+1 - Xt

    _theta = float(theta)    
    _t_delta = dt
    _xs = [np.nan] * n
    _xs[0] = x0
    _steps = np.arange(1, n, step=1)
    for step in _steps:
        _xs[step] = (_xs[step-1] +  _theta * (mu - _xs[step-1]) * _t_delta + sigma * np.random.normal(loc=0.0, scale=np.sqrt(_t_delta)))
    return _xs


def simulate_OU_SDE(x0, theta, mu, sigma, dt, n, target=None, stop = None, momentum_factor = 0, momentum_history = [0], downside_momentum_only=False):
    # a better discrete approximation of the OU simulation
    # Xt+1 = exp(- theta * detlat) * Xt + (1 - exp(- theta * deltat)) * mu + sigma * sqrt((1 - exp(- 2 * theta * deltat))/(2 * lambda)) * dWt
    """ This function simulates the OU SDE
        parameters:
        ----------
        x0: float
            current value. it is the latest point on the historical data
        theta: float
            the speed of mean reversion
        sigma: float
            vol of the mean reverion
        dt: float
            time step size
        n: int
            how many time steps
        target: float
            at which level to stop the simulation as the target is hit
        stop: float 
            at which level to stop the simulation as the stoploss is hit
        momentum_factor: float
            at number between 0 to 1 indicating how much momentum influces do we want to factor into the simulation
        momentum_history: an array of float
            the initial historical data to bootstrap the momentum effect
        downside_momentum_only: bool
            when this flag is set to true, momentum is only factored if it makes the path away from the target. therefore only the downside momentum is 
            taken into consideration 
        returns:
        -------
        result:  an tuple of (float, int, int) => (xs, stop_hit_step, target_hit_step)
            xs is the path of the OU simulation. it may or may run the full size of n subject to the target and stop level
            stop_hit_step is the step at which the stop is hit. 0 by default
            target_hit_step is the step at which the stop is hit. 0 by default 
    """

    _theta = float(theta)    
    _t_delta = float(dt)
    _theta_delta_t = _theta * _t_delta 
    _xs = [np.nan] * (n + 1)  # the first item is the last history
    _steps = np.arange(1, n + 1, step=1) # 
    _momentum_history = momentum_history
    
    #init the output
    _xs[0] = x0 
    stop_hit_step = 0
    target_hit_step = 0
    
    # a is the latest simulated value
    # b is the target or stop level
    hit_target = lambda a, b: a >= b if target >= x0 else a <= b
    hit_stop =  lambda a, b: a <= b if target >= x0 else a >= b
    for step in _steps:
        # if the momentum_history is a size of 1, effectively momentum is disgarded 
        _momemtum_effect = momentum_factor * (_momentum_history[-1] - _momentum_history[0])/len(_momentum_history)
        _momemtum_effect = 0 if np.isnan(_momemtum_effect) else _momemtum_effect
        if downside_momentum_only:
            if target >= x0 and _momemtum_effect > 0:
                _momemtum_effect = 0
            elif target < x0 and _momemtum_effect < 0:
                _momemtum_effect = 0            

        _xs[step] = np.exp(-1 * _theta_delta_t) * _xs[step-1] + \
                (1 - np.exp(-1 * _theta_delta_t)) * mu + \
                sigma * np.sqrt((1 - np.exp(-2 * _theta_delta_t))/(2 * _theta)) * \
                np.random.normal(loc=0.0, scale=np.sqrt(_t_delta)) + \
                _momemtum_effect
        
        if stop is not None:
            if hit_stop(_xs[step],  stop):
                _xs[step] = stop
                stop_hit_step = step
                return (_xs, stop_hit_step, target_hit_step)
                
        if target is not None:
            if hit_target(_xs[step],  target):
                _xs[step] = target
                target_hit_step = step
                return (_xs, stop_hit_step, target_hit_step)
    

    #if hit nothing or nothing to hit
    return (_xs, stop_hit_step, target_hit_step)


def linear_autogregession(data, n=1):
    """ This function takes in an array and do an linear autoregression on lag of n
        parameters
        ----------
        data: int
            an array of numbers
        n: int
            the lag. it is default to 1
        returns
        ----------
        a: float 
            the coefficeint for the constant
        b: float
            the coefficient for the first degree independant variable
        errs: an array of float
            the error term    
        xs: an array of float
            the independant variable sequence 
        ys:  an array of float
            the dependant variable sequence 
        y_hats: an array of float
            the estimated dependant variable sequence             
    """    
    xs = data[n:]    
    ys = data[:-n]
    (b, a) =  np.polyfit(xs, ys, 1) 
    f = lambda x: a + b * x
    y_hats = [f(x) for x in xs]
    errs = [ y - y_hat for y, y_hat in zip(ys, y_hats)]
    return a, b, errs, xs, ys, y_hats





class OrnsteinUhlenbeck(object):
    #https://en.wikipedia.org/wiki/Ornstein%E2%80%93Uhlenbeck_process
    
    def __init__(self, data, dt =None):
        if not isinstance(data, pd.Series):
            raise TypeError("data arg needs to be a pandas time series")
        self._raw_data = data
        self._dt = dt if dt is not None else 1
        self._n = None
        self.estimate_with_linear_regression()
        self.simulate_with_model_parameters()
        self.estimate_conditional_means()
        self.estimate_conditional_variances()


    def estimate_with_linear_regression(self, dt = None):
        """ use the linear regression to estimate the parameters for the OU model
        """
        a, b, errs, xs, ys, y_hats = linear_autogregession(self._raw_data, n=1)
        _dt = self._dt if dt is None else dt
        self._theta_hat = np.log(b)/(-1 * _dt)
        self._mu_hat = a /(1 - b)
        self._sigma_hat = np.std(errs, ddof=1) * np.sqrt((2 * self._theta_hat)/(1 - np.exp(-2 * self._theta_hat * _dt)))         
        self._dt = _dt

    def simulate_with_model_parameters(self, n=60, k=1, include_history=False, momentum_factor = 0, momentum_lag = 5):
        """ Simulate the OU path based on the estimated model parameters
            parameters:
            -----------
            n:int
                The number of number random draws per simulation. This is the number of days for daily data 
            k: int
                The number of simulation to do done
            include_history: bool
                Whether or not including the historical data used to estimate the model in the output
            momentum_factor: float
                a number between 0 to 1 indicates how much momentum is factored in
            momentum_lag: int
                the number of lag to use for the momentum calc
            returns:
            -----------
            paths: dataframe
                A dataframe containing the simulted result
        """

        if momentum_factor < 0 or momentum_factor > 1:
             raise  RuntimeError("The momentum factor must be between 0 and 1")

        history = self.data.values if include_history else []
        paths = {}
        x0 = self.data.values[-1]
        momentum_history =  self.data.values[-1  * momentum_lag:]
        for i in np.arange(k):
            paths["s{}".format(i+1)] = \
                np.append([np.nan] * len(history[:-1]), 
                          simulate_OU_SDE(x0, self.theta_hat, self.mu_hat, self.sigma_hat, self.dt, n, \
                                          target = None, stop = None, momentum_factor = momentum_factor, \
                                          momentum_history = momentum_history)[0]) # the first item of the triplets
        if include_history:
            paths["history"] = np.append(history, [np.nan] * (n))
        self._n = n
        self._simulated_paths = pd.DataFrame(paths)
        return self._simulated_paths


    def simulate_first_passage_time(self, target_level = None, stop_level = None, n = 60, k = 10000, momentum_factor = 0, momentum_lag = 5, downside_momentum_only = False):
        """ Simulate the first passage time based on the estimated model parameters
            parameters:
            -----------
            target_level: float or None
                the level at which the target is hit
            stop_level: float or None
                the level at which the stop is hit
            n:int
                The number of number random draws per simulation. This is the number of days for daily data 
            k: int
                The number of simulation to do done
            momentum_factor: float
                a number between 0 to 1 indicates how much momentum is factored in
            momentum_lag: int
                the number of lag to use for the momentum calc
            returns:
            -----------
            paths: Series
                A series containing the number of target hits per step/day, like a histogram
        """
        
        target_hits = { step: 0 for step in np.arange(1, n + 1, 1 )}
        stop_hits = { step: 0 for step in np.arange(1, n + 1, 1 )}
        beyond = 0
        x0 = self.data.values[-1]
        momentum_history =  self.data.values[-1  * momentum_lag:] if momentum_factor > 0 else [0]

        for i in np.arange(0, k, 1):
            result = simulate_OU_SDE(x0, self.theta_hat, self.mu_hat, self.sigma_hat, self.dt, n, \
                                     target = target_level, stop = stop_level, momentum_factor = momentum_factor, \
                                     momentum_history = momentum_history, downside_momentum_only = downside_momentum_only)

            stop_hit_step = result[1]
            target_hit_step = result[2]

            if(stop_hit_step > 0):
                stop_hits[stop_hit_step] = stop_hits[stop_hit_step] + 1
            elif (target_hit_step > 0):
                target_hits[target_hit_step] = target_hits[target_hit_step] + 1
            else:
                beyond = beyond + 1
               
        stop_hit_distribution = pd.Series(data = stop_hits.values(), index=stop_hits.keys(), name="Stop hit count")
        target_hit_distribution = pd.Series(data = target_hits.values(), index=target_hits.keys(), name="Target hit count")
        hit_distribution = pd.concat([stop_hit_distribution, target_hit_distribution], axis=1)

        count = 0
        stop_hit_ratio_distribution = {}
        target_hit_ratio_distribution = {}
        missed_hit_ratio_distribution = {}
        
        for (idx, row) in hit_distribution.iterrows():
            count = count + 1
            partial = hit_distribution.head(count)
            stop_hit_ratio_distribution[idx] = partial["Stop hit count"].sum()/k * 100
            target_hit_ratio_distribution[idx] = partial["Target hit count"].sum()/k * 100
            missed_hit_ratio_distribution[idx] = 100 - stop_hit_ratio_distribution[idx] - target_hit_ratio_distribution[idx]
        
        stop_hit_ratio_distribution = pd.Series(data = stop_hit_ratio_distribution.values(), index = stop_hit_ratio_distribution.keys(), name = "Stop hit ratio")
        target_hit_ratio_distribution = pd.Series(data = target_hit_ratio_distribution.values(), index = target_hit_ratio_distribution.keys(), name = "Target hit ratio")
        missed_hit_ratio_distribution = pd.Series(data = missed_hit_ratio_distribution.values(), index = missed_hit_ratio_distribution.keys(), name = "Sideway ratio")
        
        hit_distribution["Stop hit ratio"] = stop_hit_ratio_distribution
        hit_distribution["Target hit ratio"] = target_hit_ratio_distribution
        hit_distribution["Sideway ratio"] = missed_hit_ratio_distribution
        

        if ((k -beyond) != (stop_hit_distribution.sum() + target_hit_distribution.sum())):
            print ""
            print "==========warning============="
            print "missed hit: {}".format(beyond)
            print "target hit: {}".format(target_hit_distribution.sum())
            print "stop hit: {}".format(stop_hit_distribution.sum())
        
        return hit_distribution
    

    def eistimate_fractional_life(self, fraction):
        """ calcualte the time in days for the process's conditional expected value to revert to the long
            run mean from its current value based on the estimated parameters.                        
            returns:
            --------
            life in days: double
        """
        if fraction > 1 or fraction <= 0:
             raise TypeError("The fraction is a positive number that is > 0 and <= 1")         

        return np.log(1/fraction)/self.theta_hat        
    
    def estimate_conditional_decay_rate(self):
        """ calcualte the conditional and unconditional decay rate based on the estimated parameters.                        
            returns:
            --------
            decay_rates: an dictionary of doubles
                decay rates 
        """
        steps = self.n + 1
        f = lambda t: np.exp(-1 * self.theta_hat * t)
        decay_rates = {step:f(step) for step in np.arange(1, steps + 1, 1)}
        decay_rates['unconditional'] = 1
        return decay_rates        


    def estimate_conditional_means(self, n =None, include_history = False): 
        """ calcualte the conditional and unconditional expected value based on the estimated parameters            
            returns:
            --------
            means: an series of floats
        """
        steps = self.n if n is None else n
        x0 = self.data.values[-1]
        mu = self.mu_hat
        theta = self.theta_hat

        history = self.data.values if include_history else []

        f = lambda t: mu + (x0 - mu) * np.exp(-1*theta*t)

        #means = {step :f(step) for step in np.arange(1, steps + 1, 1)}
        means = np.append([np.nan] * len(history),  [f(step) for step in np.arange(1, steps + 1, 1)])
        unconditional_mean = [mu] * len(means)

        result = pd.concat( \
            [pd.Series(data = means, index = np.arange(1, len(means) + 1, step = 1), name = "Conditional expected value"),
             pd.Series(data = unconditional_mean, index = np.arange(1, len(means) + 1, step = 1), name = "Unconditional expected value")], 
             axis = 1)

        if include_history:
            result["History"] = np.append(history, [np.nan] * steps) 

        #means['unconditional'] = mu
        #self._conditional_means = pd.Series(data = means.values(), index=means.keys(),name="mean")
        self._conditional_means = result
        return result
    
    def estimate_conditional_variances(self, n = None, include_history = False):
        """ calcualte the conditional and unconditional variance based on the estimated parameters            
            returns:
            --------
            variances: an dictionary of doubles
        """
        steps = self.n if n is None else n
        x0 = self.data.values[-1]
        mu = self.mu_hat
        theta = self.theta_hat
        sigma = self.sigma_hat

        history = self.data.values if include_history else []

        f = lambda t: sigma * sigma * (1 - np.exp(-2 * theta * t)) / (2 * theta) 
        #variances = {step:f(step) for step in np.arange(1, steps + 1, 1)}

        variances = np.append([np.nan] * len(history), [f(step) for step in np.arange(1, steps + 1, 1)])
        unconditional_variances = [sigma * sigma / (2 * theta)]  * len(variances)


        result = pd.concat( \
            [pd.Series(data = variances, index = np.arange(1, len(variances) + 1, step = 1), name = "Conditional variance"),
             pd.Series(data = unconditional_variances, index = np.arange(1, len(variances) + 1, step = 1), name = "Unconditional variance")], 
            axis = 1)
        
        if include_history:
            result["History"] = np.append(history, [np.nan] * steps) 
        
        self._conditional_variances = result

        return result



    
    @property
    def n(self):
        if self._n is None:
            raise  RuntimeError("The model currently has no simulation horizon n set. \
                                Run at least 1 simulate_with_model_parameters with desired n first")
        return self._n

    @property
    def dt(self):
        return self._dt

    @property
    def data(self):
        return self._raw_data 

    @property
    def theta_hat(self):
        '''the reversion speed'''
        return self._theta_hat
    
    @property
    def mu_hat(self):
        ''' the mean'''
        return self._mu_hat

    @property
    def sigma_hat(self):
        '''the vol'''
        return self._sigma_hat

    @property
    def simulated_paths(self):
        return self._simulated_paths
        
    @property
    def conditional_means(self):
        return self._conditional_means
    
    @property
    def conditional_variances(self):
        return self._conditional_variances
    
    @property
    def conditional_stdevs(self):
        f = lambda x : np.sqrt(x)
        return self._conditional_variances["Conditional variance"].apply(f)
    
    @property
    def half_life(self):     
        return self.eistimate_fractional_life(1/2)

    @property
    def fractional_life(self):        
        lifes = { f : self.eistimate_fractional_life(1/f) for f in np.arange(1, 11, 1)}
        return pd.Series(data=lifes.values(),index=lifes.keys(),name ="fractional life")

    @property
    def hurst_exp(self):
        h = calc_hurst_exp(self._raw_data.values)
        return h
    
    @property
    def adf(self, lag=1):
        test_stats, _, _, _, t_stats, _ = ts.adfuller(self._raw_data.values)
        return (test_stats, t_stats)
