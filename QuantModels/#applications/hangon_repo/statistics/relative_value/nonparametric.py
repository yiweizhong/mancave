"""A set of stochastic process models 
"""
from __future__ import division
import math
import numpy as np
import scipy as sp
import pandas as pd
from bin_diagnosis import BinDiagnosis
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm
import matplotlib.ticker as ticker


def find_neighbours_in_series(ds, x, number_of_neighbour):
    xs = ds.dropna().index.values # always in assending order
    ys = ds.dropna().values

    l = len(xs)
    n = math.ceil(number_of_neighbour/2)
    if n * 2 > l:
        raise RuntimeError("not enough data to locate {} neighbours".format(number_of_neighbour))

    left = []
    right = []

    if x <= xs[0]:
        return ds.loc[xs[0:int(n*2)]]
        
    if x >= xs[-1]:
        return ds.loc[xs[int(-1*n*2):]]

    for i in np.arange(0, l, step=1):
        if xs[i-1] <= x and x <= xs[i]:
            left = xs[:i][int(-1 * n):]
            right = xs[i:][:int(n)]
            if len(left) < n:
                right =  xs[i:][:int(n + n - len(left))]
            elif len(right) < n:
                left = xs[:i][int(-1 * (n + n - len(right))):]
            break

    return ds.loc[np.append(left, right)]

def polyfit_drift_coefficients(ds, dof=1):
    output = ds.dropna()
    p = np.polyfit(list(ds.index.values), ds.values, deg = dof)
    return p


def estimate_next_mean_variance(bin, x, bins_for_mean_fitting = 6, bins_for_stdev_fitting = 12):
    means =  bin.bin_drift_coefficients['Avg'].dropna()
    stdevs = bin.bin_diffusion_coefficients['Vol'].dropna()

    mean_dx_for_fitting = find_neighbours_in_series(means, x, bins_for_mean_fitting)
    stdev_dx_for_fitting = find_neighbours_in_series(stdevs, x, bins_for_stdev_fitting)
    
    #print mean_dx_for_fitting
    #print stdev_dx_for_fitting
    
  
    (c1, b1, a1) = polyfit_drift_coefficients(mean_dx_for_fitting, dof=2)
    fitted_mean = round(a1 + b1 * x + c1 * x * x, 4) 

    (c2, b2, a2) = polyfit_drift_coefficients(stdev_dx_for_fitting, dof=2)
    fitted_stdev = round(a2 + b2 * x + c2 * x * x, 4)

    #print "fitted mean {}".format(fitted_mean)
    #print "fitted stdev {}".format(fitted_stdev)

    return fitted_mean, fitted_stdev, (c1, b1, a1) , (c2, b2, a2), mean_dx_for_fitting, stdev_dx_for_fitting

        

def simulate_nonparametric_SDE(bin, x0, dt, n, target = None, stop = None , momentum_factor = 0, momentum_history = [0], downside_momentum_only=False, number_dp_for_fitting = 4):

    """ draw mean and variance from a diagnosis bin oject
        parameters:
        ------------
        bin: BinDiagnosis
            bin based mean and stdev
        x0: float
            current value. it is the latest point on the historical data
        dt: float
            time step size
        n: int
            number of the evolution per simulation
        target: float
            at which level to stop the simulation as the target is hit
        stop: float 
            at which level to stop the simulation as the stoploss is hit
        momentum_factor: float
            at number between 0 to 1 indicating how much momentum influces do we want to factor into the simulation
        momentum_history: an array of float
            the initial historical data to bootstrap the momentum effect
        downside_momentum_only: bool
            when this flag is set to true, momentum is only factored if it makes the path away from the target. therefore only the downside momentum is 
            taken into consideration 
        returns:
        -------
        result:  an tuple of (float, int, int) => (xs, stop_hit_step, target_hit_step)
            xs is the path of the OU simulation. it may or may run the full size of n subject to the target and stop level
            stop_hit_step is the step at which the stop is hit. 0 by default
            target_hit_step is the step at which the stop is hit. 0 by default 
    """
    _xs = [np.nan] * (n + 1)
    _steps = np.arange(1, n + 1, step = 1)
    _momentum_history = momentum_history
    _t_delta = float(dt)

    #init the output
    _xs[0] = x0 
    stop_hit_step = 0
    target_hit_step = 0
    hit_target = lambda a, b: a >= b if target >= x0 else a <= b
    hit_stop =  lambda a, b: a <= b if target >= x0 else a >= b
    for step in _steps:
        # if the momentum_history is a size of 1, effectively momentum is disgarded 
        _momemtum_effect = momentum_factor * (_momentum_history[-1] - _momentum_history[0])/len(_momentum_history)
        _momemtum_effect = 0 if np.isnan(_momemtum_effect) else _momemtum_effect
        if downside_momentum_only:
            if target >= x0 and _momemtum_effect > 0:
                _momemtum_effect = 0
            elif target < x0 and _momemtum_effect < 0:
                _momemtum_effect = 0            

        #print "x{}:{} ".format(step-1, _xs[step-1])
        fitted_mean_dx, fitted_stdev_dx, _ , _, _, _ = estimate_next_mean_variance(bin, _xs[step-1], bins_for_mean_fitting = 6, bins_for_stdev_fitting = 12) 
        _xs[step] = _xs[step-1] + _t_delta * fitted_mean_dx + _t_delta * fitted_stdev_dx * np.random.normal(loc=0.0, scale=np.sqrt(_t_delta)) + _momemtum_effect
        #print "x{}:{} ".format(step, _xs[step])
        #print "====================="
        if stop is not None:
            if hit_stop(_xs[step],  stop):
                _xs[step] = stop
                stop_hit_step = step
                return (_xs, stop_hit_step, target_hit_step)
                
        if target is not None:
            if hit_target(_xs[step],  target):
                _xs[step] = target
                target_hit_step = step
                return (_xs, stop_hit_step, target_hit_step)
    

    #if hit nothing or nothing to hit
    return (_xs, stop_hit_step, target_hit_step)


def simulate_with_daignosis_bin(bin, n=60, k=1, dt = None, include_history=False, 
                                target= None, stop = None, momentum_factor = 0, momentum_lag = 5):
    """ Simulate the path based on bin
        parameters:
        -----------
        bin: DiagnosisBin
            The diagnosis bin object
        n:int
            The number of number random draws per simulation. This is the number of days for daily data 
        k: int
            The number of simulation to do done
        include_history: bool
            Whether or not including the historical data used to estimate the model in the output
        dt: float
            the time step
        target: float or None
                the level at which the target is hit
        stop: float or None
            the level at which the stop is hit
        
        momentum_factor: float 
            a number between 0 to 1 indicates how much momentum is factored in
        momentum_lag: int
            the number of lag to use for the momentum calc
        returns:
        -----------
        paths: dataframe
            A dataframe containing the simulted result
    """

    if momentum_factor < 0 or momentum_factor > 1:
            raise  RuntimeError("The momentum factor must be between 0 and 1")

    history = bin.raw_data.values if include_history else []
    paths = {}
    x0 = bin.raw_data.values[-1]
    momentum_history =  bin.raw_data.values[-1  * momentum_lag:]
    _dt = dt if dt is not None else 1
    for i in np.arange(k):
        paths["s{}".format(i+1)] = \
            np.append([np.nan] * len(history[:-1]), 
                        simulate_nonparametric_SDE(bin, x0, _dt, n, 
                                                    target = target, stop = stop, 
                                                    momentum_factor = momentum_factor, 
                                                    momentum_history = momentum_history)[0]) # the first item of the triplets
    
    paths = pd.DataFrame(paths)
    paths["mean"] = paths.mean(axis=1)
    paths["std"] = paths.std(axis=1)

    if include_history:
        paths["history"] = np.append(history, [np.nan] * (n))

    return paths
      
