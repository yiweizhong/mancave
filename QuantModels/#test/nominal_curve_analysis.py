import pandas as pd
import numpy as np
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations

from pandas.tseries.offsets import DateOffset


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


def bootstrap_ifs_fwd(data):
    nc =  { c: int( c.replace('USD_','').replace('Y_SWIT','') ) for c in data.columns}
    data = data.rename(columns=nc)
    a = data.columns.tolist()[1:]
    b = data.columns.tolist()[:-1]
    fwd = {}
    for (ca,cb) in zip(a, b):
        term = ca - cb
        forward = cb
        cca = (1 + data[ca]/100).pow(ca)
        ccb = (1 + data[cb]/100).pow(cb)
        fwd['{0}Y{1}Y'.format(forward, term)] = np.log(cca/ccb)/term * 100
    fwd = pd.DataFrame(fwd)       
    return fwd


def plot_main(ax, xs, ys, xlegend, ylegend, colors, cmaps, ticklabelsize=8):
    
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 6)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    #for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
    #    label.set_fontsize(ticklabelsize)
    #    if tick < 0:
    #        label.set_color("red")
    #    else:
    #        label.set_color("black")
    
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()    
    
    ymax_abs = max([abs(ymin), abs(ymax)])
    xmax_abs = max([abs(xmin), abs(xmax)])
    
    xymax = max([abs(ymin), abs(ymax), abs(xmin), abs(xmax)])
    
    ax.set_xlim(-1 * xmax_abs, xmax_abs)
    #ax.set_ylim(-1 * ymax_abs, ymax_abs)
    
    # Move left y-axis and bottim x-axis to centre, passing through (0,0)
    ax.spines['left'].set_position('center')
    #ax.spines['bottom'].set_position('center')
    # Eliminate upper and right axes
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    # Show ticks in the left and lower axes only
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    
    ax.xaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_major_locator(MultipleLocator(1))
    ax.xaxis.grid(True, which='major')
    #ax.xaxis.grid(True, which='minor')
    ax.yaxis.grid(True, which='major')
    
    for label in ax.get_xticklabels(minor =False):
        label.set_fontsize(7)

    for label in ax.get_yticklabels(minor =False):
        label.set_fontsize(7)
        
    ax.set_ylabel(ylegend, fontsize=9)
    ax.yaxis.set_label_coords(0, 0.12)

    ax.set_xlabel(xlegend, fontsize=9)
    ax.xaxis.set_label_coords(0.85, 0.045)
    
    # end points
    start_x = xs.loc[xs.first_valid_index()]
    start_y = ys.loc[ys.first_valid_index()]
    start_date = xs.first_valid_index().strftime(r'%Y-%m-%d')
    
    ds1 = ax.scatter(start_x, start_y, c='#f5d9c8', s=90, alpha=0.5, label=start_date, edgecolors='b')
    
    last_x = xs.loc[xs.last_valid_index()]
    last_y = ys.loc[ys.last_valid_index()]
    last_date = xs.last_valid_index().strftime(r'%Y-%m-%d')

    ds2 = ax.scatter(last_x, last_y, c='darkred', s=90, alpha=1, label=last_date)
    
    
    ax.legend([ds1, ds2], [start_date, last_date], scatterpoints=1, fancybox=True, framealpha=0, loc=1, ncol=1, fontsize=9)
    
    return mapable


def rolling_apply(df, window_size, func, min_window_size=None):
    if min_window_size is None:
        min_window_size = window_size
    result = {}
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here

        if len(sub_df) >= min_window_size:            
            idx = sub_df.index[-1]
            result[idx] = func(sub_df)
    return result


def get_flattened_cov(df):
    covmatx = df.cov()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def get_flattened_corr(df):
    covmatx = df.corr()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result




####################################
rundate = '2020-08-10'
####################################


data_gov = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_GOV_historical.csv')  
data_gov['date'] = pd.to_datetime(data_gov['date'])
data_gov = data_gov.set_index('date').ffill()


data = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_BEIRR_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


data_swap = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_SWAP_ALL_historical.csv')  
data_swap['date'] = pd.to_datetime(data_swap['date'])
data_swap = data_swap.set_index('date').ffill()

# data_bcs = pd.read_csv(r'C:/Dev/Models/QuantModels/#test/BCS_2020-07-29.csv')  
# data_bcs['date'] = pd.to_datetime(data_bcs['date'])
# data_bcs = data_bcs.set_index('date').ffill()
# data_bcs_rtn = (data_bcs - data_bcs.shift(1))/100

data_ifs = pd.read_csv(r'C:/Temp/Test/market_watch/staging/2020-08-05_SWIT_historical.csv')  
data_ifs['date'] = pd.to_datetime(data_ifs['date'])
data_ifs = data_ifs.set_index('date').ffill()


data_fwd_ifs = bootstrap_ifs_fwd(data_ifs)


US_cols =  [c for c in data.columns if 'US' in c]
US_cols.sort()
data_usd = data[US_cols].dropna()

data_usd['USD_NM_02Y'] = data_usd['USD_BE_02Y'] + data_usd['USD_RR_02Y'] 
data_usd['USD_NM_05Y'] = data_usd['USD_BE_05Y'] + data_usd['USD_RR_05Y']
data_usd['USD_NM_10Y'] = data_usd['USD_BE_10Y'] + data_usd['USD_RR_10Y']
data_usd['USD_NM_30Y'] = data_usd['USD_BE_30Y'] + data_usd['USD_RR_30Y']



data_usd['USD_NM_210'] = data_usd['USD_NM_10Y'] - data_usd['USD_NM_02Y']
data_usd['USD_NM_510'] = data_usd['USD_NM_10Y'] - data_usd['USD_NM_05Y']
data_usd['USD_NM_530'] = data_usd['USD_NM_30Y'] - data_usd['USD_NM_05Y']
data_usd['USD_NM_1030'] = data_usd['USD_NM_30Y'] - data_usd['USD_NM_10Y']
data_usd['USD_NM_020510'] = data_usd['USD_NM_10Y'] - 2 * data_usd['USD_NM_05Y'] + data_usd['USD_NM_02Y']
data_usd['USD_NM_051030'] = data_usd['USD_NM_30Y'] - 2 * data_usd['USD_NM_10Y'] + data_usd['USD_NM_05Y']


data_usd['USD_RR_210'] = data_usd['USD_RR_10Y'] - data_usd['USD_RR_02Y']
data_usd['USD_RR_510'] = data_usd['USD_RR_10Y'] - data_usd['USD_RR_05Y']
data_usd['USD_RR_530'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_05Y']
data_usd['USD_RR_230'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_02Y']
data_usd['USD_RR_1030'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_10Y']
data_usd['USD_RR_020510'] = data_usd['USD_RR_10Y'] - 2 * data_usd['USD_RR_05Y'] + data_usd['USD_RR_02Y']
data_usd['USD_RR_051030'] = data_usd['USD_RR_30Y'] - 2 * data_usd['USD_RR_10Y'] + data_usd['USD_RR_05Y']

data_usd['USD_BE_210'] = data_usd['USD_BE_10Y'] - data_usd['USD_BE_02Y']
data_usd['USD_BE_510'] = data_usd['USD_BE_10Y'] - data_usd['USD_BE_05Y']
data_usd['USD_BE_530'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_05Y']
data_usd['USD_BE_230'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_02Y']
data_usd['USD_BE_1030'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_10Y']
data_usd['USD_BE_020510'] = data_usd['USD_BE_10Y'] - 2 * data_usd['USD_BE_05Y'] + data_usd['USD_BE_02Y']
data_usd['USD_BE_051030'] = data_usd['USD_BE_30Y'] - 2 * data_usd['USD_BE_10Y'] + data_usd['USD_BE_05Y']

data_usd['USD_BE2NM30'] = data_usd['USD_NM_30Y'] - data_usd['USD_BE_02Y']

data_usd_rtn = data_usd - data_usd.shift(1)



#test 1


data_usd_rtn = data_usd - data_usd.shift(20)

nm_rates_cols = [
 'USD_NM_02Y',
 'USD_NM_05Y',
 'USD_NM_10Y',
 'USD_NM_30Y']


nm_curve_cols = [
 'USD_NM_210',
 'USD_NM_510',
 'USD_NM_530',
 'USD_NM_1030']

data_usd_rate_nm = data_usd[nm_rates_cols].dropna()
data_usd_curve_nm = data_usd[nm_curve_cols].dropna()

data_usd_rtn_rate_nm = data_usd_rtn[nm_rates_cols].dropna()
data_usd_rtn_curve_nm = data_usd_rtn[nm_curve_cols].dropna()

data_usd_rate_nm = data_usd_rate_nm[(data_usd_rate_nm.index >= '2008-01-01') & (data_usd_rate_nm.index < '2021-01-01')]
data_usd_rtn_rate_nm = data_usd_rtn_rate_nm[(data_usd_rtn_rate_nm.index >= '2008-01-01') & (data_usd_rtn_rate_nm.index < '2021-01-01')]


data_usd_rate_nm_test1_pca = pca.PCA(data_usd_rate_nm, rates_pca = True, momentum_size = None)
data_usd_rate_nm_test1_pca.plot_loadings(n=4, m=5)

data_usd_rtn_rate_nm_test1_pca = pca.PCA(data_usd_rtn_rate_nm, rates_pca = True, momentum_size = None)
data_usd_rtn_rate_nm_test1_pca.plot_loadings(n=4, m=5)








# test1 general relationship
data_usd_test1 = data_usd[['USD_NM_10Y', 'USD_RR_10Y', 'USD_BE_10Y']]
data_usd_test1_pca = pca.PCA(data_usd_test1, rates_pca = True, momentum_size = None)
data_usd_test1_pca.plot_loadings(n=3, m=5)


#test2 during sell off
data_usd_test2 = data_usd[['USD_NM_10Y', 'USD_RR_10Y', 'USD_BE_10Y']]
data_usd_test2_rtn = data_usd_test2 - data_usd_test2.shift(5)
data_usd_test2 = data_usd_test2[data_usd_test2_rtn['USD_NM_10Y'] <0 ]

data_usd_test2['CAD_NM_05Y'] = data_swap['CAD_0Y_5Y'].reindex(data_usd_test1.index)

data_usd_test2_pca = pca.PCA(data_usd_test2, rates_pca = True, momentum_size = None)
data_usd_test2_pca.plot_loadings(n=3, m=5)



#test3 during rally
data_usd_test3 = data_usd[['USD_NM_10Y', 'USD_RR_10Y', 'USD_BE_10Y']]
data_usd_test3_rtn = data_usd_test3 - data_usd_test3.shift(5)
data_usd_test3 = data_usd_test3[data_usd_test3_rtn['USD_NM_10Y'] > 0]


data_usd_test3['CAD_NM_05Y'] = data_swap['CAD_0Y_5Y'].reindex(data_usd_test3.index)


data_usd_test3_pca = pca.PCA(data_usd_test3, rates_pca = True, momentum_size = None)
data_usd_test3_pca.plot_loadings(n=3, m=5)


#test4

dt_ago = data_usd.index[-1] - DateOffset(years = 11, months=0, days=0)
data_usd_test4 = data_usd[data_usd.index >= dt_ago]
data_usd_test4 = data_usd_test4.drop(['USD_NM_02Y','USD_NM_05Y','USD_NM_10Y','USD_NM_30Y'],axis=1)

 

data_usd_test4_pca = pca.PCA(data_usd_test4, rates_pca = True, momentum_size = None)

data_usd_test4_pca.plot_loadings(n=3, m=5)


#test 5

data_usd_test5 = data_usd[data_usd.index >= '2010-01-01']
data_usd_test5 = data_usd_test5[['USD_BE_02Y', 'USD_BE_05Y','USD_BE_10Y', 'USD_BE_30Y',
                                 'USD_RR_02Y', 'USD_RR_05Y','USD_RR_10Y', 'USD_RR_30Y']]

 
data_usd_test5_pca = pca.PCA(data_usd_test5, rates_pca = True, momentum_size = None)

data_usd_test5_pca.plot_loadings(n=3, m=30)


#test 6

data_usd_test6 = data_usd[data_usd.index >= '2008-01-01']
data_usd_test6 = data_usd_test6[['USD_BE_230', 'USD_RR_30Y']]

 
data_usd_test6_pca = pca.PCA(data_usd_test6, rates_pca = True, momentum_size = None)

data_usd_test6_pca.plot_loadings(n=2, m=30)

hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_usd_test6_pca.pc_hedge
mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = 'W')
mr_obj.create_mr_info_fig((15, 6), pca_obj = data_usd_test6_pca)




#test 7

assets = ['USD_BE_230', 'USD_RR_30Y', 'USD_NM_10Y','BCS', 'USD_BE2NM30']
data_usd_test7 = data_usd_rtn[data_usd.index >= '2007-01-01']
data_usd_test7['BCS'] = data_bcs_rtn.BlendedCreditSpread
data_usd_test7 = data_usd_test7.ffill().dropna()
data_usd_test7 = data_usd_test7[assets]


#rolling vol
data_rtns_d_1y_rolling_covs = pd.DataFrame(rolling_apply(data_usd_test7, 125, get_flattened_cov)).transpose()
data_rtns_d_1y_rolling_vol = data_rtns_d_1y_rolling_covs[[ a + '_vs_' + b for a,b in set([comb for comb in combinations(assets + assets, 2)]) if a == b]]
data_rtns_d_1y_rolling_vol = data_rtns_d_1y_rolling_vol.applymap(np.sqrt)

#rolling corr
data_rtns_d_1y_rolling_corr = pd.DataFrame(rolling_apply(data_usd_test7, 125, get_flattened_corr)).transpose()
data_rtns_d_1y_rolling_corr = data_rtns_d_1y_rolling_corr[[ a + '_vs_' + b for a,b in set([comb for comb in combinations(assets, 2)]) if a != b]]


data_rtns_d_1y_rolling_corr[['USD_BE_230_vs_USD_NM_10Y',
                            'USD_NM_10Y_vs_USD_BE2NM30',
                            'USD_BE_230_vs_USD_RR_30Y',
 'USD_BE_230_vs_BCS',
 'USD_RR_30Y_vs_BCS',
 'USD_NM_10Y_vs_BCS',
 'BCS_vs_USD_BE2NM30',
 'USD_RR_30Y_vs_USD_NM_10Y']].plot()

data_rtns_d_1y_rolling_vol.plot()


#test 8

assets = ['USD_BE_230', 'USD_RR_30Y', 'USD_NM_10Y','BCS', 'USD_BE2NM30']
data_usd_test7 = data_usd_rtn[data_usd.index >= '2007-01-01']
data_usd_test7['BCS'] = data_bcs_rtn.BlendedCreditSpread
data_usd_test7 = data_usd_test7.ffill().dropna()
data_usd_test7 = data_usd_test7[assets]



# test9 fwd curve


data_fwd_ifs.tail(1).transpose()

#.plot(kind='bar')


data_fwd_ifs.loc['2020-06-01']


data_ifs.tail(1).transpose().plot()


data_usd_test9 = data_fwd_ifs[data_fwd_ifs.index >= '2008-01-01']

data_usd_test9_pca = pca.PCA(data_usd_test9, rates_pca = True, momentum_size = None)

data_usd_test9_pca.plot_loadings(n=5, m=30)







# test10

assets = ['USD_RR_10Y', 'USD_BE_10Y']
data_usd_test10 = data_usd[(data_usd.index >= '2007-01-01') & (data_usd.index < '2021-01-01')][assets]
data_d_1y_rolling_corr = pd.DataFrame(rolling_apply(data_usd_test10, 125, get_flattened_corr)).transpose()
data_d_1y_rolling_corr = data_d_1y_rolling_corr[[ a + '_vs_' + b for a,b in set([comb for comb in combinations(assets, 2)]) if a != b]]

data_usd_rtn_test10 = data_usd_rtn[(data_usd.index >= '2007-01-01') & (data_usd.index < '2021-01-01')][assets]
data_usd_rtn_test10_1y_rolling_corr = pd.DataFrame(rolling_apply(data_usd_rtn_test10, 125, get_flattened_corr)).transpose()
data_usd_rtn_test10_1y_rolling_corr = data_usd_rtn_test10_1y_rolling_corr[[ a + '_vs_' + b for a,b in set([comb for comb in combinations(assets, 2)]) if a != b]]


assets2 = ['USD_BE_210', 'USD_BE_230','USD_BE_530','USD_BE_10Y']
data_usd_test10_be_curves = data_usd[(data_usd.index >= '2007-01-01') & (data_usd.index < '2021-01-01')][assets2]
data_usd_test10_be_curves['RRINF_CORR'] = data_usd_rtn_test10_1y_rolling_corr
data_usd_test10_be_curves = data_usd_test10_be_curves.dropna()





###########################

f1 = plt.figure(figsize = (20, 20))
gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
ax0 = plt.subplot(gs[0])
#ax1 = plt.subplot(gs[1])
#ax2 = plt.subplot(gs[2])

c1 = mpl.dates.date2num(data_usd_test10_be_curves.index.to_pydatetime())
c2 = data_usd_test10_be_curves['RRINF_CORR']
cmap1 = plt.cm.get_cmap('Reds')
cmap2 = plt.cm.get_cmap('RdYlBu') 

mapable1 = plot_main(ax0, data_usd_test10_be_curves['USD_BE_10Y'], data_usd_test10_be_curves['USD_BE_210'], 'USD_BE_10Y', 'USD_BE_210', c1, cmap2)
plt.colorbar(mapable1)


mapable1 = plot_main(ax0, data_usd_test10_be_curves['RRINF_CORR'], data_usd_test10_be_curves['USD_BE_210'], 'RRINF_CORR', 'USD_BE_210', c1, cmap1)
mapable2 = plot_main(ax1, data_usd_test10_be_curves['USD_BE_10Y'], data_usd_test10_be_curves['USD_BE_210'], 'USD_BE_10Y', 'USD_BE_210', c1, cmap1)
#mapable3 = plot_main(ax2, data_usd_test10_be_curves['RRINF_CORR'], data_usd_test10_be_curves['USD_BE_530'], 'RRINF_CORR', 'USD_BE_530', c1, cmap1)

