from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


data = pd.read_csv('C:/Temp/test/market_watch/staging/2020-05-12_Sov_historical.csv')
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()
data = data.drop(['TUR','GRC'], axis=1)
data = data.dropna()

data_pca = pca.PCA(data, rates_pca = True, momentum_size = None)

data_pca.plot_loadings(n=5, m=5)


data_pca

usgg2_mr_diagnosis =  bins.create_mr_diagnosis(data['USGG2YR'], 50, min_num_of_bins = 50, resample_freq = 'W')
fig = plt.figure(figsize=(20,10))
gs = gridspec.GridSpec(2, 1, height_ratios= [3,1], width_ratios= [1])
ax1 = fig.add_subplot(gs[0])
ax2 = fig.add_subplot(gs[1])

usgg2_mr_diagnosis.plot_bin_diagnosis_ax(ax1)
usgg2_mr_diagnosis.plot_bin_diagnosis_bin_size_ax(ax2)
fig.show()


dt_9ms_ago = data.index[-1] - DateOffset(years = 0, months=9, days=0)
usgg2_mr_diagnosis =  bins.create_mr_diagnosis(data[data.index > dt_9ms_ago]['USGG2YR'], 20, min_num_of_bins = 20, resample_freq = 'B')
fig2 = plt.figure(figsize=(20,10))
gs = gridspec.GridSpec(2, 1, height_ratios= [3,1], width_ratios= [1])
ax1 = fig2.add_subplot(gs[0])
ax2 = fig2.add_subplot(gs[1])

usgg2_mr_diagnosis.plot_bin_diagnosis_ax(ax1)
usgg2_mr_diagnosis.plot_bin_diagnosis_bin_size_ax(ax2)