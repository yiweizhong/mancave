import pandas as pd
import numpy as np
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations

from pandas.tseries.offsets import DateOffset


_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper

pd.set_option('display.expand_frame_repr', False)



def convert_atmf_vol_to_yield(option, vols):

    base = 1/np.sqrt(2*3.14*12)
    if option.upper() == '1M':
        return base * np.sqrt(1) * vols
    elif option.upper() == '2M':
        return base * np.sqrt(2) * vols     
    elif option.upper() == '3M':
        return base * np.sqrt(3) * vols
    elif option.upper() == '6M':
        return base * np.sqrt(6)* vols
    elif option.upper() == '1Y':
        return base * np.sqrt(12)* vols    
    elif option.upper() == '2Y':
        return base * np.sqrt(24)* vols
    elif option.upper() == '3Y':
        return base * np.sqrt(36)* vols
    elif option.upper() == '4Y':
        return base * np.sqrt(48)* vols
    elif option.upper() == '5Y':
        return base * np.sqrt(60)* vols    
    else:
        raise ValueError("unknown optin tenor {0}".format(option))


def plot_scatter_main(ax, xs, ys, colors, cmaps, ticklabelsize=8):
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 9)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    return mapable


def plot_scatter_endpoints(ax, xs, ys, colors, sizes, labels, markers, legend_loc=None,ncol=1, fontsize=8, lw =3):
    ds = [ax.scatter(xs[i], ys[i], c=colors[i], s=sizes[i], label=labels[i], marker=markers[i], linewidth= lw) for i in np.arange(len(xs))]
    if legend_loc is not None:
        lg =ax.legend(ds, labels, scatterpoints=1, fancybox=True, framealpha=0, loc=legend_loc, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lg)
    return ds
    

def plot_scatter_fits(ax, xss, yss, label_prefixes, colors, legend_loc=None, ncol=1, fontsize=8):
    #ploting multiple fits
    pcs = []
    for (xs, ys, label_prefix, color) in zip(xss, yss, label_prefixes, colors): 
        (beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(xs, ys)
        label = r'{1} = {3:.2f} + {2:.2f} * {0} with stdev {4:.2f} '.format(xs.name, ys.name, beta_x, beta_0, residual_ys_std)
        label = label_prefix + ', ' + label if label_prefix is not None else label
        pcs = pcs + ax.plot(xs, fitted_ys, lw=1, c=color, ls = '-', marker = '.', ms = 0, label=label)
        ax.plot(xs, residual_ys_p_1std, lw=0.5, c=color, ls = ':', ms = 0)
        ax.plot(xs, residual_ys_m_1std, lw=0.5, c=color, ls = ':', ms = 0)

    if legend_loc is not None:
        lgd = ax.legend(handles=pcs, loc=legend_loc, fancybox=True, framealpha=0, ncol=ncol, fontsize=fontsize)
        ax.add_artist(lgd)
    return pcs





def get_linear_betas(xs, ys):
    betas, _, _, _, _ = np.polyfit(list(xs), list(ys), deg = 1, full=True)
    (beta_x, beta_0) = betas
    #xs = df[df.columns[0]]
    #ys = df[df.columns[1]]
    #start_x = xs.min() 
    #last_x = xs.max()
    #step = (last_x - start_x)/100 
    #xs = np.arange(start_x,last_x, step)
    fitted_ys = [round(beta_0 + beta_x * x, 4) for x in list(xs)]
    residual_ys = [y - fitted_y for y, fitted_y in  zip(ys, fitted_ys)]
    residual_ys_std = np.std(residual_ys, ddof=1) 
    residual_ys_p_1std =  [fitted_y + residual_ys_std for fitted_y in  fitted_ys]
    residual_ys_m_1std =  [fitted_y - residual_ys_std for fitted_y in  fitted_ys]
    
    return betas, (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std)
   


def bootstrap_ifs_fwd(data):
    nc =  { c: int( c[4:].replace('_SWIT','').replace('Y','') ) for c in data.columns}
    data = data.rename(columns=nc)
    a = data.columns.tolist()[1:]
    b = data.columns.tolist()[:-1]
    fwd = {}
    fwd['{0}Y'.format(data.columns[0])] = data[data.columns[0]]
    for (ca,cb) in zip(a, b):
        term = ca - cb
        forward = cb
        cca = (1 + data[ca]/100).pow(ca)
        ccb = (1 + data[cb]/100).pow(cb)
        fwd['{0}Y{1}Y'.format(forward, term)] = np.log(cca/ccb)/term * 100
    fwd = pd.DataFrame(fwd)
    return fwd


def plot_main(ax, xs, ys, xlegend, ylegend, colors, cmaps, ticklabelsize=8):
    
    mapable = ax.scatter(xs, ys, c=colors, cmap=cmaps, marker='o', s = 6)
        
    ax.set_ylabel(ys.name, fontsize=8)
    ax.set_xlabel(xs.name, fontsize=8)
    ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
    ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
    
    for label,tick in zip(ax.get_yticklabels(), ax.get_yticks()):
        label.set_fontsize(ticklabelsize)
        if tick < 0:
            label.set_color("red")
        else:
            label.set_color("black")
    
    
    #for label,tick in zip(ax.get_xticklabels(), ax.get_xticks()):
    #    label.set_fontsize(ticklabelsize)
    #    if tick < 0:
    #        label.set_color("red")
    #    else:
    #        label.set_color("black")
    
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()    
    
    ymax_abs = max([abs(ymin), abs(ymax)])
    xmax_abs = max([abs(xmin), abs(xmax)])
    
    xymax = max([abs(ymin), abs(ymax), abs(xmin), abs(xmax)])
    
    ax.set_xlim(-1 * xmax_abs, xmax_abs)
    #ax.set_ylim(-1 * ymax_abs, ymax_abs)
    
    # Move left y-axis and bottim x-axis to centre, passing through (0,0)
    ax.spines['left'].set_position('center')
    #ax.spines['bottom'].set_position('center')
    # Eliminate upper and right axes
    ax.spines['right'].set_color('none')
    ax.spines['top'].set_color('none')
    # Show ticks in the left and lower axes only
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    
    ax.xaxis.set_major_locator(MultipleLocator(0.5))
    ax.yaxis.set_major_locator(MultipleLocator(1))
    ax.xaxis.grid(True, which='major')
    #ax.xaxis.grid(True, which='minor')
    ax.yaxis.grid(True, which='major')
    
    for label in ax.get_xticklabels(minor =False):
        label.set_fontsize(7)

    for label in ax.get_yticklabels(minor =False):
        label.set_fontsize(7)
        
    ax.set_ylabel(ylegend, fontsize=9)
    ax.yaxis.set_label_coords(0, 0.12)

    ax.set_xlabel(xlegend, fontsize=9)
    ax.xaxis.set_label_coords(0.85, 0.045)
    
    # end points
    start_x = xs.loc[xs.first_valid_index()]
    start_y = ys.loc[ys.first_valid_index()]
    start_date = xs.first_valid_index().strftime(r'%Y-%m-%d')
    
    ds1 = ax.scatter(start_x, start_y, c='#f5d9c8', s=90, alpha=0.5, label=start_date, edgecolors='b')
    
    last_x = xs.loc[xs.last_valid_index()]
    last_y = ys.loc[ys.last_valid_index()]
    last_date = xs.last_valid_index().strftime(r'%Y-%m-%d')

    ds2 = ax.scatter(last_x, last_y, c='darkred', s=90, alpha=1, label=last_date)
    
    
    ax.legend([ds1, ds2], [start_date, last_date], scatterpoints=1, fancybox=True, framealpha=0, loc=1, ncol=1, fontsize=9)
    
    return mapable


def rolling_apply(df, window_size, func, min_window_size=None):
    if min_window_size is None:
        min_window_size = window_size
    result = {}
    for i in range(1, len(df)+1):
        sub_df = df.iloc[max(i - window_size, 0):i, :] #I edited here

        if len(sub_df) >= min_window_size:            
            idx = sub_df.index[-1]
            result[idx] = func(sub_df)
    return result


def get_flattened_cov(df):
    covmatx = df.cov()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result


def get_flattened_corr(df):
    covmatx = df.corr()
    nodes = set([comb for comb in combinations(covmatx.columns.tolist() + covmatx.columns.tolist(), 2)])
    result = pd.Series({col + '_vs_' + row : covmatx[col][row]  for (col, row) in nodes})
    return result



rundate = '2021-03-23'


data_econ = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_ECON_historical.csv')  
data_econ['date'] = pd.to_datetime(data_econ['date'])
data_econ = data_econ.set_index('date').ffill()


data = pd.read_csv(r'C:/Temp/Test/market_watch/staging/' + rundate + '_BEIRR_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


data_swap = pd.read_csv('C:/Temp/test/market_watch/staging/'  + rundate +  '_SWAP_ALL_historical.csv')  
data_swap['date'] = pd.to_datetime(data_swap['date'])
data_swap = data_swap.set_index('date').ffill()

# data_bcs = pd.read_csv(r'C:/Dev/Models/QuantModels/#test/BCS_2020-07-29.csv')  
# data_bcs['date'] = pd.to_datetime(data_bcs['date'])
# data_bcs = data_bcs.set_index('date').ffill()
# data_bcs_rtn = (data_bcs - data_bcs.shift(1))/100

data_ifs = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SWIT_historical.csv')  
data_ifs['date'] = pd.to_datetime(data_ifs['date'])
data_ifs = data_ifs.set_index('date').ffill()



data_sovcds = pd.read_csv(r'C:/Temp/Test/market_watch/staging/'  + rundate +  '_SOVCDS5YR_historical.csv')  
data_sovcds['date'] = pd.to_datetime(data_sovcds['date'])
data_sovcds = data_sovcds.set_index('date').ffill().drop(['IND','GRC'],axis=1)



data_gov = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_GOV_historical.csv')  
data_gov['date'] = pd.to_datetime(data_gov['date'])
data_gov = data_gov.set_index('date').ffill()

data_vol = pd.read_csv('C:/Temp/test/market_watch/staging/' + rundate + '_VOL_historical.csv')  
data_vol['date'] = pd.to_datetime(data_vol['date'])
data_vol = data_vol.set_index('date').ffill()
data_vol_in_yields = pd.DataFrame({c: convert_atmf_vol_to_yield(c.split('_')[1], data_vol[c]) for c in data_vol.columns})





ifs_c = list(set([c[:3] for c in data_ifs.columns]))
ifs_cc = {cnty:[c for c in data_ifs.columns if cnty in c] for cnty in ifs_c}

data_fwd_ifs = {c: bootstrap_ifs_fwd(data_ifs[cc]) for (c,cc) in ifs_cc.items()}



US_cols =  [c for c in data.columns if 'US' in c]
US_cols.sort()
data_usd = data[US_cols].dropna()

data_usd['USD_NM_02Y'] = data_usd['USD_BE_02Y'] + data_usd['USD_RR_02Y'] 
data_usd['USD_NM_05Y'] = data_usd['USD_BE_05Y'] + data_usd['USD_RR_05Y']
data_usd['USD_NM_10Y'] = data_usd['USD_BE_10Y'] + data_usd['USD_RR_10Y']
data_usd['USD_NM_30Y'] = data_usd['USD_BE_30Y'] + data_usd['USD_RR_30Y']



data_usd['USD_NM_210'] = data_usd['USD_NM_10Y'] - data_usd['USD_NM_02Y']
data_usd['USD_NM_510'] = data_usd['USD_NM_10Y'] - data_usd['USD_NM_05Y']
data_usd['USD_NM_530'] = data_usd['USD_NM_30Y'] - data_usd['USD_NM_05Y']
data_usd['USD_NM_1030'] = data_usd['USD_NM_30Y'] - data_usd['USD_NM_10Y']
data_usd['USD_NM_020510'] = data_usd['USD_NM_10Y'] - 2 * data_usd['USD_NM_05Y'] + data_usd['USD_NM_02Y']
data_usd['USD_NM_051030'] = data_usd['USD_NM_30Y'] - 2 * data_usd['USD_NM_10Y'] + data_usd['USD_NM_05Y']


data_usd['USD_RR_210'] = data_usd['USD_RR_10Y'] - data_usd['USD_RR_02Y']
data_usd['USD_RR_510'] = data_usd['USD_RR_10Y'] - data_usd['USD_RR_05Y']
data_usd['USD_RR_530'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_05Y']
data_usd['USD_RR_230'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_02Y']
data_usd['USD_RR_1030'] = data_usd['USD_RR_30Y'] - data_usd['USD_RR_10Y']
data_usd['USD_RR_020510'] = data_usd['USD_RR_10Y'] - 2 * data_usd['USD_RR_05Y'] + data_usd['USD_RR_02Y']
data_usd['USD_RR_051030'] = data_usd['USD_RR_30Y'] - 2 * data_usd['USD_RR_10Y'] + data_usd['USD_RR_05Y']

data_usd['USD_BE_210'] = data_usd['USD_BE_10Y'] - data_usd['USD_BE_02Y']
data_usd['USD_BE_510'] = data_usd['USD_BE_10Y'] - data_usd['USD_BE_05Y']
data_usd['USD_BE_530'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_05Y']
data_usd['USD_BE_230'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_02Y']
data_usd['USD_BE_1030'] = data_usd['USD_BE_30Y'] - data_usd['USD_BE_10Y']
data_usd['USD_BE_020510'] = data_usd['USD_BE_10Y'] - 2 * data_usd['USD_BE_05Y'] + data_usd['USD_BE_02Y']
data_usd['USD_BE_051030'] = data_usd['USD_BE_30Y'] - 2 * data_usd['USD_BE_10Y'] + data_usd['USD_BE_05Y']

data_usd['USD_BE2NM30'] = data_usd['USD_NM_30Y'] - data_usd['USD_BE_02Y']

data_usd_rtn = data_usd - data_usd.shift(1)



AU_cols =  [c for c in data.columns if 'AU' in c]
AU_cols.sort()
data_AUD = data[AU_cols].dropna()

data_AUD['AUD_NM_05Y'] = data_AUD['AUD_BE_05Y'] + data_AUD['AUD_RR_05Y']
data_AUD['AUD_NM_10Y'] = data_AUD['AUD_BE_10Y'] + data_AUD['AUD_RR_10Y']
data_AUD['AUD_NM_20Y'] = data_AUD['AUD_BE_20Y'] + data_AUD['AUD_RR_20Y']


data_AUD['AUD_NM_510'] = data_AUD['AUD_NM_10Y'] - data_AUD['AUD_NM_05Y']
data_AUD['AUD_NM_520'] = data_AUD['AUD_NM_20Y'] - data_AUD['AUD_NM_05Y']
data_AUD['AUD_NM_1020'] = data_AUD['AUD_NM_20Y'] - data_AUD['AUD_NM_10Y']
data_AUD['AUD_NM_051020'] = data_AUD['AUD_NM_20Y'] - 2 * data_AUD['AUD_NM_10Y'] + data_AUD['AUD_NM_05Y']


data_AUD['AUD_RR_510'] = data_AUD['AUD_RR_10Y'] - data_AUD['AUD_RR_05Y']
data_AUD['AUD_RR_520'] = data_AUD['AUD_RR_20Y'] - data_AUD['AUD_RR_05Y']
data_AUD['AUD_RR_1020'] = data_AUD['AUD_RR_20Y'] - data_AUD['AUD_RR_10Y']
data_AUD['AUD_RR_051020'] = data_AUD['AUD_RR_20Y'] - 2 * data_AUD['AUD_RR_10Y'] + data_AUD['AUD_RR_05Y']


data_AUD['AUD_BE_510'] = data_AUD['AUD_BE_10Y'] - data_AUD['AUD_BE_05Y']
data_AUD['AUD_BE_520'] = data_AUD['AUD_BE_20Y'] - data_AUD['AUD_BE_05Y']
data_AUD['AUD_BE_1020'] = data_AUD['AUD_BE_20Y'] - data_AUD['AUD_BE_10Y']
data_AUD['AUD_BE_051020'] = data_AUD['AUD_BE_20Y'] - 2 * data_AUD['AUD_BE_10Y'] + data_AUD['AUD_BE_05Y']

data_AUD_rtn = data_AUD - data_AUD.shift(1)







# test1 general relationship
data_usd_test1 = data_usd[['USD_NM_10Y', 'USD_RR_10Y', 'USD_BE_10Y']]
data_usd_test1_pca = pca.PCA(data_usd_test1, rates_pca = True, momentum_size = None)
data_usd_test1_pca.plot_loadings(n=3, m=5)


#test2 during sell off
data_usd_test2 = data_usd[['USD_NM_10Y', 'USD_RR_10Y', 'USD_BE_10Y']]
data_usd_test2_rtn = data_usd_test2 - data_usd_test2.shift(5)
data_usd_test2 = data_usd_test2[data_usd_test2_rtn['USD_NM_10Y'] <0 ]

data_usd_test2['CAD_NM_05Y'] = data_swap['CAD_0Y_5Y'].reindex(data_usd_test1.index)

data_usd_test2_pca = pca.PCA(data_usd_test2, rates_pca = True, momentum_size = None)
data_usd_test2_pca.plot_loadings(n=3, m=5)



#test3 during rally
data_usd_test3 = data_usd[['USD_NM_10Y', 'USD_RR_10Y', 'USD_BE_10Y']]
data_usd_test3_rtn = data_usd_test3 - data_usd_test3.shift(5)
data_usd_test3 = data_usd_test3[data_usd_test3_rtn['USD_NM_10Y'] > 0]


data_usd_test3['CAD_NM_05Y'] = data_swap['CAD_0Y_5Y'].reindex(data_usd_test3.index)


data_usd_test3_pca = pca.PCA(data_usd_test3, rates_pca = True, momentum_size = None)
data_usd_test3_pca.plot_loadings(n=3, m=5)


#test4

dt_ago = data_usd.index[-1] - DateOffset(years = 11, months=0, days=0)
data_usd_test4 = data_usd[data_usd.index >= dt_ago]
data_usd_test4 = data_usd_test4.drop(['USD_NM_02Y','USD_NM_05Y','USD_NM_10Y','USD_NM_30Y'],axis=1)

 

data_usd_test4_pca = pca.PCA(data_usd_test4, rates_pca = True, momentum_size = None)

data_usd_test4_pca.plot_loadings(n=3, m=5)


#test 5

data_usd_test5 = data_usd[data_usd.index >= '2010-01-01']
data_usd_test5 = data_usd_test5[['USD_BE_02Y', 'USD_BE_05Y','USD_BE_10Y', 'USD_BE_30Y',
                                 'USD_RR_02Y', 'USD_RR_05Y','USD_RR_10Y', 'USD_RR_30Y']]

 
data_usd_test5_pca = pca.PCA(data_usd_test5, rates_pca = True, momentum_size = None)

data_usd_test5_pca.plot_loadings(n=3, m=30)


#test 6

data_usd_test6 = data_usd[data_usd.index >= '2008-01-01']
data_usd_test6 = data_usd_test6[['USD_BE_230', 'USD_RR_30Y']]

 
data_usd_test6_pca = pca.PCA(data_usd_test6, rates_pca = True, momentum_size = None)

data_usd_test6_pca.plot_loadings(n=2, m=30)

hedge_weighted_data, hedge_w, hedge_corr_test, hedge_description = data_usd_test6_pca.pc_hedge
mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = 'W')
mr_obj.create_mr_info_fig((15, 6), pca_obj = data_usd_test6_pca)




#test 7

assets = ['USD_BE_230', 'USD_RR_30Y', 'USD_NM_10Y','BCS', 'USD_BE2NM30']
data_usd_test7 = data_usd_rtn[data_usd.index >= '2007-01-01']
data_usd_test7['BCS'] = data_bcs_rtn.BlendedCreditSpread
data_usd_test7 = data_usd_test7.ffill().dropna()
data_usd_test7 = data_usd_test7[assets]


#rolling vol
data_rtns_d_1y_rolling_covs = pd.DataFrame(rolling_apply(data_usd_test7, 125, get_flattened_cov)).transpose()
data_rtns_d_1y_rolling_vol = data_rtns_d_1y_rolling_covs[[ a + '_vs_' + b for a,b in set([comb for comb in combinations(assets + assets, 2)]) if a == b]]
data_rtns_d_1y_rolling_vol = data_rtns_d_1y_rolling_vol.applymap(np.sqrt)

#rolling corr
data_rtns_d_1y_rolling_corr = pd.DataFrame(rolling_apply(data_usd_test7, 125, get_flattened_corr)).transpose()
data_rtns_d_1y_rolling_corr = data_rtns_d_1y_rolling_corr[[ a + '_vs_' + b for a,b in set([comb for comb in combinations(assets, 2)]) if a != b]]


data_rtns_d_1y_rolling_corr[['USD_BE_230_vs_USD_NM_10Y',
                            'USD_NM_10Y_vs_USD_BE2NM30',
                            'USD_BE_230_vs_USD_RR_30Y',
 'USD_BE_230_vs_BCS',
 'USD_RR_30Y_vs_BCS',
 'USD_NM_10Y_vs_BCS',
 'BCS_vs_USD_BE2NM30',
 'USD_RR_30Y_vs_USD_NM_10Y']].plot()

data_rtns_d_1y_rolling_vol.plot()


#test 8

assets = ['USD_BE_230', 'USD_RR_30Y', 'USD_NM_10Y','BCS', 'USD_BE2NM30']
data_usd_test7 = data_usd_rtn[data_usd.index >= '2007-01-01']
data_usd_test7['BCS'] = data_bcs_rtn.BlendedCreditSpread
data_usd_test7 = data_usd_test7.ffill().dropna()
data_usd_test7 = data_usd_test7[assets]



# test9 fwd curve



data_fwd_ifs_usd = data_fwd_ifs['USD']



df = pd.DataFrame({'Now':data_fwd_ifs_usd.iloc[-1], '2010 avg':data_fwd_ifs_usd.iloc[-2800:-2500].mean(), 
                   '2017-2019 avg':data_fwd_ifs_usd.iloc[-950:-150].mean()})

df.plot(kind='bar')


data_fwd_ifs.iloc[-950:-150]

data_fwd_ifs.tail(1).transpose().plot()


data_usd_test9 = data_fwd_ifs_usd[data_fwd_ifs_usd.index >= '2008-01-01']

data_usd_test9_pca = pca.PCA(data_usd_test9, rates_pca = True, momentum_size = None)

data_usd_test9_pca.plot_loadings(n=5, m=30)


df = pd.DataFrame({'USD':data_fwd_ifs['USD'].iloc[-1], 
                   'EUR':data_fwd_ifs['EUR'].iloc[-1], 
                   'FRA':data_fwd_ifs['FRA'].iloc[-1],
                   'GBP':data_fwd_ifs['GBP'].iloc[-1],
                   'ITL':data_fwd_ifs['ITL'].iloc[-1],
                   'SPA':data_fwd_ifs['SPA'].iloc[-1]
                   })


df.plot(kind='bar')



# test10

assets = ['USD_RR_10Y', 'USD_BE_10Y']
data_usd_test10 = data_usd[(data_usd.index >= '2007-01-01') & (data_usd.index < '2021-01-01')][assets]
data_d_1y_rolling_corr = pd.DataFrame(rolling_apply(data_usd_test10, 125, get_flattened_corr)).transpose()
data_d_1y_rolling_corr = data_d_1y_rolling_corr[[ a + '_vs_' + b for a,b in set([comb for comb in combinations(assets, 2)]) if a != b]]

data_usd_rtn_test10 = data_usd_rtn[(data_usd.index >= '2007-01-01') & (data_usd.index < '2021-01-01')][assets]
data_usd_rtn_test10_1y_rolling_corr = pd.DataFrame(rolling_apply(data_usd_rtn_test10, 125, get_flattened_corr)).transpose()
data_usd_rtn_test10_1y_rolling_corr = data_usd_rtn_test10_1y_rolling_corr[[ a + '_vs_' + b for a,b in set([comb for comb in combinations(assets, 2)]) if a != b]]


assets2 = ['USD_BE_210', 'USD_BE_230','USD_BE_530','USD_BE_10Y']
data_usd_test10_be_curves = data_usd[(data_usd.index >= '2007-01-01') & (data_usd.index < '2021-01-01')][assets2]
data_usd_test10_be_curves['RRINF_CORR'] = data_usd_rtn_test10_1y_rolling_corr
data_usd_test10_be_curves = data_usd_test10_be_curves.dropna()





# test11




data_usd_test11 = data_ifs[['USD_30Y_SWIT']]
data_usd_test11['USD_2S10S_SWIT'] = data_ifs['USD_10Y_SWIT'] - data_ifs['USD_2Y_SWIT']
data_usd_test11 = data_usd_test11.dropna().iloc[-1200:]


c1 = mpl.dates.date2num(data_usd_test11.index.to_pydatetime())
cmap1 = plt.cm.get_cmap('Reds')
fig = plt.figure()
ax = fig.add_subplot(111)

main_pc = plot_scatter_main(ax, data_usd_test11['USD_30Y_SWIT'], data_usd_test11['USD_2S10S_SWIT'], c1, cmap1)


start_y = data_usd_test11['USD_2S10S_SWIT'].iloc[0]
start_x = data_usd_test11['USD_30Y_SWIT'].iloc[0]
start_dt = data_usd_test11.first_valid_index().strftime('%Y-%m-%d')


last_y = data_usd_test11['USD_2S10S_SWIT'].iloc[-1]
last_x = data_usd_test11['USD_30Y_SWIT'].iloc[-1]
last_dt = data_usd_test11.last_valid_index().strftime('%Y-%m-%d')


endpt_pc = plot_scatter_endpoints(ax, [start_x, last_x], [start_y, last_y], ['lightgrey','black'], [80,80], [start_dt, last_dt], ['v','^'], legend_loc=1)



#plot fit
fit_pcs = plot_scatter_fits(ax, 
                    [data_usd_test11['USD_30Y_SWIT']], 
                    [data_usd_test11['USD_2S10S_SWIT']],
                    [''], 
                    ['black'], 
                    legend_loc = 2)






(beta_x, beta_0), (xs, fitted_ys, residual_ys, residual_ys_std, residual_ys_p_1std, residual_ys_m_1std) = get_linear_betas(data_usd_test11['USD_2S10S_SWIT'], data_usd_test11['USD_30Y_SWIT'])

label = r'{1} = {3:.2f} + {2:.2f} * {0} '.format('USD_2S10S_SWIT','USD_30Y_SWIT', beta_x, beta_0)
 
ls1 = ax.plot(xs, fitted_ys, lw=1, c='black', ls = '-', marker = '.', ms = 0, label=label)






#test12


data_aud_test12_pca = pca.PCA(data_AUD[data_AUD.index > '2015-01-01'], rates_pca = True, momentum_size = None)
data_aud_test12_pca.plot_loadings(n=3, m=5)


data_audusd_test12 = pd.concat([data_AUD[['AUD_BE_05Y', 'AUD_BE_20Y']], data_usd[['USD_BE_02Y', 'USD_BE_05Y', 'USD_BE_10Y', 'USD_BE_30Y']]], axis=1)



data_audusd_test12_pca = pca.PCA(data_audusd_test12[data_audusd_test12.index > '2020-01-01'], rates_pca = True, momentum_size = None)
data_audusd_test12_pca.plot_loadings(n=3, m=5)



#test13

usd_spot_swap_test13 = data_swap[['USD_0Y_1Y',
                         'USD_0Y_2Y',
                         'USD_0Y_3Y',
                         #'USD_0Y_4Y',
                         'USD_0Y_5Y',
                         #'USD_0Y_6Y',
                         #'USD_0Y_7Y',
                         #'USD_0Y_8Y',
                         #'USD_0Y_9Y',
                         'USD_0Y_10Y',
                         'USD_0Y_20Y',
                         'USD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")


usd_spot_gov_test13 = data_gov[[ 'USD_0Y_1Y',
 'USD_0Y_2Y',
 'USD_0Y_3Y',
 'USD_0Y_5Y',
 #'USD_0Y_7Y',
 'USD_0Y_10Y',
 'USD_0Y_20Y',
 'USD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")




usd_spot_ifs_test13 = data_ifs[['USD_1Y_SWIT',
                         'USD_2Y_SWIT',
                         'USD_3Y_SWIT',
                         #'USD_4Y_SWIT',
                         'USD_5Y_SWIT',
                         #'USD_6Y_SWIT',
                         #'USD_7Y_SWIT',
                         #'USD_8Y_SWIT',
                         #'USD_9Y_SWIT',
                         'USD_10Y_SWIT',
                         'USD_20Y_SWIT',
                         'USD_30Y_SWIT']]


#inflation swap date seems to be always 1day lag. check
usd_spot_rr_test13 = usd_spot_gov_test13 - usd_spot_ifs_test13.rename(lambda c: c.replace('_SWIT',''), axis=1).reindex(usd_spot_gov_test13.index).ffill()

usd_spot_ss_test13 = usd_spot_swap_test13.reindex(usd_spot_gov_test13.index).ffill() - usd_spot_gov_test13 


usd_fwd_swap_test13 = bootstrap_ifs_fwd(usd_spot_swap_test13)
usd_fwd_gov_test13 = bootstrap_ifs_fwd(usd_spot_gov_test13)
usd_fwd_ifs_test13 =  bootstrap_ifs_fwd(usd_spot_ifs_test13)
usd_fwd_rr_test13 =  bootstrap_ifs_fwd(usd_spot_rr_test13)
usd_fwd_ss_test13 =  usd_fwd_swap_test13.reindex(usd_fwd_gov_test13.index).ffill() - usd_fwd_gov_test13


def func(df): 
    return pd.DataFrame({
                      'Now':df.iloc[-2:].mean(),
                      #'2020-09-11':df[(df.index >= '2020-09-08') & (df.index <= '2020-09-12')].mean(),
                      #'2020-09-04':df[(df.index >= '2020-09-02') & (df.index <= '2020-09-05')].mean(),
                      #'2020-08-12':df[(df.index >= '2020-08-10') & (df.index <= '2020-08-13')].mean(),
                      #'Pre speech':df.iloc[-3],
                      '7d avg':df.iloc[-7:].mean(), 
                      '7d avg 7d ago':df.iloc[-14:-7].mean(), 
                      '7d avg 14d ago':df.iloc[-21:-14].mean(), 
                      '7d avg 21d ago':df.iloc[-28:-21].mean(), 
                      #'7d avg 28d ago':df.iloc[-35:-28].mean(), 
                      #'avg last 3 years':df.iloc[-97:-90].mean(),
                      #'7d avg 180 day ago':df.iloc[-187:-180].mean(),
                      #'7d avg 360 day ago':df.iloc[-367:-360].mean(),
                      #'2010-2011 avg': df[(df.index > '2010-01-01') & (df.index < '2011-12-31')].mean(),
                      #'2010-Oct-2011-Jul': df[(df.index > '2010-10-01') & (df.index < '2011-07-01')].mean(),
                      #'2012-2013 avg': df[(df.index > '2012-01-01') & (df.index < '2013-12-31')].mean(),
                      #'2016-2019 avg': df[(df.index > '2016-01-01') & (df.index < '2019-12-31')].mean(),
                      #'2016-08': df[(df.index > '2016-08-01') & (df.index < '2016-09-01')].mean(),
                      #'2016-09': df[(df.index > '2016-09-01') & (df.index < '2016-10-01')].mean(),
                      #'2016-10': df[(df.index > '2016-10-01') & (df.index < '2016-11-01')].mean()
                      })





f1 = plt.figure(figsize = (30, 20))
gs = gridspec.GridSpec(5, 2, height_ratios=[1,1,1,1,1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[3])
ax4 = plt.subplot(gs[4])
ax5 = plt.subplot(gs[5])
ax6 = plt.subplot(gs[6])
ax7 = plt.subplot(gs[7])
ax8 = plt.subplot(gs[8])
ax9 = plt.subplot(gs[9])



#f1.title('US interest curves snapshot')

#ifs
func(usd_spot_ifs_test13).rename(lambda c: c.split('_')[1]).plot(ax=ax0)
#plt.xticks(np.arange(len(func(usd_spot_ifs_test13).index)).tolist(), [c.split('_')[1] for c in func(usd_spot_ifs_test13).index])
ax0.set_title('Spot inflation swap curves')

func(usd_fwd_ifs_test13).plot(ax=ax1)
#plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())
ax1.set_title('Forward inflation swap curves')
ax0.set_ylim([1.4, 2.2])
ax1.set_ylim([1.4, 2.2])

#swap
func(usd_spot_swap_test13).rename(lambda c: c.split('_')[1]).plot(ax=ax2)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
ax2.set_title('Spot nominal swap curves')

func(usd_fwd_swap_test13).plot(ax=ax3)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
ax3.set_title('Forward nominal swap curves')

ax2.set_ylim([0, 1.5])
ax3.set_ylim([0, 1.5])

#gov

func(usd_spot_gov_test13).rename(lambda c: c.split('_')[1]).plot(ax=ax4)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
ax4.set_title('Spot treasury curves')

func(usd_fwd_gov_test13).plot(ax=ax5)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
ax5.set_title('Forward treasury curves')

ax4.set_ylim([0, 2.2])
ax5.set_ylim([0, 2.2])

#rr

func(usd_spot_rr_test13).rename(lambda c: c.split('_')[1]).plot(ax=ax6)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
ax6.set_title('Spot rr curves, gov - ifs')

func(usd_fwd_rr_test13).plot(ax=ax7)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
ax7.set_title('Forwardt rr curves, gov - ifs')

ax6.set_ylim([-2, 0])
ax7.set_ylim([-2, 0])



#ss

func(usd_spot_ss_test13).rename(lambda c: c.split('_')[1]).plot(ax=ax8)
#plt.xticks(np.arange(len(func(usd_spot_swap_test13).index)), [c.split('_')[1] for c in func(usd_spot_swap_test13).index])
ax8.set_title('Spot swap spread curves')

func(usd_fwd_ss_test13).plot(ax=ax9)
#plt.xticks(np.arange(len(func(usd_fwd_swap_test13).index)), func(usd_fwd_swap_test13).index.tolist())
ax9.set_title('Forwardt swap spread curves')

ax8.set_ylim([-0.8, 0.3])
ax9.set_ylim([-0.8, 0.3])



f1.tight_layout()







pca.PCA(usd_spot_ifs_test13[usd_spot_ifs_test13.index > '2005-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3, m=100)


pca.PCA(usd_spot_ifs_test13[usd_spot_ifs_test13.index > '2005-01-01'], rates_pca = True, momentum_size = None).plot_valuation_history(300, n=[1,2], w=16, h=12, rows=4, cols=4, normalise_valuation_yaxis = False,  normalise_data_yaxis = False )











usd_fwd_swap_curvature = usd_fwd_norm_test13.sub(usd_fwd_norm_test13['1Y'], axis=0) 
#norm_curvature = usd_fwd_norm_test13[usd_fwd_norm_test13.columns[1:]].sub(usd_fwd_norm_test13[usd_fwd_norm_test13.columns[:-1]].values, axis=0)
usd_fwd_ifs_curvature = usd_fwd_ifs_test13.sub(usd_fwd_ifs_test13['1Y'], axis=0) 
usd_fwd_gov_curvature = usd_fwd_gov_test13.sub(usd_fwd_gov_test13['1Y'], axis=0)


usd_fwd_swap_move = usd_fwd_norm_test13.iloc[-1] - usd_fwd_norm_test13.iloc[-3] 
usd_fwd_ifs_move = usd_fwd_ifs_test13.iloc[-1] -  usd_fwd_ifs_test13.iloc[-3]
usd_fwd_gov_move = usd_fwd_gov_test13.iloc[-1] - usd_fwd_gov_test13.iloc[-3]



cad_norm_curvature = cad_fwd_norm_test13.sub(cad_fwd_norm_test13['1Y'], axis=0) 



norm_curvature_7d_mva = norm_curvature.rolling(7).mean().dropna()
norm_curvature_mtha = norm_curvature.groupby(norm_curvature.index.to_period("Q")).agg('mean')

norm_curvature_7d_mva_variance = norm_curvature_7d_mva.iloc[:-1] - norm_curvature_7d_mva.iloc[-1]
(norm_curvature_7d_mva_variance.apply(lambda x:x*x).sum(axis=1)).iloc[:-200].sort_values().head(50)
(norm_curvature_7d_mva_variance.std(axis=1)).iloc[:-200].sort_values().head(50)
func(norm_curvature_7d_mva).plot()

func(usd_fwd_norm_test13.rolling(7).mean().dropna()).plot()
plt.xticks(np.arange(len(func(usd_fwd_norm_test13.rolling(7).mean().dropna()).index)), func(usd_fwd_norm_test13.rolling(7).mean().dropna()).index.tolist())


norm_curvature_mtha[(norm_curvature_mtha.index >= '2010-01-01') & (norm_curvature_mtha.index <= '2012-01-01')].transpose().plot()
plt.xticks(np.arange(len(func(usd_fwd_norm_test13.rolling(7).mean().dropna()).index)), func(usd_fwd_norm_test13.rolling(7).mean().dropna()).index.tolist())


(usd_spot_ifs - usd_spot_ifs.shift(1)).rolling(7).std().tail(60).plot()


pca.PCA(norm_curvature[norm_curvature.index > '2010-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3, m=5)



func(usd_fwd_ifs_test13).plot()
plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())



func(usd_fwd_ifs_test13).plot()
plt.xticks(np.arange(len(func(usd_fwd_ifs_test13).index)), func(usd_fwd_ifs_test13).index.tolist())





f1 = plt.figure(figsize = (60, 20))
gs = gridspec.GridSpec(1, 3, height_ratios=[1], width_ratios=[1,1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])

func(usd_fwd_gov_curvature).plot(ax=ax0)
plt.sca(ax0)
plt.xticks(np.arange(len(func(usd_fwd_gov_curvature).index)), func(usd_fwd_gov_curvature).index.tolist())
ax0.set_ylim(-0.5, 2)
ax0.axhline(linewidth=0.8, y=0, color='black')
ax0.set_title('Gov fwds curvature')


func(usd_fwd_swap_curvature).plot(ax=ax1)
plt.sca(ax1)
plt.xticks(np.arange(len(func(usd_fwd_swap_curvature).index)), func(usd_fwd_swap_curvature).index.tolist())
ax1.set_ylim(-0.5, 2)
ax1.axhline(linewidth=0.8, y=0, color='black')
ax1.set_title('Swap fwds curvature')



func(usd_fwd_ifs_curvature).plot(ax=ax2)
plt.sca(ax2)
plt.xticks(np.arange(len(func(usd_fwd_ifs_curvature).index)), func(usd_fwd_ifs_curvature).index.tolist())
ax2.set_ylim(-0.5, 2)
ax2.axhline(linewidth=0.8, y=0, color='black')
ax2.set_title('Infs fwds curvature')










#test14

aud_spot_swap = data_swap[['AUD_0Y_1Y',
                         'AUD_0Y_2Y',
                         'AUD_0Y_3Y',
                         #'USD_0Y_4Y',
                         'AUD_0Y_5Y',
                         #'USD_0Y_6Y',
                         #'USD_0Y_7Y',
                         #'USD_0Y_8Y',
                         #'USD_0Y_9Y',
                         'AUD_0Y_10Y',
                         'AUD_0Y_20Y',
                         'AUD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")


aud_spot_gov = data_gov[[ 'AUD_0Y_1Y',
 'AUD_0Y_2Y',
 'AUD_0Y_3Y',
 'AUD_0Y_5Y',
 #'USD_0Y_7Y',
 'AUD_0Y_10Y',
 'AUD_0Y_20Y',
 'AUD_0Y_30Y']].rename(lambda c: c.replace("_0Y_","_"), axis="columns")


audusd_spot_gov = pd.concat( [data_gov[[ 
 'AUD_0Y_3Y',
 'AUD_0Y_5Y',
 'AUD_0Y_10Y',
 'AUD_0Y_15Y'
 ]].rename(lambda c: c.replace("_0Y_","_"), axis="columns").shift(1),
data_gov[[ 
 
 'USD_0Y_2Y',
 'USD_0Y_5Y',
 'USD_0Y_10Y',
 'USD_0Y_30Y',
 ]].rename(lambda c: c.replace("_0Y_","_"), axis="columns")
],axis=1)





aud_fwd_swap_test14 = bootstrap_ifs_fwd(aud_spot_swap)
aud_fwd_gov_test14 = bootstrap_ifs_fwd(aud_spot_gov)



def func(df): 
    return pd.DataFrame({
                      'Now':df.iloc[-1],
                      '7d avg':df.iloc[-7:].mean(), 
                      #'7d avg 7d ago':df.iloc[-14:-7].mean(), 
                      #'7d avg 14d ago':df.iloc[-21:-14].mean(), 
                      #'7d avg 21d ago':df.iloc[-28:-21].mean(), 
                      '7d avg 28d ago':df.iloc[-35:-28].mean(), 
                      })







f1 = plt.figure(figsize = (20, 10))
gs = gridspec.GridSpec(2, 2, height_ratios=[1,1], width_ratios=[1,1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])
ax3 = plt.subplot(gs[3])


func(aud_spot_swap).plot(ax = ax0)
plt.sca(ax0)
plt.xticks(np.arange(len(func(aud_spot_swap).index)), func(aud_spot_swap).index.tolist())
ax0.set_ylim(0, 2.5)
ax0.set_title('Swap spot')


func(aud_spot_gov).plot(ax = ax1)
plt.sca(ax1)
plt.xticks(np.arange(len(func(aud_spot_gov).index)), func(aud_spot_gov).index.tolist())
ax1.set_ylim(0, 2.5)
ax1.set_title('Gov spot')


func(aud_fwd_swap_test14).plot(ax=ax2)
plt.sca(ax2)
plt.xticks(np.arange(len(func(aud_fwd_swap_test14).index)), func(aud_fwd_swap_test14).index.tolist())
ax2.set_ylim(0, 2.5)
ax2.set_title('Swap fwd')


func(aud_fwd_gov_test14).plot(ax=ax3)
plt.sca(ax3)
plt.xticks(np.arange(len(func(aud_fwd_gov_test14).index)), func(aud_fwd_gov_test14).index.tolist())
ax3.set_ylim(0, 2.5)
ax3.set_title('Gov fwd')



pca.PCA(aud_spot_swap[aud_spot_swap.index > '2010-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3, m=5)

pca.PCA(aud_fwd_swap_test14[aud_fwd_swap_test14.index > '2010-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3, m=5)



pca.PCA(audusd_spot_gov[audusd_spot_gov.index > '2010-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3, m=5)

audusd_spot_gov_rtn = (audusd_spot_gov - audusd_spot_gov.shift(7))
audusd_spot_gov_rtn = audusd_spot_gov_rtn.groupby(audusd_spot_gov_rtn.index.to_period("W")).agg('last')
pca.PCA(audusd_spot_gov_rtn[audusd_spot_gov_rtn.index > '2020-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3, m=10)




###########################################################################################################################################################

#test15

usd_spot_ifs = data_ifs[['USD_1Y_SWIT',
                         'USD_2Y_SWIT',
                         'USD_9Y_SWIT',
                         'USD_10Y_SWIT'
                         ]].dropna()

usd_spot_ifs_1yr_later = usd_spot_ifs.shift(-262).dropna() 

usd_spot_ifs['IFS_2Y_CAP_RTN'] = usd_spot_ifs['USD_2Y_SWIT'] - usd_spot_ifs_1yr_later['USD_1Y_SWIT'] 
usd_spot_ifs['IFS_10Y_CAP_RTN'] = usd_spot_ifs['USD_10Y_SWIT'] - usd_spot_ifs_1yr_later['USD_9Y_SWIT']  
usd_spot_ifs['IFS_2Y_CARRY_RTN'] = usd_spot_ifs['USD_2Y_SWIT']  
usd_spot_ifs['IFS_10Y_CARRY_RTN'] = usd_spot_ifs['USD_10Y_SWIT']  


usd_spot_ifs = usd_spot_ifs.dropna()



f1 = plt.figure(figsize = (20, 20))
gs = gridspec.GridSpec(3, 1, height_ratios=[1,1,2], width_ratios=[1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])



usd_2yr_ifs_return_p,usd_2yr_ifs_return_n = usd_spot_ifs[['IFS_2Y_CAP_RTN', 'IFS_2Y_CARRY_RTN']].clip(lower=0), usd_spot_ifs[['IFS_2Y_CAP_RTN', 'IFS_2Y_CARRY_RTN']].clip(upper=0)

usd_2yr_ifs_return_p.plot.area(ax=ax0, stacked=True, linewidth=0.)
# reset the color cycle
ax0.set_prop_cycle(None)
# stacked area plot of negative values, prepend column names with '_' such that they don't appear in the legend
usd_2yr_ifs_return_n.rename(columns=lambda x: '_' + x).plot.area(ax=ax0, stacked=True, linewidth=0.)
# rescale the y axis
ax0.set_ylim([-10, 10])


usd_10yr_ifs_return_p,usd_10yr_ifs_return_n = usd_spot_ifs[['IFS_10Y_CAP_RTN', 'IFS_10Y_CARRY_RTN']].clip(lower=0), usd_spot_ifs[['IFS_10Y_CAP_RTN', 'IFS_10Y_CARRY_RTN']].clip(upper=0)

usd_10yr_ifs_return_p.plot.area(ax= ax1, stacked=True, linewidth=0.)
# reset the color cycle
ax1.set_prop_cycle(None)
# stacked area plot of negative values, prepend column names with '_' such that they don't appear in the legend
usd_10yr_ifs_return_n.rename(columns=lambda x: '_' + x).plot.area(ax=ax1, stacked=True, linewidth=0.)
# rescale the y axis
ax1.set_ylim([-10, 10])

ifs_risk_premium = usd_spot_ifs[['IFS_10Y_CAP_RTN', 'IFS_10Y_CARRY_RTN']].sum(axis=1) - usd_spot_ifs[['IFS_2Y_CAP_RTN', 'IFS_2Y_CARRY_RTN']].sum(axis=1) 


ifs_risk_premium = pd.DataFrame({'ifs_risk_premium': ifs_risk_premium, 
                                 'ifs_risk_premium_1y_mva': ifs_risk_premium.rolling(262).mean(),
                                 'ifs_risk_premium_3y_mva': ifs_risk_premium.rolling(780).mean()
                                 }).plot(ax=ax2)
ax2.axhline(linewidth=0.8, y=0, color='black')
ax2.axhline(linewidth=0.8, y=0.5, color='red')





usd_spot_ifs = data_ifs[['USD_1Y_SWIT',
                         'USD_2Y_SWIT',
                         'USD_9Y_SWIT',
                         'USD_10Y_SWIT'
                         ]].dropna()


usd_spot_swap = data_swap[['USD_0Y_1Y',
                         'USD_0Y_2Y',
                         'USD_0Y_9Y',
                         'USD_0Y_10Y'
                         ]].dropna()




usd_spot_rr = -(usd_spot_ifs - usd_spot_swap.reindex(usd_spot_ifs.index).values)

usd_spot_rr_1yr_later = usd_spot_rr.shift(-262).dropna() 

usd_spot_rr['RR_2Y_CAP_RTN'] = usd_spot_rr['USD_2Y_SWIT'] - usd_spot_rr_1yr_later['USD_1Y_SWIT'] 
usd_spot_rr['RR_10Y_CAP_RTN'] = usd_spot_rr['USD_10Y_SWIT'] - usd_spot_rr_1yr_later['USD_9Y_SWIT']  
usd_spot_rr['RR_2Y_CARRY_RTN'] = usd_spot_rr['USD_2Y_SWIT']  
usd_spot_rr['RR_10Y_CARRY_RTN'] = usd_spot_rr['USD_10Y_SWIT']  

usd_spot_rr = usd_spot_rr.dropna()



f1 = plt.figure(figsize = (20, 20))
gs = gridspec.GridSpec(3, 1, height_ratios=[1,1,2], width_ratios=[1])
ax0 = plt.subplot(gs[0])
ax1 = plt.subplot(gs[1])
ax2 = plt.subplot(gs[2])

usd_2yr_rr_return_p,usd_2yr_rr_return_n = usd_spot_rr[['RR_2Y_CAP_RTN', 'RR_2Y_CARRY_RTN']].clip(lower=0), usd_spot_rr[['RR_2Y_CAP_RTN', 'RR_2Y_CARRY_RTN']].clip(upper=0)

usd_2yr_rr_return_p.plot.area(ax=ax0, stacked=True, linewidth=0.)
# reset the color cycle
ax0.set_prop_cycle(None)
# stacked area plot of negative values, prepend column names with '_' such that they don't appear in the legend
usd_2yr_rr_return_n.rename(columns=lambda x: '_' + x).plot.area(ax=ax0, stacked=True, linewidth=0.)
# rescale the y axis
ax0.set_ylim([-10, 10])


usd_10yr_rr_return_p,usd_10yr_rr_return_n = usd_spot_rr[['RR_10Y_CAP_RTN', 'RR_10Y_CARRY_RTN']].clip(lower=0), usd_spot_rr[['RR_10Y_CAP_RTN', 'RR_10Y_CARRY_RTN']].clip(upper=0)

usd_10yr_rr_return_p.plot.area(ax= ax1, stacked=True, linewidth=0.)
# reset the color cycle
ax1.set_prop_cycle(None)
# stacked area plot of negative values, prepend column names with '_' such that they don't appear in the legend
usd_10yr_rr_return_n.rename(columns=lambda x: '_' + x).plot.area(ax=ax1, stacked=True, linewidth=0.)
# rescale the y axis
ax1.set_ylim([-10, 10])

rr_risk_premium = usd_spot_rr[['RR_10Y_CAP_RTN', 'RR_10Y_CARRY_RTN']].sum(axis=1) - usd_spot_rr[['RR_2Y_CAP_RTN', 'RR_2Y_CARRY_RTN']].sum(axis=1) 

rr_risk_premium = pd.DataFrame({'rr_risk_premium': rr_risk_premium, 
                                 'rr_risk_premium_1y_mva': rr_risk_premium.rolling(262).mean(),
                                 'rr_risk_premium_3y_mva': rr_risk_premium.rolling(780).mean()
                                 }).plot(ax=ax2)
ax2.axhline(linewidth=0.8, y=0, color='black')
ax2.axhline(linewidth=0.8, y=0.5, color='red')




###########################################################################################################################################################

#test16


swap_test16 = data_swap[['USD_0Y_30Y',
                           'AUD_0Y_30Y',
                           'EUR_0Y_30Y'
                         ]].dropna()


swap_return_test16 = swap_test16 - swap_test16.shift(1)

assets = swap_test16.columns.tolist()

data_rtns_d_30_rolling_covs = pd.DataFrame(rolling_apply(swap_return_test16, 30, get_flattened_cov)).transpose()
data_rtns_d_30_rolling_vol = data_rtns_d_30_rolling_covs[[ a + '_vs_' + b for a,b in set([comb for comb in combinations(assets + assets, 2)]) if a == b]]
data_rtns_d_30_rolling_vol = data_rtns_d_30_rolling_vol.applymap(np.sqrt)


data_rtns_d_30_rolling_vol.plot()


pca.PCA(swap_return_test16[swap_return_test16.index > '2018-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3, m=10)



vol_in_yields_test16 = data_vol_in_yields[['USD_3M_10Y',
                                             'USD_3M_30Y',
                                             'AUD_3M_10Y',
                                             'AUD_3M_30Y',
                                             'EUR_3M_10Y',
                                             'EUR_3M_30Y',
                                             'GBP_3M_10Y',
                                             'GBP_3M_30Y'
                                         ]].dropna()



pca.PCA(vol_in_yields_test16[vol_in_yields_test16.index > '2018-01-01'], rates_pca = True, momentum_size = None).plot_loadings(n=3, m=10)



###########################################################################################################################################################





###############################################################3

f1 = plt.figure(figsize = (15, 5))
gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
ax0 = plt.subplot(gs[0])

pd.DataFrame({'Gov':usd_fwd_gov_move,'Swap':usd_fwd_swap_move,'Ifs':usd_fwd_ifs_move }).plot(ax=ax0, kind='bar')
ax0.set_title('Moves on jackson holes')

###########################

f1 = plt.figure(figsize = (20, 20))
gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
ax0 = plt.subplot(gs[0])
#ax1 = plt.subplot(gs[1])
#ax2 = plt.subplot(gs[2])

c1 = mpl.dates.date2num(data_usd_test10_be_curves.index.to_pydatetime())
c2 = data_usd_test10_be_curves['RRINF_CORR']
cmap1 = plt.cm.get_cmap('Reds')
cmap2 = plt.cm.get_cmap('RdYlBu') 

mapable1 = plot_main(ax0, data_usd_test10_be_curves['USD_BE_10Y'], data_usd_test10_be_curves['USD_BE_210'], 'USD_BE_10Y', 'USD_BE_210', c1, cmap2)
plt.colorbar(mapable1)


mapable1 = plot_main(ax0, data_usd_test10_be_curves['RRINF_CORR'], data_usd_test10_be_curves['USD_BE_210'], 'RRINF_CORR', 'USD_BE_210', c1, cmap1)
mapable2 = plot_main(ax1, data_usd_test10_be_curves['USD_BE_10Y'], data_usd_test10_be_curves['USD_BE_210'], 'USD_BE_10Y', 'USD_BE_210', c1, cmap1)
#mapable3 = plot_main(ax2, data_usd_test10_be_curves['RRINF_CORR'], data_usd_test10_be_curves['USD_BE_530'], 'RRINF_CORR', 'USD_BE_530', c1, cmap1)




###############################################################

#test17 inflation swap forecast for regimes

###############################################################


def plot_table(df):    
    f1 = plt.figure(figsize = (10, 6))
    gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
    ax0 = plt.subplot(gs[0])
    df.plot(ax=ax0, table=True)
    ax0.xaxis.tick_top()
    f1.tight_layout()
    return f1
    


usd_spot_ifs_test17 = data_ifs[['USD_1Y_SWIT',
                             'USD_2Y_SWIT',
                             'USD_3Y_SWIT',
                             'USD_4Y_SWIT',
                             'USD_5Y_SWIT',
                             #'USD_6Y_SWIT',
                             #'USD_7Y_SWIT',
                             #'USD_8Y_SWIT',
                             #'USD_9Y_SWIT',
                             'USD_10Y_SWIT',
                             'USD_20Y_SWIT',
                             'USD_30Y_SWIT']]


usd_fwd_ifs_test17 =  bootstrap_ifs_fwd(usd_spot_ifs_test17)

usd_pce_test17 =  data_econ['US_COREPCE']

#usd_spot_ifs_test13['PCE'] = data_econ['US_COREPCE']
#usd_spot_ifs_test13 = usd_spot_ifs_test13.ffill().resample('M').last()



# For forecast last 5yrs are chosen as the base line

regime_forecast_start_dt = '2015-01-01'

usd_spot_ifs_test17_pca = pca.PCA(usd_spot_ifs_test17[usd_spot_ifs_test17.index > regime_forecast_start_dt], rates_pca = True, momentum_size = None)
usd_spot_ifs_test17_pca.plot_loadings(n=5)



regime_forecast  = pd.DataFrame({'slow_recovery':(usd_spot_ifs_test17_pca.loadings['Loading1'] * 0.3  
                                             + usd_spot_ifs_test17_pca.data_mean),
                                'double_dip':(usd_spot_ifs_test17_pca.loadings['Loading1'] * - 5 
                                             + usd_spot_ifs_test17_pca.loadings['Loading2'] * - 0.2 
                                             + usd_spot_ifs_test17_pca.data_mean),
                                'growth_surprise':(usd_spot_ifs_test17_pca.loadings['Loading1'] * 0.5  
                                             + usd_spot_ifs_test17_pca.data_mean),
                                'taper_tanstrum':(usd_spot_ifs_test17_pca.loadings['Loading1'] * 1.1  
                                             + usd_spot_ifs_test17_pca.data_mean),
                                'current':usd_spot_ifs_test17_pca.data.iloc[-1],
                                'regime_mean':usd_spot_ifs_test17_pca.data_mean                                
                                })


regime_forecast_changes  = regime_forecast.copy() 
regime_forecast_changes.slow_recovery = regime_forecast_changes.slow_recovery - regime_forecast_changes.current
regime_forecast_changes.double_dip = regime_forecast_changes.double_dip - regime_forecast_changes.current
regime_forecast_changes.growth_surprise = regime_forecast_changes.growth_surprise - regime_forecast_changes.current
regime_forecast_changes.taper_tanstrum = regime_forecast_changes.taper_tanstrum - regime_forecast_changes.current


plot_table(regime_forecast.round(2))

    regime_forecast_changes.round(2)
