from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset, BDay
from itertools import permutations, combinations
from collections import namedtuple
_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper



sims = st.simulate_OU_SDE(-5, 0.2, 1, 0.3, 1, 100, lambda x: (False, 0), lambda x: (False, 0))
s = pd.Series(sims[0], index=pd.date_range(start='2018-01-01', periods=len(sims[0]), freq='B'))
s.name = 'sample1'
s_1r = s - s.shift(1)
s_1r.dropna().std()

ou_obj = st.OrnsteinUhlenbeck(s.tail(90))

print(ou_obj.theta_hat)
print(ou_obj.mu_hat)
print(ou_obj.sigma_hat)
#ou_obj.moments

s.plot()
