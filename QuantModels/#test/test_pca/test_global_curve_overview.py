from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper

# data
data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-08-13_SWAP_ALL_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()

#making amendments
data['KRW_0Y_30Y'] = np.NaN



#function

def get_data(data, data_date):
    return data[data.index == data_date]


def restructure(data, tenors, curve_names):
    df_data = {}
    curve_names.sort()    
    for curve in curve_names:
        curve_columns = [curve + '_' + tenor for tenor in tenors]
        curve_data = data[curve_columns].rename(lambda x: x.split('_')[2], axis='columns')
        ds = curve_data.iloc[0]
        df_data[curve] = ds 
    df = pd.DataFrame(df_data)
    df.index.name = 'Tenor'
    return df


def plot_spot(data, countries):
    data[countries].plot()

# filter for a date
data_sample = get_data(data, data.index.max())
all_rates_curve_names = list(set(data_sample.columns.map(lambda x: '{}_{}'.format(x.split('_')[0], x.split('_')[1]))))
spot_rates_curve_names = [curve_name for curve_name in all_rates_curve_names if '_0Y' in curve_name] 

all_rates_tenors = ['1Y','2Y','3Y','5Y','10Y']
spot_rates_tenors =  ['3M', '1Y', '2Y', '3Y', '4Y', '5Y', '6Y', '7Y', '8Y', '9Y', '10Y', '30Y']

all_rates_curves = restructure(data_sample, all_rates_tenors, all_rates_curve_names)
 
spot_rates_curves = restructure(data_sample, spot_rates_tenors, spot_rates_curve_names).rename(lambda x:x.replace('_0Y',''),axis='columns')


from pandas.plotting import parallel_coordinates
plt.figure()
parallel_coordinates(spot_rates_curves[['AUD','NZD','CAD','USD','EUR','GBP','SEK','JPY']].T.reset_index(),'index')


#
def calc_fwd_spread(df, denorm=True):
    lcol = df.columns[0]
    rcol = df.columns[1]
    denorminator = int(rcol.split('_')[1].replace('Y','')) - int(lcol.split('_')[1].replace('Y',''))
    denorminator = denorminator if denorm else  denorminator/abs(denorminator)
    print denorminator
    if denorminator >= 0:
        return '{}_{}'.format(rcol, lcol), round((df[rcol][0] - df[lcol][0])/denorminator * 100,2) 
    else:
        return '{}_{}'.format(lcol, rcol), round(df[lcol][0] - df[rcol][0]/denorminator * 100,2)
    

rates_xy_1y = data_sample[[ col for col in data_sample.columns if col.endswith('_1Y')]]
countries_xy_1y = list(set([ col[:3] for col in rates_xy_1y.columns]))
results = {}
for c in countries_xy_1y:
    rates_xy_1y_for_c = rates_xy_1y[[col for col in rates_xy_1y.columns if col.startswith(c)]]
    c_pairs = combinations(rates_xy_1y_for_c.columns,2)
    for pair in c_pairs:
        rates_xy_1y_for_pair = rates_xy_1y_for_c[list(pair)]
        key, val = calc_fwd_spread(rates_xy_1y_for_pair)
        results[key] = val
        
        

