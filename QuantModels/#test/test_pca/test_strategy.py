from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset
from itertools import permutations, combinations
_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper
from statistics.strategy import rv 



data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-03-28_FWD_G10_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()


carry = pd.read_csv('C:/Temp/test/market_watch/staging/CarryRoll_2018-03-28.csv')
carry = carry.set_index('sec')
carry_repo = carry['roll'] + carry['carry']

trading_cost = pd.Series({'AUD_0Y_1Y':0, 'AUD_0Y_2Y':0}, index = ['AUD_0Y_1Y', 'AUD_0Y_2Y'])
trading_cost.index.name = 'sec'
trading_cost_repo = trading_cost

components = data[['AUD_0Y_1Y', 'AUD_0Y_2Y']] 
#components['blah'] = 1
#componet_weights = pd.Series({'AUD_0Y_1Y':1, 'AUD_0Y_2Y':1, 'blah': 0}) 
#ratestrategy = rv.RatesStrategy(components, componet_weights, carry_repo, trading_cost_repo)
#ratestrategy._component_weighted_carries

rv.create_pca_based_mr_rates_trade(components, carry_repo=carry_repo, trading_cost_repo=trading_cost_repo, 
                                   beta = None, existing_risk = None, num_bins = 20,min_num_bins = 20)
