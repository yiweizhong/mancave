from __future__ import division
import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import *
from itertools import permutations, combinations

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca


data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-03-15_FWD_G10_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')


dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_9mths_ago = data.index[-1] - DateOffset(years = 0, months=9, days=0)
dt_3mths_ago = data.index[-1] - DateOffset(years = 0, months=3, days=0)
dt_1mths_ago = data.index[-1] - DateOffset(years = 0, months=1, days=0)


data_sub = data[data.index> dt_1yrs_ago].ffill()

data_tenor = [d for d in data.columns if 'EUR_0Y_10Y' in d]  + \
             [d for d in data.columns if 'USD_0Y_10Y' in d]  + \
             [d for d in data.columns if 'AUD_0Y_10Y' in d]  + \
             [d for d in data.columns if 'JPY_0Y_10Y' in d]  + \
             [d for d in data.columns if 'CAD_0Y_10Y' in d]  + \
             [d for d in data.columns if 'GBP_0Y_10Y' in d]
             
             

data_sub_narrow = data_sub[data_tenor]

#data_sub.columns = [c.replace('_', '') for c in data_sub.columns]

data_sub_narrow_pca = pca.PCA(data_sub_narrow,rates_pca = True)

global_level = data_sub_narrow_pca.pcs['PC1']

def signed_rsquared(xs,ys):
    r = stats.linregress(xs, ys).rvalue
    return r * abs(r)

spot_tenor = [d for d in data.columns if '_0Y_' in d]
rsquared = { col:signed_rsquared(global_level,data_sub[col]) for col in spot_tenor}
for k in sorted(rsquared, key=rsquared.get, reverse=True):
    print '%s, %.2f' % (k, rsquared[k])


belly_tenor = [d for d in data.columns if '_2Y_5Y' in d]
rsquared = { col:signed_rsquared(global_level,data_sub[col]) for col in belly_tenor}
for k in sorted(rsquared, key=rsquared.get, reverse=True):
    print '%s, %.2f' % (k, rsquared[k])

