from __future__ import division
import os, sys, inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import *
from itertools import permutations, combinations

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)

from statistics.factor import pca


data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-03-13_FWD_G10_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date')
dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
def rename_loading_idx(loadings): return pd.Series(loadings.values, [i[4:] for i in loadings.index]) 

##########################################################################
#                          spot pca
##########################################################################

spot_tenor = '_0Y_'
countries = set(data.columns.map(lambda x: x[:3]))
country_cols = map(lambda country: [col for col in data.columns if country in col and spot_tenor in col], countries)
spot_data = map(lambda cols: (cols[0][:3], data[cols]), country_cols)
spot_return_data = map(lambda cols: (cols[0][:3], data[cols] - data[cols].shift(1)), country_cols)




##########################################################################
#2 year spot pca
##########################################################################

spot_data_2yrs = {country: country_data[country_data.index >= dt_2yrs_ago].ffill() for (country, country_data) in spot_data}  
# skip the 3m, 1y and 30y
spot_data_2yrs_selected_tenors = {country: country_data.iloc[:,2:-1] for (country, country_data) in spot_data_2yrs.iteritems()}
spot_data_2yrs_selected_tenors_pcas = {country: pca.PCA(country_data, rates_pca=True) for (country, country_data) in spot_data_2yrs_selected_tenors.iteritems()}


##########################################################################
#2 year spot return pca
##########################################################################

spot_return_data_2yrs = {country: country_data[country_data.index >= dt_2yrs_ago].ffill() for (country, country_data) in spot_return_data}  
# skip the 3m, 1y and 30y
spot_return_data_2yrs_selected_tenors = {country: country_data.iloc[:,2:-1] for (country, country_data) in spot_return_data_2yrs.iteritems()}
spot_return_data_2yrs_selected_tenors_pcas = {country: pca.PCA(country_data, rates_pca=True) for (country, country_data) in spot_return_data_2yrs_selected_tenors.iteritems()}




##########################################################################
#10 year spot pca
##########################################################################

spot_data_10yrs = {country: country_data[country_data.index >= dt_10yrs_ago].ffill() for (country, country_data) in spot_data}  
# skip the 3m, 1y and 30y
spot_data_10yrs_selected_tenors = {country: country_data.iloc[:,2:-1] for (country, country_data) in spot_data_10yrs.iteritems()}
spot_data_10yrs_selected_tenors_pcas = {country: pca.PCA(country_data, rates_pca=True) for (country, country_data) in spot_data_10yrs_selected_tenors.iteritems()}


##########################################################################
#spot pca pcs
##########################################################################

spot_data_2yrs_selected_tenors_pc1 = pd.DataFrame({ country: country_pca.pcs['PC1'] for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})
spot_data_2yrs_selected_tenors_pc2 = pd.DataFrame({ country: country_pca.pcs['PC2'] for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})
spot_data_2yrs_selected_tenors_pc3 = pd.DataFrame({ country: country_pca.pcs['PC3'] for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})

spot_return_data_2yrs_selected_tenors_pc1 = pd.DataFrame({ country: country_pca.pcs['PC1'] for (country, country_pca) in spot_return_data_2yrs_selected_tenors_pcas.iteritems()})
spot_return_data_2yrs_selected_tenors_pc2 = pd.DataFrame({ country: country_pca.pcs['PC2'] for (country, country_pca) in spot_return_data_2yrs_selected_tenors_pcas.iteritems()})
spot_return_data_2yrs_selected_tenors_pc3 = pd.DataFrame({ country: country_pca.pcs['PC3'] for (country, country_pca) in spot_return_data_2yrs_selected_tenors_pcas.iteritems()})


spot_data_10yrs_selected_tenors_pc1 = pd.DataFrame({ country: country_pca.pcs['PC1'] for (country, country_pca) in spot_data_10yrs_selected_tenors_pcas.iteritems()})
spot_data_10yrs_selected_tenors_pc2 = pd.DataFrame({ country: country_pca.pcs['PC2'] for (country, country_pca) in spot_data_10yrs_selected_tenors_pcas.iteritems()})
spot_data_10yrs_selected_tenors_pc3 = pd.DataFrame({ country: country_pca.pcs['PC3'] for (country, country_pca) in spot_data_10yrs_selected_tenors_pcas.iteritems()})


spot_data_2yrs_selected_tenors_pc1_corr = spot_data_2yrs_selected_tenors_pc1.corr()
spot_data_2yrs_selected_tenors_pc2_corr = spot_data_2yrs_selected_tenors_pc2.corr()
spot_data_2yrs_selected_tenors_pc3_corr = spot_data_2yrs_selected_tenors_pc3.corr()

spot_return_data_2yrs_selected_tenors_pc1_corr = spot_return_data_2yrs_selected_tenors_pc1.corr()
spot_return_data_2yrs_selected_tenors_pc2_corr = spot_return_data_2yrs_selected_tenors_pc2.corr()
spot_return_data_2yrs_selected_tenors_pc3_corr = spot_return_data_2yrs_selected_tenors_pc3.corr()

spot_data_10yrs_selected_tenors_pc1_corr = spot_data_10yrs_selected_tenors_pc1.corr()
spot_data_10yrs_selected_tenors_pc2_corr = spot_data_10yrs_selected_tenors_pc2.corr()
spot_data_10yrs_selected_tenors_pc3_corr = spot_data_10yrs_selected_tenors_pc2.corr()

##########################################################################
#spot pca loadings
##########################################################################

spot_data_2yrs_selected_tenors_loading1 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading1']) for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})
spot_data_2yrs_selected_tenors_loading2 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading2']) for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})
spot_data_2yrs_selected_tenors_loading3 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading3']) for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})

spot_return_data_2yrs_selected_tenors_loading1 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading1']) for (country, country_pca) in spot_return_data_2yrs_selected_tenors_pcas.iteritems()})
spot_return_data_2yrs_selected_tenors_loading2 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading2']) for (country, country_pca) in spot_return_data_2yrs_selected_tenors_pcas.iteritems()})
spot_return_data_2yrs_selected_tenors_loading3 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading3']) for (country, country_pca) in spot_return_data_2yrs_selected_tenors_pcas.iteritems()})


spot_data_10yrs_selected_tenors_loading1 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading1']) for (country, country_pca) in spot_data_10yrs_selected_tenors_pcas.iteritems()})
spot_data_10yrs_selected_tenors_loading2 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading2']) for (country, country_pca) in spot_data_10yrs_selected_tenors_pcas.iteritems()})
spot_data_10yrs_selected_tenors_loading3 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading3']) for (country, country_pca) in spot_data_10yrs_selected_tenors_pcas.iteritems()})

##########################################################################
#spot pca explainatory power
##########################################################################


for c, cp in spot_data_2yrs_selected_tenors_pcas.iteritems():
    print c, cp.eigvals


spot_data_2yrs_selected_tenors_variance = pd.DataFrame({ country: country_pca.eigvals for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})
spot_data_2yrs_selected_tenors_lambda = pd.DataFrame({ country: country_pca.loadings_lambda for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})


spot_return_data_2yrs_selected_tenors_variance = pd.DataFrame({ country: country_pca.eigvals for (country, country_pca) in spot_return_data_2yrs_selected_tenors_pcas.iteritems()})
spot_return_data_2yrs_selected_tenors_lambda = pd.DataFrame({ country: country_pca.loadings_lambda for (country, country_pca) in spot_return_data_2yrs_selected_tenors_pcas.iteritems()})


spot_data_10yrs_selected_tenors_variance = pd.DataFrame({ country: country_pca.eigvals for (country, country_pca) in spot_data_10yrs_selected_tenors_pcas.iteritems()})
spot_data_10yrs_selected_tenors_lambda = pd.DataFrame({ country: country_pca.loadings_lambda for (country, country_pca) in spot_data_10yrs_selected_tenors_pcas.iteritems()})

##########################################################################
# spot pca factor residuals
##########################################################################

spot_data_2yrs_selected_tenors_3factor_residual = pd.DataFrame({country: rename_loading_idx(country_pca.data_gaps_from_reconstruction(n=[1,2,3]).iloc[-1,:]) for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})
spot_data_2yrs_selected_tenors_2factor_residual = pd.DataFrame({country: rename_loading_idx(country_pca.data_gaps_from_reconstruction(n=[1,2]).iloc[-1,:]) for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})
spot_data_2yrs_selected_tenors_1factor_residual = pd.DataFrame({country: rename_loading_idx(country_pca.data_gaps_from_reconstruction(n=[1]).iloc[-1,:]) for (country, country_pca) in spot_data_2yrs_selected_tenors_pcas.iteritems()})

# no point to do this on return!


##########################################################################
#                                   fwd
##########################################################################

fwd_tenor = '_1Y'
countries = set(data.columns.map(lambda x: x[:3]))
country_cols = map(lambda country: [col for col in data.columns if country in col and col.endswith(fwd_tenor)], countries)
fwd_data = map(lambda cols: (cols[0][:3], data[cols]), country_cols)
fwd_return_data = map(lambda cols: (cols[0][:3], data[cols] - data[cols].shift(1)), country_cols)



##########################################################################
#2 year fwd pca
##########################################################################
fwd_data_2yrs = {country: country_data[country_data.index >= dt_2yrs_ago].ffill() for (country, country_data) in fwd_data}  
# skip the 3m, 1y and 30y
fwd_data_2yrs_selected_tenors = {country: country_data.iloc[:,2:-2] for (country, country_data) in fwd_data_2yrs.iteritems()}
fwd_data_2yrs_selected_tenors_pcas = {country: pca.PCA(country_data, rates_pca=True) for (country, country_data) in fwd_data_2yrs_selected_tenors.iteritems()}

##########################################################################
#10 year fwd pca
##########################################################################
fwd_data_10yrs = {country: country_data[country_data.index >= dt_10yrs_ago].ffill() for (country, country_data) in fwd_data}  
# skip the 3m, 1y and 30y
fwd_data_10yrs_selected_tenors = {country: country_data.iloc[:,2:-2] for (country, country_data) in fwd_data_10yrs.iteritems()}
fwd_data_10yrs_selected_tenors_pcas = {country: pca.PCA(country_data, rates_pca=True) for (country, country_data) in fwd_data_10yrs_selected_tenors.iteritems()}


##########################################################################
#fwd pca pcs
##########################################################################

fwd_data_2yrs_selected_tenors_pc1 = pd.DataFrame({ country: country_pca.pcs['PC1'] for (country, country_pca) in fwd_data_2yrs_selected_tenors_pcas.iteritems()})
fwd_data_2yrs_selected_tenors_pc2 = pd.DataFrame({ country: country_pca.pcs['PC2'] for (country, country_pca) in fwd_data_2yrs_selected_tenors_pcas.iteritems()})
fwd_data_2yrs_selected_tenors_pc3 = pd.DataFrame({ country: country_pca.pcs['PC3'] for (country, country_pca) in fwd_data_2yrs_selected_tenors_pcas.iteritems()})

fwd_data_10yrs_selected_tenors_pc1 = pd.DataFrame({ country: country_pca.pcs['PC1'] for (country, country_pca) in fwd_data_10yrs_selected_tenors_pcas.iteritems()})
fwd_data_10yrs_selected_tenors_pc2 = pd.DataFrame({ country: country_pca.pcs['PC2'] for (country, country_pca) in fwd_data_10yrs_selected_tenors_pcas.iteritems()})
fwd_data_10yrs_selected_tenors_pc3 = pd.DataFrame({ country: country_pca.pcs['PC3'] for (country, country_pca) in fwd_data_10yrs_selected_tenors_pcas.iteritems()})

fwd_data_2yrs_selected_tenors_pc1_corr = fwd_data_2yrs_selected_tenors_pc1.corr()
fwd_data_2yrs_selected_tenors_pc2_corr = fwd_data_2yrs_selected_tenors_pc2.corr()
fwd_data_2yrs_selected_tenors_pc3_corr = fwd_data_2yrs_selected_tenors_pc2.corr()

fwd_data_10yrs_selected_tenors_pc1_corr = fwd_data_10yrs_selected_tenors_pc1.corr()
fwd_data_10yrs_selected_tenors_pc2_corr = fwd_data_10yrs_selected_tenors_pc2.corr()
fwd_data_10yrs_selected_tenors_pc3_corr = fwd_data_10yrs_selected_tenors_pc2.corr()


##########################################################################
#fwd pca loadings
##########################################################################

fwd_data_2yrs_selected_tenors_loading1 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading1']) for (country, country_pca) in fwd_data_2yrs_selected_tenors_pcas.iteritems()})
fwd_data_2yrs_selected_tenors_loading2 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading2']) for (country, country_pca) in fwd_data_2yrs_selected_tenors_pcas.iteritems()})
fwd_data_2yrs_selected_tenors_loading3 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading3']) for (country, country_pca) in fwd_data_2yrs_selected_tenors_pcas.iteritems()})

fwd_data_10yrs_selected_tenors_loading1 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading1']) for (country, country_pca) in fwd_data_10yrs_selected_tenors_pcas.iteritems()})
fwd_data_10yrs_selected_tenors_loading2 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading2']) for (country, country_pca) in fwd_data_10yrs_selected_tenors_pcas.iteritems()})
fwd_data_10yrs_selected_tenors_loading3 = pd.DataFrame({ country: rename_loading_idx(country_pca.loadings['Loading3']) for (country, country_pca) in fwd_data_10yrs_selected_tenors_pcas.iteritems()})



##########################################################################
#fwd pca explainatory power
##########################################################################

fwd_data_2yrs_selected_tenors_variance = pd.DataFrame({ country: country_pca.eigvals for (country, country_pca) in fwd_data_2yrs_selected_tenors_pcas.iteritems()})
fwd_data_10yrs_selected_tenors_variance = pd.DataFrame({ country: country_pca.eigvals for (country, country_pca) in fwd_data_10yrs_selected_tenors_pcas.iteritems()})

##########################################################################
#fwd pca factor residuals
##########################################################################

fwd_data_2yrs_selected_tenors_3factor_residual = pd.DataFrame({country: rename_loading_idx(country_pca.data_gaps_from_reconstruction(n=[1,2,3]).iloc[-1,:]) for (country, country_pca) in fwd_data_2yrs_selected_tenors_pcas.iteritems()})
fwd_data_2yrs_selected_tenors_2factor_residual = pd.DataFrame({country: rename_loading_idx(country_pca.data_gaps_from_reconstruction(n=[1,2]).iloc[-1,:]) for (country, country_pca) in fwd_data_2yrs_selected_tenors_pcas.iteritems()})
fwd_data_2yrs_selected_tenors_1factor_residual = pd.DataFrame({country: rename_loading_idx(country_pca.data_gaps_from_reconstruction(n=[1]).iloc[-1,:]) for (country, country_pca) in fwd_data_2yrs_selected_tenors_pcas.iteritems()})



#


fwd_data_2yrs_selected_tenors_pcas['USD'].loadings.iloc[:,:4].plot(kind='bar')
#fwd_data_2yrs_selected_tenors_pcas['USD'].pcs.iloc[:,:4].plot(secondary_y=['PC3','PC4'])
fwd_data_2yrs_selected_tenors_pcas['USD'].pcs.iloc[:,:1].plot()
fwd_data_2yrs_selected_tenors_pcas['USD'].pcs.iloc[:,1:3].plot(secondary_y=['PC3'])

sns.heatmap(spot_data_2yrs_selected_tenors_lambda.T, annot=True,linewidth=0.5,  cmap="YlGnBu")


spot_data_2yrs_selected_tenors_2factor_residual.plot()

spot_data_2yrs_selected_tenors_loading2.plot()

spot_return_data_2yrs_selected_tenors_loading1.plot()

spot_data_2yrs_selected_tenors_2factor_residual[['EUR','CAD','AUD','USD']].plot()

spot_data_2yrs_selected_tenors_1factor_residual[['EUR','CAD','AUD','USD']].plot()


spot_return_data_2yrs_selected_tenors_loading2[['EUR','CAD','AUD','USD','GBP']].plot(kind='bar', ylim = (-0.8,0.8))
spot_data_2yrs_selected_tenors_loading2[['EUR','CAD','AUD','USD','GBP']].plot(kind='bar', ylim = (-0.8,0.8))

spot_return_data_2yrs_selected_tenors_pc3[['EUR','CAD','AUD','USD','GBP']].tail(20).plot(kind ='bar')
spot_data_2yrs_selected_tenors_pc3[['EUR','CAD','AUD','USD','GBP']].plot()


g = sns.lmplot(x = 'EUR', y = 'CAD', data = spot_data_2yrs_selected_tenors_pc3, fit_reg=True)

print spot_data_2yrs_selected_tenors_pc3

spot_return_data_2yrs_selected_tenors_loading3.plot()
# -*- coding: utf-8 -*-


blah = {country: country_data.iloc[:,2:] for (country, country_data) in spot_data_2yrs.iteritems()}
us2s5s10s30s = blah['USD'][['USD_0Y_2Y', 'USD_0Y_5Y', 'USD_0Y_10Y','USD_0Y_30Y']]
us2s5s10s30s_pca = pca.PCA(us2s5s10s30s, rates_pca=True)

us2s5s10s30s_pca.loadings.plot()
us2s5s10s30s_pca.eigvals
us2s5s10s30s_pca.pcs.plot(secondary_y=['PC3','PC4'])

us2s5s10s30s_pca.data_gaps_from_reconstruction(n=[1,2,3]).tail(1).plot(kind='bar')
us2s5s10s30s_pca.data_gaps_from_reconstruction(n=[1,2]).tail(1).plot(kind='bar')
us2s5s10s30s_pca.data_gaps_from_reconstruction(n=[1]).tail(1).plot(kind='bar')



data_2col = spot_data_2yrs['AUD'].iloc[:,2:4]
data_2col_pca = pca.PCA(data_2col,rates_pca = True)
w2 = -1 * data_2col_pca.loadings['Loading1'][0]/data_2col_pca.loadings['Loading1'][1]
hedged_combination = data_2col_pca.centered_data.iloc[:,0] + w2 * data_2col_pca.centered_data.iloc[:,1]  
hedged_combination.plot()
np.corrcoef(hedged_combination, data_2col_pca.pcs['PC1'])


data_sub = data[data.index>'2016-01-01'][['USD_3Y_2Y', 'USD_5Y_2Y']]
data_sub_pca = pca.PCA(data_sub,rates_pca = True)
data_sub_pca.plot_hedge_pcs().show()


for (a, b) in combinations([d for d in data.columns if 'USD' in d][2:], 2): 
    data_sub = data[data.index>'2016-01-01'][[a, b]]
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    plt.savefig('C:/Temp/test\market_watch/output/%s_%s.png' % (a, b), bbox_inches='tight')
    plt.close()


for (a, b) in combinations([d for d in data.columns if 'CAD_0Y_1Y' in d or 'CAD_2Y_3Y' in d  ], 2): 
    data_sub = data[data.index>'2016-10-01'][[a, b]].ffill()
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    plt.savefig('C:/Temp/test\market_watch/output/%s_%s.png' % (a, b), bbox_inches='tight')
    plt.close()


dt = '2014-01-01'
for (a, b) in combinations([d for d in data.columns if 'NOK_5Y_5Y' in d or 'CAD_5Y_5Y' in d  ], 2): 
    data_sub = data[data.index>dt][[a, b]].ffill()
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    f.show()
    #plt.savefig('C:/Temp/test\market_watch/output/%s_%s_%s.png' % (a, b, dt), bbox_inches='tight')
    #plt.close()


dt = '2015-10-01'
for (a, b, c) in combinations([d for d in data.columns if 'USD_0Y_5Y' in d or 'USD_0Y_10Y' in d or 'USD_0Y_30Y' in d  ], 3): 
    data_sub = data[data.index> dt][[a, b, c]].ffill()
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    plt.savefig('C:/Temp/test\market_watch/output/%s_%s_%s_%s.png' % (a, b, c, dt), bbox_inches='tight')
    plt.close()
#




### Watching ####
    
dt = '2014-01-01'
for (a, b) in combinations([d for d in data.columns if 'NOK_5Y_5Y' in d or 'CAD_5Y_5Y' in d  ], 2): 
    data_sub = data[data.index>dt][[a, b]].ffill()
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    f.show()
 
dt = '2016-03-01'
for (a, b) in combinations([d for d in data.columns if 'AUD_5Y_5Y' in d or 'USD_5Y_5Y' in d  ], 2): 
    data_sub = data[data.index>dt][[a, b]].ffill()
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    f.show()

dt = '2016-03-01'
for (a, b) in combinations([d for d in data.columns if 'SEK_5Y_5Y' in d or 'GBP_5Y_5Y' in d  ], 2): 
    data_sub = data[data.index>dt][[a, b]].ffill()
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    f.show()

dt = '2016-03-01'
for (a, b) in combinations([d for d in data.columns if 'EUR_5Y_5Y' in d or 'JPY_5Y_5Y' in d  ], 2): 
    data_sub = data[data.index>dt][[a, b]].ffill()
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    f.show()


dt = '2016-03-01'
for (a, b) in combinations([d for d in data.columns if 'EUR_3Y_2Y' in d or 'AUD_3Y_2Y' in d  ], 2): 
    data_sub = data[data.index>dt][[a, b]].ffill()
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    f.show()

     
    
dt = '2014-01-01'
for (a, b) in combinations([d for d in data.columns if 'AUD_3Y_1Y' in d or 'CAD_3Y_1Y' in d  ], 2): 
    data_sub = data[data.index>dt][[a, b]].ffill()
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    f.show()


dt = '2014-01-01'
for (a, b) in combinations([d for d in data.columns if 'CAD_0Y_30Y' in d or 'USD_0Y_30Y' in d  ], 2): 
    data_sub = data[data.index>dt][[a, b]].ffill()
    data_sub_pca = pca.PCA(data_sub,rates_pca = True)
    f = data_sub_pca.plot_hedge_pcs()
    f.show()

  


