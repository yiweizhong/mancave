from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset, BDay
from itertools import permutations, combinations, chain, tee
from collections import namedtuple
from dask.distributed import Client
from concurrent.futures import ProcessPoolExecutor

_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


###############################################
#type
###############################################

#OverviewOutput = namedtuple('OverviewOutput', ['tag', 'spread_type', 'weighted_data', 'weights', 'mr', 'pca', 'hedge_description_sec'])



###############################################
#func
###############################################



###############################################
#main
###############################################

if __name__ == '__main__':
    #
    print(datetime.datetime.now())    
    print('test start')


    print('test start')