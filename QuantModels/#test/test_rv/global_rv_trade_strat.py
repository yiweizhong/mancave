from __future__ import division
import datetime
import os, sys, inspect
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns
from scipy import stats
from pandas.tseries.offsets import DateOffset, BDay
from itertools import permutations, combinations, chain, tee
from collections import namedtuple
from dask.distributed import Client
_p = 'C:\Dev\Models\QuantModels'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics'
if _p not in sys.path: sys.path.insert(0, _p)
_p = 'C:\Dev\Models\QuantModels\statistics\mr'
if _p not in sys.path: sys.path.insert(0, _p)
from statistics.factor import pca
from statistics.mr import bins
from statistics.mr import stochastic as st
from statistics.mr import helper


###############################################
#type
###############################################

RvOutput = namedtuple('RvOutput', ['tag', 'spread_type', 'weighted_data', 'weights', 'mr', 'pca', 'hedge_description_sec'])

RvConstraints = namedtuple('RvConstraints', ['pair_weighting_up_lim', 
                                             'pair_weighting_low_lim', 
                                             'reversion_potential_low_lim', 
                                             'reversion_speed_low_lim', 
                                             'reversion_vol_up_lim_pct_potential'])

CountryConstraints = namedtuple('CountryConstraints', ['leverage', 'total_fwd_tenor'])


###############################################
# Func
###############################################


def partition(items, predicate=bool):
    a, b = tee((predicate(item), item) for item in items)
    return ((item for pred, item in a if pred),
            (item for pred, item in b if not pred))

def smooth_series(ds, lim=2, half_window=1):

    def smooth(row):

        if np.isnan(row['ds_prev']) or np.isnan(row['ds_next']):
            return row['ds']

        if abs(row['ds_prev'] - row['ds']) > lim and abs(row['ds_next'] - row['ds']) > lim and abs((row['ds_prev'] + row['ds_next'])/2 - row['ds']) > lim:
            return (row['ds_prev'] + row['ds_next'])/2
        else:
            return row['ds']

    data = pd.DataFrame({'ds':ds, 'ds_prev': ds.shift(half_window), 'ds_next': ds.shift(-1*half_window)})

    return data.apply(smooth, axis=1)
 
    

def smooth_fwd_rates(data, limit, half_window): 
    
    out = {}
    
    for col, ds in data.iteritems():
        if int(col.split('_')[1].replace('Y','')) != 0:
            out[col] = smooth_series(ds, lim=limit, half_window=half_window)
        else:
            out[col] = ds
    
    return pd.DataFrame(out)
    


def get_data(data, data_date):
    return data[data.index == data_date]


def calc_fwd_spread(df, denorm=True):
    lcol = df.columns[0]
    rcol = df.columns[1]
    
    denorminator = int(rcol.split('_')[2].replace('Y','')) - int(lcol.split('_')[2].replace('Y',''))
    
    
    #denorminator = denorminator if denorm else  denorminator/abs(denorminator)
    
    #print denorminator
    
    if denorminator >= 0:
        #return lcol, rcol, round((df[rcol][0] - df[lcol][0]) * 100,2)
        spread = ((df[rcol][:] - df[lcol][:]) * 100).round(2)
        spread.name = '{}_{}'.format(lcol, rcol)
        return lcol, rcol, spread
    else:
        #return rcol, lcol, round((df[lcol][0] - df[rcol][0]) * 100,2)
        spread = ((df[lcol][:] - df[rcol][:]) * 100).round(2)
        spread.name = '{}_{}'.format(rcol, lcol)
        return rcol, lcol, spread
    

def rearrange_cols(df, denorm=True):
    lcol = df.columns[0]
    rcol = df.columns[1]
    
    denorminator = int(rcol.split('_')[2].replace('Y','')) - int(lcol.split('_')[2].replace('Y',''))
    
    if denorminator >= 0:
        return rcol, lcol, (df[[rcol, lcol]]) 
    else:
        return lcol, rcol, (df[[lcol, rcol]])

    
def get_carryroll(data, sec):
    #print sec
    return data[data['sec'] == sec]['carry'].values[0] + data[data['sec'] == sec]['roll'].values[0] 


def chart_spread_carry(data):
    sns.lmplot(data=data, x='carry', y='spread', hue='country', fit_reg=False, scatter=True, legend=True, legend_out=True, size=20, aspect=1)
    ax = plt.gca()
    for sec, row in data.iterrows():
        ax.text(row['carry'], row['spread'], sec, horizontalalignment='left', size='x-small', color='black', weight='ultralight')
    

def check_leverage(ticker, leverage_lim = 5):
    _, fwd, tenor = ticker.split('_')
    fwd = float(fwd.replace('Y',''))
    tenor = float(tenor.replace('Y',''))

    if fwd * 2 + tenor < 6.0:
        return True
    
    if (fwd*2.0+tenor)/tenor > leverage_lim:
        return False
    else:
        return True
    
    
def check_total_fwd_tenor_length(ticker, length_lim = 12):
    _, fwd, tenor = ticker.split('_')
    fwd = float(fwd.replace('Y',''))
    tenor = float(tenor.replace('Y',''))

    if fwd + tenor <= length_lim:
        return True
    else:
        return False
    
    
    
def check_overlapping(ticker1, ticker2, overlaping_lim = 0.5): 
    _, fwd1, tenor1 = ticker1.split('_')
    fwd1 = float(fwd1.replace('Y',''))
    tenor1 = float(tenor1.replace('Y',''))
    
    _, fwd2, tenor2 = ticker2.split('_')
    fwd2 = float(fwd2.replace('Y',''))
    tenor2 = float(tenor2.replace('Y',''))
    
    if tenor2 >= tenor1:
        if tenor2 + fwd2 - fwd1 == 0 or tenor1 / (tenor2 + fwd2 - fwd1) > overlaping_lim:
            return False
        else:
            return True
    else:
        if tenor1 + fwd1 - fwd2 == 0 or tenor2 / (tenor1 + fwd1 - fwd2) > overlaping_lim:
            return False
        else:
            return True
        

def load_sec_name_translations(file):
    with open(file, 'r') as fh:
        for line in [l for l in (line.strip() for line in fh) if l if not l.startswith('#')]:
            val = line.split(',')[0].strip()
            key = line.split(',')[1].strip()
            yield key, val
        

###############################################
# data
###############################################

data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-08-01_SWAP_ALL_historical.csv')  
data['date'] = pd.to_datetime(data['date'])
data = data.set_index('date').ffill()
carry_data = pd.read_csv('C:/Temp/test/market_watch/staging/2018-08-01_CarryRoll.csv')
sec_name_dict = {key:val for (key, val) in load_sec_name_translations('C:/Temp/test/market_watch/input/SWAP_ALL_historical.csv')}
    


dt_10yrs_ago = data.index[-1] - DateOffset(years = 10, months=0, days=0)
dt_5yrs_ago = data.index[-1] - DateOffset(years = 5, months=0, days=0)
dt_3yrs_ago = data.index[-1] - DateOffset(years = 3, months=0, days=0)
dt_2yrs_ago = data.index[-1] - DateOffset(years = 2, months=0, days=0)
dt_1yrs_ago = data.index[-1] - DateOffset(years = 1, months=0, days=0)
dt_6mths_ago =  data.index[-1] - DateOffset(years = 0, months=6, days=0)
dt_18mths_ago = data.index[-1] - DateOffset(years = 0, months=18, days=0)

rv_constraints_weekly_dp = RvConstraints(
        pair_weighting_up_lim = 4,
        pair_weighting_low_lim = 0.4,
        reversion_potential_low_lim = 20,
        reversion_speed_low_lim = 0.4,
        reversion_vol_up_lim_pct_potential = 0.7
        ) 


rv_constraints_daily_dp = RvConstraints(
        pair_weighting_up_lim = float('inf'),
        pair_weighting_low_lim = float('inf'),
        reversion_potential_low_lim = float('inf'),
        reversion_speed_low_lim = float('inf'),
        reversion_vol_up_lim_pct_potential = float('inf')
        ) 
        
#data_sample = get_data(data, data.index.max())

fwds = [ '_{}Y'.format(i) for i in [
        #0, 
        1, 
        2, 
        3, 
        4, 
        5,
        ]]

tenors = [ '_{}Y'.format(i) for i in [
        1, 
        2, 
        3, 
        4, 
        5,
        10,
        ]]


country_constraints = {
 'USD':CountryConstraints(leverage=4, total_fwd_tenor=15),
 'AUD':CountryConstraints(leverage=4, total_fwd_tenor=12),
 #'NOK':CountryConstraints(leverage=5, total_fwd_tenor=np.maximum),
 'KRW':CountryConstraints(leverage=4, total_fwd_tenor=10),
 'SGD':CountryConstraints(leverage=4, total_fwd_tenor=10),
 'JPY':CountryConstraints(leverage=5, total_fwd_tenor=10),
 'GBP':CountryConstraints(leverage=4, total_fwd_tenor=15),
 'NZD':CountryConstraints(leverage=4, total_fwd_tenor=12),
 'CAD':CountryConstraints(leverage=4, total_fwd_tenor=12),
 'SEK':CountryConstraints(leverage=4, total_fwd_tenor=12),
 'THB':CountryConstraints(leverage=4, total_fwd_tenor=10),
 'HKD':CountryConstraints(leverage=4, total_fwd_tenor=10),
 'EUR':CountryConstraints(leverage=4, total_fwd_tenor=15),
 }


spread_types = [
        'country_spread', 
        'cross_country_spread',
        ]


#####################################################################################################
# main
#####################################################################################################

fwd_tenors = [ '{}{}'.format(fwd, tenor) for tenor in tenors for fwd in fwds]
#fwd_tenors = [fwd_tenor for fwd_tenor in fwd_tenors if check_leverage(fwd_tenor, leverage_lim = 5)]
fwd_tenors = [ col for col in data.columns if col[3:] in fwd_tenors if col[:3] in country_constraints.keys()]
fwd_tenors = [fwd_tenor for fwd_tenor in fwd_tenors if check_leverage(fwd_tenor, leverage_lim = country_constraints[fwd_tenor[:3]].leverage)]
fwd_tenors = [fwd_tenor for fwd_tenor in fwd_tenors if check_total_fwd_tenor_length(fwd_tenor, length_lim = country_constraints[fwd_tenor[:3]].total_fwd_tenor)]
rates_xy_xy = data[fwd_tenors]
rates_xy_xy = smooth_fwd_rates(rates_xy_xy, 0.2, 1) # ensure forward rates are free from occassional abnormaly 
countries_xy_xy = list(set([ col[:3] for col in rates_xy_xy.columns]))
rates_pair_group_by_country = [list(combinations([col for col in rates_xy_xy.columns if col.startswith(c)],2)) for c in countries_xy_xy]
country_rates_pairs = [pair for pair in chain.from_iterable(rates_pair_group_by_country) if check_overlapping(*pair)] 
country_rates_pair_spread_calcs = [calc_fwd_spread(rates_xy_xy[list(pair)], denorm=False) for pair in country_rates_pairs]
country_rates_pair_spread_history = pd.DataFrame({spread.name: spread for (key1, key2, spread) in country_rates_pair_spread_calcs})
country_rates_pair_spread_sec_names = {spread.name: '({} - {})'.format(sec_name_dict[key2], sec_name_dict[key1]) for (key1, key2, spread) in country_rates_pair_spread_calcs}
country_rates_pair_countries = {spread.name: key1[0:3]  for (key1, key2, spread) in country_rates_pair_spread_calcs}
country_rates_pair_carries = {spread.name: get_carryroll(carry_data, key1) - get_carryroll(carry_data, key2) for (key1, key2, spread) in country_rates_pair_spread_calcs}
country_rate_pair_spread_current_snapshots = pd.DataFrame({'country':country_rates_pair_countries,
                                                           'spread': country_rates_pair_spread_history.iloc[-1], 
                                                           'carry':pd.Series(country_rates_pair_carries)})[['country','spread','carry']]

country_rate_pair_spread_current_snapshots.to_csv('C:/Temp/test/market_watch/output/2018-05-31_spreadscarry.csv', index_label = 'sec')
cross_country_box_rates, country_box_rates = partition(combinations(country_rates_pair_spread_history.columns,2), predicate=lambda pair: pair[0][:3] != pair[1][:3])
rates_combos = [box for box in chain((cross_country_box_rates if 'cross_country_spread' in spread_types else []), (country_box_rates if 'country_spread' in spread_types else []))] + (country_rates_pairs if 'country_spread' in spread_types else [])     
rates_combos_history = pd.concat([country_rates_pair_spread_history, rates_xy_xy], axis = 1)
print('total combo for rates_combos: {}'.format(len(rates_combos)))




spread_output = {}

rv_constraints = rv_constraints_weekly_dp
time_frame =  dt_2yrs_ago #dt_6mths_ago
resample_freq = 'W' #'B'


i = 0
for pair in rates_combos[:]:
        #data_sub_narrow_pair = country_rates_pair_spread_history[list(pair)]
        data_sub_narrow_pair = rates_combos_history[list(pair)]
        key1, key2 = pair
        
        spread_type = 'country_spread' if key1[:3] == key2[:3] else 'cross_country_spread'
        i = i + 1
        if i % 5000 == 0:
            print('{} -> {}'.format(i, len(spread_output)))
            
        if spread_type in spread_types:    
            
            data_sub_narrow_pair = data_sub_narrow_pair[data_sub_narrow_pair.index> time_frame].ffill()
            data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair.resample(resample_freq).last(), rates_pca = True)
            
            hedge_weighted_data, weights, _, hedge_description = data_sub_narrow_pair_pca.pc_hedge
             
            if(weights.abs().max() <= rv_constraints.pair_weighting_up_lim \
               and weights.abs().min() >= rv_constraints.pair_weighting_low_lim \
               and np.sign(weights.min()) < 0):
                
                mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
                
                if(mr_obj.ou.conditional_reversion_potential >= rv_constraints.reversion_potential_low_lim \
                   and mr_obj.ou.theta_hat >= rv_constraints.reversion_speed_low_lim \
                   and mr_obj.ou.sigma_hat <= rv_constraints.reversion_vol_up_lim_pct_potential * mr_obj.ou.conditional_reversion_potential):
                   
                    hedge_description_sec = ' '.join(['%+.3f x %s' % (weight, sec) for (sec, weight) in  weights.rename(lambda weight: country_rates_pair_spread_sec_names[weight]).iteritems()])
                    #print(hedge_description_sec)
                    #print (hedge_description)
                    #print ("================================")
                    spread_output[hedge_description] =  RvOutput(
                            tag = pair,
                            spread_type = spread_type,
                            weighted_data = hedge_weighted_data,
                            weights = weights,
                            mr = mr_obj,
                            pca = data_sub_narrow_pair_pca,
                            hedge_description_sec = hedge_description_sec)
                    
#sorted_mrs = [(key, value) for key, value in sorted(cross_spread_mr.iteritems(), key=lambda (k,v): v.ou.theta_hat,reverse=True)]
sorted_outputs = [(key, value) for key, value in sorted(spread_output.items(), key=lambda k_v: k_v[1].mr.ou.theta_hat * k_v[1].mr.ou.conditional_reversion_potential, reverse=True)]



##########################################
### output
##########################################

for hedge_description, sorted_output in sorted_outputs[:]:
    if sorted_output.spread_type in spread_types:
        #tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
        tstmp =datetime.datetime.today().strftime('%m-%d')
        fig2 = sorted_output.mr.create_mr_info_fig((18, 7), pca_obj = sorted_output.pca)
        plt.savefig('C:/Temp/test/market_watch/output/%s/%s_%s_%s_%s.png' % (sorted_output.spread_type, hedge_description, sorted_output.hedge_description_sec, 'W', tstmp), bbox_inches='tight')
        plt.close()
            

#################################################
### Second PCA using daily data for verification
#################################################
        
selected_paris = [('CAD_1Y_1Y_CAD_2Y_5Y', 'EUR_2Y_1Y_EUR_3Y_2Y')]
selected_spread_output = {}

for pair in selected_paris:
        data_sub_narrow_pair = spread_histories[list(pair)]
        key1, key2 = pair
        
        spread_type = 'country_spread' if key1[:3] == key2[:3] else 'cross_country_spread'
            
        if spread_type in spread_types:    
        
            data_sub_narrow_pair = data_sub_narrow_pair[data_sub_narrow_pair.index> time_frame].ffill()
            data_sub_narrow_pair_pca = pca.PCA(data_sub_narrow_pair, rates_pca = True)
            
            hedge_weighted_data, weights, _, hedge_description = data_sub_narrow_pair_pca.pc_hedge
            
            mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, 20, min_num_of_bins = 20, resample_freq = resample_freq)
            
            hedge_description_sec = ' '.join(['%+.3f x %s' % (weight, sec) for (sec, weight) in  weights.rename(lambda weight: spread_sec_names[weight]).iteritems()])
            selected_spread_output[hedge_description] =  RvOutput(
                    tag = pair,
                    spread_type = spread_type,
                    weighted_data = hedge_weighted_data,
                    weights = weights,
                    mr = mr_obj,
                    pca = data_sub_narrow_pair_pca,
                    hedge_description_sec = hedge_description_sec)
            
##########################################
### output
##########################################


for hedge_description, sorted_output in selected_spread_output.items():
    if sorted_output.spread_type in spread_types:
        tstmp =datetime.datetime.today().strftime('%Y-%m-%d_%H%M%S')
        fig2 = sorted_output.mr.create_mr_info_fig((18, 7), pca_obj = sorted_output.pca)
        plt.savefig('C:/Temp/test/market_watch/output/%s/%s_%s_%s_%s.png' % (sorted_output.spread_type, hedge_description, sorted_output.hedge_description_sec, 'W', tstmp), bbox_inches='tight')
        plt.close()
        
        
     
        
        
        
        
        
        
        
        
        

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        