from __future__ import division
import pandas as pd
import numpy as np
import scipy as sp
import math
import numbers
from datetime import datetime
from datetime import timedelta
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from itertools import permutations, combinations


np.set_printoptions(suppress=True)


class PCA(object):
    def __init__(self, data, matrixtype="COV", name=None):
        """ A generic Principle Component Analysis object
            parameter:
            ----------
                data:dataframe: 
                    Input data
                matrixtype:str
                    Whether to use covariance or correlation matrix for the eigen decomposition.
                    The valid values are "COV" or "CORR"     
        """
        
        if len(data.columns) < 2:
            raise ValueError("Minimal 2 underline data series are required for PCA calc ")
        if matrixtype not in ("COV", "CORR"):
            raise ValueError("The matrixtype must be either COV or CORR")
        


        self._name = name
        self._rates_pca = rates_pca
        self._data = data
        self._matrixtype = matrixtype
        self._data_mean = self.data.mean()
        self._centered_data = self._data.apply(lambda col: col - col.mean(), axis=0)
        self._eigvals, self._eigvecs, self._eigvals_raw, self._eigvecs_roots = self.get_eigens()
        self._pcs = self.pcs
        (self._reconstructed_data, self._reconstruction_pcs) = self.reconstruct_data_by_n_pc(n=[1, 2, 3])
        

    def get_eigens(self):
        """ calculate the eigen decomposition
            returns:
            --------
                result: tuple
                    1st element is the eigenvals expressed in pct of total variance (1d numpy array)
                    2nd element is the eigenvecs (2d numpy array). The 2d array eigen vectors are column based. 
                    in order to access an eigen value's eigen vector using a single Index the eigen vector 2d array needs 
                    to be transposed first.
                    3rd element is the eigenvals in value (1d numpy array)
        """

        def get_roots(eigvecs):
            def find_cross(a):
                if a[0] * a[1] < 0:
                    return 1
                else:
                    return 0
            for eigvec in eigvecs:
                arr1 = eigvec[:-1]
                arr2 = eigvec[1:]
                roots = np.apply_along_axis(find_cross, 0, np.array([arr1, arr2]))
                yield roots.sum()

        matrix = self.cov if self._matrixtype == "COV" else self.corr
        eigvals, eigvecs = np.linalg.eig(matrix)
        eigvecs = eigvecs[:, eigvals.argsort()[::-1]]

        #changing signs for rates pca
        if(self._rates_pca):
            eigvecs = eigvecs.T
            #pc1 (level is a positive function of pc1 )
            eigvecs[0] = -1 * eigvecs[0] if len(np.extract(eigvecs[0] > 0, eigvecs[0]))/len(eigvecs[0]) < 0.5 else eigvecs[0] 
            
            #PC2 (curve steepening is a positive function of pc2)
            #left_half = eigvecs[1][:int((len(eigvecs[1]) + 1)/2.0)]
            betas = np.polyfit(np.arange(len(eigvecs[1])), eigvecs[1], deg = 1, full=False)
            eigvecs[1] = -1 * eigvecs[1] if betas[0] < 0 else eigvecs[1]
            
            #PC3 (belly is a positive function of PC3)
            if(len(eigvecs) >2):
                chop_size = int((len(eigvecs[2]) + 1)/4.0)
                belly = eigvecs[2][chop_size+1:len(eigvecs[2]) - chop_size + 1]
                eigvecs[2] = -1 * eigvecs[2] if len(np.extract(belly > 0, belly))/len(belly) < 0.5 else eigvecs[2]
            
            eigvecs = eigvecs.T

        eigvals = eigvals[eigvals.argsort()[::-1]]
        eigvals_pct = eigvals / eigvals.sum()
        eigvecs_roots = list(get_roots(eigvecs.T))
        return eigvals_pct, eigvecs, eigvals, eigvecs_roots

    def reconstruct_data_by_n_pc(self, n=[1, 2, 3], new_data=None):
        """After the original data are decomposed into principle components, they can be reconstructed
            via the principle components. One way to filter out noises are to ignore the eigen vectors
            whose eigen values' explainatory powers are low.

            parameters:
            ----------- 
                n an array of int: 
                    the eigen value index to be included in the value reconstruction
            Returns:
            -------
                result: dataframe 
                    principle components
                
        """
        assert isinstance(n, (list))
        assert not isinstance(n, str)

        selectedloadings = self.loadings.copy()
        pcs = self.pcs.copy()

        # pandas checks not only the dimension but also the column index name are matching!
        pcs.columns = [c.replace("PC", "Loading") for c in pcs.columns.tolist()]

        for col in selectedloadings.columns.tolist():
            if col not in ["Loading" + str(i) for i in n]:
                selectedloadings[col] = 0

        loadingsT = selectedloadings.transpose()

        self._reconstructed_data = pcs.dot(loadingsT)
        self._reconstruction_pcs = n

        return (self._reconstructed_data, self._reconstruction_pcs)



    @property
    def data(self):
        return self._data

    @property
    def centered_data(self):
        return self._centered_data
    
    @property
    def corr(self):
        if (self._momentum_size is None):
            return self._data.corr()
        else:
            return (self._data - self._data.shift(self._momentum_size)).corr()

    @property
    def cov(self):
        if (self._momentum_size is None):
            return self.centered_data.cov()
        else:
            return (self._data - self._data.shift(self._momentum_size)).cov()

    @property
    def pcs(self):
        """
        Returns:
            pcs: dataframe
                principle components 
        """
        pcdf = self.centered_data.dot(self._eigvecs)
        pcdf.columns = ["PC" + str(i + 1) for i in range(len(self._data.columns.tolist()))]
        return pcdf

    @property
    def loadings(self):
        loadings = {"Loading" + str(i + 1): pd.Series(self._eigvecs[:, i], index=self._data.columns.tolist())
                    for i in range(len(self._data.columns.tolist()))}
        colnames = ["Loading" + str(i + 1) for i in range(len(self._data.columns.tolist()))]
        return pd.DataFrame(loadings)[colnames]

    @property
    def eigvecs(self):
        return self._eigvecs.T

    @property
    def eigvecs_roots(self):
        return self._eigvecs_roots

    @property
    def loadings_lambda(self):
        lambda_name = ["Loading" + str(i + 1) for i in range(len(self._data.columns.tolist()))]
        return pd.Series(data = self.eigvals, name='lambda', index=lambda_name)

    @property
    def eigvals(self):
        """
        Returns:
            eigvals: 1d numpy array
                eigvals in percent 
        """
        #return np.vectorize(lambda x: '%.4f' % x)(self._eigvals)
        return self._eigvals

    @property
    def eigvals_raw(self):
        """
        Returns:
            eigvals: 1d numpy array
                eigvals in absolute value 
        """
        return self._eigvals_raw
    
    
    @property
    def data_mean(self):
        return self._data_mean

