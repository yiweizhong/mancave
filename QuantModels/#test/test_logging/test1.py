import logging

logger = logging.getLogger('testlogger')
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

fh = logging.FileHandler('c:/Temp/test.log')
fh.setLevel(logging.INFO)
fh.setFormatter(formatter)

logger.addHandler(fh)
logger.debug('This message should go to the log file')
logger.info('So should this')
logger.warning('And this, 2')



#logging.basicConfig(filename='c:/Temp/test.log',level=logging.INFO)
#logging.debug('This message should go to the log file')
#logging.info('So should this')
#logging.warning('And this, too')