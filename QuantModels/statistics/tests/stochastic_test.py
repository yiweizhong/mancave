"""
unitTest
"""
from __future__ import division
import os
import sys
import inspect
import numpy as np
import pandas as pd
import unittest
import xlwings as xw
import statistics.data.extension as extn
import relative_value.stochastic as st
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm


def bootstrap_ts(data = None, dates = None):
    """bootstrap ts"""


    _data = data if data is not None else [1, 20, 3.3, 3.35, 5.5, 6, 7.7, 8, 9, 10.1]
    _dates = dates if dates is not None else ['2017-02-01','2017-02-02','2017-02-03','2017-02-04','2017-02-05',
                          '2017-02-06','2017-02-07','2017-02-08','2017-02-09','2017-02-10']

    data = pd.Series(data=_data, index=_dates)
    data.index = pd.to_datetime(data.index)
    data.index.name = "DATE"
    return data

def load_bbg_historical_data(file_path, key_col = 'date'):
    data = extn.create_datetime_df(pd.read_csv(file_path),key = key_col,utc=False)
    return data
   
class OrnsteinUhlenbeckTest(unittest.TestCase):
    
    def setUp(self):
        """
        """
        self.date_idx = "DATE"
        plt.ion()

    def test_env_setup(self):
        """make sure testenv is ready"""
        self.assertEqual(1, 1)
    

    def test_deltaT_OrnsteinUhlenbeck_simulation(self):
        fig = plt.figure(figsize=(8,8))
        gs = gridspec.GridSpec(2, 1, height_ratios=[1,1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])
        ax1 = plt.subplot(gs[1])

        
        #mu_func = lambda x, t: 10
        #sigma_func = lambda x, t: 5


        x0 = 10
        theta = 5
        mu =5
        sigma = 20
        
        n = 200

        #OU_Xs = st.simulate_OU_SDE(x0,theta,mu,sigma,t_start, t_end, n)
        #GBM_Xs = st.simulate_GBM_SDE(x0, mu, sigma, t_start, t_end, n)

        OU_X = []
        OU_X2 = []

        axis_1 = np.arange(0,2,step=2/200)
        axis_2 = np.arange(0,2,step=2/20000)

        for i in np.arange(1):
            OU_X.append(pd.DataFrame(st.simulate_OU_SDE(x0,theta,mu,sigma, 2/200, 200)[0], index = axis_1))
            OU_X.append(pd.DataFrame(st.simulate_OU_SDE(x0,theta,mu,sigma, 2/20000, 20000)[0], index = axis_2))

            OU_X2.append(pd.DataFrame(st.simulate_OU_SDE_simple(x0,theta,mu,sigma, 2/200, 200), index = axis_1))
            OU_X2.append(pd.DataFrame(st.simulate_OU_SDE_simple(x0,theta,mu,sigma, 2/20000, 20000), index = axis_2))
            
        df = pd.concat(OU_X,axis=1).ffill()
        print df
        df.plot(ax=ax0, lw=0.5)

        df1 = pd.concat(OU_X2,axis=1).ffill()
        df1.plot(ax=ax1, lw=0.5)

        plt.tight_layout()
        plt.show(block=True)

    def test_OrnsteinUhlenbeck_simulation(self):
        fig = plt.figure(figsize=(8,8))
        gs = gridspec.GridSpec(2, 1, height_ratios=[1,1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])
        ax1 = plt.subplot(gs[1])


        x0 = 20
        theta = 5
        mu =2
        sigma = 20
        n = 200

        OU_X = {}
        GBM_X = {}
        for i in np.arange(5):
            OU_X[i] = st.simulate_OU_SDE(x0,theta,mu,sigma,2/n, n)[0]
            GBM_X[i] =st.simulate_GBM_SDE(x0, mu, 0.5, 2/n, n)


        df = pd.DataFrame(OU_X)
        df.plot(ax=ax0, lw=0.5)

        df1 = pd.DataFrame(GBM_X)
        df1.plot(ax=ax1, lw=0.5)

        plt.tight_layout()
        plt.show(block=True)



    #@unittest.skip("skipping")
    def test_np_norm(self):

        x1 =  4 * np.random.normal(loc=0,  scale=0.5)
        print x1

        x2 = 4 * np.random.normal(loc=0,scale = 0.5)
        print x2

        print np.arange(5)[-3:]



    def test_OrnsteinUhlenbeck_parameters_estimation(self):  
        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "data/FWD_historical.csv"))                         
        
        sample_size = 600
        fwd_rates = load_bbg_historical_data(file_path).ffill().tail(sample_size)                
        data = (fwd_rates['CAD_5Y_5Y'] - fwd_rates['SEK_5Y_5Y'])
        data.name = 'CDSK55'         

        cad5y5y_ou_model = st.OrnsteinUhlenbeck(data, dt=1)

        x0 = cad5y5y_ou_model.data.values[-1]
        
        theta = cad5y5y_ou_model.theta_hat
        mu = cad5y5y_ou_model.mu_hat
        sigma = cad5y5y_ou_model.sigma_hat        
        dt = cad5y5y_ou_model.dt
                
        simulated_paths = cad5y5y_ou_model.simulate_with_model_parameters(n=60, k = 1, include_history=True)

        print "\n"       
        print "======================================================"
        print "x0: {}".format(x0)
        print "theta_hat: {}".format(theta)
        print "mu_hat: {}".format(mu)
        print "sigma_hat: {}".format(sigma)
        print "dt: {}".format(dt)        
        print "======================================================"
        print simulated_paths.describe()
        print "======================================================"
        
        
        fig = plt.figure(figsize=(8,8))
        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])        
        simulated_paths.plot(ax=ax0, lw=0.5)    
        plt.tight_layout()
        plt.show(block=True)
        

    
    def test_OrnsteinUhlenbeck_conditional_moment(self):  
        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "data/FWD_historical.csv"))                         
        
        sample_size = 600
        fwd_rates = load_bbg_historical_data(file_path).ffill().tail(sample_size)                
        data = (fwd_rates['CAD_5Y_5Y'] - fwd_rates['SEK_5Y_5Y'])
        data.name = 'CDSK55'         

        cad5y5y_ou_model = st.OrnsteinUhlenbeck(data, dt=1)

        x0 = cad5y5y_ou_model.data.values[-1]
        
        theta = cad5y5y_ou_model.theta_hat
        mu = cad5y5y_ou_model.mu_hat
        sigma = cad5y5y_ou_model.sigma_hat        
        dt = cad5y5y_ou_model.dt
                        

        print "\n"       
        print "======================================================"
        print "x0: {}".format(x0)
        print "theta_hat: {}".format(theta)
        print "mu_hat: {}".format(mu)
        print "sigma_hat: {}".format(sigma)
        print "dt: {}".format(dt)        
        print "======================================================"
        print cad5y5y_ou_model.simulated_paths.describe()
        print "======================================================"

        means = cad5y5y_ou_model.conditional_means
        means_up1std = means + cad5y5y_ou_model.conditional_stdevs
        means_down1std = means - cad5y5y_ou_model.conditional_stdevs
        means_up1std.name = "1 stdev"
        means_down1std.name = "-1 stdev"
        result = pd.concat([means_up1std,means,means_down1std], axis=1)                
                
        fig = plt.figure(figsize=(8,8))
        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])        
        result.plot(ax=ax0, lw=0.5)    
        plt.tight_layout()
        plt.show(block=True)        


    
    def test_OrnsteinUhlenbeck_fractional_life(self):  
        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "data/FWD_historical.csv"))                         
        
        sample_size = 240
        fwd_rates = load_bbg_historical_data(file_path).ffill().tail(sample_size)                
        data = (fwd_rates['CAD_5Y_5Y'] - fwd_rates['SEK_5Y_5Y'])
        data.name = 'CDSK55'         

        cad5y5y_ou_model = st.OrnsteinUhlenbeck(data, dt=1)

        x0 = cad5y5y_ou_model.data.values[-1]
        
        theta = cad5y5y_ou_model.theta_hat
        mu = cad5y5y_ou_model.mu_hat
        sigma = cad5y5y_ou_model.sigma_hat        
        dt = cad5y5y_ou_model.dt
                        

        print "\n"       
        print "======================================================"
        print "x0: {}".format(x0)
        print "theta_hat: {}".format(theta)
        print "mu_hat: {}".format(mu)
        print "sigma_hat: {}".format(sigma)
        print "dt: {}".format(dt)        
        print "======================================================"
        print cad5y5y_ou_model.simulated_paths.describe()
        print "======================================================"
        print cad5y5y_ou_model.fractional_life
                
        fig = plt.figure(figsize=(8,8))
        gs = gridspec.GridSpec(1, 1, height_ratios=[1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])        
        cad5y5y_ou_model.fractional_life.plot(ax=ax0, lw=0.5)    
        plt.tight_layout()
        plt.show(block=True) 


    def test_OrnsteinUhlenbeck_first_passage_time(self):  
        np.random.seed(198908)
        path = os.path.split(inspect.getfile(inspect.currentframe()))[0]      
        path = os.path.realpath(os.path.abspath(path))
        file_path = os.path.abspath(os.path.join(path, "data/FWD_historical.csv"))                         
        
        sample_size = 180
        fwd_rates = load_bbg_historical_data(file_path).ffill().tail(sample_size)                
        data = (fwd_rates['CAD_5Y_5Y'] - fwd_rates['SEK_5Y_5Y'])
        data.name = 'CDSK55'         

        cad5y5y_ou_model = st.OrnsteinUhlenbeck(data, dt=1)

        x0 = cad5y5y_ou_model.data.values[-1]
        
        theta = cad5y5y_ou_model.theta_hat
        mu = cad5y5y_ou_model.mu_hat
        sigma = cad5y5y_ou_model.sigma_hat        
        dt = cad5y5y_ou_model.dt
        hl = cad5y5y_ou_model.half_life
                        

        print "\n"       
        print "======================================================"
        print "x0: {}".format(x0)
        print "theta_hat: {}".format(theta)
        print "mu_hat: {}".format(mu)
        print "sigma_hat: {}".format(sigma)
        print "dt: {}".format(dt)        
        print "half life: {}".format(hl)        
        print "======================================================"
        print cad5y5y_ou_model.simulated_paths.describe()
        print "======================================================"
        
        hit_distribution = cad5y5y_ou_model.simulate_first_passage_time(0.3, n = 20, k = 10000)
        hit_distribution.name = "Liklihood of hitting {} within {} days".format(0.25, 100)

        hit_distribution2 = cad5y5y_ou_model.simulate_first_passage_time(0.3, n = 20, k = 10000,
                                  momentum_factor = 0.9, momentum_lag = 5)
        hit_distribution2.name = "Liklihood of hitting {} within {} days with {} day mom".format(0.25, 100, 5)
        result = pd.concat([hit_distribution, hit_distribution2], axis=1)

        simulated_paths = cad5y5y_ou_model.simulate_with_model_parameters(n=20, k = 1, include_history=True)

        fig = plt.figure(figsize=(8,8))
        gs = gridspec.GridSpec(2, 1, height_ratios=[1,1], width_ratios=[1])
        ax0 = plt.subplot(gs[0])        
        ax1 = plt.subplot(gs[1])
        result.plot(ax=ax0, kind="bar")    
        simulated_paths.plot(ax=ax1)
        plt.tight_layout()
        plt.show(block=True)  

###############################################################################################################################3
###############################################################################################################################3
###############################################################################################################################3
###############################################################################################################################3


    
if __name__ == '__main__':
    unittest.main()
