from __future__ import division
import pandas as pd
import numpy as np
import scipy as sp
import math
from datetime import datetime
from datetime import timedelta
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from itertools import permutations, combinations
from collections import namedtuple

from statistics.factor import pca
from statistics.mr import bins

np.set_printoptions(suppress=True)

def filter_carry(carries, filters):
    found = carries.filter(filters, axis = 'index')
    found_idx = found.index.values.tolist()
    for flt in filters:
        if flt not in found_idx:
            found_idx = found_idx + [flt]
    #found = found.reindex(found_idx)
    #found['curr'] = found['curr'].fillna('NA')
    #found['fwd'] = found['fwd'].fillna('NA')
    #found['tenor'] = found['tenor'].fillna('NA')
    #found['carry'] = found['carry'].fillna(0)
    #found['roll'] = found['roll'].fillna(0)
    return found.reindex(found_idx).fillna(0)

def filter_cost(costs, filters):
    found = costs.filter(filters, axis = 'index')
    found_idx = found.index.values.tolist()
    for flt in filters:
        if flt not in found_idx:
            found_idx = found_idx + [flt]
    return found.reindex(found_idx).fillna(0)

class RatesStrategy(object):
    
    def __init__(self, components, componet_weights, carry_repo, trading_cost_repo):
        """ This is a wrapper class for a potential trade
            parameters:
            -----------
            components: dataframe 
                representing the historical data made up the trade
            component_weights: series
                representing the weights component
            carry_repo: dataframe (curr,fwd,tenor,sec(index col),carry,roll)
                representing the forward looking carry 
            trading_cost: dataframe
                representing the negative impacts on the pnl from each component of the trade at the entry and exit                                      
        """

        self._components = components
        self._component_weights = componet_weights
        self._component_carries = filter_carry(carry_repo, self._components.columns)
        self._component_weighted_carries = self._component_carries * self._component_weights  
        self._component_trading_costs = filter_cost(trading_cost_repo, self._components.columns)
        name_weight_pairs = zip(self._components.columns.tolist(), self._component_weights.values)
        weighted_components = self._components * self._component_weights    
        weighted_components.columns = map(lambda n: 'w_{}'.format(n), weighted_components.columns.tolist()) 
        strategy_series = weighted_components.sum(axis=1)
        strategy_series.name = 'data' 
        self._strategy_components  = pd.concat([strategy_series, self._components, weighted_components], axis=1) 
        self._strategy_components.name = ' '.join(['%+.3f * %s' % (weight, sec) for (sec, weight) in name_weight_pairs])

    @property
    def data(self):
        d = self._strategy_components['data']
        d.name = self._strategy_components.name
        return d

    @property
    def component_weights(self):
        return self._component_weights

    @property
    def component_weighted_carries(self):
        return self._component_weighted_carries

    @property
    def component_trading_costs(self):
        return self._component_trading_costs
    
    @property
    def strategy_components(self):
        return self._strategy_components


def create_pca_based_mr_rates_trade(data, 
    carry_repo=None, 
    trading_cost_repo=None, 
    beta = None, 
    existing_risk = None,
    num_bins = 20,
    min_num_bins = 20):

    """Create a mr trade strategy optimised using PCA
        Parameters
        -----------
        data: dataframe
            containing the historical data which makes up the weighted data series
        carry_repo: series
            repositary of security carry and roll
        trading_cost_repo: series 
            repositary of seurity trading cost
        beta: dataframe
            containing the historical data representing different beta 
        existing_risk: dataframe
            containing the historical data representing the exisitng risk 

        Returns
        --------

    """

    # run pca to get the weighted series and weights
    pca_obj = pca.PCA(data, rates_pca = True)
    hedge_weighted_data, hedge_weights, hedge_corr_test, hedge_description = pca_obj.pc_hedge
    print hedge_weights
    # run mr diagnosis using the weighted data
    mr_obj = bins.create_mr_diagnosis(hedge_weighted_data, num_bins, min_num_of_bins = min_num_bins)
    # create rate strategy based on the mr direction
    directional_hedge_weights = hedge_weights if mr_obj.direction == 'R' else hedge_weights * -1 
    pca_mr_rates_strategy = RatesStrategy(data, hedge_weights, carry_repo, trading_cost_repo)
    print directional_hedge_weights
    # pnl estimation accounted for carry and tading cost 
    # run correlation test with beta if available 
    # run correlation test with existing risk if available

    mr_obj.create_mr_info_fig((18, 7), pca_obj = pca_obj)
    pca_obj.create_hedged_pcs_fig()

    return pca_mr_rates_strategy




