import pandas as pd
import numpy as np
import os, sys, inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from cycler import cycler
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import seaborn as sns
from itertools import combinations
from pandas.tseries.offsets import DateOffset
from scipy import stats
from scipy.cluster.hierarchy import dendrogram, linkage, cophenet, fcluster
import scipy.cluster.hierarchy as cl
from scipy.spatial.distance import pdist, squareform

pd.set_option('display.expand_frame_repr', False)



def price_swaption(forward, strike, expiry, vol, payer=True, normal=True):
    """[summary]
        Parameters
        ----------- 
        forward num: 
            the fwd in %
        strike num:
            the strike level in %
        expiry num:
            time to expiry in years including fraction
        vol num:
            annualised vol in bps
        payer bool:
            true for payer and false for receriver
        normal bool: 
            true for normal false for log normal
        Returns
        -------
        result: swaption price in % underline rate term.                         
    """
    payer_price = np.nan
    receriver_price = np.nan
    
    
    if normal:
        _vol_t = (vol/100) * np.sqrt(expiry)
        
        # if(_vol_t < 0.01):
        # print(f'forward: {forward},strike {strike}, v:{vol} exp:{expiry}')
        
        _x = (forward - strike)/_vol_t
        N1 = stats.norm.cdf(_x, loc=0, scale=1)
        payer_price = (forward - strike) * N1 + (_vol_t / np.power(2 * np.pi, 0.5)) * np.exp(-(np.power(_x,2)/2))    
        receiver_price = payer_price - (forward - strike)
        return payer_price if payer else receiver_price          
    else:
        _vol_t = (vol/100) * np.sqrt(expiry)
        _x = (np.log(forward/strike) + 0.5 * np.power(_vol_t, 2)) / _vol_t
        N1 = stats.norm.cdf(_x, loc=0, scale=1)
        N2 = stats.norm.cdf(_x - _vol_t, loc=0, scale=1)
        payer_price = forward * N1 - strike * N2
        receiver_price = payer_price - (forward - strike)
        return payer_price if payer else receiver_price 


def swaption_delta(forward, strike, expiry, vol, payer=True, normal=True):
    """[summary]
        Parameters
        ----------- 
        forward num: 
            the fwd in %
        strike num:
            the strike level in %
        expiry num:
            time to expiry in years including fraction
        vol num:
            annualised vol in bps
        payer bool:
            true for payer and false for receriver
        normal bool: 
            true for normal false for log normal
        Returns
        -------
        result: swaption delta in -1 to 1. unit is bp of the underline                         
    """
    dd = price_swaption(forward - 0.005, strike, expiry, vol, payer=payer, normal=normal)
    du = price_swaption(forward + 0.005, strike, expiry, vol, payer=payer, normal=normal)
    return (du - dd) * 100

def swaption_gamma (forward, strike, expiry, vol, payer=True, normal=True):
    """[summary]
        Parameters
        ----------- 
        forward num: 
            the fwd in %
        strike num:
            the strike level in %
        expiry num:
            time to expiry in years including fraction
        vol num:
            annualised vol in bps
        payer bool:
            true for payer and false for receriver
        normal bool: 
            true for normal false for log normal
        Returns
        -------
        result: swaption gamma in same unit as delta. unit is bp of underline                           
    """
    ddd = swaption_delta(forward - 0.005, strike, expiry, vol, payer=payer, normal=normal)
    ddu = swaption_delta(forward + 0.005, strike, expiry, vol, payer=payer, normal=normal)
    return (ddu-ddd)
    
def swaption_vega (forward, strike, expiry, vol, payer=True, normal=True):
    """[summary]
        Parameters
        ----------- 
        forward num: 
            the fwd in %
        strike num:
            the strike level in %
        expiry num:
            time to expiry in years including fraction
        vol num:
            annualised vol in bps
        payer bool:
            true for payer and false for receriver
        normal bool: 
            true for normal false for log normal
        Returns
        -------
        result: swaption vega impact on the swaption price in bp of underline term.   
                it represents either 1bp move higher in normal vol or 
                1% move higher in lognormal vol                     
    """
    dvd = price_swaption(forward, strike, expiry, vol-0.5, payer=payer, normal=normal)
    dvu = price_swaption(forward, strike, expiry, vol+0.5, payer=payer, normal=normal)
    return (dvu - dvd) * 100
    
def swaption_theta(forward, strike, expiry, vol, payer=True, normal=True):
    """[summary]
        Parameters
        ----------- 
        forward num: 
            the fwd in %
        strike num:
            the strike level in %
        expiry num:
            time to expiry in years including fraction
        vol num:
            annualised vol in bps
        payer bool:
            true for payer and false for receriver
        normal bool: 
            true for normal false for log normal
        Returns
        -------
        result: swaption theta impoact on swaption price in bp of underline term.
                a year is considered to be 252 days                         
    """
    dtnow = price_swaption(forward, strike, expiry, vol, payer=payer, normal=normal)
    dtolder = price_swaption(forward, strike, expiry-1/252, vol, payer=payer, normal=normal)
    return (dtolder- dtnow) * 100

    
     
                   
def convert_to_year(period):
    """
    Parameters
    ----------
    period : string
        i.e '3M', '2Y', '1W'
    Returns
    -------
    years in fraction.

    """
    _p =float(period[:-1])
    if period.upper().endswith('Y'):
        pass
    elif period.upper().endswith('M'):
        _p = _p / 12
    elif period.upper().endswith('W'):
        _p = _p / 52
    elif period.upper().endswith('Q'):
        _p = _p / 4
    return _p
    

    
# test
if(2 > 3):
    assert abs(price_swaption(1.325, 1.5, 5,52, payer=True, normal=True) - 0.3816) < 0.0001    
    assert abs(price_swaption(2.8, 3.1, 0.25, 13, payer=False, normal=False) - 0.3048) < 0.0001
    price_swaption(2.55, 2.8, 5, 74.5, payer=True, normal=True)
    swaption_detla(1.1, 1.3, 1, 50, payer=True, normal=True)
    swaption_gamma(1.1, 1.3, 1, 50, payer=True, normal=True)
    swaption_vega(1.1, 1.3, 1, 50, payer=True, normal=True)
    swaption_theta(1.1, 1.3, 1, 50, payer=True, normal=True)
    
    
    
    
    
    