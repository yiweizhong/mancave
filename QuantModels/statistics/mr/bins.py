""" bin diagnostics
"""
from __future__ import division
import numpy as np
from scipy.stats import gaussian_kde
import pandas as pd
from pandas.tseries.offsets import BDay,Minute, DateOffset
import statsmodels.tsa.stattools as ts
import functools
from core import tools
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
import matplotlib.dates as mpd
from matplotlib.ticker import MultipleLocator
import matplotlib.dates as mdates
from matplotlib.patches import (Circle,Ellipse)
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage, AnnotationBbox)
import matplotlib.cm as cm
import matplotlib.ticker as ticker
from collections import namedtuple
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import helper as helper 
import stochastic as st



def create_equal_width_bins_with_high_evenness(data, n, min_size_limit=0.05, reduction_size = 2, min_num_of_bins = 8):
    """ auto reduce the number of the bin to increase the bin size evenness
    parameters
    ----------
    Parameters
    ----------
    data: 2 col dataframes
        level, change 
    n:int
        the number of bins
    min_size_limit: float
        the min bin size ratio can be
    reduction_size: int
        the number of bins to reduce during each reduction
    min_num_of_bins: int
        the minimal amount of bins

    Returns
    -------
    dataframe 
        the complete set of bins regardless whether it is empty
    """
    
    holder = []
    
    holder.append(create_equal_width_bins(data, n))
    holder.append(0)

    while (holder[0]['bin_size_ratio'].dropna().min() < min_size_limit and (n - holder[1]) > min_num_of_bins):
        holder[1] = holder[1] + reduction_size
        holder[0] = create_equal_width_bins(data, n - holder[1])
             
    return holder[0]
    
def create_equal_width_bins(data, n):
    """" Create equal sized bin for the level and aggregate the returns 
    Parameters
    ----------
    data: 2 col dataframes
        level, change 
    n:int
        the number of bins

    Returns
    -------

    dataframe 
        the complete set of bins with stats regardless whether it is empty
    """     
    def f_stdev(x): return np.std(x.dropna(), ddof=1)
    def f_mean(x): return np.average(x.dropna()) 
    def f_count(x): return len(x.dropna())
    
    levels = data["level"]
    changes = data["change"]
  
    max = levels.values.max()
    min = levels.values.min()
    width = (max - min)/n

    # create the equal sized bins based on the levels
    edges = [ min + width * (i) for i in np.arange(n)] + [max]   
    l_edges = [ min + width * (i)  for i in np.arange(n)]
    r_edges = [ min + width * (i + 1)  for i in np.arange(n)]

    #make the range slightly wider so every data point can be included
    # ensure the max/min item dont accidentally drops off due to the way width is calculated.
    
    l_edges[0] = levels.values.min() if levels.values.min() < l_edges[0] else l_edges[0]
    r_edges[-1] = levels.values.max() if levels.values.max() > r_edges[-1] else r_edges[-1]

    mids = [ min + width * (i + 0.5)  for i in np.arange(n)]    
    
    #lcols = pd.cut(levels, edges, right=True, labels= l_edges, include_lowest =True)
    mcols = pd.cut(levels, edges, right=True, labels= mids, include_lowest =True) 
    #rcols = pd.cut(levels, edges, right=True, labels= r_edges, include_lowest =True)  
    
    #attach the bin category onto the changes

    #this is changed after updating pandas
    # bins = pd.DataFrame({"bin_mid": mcols,'changes': changes })  
    bins = pd.DataFrame({"bin_mid": mcols.astype(float) ,'changes': changes })

    #calculate the stats of changes based on the category
    bin_stats = bins.groupby("bin_mid")['changes'] \
                   .agg([f_count, f_mean,f_stdev]) \
                   .rename(columns={'f_count': 'bin_size', 'f_mean':'bin_avg', 'f_stdev':'bin_vol'})
    
    
    
    #create the bin profile
    bin_profile = pd.DataFrame({"bin_l_edge" : l_edges, "bin_mid": mids ,"bin_r_edge": r_edges})
    bin_profile = pd.merge(bin_profile.reset_index(), bin_stats.reset_index(), how='inner', on='bin_mid').set_index('index')
    bin_profile.index.name = "bin_id"
    bin_profile['bin_size'] = bin_profile['bin_size'].fillna(0)
    bin_profile['bin_avg'] = bin_profile['bin_avg'].fillna(0)
    bin_profile['bin_vol'] = bin_profile['bin_vol'].fillna(0)
    bin_profile['bin_avg_u_std'] = bin_profile['bin_avg'] + bin_profile['bin_vol']
    bin_profile['bin_avg_l_std'] = bin_profile['bin_avg'] - bin_profile['bin_vol']
    bin_profile['bin_avg_u_2std'] = bin_profile['bin_avg'] + 2 * bin_profile['bin_vol']
    bin_profile['bin_avg_l_2std'] = bin_profile['bin_avg'] - 2 * bin_profile['bin_vol']
    bin_profile['bin_avg_u_3std'] = bin_profile['bin_avg'] + 3 * bin_profile['bin_vol']
    bin_profile['bin_avg_l_3std'] = bin_profile['bin_avg'] - 3 * bin_profile['bin_vol']   
    bin_profile['bin_size_ratio'] = bin_profile['bin_size']/len(levels.index)    
    bin_profile.name = 'bin_' + data.name if data.name is not None else 'data' 
    
    return bin_profile

def derive_mr_attributes(data, bin_profile):
    r""" check the mr quality from 2 aspects:
                  
    Parameters
    ----------
    data_with_bin_id: dataframe
        data with bin id
        
    bin_profile: dataframe
        the bin stats
    Returns
    -------
    series
        various measurements of mr quality 
    """
    
    bin_avg = bin_profile['bin_avg']
    bin_size = bin_profile['bin_size']
    
    # asymmetry
    h_ln = int(len(bin_avg.index)/2)
    #integrate area for the left and right half of the bin profile
    l_half_area = (bin_avg[:h_ln] * bin_size[:h_ln]).dropna().sum()
    r_half_area = (bin_avg[h_ln:] * bin_size[h_ln:]).dropna().sum()
    #derive the symmetry score (0, 1)
    mr_symmetry = 1 - abs((l_half_area + r_half_area)/(abs(l_half_area) + abs(r_half_area)))
    
    #use 2nd poly fit. The sum of beta_x and beta_xx is used rank the mr strength
    bin_avg = bin_avg.dropna()
    betas, _, _, _, _ = np.polyfit(list(bin_avg.index.values), bin_avg.values, deg = 2, full=True)
    beta_xx = betas[0]
    beta_x = betas[1]
    beta_total = beta_xx + beta_x
    linearity = 1 - abs(beta_xx)/(abs(beta_x) + abs(beta_xx)) #(0, 1)
    
    #placeholders

    mr_score = -99999
    #lowestpossiblestationary_score = 0
    maxtype2errchanceforstationaryconviction = 1
    
    #adf
    levels = data['level'].dropna().values
    adf_stats, adf_p_value, _, _, adf_ttable, _  = ts.adfuller(levels)

    ##hurst exp
    # Create the range of lag values
    lags = range(2, len(levels)-1)
    # Calculate the array of the variances of the lagged differences
    tau = [np.sqrt(np.std(np.subtract(levels[lag:], levels[:-lag]))) for lag in lags]
    # Use a linear fit to estimate the Hurst Exponent
    poly = np.polyfit(np.log(lags), np.log(tau), 1)
    # Return the Hurst exponent from the polyfit output
    


    
    #only test for stationary and update the mr_score if we have a negative beta
    if beta_total < 0:
        
        #adf_stats, adf_p_value, _, _, adf_ttable, _  = ts.adfuller(levels)
        #stationary = 1 if adf_p_value <= 0.05 else 0 # binary here
        maxtype2errchanceforstationaryconviction = 0.01 if adf_stats < adf_ttable['1%'] else ( 1 if adf_stats >= adf_ttable['10%'] else (0.05 if adf_stats < adf_ttable['5%'] else 0.1))
        #lowestpossiblestationary_score = stationary * (1 - maxtype2errchanceforstationaryconviction)
        
        #mr_symmetry from the left and right bin profilt might be close to 1 but the betas can be 0 too. that means the bean profile has 0 mr as the value of the bin are alternating 
        #above and below the average changes (the x-axis). in order to reflect that the betas are timed with the symmetry so if the beta is near zero the mr_score will be close to 0 too 
        mr_score = (abs(l_half_area) + abs(r_half_area)) * mr_symmetry * (abs(beta_x) * h_ln + abs(beta_xx) * h_ln * h_ln)  
    
    
    attribute = pd.Series({'name': data.name,
                         'l_half_area' : l_half_area, 
                         'r_half_area' : r_half_area,
                         'beta_xx': beta_xx, 
                         'beta_x': beta_x,
                         'beta_total': beta_total,
                         'mr_symmetry': mr_symmetry, 
                         'max_stationary_err_chance' : maxtype2errchanceforstationaryconviction,
                         #'conservative_stationary_score': lowestpossiblestationary_score, 
                         'linearity': linearity,
                         'mr_score': mr_score,
                         'adf':(adf_stats, adf_p_value, adf_ttable),
                         'hurst_exp':poly[0] * 2.0
                         })
    attribute.name = data.name     
    
    return attribute

'''Main API entry point'''

def create_mr_diagnosis(ds_in, n, offset=1, shift_forward = True, min_bin_content_pct=0.05, epoch_reduction_size = 2, min_num_of_bins = -1, resample_freq = 'B', sample_range=None):
    
    def get_sample_range_index(ds_in, sample_range):
        def is_date(dt_str):
            try:
                pd.to_datetime(dt_str)
            except:
                return False
            return True
            
        if sample_range is None:
            return ds_in.index
        elif ((type(sample_range) is tuple) & 
              (len(sample_range) == 2) &
              (is_date(sample_range[0])) &
              (is_date(sample_range[1]))):
            return ds_in.index[(ds_in.index >= sample_range[0]) & 
                               (ds_in.index <= sample_range[1])]
        else: 
            raise ValueError("Unrecognised range for sampling")               
    
    
    if resample_freq.upper() not in ['B', 'W'] : raise TypeError('resample_freq must be B - business daily, W - weekly')
    
    ds = ds_in.resample(resample_freq).last()

    if  hasattr(ds_in, 'name'):
        ds.name = ds_in.name

    #
    diff = helper.calc_next_n_periods_moves(ds, n=offset) if shift_forward else helper.calc_prev_n_periods_moves(ds, n=offset)
    
    #bin_data = pd.DataFrame({'level':ds, 'change':diff}).dropna().sort_values(['level'],ascending=[True])
    # need to use forward fill here otherwise raw ds and bin data is out of sync
    bin_data = pd.DataFrame({'level':ds, 'change':diff}).ffill().sort_values(['level'],ascending=[True])
    
    bin_data.name = ds.name if  hasattr(ds, 'name') else 'NA'

    diagnosis_bins = create_equal_width_bins_with_high_evenness(bin_data, 
                                                                n, 
                                                                min_size_limit=min_bin_content_pct, 
                                                                reduction_size = epoch_reduction_size, 
                                                                min_num_of_bins = n if min_num_of_bins == -1 else min_num_of_bins)    

    mr_attributes = derive_mr_attributes(bin_data, diagnosis_bins)

    #2022-07-24 introducing sample range for OU
    #the data used for fitting is a subset based on sampe range. the history_data includes everything it is for charting
    ou = st.OrnsteinUhlenbeck(ds.reindex(get_sample_range_index(ds, sample_range)), resample_freq = resample_freq,history_data=ds)
    
    bin_diagnosis = MrDiagnosis(ds, bin_data, diagnosis_bins, mr_attributes, ou, shift_forward, offset, resample_freq = resample_freq)
    
    return bin_diagnosis

class MrDiagnosis(object):
    def __init__(self, ds, bin_data, diagnosis_bins, 
                 mr_attributes, OU, shift_forward, 
                 offset, beta = None, carry= None,
                 sec_name_translation = None,
                 resample_freq = 'B'):
        """ This is a wrapper class for MR analysis. There are 2 main models in
            a MrDiagnosis object: OU and Bins. 
            OU is for parametric modelling
            Bins is for non parametric modelling
            parameters:
            -----------
            ds: data series
                representing the data on which MR is being analysed
            bin_data: dataframe
                the ds with its relevant changes
            diagnosis_bins: dataframe
                bin_data being turned into equal width bins and boundaries 
            mr_attributes:
                some info regarding the quality of the mr
            OU: an stochastic object 
                containing all the information regarding OU estimation and modelling
            shift_forward: bool
                indicating if the changes of ds is shifting forward (next days movements) or shifting backward (previous days movements)
            offset:
                how many periods to shift
            beta: dataframe
                assets prices representing different betas
            carry: dataframe
                carry for different rates
            sec_name_translation: dictionary
                translate the fwd_tenor into the bbg ticker
            resample_freq: string
                resample frequence: 'B' - business day, 'W' - weekly


        """
        if resample_freq.upper() not in ['B', 'W'] : raise TypeError('resample_freq must be BD - business daily, W - weekly')
        if not isinstance(ds, pd.Series): raise TypeError("ds arg needs to be a pandas time series")
        if not hasattr(ds, 'name'): raise TypeError("The input ds needs to have a name for display purpose")

        self._name = ds.name
        self._raw_ds = ds
        self._bin_data = bin_data
        self._diagnosis_bins = diagnosis_bins
        self._mr_attributes = mr_attributes
        self._ou = OU
        self._shift_forward = shift_forward
        self._offset = offset
        self._beta = beta
        self._carry = carry
        self._sec_name_translation = sec_name_translation
        self._resample_freq = resample_freq
        self._sim_path = None
        self._sim_result = None
        self._transition_density_matrix = None
        self._transition_state_hist = None
 

    @property
    def name(self):
        return self._name
    
    @property
    def raw_ds(self):
        return self._raw_ds

    @property
    def bin_data(self):   
        return self._bin_data

    @property
    def bin_diagnosis(self):   
        return self._diagnosis_bins

    @property
    def ou(self):   
        return self._ou

    @property
    def bin_drift_coefficients(self):
        return self._diagnosis_bins['bin_avg']

    @property
    def bin_diffusion_coefficients(self):
        return self._diagnosis_bins['bin_vol']

    @property 
    def mr_attributes(self):
        return self._mr_attributes

    @property
    def current_details(self):
        x = self.raw_ds.iloc[-1] # get the last dp from the raw input, which is the level as the latest dp
        # inclusive left edge only. therefore do a tail(1) to ensure only 1 bin gets picked up
        return (x, self._diagnosis_bins[(self._diagnosis_bins['bin_l_edge'] <= x) & (x <= self._diagnosis_bins['bin_r_edge'])].tail(1))

    @property
    def direction(self):
        return 'R' if self.ou.history_data[-1] > self.ou.conditional_means[-1] else 'P'
    
    @property
    def sim_path(self):
        if(self._sim_path is None):
            raise ValueError("run estimate_transition_probability_denisty first")
        return self._sim_path

    @property
    def sim_result(self):
        if(self._sim_result is None):
            raise ValueError("run estimate_transition_probability_denisty first")
        return self._sim_result

    @property
    def transition_density_matrix(self):
        if(self._transition_density_matrix is None):
            raise ValueError("run estimate_transition_probability_denisty first")
        return self._transition_density_matrix

    @property
    def transition_state_hist(self):
        if(self._transition_state_hist is None):
            raise ValueError("run estimate_transition_probability_denisty first")
        return self._transition_state_hist



    def estimate_moment(self, x, number_of_neighbour_for_mean_bracket = 6, number_of_neighbour_for_var_bracket=6, full_return = False):
        """ estimate what the moment (i.e mean and variance) would be based on the diagnosis bins based on a specified level
            using a 2nd degree ployfit.
            parameters:
            -----------
            x: float
                level
            
            returns:
            --------
            fitted_mean: float
            fitted_vol: float
            coefficients for mean: triplet of floats
            coefficients for vol: triplet of floats 
            selected mean brackets for interpolation: series
            selected vol brackets for interpolation: series
        """
        
        # simply locate the bin using the  level and return the bin_avg and bin_vol give too little varibilities. 
        # Instead, bracket out a small range of bins around the bin where the input level is in. fit the mean and 
        # variance from the bracket of bins

        mean_data = self.bin_diagnosis[['bin_mid', 'bin_avg']].dropna().set_index('bin_mid')['bin_avg']
        mean_bracket = helper.find_neighbours_in_series(mean_data, x, number_of_neighbour_for_mean_bracket)
        
        
        vol_data = self.bin_diagnosis[['bin_mid', 'bin_vol']].dropna().set_index('bin_mid')['bin_vol']
        vol_bracket = helper.find_neighbours_in_series(vol_data, x, number_of_neighbour_for_var_bracket)
        
        (c1, b1, a1) = np.polyfit(list(mean_bracket.index.values), list(mean_bracket.values), deg = 2)
        fitted_mean = round(a1 + b1 * x + c1 * x *  x, 4) 
        
        (c2, b2, a2) = np.polyfit(list(vol_bracket.index.values), list(vol_bracket.values), deg = 2)
        fitted_vol = round(a2 + b2 * x + c2 * x * x, 4)

        if full_return:
            return fitted_mean, fitted_vol, (c1, b1, a1) , (c2, b2, a2), mean_bracket, vol_bracket
        else: 
            return fitted_mean, fitted_vol



      

    def simulate_paths_nonparametrically(self, n=60, k=1, dt=1, x0_override=None, target=None,stop=None, mm_factor=0, mm_window=5):
        """ Simulate data paths using the diagnosed mean reversion characteristics
            parameters:
            -----------
            n: int
                the number of data points to be simulated in a path. 
            k: int
                the number of simulations
            dt: float
                the time step
            x0_override: float
                optional set the x0 
            target: float or None
                the value at which the simulation will stop due to hitting the target
            stop: float or None
                the value at which the simulation will stop due to hitting the stop
            mm_factor: float 
                a number between 0 to 1 indicates how much momentum strength will be factored in from previous values
            mm_window: int
                the number of days between which the momentum is calculated from

            returns:
            ----------
            simulated paths: dataframe
                
        """

        if mm_factor < 0 or mm_factor > 1: raise  RuntimeError("The momentum factor must be between 0 and 1")        

        n = n
        freq = 'B'
        dt = dt

        
        # if self._resample_freq.upper() == 'W':
        #     n = 10
        #     freq = 'W'
        #     dt = 1
        #     print('override by W')
        

        def simulate_1_path_dx(n, x0, dt, mm_factor, mm_window, test_target, test_stop):
                  
            mm_history = [np.nan] * mm_window
            xs = [np.nan] * n
            stops = [0] * n
            targets = [0] * n
            
            x_prev = x0

            for i in np.arange(0, n, 1):
                mm_effect = helper.calc_momentum(mm_history) * mm_factor
                mean, vol = self.estimate_moment(x_prev)

                dx = dt * mean +  vol * np.random.normal(loc=0.0, scale=np.sqrt(dt)) + mm_effect
                x_prev = x_prev + dx
                xs[i] = x_prev
                
                mm_history = mm_history[1:] + [x_prev]
                
                hit_stop, stop = test_stop(xs[i])
                if (hit_stop):
                    xs[i] = stop
                    stops[i] = 1
                    return xs, stops, targets
                
                hit_target, target = test_target(xs[i])
                if (hit_target):
                    xs[i] = target
                    targets[i] = 1
                    return xs, stops, targets
                
            return xs, stops, targets
                

        x0 = self.raw_ds[-1] if x0_override is None else x0_override       
        test_target = helper.create_hit_test(x0, target)
        test_stop = helper.create_hit_test(x0, stop)
        #sim_dates = pd.date_range(start=self.raw_ds.index[-1], periods=n + 1, freq=self._resample_freq.upper())[1:]
        sim_dates = pd.date_range(start=self.raw_ds.index[-1], periods=n + 1, freq=self._resample_freq.upper())

        
        fdt = float(dt)
        path_ids = ['path_%s' % (i+1) for i in np.arange(k)]
        path_x = {}
        path_stop = {}
        path_target = {}

        #simulation loops
        for path_id in path_ids:
            xs, stops, targets = simulate_1_path_dx(n, x0, fdt, mm_factor, mm_window, test_target, test_stop)
            #add the first x
            xs = [x0]+xs
            stops = [0] + stops
            targets = [0] + targets
            path_x[path_id] = pd.Series(data=xs,index=sim_dates)
            path_stop[path_id] = pd.Series(data=stops,index=sim_dates)
            path_target[path_id] = pd.Series(data=targets,index=sim_dates)            

        simulated_paths = pd.DataFrame(path_x)

        sim_distribution = pd.DataFrame(
            {
                'stop_hits': pd.DataFrame(path_stop).sum(axis=1),
                'target_hits': pd.DataFrame(path_target).sum(axis=1)
            }    
        )
        
        sim_distribution['stop_hits_exp_pct'] =  sim_distribution['stop_hits'].cumsum()/k
        sim_distribution['target_hits_exp_pct'] =  sim_distribution['target_hits'].cumsum()/k 
        sim_distribution['miss_hits_exp_pct'] =  1 - sim_distribution['stop_hits_exp_pct'] - sim_distribution['target_hits_exp_pct']
        sim_distribution['x_mean'] = simulated_paths.mean(axis=1)
        sim_distribution['x_std'] = simulated_paths.std(axis=1)
        #to avoid 0 std from the original data point at position 0 which will distort the chart
        sim_distribution.loc[sim_distribution.index[0], 'x_std'] = np.nan
        
        return simulated_paths, sim_distribution
    
    
    def estimate_conditional_transition_denisty(self, n=60, k=1, dt=1, x0_override=None, target=None,stop=None, mm_factor=0, mm_window=5):
        """ estimating the conditional transtion density through simulation. bin stats are unconditional 
            parameters:
            -----------
            n: int
                the number of data points to be simulated in a path. 
            k: int
                the number of simulations
            dt: float
                the time step
            target: float or None
                the value at which the simulation will stop due to hitting the target
            stop: float or None
                the value at which the simulation will stop due to hitting the stop
            mm_factor: float 
                a number between 0 to 1 indicates how much momentum strength will be factored in from previous values
            mm_window: int
                the number of days between which the momentum is calculated from

            returns:
            ----------
            simulated paths: dataframe
                
        """
            
        sim_path, sim_result = self.simulate_paths_nonparametrically(n=n,k=k,dt=dt, x0_override=x0_override, 
                                                                     target=target,stop=stop, mm_factor=mm_factor, 
                                                                     mm_window=mm_window)
        lr_boundries = self.bin_diagnosis[['bin_l_edge','bin_r_edge']]
        
        transition_state_hist = sim_path.applymap(lambda x: helper.find_bin(lr_boundries,x))
        
        # transtion density matrix is a markov state matrix
        transition_density_matrix = helper.create_transition_matrix(lr_boundries.index, transition_state_hist)
        
        transition_density_matrix.index = self.bin_diagnosis['bin_mid'].values
        
        transition_density_matrix.columns = self.bin_diagnosis['bin_mid'].values
        
        
        self._sim_path = sim_path
        self._sim_result = sim_result
        self._transition_density_matrix = transition_density_matrix
        self._transition_state_hist = transition_state_hist
        

        return sim_path, sim_result, transition_density_matrix, transition_state_hist         
  
    
    def markvo_forecast_horizon_value(self, ns = [5], starting_bin = None):
        """ forecast the horizon value using the conditional transition densities
            The esitmation needs to be done separately before hand!
            parameters:
            -----------
            ns: list of int
                a list of horizons. 
            starting_bin: int
                the bin id the starting value. If it is None then the current value 
                will be used. 
            returns:
            ----------
            horizon value distributions: dataframe
                each col is a horizon            
        """
        
        # A is the markov chian state matrix
        # if the estimation has not ran here will fail
        A = self._transition_density_matrix.to_numpy()
        
        
        _starting_bin = starting_bin if starting_bin is not None else self.current_details[1].index[0]
        init_states = np.array([0] * len(self.bin_diagnosis.index))
        init_states[_starting_bin] = 1 #100% happened 
        init_states = init_states.reshape(-1,1) #turn it into col vector
        
        result = {}
        for n in ns:
            end_state = (init_states.T @ np.linalg.matrix_power(A, n)) # end_State is 1 x n 2d np array            
            result[n] = pd.Series(data=list(end_state.flat), index=self._transition_density_matrix.index)            
        return pd.DataFrame(result)



   




    def plot_bin_diagnosis_bin_size_ax(self, ax, xlim = None, ylim = None):

        if xlim is not None: ax.set_xlim(xlim)
        if ylim is not None: ax.set_ylim(ylim)
        ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')

        major_ticks = list(self.bin_diagnosis.bin_mid.values)

        ax.set_xticks(major_ticks) 
        ax.xaxis.set_major_locator(ticker.FixedLocator(major_ticks))
        ax.set_xlim(self.bin_diagnosis.iloc[0,:].bin_l_edge, self.bin_diagnosis.iloc[-1,:].bin_r_edge)

        #ax.xaxis.set_visible(False)
        ax.axis('off')

        #plot the sizes of the bins
        bar_width = 0.3 * (self.bin_diagnosis.iloc[-1,:].bin_r_edge - self.bin_diagnosis.iloc[0,:].bin_l_edge) / len(self.bin_diagnosis.index)
        bars = ax.bar(list(self.bin_diagnosis.bin_mid.values), list(self.bin_diagnosis.bin_size.values), width=bar_width, color='lightgrey')
        for bar in bars:
            height = bar.get_height()
            ax.text(bar.get_x() + bar.get_width()/2., 1.01*height, '%d' % int(height), ha='center', va='bottom', color='dimgrey',fontsize=6)
        ax.set_ylabel('bin sample size', fontsize=8)

        plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8) #6->8
        #plt.setp(ax.xaxis.get_majorticklabels(), rotation=40, fontsize = 6)
        #plt.setp(ax.xaxis.get_majorticklabels(), fontsize = 6)
        


    
    def plot_bin_diagnosis_ax(self, ax, xlim = None, ylim = None, dot_size = 0.1, fit_1std = True, fit_2ndd = True, ignore_bin = []):
        
        if xlim is not None: ax.set_xlim(xlim)
        if ylim is not None: ax.set_ylim(ylim)
        ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.yaxis.grid(True, which='both', linestyle='-', linewidth='0.2')
        ax.minorticks_on()

        #flip up 
        ax.xaxis.set_ticks_position('top')
        ax.xaxis.set_label_position('top')
        
        #ax.yaxis.set_ticks_position('left')

        major_ticks = list(self.bin_diagnosis.bin_mid.values)

        ax.set_xticks(major_ticks) 
        ax.xaxis.set_major_locator(ticker.FixedLocator(major_ticks))
        ax.set_xlim(self.bin_diagnosis.iloc[0,:].bin_l_edge * 0.99, (self.bin_diagnosis.iloc[-1,:].bin_r_edge + self.bin_diagnosis.iloc[-1,:].bin_r_edge - self.bin_diagnosis.iloc[-1,:].bin_mid) * 1.01)

        ax.axhline(y=0, color="black", lw=0.5)
        plt.setp(ax.xaxis.get_majorticklabels(), rotation=40, fontsize = 8) #6->8
        plt.setp(ax.xaxis.get_majorticklabels(), fontsize = 8) #6->8
        plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 8) #6->8
        ax.set_ylabel('{} {} period return distribution'.format( 'next' if self._shift_forward else 'previous', self._offset), fontsize=8)



        #plot the drift coefficients
        for bin_id, row in self.bin_diagnosis.iterrows():
            
            coefficient = np.nan if np.isnan(row.bin_avg) else row.bin_avg
            coefficient_u_1std = np.nan if np.isnan(coefficient) else row.bin_avg_u_std
            coefficient_l_1std = np.nan if np.isnan(coefficient) else row.bin_avg_l_std
            coefficient_u_2std = np.nan if np.isnan(coefficient) else row.bin_avg_u_2std
            coefficient_l_2std = np.nan if np.isnan(coefficient) else row.bin_avg_l_2std
            

            if(bin_id not in ignore_bin):
                point_alpha = 0 if np.isnan(row.bin_avg) else 0.4
            else: 
                point_alpha = 0 if np.isnan(row.bin_avg) else 0.1
            
            #coefficients
            ax.scatter(row.bin_mid, coefficient, c='blue', alpha=point_alpha)
            #coefficients +- 1std boundary
            
            #ax.plot([row.bin_mid, row.bin_mid], [coefficient_l_1std, coefficient_u_1std], lw=8, c='blue', ls = '-', alpha=point_alpha * 0.4)
            
            
            
            
            
            ax.annotate('%.2f' % (row.bin_avg),
                        (row.bin_mid, row.bin_avg), 
                        xytext=(-2, 5), xycoords='data', textcoords='offset points', fontsize=8) #6->8
            
            #plot underline bin content
            if(bin_id not in ignore_bin):
                left_edge = -0.001 if(bin_id == 0) else 0
                underlines = self._bin_data[(self._bin_data['level'] >= row.bin_l_edge + left_edge) & \
                                            (self._bin_data['level'] <= row.bin_r_edge)]['change']
                
                for v in underlines.values:
                    #print(f'{}-{}'.format(v)
                    #_v = v if (np.isnan(row.bin_vol) or (v <= row.bin_avg_u_3std and v >= row.bin_avg_l_3std)) else (row.bin_avg_u_3std if v > row.bin_avg_u_3std else  row.bin_avg_l_3std)
                    _v = v #retain all the outliers 
                    ax.scatter(row.bin_mid, _v, c='black', s=6, alpha=point_alpha * 0.4)
                
                #ax.scatter(row.bin_mid, row.bin_avg_u_2std, c='green', s=6, alpha=point_alpha * 2)
                #ax.scatter(row.bin_mid, row.bin_avg_l_2std, c='green', s=6, alpha=point_alpha * 2)
            
                
                if(len(underlines.values) > 2):
                    
                    #density 
                    bin_density = gaussian_kde(underlines.values)
                    dys = np.linspace(-3 * row.bin_vol, 3 * row.bin_vol, 200)                         
                    bin_density.set_bandwidth(bw_method=bin_density.factor / 2.)                    
                    bin_density_vals = bin_density.evaluate(dys) /np.max(bin_density.evaluate(dys))                    
                    dxs = row.bin_mid + bin_density_vals  * (row.bin_r_edge - row.bin_l_edge)                          
                    ax.plot(dxs, dys, lw=0.5, c='coral')
                    
                    #stdevs 
                    ax.plot([row.bin_mid, row.bin_mid + row.bin_r_edge - row.bin_l_edge], [coefficient_u_1std, coefficient_u_1std], lw= 0.6,  c='blue', ls = ':')            
                    ax.plot([row.bin_mid, row.bin_mid + row.bin_r_edge - row.bin_l_edge], [coefficient_l_1std, coefficient_l_1std], lw= 0.6,  c='blue', ls = ':')                                        
                    ax.plot([row.bin_mid, row.bin_mid + row.bin_r_edge - row.bin_l_edge], [coefficient_u_2std, coefficient_u_2std], lw= 0.6,  c='blue', ls = ':')            
                    ax.plot([row.bin_mid, row.bin_mid + row.bin_r_edge - row.bin_l_edge], [coefficient_l_2std, coefficient_l_2std], lw= 0.6,  c='blue', ls = ':')
                    
       
        #plot the current data
        (current_level, current_bin) = self.current_details
        ax.scatter(current_bin.bin_mid, current_bin.bin_avg, c='red', alpha=0.2, s = 120)
        
        # debug =>
        #print (current_level)
        #print (current_bin.bin_mid, current_bin.bin_avg)
        #print(self.bin_diagnosis)
        # debug =<

        ax.annotate('@%.4f' % (current_level), (current_bin.bin_mid, current_bin.bin_avg), 
                    xytext=(-12, -12), xycoords='data', textcoords='offset points', fontsize=8) #6->8
        

        ds = self.bin_diagnosis[['bin_mid', 'bin_avg']].dropna()
        
        
       
        #plot the linear fit of the bin's coefficients
        if fit_1std:
            (b, a) = np.polyfit(list(ds.bin_mid.values), list(ds.bin_avg.values), deg = 1)
            fitted_ys = [round(a + b * x, 4) for x in list(ds.bin_mid.values)]
            ax.plot(list(ds.bin_mid.values), fitted_ys, lw=0.7, c='g', ls = ':')
        
        #plot the 2nd degree ploy fit of the bin's coefficients

        
        if fit_2ndd:
            (c, b, a) = np.polyfit(list(ds.bin_mid.values), list(ds.bin_avg.values), deg = 2)
            fitted_ys = [a + b * x + c * x * x  for x in list(ds.bin_mid.values)]
            ax.plot(list(ds.bin_mid.values), fitted_ys, lw=0.5, c='darkgreen', ls = '--')
        
        
        #plot OU implied drift and vol coefficients
        
        _fitted_unconditional_m_v_func = lambda s: (self.ou.theta_hat * (self.ou.mu_hat - s), self.ou.sigma_hat)
        fitted_ys = [_fitted_unconditional_m_v_func(x)[0] for x in list(ds.bin_mid.values)]
        ax.plot(list(ds.bin_mid.values), fitted_ys, lw=0.7, c='orangered', ls = ':')
        

        
        ''' replaced by actual std from the bin

        fitted_ys = [_fitted_unconditional_m_v_func(x)[0] + _fitted_unconditional_m_v_func(x)[1] 
                     for x in list(ds.bin_mid.values)]
        ax.plot(list(ds.bin_mid.values), fitted_ys, lw=0.5, c='coral', ls = '--')
        fitted_ys = [_fitted_unconditional_m_v_func(x)[0] - _fitted_unconditional_m_v_func(x)[1] 
                     for x in list(ds.bin_mid.values)]
        ax.plot(list(ds.bin_mid.values), fitted_ys, lw=0.5, c='coral', ls = '--')
        
        '''
                
              
        
        
        
        
        '''
        ax_twinx = ax.twinx()
        ax_twinx.set_ylim(0, self.bin_diagnosis.bin_size.dropna().sum())
        
        #plot the sizes of the bins
        bar_width = 0.01
        bars = ax_twinx.bar(list(self.bin_diagnosis.bin_mid.values), list(self.bin_diagnosis.bin_size.values), bar_width, color='lightgrey')
        for bar in bars:
            height = bar.get_height()
            ax_twinx.text(bar.get_x() + bar.get_width()/2., 1.01*height, '%d' % int(height), ha='center', va='bottom', color='dimgrey',fontsize=6)
        
        ax_twinx.set_ylabel('bin sample size', fontsize=8)
        plt.setp(ax_twinx.yaxis.get_majorticklabels(), fontsize = 6)  
        
        
        #ax.set_xticks(major_ticks) 
        #ax.xaxis.set_major_locator(ticker.FixedLocator(major_ticks))
        #ax.set_xlim(self.bin_diagnosis.iloc[0,:].bin_l_edge, self.bin_diagnosis.iloc[-1,:].bin_r_edge)
        
        plt.setp(ax.get_xticklabels(), visible=True)
        '''

    def print_mr_stats_notes_ax(self, ax):

        ax.axis('off')
        font = {'color': 'black', 'weight': 'light', 'size': 8}

        conditional_expected_val = r"Conditional expected value: " + r"$x_0e^{ \theta t} + \mu (1 - e^{ \theta t})$"
        conditional_variance = r"Conditional variance: " + r"$\frac{ \sigma ^2(1 - e^{-2 \theta t} )}{2 \theta}$"
        mu_txt =  r"$\hat \mu: $ mean estimation [{0:.2f}]".format(self.ou.mu_hat)
        x0_txt = r"$x_0:$ most recent history [{0:.2f}]".format(self.ou._history_data[-1])
        t_txt = '%s days simulated' % len(self.ou.moments.index) 
        theta_txt = r"$\hat \theta: $ mean reversion speed estimation [{0:.2f}]".format(self.ou.theta_hat)
        
        sigma_txt =  r"$\hat \sigma: $ vol estimation [{0:.2f}]".format(self.ou.sigma_hat)
        half_life_txt =  r"$\hat \lambda: $ half life estimation [{0:.2f}]".format(self.ou.half_life)

        #pnl_detail_txt = r'pre cost pnl potential: [{0:.2f}]'.format(abs(self.ou.data[-1] - self.ou.conditional_means[-1]))
        pnl_detail_txt = r'pnl cond/uncond: [{0:.2f}]/[{1:.2f}] '.format(abs(self.ou.history_data[-1] - self.ou.conditional_means[-1]), abs(self.ou.history_data[-1] - self.ou.mu_hat))


        #trade_detail_txt = r'[{}] the weighted series'.format('Receive' if self.ou.data[-1] > self.ou.conditional_means[-1] else 'Pay')
        trade_detail_txt = r'[{}] the weighted series'.format('Receive' if self.ou.history_data[-1] > self.ou.theta_hat else 'Pay')


        #ax_note.text(-0.01, 1, "Solutions to conditional moments: ", fontdict=font_group_title, verticalalignment = "top")
        ax.text(-0.01, 0.9, conditional_expected_val, fontdict=font, verticalalignment = "top")
        #ax.text(0.2, 0.94, conditional_expected_val_form, fontdict=font,fontsize =7, verticalalignment = "top")
        ax.text(-0.01, 0.7, conditional_variance, fontdict=font, verticalalignment = "top")
        #ax.text(0.2, 0.74, conditional_variance_form, fontdict=font, fontsize = 7, verticalalignment = "top")

        ax.text(-0.01, 0.5, mu_txt, fontdict=font, verticalalignment = "top")
        ax.text(-0.01, 0.25, x0_txt, fontdict=font, verticalalignment = "top")
        ax.text(-0.01, 0, theta_txt, fontdict=font, verticalalignment = "top")
        
        ax.text(0.6, 0.9, t_txt, fontdict=font, verticalalignment = "top")
        ax.text(0.6, 0.7, sigma_txt, fontdict=font, verticalalignment = "top")
        ax.text(0.6, 0.5, half_life_txt, fontdict=font, verticalalignment = "top")
        ax.text(0.6, 0.3, pnl_detail_txt, fontdict=font, verticalalignment = "top")
        ax.text(0.6, 0.1, trade_detail_txt, fontdict=font, verticalalignment = "top")
        
        
    def plot_distribution_diagnosis(self, figsize):
        
        
        f_mr = plt.figure(figsize=figsize)

        gs_columns = 1
        
        gs = gridspec.GridSpec(1, gs_columns, height_ratios= [1], width_ratios= [1])
    
        gs_l = gridspec.GridSpecFromSubplotSpec(2, 1, height_ratios= [8, 1], subplot_spec=gs[0,0], hspace = 0.1)
       

        #gs_l
        ax_bin_diag = plt.subplot(gs_l[0])
        ax_bin_size = plt.subplot(gs_l[1])
        
        self.plot_bin_diagnosis_ax(ax_bin_diag)
        self.plot_bin_diagnosis_bin_size_ax(ax_bin_size)
        
        f_mr.tight_layout()
        
        return f_mr


    def create_mr_info_fig(self, figsize, pca_obj = None):
        
        # figuresize (16, 7)
        # figuresize (9, 7)

        f_mr = plt.figure(figsize=figsize)

        gs_columns = 2 if pca_obj is None else 3
        gs_width_ratios = [3, 3] if pca_obj is None else [3, 3, 3]

        gs = gridspec.GridSpec(1, gs_columns, height_ratios= [1], width_ratios= gs_width_ratios)
    
        gs_l = gridspec.GridSpecFromSubplotSpec(3, 1, height_ratios= [6, 1, 3], subplot_spec=gs[0,0], hspace = 0.1)
        gs_m = gridspec.GridSpecFromSubplotSpec(2, 2, height_ratios= [2, 1], width_ratios= [1, 1], subplot_spec=gs[0,1])
        

        #gs_r
        if pca_obj is not None:
            gs_r = gridspec.GridSpecFromSubplotSpec(3, 2, height_ratios= [1, 1, 1], subplot_spec=gs[0,2])
            ax_pca_u = plt.subplot(gs_r[0,:])
            ax_pca_m = plt.subplot(gs_r[1,:])
            ax_pca_dl = plt.subplot(gs_r[2,0])
            ax_pca_dr = plt.subplot(gs_r[2,1])
            
            pca_obj.plot_hedged_weighted_data_ax(ax_pca_u)
            pca_obj.plot_pcs_ax(ax_pca_m)
            pca_obj.plot_loadings_ax(ax_pca_dl)
            pca_obj.plot_loadings_lambda_ax(ax_pca_dr)


        #gs_l
        ax_bin_diag = plt.subplot(gs_l[0])
        ax_bin_size = plt.subplot(gs_l[1])
        ax_mr_notes = plt.subplot(gs_l[2])
        
        self.plot_bin_diagnosis_bin_size_ax(ax_bin_size)
        self.plot_bin_diagnosis_ax(ax_bin_diag)
        self.print_mr_stats_notes_ax(ax_mr_notes)

        #gs_m
        ax_mr_exp_moments = plt.subplot(gs_m[0,:])
        ax_mr_exp_sharpe_ratio =  plt.subplot(gs_m[1,0])
        ax_mr_hit_stop_distribution =  plt.subplot(gs_m[1,1])

        # ou.plot_ou_moments_ax will pick one random simulated path from ou._simulated_paths
        # these paths are generated by simulate_with_model_parameters from
        self.ou.plot_ou_moments_ax(ax_mr_exp_moments)
        self.ou.plot_ou_sr(ax_mr_exp_sharpe_ratio)
        self.ou.plot_hitstop_distribution(ax_mr_hit_stop_distribution)

        f_mr.tight_layout()

        if pca_obj is not None:
            #fixing shits here
            for tk in ax_pca_u.get_xticklabels():
                tk.set_visible(True)

            for tk in ax_pca_m.get_xticklabels():
                tk.set_visible(True)

        return f_mr



    def create_markov_info_fig(self, figsize, horizons=[5,10,100]):
        
        # figuresize (16, 7)
        # figuresize (9, 7)

        f_mr = plt.figure(figsize=figsize)

        gs_columns = 2 
        gs_width_ratios = [1, 1] 

        gs = gridspec.GridSpec(1, gs_columns, height_ratios= [1], width_ratios= gs_width_ratios)
    
        gs_l = gridspec.GridSpecFromSubplotSpec(2, 1, height_ratios= [3, 2], subplot_spec=gs[0,0], hspace = 0.1)
        gs_m = gridspec.GridSpecFromSubplotSpec(2, 1, height_ratios= [3, 2], width_ratios= [1], subplot_spec=gs[0,1], hspace = 0.1)
        

        

        #gs_l
        ax_bin_diag = plt.subplot(gs_l[0])        
        ax_mr_exp_moments = plt.subplot(gs_l[1])
        
        self.plot_bin_diagnosis_ax(ax_bin_diag)
        self.ou.plot_ou_moments_ax(ax_mr_exp_moments)
        
        #gs_m
        ax_markov_matrix = plt.subplot(gs_m[0])  
        ax_markov_distribution = plt.subplot(gs_m[1])
        self.print_markov_states_matrix_ax(ax_markov_matrix)
        self.plot_markov_distribution_ax(ax_markov_distribution, horizons=horizons)
        

        f_mr.tight_layout()

        return f_mr
    
    def plot_markov_distribution_ax(self, ax, horizons=[5,10,100]):
        
        horizon_value_distributions = self.markvo_forecast_horizon_value(ns=horizons)
        
        horizon_value_distributions = horizon_value_distributions.rename(lambda x: f'{x} day markov chain states', axis=1)
        
        horizon_value_distributions.plot(ax=ax)
        
        leg = ax.legend(fancybox=True, prop={'size': 7}, ncol=1)
        leg.get_frame().set_facecolor('none')
        leg.get_frame().set_linewidth(0.0)
        
        plt.setp(ax.xaxis.get_minorticklabels(), fontsize = 6)
        plt.setp(ax.xaxis.get_majorticklabels(), fontsize = 6)
        plt.setp(ax.yaxis.get_majorticklabels(), fontsize = 6)
        
        ax.xaxis.grid(True, linestyle='-', linewidth='0.2')
        ax.yaxis.grid(True, linestyle='-', linewidth='0.2')
        
        ax.axhline(y=0, color='grey', linestyle='--',  lw=0.7) 
        
        
        
    
   
    def print_markov_states_matrix_ax(self, ax):
    
        tbl_data = (self.transition_density_matrix * 100).round(0).replace(0, '')
        col_labels = ['{:.2f}'.format(l) for l in tbl_data.columns]
        row_labels = col_labels
        print('yeah 1')
        table = ax.table( 
            cellText = tbl_data.to_numpy(),  
            rowLabels = col_labels,  
            colLabels = row_labels, 
            #rowColours =["palegreen"] * 10,  
            #colColours =["palegreen"] * 10,
            cellLoc ='center',  
            loc ='upper left')  
            
        table.auto_set_font_size(False)
        table.set_fontsize(6)
        #table.scale(1.1, 1.1)
        ax.set_axis_off() 
        
        ax.set_title('conditional transitional matrix') 
        
        for k, cell in table.get_celld().items():
            cell.set_edgecolor('#CDCDCD')
            if k[0] == 0 or k[1] < 0:
                cell.set_text_props(weight='bold', color='w')
                cell.set_facecolor('#000000')
            else:
                cell.set_facecolor('w')
                
            
        
        
        
        
        
    
'''##############'''    
'''UNIT TEST HERE'''
'''##############'''  
if(2 > 3):
    import sys
    import pandas as pd
    pd.set_option('display.expand_frame_repr', False)
    _p = 'C:\Dev\mancave\QuantModels'
    if _p not in sys.path: sys.path.insert(0, _p)
    _p = 'C:\Dev\mancave\QuantModels\statistics'
    if _p not in sys.path: sys.path.insert(0, _p)
    _p = 'C:\Dev\mancave\QuantModels\statistics\mr'
    if _p not in sys.path: sys.path.insert(0, _p)
    _p = 'C:\Dev\mancave\QuantModels\statistics\derivative\pysabr'
    if _p not in sys.path: sys.path.insert(0, _p)
    _p = 'C:\Dev\mancave\QuantModels\core'
    if _p not in sys.path: sys.path.insert(0, _p)
    
    from statistics.factor import pca
    import stochastic as st
    # from statistics.mr import bins
    # from statistics.mr import stochastic as st
    # from statistics.mr import helper
    # from statistics.derivative import bs as bs
    # from statistics.derivative import quantlibutils as qlutils
    # from statistics.derivative.pysabr import hagan_2002_normal_sabr as nsabr
    # from statistics.mr import tools as mrt
    # from core import dirs
    from core import tools as ctls
    
    swap_data_all = ctls.tframe(pd.read_csv('./statistics/mr/data/SWAP_ALL_V2_historical.csv'), key='date').ffill()

    ussw10 = swap_data_all['USD_0Y_10Y']
    ussw530 = swap_data_all['USD_0Y_30Y'] - swap_data_all['USD_0Y_5Y']
    ussw530.name = 'ussw530'
    usca55 = swap_data_all[['USD_5Y_5Y', 'CAD_5Y_5Y']].dropna().resample('B').last()
    
    
    
    usca55_pca = pca.PCA(usca55,rates_pca=True, sample_range=('2012-01-01', '2019-12-01'))
    # check nan ussw10.iloc[-1000:].isnull().values.sum()
    stochastic_obj = create_mr_diagnosis(usca55_pca.pc_hedge_weighted_data, 20, resample_freq = 'B')



    #to create a markov 
    npnparam_sim_result = stochastic_obj.estimate_conditional_transition_denisty(n=30,k=150,dt=1)
    stochastic_obj.create_markov_info_fig((16, 8),  horizons=[5,10,100])
    
    stochastic_obj.create_mr_info_fig((18, 8),pca_obj=usca55_pca)
 


if(2 > 3):
    
    # no need to use bins inside the module
    
    create_mr_diagnosis((swap_data_all['USD_0Y_30Y'] - swap_data_all['USD_0Y_5Y']).iloc[-5000:], 20, min_num_of_bins = 20, resample_freq = 'W').plot_distribution_diagnosis((14, 8))
    
    
    create_mr_diagnosis((swap_data_all['USD_5Y_5Y'] - swap_data_all['CAD_5Y_5Y']).iloc[-3000:], 20, min_num_of_bins = 20, resample_freq = 'B').plot_distribution_diagnosis((14, 8))
    
    
    
   
    
    
   


