from __future__ import division
import math
import itertools
import numpy as np
import pandas as pd
import statsmodels.tsa.stattools as ts
import functools
from core import tools



def calc_next_n_periods_moves(vals, n=1): 
    return (vals.shift(-1 * n) - vals).reindex(vals.index)

def calc_prev_n_periods_moves(vals, n=1):
    return (- vals.shift(1 * n) + vals).reindex(vals.index)


def find_bin(bounds, val):
    ''' find the bin where the val belong
        bounds are a dataframe with 2 columns, col 0 left edge and col 1 right edge
    '''
    print(val)
    lr = bounds
    lr.iloc[0].iloc[0] = float(-np.inf)
    lr.iloc[-1].iloc[-1] = float(np.inf)
    return lr[(lr.iloc[:,0] < val) & (lr.iloc[:,1] >= val)].index[0]
    

# lr
# Out[158]: 
#         bin_l_edge  bin_r_edge
# bin_id                        
# 0        -0.879502   -0.813400
# 1        -0.813400   -0.747298
# 2        -0.747298   -0.681196
# 3        -0.681196   -0.615094
# 4        -0.615094   -0.548992
# 5        -0.548992   -0.482891
# 6        -0.482891   -0.416789
# 7        -0.416789   -0.350687
# 8        -0.350687   -0.284585
# 9        -0.284585   -0.218483
# 10       -0.218483   -0.152381
# 11       -0.152381   -0.086279
# 12       -0.086279   -0.020177
# 13       -0.020177    0.045925
# 14        0.045925    0.112027
# 15        0.112027    0.178129
# 16        0.178129    0.244231
# 19        0.376434    0.442536

# find_bin(lr,-10)
# Out[161]: 0

# find_bin(lr,-0.1)
# Out[162]: 11


def create_transition_matrix(idx, data, as_pct=True):
    '''
    idx is a list like thing holding the transition state name or id
    data is a dataframe. each col of data is a sample path. each row of data
    is a state. so from row 1 to row 2 for col1 is a state transition event for
    sample path col1.
    '''
    output = pd.DataFrame(index = idx, columns = idx).fillna(0)
    output = output.rename_axis('row', axis=0)
    output = output.rename_axis('col', axis=1)

    for c in data.columns:
        ds = data[c].values
        #create the pair sequence of old -> new state
        for _old, _new in zip(ds, ds[1:]):
            output.iloc[_old, _new] = output.iloc[_old, _new] + 1 
            if (as_pct):
                #the output row index is the from state the column index is to to state
                #to calc % needs to each row for that row (or starting state) and divide
                #the row by the row sum
                #its easier to transpose the df and do it as col divided by cols sum and 
                #transpose back
                output = (output.transpose()/output.transpose().sum(axis=0)).transpose().fillna(0)            
    return output
    
# states_hist = npnparam_sim_result[0].applymap(lambda x: find_bin(lr,x))
# create_transition_matrix(lr.index, states_hist)




def find_neighbours_in_series(ds, x, number_of_neighbour):
    """ find n number of neighbours around x in ds
    """
    xs = ds.dropna().index.values # always in assending order
    ys = ds.dropna().values

    l = len(xs)
    n = math.ceil(number_of_neighbour/2)
    
    if n * 2 > l:
        raise RuntimeError("not enough data to locate {} neighbours".format(number_of_neighbour))

    left = []
    right = []

    # 
    if x <= xs[0]:
        return ds.loc[xs[0:int(n*2)]]
    #    
    if x >= xs[-1]:
        return ds.loc[xs[int(-1*n*2):]]

    #
    for i in np.arange(0, l, step=1):
        if xs[i-1] <= x and x <= xs[i]:
            left = xs[:i][int(-1 * n):]
            right = xs[i:][:int(n)]
            if len(left) < n:
                right =  xs[i:][:int(n + n - len(left))]
            elif len(right) < n:
                left = xs[:i][int(-1 * (n + n - len(right))):]
            break

    return ds.loc[np.append(left, right)]



def calc_momentum(vals):
    if np.count_nonzero(np.isnan(vals)) == 0:
        return (vals[-1] - vals[0])/len(vals)
    else:
        return 0

def create_hit_test(x0, level):
    if(level is None):
        return lambda val: (False, None)
    elif (level >= x0):
        return lambda val: (level <= val, level)
    elif(level < x0):
        return lambda val: (level >= val, level)





def calc_hurst_exp(ts):
	# Create the range of lag values
	lags = range(2, 100)
	# Calculate the array of the variances of the lagged differences
	tau = [np.sqrt(np.std(np.subtract(ts[lag:], ts[:-lag]))) for lag in lags]
	# Use a linear fit to estimate the Hurst Exponent
	poly = np.polyfit(np.log(lags), np.log(tau), 1)
	# Return the Hurst exponent from the polyfit output
	return poly[0]*2.0


def calc_weighted_risk_cix(data, risks):
    for risk, legs in risks.items():
        cix_components = {}
        #for sec, weight in legs.iteritems():
        for sec, weight in legs.items():
           cix_components[sec] = data[sec] * weight
           #print sec, weight
        cix = pd.DataFrame(cix_components).mean(axis=1)
        cix.name = risk
        yield (risk, cix)
