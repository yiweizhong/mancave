#pragma once
#include "stdafx.h"
#include "CallOption.h"

using std::vector;
using std::shared_ptr;

class HedgingSimulator {

public:
	/*  Default constructor */
	HedgingSimulator();
	/*  Runs a number of simulations and returns
		a vector of the profit and loss */
	vector<double> runSimulations(int nSimulations) const;
	void setOptionToHedge(shared_ptr<CallOption> option) {
		this->optionToHedge = option;
	}
	void setSimulationModel(shared_ptr<BlackScholesModel> model) {
		this->simulationModel = model;
	}
	void setPricingModel(shared_ptr<BlackScholesModel> model) {
		this->pricingModel = model;
	}
	void setNsteps(int nSteps) {
		this->nSteps = nSteps;
	}


private:
	/*  The option that has been written */
	shared_ptr<CallOption> optionToHedge;
	/*  The model used to simulate stock prices */
	shared_ptr<BlackScholesModel> simulationModel;
	/*  The model used to price the option and calculate the delta*/
	shared_ptr<BlackScholesModel> pricingModel;
	/*  The number of steps to use */
	int nSteps;

	double simulateOnce() const;
	/*  How much should we charge the customer for the option aka 
	price the option at the beginning  */
	double chooseCharge(double stockPrice) const;
	/*  Hoe much stock should we hold as a delta hedge */
	double selectStockQuantity(double hedgingTimeStop, double stockPrice) const;

};

void testHedgingSimulator();