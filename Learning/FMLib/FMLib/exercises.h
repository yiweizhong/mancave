#pragma once
#include "stdafx.h"

void testChapter7Excercise();


class QuardraticRoots {
private:
	double _root1;
	double _root2;
public:
	const double root1();
	const double root2();
	QuardraticRoots(double root1, double root2);
};
