#include "matlib.h"
#include "LineChart.h"
#include "Histogram.h"
#include <random>
#include <cstdlib>
#include <exception>
#include <numeric>


using std::vector;
using std::accumulate;
using std::string;

namespace base
{

	NotImplementedException::NotImplementedException() : std::exception{ "Function not yet implemented." } {}

}

const double PI = 3.14159265359;
const double SQRT_2PI = sqrt(2 * PI);


/**
 *  Sort and return a copy of vector of doubles
 */
vector<double> sort(const std::vector<double>& v) {
	vector<double> copy = v;
	std::sort(copy.begin(), copy.end());
	return copy;
}


/**
 *  Find the sum of the elements in an array
 */
double sum(const vector<double>& v) {
	double total = 0.0;
	auto n = v.size();
	for (size_t i = 0; i < n; i++) {
		total += v[i];
	}
	return total;
}


/*  Compute the mean of a vector */
double mean(const vector<double>& v) {
	auto n = v.size();
	ASSERT(n > 0);
	return sum(v) / n;
}

/*  Compute the standard deviation of a vector */
double standardDeviation(const vector<double>& v, bool population) {
	auto n = v.size();
	double total = 0.0;
	double totalSq = 0.0;
	for (int i = 0; i < n; i++) {
		total += v[i];
		totalSq += v[i] * v[i];
	}
	if (population) {
		ASSERT(n > 0);
		return sqrt((totalSq - total * total / n) / n);
	}
	else {
		ASSERT(n > 1);
		return sqrt((totalSq - total * total / n) / (n - 1.0));
	}
}

/*  Find the minimum of a vector */
double min(const vector<double>& v) {
	auto n = v.size();
	ASSERT(n > 0);
	double min = v[0];
	for (int i = 1; i < n; i++) {
		if (v[i] < min) {
			min = v[i];
		}
	}
	return min;
}

/*  Find the maximum of a vector */
double max(const vector<double>& v) {
	auto n = v.size();
	ASSERT(n > 0);
	double max = v[0];
	for (int i = 1; i < n; i++) {
		if (v[i] > max) {
			max = v[i];
		}
	}
	return max;
}


/*  Create a linearly spaced vector */
vector<double> linspaceV(double from, double to, int numPoints) {
	ASSERT(numPoints >= 2);
	vector<double> ret(numPoints, 0.0);
	double step = (to - from) / (numPoints - 1.0);
	double current = from;
	for (int i = 0; i < numPoints; i++) {
		ret[i] = current;
		current += step;
	}
	return ret;
}

double normcdf(double x) {

	double a = 0.319381530;
	double b = -0.356563782;
	double c = 1.781477937;
	double d = -1.821255978;
	double e = 1.330274429;

	auto k = [](double x) { return 1 / (1 + 0.2316419 * x); };
	auto N = [a, b, c, d, e, k](double x) { return 1 - 1 / SQRT_2PI * exp(-x * x / 2) * k(x)*(a + k(x)*(b + k(x)*(c + k(x)*(d + e * k(x))))); };

	return (x >= 0) ? N(x) : 1 - N(-x);
}


static inline double hornerFunction(
	double x,
	double a0,
	double a1) {
	return a0 + x * a1;
}

static inline double hornerFunction(double x, double a0, double a1, double a2) {
	return a0 + x * hornerFunction(x, a1, a2);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3) {
	return a0 + x * hornerFunction(x, a1, a2, a3);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3, double a4) {
	return a0 + x * hornerFunction(x, a1, a2, a3, a4);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3, double a4,
	double a5) {
	return a0 + x * hornerFunction(x, a1, a2, a3, a4, a5);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3, double a4,
	double a5, double a6) {
	return a0 + x * hornerFunction(x, a1, a2, a3, a4, a5, a6);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3, double a4,
	double a5, double a6, double a7) {
	return a0 + x * hornerFunction(x, a1, a2, a3, a4, a5, a6, a7);
}

static inline double hornerFunction(double x, double a0, double a1, double a2, double a3, double a4,
	double a5, double a6, double a7, double a8) {
	return a0 + x * hornerFunction(x, a1, a2, a3, a4, a5, a6, a7, a8);
}

static const double a0 = 2.50662823884;
static const double a1 = -18.61500062529;
static const double a2 = 41.39119773534;
static const double a3 = -25.44106049637;
static const double b1 = -8.47351093090;
static const double b2 = 23.08336743743;
static const double b3 = -21.06224101826;
static const double b4 = 3.13082909833;
static const double c0 = 0.3374754822726147;
static const double c1 = 0.9761690190917186;
static const double c2 = 0.1607979714918209;
static const double c3 = 0.0276438810333863;
static const double c4 = 0.0038405729373609;
static const double c5 = 0.0003951896511919;
static const double c6 = 0.0000321767881768;
static const double c7 = 0.0000002888167364;
static const double c8 = 0.0000003960315187;

double norminv(double x) {
	// We use Moro's algorithm
	double y = x - 0.5;
	if (y<0.42 && y>-0.42) {
		double r = y * y;
		return y * hornerFunction(r, a0, a1, a2, a3) / hornerFunction(r, 1.0, b1, b2, b3, b4);
	}
	else {
		double r;
		if (y < 0.0) {
			r = x;
		}
		else {
			r = 1.0 - x;
		}
		double s = log(-log(r));
		double t = hornerFunction(s, c0, c1, c2, c3, c4, c5, c6, c7, c8);
		if (x > 0.5) {
			return t;
		}
		else {
			return -t;
		}
	}
}

static std::mt19937 mersenneTwister;

static std::mutex rngMutex;


/*  Reset the random number generator.
We ignore the description string */
void rng(const string& description) {
	ASSERT(description == "default");
	std::lock_guard<std::mutex> lock(rngMutex);
	mersenneTwister.seed(std::mt19937::default_seed);
}


/*  Create uniformly distributed random numbers*/

void resetSeed(bool staticSeed) {
	if (staticSeed) {
		mersenneTwister.seed(std::mt19937::default_seed);
	}
	else {
		std::random_device rd;  //Will be used to obtain a seed for the random number engine
		mersenneTwister.seed(rd());
	}
}

vector<double> randUniform(int n) {

	vector<double> ranNum{};

	//std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()	

	std::uniform_real_distribution<> uniformDitribution0_1(0.0, 1);

	for (int i = 0; i < n; i++) {
		auto r = uniformDitribution0_1(mersenneTwister);
		ranNum.push_back(r);
	}
	return ranNum;
}


Matrix randuniform(std::mt19937& mersenneTwister, int rows, int cols) {
	auto rtns  = Matrix(rows, cols, true);

	std::uniform_real_distribution<> uniformDitribution0_1(0.0, 1);

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			rtns(i, j) = uniformDitribution0_1(mersenneTwister);
		}
	}
	return rtns;
}

/*  Generate random numbers */
Matrix randuniform(int rows, int cols) {
	std::lock_guard<std::mutex> lock(rngMutex);
	return randuniform(mersenneTwister, rows, cols);
}


/*  Create normally distributed random numbers*/

vector<double> randNorm(int n) {
	vector<double> ranNum{};
	auto ranUniformedNum = randUniform(n);

	for(const auto r: ranUniformedNum) {
		ranNum.push_back(norminv(r));
	}
	return ranNum;
}

Matrix randNorm(std::mt19937& mersenneTwister, int rows, int cols) {
	Matrix ret = randuniform(mersenneTwister, rows, cols);
	for (int j = 0; j < cols; j++) {
		for (int i = 0; i < rows; i++) {
			ret(i, j) = norminv(ret(i, j));
		}
	}
	return ret;
}

Matrix randNorm(int rows, int cols) {
	std::lock_guard<std::mutex> lock(rngMutex);
	return randNorm(mersenneTwister, rows, cols);
}



///*  MersenneTwister random number generator */
//static std::mt19937 mersenneTwister;
//
//
///*  Create uniformly distributed random numbers using
//	the Mersenne Twister algorithm. See the code above for the answer
//	to the homework excercise which should familiarize you with the C API*/
//vector<double> randuniform(int n) {
//	
//	vector<double> ret(n, 0.0);
//	for (int i = 0; i < n; i++) {
//		ret[i] = (mersenneTwister() + 0.5) /
//			(mersenneTwister.max() + 1.0);
//	}
//	return ret;
//}
//
///*  Create normally distributed random numbers */
//vector<double> randn(int n) {
//	vector<double> v = randuniform(n);
//	for (int i = 0; i < n; i++) {
//		v[i] = norminv(v[i]);
//	}
//	return v;
//}
//






vector<double> mva(int p, vector<double> data) {
	vector<double> _mva{};
	double _mvSum = 0;
	vector<double> _mvSumData{}; 

	for (int i = 0; i < (int) data.size(); ++i) {
		_mvSum += data[i];
		_mvSumData.push_back(data[i]);
		if (i >= p) {
			_mva.push_back(_mvSum / (double)p);
			_mvSum -= data[i - p];
			_mvSumData.clear();
		}
	}
	return _mva;
}

/**
 *  Convenience method for generating plots
 */
void plot(const string& file,
	const Matrix& x,
	const Matrix& y) {
	LineChart lc;
	lc.setSeries(x.asVector(), y.asVector());
	lc.writeAsHTML(file);
}

/**
 *  Convenience method for generating plots
 */
void hist(const string& file,
	const vector<double>& data,
	int numBuckets) {
	Histogram h;
	h.setData(data);
	h.setNumBuckets(numBuckets);
	h.writeAsHTML(file);
}

/*
Better MVA. iterator cant be ++ over its boundary!
*/
//vector<double> mva2(int p, const vector<double> & data) {
//	vector<double> _mva{};
//	auto _left = data.cbegin();
//	auto _right = data.cbegin() + p;
//
//	for (int i = 0; i < data.size() - (p-1); i++) {
//		if (i != 0) {
//			_left++;
//			_right++;
//		}
//		//auto _subset =  vector(_left, _right);
//		_mva.push_back(accumulate(_left, _right, 0)/(double)p);		
//	}
//	return _mva;
//}


///////////////////////////////////////////////;
//   Matrix based functions
///////////////////////////////////////////////

/*  Create a linearly spaced vector */
Matrix linspace(double from, double to, int numPoints, bool rowVector) {
	auto data = linspaceV(from, to, numPoints);
	return rowVector == true ? Matrix(data, false) : Matrix(data, true);
}

/*  Compute the sum of a matrix's rows */
Matrix sumRows(const Matrix& m) {
	vector<double> sums(m.nRows());
	for (auto i = 0; i < m.nRows(); i++) {
		for (auto j = 0; j < m.nCols(); j++) {
			sums[i] += m(i, j);
		}
	}
	return Matrix(sums, true);
}

/*  Compute the sum of a matrix's cols */
Matrix sumCols(const Matrix& m) {
	vector<double> sums(m.nCols());
	for (auto i = 0; i < m.nCols(); i++) {
		for (auto j = 0; j < m.nRows(); j++) {
			sums[i] += m(j, i);
		}
	}
	return Matrix(sums, false);
}


/*  Compute the mean of a matrix's rows */
Matrix meanRows(const Matrix& m) {
	auto means = sumRows(m);
	means *= (1.0 / m.nCols());
	return means;
}

/*  Compute the mean of a matrix's cols */
Matrix meanCols(const Matrix& m) {
	auto means = sumCols(m);
	means *= (1.0 / m.nRows());
	return means;
}

/*  Compute the standard deviation of a matrix's rows */
Matrix stdRows(const Matrix& m, bool population) {

	//create a mean matrix whose dimension is the same as the original matrix
	// and each row contains the mean of that row 
	//need more than 1 col
	ASSERT(m.nCols() > 1);
	//DEBUG_PRINT(m);
	auto meanMatrix = duplicateColVector(meanRows(m), m.nCols());
	//DEBUG_PRINT(meanMatrix);
	auto meanDiff = (m - meanMatrix);
	//DEBUG_PRINT(meanDiff);
	meanDiff.pow(2);
	//DEBUG_PRINT(meanDiff);
	auto meanDiffSqauredSum = sumRows(meanDiff);
	//DEBUG_PRINT(meanDiffSqauredSum);
	if (population) {
		meanDiffSqauredSum *= (1.0 / m.nCols());
	}
	else {
		meanDiffSqauredSum *= (1.0 / (m.nCols()-1.0));
	}
	//DEBUG_PRINT(meanDiffSqauredSum);
	meanDiffSqauredSum.sqrt();
	//DEBUG_PRINT(meanDiffSqauredSum);
	return meanDiffSqauredSum;
}

/*  Compute the standard deviation of a matrix's rows */
Matrix stdCols(const Matrix& m, bool population) {

	//create a mean matrix whose dimension is the same as the original matrix
	// and each row contains the mean of that row 
	//need more than 1 col
	ASSERT(m.nRows() > 1);
	//DEBUG_PRINT(m);
	auto meanMatrix = duplicateRowVector(meanCols(m), m.nRows());
	//DEBUG_PRINT(meanMatrix);
	auto meanDiff = (m - meanMatrix);
	//DEBUG_PRINT(meanDiff);
	meanDiff.pow(2);
	//DEBUG_PRINT(meanDiff);
	auto meanDiffSqauredSum = sumCols(meanDiff);
	//DEBUG_PRINT(meanDiffSqauredSum);
	if (population) {
		meanDiffSqauredSum *= (1.0 / m.nRows());
	}
	else {
		meanDiffSqauredSum *= (1.0 / (m.nRows() - 1.0));
	}
	//DEBUG_PRINT(meanDiffSqauredSum);
	meanDiffSqauredSum.sqrt();
	//DEBUG_PRINT(meanDiffSqauredSum);
	return meanDiffSqauredSum;
}

Matrix minOverRows(const Matrix& m) {

	vector<double> mins(m.nRows(), std::numeric_limits<double>::max());
	for (auto i = 0; i < m.nRows(); i++) {
		for (auto j = 0; j < m.nCols(); j++) {
			if (mins[i] > m(i, j)) {
				mins[i] =  m(i, j);
			}
		}
	}
	return Matrix(mins, true);
}

Matrix maxOverRows(const Matrix& m) {

	vector<double> maxes(m.nRows(), std::numeric_limits<double>::min());
	for (auto i = 0; i < m.nRows(); i++) {
		for (auto j = 0; j < m.nCols(); j++) {
			if (maxes[i] < m(i, j)) {
				maxes[i] = m(i, j);
			}
		}
	}
	return Matrix(maxes, true);
}

Matrix minOverCols(const Matrix& m) {

	vector<double> mins(m.nCols(), std::numeric_limits<double>::max());
	for (auto i = 0; i < m.nCols(); i++) {
		for (auto j = 0; j < m.nRows(); j++) {
			if (mins[i] > m(j, i)) {
				mins[i] = m(j, i);
			}
		}
	}
	return Matrix(mins, false);
}

Matrix maxOverCols(const Matrix& m) {

	vector<double> maxes(m.nCols(), std::numeric_limits<double>::min());
	for (auto i = 0; i < m.nCols(); i++) {
		for (auto j = 0; j < m.nRows(); j++) {
			if (maxes[i] < m(j, i)) {
				maxes[i] = m(j, i);
			}
		}
	}
	return Matrix(maxes, false);
}


/**
 *  Find the given percentile of a distribution
 */
static double prctile(const std::vector<double>& in, double percentage) {
	// See the MATLAB documentation for a specification of what prctile actually does
	// its a little fiddly. The tests were all computed using MATLAB
	ASSERT(percentage >= 0.0);
	ASSERT(percentage <= 100.0);
	int n = in.size();

	vector<double> sorted = in;
	std::sort(sorted.begin(), sorted.end());

	int indexBelow = (int)(n * percentage / 100.0 - 0.5);
	int indexAbove = indexBelow + 1;
	if (indexAbove > n - 1) {
		return sorted[n - 1];
	} if (indexBelow < 0) {
		return sorted[0];
	}
	double valueBelow = sorted[indexBelow];
	double valueAbove = sorted[indexAbove];
	double percentageBelow = 100.0 * (indexBelow + 0.5) / n;
	double percentageAbove = 100.0 * (indexAbove + 0.5) / n;
	if (percentage <= percentageBelow) {
		return valueBelow;
	}
	if (percentage >= percentageAbove) {
		return valueAbove;
	}
	double correction = (percentage - percentageBelow) * (valueAbove - valueBelow) / (percentageAbove - percentageBelow);
	return valueBelow + correction;
}

/**
 *   Return the given percentile on each row
 */
Matrix prctileRows(const Matrix& m, double percentage) {
	Matrix ret(m.nRows(), 1, 0);
	for (int i = 0; i < m.nRows(); i++) {
		vector<double> row = m.row(i).rowVector();
		ret(i) = prctile(row, percentage);
	}
	return ret;
}

/**
 *   Return the given percentile on each column
 */
Matrix prctileCols(const Matrix& m, double percentage) {
	Matrix ret(1, m.nCols(), 0);
	for (int i = 0; i < m.nCols(); i++) {
		vector<double> col = m.col(i).colVector();
		ret(i) = prctile(col, percentage);
	}
	return ret;
}


/*  Matrix transpose */
Matrix transpose(const Matrix& m) {
	Matrix ret(m.nCols(), m.nRows(), true);
	for (auto j = 0; j < m.nCols(); j++) {
		for (auto i = 0; i < m.nRows(); i++) {
			ret(j, i) = m(i, j);
		}
	}
	return ret;
}

/*  Compute the cholesky decomposition */
Matrix chol(const Matrix& A) {
	int n = A.nRows();
	ASSERT(n == A.nCols());
	Matrix L(n, n);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < i; j++) {
			double s = A(i, j);
			for (int k = 0; k < j; k++) {
				s -= L(i, k) * L(j, k);
			}
			L(i, j) = s / L(j, j);
		}
		double s = A(i, i);
		for (int k = 0; k < i; k++) {
			s -= L(i, k) * L(i, k);
		}
		ASSERT(s >= 0); /* A must be positive definite */
		L(i, i) = sqrt(s);
	}
	return L;
}




///////////////////////////////////////////////
//
//   TESTS
//
///////////////////////////////////////////////


static void testMatrixBasedFuctions() {

	{
		auto rv = linspace(1.0, 4.0, 4, true);
		DEBUG_PRINT(">>> Test create row vector using linspace");
		ASSERT(rv.nRows() == 1);
		ASSERT(rv.nCols() == 4);
		ASSERT_APPROX_EQUAL(rv.get(0, 0), 1., 0.0001);
		ASSERT_APPROX_EQUAL(rv.get(0, 1), 2., 0.0001);
		ASSERT_APPROX_EQUAL(rv.get(0, 2), 3., 0.0001);
		ASSERT_APPROX_EQUAL(rv.get(0, 3), 4., 0.0001);
		DEBUG_PRINT("<<< passed");
	}

	{
		auto rv = linspace(1.0, 4.0, 4, false);
		DEBUG_PRINT(">>> Test create col vector using linspace");
		ASSERT(rv.nRows() == 4);
		ASSERT(rv.nCols() == 1);
		ASSERT_APPROX_EQUAL(rv.get(0, 0), 1., 0.0001);
		ASSERT_APPROX_EQUAL(rv.get(1, 0), 2., 0.0001);
		ASSERT_APPROX_EQUAL(rv.get(2, 0), 3., 0.0001);
		ASSERT_APPROX_EQUAL(rv.get(3, 0), 4., 0.0001);
		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test sumRows into a col vector");

		Matrix m("1,2,3;4,5,6");
		Matrix expected = Matrix("6;15");
		auto output = sumRows(m);
		ASSERT(output.nRows() == 2);
		ASSERT(output.nCols() == 1);
		expected.assertEquals(output, 0.001);
		INFO(output);

		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test sumCols into a row vector");

		Matrix m("1,2,3;4,5,6");
		Matrix expected = Matrix("5, 7, 9");
		auto output = sumCols(m);
		ASSERT(output.nRows() == 1);
		ASSERT(output.nCols() == 3);
		expected.assertEquals(output, 0.001);
		INFO(output);

		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test meanRows into a col vector");

		Matrix m("1,2,3;4,5,6");
		Matrix expected = Matrix("2;5");
		auto output = meanRows(m);
		ASSERT(output.nRows() == 2);
		ASSERT(output.nCols() == 1);
		expected.assertEquals(output, 0.001);
		INFO(output);

		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test meanCols into a row vector");

		Matrix m("1,2,3;4,5,6");
		Matrix expected = Matrix("2.5, 3.5, 4.5");
		auto output = meanCols(m);
		ASSERT(output.nRows() == 1);
		ASSERT(output.nCols() == 3);
		expected.assertEquals(output, 0.001);
		INFO(output);

		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test stdRows");

		Matrix m("1,2,3;4,15,16");
		Matrix expected = Matrix("0.81649658092773; 5.4365021434334");
		Matrix expected2 = Matrix("1; 6.6583281184794");

		auto output = stdRows(m, true);
		ASSERT(output.nRows() == 2);
		ASSERT(output.nCols() == 1);
		expected.assertEquals(output, 0.001);
		INFO(output);


		auto output2 = stdRows(m, false);
		ASSERT(output2.nRows() == 2);
		ASSERT(output2.nCols() == 1);
		expected2.assertEquals(output2, 0.001);
		INFO(output2);

		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test stdCols");

		Matrix m("1,4;2,15;3,16");
		Matrix expected = Matrix("0.81649658092773, 5.4365021434334");
		Matrix expected2 = Matrix("1, 6.6583281184794");

		auto output = stdCols(m, true);
		ASSERT(output.nRows() == 1);
		ASSERT(output.nCols() == 2);
		expected.assertEquals(output, 0.001);
		INFO(output);


		auto output2 = stdCols(m, false);
		ASSERT(output2.nRows() == 1);
		ASSERT(output2.nCols() == 2);
		expected2.assertEquals(output2, 0.001);
		INFO(output2);

		DEBUG_PRINT("<<< passed");
	}


	{
		DEBUG_PRINT(">>> Test minOverRows");
		Matrix m("1,2,3;4,5,6");
		Matrix expected = Matrix("1; 4");
		auto output = minOverRows(m);
		ASSERT(output.nRows() == 2);
		ASSERT(output.nCols() == 1);
		expected.assertEquals(output, 0.001);
		INFO(output);
		DEBUG_PRINT("<<< passed");
	}


	{
		DEBUG_PRINT(">>> Test maxOverRows");
		Matrix m("1,2,3;4,5,6");
		Matrix expected = Matrix("3; 6");
		auto output = maxOverRows(m);
		ASSERT(output.nRows() == 2);
		ASSERT(output.nCols() == 1);
		expected.assertEquals(output, 0.001);
		INFO(output);
		DEBUG_PRINT("<<< passed");
	}


	{
		DEBUG_PRINT(">>> Test minOverCols");
		Matrix m("1,2,3;4,5,6");
		Matrix expected = Matrix("1, 2, 3");
		auto output = minOverCols(m);
		ASSERT(output.nRows() == 1);
		ASSERT(output.nCols() == 3);
		expected.assertEquals(output, 0.001);
		INFO(output);
		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test maxOverCols");
		Matrix m("1,2,3;4,5,6");
		Matrix expected = Matrix("4, 5, 6");
		auto output = maxOverCols(m);
		ASSERT(output.nRows() == 1);
		ASSERT(output.nCols() == 3);
		expected.assertEquals(output, 0.001);
		INFO(output);
		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test matrix transpose");
		Matrix m("3,6;2,4;1,2");
		Matrix expected("3,2,1; 6,4,2");
		auto output = transpose(m);
		expected.assertEquals(output, 0.001);
		INFO(output);
		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test matrix chol decomposition");
		Matrix m("3,1,2;1,4,-1;2,-1,5");
		Matrix c = chol(m);
		Matrix product = c * transpose(c);
		m.assertEquals(product, 0.001);
		DEBUG_PRINT("<<< passed");
	}




}


static void testSort() {
	vector<double> a = { 3.,5.6,4 };
	auto b = sort(a);
	

	ASSERT_APPROX_EQUAL(a[0], 3.0, 0.001);
	ASSERT_APPROX_EQUAL(a[1], 5.6, 0.001);
	ASSERT_APPROX_EQUAL(a[2], 4.0, 0.001);

	ASSERT_APPROX_EQUAL(b[0], 3.0, 0.001);
	ASSERT_APPROX_EQUAL(b[1], 4.0, 0.001);
	ASSERT_APPROX_EQUAL(b[2], 5.6, 0.001);

}

static void testMva() {

	auto result = mva(5, vector<double> {10, 11, 22, 12, 13, 23, 12, 32, 12, 3, 2, 22, 32});

	assert(1 == 1);
}

static void testRandUniform() {


	{

		resetSeed(false);
		auto ranNums = randUniform(1000);
		for (const auto& r : ranNums) {
			ASSERT(r >= 0);
			ASSERT(r <= 1);
		}
		assert(ranNums.size() == 1000);

		Histogram h;
		h.setTitle("Uniform distribution");
		h.setData(ranNums);
		h.writeAsHTML("uniformDistributionHistogram.html");

	}

	{
		DEBUG_PRINT(">>> Test generate uniform random numbers");
		rng("default");
		Matrix m = randuniform(1000, 1);
		ASSERT(m.nRows() == 1000);
		ASSERT_APPROX_EQUAL(meanCols(m)(0, 0), 0.5, 0.1);
		ASSERT(maxOverCols(m)(0, 0) < 1.0);
		ASSERT(minOverCols(m)(0, 0) > 0.0);
		DEBUG_PRINT("<<< Passed");

	}

	{
		DEBUG_PRINT(">>> Test generate random Normal numbers");
		rng("default");
		Matrix m = randNorm(10000, 1);
		ASSERT(m.nRows() == 10000);
		ASSERT_APPROX_EQUAL(meanCols(m)(0,0), 0.0, 0.1);
		ASSERT_APPROX_EQUAL(stdCols(m)(0,0), 1.0, 0.1);
		DEBUG_PRINT("<<< Passed");
	}



}


static void testRandNormal() {

	resetSeed(false);
	auto ranNums = randNorm(10000);
	assert(ranNums.size() == 10000);
	Histogram h;
	h.setTitle("Standard Norm distribution");
	h.setData(ranNums);
	h.writeAsHTML("standardNormDistributionHistogram.html");

}


static void testNormcdf() {
	//test tests
	ASSERT(3 > 2);
	// test bounds
	ASSERT(normcdf(0.3) > 0);
	ASSERT(normcdf(0.3) < 1);
	// test extreme values
	ASSERT_APPROX_EQUAL(normcdf(-1e10), 0, 0.001);
	ASSERT_APPROX_EQUAL(normcdf(1e10), 1.0, 0.001);
	// test increasing
	ASSERT(normcdf(0.3) < normcdf(0.5));
	// test symmetry
	ASSERT_APPROX_EQUAL(normcdf(0.3),
		1 - normcdf(-0.3), 0.0001);
	ASSERT_APPROX_EQUAL(normcdf(0.0), 0.5, 0.0001);
	// test inverse
	ASSERT_APPROX_EQUAL(normcdf(norminv(0.3)),
		0.3, 0.0001);
	// test well known value
	ASSERT_APPROX_EQUAL(normcdf(1.96), 0.975, 0.001);
}

static void testNormInv() {
	ASSERT_APPROX_EQUAL(norminv(0.975), 1.96, 0.01);
}

void testMatlib() {

	setDebugEnabled(true);

	TEST(testMatrixBasedFuctions);	
	//TEST(testSort);
	//TEST(testMva);
	//TEST(testRandUniform);
	//TEST(testRandNormal);	
	//TEST(testNormcdf);
	//TEST(testNormInv);

	setDebugEnabled(false);
	
}