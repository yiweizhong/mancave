#pragma once
#include "BlackScholesModel.h"

class Priceable {
public:
	virtual double price(BlackScholesModel& bsm) const = 0;

};
