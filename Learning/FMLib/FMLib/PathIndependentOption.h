#pragma once
class PathIndependentOption {
	/*The purpoe of this interface is to be called by a montecarlo pricer*/
public:
	/*virtual destructor. it actually has the definition {} here. */
	virtual ~PathIndependentOption() {};
	/*a virtual const function = 0 indicatoring it is an interface function*/
	virtual double getMaturity() const = 0;
	/*a virtual const function = 0 indicating it is an interface function*/
	virtual double payoff(double stockAtMaturity) const = 0;

	/*The BS option object does have a security type specific price function
	can analytically price the option. But this is not alway available to all 
	the option security type. It is not part of this interface.*/
};