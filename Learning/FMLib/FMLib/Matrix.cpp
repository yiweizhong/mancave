#include "Matrix.h"
#include "matlib.h"
using std::stringstream;
using std::ostream;


/*constructor*/
Matrix::Matrix():
	nrows(1), ncols(1) {
	int size = nrows * ncols;
	data = new double[size]; //raw pointer
	//data is pointing at the beginning while 
	//the endPointer points to one position after the end of array 
	endPointer = data + size;
	set(0, 0, 0);
}
Matrix::Matrix(int nrows, int ncols, bool zeros):
	nrows(nrows), ncols(ncols) {
	
	int size = nrows * ncols;
	data = new double[size]; //raw pointer
	//data is pointing at the beginning while 
	//the endPointer points to one position after the end of array 
	endPointer = data + size; 
	if (zeros) {
		//memset is a low level of function that is faster than
		//looping!!!!
		memset(data, 0, sizeof(double) * size);
	}
}



Matrix::Matrix(std::vector<double> values, bool colMajor)
{
	if (colMajor) {
		nrows = values.size();
		ncols = 1;
	}
	else {
		nrows = 1;
		ncols = values.size();
	}
	int size = nrows * ncols;
	data = new double[size]; //raw pointer
	endPointer = data + size;
	for (int i = 0; i < size; i++) {
		data[i] = values[i];
	}
}

Matrix::Matrix(std::vector<double> values, bool colMajor, int nrows, int ncols):
	nrows(nrows), ncols(ncols) {

	int size = nrows * ncols;
	data = new double[size]; //dont know why
	endPointer = data + size; //really dont know why c style array is needed here 

	if (colMajor) {
		int k = 0;
		for (int j = 0; j < ncols; j++) {
			for (int i = 0; i < nrows; i++) {
				data[offset(i, j)] = values[k++];
			}
		}
	}
	else {
		int k = 0;
		for (int i = 0; i < nrows; i++) {
			for (int j = 0; j < ncols; j++) {
				data[offset(i, j)] = values[k++];
			}
		}
	}
}


/*Matrix m("1,2,3; 4, 5, 6") This constructor will take ; seperated section as row not column*/
Matrix::Matrix(std::string const& values)
{
	char separator;
	
	// read once to compute the size
	nrows = 1;
	ncols = 1;
	//get the toal length of the values string
	int n = values.size();

	//loop through the content to update the nrows and ncols
	for (int i = 0; i < n; i++) {
		//; is the end of a row 
		if (values[i] == ';') {
			nrows++; //increase the nrows
		}
		//there is no need to recount the number of columns after doing so for row 1
		if (nrows == 1 && values[i] == ',') {
			ncols++;
		}
	}

	// now check we can read the string
	stringstream ss1;
	ss1.str(values);
	for (int i = 0; i < nrows; i++) {
		for (int j = 0; j < ncols; j++) {
			double ignored;
			ss1 >> ignored;
			ss1 >> separator; //stringstream >> can be called even after reaching the end. it is just not reading in anything
			if (j == ncols - 1 && i < nrows - 1) {
				ASSERT(separator == ';');
			}
			else if (j < ncols - 1) {
				ASSERT(separator == ',');
			}
		}
	}

	// allocate memory now we know nothing will go wrong
	stringstream ss;
	ss.str(values);
	int size = nrows * ncols;
	data = new double[size];
	endPointer = data + size;
	for (int i = 0; i < nrows; i++) {
		for (int j = 0; j < ncols; j++) {
			double* p = begin() + offset(i, j);
			ss >> (*p);
			ss >> separator;
		}
	}
}

/**
 *  Assign all the member variables of this matrix
 *  so that they match another matrix
 */
void Matrix::assign(const Matrix& other) {
	nrows = other.nrows;
	ncols = other.ncols;
	int size = nrows * ncols;
	data = new double[size];
	endPointer = data + size;
	//memcpy(data, other.data, sizeof(double) * size);
	std::copy(other.data, other.data + size, data);
}

/**
 *  Assign all the member variables of this matrix
 *  so that they match another matrix
 */
void Matrix::steal(Matrix& other) noexcept {
	nrows = other.nrows;
	ncols = other.ncols;
	int size = nrows * ncols;
	data = other.data;
	endPointer = data + size;
	//
	other.data = nullptr;
	other.endPointer = nullptr;
}



vector<double> Matrix::rowVector() const
{
	ASSERT(nrows == 1);
	vector<double> rtns(ncols);
	for (int i = 0; i < ncols; i++) {
		rtns[i] = (*this)(0, i);
	}
	return rtns;
}


vector<double> Matrix::colVector() const
{
	ASSERT(ncols == 1);
	vector<double> rtns(nrows);
	for (int i = 0; i < nrows; i++) {
		rtns[i] = (*this)(i, 0);
	}
	return rtns;
}

vector<double> Matrix::asVector() const
{
	int k = 0;
	vector<double> rtns(nrows * ncols);
	for (int i = 0; i < nrows; i++) {
		for (int j = 0; j < ncols; j++) {
			rtns[k++] = (*this)(i, j);
		}
	}
	return rtns;
}

Matrix Matrix::row(int row) const {
	vector<double> rtns(this->nCols());
	for (auto j = 0; j < this->nCols(); j++) {
		rtns[j] = (*this)(row, j);
	}
	return Matrix(rtns, false);
}


Matrix Matrix::col(int col) const {
	vector<double> rtns(this->nRows());
	for (auto i = 0; i < this->nRows(); i++) {
		rtns[i] = (*this)(i, col);
	}
	return Matrix(rtns, true);
}



void Matrix::assertEquals(const Matrix& other, double tolerance)
{
	ASSERT(other.nRows() == nRows());
	ASSERT(other.nCols() == nCols());
	for (int i = 0; i < nrows; i++) {
		for (int j = 0; j < ncols; j++) {
			double expected = (*this)(i, j);
			double actual = other(i, j);
			if (fabs(expected - actual) > tolerance) {
				stringstream s;
				s << "ASSERTION FAILED\n";
				s << "Mismatch at index " << i << ", " << j << ". ";
				s << "Expected " << expected << ", actual " << actual << "\n";
				//s << "this= " << (*this) << "\n";
				//s << "other=" << other << "\n";
				INFO(s.str());
				throw std::runtime_error(s.str());
			}
		}
	}

}



//########################################################
//### member math functions
//########################################################


void Matrix::apply(function<void(double&)> f)
{
	double* dest = this->begin();
	double* end = this->end();
	while (dest != end) {
		f(*dest);
		dest++;
	}
}

void Matrix::apply(function<void(double&, const double&)> f, const Matrix& other)
{
	//* check dimensions
	ASSERT(this->nRows() == other.nRows());
	ASSERT(this->nCols() == other.nCols());

	double* dest = this->begin();
	double const* s2 = other.begin();
	double const* end = other.end();

	while (s2 != end) {
		f(*dest, *s2);
		dest++;
		s2++;
	}
}

void Matrix::exp()
{
	auto lambda = [](double& x) {
		x = std::exp(x);
	};
	apply(lambda);
}
void Matrix::sqrt()
{
	auto lambda = [](double& x) {
		x = std::sqrt(x);
	};
	apply(lambda);
}
void Matrix::log()
{
	auto lambda = [](double& x) {
		x = std::log(x);
	};
	apply(lambda);
}
/*  Take the positive part of every element in the matrix */
void Matrix::positivePart() {
	for (double* p = begin(); p != end(); p++) {
		double val = *p;
		*p = (val > 0.0) ? val : 0.0;
	}
}
/*  Take the negative part of every element in the matrix */
void Matrix::negativePart() {
	for (double* p = begin(); p != end(); p++) {
		double val = *p;
		*p = (val < 0.0) ? val : 0.0;
	}
}
void Matrix::pow(const Matrix& powers)
{
	auto lambda = [](double& x, const double& y) {
		x = std::pow(x, y);
	};
	apply(lambda, powers);
}

void Matrix::pow(double power)
{
	auto lambda = [&power](double& x) {
		x = std::pow(x, power);
	};
	apply(lambda);
}


void Matrix::times(const Matrix& other)
{
	auto lambda = [](double& x, const double& y) {
		x = x * y;
	};
	apply(lambda, other);
}


/*  Scalar multiplication */
Matrix& Matrix::operator*=(double scalar) {
	auto lambda = [&scalar](double& x) {
		x = x * scalar;
	};
	apply(lambda);
	return *this;
}

/*  Scalar addition */
Matrix& Matrix::operator+=(double scalar) {
	auto lambda = [&scalar](double& x) {
		x = x + scalar;
	};
	apply(lambda);
	return *this;
}

/*  Scalar subtraction */
Matrix& Matrix::operator-=(double scalar) {
	auto lambda = [&scalar](double& x) {
		x = x - scalar;
	};
	apply(lambda);
	return *this;
}

/*  Addition */
Matrix& Matrix::operator+=(const Matrix& other) {
	auto lambda = [](double& x, const double& y) {
		x = x + y;
	};
	apply(lambda, other);
	return *this;
}

/*  Subtraction */
Matrix& Matrix::operator-=(const Matrix& other) {
	auto lambda = [](double& x, const double& y) {
		x = x - y;
	};
	apply(lambda, other);
	return *this;
}

Matrix Matrix::cumsum(bool byCol) const
{
	Matrix ret(this->nrows, this->ncols, true);
	
	if (byCol)
	{
		for (int j = 0; j < ncols; j++)
		{
			for (int i = 0; i < nrows; i++)
			{
				if (i == 0) {
					ret(i, j) = data[offset(i, j)];
				}
				else {
					ret(i, j) = ret(i - 1, j) + data[offset(i, j)];
				}
			}
		}		
	}
	else {
		for (int i = 0; i < nrows; i++)
		{
			for (int j = 0; j < ncols; j++)
			{
				if (j == 0) {
					ret(i, j) = data[offset(i, j)];
				}
				else {
					ret(i, j) = ret(i, j - 1) + data[offset(i, j)];
				}
			}
		}

	}

	return ret;

}




//########################################################
//### None member functions
//########################################################

/*duplicate the column n times for a col vector*/
Matrix duplicateColVector(const Matrix& m, int n) {
	ASSERT(m.nCols() == 1);
	Matrix ret = Matrix(m.nRows(), n, 0); //placeholder
	for (auto i = 0; i < ret.nRows(); i++)
	{
		for (auto j = 0; j < ret.nCols(); j++) {
			ret(i, j) = m(i, 0);
		}
	}
	return ret;
}

/*duplicate the row n times for a row vector*/
Matrix duplicateRowVector(const Matrix& m, int n) {
	ASSERT(m.nRows() == 1);
	Matrix ret = Matrix(n, m.nCols(), 0); //placeholder
	for (auto i = 0; i < ret.nRows(); i++)
	{
		for (auto j = 0; j < ret.nCols(); j++) {
			ret(i, j) = m(0, j);
		}
	}
	return ret;
}


Matrix operator+(const Matrix& x, const Matrix& y)
{
	/*check dimensions*/
	ASSERT(x.nRows() == y.nRows());
	ASSERT(x.nCols() == y.nCols());

	Matrix ret = Matrix(x.nRows(), x.nCols(), 0); //placeholder
	double* dest = ret.begin();
	double const* s1 = x.begin();
	double const* s2 = y.begin();
	double const* end = x.end();

	while (s1 != end) {
		*(dest++) = *(s1++) + *(s2++);
	}

	return ret;
}

Matrix operator+(const Matrix& x, double scalar)
{
	Matrix ret = Matrix(x.nRows(), x.nCols(), 0); //placeholder
	double* dest = ret.begin();
	double const* s1 = x.begin();
	double const* end = x.end();

	while (s1 != end) {
		*(dest++) = *(s1++) + scalar;
	}

	return ret;
}

Matrix operator+(double scalar, const Matrix& x)
{
	return x + scalar;
}

Matrix operator-(const Matrix& x, const Matrix& y)
{
	auto y_copy = y;
	y_copy *= -1;
	return x + y_copy;
}

Matrix operator-(const Matrix& x, double scalar)
{
	return x + (-1) * scalar;
}

Matrix operator-(double scalar, const Matrix& x)
{
	return x - scalar;
}

Matrix operator*(const Matrix& x, double scalar)
{
	Matrix ret = Matrix(x.nRows(), x.nCols(), 0); //placeholder
	double* dest = ret.begin();
	double const* s1 = x.begin();
	double const* end = x.end();

	while (s1 != end) {
		*(dest++) = *(s1++) * scalar;
	}

	return ret;
}

Matrix operator*(double scalar, const Matrix& x)
{
	return x * scalar;
}



Matrix operator>=(const Matrix& x, const Matrix& y)
{
	/*check dimensions*/
	ASSERT(x.nRows() == y.nRows());
	ASSERT(x.nCols() == y.nCols());

	Matrix ret = Matrix(x.nRows(), x.nCols(), 0); //placeholder
	double* dest = ret.begin();
	double const* s1 = x.begin();
	double const* s2 = y.begin();
	double const* end = x.end();

	while (s1 != end) {
		*(dest++) = *(s1++) >= *(s2++);
	}

	return ret;
}

Matrix operator>(const Matrix& x, const Matrix& y)
{
	/*check dimensions*/
	ASSERT(x.nRows() == y.nRows());
	ASSERT(x.nCols() == y.nCols());

	Matrix ret = Matrix(x.nRows(), x.nCols(), 0); //placeholder
	double* dest = ret.begin();
	double const* s1 = x.begin();
	double const* s2 = y.begin();
	double const* end = x.end();

	while (s1 != end) {
		*(dest++) = *(s1++) > *(s2++);
	}

	return ret;
}

Matrix operator<=(const Matrix& x, const Matrix& y)
{
	return (y >= x);
}

Matrix operator<(const Matrix& x, const Matrix& y)
{
	return (y > x);
}

Matrix operator==(const Matrix& x, const Matrix& y)
{
	/*check dimensions*/
	ASSERT(x.nRows() == y.nRows());
	ASSERT(x.nCols() == y.nCols());

	Matrix ret = Matrix(x.nRows(), x.nCols(), 0); //placeholder
	double* dest = ret.begin();
	double const* s1 = x.begin();
	double const* s2 = y.begin();
	double const* end = x.end();

	while (s1 != end) {
		*(dest++) = *(s1++) == *(s2++);
	}

	return ret;
}

Matrix operator!=(const Matrix& x, const Matrix& y)
{
	/*check dimensions*/
	ASSERT(x.nRows() == y.nRows());
	ASSERT(x.nCols() == y.nCols());

	Matrix ret = Matrix(x.nRows(), x.nCols(), 0); //placeholder
	double* dest = ret.begin();
	double const* s1 = x.begin();
	double const* s2 = y.begin();
	double const* end = x.end();

	while (s1 != end) {
		*(dest++) = *(s1++) != *(s2++);
	}

	return ret;
}

ostream& operator <<(ostream& out, const Matrix& m) {
	int nRow = m.nRows(); int nCol = m.nCols(); out << "["; for (int i = 0; i < nRow; i++) {
		for (int j = 0; j < nCol; j++) {
			out << m(i, j); if (j != nCol - 1) {
				out << ",";
			}
		} if (i != nRow - 1) {
			out << ";\n";
		}
	} out << "]\n"; return out;
}


Matrix operator*(const Matrix& x, const Matrix& y) {
	/*check dimensions*/
	ASSERT(x.nCols() == y.nRows());
	Matrix ret = Matrix(x.nRows(), y.nCols(), true); //placeholder
	
	for (auto j = 0; j < y.nCols(); j++) {
		for (auto i = 0; i < x.nRows(); i++) {
			for (auto k = 0; k < x.nCols(); k++) {
				ret(i, j) = ret(i, j) + x(i, k) * y(k, j);
			}
		}
	}
	return ret;
}


///////////////////////////////
//
//   TESTS
//
///////////////////////////////


static void testMathFunctions() {





	{
		Matrix a("1, 2, 3; 4, 5, 6");
		Matrix a_exp("1, 2, 3; 5, 7, 9");
		INFO(a.cumsum(true));
		a.cumsum(true).assertEquals(a_exp, 0.001);
		DEBUG_PRINT("Test matrix cumsum by col passed");
	}

	{
		Matrix a("1, 2, 3; 4, 5, 6");
		Matrix a_exp("1, 3, 6; 4, 9, 15");

		a.cumsum(false).assertEquals(a_exp, 0.001);
		DEBUG_PRINT("Test matrix cumsum by row passed");
	}

	
	{
		Matrix a("1,2,3");
		Matrix a_exp("3,5,7");

		(2 * a + 1).assertEquals(a_exp, 0.001);
		DEBUG_PRINT("Test 2 * matrix + scalar passed");
	}




	{
		Matrix a("1,2,3;4,5,6");
		Matrix a_exp("0,1,2;3,4,5");
		a -= 1;
		a.assertEquals(a_exp, 0.001);
		DEBUG_PRINT("Test matrix -= scalar passed");
	}


	{
		Matrix a("1,2,3;4,5,6");
		Matrix a_exp("2,3,4;5,6,7");
		a += 1;
		a.assertEquals(a_exp, 0.001);
		DEBUG_PRINT("Test matrix += scalar passed");
	}

	{
		Matrix a("1,2,3;4,5,6");
		Matrix a_exp("2,4,6;8,10,12");
		a *= 2;
		a.assertEquals(a_exp, 0.001);
		DEBUG_PRINT("Test matrix *= scalar passed");
	}

	{
		Matrix a("1,2,3;4,5,6");
		auto a_exp = Matrix{ "2.71828183, 7.3890561, 20.08553692; 54.59815003, 148.4131591, 403.42879349" };
		a.exp();
		a.assertEquals(a_exp, 0.001);
		DEBUG_PRINT("Test matrix.exp - passed");
	}

	{
		Matrix a("1,4,9;9,16,25");
		auto a_exp = Matrix{ "1, 2, 3; 3, 4, 5" };
		a.sqrt();
		a.assertEquals(a_exp, 0.00001);
		DEBUG_PRINT("Test matrix.sqrt - passed");
	}

	{
		Matrix a("1,2,3;4,5,6");
		Matrix a_exp("1,2,3;4,5,6");
		a.exp();
		a.log();
		a.assertEquals(a_exp, 0.001);
		DEBUG_PRINT("Test matrix.log - passed");
	}

	{
		Matrix a("1,2,3;4,5,6");
		Matrix a_exp("1,2,3;4,5,6");
		a.sqrt();
		a.pow(2);
		a.assertEquals(a_exp, 0.0001);
		DEBUG_PRINT("Test matrix.pow(2) - passed");
	}

	{
		Matrix a("1,2;3,4");
		Matrix powers("2,2;3,3");
		Matrix a_exp("1,4;27,64");
		a.pow(powers);
		a.assertEquals(a_exp, 0.0001);
		DEBUG_PRINT("Test matrix.pow(const Matrix& powers) - passed");
	}


	{
		Matrix a("1,2,3;4,5,6");
		Matrix b("1,2,3;4,5,6");
		Matrix c("1,4,9; 16,25,36");
		a.times(b);
		a.assertEquals(c, 0.001);
		DEBUG_PRINT("Test matrix a times matrix b element wise - passed");
	}

	{
		Matrix a("1,2,3;4,5,6");
		Matrix c("2,4,6; 8, 10, 12");
		a.times(2.0);
		a.assertEquals(c, 0.001);
		DEBUG_PRINT("Test matrix a times a scalar - passed");
	}

	{
		Matrix a("1,2,3;4,5,6");
		Matrix a_exp("2,4,6; 8, 10, 12");
		a += a;
		a.assertEquals(a_exp, 0.001);
		DEBUG_PRINT("Test matrix += matrix - passed");
	}

	{
		Matrix a("1,2,3;4,5,6");
		Matrix a_exp("0,0,0; 0,0,0");
		a -= a;
		a.assertEquals(a_exp, 0.001);
		DEBUG_PRINT("Test matrix -= matrix - passed");
	}



	{
		DEBUG_PRINT(">>> Test matrix - matrix");
		Matrix a("1,2,3;4,5,6");
		Matrix expected("0,0,0; 0,0,0");
		auto output = a - a;
		output.assertEquals(expected, 0.001);
		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test matrix - scalar");
		Matrix a("1,2,3;4,5,6");
		double b = 1.0;
		Matrix expected("0,1.0, 2.0; 3.0,4.0,5.0");
		auto output = a - b;
		output.assertEquals(expected, 0.001);
		DEBUG_PRINT("<<< passed");
	}


	{
		DEBUG_PRINT(">>> Test matrix - scalar");
		Matrix a("1,2,3;4,5,6");
		double b = 1.0;
		Matrix expected("0,1.0, 2.0; 3.0,4.0,5.0");
		auto output = a - b;
		output.assertEquals(expected, 0.001);
		DEBUG_PRINT("<<< passed");
	}


	{
		DEBUG_PRINT(">>> Test matrix * matrix");
		Matrix a("1,2,3;4,5,6");
		Matrix b("1,2;3,4;5,6");
		Matrix product = a * b;
		Matrix expected("22,28;49,64");
		product.assertEquals(expected, 0.001);
		DEBUG_PRINT("<<< passed");
	}



}

static void testMatrixTransformation() {

	{
		DEBUG_PRINT(">>> Test matrix row(i)");

		Matrix a("1,2,3;4,5,6");
		Matrix expected("1,2,3");
		Matrix expected2("4,5,6");
		a.row(0).assertEquals(expected, 0.001);
		a.row(1).assertEquals(expected2, 0.001);
		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test matrix col(i)");

		Matrix a("1,2,3;4,5,6");
		Matrix expected("1;4");
		Matrix expected2("3; 6");
		a.col(0).assertEquals(expected, 0.001);
		a.col(2).assertEquals(expected2, 0.001);
		DEBUG_PRINT("<<< passed");
	}


	{
		DEBUG_PRINT(">>> Test duplicateColVector");

		Matrix a(vector<double>{1, 2, 3}, true);
		Matrix expected("1,1,1,1;2,2,2,2;3,3,3,3");

		auto output = duplicateColVector(a, 4);
		INFO(output);
		ASSERT(output.nRows() == 3);
		ASSERT(output.nCols() == 4);
		expected.assertEquals(output, 0.001);
		DEBUG_PRINT("<<< passed");
	}

	{
		DEBUG_PRINT(">>> Test duplicateRowVector");

		Matrix a(vector<double>{1, 2, 3}, false);
		Matrix expected("1,2,3;1,2,3;1,2,3;1,2,3");

		auto output = duplicateRowVector(a, 4);
		INFO(output);
		ASSERT(output.nRows() == 4);
		ASSERT(output.nCols() == 3);
		expected.assertEquals(output, 0.001);
		DEBUG_PRINT("<<< passed");
	}

	{
		Matrix m(1, 10, 1);
		for (int i = 0; i < 10; i++) {
			m(0, i) = i;
		}
		vector<double> row = m.rowVector();
		for (int i = 0; i < 10; i++) {
			ASSERT(row[i] == i);
		}

		DEBUG_PRINT("Test matrix.rowVector() - passed");
	}

	{
		Matrix m(10, 1);
		for (int i = 0; i < 10; i++) {
			m(i, 0) = i;
		}
		vector<double> col = m.colVector();
		for (int i = 0; i < 10; i++) {
			ASSERT(col[i] == i);
		}

		DEBUG_PRINT("Test matrix.colVector() - passed");
	}


}



void static testMatrixComparison() {
	{
		Matrix a("1,2,3;4,5,6");
		Matrix b("1,2,3;7,5,1");
		
		Matrix c("1,1,1; 0,1,1");
		Matrix c2("0,0,0; 0,0,1");
		Matrix c3("1,1,1; 0,1,0");
		Matrix c4("0,0,0; 1,0,1");


		(a >= b).assertEquals(c, 0.001);
		DEBUG_PRINT("Test matrix a >= b element wise - passed");
		
		(b <= a).assertEquals(c, 0.001);
		DEBUG_PRINT("Test matrix b >= a element wise - passed");

		(a > b).assertEquals(c2, 0.001);
		DEBUG_PRINT("Test matrix a > b element wise - passed");

		(b < a).assertEquals(c2, 0.001);
		DEBUG_PRINT("Test matrix b > a element wise - passed");

		(a == b).assertEquals(c3, 0.001);
		DEBUG_PRINT("Test matrix a == b element wise - passed");

		(a != b).assertEquals(c4, 0.001);
		DEBUG_PRINT("Test matrix a != b element wise - passed");		
		
	}

}



void static testCreateMatrix() {

	{
		 
		Matrix a = Matrix(std::vector<double>{1., 2., 3., 4., 5., 6.}, true, 2 , 3);
		Matrix a_exp("1,3,5;2,4,6");
		INFO(a);
		DEBUG_PRINT("Test create a 2x3 matrix from col major std::vector");
		a.assertEquals(a_exp, 0.0001);

	}

	{

		Matrix a = Matrix(std::vector<double>{1., 2., 3., 4., 5., 6.}, false, 2, 3);
		Matrix a_exp("1,2,3; 4,5,6");
		INFO(a);
		DEBUG_PRINT("Test create a 2x3 matrix from row major std::vector");
		a.assertEquals(a_exp, 0.0001);

	}



	{
		Matrix a("1,2,3;4,5,6");
		Matrix b("1,2,3;4,5,6");
		a.assertEquals(b, 0.01);
		DEBUG_PRINT("Test create 2 matrix are equal - passed");
	}

	{
		Matrix a("1,2,3;4,5,6");
		Matrix b("1,2,3;4,5,6");
		Matrix c("2,4,6;8,10,12");
		auto d = a + b;
		c.assertEquals(d, 0.01);
		DEBUG_PRINT("Test matrix + matrix - passed");
	}
	
	{
		//create a 3x2 matrix and initlialise it to 0s
		Matrix m = Matrix(3, 2, true);
		DEBUG_PRINT("Test create 3x2 matrix");
		ASSERT(m.nRows() == 3);
		ASSERT(m.nCols() == 2);

		m.set(1, 1, 2);
		DEBUG_PRINT("Test set value");
		ASSERT(m.get(1, 1) == 2);
	}

	{
		//create a  matrix with default constructor
		Matrix m = Matrix();
		DEBUG_PRINT("Test matrix default constructor");
		ASSERT(m.nRows() == 1);
		ASSERT(m.nCols() == 1);
		ASSERT(m.get(0, 0) == 0);
	}

	{
		//create a col vector matrix from std::vector 
		Matrix m = Matrix(std::vector<double>{1.,2.,3.}, true);
		DEBUG_PRINT("Test create a col vector matrix from std::vector");
		ASSERT(m.nRows() == 3);
		ASSERT(m.nCols() == 1);
		ASSERT_APPROX_EQUAL(m.get(0, 0), 1., 0.0001);
		ASSERT_APPROX_EQUAL(m.get(1, 0), 2., 0.0001);
		ASSERT_APPROX_EQUAL(m.get(2, 0), 3., 0.0001);
	}
	{
		//create a row vector matrix from std::vector 
		Matrix m = Matrix(std::vector<double>{1., 2., 3.}, false);
		DEBUG_PRINT("Test create a row vector matrix from std::vector");
		ASSERT(m.nRows() == 1);
		ASSERT(m.nCols() == 3);
		ASSERT_APPROX_EQUAL(m.get(0, 0), 1., 0.0001);
		ASSERT_APPROX_EQUAL(m.get(0, 1), 2., 0.0001);
		ASSERT_APPROX_EQUAL(m.get(0, 2), 3., 0.0001);
	}

	{
		Matrix m = Matrix("1,2,3; 4, 5, 6");
		DEBUG_PRINT("Test create a matrix based on literal string chars");
		DEBUG_PRINT("Test access matrix via ()");
		ASSERT(m.nRows() == 2);
		ASSERT(m.nCols() == 3);
		ASSERT_APPROX_EQUAL(m(0, 0), 1., 0.0001);
		ASSERT_APPROX_EQUAL(m(0, 1), 2., 0.0001);
		ASSERT_APPROX_EQUAL(m(0, 2), 3., 0.0001);
		ASSERT_APPROX_EQUAL(m(1, 0), 4., 0.0001);
		ASSERT_APPROX_EQUAL(m(1, 1), 5., 0.0001);
		ASSERT_APPROX_EQUAL(m(1, 2), 6., 0.0001);
		//ASSERT_APPROX_EQUAL(m(0), 1., 0.0001);
		//ASSERT_APPROX_EQUAL(m(1), 2., 0.0001);

	}
}

void testMatrix()
{
	setDebugEnabled(true);
	//TEST(testMatrixTransformation);
	TEST(testMathFunctions);
	//TEST(testMatrixComparison);
	//TEST(testCreateMatrix);
	setDebugEnabled(false);
}
