#include <cmath>
#include "BlackScholesModel.h"
#include "matlib.h"
#include "LineChart.h"


using std::vector;


BlackScholesModel::BlackScholesModel(): drift(0.0),stockPrice(0.0),volatility(0.0),riskFreeRate(0.0),date(0.0) {};
BlackScholesModel::BlackScholesModel(double riskFreeRate, double volatility, double stockPrice, double date) :
	stockPrice(stockPrice), volatility(volatility), riskFreeRate(riskFreeRate), date(date), drift(riskFreeRate) {
	//drift is always set to risk free rate by default hence it is risk neutral
};

//vector<double> BlackScholesModel::generatePricePath(double toDate, int nSteps) const
//{
//	return this->generatePricePath(toDate, nSteps, this->drift);
//}

vector<double> BlackScholesModel::generateRiskNeutralPricePath(double toDate, int nSteps)
{
	//double _riskNeutralDrift = this->riskFreeRate;
	//double _riskNeutralDrift = this->riskFreeRate;
	
	//in order to allow the member function to change the object member the function 
	// CAN NOT be const. If the function is defined as const then the member object needs 
	// to be mutable which is pretty bad in my view
	drift = this->riskFreeRate;
	return this->generatePricePath(toDate, nSteps);
}

vector<double> BlackScholesModel::generatePricePath(double toDate, int nSteps) const
{
	vector<double> _pricePath(nSteps, nan(""));
	//vector<double> epsilon = randn(nSteps);
	vector<double> epsilon = randNorm(nSteps);
	
	double dt = (toDate - this->date) / (double) nSteps;
	double a = (this -> drift - this->volatility * this->volatility * 0.5) * dt;
	double b = this->volatility * sqrt(dt);
	double lp = log(this->stockPrice);

	for (int i = 0; i < nSteps; i++) {
		double dlp = a + b * epsilon[i];
		double lpNext = lp + dlp;
		_pricePath[i] = exp(lpNext);
		lp = lpNext;
	}
	return _pricePath;
}

Matrix BlackScholesModel::generatePricePaths(std::mt19937& rng, double toDate, int nSteps, int nPaths, double drift) const
{
	vector<double> epsilon;
	epsilon.reserve(nSteps * nPaths);
	for (int i = 0; i < nPaths; i++)
	{
		auto rv = randNorm(nSteps);
		epsilon.insert(epsilon.end(), rv.begin(), rv.end());
	}

	//each row is a simulation path and each col is a step 
	Matrix mtxEpsilon = Matrix(epsilon, true, nPaths, nSteps);
	//hist("draws.html", mtxEpsilon.asVector(), 10);

	//INFO(mtxEpsilon);

	double dt = (toDate - this->date) / (double)nSteps;
	double a = (drift - this->volatility * this->volatility * 0.5) * dt;
	double b = this->volatility * sqrt(dt);
	double lp = log(this->stockPrice);

	auto dlp = a + b * mtxEpsilon;
	auto dlpCumsum = dlp.cumsum(false); //cumsum each row


	//Matrix times = linspace(dt, toDate, nSteps, 1);
	//plot("vol.html", times, dlp);
	//plot("cumVol.html",times, dlpCumsum);
	//INFO(mtxEpsilon);


	auto pricePaths = lp + dlpCumsum;
	pricePaths.exp();

	return pricePaths;
}

Matrix BlackScholesModel::generateRiskNeutralPricePaths(std::mt19937& rng, double toDate, int nSteps, int nPaths) const
{
	return this->generatePricePaths(rng, toDate, nSteps, nPaths, this->riskFreeRate);
}




///////////////////////////////////////////////;
//
//   TESTS
//
///////////////////////////////////////////////


static void testGeneratePricePaths() {
	
	static std::mt19937 mersenneTwister;

	resetSeed(false);

	BlackScholesModel bsm;
	bsm.riskFreeRate = 0.001;
	bsm.volatility = 0.1;
	bsm.stockPrice = 100.0;
	bsm.date = 2.0;
	drift = bsm.riskFreeRate; //set drift

	int nPaths = 10;
	int nSteps = 1000;
	double maturity = 4.0;
	Matrix paths =
		bsm.generatePricePaths(mersenneTwister,
			maturity,
			nSteps,
			nPaths,
			drift);

	Matrix finalPrices = paths.col(nSteps - 1);
	auto finalPrice = meanCols(finalPrices).asScalar();
	auto expectedPrice = exp(bsm.riskFreeRate * 2.0) * bsm.stockPrice;
	
	ASSERT_APPROX_EQUAL(finalPrice, expectedPrice, 50);

	double dt = (maturity - bsm.date) / nSteps;
	Matrix times = linspace(dt, maturity, nSteps, 1);

	auto avgPath = meanCols(paths);
	plot("avgPricePath.html",
		times,
		avgPath);

}


static void testGeneratePricePath() {

	auto riskFreeRate = 0.05;
	auto volatility = 0.1;
	auto stockPrice = 100.0;
	auto date = 2.0;
	auto nSteps = 1000;
	auto maturity = 4.0;

	BlackScholesModel bsm { riskFreeRate, volatility, stockPrice, date};

	vector<double> pricePath = bsm.generateRiskNeutralPricePath(maturity, nSteps);

	double dt = (maturity - bsm.date) / (double) nSteps;
	vector<double> times = linspaceV(dt, maturity, nSteps);

	LineChart lineChart;
	lineChart.setTitle("A Stock Price Path");
	lineChart.setSeries(times, pricePath);
	lineChart.writeAsHTML("examplePricePath.html ");
}

static void testRiskNeutralPricePath() {
	/*If risk neutral price pathes are correct the discounted value of the mean path
	terminal values should be equal to the initial price*/

	auto riskFreeRate = 0.05;
	auto volatility = 0.1;
	auto stockPrice = 100.0;
	auto date = 2.0;
	
	BlackScholesModel bsm{ riskFreeRate, volatility, stockPrice, date };

	auto nPaths = 10000; //generate 10000 simulation
	auto nSteps = 50; // each simulation has 50 pricing iterations
	auto maturity = 4.0; 
	vector<double> finalPrices(nPaths, 0.0);

	for (int i = 0; i < nPaths; i++) {
		vector<double> _singlePath = bsm.generateRiskNeutralPricePath(maturity, nSteps);
		finalPrices[i] = _singlePath.back(); //get the terminal price of the simulation
	}

	auto _meanPrice = mean(finalPrices);
	auto _expectedPrice = bsm.stockPrice * exp(bsm.riskFreeRate * 2);

	ASSERT_APPROX_EQUAL(_meanPrice, _expectedPrice, 0.5);
}

void testBlackScholesModel() {

	setDebugEnabled(true);
	TEST(testGeneratePricePaths);
	//TEST(testGeneratePricePath);
	//TEST(testRiskNeutralPricePath);
	setDebugEnabled(false);

}