#include "matlib.h"
#include "HedgingSimulator.h"


using std::make_shared;
//default constructor
HedgingSimulator::HedgingSimulator(){
	auto model = make_shared<BlackScholesModel>();
	model->stockPrice = 1.;
	model->date = 0;
	model->riskFreeRate = 0.05;
	model->volatility = 0.2;
	model->drift = 0.1;

	auto option = make_shared<CallOption>();
	option->strike = model->stockPrice; //set the strike to current price
	option->maturity = 1;

	this->setOptionToHedge(option);
	this->setSimulationModel(model);
	this->setPricingModel(model);

	nSteps = 10;

}

vector<double> HedgingSimulator::runSimulations(int nSimulations) const
{
	vector<double> ret(nSimulations);
	for (int i = 0; i < nSimulations; i++) {
		ret[i] = this->simulateOnce();
	}
	return ret;
}

double HedgingSimulator::simulateOnce() const
{

	double T = this->optionToHedge->getMaturity();
	double dt = T / this->nSteps;
	double S0 = this->simulationModel->stockPrice;
	//we know how much the option worth from 1 random simulated path
	vector<double> simulatedStepPricesAlongFromOneSimilation = this->simulationModel->generatePricePath(T, this->nSteps);
	//option price based on blackSchole closed form solution
	double charge = chooseCharge(S0);
	//initial hedging quantity based on S0
	double stockQuantity = selectStockQuantity(0, S0);
	//Sold the option, bought the stock to hedge and deposit the rest to the bank
	double bankBalance = charge - stockQuantity * S0;

	//stock started to trade and continue to delta hedge
	for (int i = 0; i < this->nSteps - 1; i++) {
		double balanceWithInterest = bankBalance * exp(simulationModel->riskFreeRate * dt);
		//get the next price along the path
		double S = simulatedStepPricesAlongFromOneSimilation[i];
		//roll time forward by 1 step
		double hedgingTimeStop = dt * (i + 1);
		//recalc the delta and hedge amount using the new stock price 
		double newStockQuantity = this-> selectStockQuantity(hedgingTimeStop, S);
		//calc the net payments for either buy or sell the stock
		double costs = (newStockQuantity - stockQuantity) * S;
		//update the bank balance with the payment
		bankBalance = balanceWithInterest - costs;
		//set the delta hedge quantity
		stockQuantity = newStockQuantity;
	}

	//At the expiry

	//another day of interest
	double balanceWithInterest = bankBalance * exp(simulationModel->riskFreeRate * dt);
	
	//get the simulated stock price at the expiry
	double S = simulatedStepPricesAlongFromOneSimilation.back();
	double stockValue = stockQuantity * S;
	double optionPayout = this->optionToHedge->payoff(S);
	return balanceWithInterest + stockValue - optionPayout;

}

/*  How much should we charge the customer for the option aka price the option */
double HedgingSimulator::chooseCharge(double stockPrice) const
{
	//creating a copy of the bms, the * will force point to be dereferenced 
	BlackScholesModel bsm = *this->pricingModel;
	bsm.stockPrice = stockPrice;
	return this->optionToHedge->price(bsm);
}

double HedgingSimulator::selectStockQuantity(double hedgingTimeStop, double stockPrice) const
{
	//creating a copy of the bms, the * will force point to be dereferenced 
	BlackScholesModel bsm = *this->pricingModel;
	bsm.stockPrice = stockPrice;
	/*the date is the parameter passed in to indicator the aging of the option
	the delta calc is a continuous process happens at each hedging stop*/
	bsm.date = hedgingTimeStop;

	return this -> optionToHedge->calcDelta(bsm);
}


//////////////////////////////////////////////////////
//
//   TESTS
//
//////////////////////////////////////////////////////


static void testDeltaHedgingMeanPayoff() {
	resetSeed(true);
	auto simulator = HedgingSimulator();
	simulator.setNsteps(1000);
	vector<double> result = simulator.runSimulations(1);
	ASSERT_APPROX_EQUAL(result[0], 0.0, 0.01);
}

static void testPlotDeltaHedgingHistogram() {
	resetSeed(true);
	auto simulator = HedgingSimulator();
	simulator.setNsteps(100);
	vector<double> result = simulator.runSimulations(10000);
	hist("simulatedDeltaHedgingPayoffs.html", result, 20);
}


void testHedgingSimulator()
{
	TEST(testDeltaHedgingMeanPayoff);
	TEST(testPlotDeltaHedgingHistogram);
	
}
