#pragma once
#include "stdafx.h"
#include "Matrix.h"

//no longer going to use the vector. replace it with matrix
using std::vector;

//Test handler 
void testBlackScholesModel();


class BlackScholesModel {

public:
	
	//https://stackoverflow.com/questions/32686220/is-it-possible-change-value-of-member-variable-within-const-function/32686228
	double drift;
	double stockPrice;
	double volatility;
	double riskFreeRate;
	double date;

	BlackScholesModel();
	BlackScholesModel(double riskFreeRate,  double volatility, double stockPrice, double date);

	vector<double> generatePricePath(
		double toDate,
		int nSteps) const;

	vector<double> generateRiskNeutralPricePath(
		double toDate,
		int nSteps);

	Matrix generatePricePaths(
		std::mt19937& rng,
		double toDate,
		int nSteps,
		int nPaths,
		double drift) const;

	Matrix generateRiskNeutralPricePaths(
		std::mt19937& rng,
		double toDate,
		int nSteps,
		int nPaths) const;

	void test(double a);

/*
private:
	vector<double> generatePricePath(
		double toDate,
		int nSteps,
		double drift) const;
*/

};
