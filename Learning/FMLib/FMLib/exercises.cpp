#include "exercises.h"
#include "stdafx.h"
#include "matlib.h"

using std::vector;


QuardraticRoots::QuardraticRoots(double root1, double root2) :_root1(root1), _root2(root2) {}
const double QuardraticRoots::root1() { return _root1; }
const double QuardraticRoots::root2() { return _root2; }



QuardraticRoots solveQuadratic(double a, double b, double c) {

	//y = a * x * x + b * x + c 
	vector<double> result{ 0, 0 };


	double d = pow(b, 2) - 4 * a * c;

	//there is a root
	if (d == 0) {
		auto rt1 = -b / (2 * a);
		auto rt2 = result[0];

		QuardraticRoots rt(rt1, rt2);
		return rt;
	}
	else if (d > 0) {
		auto rt1 = -b / (2 * a) + sqrt(d / (4 * pow(a, 2)));
		auto rt2 = -b / (2 * a) - sqrt(d / (4 * pow(a, 2)));

		QuardraticRoots rt(rt1, rt2);
		return rt;
	}
	else{
		//throw std::exception{ "Function not yet implemented." };
		throw base::NotImplementedException();
	}


}


///////////////////////////////////////////////
//
//   TESTS
//
///////////////////////////////////////////////

static void tetSolveQuadratic() {
	
	auto result = solveQuadratic(2, 5, -3);

	//verify the roots
	ASSERT_APPROX_EQUAL(result.root1(), 0.5, 0.0001);
	ASSERT_APPROX_EQUAL(result.root2(), -3, 0.0001);

	


}


void testChapter7Excercise() {

	TEST(tetSolveQuadratic);
}
