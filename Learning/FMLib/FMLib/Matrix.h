#pragma once
#include "stdafx.h"


using std::vector;
using std::ostream;
using std::function;


class Matrix {

private:
	/*number of rows in the matrix*/
	int nrows;
	/*number of columns in the matrix*/
	int ncols;
	/*A pointer to the beginning of the underline data array*/
	double* data;
	/*A pointer to the one after theend of the underline data array*/
	double* endPointer;

	/**
	 *  Assign values to this matrix so that it contains
	 * the same data as another matrix
	 */
	void assign(const Matrix& other);

	/**
	 *  Steal values to this matrix so that it contains
	 * the data from another matrix
	 */
	void steal(Matrix& other) noexcept;

	/*element wise apply with 1 double input*/
	void apply(function<void(double&)> f);
	void apply(function<void(double&, const double&)> f, const Matrix& other);

	
	

public:
	/*container conventions*/
	typedef double value_type;
	typedef double* iterator;
	typedef double* const_iterator;


	/*default constructor*/
	Matrix();
	
	/*constructor with shape dimension and option to set the default values to 0*/
	Matrix(int nrows, int ncols, bool zeros = true);
	
	/*take a vector and create a column vector matrix or row vector matrix*/
	Matrix(std::vector<double> values, bool colMajor = true);

	/*Matrix m("1,2,3; 4, 5, 6")*/
	Matrix(std::string const& values);

	Matrix(std::vector<double> values, bool colMajor, int nrows, int ncols);

	/*destructor*/
	
	~Matrix() { 
		delete[] data;
		data = nullptr;
	};

	/**
	 *   Copy assignment
	 */
	Matrix& operator=(const Matrix& other) {
		delete[] data;
		assign(other);
		return *this;
	}

	/**
	 *   Copy constructor
	 */
	Matrix(const Matrix& other) {
		assign(other);
	}

	/**
	 *   Move assignment
	 */
	Matrix& operator=(Matrix&& other) noexcept {
		steal(other);
		return *this;
	}

	/**
	 *   Move constructor
	 */
	Matrix(Matrix& other) noexcept {
		steal(other);
	}

	/*getter for the number of rows in the matrix*/
	int nRows() const { return this->nrows; }
	
	/*getter for the number of the cols in the matrix*/
	int nCols() const { return this->ncols; }

	/*Access the beginning of the the array*/
	const double* begin() const{
		return data;
	}
	/*non const version*/
	double* begin() {
		return data;
	}
	/*Access the 1 element right after the the array*/
	const double* end() const {
		return endPointer;
	}
	/*non const version*/
	double* end() {
		return endPointer;
	}


	/**
	 *   Allows one to access a cell using parentheses
	 *   Apparently using round brackets rather than square
	 *   ones is preferable in terms of speed!
	 */
	double& operator()(int i, int j) {
		return data[offset(i, j)];
	}

	/**
	 *   If you want a reference to something inside a const
	 *   object, the returned reference must be const
	 */
	const double& operator()(int i, int j) const {
		return data[offset(i, j)];
	}

	/**
	 *   Allows one to access a cell of a vector using parentheses
	 */
	double& operator()(int i) {
		ASSERT(i < nrows* ncols);
		return  data[i];
	}

	/**
	 *   Allows one to access a cell of a vector using parentheses
	 */
	const double& operator()(int i) const {
		ASSERT(i < nrows* ncols);
		return data[i];
	}

	/*  Converts a 1x1 matrix to a scalar */
	double asScalar() const {
		ASSERT(nrows == 1 && ncols == 1);
		return *data;
	}

	/*  Convert a row vector to a std::vector<double> */
	vector<double> rowVector() const;
	
	/*  Convert a column vector to a std::vector<double> */
	vector<double> colVector() const;
	
	/*  Convert a row or column vector into a std::vector<double> */
	vector<double> asVector() const;


	/*  Returns a matrix representing the given row */
	Matrix row(int row) const;
	/*  Returns a matrix representing the given column */
	Matrix col(int col) const;


	/*convert the row index i and col index j into the location of the data point in the
	array. The underline data array represents a column major matrix, ie a 1d array data is
	laid into a 2d matrix column by column */
	int offset(int i, int j) const {
		ASSERT(i >= 0 && i < nrows&& j >= 0 && j < ncols); //macro only compiled during debug
		return j * this->nrows + i;
	}
	/*retrieve the data from row index i and col index j*/
	double get(int i, int j) const {
		return data[offset(i, j)]; //raw pointer operation
	}
	/*set the data from row index i and col index j to value*/
	void set(int i, int j, double value) {
		data[offset(i, j)] = value;
	}

	/*  Assert two matrices are identical*/
	void assertEquals(const Matrix& other, double tolerance);

	/*Expontetiate every element*/
	void exp();
	/*  Square root every element */
	void sqrt();
	/*  Take the log of every element */
	void log();
	/*  Take the positive part of every element */
	void positivePart();
	/*  Take the negative part of every element */
	void negativePart();
	/*  Entrywise raising to a power */
	void pow(double power);
	/*  Entrywise raising to a power */
	void pow(const Matrix & powers);
	
	/*  Entrywise multiplication */
	inline void times(double factor) {
		(*this) *= factor;
	}

	/*  Entrywise multiplication */
	void times(const Matrix& other);

	/*  Scalar multiplication */
	Matrix& operator*=(double factor);
	/*  Scalar addition */
	Matrix& operator+=(double scalar);
	/*  Addition */
	Matrix& operator+=(const Matrix& other);
	/*  Scalar subtraction */
	Matrix& operator-=(double scalar);
	/*  Subtraction */
	Matrix& operator-=(const Matrix& other);

	/*  cumsum */
	Matrix cumsum(bool byCol = true) const;

};


/*define the shared ptr to a matrix type*/
typedef std::shared_ptr<Matrix> SPMatrix;
typedef std::shared_ptr<const Matrix> SPCMatrix;


/*None Matrix class member functions*/


/* + operator override */

Matrix operator*(const Matrix& x, const Matrix& y);
Matrix operator*(const Matrix& x, double scalar);
Matrix operator*(double scalar, const Matrix& x);


Matrix operator+(const Matrix& x, const Matrix& y);
Matrix operator+(const Matrix& x, double scalar);
Matrix operator+(double scalar, const Matrix& x);

Matrix operator-(const Matrix& x, const Matrix& y);
Matrix operator-(const Matrix& x, double scalar);
Matrix operator-(double scalar, const Matrix& x);

Matrix operator>(const Matrix& x, const Matrix& y);
Matrix operator>=(const Matrix& x, const Matrix& y);
Matrix operator<(const Matrix& x, const Matrix& y);
Matrix operator<=(const Matrix& x, const Matrix& y);
ostream& operator <<(ostream& out, const Matrix& m);
Matrix operator== (const Matrix& x, const Matrix& y);
Matrix operator!= (const Matrix& x, const Matrix& y);

/*duplicate the col n times for a col vector*/
Matrix duplicateColVector(const Matrix& m, int n); 
/*duplicate the row n times for a row vector*/
Matrix duplicateRowVector(const Matrix& m, int n); 





///////////////////////////////
//
//   TESTS
//
///////////////////////////////


void testMatrix();