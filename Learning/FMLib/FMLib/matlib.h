#pragma once
#include "stdafx.h"
#include "Matrix.h"

using std::vector;


/*  Create a linearly spaced vector */
Matrix linspace(double from, double to, int numPoints, 
	bool rowVector = false);
/*  Compute the sum of a matrix's rows */
Matrix sumRows(const Matrix& m);
/*  Compute the sum of a matrix's cols */
Matrix sumCols(const Matrix& m);
/*  Compute the mean of a matrix's rows */
Matrix meanRows(const Matrix& m);
/*  Compute the mean of a matrix's cols */
Matrix meanCols(const Matrix& m);
/*  Compute the standard deviation of a matrix's rows */
Matrix stdRows(const Matrix& m, bool population = false);
/*  Compute the standard deviation of a matrix's rows */
Matrix stdCols(const Matrix& m, bool population = false);
/*  Compute the minimum entry of each row */
Matrix minOverRows(const Matrix& m);
/*  Compute the minimum entry of each col */
Matrix minOverCols(const Matrix& m);
/*  Compute the maximum entry of each row */
Matrix maxOverRows(const Matrix& m);
/*  Compute the maximum entry of each col */
Matrix maxOverCols(const Matrix& m);
/*  Find the given percentile over the rows of a vector */
Matrix prctileRows(const Matrix& m, double percentage);
/*  Find the given percentile over the cols of a vector */
Matrix prctileCols(const Matrix& m, double percentage);
/*  Sort the rows of a matrix */
Matrix sortRows(const Matrix& m);
/*  Sort the cols of a matrix */
Matrix sortCols(const Matrix& m);
/*  Create uniformly distributed random numbers */
Matrix randuniform(int rows, int cols);
/*  Create normally distributed random numbers */
Matrix randNorm(int rows, int cols);
/**
 *  Exponentiate a matrix
 */
inline Matrix exp(const Matrix& m) {
	Matrix ret = m;
	ret.exp();
	return ret;
}

/**
 *  Pointwise product
 */
inline Matrix dotTimes(Matrix& a, const Matrix& b) {
	Matrix ret = a;
	ret.times(b);
	return ret;
}


/*  Matrix transpose */
Matrix transpose(const Matrix& m);
/*  Cholesky decomposition */
Matrix chol(const Matrix& m);





/**
 *  Sort and return a copy of vector of doubles
 */
vector<double> sort(const std::vector<double>& v);

/**
 *  Find the sum of the elements in an array
 */
double sum(const vector<double>& v);


/*  Compute the mean of a vector */
double mean(const vector<double>& v);

/*  Find the minimum of a vector */
double min(const vector<double>& v);

/*  Find the maximum of a vector */
double max(const vector<double>& v);

/*  Create a linearly spaced vector */
vector<double> linspaceV(double from, double to, int numPoints);

/*  Compute the standard deviation of a vector */
double standardDeviation(const vector<double>& v, bool population);

/*
Computes the cumulative dirstribution function of the norm distribution
*/
double normcdf(double x);
/*
Computes the inverse of the normcdf
*/
double norminv(double x);


/*
reset seed for random variable generator
*/
void resetSeed(bool staticSeed = false);

/*
generate n uniformly distributed random variables
*/
vector<double> randUniform(int n);

/*
generate n normally distributed normal random variables
*/
vector<double> randNorm(int n);
//vector<double> randn(int n);

/**
 *  Convenience method for generating plots
 */
void plot(const  std::string& file,
	const Matrix& x,
	const Matrix& y);

/*  Plot a histogram */
void hist(const std::string& fileName,
	const std::vector<double>& values,
	int numBuckets = 10);


/*test harness interface*/
void testMatlib();

namespace base
{
	class NotImplementedException : public std::exception
	{
		public:
			NotImplementedException();
		
	};
};
