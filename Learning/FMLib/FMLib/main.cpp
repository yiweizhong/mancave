#include <iostream>
#include "matlib.h"
#include "Histogram.h"
#include "exercises.h"
#include "BlackScholesModel.h"
#include "CallOption.h"
#include "PutOption.h"
#include "KnockoutOption.h"
#include "MonteCarloPricer.h"
#include "Portfolio.h"
#include "HedgingSimulator.h"
#include "Matrix.h"

using namespace std;

int main()
{
	//testMatrix();
	//testHedgingSimulator();
	//testHistogram();
	//testMatlib();
	//testPortfolio();
	//testKnockoutOption();
	//testPutOption();
	//testMonteCarloPricer();
	//testCallOption();
	testBlackScholesModel();
	//testMatlib();
	//testChapter7Excercise();

	return 0;
}
