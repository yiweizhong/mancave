#pragma once

#include <map>
#include <boost/optional.hpp>

typedef std::map<std::string, std::string> dict;

std::string find_in_dict(const dict& source, std::string& search_key);
boost::optional<std::string> find_in_dict_optional(const dict& source, std::string& search_key);

void test_Chp1();