#include "Ch1.h"
#include "Testing.h"


using std::string;

string find_in_dict(const dict& source, string& search_key)
{
	auto iter = source.find(search_key);

	if (iter != source.end()) {
		return iter->second;
	}
	else
	{
		return "";
	}
}

boost::optional<std::string> find_in_dict_optional(const dict& source, std::string& search_key)
{
	auto iter = source.find(search_key);

	if (iter != source.end()) {
		return iter->second;
	}
	else
	{
		return boost::none;
	}
}


/////////////////////////////////////////////////////////////
/////// Test
/////////////////////////////////////////////////////////////

static void test_find_in_dict() {
	
	auto source = dict{ {"A","a"}, {"B","b"} };
	auto good_key = string{ "A" };
	auto bad_key = string{ "C" };

	auto result_found = find_in_dict(source, good_key);
	auto result_not_found = find_in_dict(source, bad_key);

	ASSERT(result_found == string("a"));
	ASSERT(result_not_found == string(""));

	auto result_found_opt = find_in_dict_optional(source, good_key);
	auto result_not_found_opt = find_in_dict_optional(source, bad_key);

	ASSERT(result_found_opt); //result_found_opt should evaluate to true
	ASSERT(result_found_opt->empty() == false);
	ASSERT(result_found_opt.get() == string("a"));
	ASSERT(*result_found_opt == string("a"));

	ASSERT(!result_not_found_opt); //result_found_opt should evaluate to true
	ASSERT(result_not_found_opt.get_value_or("Not found") == string("Not found")); //provide default for not found

}

void test_Chp1()
{
	setDebugEnabled(true);
	TEST(test_find_in_dict);
	setDebugEnabled(false);
}

