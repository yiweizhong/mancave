%% Modelling yields under Q
% All we do here is to plot the loading structures of the Svensson-
% Soderlind and the 4-factor SRB models
%
path_=[pwd,'\MATLAB_classes'];
addpath(path_);

tau    = ( 1:1:120 )';
nTau   = size(tau,1);

Bfunc_SS = @(lambda_,tau_,nTau_) ...
[ ones(nTau_,1) (1-exp(-lambda_(1,1).*tau_))./ (lambda_(1,1).*tau_) ...
(1-exp(-lambda_(1,1).*tau_))./(lambda_(1,1).*tau_)-exp(-lambda_(1,1).*tau_) ... 
(1-exp(-lambda_(2,1).*tau_))./(lambda_(2,1).*tau_)-exp(-lambda_(2,1).*tau_) ];


Bfunc_SRB4 = @(lambda_,tau_,nTau_) ...
    [ ones(nTau_,1) 1-(1-lambda_.^tau_)./((1-lambda_).*tau_) ...
     -(lambda_.^(tau_-1))+(1-lambda_.^tau_)./((1-lambda_).*tau_) ...
     -0.5.*(tau_-1).*(lambda_-1).*lambda_.^(tau_-2) ];

L_SS   = [ 0.0381; 0.1491 ]; 

L_SRB4 = 0.945;    

B_SS   = Bfunc_SS( L_SS, tau, nTau );
B_SRB4 = Bfunc_SRB4( L_SRB4, tau, nTau );

tau_plot = [3 12:12:120]';
figure('units','normalized','outerposition',[0 0 1 1])
    subplot(3,1,1), plot( tau, 1-B_SS(:,2) , ...
        'LineWidth',2),  ylim([0 1]),  title('Slope'), 
         ylabel('Value'), set(gca, 'FontSize', 20)
    hold on
    subplot(3,1,1), plot(tau,B_SRB4(:,2) , ...
         'LineWidth',2), grid 'on'
         xticks(tau_plot),xticklabels(tau_plot)
         legend('Svensson-Soderlind','4-factor SRB','Location','SE')
     
    subplot(3,1,2), plot( tau, B_SS(:,3) , ...
        'LineWidth',2),  ylim([0 0.5]),  title('Curvature 1'), 
         ylabel('Value'), set(gca, 'FontSize', 20)
    hold on
    subplot(3,1,2), plot(tau,B_SRB4(:,3) , ...
         'LineWidth',2), grid 'on'
         xticks(tau_plot),xticklabels(tau_plot)

    subplot(3,1,3), plot( tau, B_SS(:,4) , ...
        'LineWidth',2),  ylim([0 0.5]),  title('Curvature 2'), 
         ylabel('Value'), set(gca, 'FontSize', 20)
    hold on
    subplot(3,1,3), plot(tau,B_SRB4(:,4) , ...
         'LineWidth',2), grid 'on'
         xticks(tau_plot),xticklabels(tau_plot)
         print -depsc Loadings_SS_SRB4

