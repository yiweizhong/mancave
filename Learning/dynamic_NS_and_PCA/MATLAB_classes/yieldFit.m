classdef yieldFit
    %
    % Usage:        getNS     ->  yield and instantaneous forward via Nelson-Siegel
    %               getSS     ->  yield and instantaneous forward via Svensson-Soderlind
    %               getSpline ->  yield and instantaneous forward via Smoothing Spline
    %
    % Description:  This class allows for easy point-in-time estimation, 
    %               interpolation, extrapolation, and forward curve 
    %               calculation on the basis of a provided yield curve. 
    %
    % Inputs:
    % -------
    %               Y:        input term structure, in decimal form, ( nTau-by-1 )
    %                         e.g. 0.05 = 5%
    %
    %               tau:      maturities for the observations in Y 
    %                         should be provided as months (12 and not 1, 
    %                         for the one year point),      ( nTau-by-1 )
    %
    %               tau_out:  maturities at which the output curves are
    %                         reported, in months (12 and not 1, 
    %                         for the one year point),   ( nTauOut-by-1 )
    %
    %               Date:     MATLAB date number for the observed curve,
    %                         e.g. 737209 not '29-May-2018'.
    %                         If provided, the output curves will be dated
    %                         accordingly.                ( 1-by-1 )
    %                         If left empty, today's date is used. 
    %
    % Output:
    % -------
    %               yield:    Fitted zero curve
    %               forward:  Instantaneous forward curve
    %
    % Ken Nyholm
    % June 2018
    % Version 1.0
    %

    properties
        Y double          = NaN(11,1);
        tau double        = [ 3 12:12:120 ]';
        tauOut double     = [ 3 12:12:120 ]';
        date double       = today();
        flagg double 
        maturityOut double
        Instruments double
        optOptions struct
        yield double 
        forward double
        parameters double
    end
    
    methods (Access = 'private')
        function [yieldFit] = chkInput(yieldFit)    
            % Checking the dimension of the inputs
            % 1-> ok,  0-> problems
            %
            if ( sum(size(yieldFit.Y)==size(yieldFit.tau))==2 && ...
                 size(yieldFit.tauOut,1)>1 && ...
                 size(yieldFit.tauOut,2)==1 )
                yieldFit.flagg = 1;
            else
                yieldFit.flagg = 0;
                disp('Something is wrong: Please check the dimensions of the supplied variables...')
                return
            end
        end
        function [yieldFit] = Transformations(yieldFit)
            % Setting up the input data for the curve calculation 
            %
            settle_      = repmat(yieldFit.date, length(yieldFit.tau), 1);
            maturity_    = busdate( settle_(1,1) + ceil(yieldFit.tau./12.*365) );
            price_       = zero2disc(yieldFit.Y, maturity_, settle_, -1, 8).*100;
            cpn_         = zeros(length(yieldFit.Y),1);
            
            yieldFit.maturityOut = busdate( settle_(1,1) + ceil(yieldFit.tauOut./12.*365) );
            yieldFit.Instruments = [settle_ maturity_ price_ cpn_ ];
            yieldFit.optOptions  = optimset('TolFun', 1e-8, 'TolX', 1e-8, ...
                                            'MaxFunEvals', 1e12, ...
                                            'MaxIter', 1e12, 'Display', 'none' );
            
        end
    end
    methods (Access = 'public')
        function [yieldFit] = getNS(yieldFit)
            [yieldFit] = chkInput(yieldFit);
            [yieldFit] = Transformations(yieldFit);

            b_0  = [ 4.00  -3.00  0.40  1.50 ];
            lb_  = [ 0.00  -inf  -inf   0.00 ];
            ub_  = [ 25.0   inf   inf   inf ];   
            
            fitOptions_  = IRFitOptions(b_0, 'FitType', 'durationweightedprice', ...
                                       'LowerBound', lb_, 'UpperBound', ub_, ...
                                       'OptOptions', yieldFit.optOptions );

            est_NS = IRFunctionCurve.fitNelsonSiegel('Zero', yieldFit.Instruments(1,1), ...
                                                      yieldFit.Instruments, ...
                                                      'Compounding', -1, ...
                                                      'Basis', 8, ...                                                 
                                                      'IRFitOptions', fitOptions_);

            yieldFit.yield      = est_NS.getZeroRates(yieldFit.maturityOut);
            yieldFit.forward    = est_NS.getForwardRates(yieldFit.maturityOut); 
            yieldFit.parameters = est_NS.Parameters;
        end
        
        function [yieldFit] = getSS(yieldFit)
            [yieldFit] = chkInput(yieldFit);
            [yieldFit] = Transformations(yieldFit);
            
            b_0  = [ 4.00  -3.00  0.40  -0.50  3.50  1.50 ];
            lb_  = [ 0.00  -inf  -inf  -inf   0.00  0.00 ];
            ub_  = [ 25.0   inf   inf   inf    inf   inf ];
            
            fitOptions_  = IRFitOptions(b_0, 'FitType', 'durationweightedprice', ...
                                       'LowerBound', lb_, 'UpperBound', ub_, ...
                                       'OptOptions', yieldFit.optOptions );

            est_Svensson = IRFunctionCurve.fitSvensson('Zero', yieldFit.Instruments(1,1), ...
                                                       yieldFit.Instruments, ...
                                                       'Compounding', -1, ...
                                                       'Basis', 8, ...                                                 
                                                       'IRFitOptions', fitOptions_);

            yieldFit.yield   = est_Svensson.getZeroRates(yieldFit.maturityOut);
            yieldFit.forward = est_Svensson.getForwardRates(yieldFit.maturityOut);              
        end
        
        function [yieldFit] = getSpline(yieldFit)
            [yieldFit] = chkInput(yieldFit);
            [yieldFit] = Transformations(yieldFit);
                       
            L = 9.2; S = -1; mu = 1;
            penalty_fx = @(t) exp(L - (L-S)*exp(-t/mu));

            est_Spline = IRFunctionCurve.fitSmoothingSpline('Zero', yieldFit.Instruments(1,1), ...
                                                               yieldFit.Instruments, ...
                                                               penalty_fx, ...
                                                               'Compounding', -1, ...
                                                               'Basis', 8 );

            yieldFit.yield   = est_Spline.getZeroRates(yieldFit.maturityOut); 
            yieldFit.forward = est_Spline.getForwardRates(yieldFit.maturityOut);
        end
    end   
end