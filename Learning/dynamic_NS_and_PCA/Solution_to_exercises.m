% (A) comparing Dynamic Nelson-Siegel and Dynamic Svensson-Soderlind models
%
% Load data
load('Data_GSW.mat'); 
GSW_         = GSW;                  % creates an instance of the GSW class
GSW_.tau     = [3 12:12:120]';       % vector of maturities
GSW_.beta    = GSW_factors(:,2:5);   % yield curve factors
GSW_.lambda  = GSW_factors(:,6:7);   % lambdas
GSW_         = GSW_.getYields;       % getting yields

dates = GSW_factors(:,1);
Y     = GSW_.yields;
tau   = GSW_.tau;
nTau  = size(tau,1);

figure('units','normalized','outerposition',[0 0 1 1])
    surf(tau./12,dates,Y)
    date_ticks = datenum(1960:4:2020,1,1);
    set(gca, 'ytick', date_ticks);
    datetick('y','mmm-yy','keepticks')
    xticks(0:1:11), xticklabels({tau}),
    xlabel('Maturity (months)'), zlabel('Yield (pct)'), 
    view([-109 38]),
    ytickangle(-25),
    set(gca, 'FontSize', 18)
%
% Estimate the two models   
%
DNS          = TSM;            % create an instance of the TSM class
DNS.yields   = Y;              % assign data to the DNS instance 
DNS.tau      = tau;
DNS.nF       = 3;
DNS.DataFreq = 12;
DSS          = DNS;            % make a copy of the DNS instance

DNS          = DNS.getDNS;     % estimate the DNS model
DSS          = DSS.getDSS;     % estimate the DSS model

%
% A1. compare factors
figure 
    subplot(4,1,1), plot (dates,[DNS.beta(1,:)' DSS.beta(1,:)' ])
    set(gca, 'xtick', date_ticks)
    datetick('x','mmm-yy','keepticks')
    title('Factor 1')
    legend('DNS','DSS')
    
    subplot(4,1,2), plot (dates,[DNS.beta(2,:)' DSS.beta(2,:)' ])
    set(gca, 'xtick', date_ticks)
    datetick('x','mmm-yy','keepticks')
    title('Factor 2')
    legend('DNS','DSS')

    subplot(4,1,3), plot (dates,[DNS.beta(3,:)' DSS.beta(3,:)' ])
    set(gca, 'xtick', date_ticks)
    datetick('x','mmm-yy','keepticks')
    title('Factor 3')
    legend('DNS','DSS')

    subplot(4,1,4), plot (dates,[DSS.beta(4,:)' ])
    set(gca, 'xtick', date_ticks)
    datetick('x','mmm-yy','keepticks')
    title('Factor 4')
    legend('DSS')

%
% A2. compare factor loadings
figure
    plot(tau,[DNS.B DSS.B])
    set(gca, 'xtick', tau)
    ylim([0 1.5])

%
% A3. compare term premia
figure
    plot(dates,[DNS.TP(:,2) DSS.TP(:,2) ])
    set(gca, 'xtick', date_ticks)
    datetick('x','mmm-yy','keepticks')
    title('1-year term premium')
    legend('DNS','DSS')
    
figure
    plot(dates,[DNS.TP(:,11) DSS.TP(:,11) ])
    set(gca, 'xtick', date_ticks)
    datetick('x','mmm-yy','keepticks')
    title('10-year term premium')
    legend('DNS','DSS')
%
% A4. compare the root mean squared error of the two models
figure
    bar(DNS.tau,[DNS.RMSE' DSS.RMSE'])
    legend('DNS','DSS')
    ylabel('RMSE (basis points)')
    xlabel('Maturity in months')
    
%
% A5. rotate the DNS factors to short rate, slope, curvature
A = [ 1   1  0  ;
      0  -1  0  ; 
      0   0  1 ];
    
X_rotate = A*DNS.beta;
figure
    plot(dates,[X_rotate(1,:)' DNS.yields(:,1) ])
    set(gca, 'xtick', date_ticks)
    datetick('x','mmm-yy','keepticks')
    title('Rotated factor')
    legend('Rotated DNS factor','3-m yield')
