function [R,S,T,U,Mean0,Cov0,StateType] = pMap( p, tau )
%
% Setting up the matrices necessary to estimate the state-space model
%
nTau_1 = length(tau);
nTau   = max(tau);

cP  = p(1,1);
aP  = p(2,1);
s   = p(3,1);
L0  = p(4,1);  %lambda0
L1  = p(5,1);  %lambda1
sY  = p(6:end,1);

cQ  = cP - s*L0;
aQ  = aP - s*L1;

[ a_tau, b_tau ] = find_a_b(s,cQ,aQ,tau);

R = [ aP cP; 0 1 ];
S = [ s; 0 ];
T = [ b_tau a_tau; 1 0; 0 1 ];  
U = [ diag(sY); zeros(2,nTau_1) ];

% ... other assignments
    Mean0     = [];
    Cov0      = [];
    StateType = [ 0; 1 ];    
end

function [a_n, b_n] = find_a_b(s,cQ,aQ,tau)
% determines the loadings and the constant vector using the 
% recursive equations and closed form expressions. 
%
flagg = 1;    % 1-> closed form results, 0->iterative solution

a_nF = @(a_,n_,c_,s_) -c_/(1-a_)*( n_ - (1-a_.^n_)/(1-a_) )...
                      +(s_^2)/(2*((1-a_)^2)).*(n_ + ...
                      (1-a_.^(2*n_))./(1-a_^2) - 2*(1-a_.^n_)./(1-a_) );
b_nF = @(a_,n_) -(1-a_.^n_)./((1-a_));

if (flagg==0)
    nTau = max(tau(:));
    ttau = (1:1:nTau)';
    a_t  = zeros(nTau,1);
    b_t  = zeros(nTau,1);
    for (j=2:nTau+1)
        b_t(j,1) = b_t(j-1,1)*aQ - 1;
        a_t(j,1) = a_t(j-1,1) + b_t(j-1,1)*cQ - 0.5*s^2*(b_t(j-1,1))^2;
    end
    a_n = -a_t(tau+1,1)./tau;
    b_n = -b_t(tau+1,1)./tau;
else
    a_n = -a_nF(aQ,tau,cQ,s)./tau;
    b_n = -b_nF(aQ,tau)./tau;
end
end
