function [err2,pp_] = min_prem( p, n_, P_target, r_t, c_ )
%
    a_   = p(1,1);
    r_n  = @(r_t, c_, a_, n_) r_t.*(1-a_.^n_)/(1-a_) + c_.*(a_.^n_-a_.*n_+n_-1)./(a_-1)^2 ;
    pp_  = exp(-r_n( r_t, c_ ,a_ ,n_ ).*(1/12)).*100;
    err2 = sum((P_target-pp_).^2); 
end