import numpy as np
import book_format
import kf_book.book_plots as book_plots
from filterpy.discrete_bayes import normalize

#####

### original starting point

### belief reflects the dog can be any of the 10 locations
belief = np.array([1/10]*10) #
hallway = np.array([1, 1, 0, 0, 0, 0, 0, 0, 1, 0]) # 1 for door and 0 for hallway
book_plots.bar_plot(belief)


### after the dog moved the sensor detected it is a door
### update the believe naively, to reflect the probabilities
belief = np.array([1/3, 1/3, 0, 0, 0, 0, 0, 0, 1/3, 0])
book_plots.bar_plot(belief)

### Say we get a reading of door, and suppose that testing shows that the sensor 
### is 3 times more likely to be right than wrong. 
### We should scale the probability distribution by 3 where there is a door. 
### If we do that the result will no longer be a probability distribution, 
### but we will learn how to fix that in a moment.

def update_belief(hall, belief, z, correct_scale):
    for i, val in enumerate(hall):
        if val == z:
            belief[i] *= correct_scale


belief = np.array([0.1] * 10)
reading = 1 # 1 is 'door'
update_belief(hallway, belief, z=reading, correct_scale=3.)
print('belief:', belief)
print('sum =', sum(belief)) ### sum of belief is not 1 anymore
belief = belief / sum(belief)
#plt.figure()
book_plots.bar_plot(belief)


### a better version of the above code which avoided for loop with numpy

def scaled_update(hall, belief, z, z_prob): 
    scale = z_prob / (1. - z_prob)
    belief[hall==z] *= scale
    normalize(belief)

belief = np.array([0.1] * 10)
scaled_update(hallway, belief, z=1, z_prob=.75)

print('sum =', sum(belief))
print('probability of door =', belief[0])
print('probability of wall =', belief[2])
book_plots.bar_plot(belief, ylim=(0, .3))



### Another term is the likelihood. When we computed belief[hall==z] *= scale 
### we were computing how likely each position was given the measurement. 
### The likelihood is not a probability distribution because it does not sum to one.

### When we talk about the filter's output we typically call the state after 
### performing the prediction the prior or prediction, 
### and we call the state after the update either the posterior or the estimated state.


#step1 update the likelihood
def scaled_update_likelihood(hall, z, z_prob): 
    scale = z_prob / (1. - z_prob)
    likelihood = np.ones(len(hall))
    likelihood[hall==z] *= scale
    return likelihood

#generate the posterior using new likelihood and prior
def generate_posterior(likelihood, prior):
    return normalize(likelihood * prior)



# Assume that the sensor's movement measurement is 80% likely to be correct, 
# 10% likely to overshoot one position to the right, and 10% likely to undershoot to the left. 

def predict_move(belief, move, p_under, p_correct, p_over):
    n = len(belief)
    prior = np.zeros(n)
    for i in range(n):
        prior[i] = (
            belief[(i-move) % n]   * p_correct +
            belief[(i-move-1) % n] * p_over +
            belief[(i-move+1) % n] * p_under)      
    return prior

belief = [0, 0, .4, .6, 0, 0, 0, 0, 0, 0]
prior = predict_move(belief, 2, .1, .8, .1)
book_plots.plot_belief_vs_prior(belief, prior)



# dog tracking example

def lh_hallway(hall, z, z_prob):
    """ compute likelihood that a measurement matches
    positions in the hallway."""
    
    try:
        scale = z_prob / (1. - z_prob)
    except ZeroDivisionError:
        scale = 1e8

    likelihood = np.ones(len(hall))
    likelihood[hall==z] *= scale
    return likelihood


#use the likelihood to update the prior into poterior
#this means incorporating measurement which z=1 indicating
#we are incorporating the measurement of at a door
#The likelihood basiclly saying the unconditional probability of having a door
#which can be seen on the chart for the 3 possible location
#The measurement is not 100% certain Therefore it is a 3 to 1 odds z=1
#with the likelihood the piror is updated to be posterior

from filterpy.discrete_bayes import update, predict

hallway = np.array([1, 1, 0, 0, 0, 0, 0, 0, 1, 0])
prior = np.array([.1] * 10)
likelihood = lh_hallway(hallway, z=1, z_prob=.75)
posterior = update(likelihood, prior)
book_plots.bar_plot(likelihood, ylim=(0, 5))
book_plots.plot_prior_vs_posterior(prior, posterior, ylim=(0,.5))


#next with the posterior we are going to make a new prior for the next round
#this means we will do a predict based on theritical model.
#for a 1 step prediction we used a kernel density (0.1, 0.8, 0.1) to predict the 
# the prior for the next round. That is how the prediction uncertainities are
#incorporated into the process

kernel = (.1, .8, .1)
prior = predict(posterior, 1, kernel)
book_plots.plot_prior_vs_posterior(prior, posterior, True, ylim=(0,.5))


#next measurement again comes back with a door,
#this doesnt change the likelihood as it is unconditional
#but the update step has increased the probability on pos 1 in the posterior 
#plot. this means we are incorporating both round 1 and round 2 into the output


likelihood = lh_hallway(hallway, z=1, z_prob=.75)
posterior = update(likelihood, prior)
book_plots.plot_prior_vs_posterior(prior, posterior, ylim=(0,.5))


#make another prediction to get the new prior again 
#given the above output is showing we are most likely at pos 1 the outcome
#tells us the predict next location is at 2

prior = predict(posterior, 1, kernel)

# measurement comes back as a hall way this time. again this is unconditional
likelihood = lh_hallway(hallway, z=0, z_prob=.75)

#posterio would reflect the fact both prediction and measurement are suggesting 
#it is most likely at pos 2
 
posterior = update(likelihood, prior)
book_plots.plot_prior_vs_posterior(prior, posterior, ylim=(0,.5))


#next round 

#make the prior from the posterior aka make a prediction using model and uncertainty
#kernel
prior = predict(posterior, 1, kernel)
#meaurement comes back as a hallway (ie conforms the prediction)

likelihood = lh_hallway(hallway, z=0, z_prob=.75)

#posterior is convincing reflecting the confirmation between prediction and measurements
# that we are at 3 now
posterior = update(likelihood, prior)

book_plots.plot_prior_vs_posterior(prior, posterior, ylim=(0,.5))