function [MOMENTS, COV]=autoc(data,n,lags);

% computes autocorrelation


% n= no of autocorrelations 

T = size(data,1);

m1=mean(data);
m2=sum((data(1:T-1)-mean(data(1:T-1))).^2)/(T-1);
 
mi = zeros(n,1);

y=data;
yi = zeros(T,n);


for i=1:n 
  y=data - sum(data(i+1:T))/(T-i);
  temp = data - sum(data(1:T-i))/(T-i);
  yi(:,i)=[zeros(i,1);temp(1:T-i)];
  mi(i) = sum(y.*yi(:,i))/(T-i);
end

MEAN = m1;
VAR = m2;
RHO = mi ./ VAR;  % autocorrelations 

%  Moment conditions @

h1 = data - m1;
h2 = h1.^2 - m2;

h=[h1 h2];

y=data-mean(data);
for i=1:n 
  hi = y.*yi(:,i)-mi(i);
  h= [h hi];
end

nobs = T-n;
R = h'*h/nobs;
n_moving=lags;

for i=1:n_moving;
   R_temp=h(i+1:nobs,:)'*h(1:nobs-i,:)/nobs;
   R=R+(R_temp'+R_temp)*(1-i/(n_moving+1));
end
W=inv(R);

%@ Derivative matrix @

D = -eye(n+2);

%@ Covariance matrix for moment conditions @

COV=inv(D'*W*D);


%@ Standard errors for functions of interest @


D=eye(n+2);

for i=1:n
  D(i+2,2)=-mi(i)/m2^2;
  D(i+2,i+2) = 1/m2;
end

COV = D*COV*D'/nobs;

MOMENTS = [MEAN;VAR;RHO];











