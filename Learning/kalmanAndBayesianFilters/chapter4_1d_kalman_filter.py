#format the book
%matplotlib inline
import book_format
book_format.set_style()
import filterpy.stats as stats
from collections import namedtuple




gaussian = namedtuple('Gaussian', ['mean', 'var'])

#
def predict(posterior:gaussian, forecast:gaussian)->gaussian:
    '''
    In the case kinetic energy. the posterior represents the pos while the
    forecast represents the movements. The output of the predict is the prior
    distribution (which is the distribution of the new pos after the comibining
    the knowledge of the model and posterior)
    
    Parameters
    ----------
    posterior : gaussian
        This is the gaussian distribution of the previous iteration's posterior
        distribution.
    forecast : gaussian
        This is the process/model/forecast which is itself a normal distribution.

    Returns
    -------
    gaussian - representing the prior distribution

    '''
    
    return gaussian(posterior.mean+forecast.mean, posterior.var + forecast.var)

    




g1 = gaussian(10, 1)
g2 = gaussian(10.5, 1)
g3 = predict(g1, g2)

#plot a normal distribution
stats.plot_gaussian_pdf(mean=g1.mean, variance=g1.var, xlim=(4, 16), ylim=(0, .5));



