function outVol = normalvolbysabr(Alpha, Beta, Rho, Nu, Settle, ExerciseDate, ForwardValue, Strike, varargin)
%NORMALVOLBYSABR Implied Normal (Bachelier) volatility by SABR model.
%   NORMALVOLBYSABR returns the implied Normal (Bachelier) volatility by
%   using the SABR stochastic volatility model.
%
%   outVol = normalvolbysabr(Alpha, Beta, Rho, Nu, Settle, ExerciseDate, ...
%            ForwardValue, Strike)
%   outVol = normalvolbysabr(Alpha, Beta, Rho, Nu, Settle, ExerciseDate, ...
%            ForwardValue, Strike, Parameter, Value, ... )
%
%   Inputs:
%     Alpha - Current SABR volatility parameter. (Scalar)
%     Beta  - SABR CEV exponent. (Scalar) Set this parameter to zero to
%             allow negative ForwardValue and Strike.  
%     Rho   - Correlation between forward value and volatility. (Scalar)
%     Nu    - Volatility of volatility. (Scalar)
%     Settle  - Settlement date. Scalar serial date number or a date
%               character vector. 
%     ExerciseDate - Option exercise date. Scalar serial date number or a
%                    date character vector.
%     ForwardValue - Current forward value of the underlying asset.
%                    (Scalar or vector of size NumVols x 1)
%     Strike   - Option strike. (Scalar or vector of size NumVols x 1)
%
%   Optional Parameter-Value Pairs:
%     'Basis' - Day-count basis.
%               0 = actual/actual
%               1 = 30/360 (SIA)
%               2 = actual/360
%               3 = actual/365
%               4 = 30/360 (BMA)
%               5 = 30/360 (ISDA)
%               6 = 30/360 (European)
%               7 = actual/365 (Japanese)
%               8 = actual/actual(ICMA)
%               9 = actual/360 (ICMA)
%               10 = actual/365(ICMA)
%               11 = 30/360E (ICMA)
%               12 = actual/actual(ISDA)
%               13 = BUS/252
%               Default: 0
%
%   Output:
%     outVol - Implied Normal (Bachelier) volatility computed by SABR model.
%              (Scalar or vector of size NumVols x 1)
%   Example:
%
%    ForwardRate = 0.0357;
%    Strike = 0.03;
%    Alpha = 0.036;
%    Beta = 0.5;
%    Rho = -0.25;
%    Nu = 0.35;
% 
%    Settle = datenum('25-Sep-2017');
%    ExerciseDate = datenum('25-Sep-2019');
%   
%    ComputedVols = normalvolbysabr(Alpha, Beta, Rho, Nu, Settle, ...
%      ExerciseDate, ForwardRate, Strike)
%
%   References:
%     Hagan, P. S., Kumar, D., Lesniewski, A. S. and Woodward, D. E.,
%       Managing smile risk, Wilmott Magazine, 2002.


%  Copyright 2018 The MathWorks, Inc.

%% Input argument checking

% Number of inputs must be >=8 and <=12.
narginchk(8, 10);

if (~isnumeric(Alpha))||(~isscalar(Alpha))||(Alpha<=0)
    error(message('fininst:normalvolbysabr:invalidAlpha'));
end

if (~isnumeric(Nu))||(~isscalar(Nu))||(Nu<=0)
    error(message('fininst:normalvolbysabr:invalidNu'));
end

if (~isnumeric(Beta))||(~isscalar(Beta))||(Beta<0)||(Beta>1)
    error(message('fininst:normalvolbysabr:invalidBeta'));
end

if (~isnumeric(Rho))||(~isscalar(Rho))||(Rho<-1)||(Rho>1)
    error(message('fininst:normalvolbysabr:invalidRho'));
end

if (1-Rho)>=0 && (1-Rho)<1e-12
    Rho = 1-1e-12; % Upper bound on Rho.
end

if (~isnumeric(ForwardValue))||(~isvector(ForwardValue))
    error(message('fininst:normalvolbysabr:invalidForwardValue'));
end
    
if (~isnumeric(Strike))||(~isvector(Strike))
    error(message('fininst:normalvolbysabr:invalidStrike'));
end

try
    Settle = datenum(Settle);
    ExerciseDate = datenum(ExerciseDate);
catch ME
    newMsg = message('fininst:normalvolbysabr:invalidSettleExerciseDate');
    newME = MException(newMsg.Identifier,getString(newMsg));
    newME = addCause(newME,ME);
    throw(newME)
end

if (~isscalar(Settle))||(~isscalar(ExerciseDate))
    error(message('fininst:normalvolbysabr:invalidSettleExerciseDate'));
end

if (Settle > ExerciseDate)
    error(message('fininst:normalvolbysabr:ExerciseDateBeforeSettle'));
end

p = inputParser;
p.addParameter('Basis',0,@(x) all(isvalidbasis(x))&&isscalar(x));

try
   p.parse(varargin{:});
catch ME
    newMsg = message('fininst:normalvolbysabr:invalidInputs');
    newME = MException(newMsg.Identifier,getString(newMsg));
    newME = addCause(newME,ME);
    throw(newME)
end

Basis = p.Results.Basis;

BetaZeroTolerance = 1e-15;
if any(ForwardValue<=0)&&(Beta>BetaZeroTolerance)
    error(message('fininst:normalvolbysabr:ForwardValueTooLow'));
end

if any(Strike<=0)&&(Beta>BetaZeroTolerance)
    error(message('fininst:normalvolbysabr:StrikeTooLow'));
end

Time = yearfrac(Settle, ExerciseDate, Basis);

[ForwardValue,Strike] = finargsz(1,ForwardValue(:),Strike(:));

NumVols = length(Strike);
outVol = zeros(NumVols,1);

if (Beta <= BetaZeroTolerance) % Special case: Beta is 0 (allows negative rates)
    % At-the-money, (ForwardValue == Strike)
    ATMidx = find(abs(ForwardValue - Strike) <= 1e-6);
    V2 = (1 + ((2 - 3.*Rho.^2).*Nu.^2./24).*Time);
    
    outVol(ATMidx) = Alpha.*V2;
    % Not at-the-money, (ForwardValue ~= Strike)
    NATMidx = setdiff((1:NumVols)',ATMidx);
    z = Nu./Alpha.*(ForwardValue(NATMidx) - Strike(NATMidx));       
    x = log((sqrt(1 - 2.*Rho.*z + z.^2) + z - Rho)./(1 - Rho)); 
    display(V2)
    display(z)
    display(x)    
    outVol(NATMidx) = Nu.*(ForwardValue(NATMidx) - Strike(NATMidx)).*V2./x;
elseif (Beta >= (1 - 1e-7)) % Special case: Beta is 1
    % At-the-money, (ForwardValue == Strike)
    ATMidx = find(abs(ForwardValue - Strike) <= 1e-6);
    V2 = (1 + (-Alpha.^2./24 + 0.25.*Rho.*Nu.*Alpha + ...
        (2 - 3.*Rho.^2).*Nu.^2./24).*Time);
    outVol(ATMidx) = Alpha.*ForwardValue(ATMidx).*V2;
    % Not at-the-money, (ForwardValue ~= Strike)
    NATMidx = setdiff((1:NumVols)',ATMidx);
    z = Nu./Alpha.*log(ForwardValue(NATMidx)./Strike(NATMidx));
    x = log((sqrt(1 - 2.*Rho.*z + z.^2) + z - Rho)./(1 - Rho));    
    outVol(NATMidx) = Nu.*(ForwardValue(NATMidx) - Strike(NATMidx)).*V2./x;
else % Other cases: Beta is neither 0 nor 1
    % At-the-money, (ForwardValue == Strike)
    ATMidx = find(abs(ForwardValue - Strike) <= 1e-6);
    V1 = ForwardValue(ATMidx).^(1 - Beta);
    V2 = (1 + (Beta.*(Beta - 2).*Alpha.^2./24./V1.^2 + ...
        0.25.*Rho.*Beta.*Nu.*Alpha./V1 + ...
        (2 - 3.*Rho.^2).*Nu.^2./24).*Time);
    outVol(ATMidx) = Alpha.*(ForwardValue(ATMidx).^Beta).*V2;
    % Not at-the-money, (ForwardValue ~= Strike)
    NATMidx = setdiff((1:NumVols)',ATMidx);    
    V1 = (0.5.*(ForwardValue(NATMidx) + Strike(NATMidx))).^(1 - Beta);
    z = Nu./Alpha.*(ForwardValue(NATMidx).^(1 - Beta) - Strike(NATMidx).^(1 - Beta))./(1 - Beta);
    x = log((sqrt(1 - 2.*Rho.*z + z.^2) + z - Rho)./(1 - Rho));
    V2 = (1 + (Beta.*(Beta - 2).*Alpha.^2./24./V1.^2 + ...
        0.25.*Rho.*Beta.*Nu.*Alpha./V1 + ...
        (2 - 3.*Rho.^2).*Nu.^2./24).*Time);
    outVol(NATMidx) = Nu.*(ForwardValue(NATMidx) - Strike(NATMidx)).*V2./x;
end % end of if
