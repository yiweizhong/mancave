#include <algorithm>
#include <cassert>
#include <cstddef>
#include <memory>
#include <utility>

// Let's now think about the design of our `vector` clone.
// The `vector` class is both responsible for the management
// of a dynamically-allocated buffer and a `_size` that keeps
// track of how many elements are in the container.

// Let's try to apply the "rule of zero" and move as much
// resource management code as possible in a separate class.
// Luckily, `std::unique_ptr` is a great fit here.

template <typename T>
auto copy_uptr_array(const std::unique_ptr<T[]>& src,
                     std::size_t capacity,
                     std::size_t size)
{
    auto result = std::make_unique<T[]>(capacity);
    std::copy(src.get(), src.get() + size, result.get());
    return result;
}

template <typename T>
class vector
{
private:
    std::unique_ptr<T[]> _data;
    std::size_t _size{0}, _capacity{0};

public:
    vector() = default;

    // Using `= default` here forces the compiler to
    // automatically generate the move operations for us,
    // even though implicitly generation was suppressed
    // due to the presence of copy operations.

    vector(vector&& rhs) = default;
    vector& operator=(vector&& rhs) = default;

    vector(const vector& rhs)
        : _size{rhs._size}, _capacity{rhs._capacity}
    {
        _data = copy_uptr_array(rhs._data, _capacity, _size);
    }

    vector& operator=(const vector& rhs)
    {
        _size = rhs._size;
        _capacity = rhs._capacity;
        _data = copy_uptr_array(rhs._data, _capacity, _size);

        return *this;
    }

    void push_back(const T& x)
    {
        if(_capacity == _size)
        {
            const auto new_capacity = _capacity + 10;
            _data = copy_uptr_array(_data, new_capacity, _size);
            _capacity = new_capacity;
        }

        _data[_size] = x;
        ++_size;
    }

    const auto& at(std::size_t i) const
    {
        assert(i < _size);
        return _data[i];
    }

    auto size() const { return _size; }
    auto capacity() const { return _capacity; }
};

// Due to the fact that our copy operations have to take the
// `_size` member variable into account, the "rule of zero"
// cannot be completely applied in this situation, at least
// not without significant refactoring.

// Being able to `= default` the move operations however
// prevents possible mistakes and increases maintainability
// and readability of the code. Getting rid of the manual
// memory management prevents potential security-critical
// pitfalls that we've seen in the previous videos.

int main()
{
    vector<int> v0;
    assert(v0.size() == 0);
    assert(v0.capacity() == 0);

    v0.push_back(5);
    assert(v0.size() == 1);
    assert(v0.capacity() == 10);
    assert(v0.at(0) == 5);

    auto v1 = v0;
    assert(v1.size() == 1);
    assert(v1.capacity() == 10);
    assert(v1.at(0) == 5);

    auto v2 = std::move(v0);
    assert(v2.size() == 1);
    assert(v2.capacity() == 10);
    assert(v2.at(0) == 5);
}
