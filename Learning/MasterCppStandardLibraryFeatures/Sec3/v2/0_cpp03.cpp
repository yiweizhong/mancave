#include <algorithm>
#include <cassert>
#include <cstddef>

// In this snippet we'll look at a barebones `std::vector`
// implementation, designed as if move semantics did not
// exist. This will provide a real-world example of the
// "rule of three" in use.

// Please be aware that the code below:
// * Doesn't deal with exception safety
// * Doesn't deal with deferred construction of objects
// * Doesn't provide the entire `std::vector` interface
// This is just an example focused on resource management and
// should not be seen as a valid `std::vector` implementation.

template <typename T>
class vector
{
private:
    T* _data{nullptr};
    std::size_t _size{0}, _capacity{0};

public:
    vector() = default;
    ~vector() { delete[] _data; }

    vector(const vector& rhs)
        : _size{rhs._size}, _capacity{rhs._capacity}
    {
        _data = new T[_capacity];
        std::copy(rhs._data, rhs._data + _size, _data);
    }

    vector& operator=(const vector& rhs)
    {
        _size = rhs._size;
        _capacity = rhs._capacity;

        _data = new T[_capacity];
        std::copy(rhs._data, rhs._data + _size, _data);

        return *this;
    }

    void push_back(const T& x)
    {
        if(_capacity == _size)
        {
            const auto new_capacity = _capacity + 10;
            T* tmp = new T[new_capacity];
            std::copy(_data, _data + _capacity, tmp);
            std::swap(tmp, _data);
            delete[] tmp;

            _capacity = new_capacity;
        }

        _data[_size] = x;
        ++_size;
    }

    const auto& at(std::size_t i) const
    {
        assert(i < _size);
        return _data[i];
    }

    auto size() const { return _size; }
    auto capacity() const { return _capacity; }
};

int main()
{
    vector<int> v0;
    assert(v0.size() == 0);
    assert(v0.capacity() == 0);

    v0.push_back(5);
    assert(v0.size() == 1);
    assert(v0.capacity() == 10);
    assert(v0.at(0) == 5);

    auto v1 = v0;
    assert(v1.size() == 1);
    assert(v1.capacity() == 10);
    assert(v1.at(0) == 5);
}
