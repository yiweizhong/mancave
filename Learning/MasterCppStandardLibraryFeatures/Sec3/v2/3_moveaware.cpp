#include <algorithm>
#include <cassert>
#include <cstddef>
#include <memory>
#include <utility>

// As a last major improvement, let's make our `push_back`
// function move-aware: it will move `T` instances inside
// the container instead of copying whenever possible.

template <typename T>
auto copy_uptr_array(const std::unique_ptr<T[]>& src,
                     std::size_t capacity,
                     std::size_t size)
{
    auto result = std::make_unique<T[]>(capacity);
    std::copy(src.get(), src.get() + size, result.get());
    return result;
}

template <typename T>
class vector
{
private:
    std::unique_ptr<T[]> _data;
    std::size_t _size{0}, _capacity{0};

public:
    vector() = default;

    vector(vector&& rhs) = default;
    vector& operator=(vector&& rhs) = default;

    vector(const vector& rhs)
        : _size{rhs._size}, _capacity{rhs._capacity}
    {
        _data = copy_uptr_array(rhs._data, _capacity, _size);
    }

    vector& operator=(const vector& rhs)
    {
        _size = rhs._size;
        _capacity = rhs._capacity;
        _data = copy_uptr_array(rhs._data, _capacity, _size);

        return *this;
    }

    template <typename U>
    void push_back(U&& x)
    {
        if(_capacity == _size)
        {
            const auto new_capacity = _capacity + 10;
            _data = copy_uptr_array(_data, new_capacity, _size);
            _capacity = new_capacity;
        }

        _data[_size] = std::forward<U>(x);
        ++_size;
    }

    const auto& at(std::size_t i) const
    {
        assert(i < _size);
        return _data[i];
    }

    auto size() const { return _size; }
    auto capacity() const { return _capacity; }
};

// By using perfect-forwarding we can avoid code repetition
// and provide a member function that works for both lvalues
// and rvalues, moving where possible in order to increase
// performance and provide support for move-only types.

// Note that a real `vector` implementation would have used
// "placement `new`" to conditionally construct objects in
// the buffer instead of invoking the copy/move constructors.

int main()
{
    vector<int> v0;
    assert(v0.size() == 0);
    assert(v0.capacity() == 0);

    v0.push_back(5);
    assert(v0.size() == 1);
    assert(v0.capacity() == 10);
    assert(v0.at(0) == 5);

    auto v1 = v0;
    assert(v1.size() == 1);
    assert(v1.capacity() == 10);
    assert(v1.at(0) == 5);

    auto v2 = std::move(v0);
    assert(v2.size() == 1);
    assert(v2.capacity() == 10);
    assert(v2.at(0) == 5);
}
