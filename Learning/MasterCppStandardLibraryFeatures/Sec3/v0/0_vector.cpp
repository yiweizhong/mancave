#include <vector>
#include <iostream>

// Almost every container in the Standard Library
// is "move-aware". This means that it will use
// move operations (if available) when dealing
// with its items.

// Let's begin by looking at `std::vector`.

// Assuming that `foo` is a class that supports
// move semantics, we can see that `vector` will
// try to move whenever possible.

struct foo
{
    foo()  { std::cout << "foo()\n"; }
    ~foo() { std::cout << "~foo()\n"; }

    foo(const foo&) { std::cout << "foo(const foo&)\n"; }
    foo(foo&&)      { std::cout << "foo(foo&&)\n"; }
};

void vector_move_awareness()
{
    std::vector<foo> v;
    v.reserve(10);
    foo f0;

    // Copies `f0`
    v.push_back(f0);

    // Moves `f0`
    v.push_back(std::move(f0));

    // Moves `foo` temporary
    v.push_back(foo{});
}

// Containers allow us to go one step further, and
// sometimes allow us to entirely avoid the move.
// This can happen when an item is being constructed
// "in place" inside the container.

// This operation is called "emplacement" and is
// supported by most Standard Library containers.

struct bar
{
    int _x;
    bar(int x) : _x{x} { std::cout << "bar(int)\n"; }
    bar(const bar&)    { std::cout << "bar(const bar&)\n"; }
    bar(bar&&)         { std::cout << "bar(bar&&)\n"; }
    bar()              { std::cout << "bar()\n"; }
};

void vector_emplacement()
{
    std::vector<bar> v;
    v.reserve(10);

    // Moves `bar` temporary
    v.push_back(bar{42});

    // Constructs `bar` instance "in place"
    v.emplace_back(42);
}

// Emplacement works by perfectly-forwarding all
// arguments passed to `emplace_back` to the
// constructor of a `bar` instance being created
// directly in the target memory location inside
// the `vector`.

// Note that writing move-aware classes is very
// important even if emplacement exists. Consider
// the case where a "wrapper" class is going to
// be emplaced.

struct bar_wrapper
{
    bar _b;

    bar_wrapper(bar b) : _b{std::move(b)}
    {
    }
};

void vector_emplacement_and_move()
{
    std::vector<bar_wrapper> v;

    // Moves `bar` temporary when constructing
    // `bar_wrapper` "in place"
    v.emplace_back(bar{42});
}

// In the code above, the move operation for `bar`
// cannot be avoided as it happens while constructing
// `bar_wrapper`. This example shows that having
// support for move operations is beneficial even
// when trying to maximize the use of `emplace_back`
// and similar methods.

int main()
{
    std::cout << "vector_move_awareness()\n";
    vector_move_awareness();

    std::cout << "\nvector_emplacement()\n";
    vector_emplacement();
}
