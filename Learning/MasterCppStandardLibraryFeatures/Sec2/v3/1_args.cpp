#include <memory>

// Observing/mutating an object: pass by reference.
void f0(int&);
void f1(const int&);

// Observing/mutating a smart pointer: pass by reference.
void f2(std::unique_ptr<int>&);
void f3(const std::unique_ptr<int>&);
void f4(std::shared_ptr<int>&);
void f5(const std::shared_ptr<int>&);
void f6(std::weak_ptr<int>&);
void f7(const std::weak_ptr<int>&);

// Transferring ownership: pass by value.
void f8(std::unique_ptr<int>);

// Sharing ownership: pass by value.
void f9(std::shared_ptr<int>);

// Observing/mutating an optional object: pass by raw pointer.
void f10(int*);
void f11(const int*);
