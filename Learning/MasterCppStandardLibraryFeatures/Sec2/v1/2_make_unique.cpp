#include <iostream>
#include <stdexcept>
#include <memory>

void foo(std::unique_ptr<int>, int);

int bar()
{
    throw std::runtime_error{"whoops!"};
}

int main()
{
    // Consider the following line of code:
    foo(std::unique_ptr<int>{new int{5}}, bar());

    // There are three orders in which the above
    // `foo` invocation could be executed.

    // Order #0:
    // * Allocate memory for `new int{5}`
    // * Construct `unique_ptr`
    // * Invoke `bar()` and throw

    // Order #1:
    // * Invoke `bar()` and throw
    // * Allocate memory for `new int{5}`
    // * Construct `unique_ptr`

    // Order #2:
    // * Allocate memory for `new int{5}`
    // * Invoke `bar()` and throw
    // * Construct `unique_ptr`

    // In the case of #0 and #1, everything is
    // perfectly safe: the `unique_ptr` cleans up
    // after itself if required and no memory
    // leaks occur.

    // In the case of order #2, there is a memory
    // leak! The `unique_ptr` doesn't get a chance
    // to complete its construction before the
    // exception is thrown. However, the memory
    // for the `int` was already allocated, so it
    // gets leaked.

    // Now, consider this alternative `foo` call:
    foo(std::make_unique<int>(5), bar());

    // Using `std::make_unique` prevents the issue
    // explained above - the above invocation will
    // not interleave an allocation with the call
    // to `bar()`.

    // Additionally, using `make_unique`:
    // * Makes the code terser and more readable
    // * Doesn't require an explicit `new` call
}
