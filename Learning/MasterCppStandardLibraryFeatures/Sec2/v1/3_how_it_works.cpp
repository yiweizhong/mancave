#include <iostream>

// How does `std::unique_ptr` work? Why are move
// semantics necessary for its implementation?

// Let's answer those questions by creating our
// own barebones `my_unique_ptr`.

template <typename T>
class my_unique_ptr
{
private:
    T* _ptr{nullptr};

public:
    my_unique_ptr() = default;
    my_unique_ptr(T* ptr) : _ptr{ptr} { }

    ~my_unique_ptr()
    {
        // Deleting a `nullptr` is perfectly safe.
        delete _ptr;
    }

    // Prevent copies.
    my_unique_ptr(const my_unique_ptr&) = delete;
    my_unique_ptr& operator=(const my_unique_ptr&) = delete;

    // Ownership transfer.
    my_unique_ptr(my_unique_ptr&& rhs)
        : _ptr{rhs._ptr}
    {
        rhs._ptr = nullptr;
    }

    my_unique_ptr& operator=(my_unique_ptr&& rhs)
    {
        _ptr = rhs._ptr;
        rhs._ptr = nullptr;
        return *this;
    }
};

// The above implementation is very simple, but
// demonstrates the concepts behind `unique_ptr`
// that require move semantics to be implemented.

// The destructor of `my_unique_ptr` conditionally
// releases the resource: if the held pointer is
// `nullptr` nothing will be released.

// Copies are forbidden to prevent having multiple
// instances of `my_unique_ptr` that refer to the
// same resource.

// Moves make sure that the "moved-from object"'s
// held pointer is set to `nullptr`, so that it
// loses its ownership over the resource and does
// not release it upon destruction.

int main()
{
}
