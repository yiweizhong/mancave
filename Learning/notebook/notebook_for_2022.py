import seaborn as sns
import pandas as pd
import math
import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats


sample_data=np.random.randint(1,30,20)

std_error=np.std(sample_data, ddof=1) / np.sqrt(len(sample_data))

sample_mean = np.mean(sample_data)

z_critical = stats.norm.ppf(q = 0.95)

confidence_interval=[sample_mean+z_critical * (std_error/math.sqrt(len(sample_data))),sample_mean-z_critical * (std_error/math.sqrt(len(sample_data)))]

df=pd.DataFrame( {'x':sample_data, 'y':sample_data*np.random.normal(0, 1, 20)})


sns.lmplot('x', 'y', df, ci=90)
plt.title('Confidence Band with 90% Confidence Interval')


sns.lmplot('x', 'y', df, ci=99)
plt.title('Confidence Band with 99% Confidence Interval')



sns.tsplot(np.array([[0,1,0,1,0,1,0,1], [1,0,1,0,1,0,1,0], [.5,.5,.5,.5,.5,.5,.5,.5]]))






#Using bootstrapping for error estimate

import numpy as np; np.random.seed(1)
from scipy import stats
import matplotlib.pyplot as plt
import seaborn as sns

x = np.linspace(0, 15, 31)
data = np.sin(x) + np.random.rand(10, 31) + np.random.randn(10, 1)


fig, (ax,ax2) = plt.subplots(ncols=2, sharey=True)
ax = sns.tsplot(data=data,ax=ax)

def bootstrap(data, n_boot=10, ci=68):
    boot_dist = []
    for i in range(int(n_boot)):
        resampler = np.random.randint(0, data.shape[0], data.shape[0])
        sample = data.take(resampler, axis=0)
        boot_dist.append(np.mean(sample, axis=0))
    b = np.array(boot_dist)
    s1 = np.apply_along_axis(stats.scoreatpercentile, 0, b, 50.-ci/2.)
    s2 = np.apply_along_axis(stats.scoreatpercentile, 0, b, 50.+ci/2.)
    return (s1,s2)
    
def tsplotboot(ax, data,**kw):
    x = np.arange(data.shape[1])
    est = np.mean(data, axis=0)
    cis = bootstrap(data)
    ax.fill_between(x,cis[0],cis[1],alpha=0.2, **kw)
    ax.plot(x,est,**kw)
    ax.margins(x=0)

tsplotboot(ax2, data)

ax.set_title("sns.tsplot")
ax2.set_title("custom tsplot")

plt.show()


### http://www.variousconsequences.com/2010/02/visualizing-confidence-intervals.html

import scipy as sp
from scipy.stats import norm
import numpy as np
import itertools as it
import matplotlib.pyplot as plt

nx = 6 
x = sp.linspace(0.0, 1.0, nx) 
data = x + norm.rvs(loc=0.0, scale=0.1, size=nx) 
yp = sp.polyfit(x, data, 1) 
y = sp.polyval(yp,x) 
r = data - y

nperm = math.factorial(nx) 

for perm in it.permutations(r,nx): # loop over all the permutations of the resids
    print(perm) 
    pc = sp.polyfit(x, y + perm, 1) 
    plt.plot(x, sp.polyval(pc,x), 'k-', linewidth=2, alpha=2.0/float(nperm)) 
plt.plot(x, y, 'k-') 
plt.plot(x, data, 'ko')



# the above permutation is process intensive
# achieve the same result using random sampling

bootindex = sp.random.random_integers
 
nboot = 400 
for i in np.arange(nboot): # loop over n bootstrap samples from the resids 
    pc = sp.polyfit(x, y + r[bootindex(0, len(r)-1, len(r))], 1) 
    plt.plot(x, sp.polyval(pc,x), 'k-', linewidth=2, alpha=3.0/float(nboot)) 
plt.plot(x, y, 'k-') 
plt.plot(x, data, 'ko')




data = np.array([[2,5,5,6,8],[5,4,1,2,9]])
data_cov = np.cov(data)
data_corr = np.corrcoef(data)
data_cov_d = np.sqrt(np.diag(np.diag(data_cov)))


#data_cov_d.transpose().dot(data_cov) same as @ 
np.linalg.inv(data_cov_d) @ data_cov @ np.linalg.inv(data_cov_d)










