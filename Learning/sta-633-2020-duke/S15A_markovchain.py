import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import quantecon as qe
from mpl_toolkits.mplot3d import Axes3D

'''
https://nbviewer.jupyter.org/github/cliburn/sta-663-2020/blob/master/notebooks/S15A_MarkovChains.ipynb
https://python.quantecon.org/finite_markov.html
'''

#
n = 1000 #1000 sample
k = 100 #100 steps per sample
# np.random.choice([-1,1], (n, k)) - 1000x100 matrix of random (1 or -1)
# attach a column of 0 at the front. therefore the xs is 1000 x 101
xs = np.c_[np.zeros(n), np.random.choice([-1,1], (n, k))]
xs = np.cumsum(xs, axis=1)

#xs is 1000 samples of 100 steps from 0 of random +-1 cumsumed

for x in xs:
    plt.plot(np.arange(k+1), x)
    
    
#transition matrix A

A = np.array([
    [0.9, 0.05, 0.05],
    [0.1, 0.8, 0.1],
    [0.04, 0.01, 0.95]
])

# starting distribution

x = np.array([300000, 300000, 300000]).reshape(-1,1)


# move forward 1 step

x.T @ A

#array([[312000., 258000., 330000.]])


# move forward 2 steps

x.T @ A @ A

#rray([[319800., 225300., 354900.]])



#############################################################
# brute force 100 steps forward and reached the steady states
#############################################################

n = 100
x.T @ np.linalg.matrix_power(A, n)

#array([[300000.50469469, 100000.21203929, 499999.28326602]])

#############################################################
# Eigenvector solution the steady states
#############################################################

'''
1. In this case the transition matrix is row based, ie each row adds up to 100
2. x.shape is (3,1), xT is x transposed
3. xi_T @ A = xi+1_T, therefore 
    A_T @ xi = Xi+1 therefore 
    A_T @ A_T @ xi = Xi+2 therefore 
    A_T ^ n @ xi = Xi+n 
    and xi+n is considered steady state if A_T @ xi+n = xi+n

4. 

E, V = np.linalg.eig(A.T)
_A_T = V @ np.diag(E) @ np.linalg.inv(V)


A.T
Out[19]: 
array([[0.9 , 0.1 , 0.04],
       [0.05, 0.8 , 0.01],
       [0.05, 0.1 , 0.95]])

_A_T
Out[20]: 
array([[0.9 , 0.1 , 0.04],
       [0.05, 0.8 , 0.01],
       [0.05, 0.1 , 0.95]])

Therefore in order to find the steady state (using xi @ A_T ^ n)

A_T ^ n @ xi =
V @  np.diag(E) ^ n @ np.linalg.inv(V) @ xi

set lambdas = np.diag(E)
and set lambdas[1,1] and lambdas[2,2] to be 0 because when steady state is reached
the diag terms lambdas[1,1] and lambdas[2,2] have to be very small under power of 
n for continue multiply A_T to have no effect. 

https://medium.com/@andrew.chamberlain/using-eigenvectors-to-find-steady-state-population-flows-cd938f124764


       
'''

E, V = np.linalg.eig(A.T)
_A_T = V @ np.diag(E) @ np.linalg.inv(V)
lambdas = np.diag(E)
lambdas[1,1] = 0
lambdas[2,2] = 0

'''steady sate prob'''

V @ lambdas @ np.linalg.inv(V) @ x 

'''
Out[36]: 
array([[300000.],
       [100000.],
       [500000.]])
'''

'''if not the actual number but using initial prob'''
V @ lambdas @ np.linalg.inv(V) @ np.array([1/3,1/3,1/3]).reshape(3,1)


'''
Out[54]: 
array([[0.33333333],
       [0.11111111],
       [0.55555556]])
'''



#############################################################
# linear system solution the steady states
#############################################################

'''
1. In this case the transition matrix is row based, ie each row adds up to 100
2. x.shape is (3,1), xT is x transposed
3. xi_T @ A = xi+1_T, therefore 
    A_T @ xi = Xi+1, therefore at steady state
    A_T @ x_steady = x_steady, therefore
    (A_T -I) @ x_steady = 0


4.

A.T - np.eye(A.shape[0])

Out[61]: 
array([[-0.1 ,  0.1 ,  0.04],
       [ 0.05, -0.2 ,  0.01],
       [ 0.05,  0.1 , -0.05]])

however the rank of the output is 2 therefore it is not enough to solve the for 
steady state probs

np.linalg.matrix_rank(A.T - np.eye(A.shape[0]))
Out[63]: 2

5. however the prob of steady state should sum up to 1 that give another dimension
to solve the linear equation


'''


M = np.r_[A.T - np.eye(3), [np.ones(3)]]
b = np.r_[np.zeros(3), 1].reshape(-1,1)

'''
M
Out[65]: 
array([[-0.1 ,  0.1 ,  0.04],
       [ 0.05, -0.2 ,  0.01],
       [ 0.05,  0.1 , -0.05],
       [ 1.  ,  1.  ,  1.  ]])

b
Out[67]: 
array([[0.],
       [0.],
       [0.],
       [1.]])

'''

x, res, rank, s = np.linalg.lstsq(M, b, rcond=None)

'''

x, res, rank, s = np.linalg.lstsq(M, b, rcond=None)

res
Out[69]: array([2.12643633e-34])

rank
Out[70]: 3

s
Out[71]: array([1.73509911, 0.23570109, 0.11435062])

x
Out[72]: 
array([[0.33333333],
       [0.11111111],
       [0.55555556]])

'''




#############################################################
# simulation solution the steady states
#############################################################

P_cdf = np.cumsum(A, axis=1)

'''
1. 

in order to map uniform random draw to the prob distribution of transition matrix
A, first create a CDF for A. Its row based here. So each row is a cdf for that state.
and state is represented by the row indice 0, 1, 2. therefore if we draw a number
out of uniformly distribted 0 to 1 from a initial state we can map to where it
ends next using prob simulation

np.cumsum(A, axis=1)

array([[0.9 , 0.95, 1.  ],
       [0.1 , 0.9 , 1.  ],
       [0.04, 0.05, 1.  ]])
'''

n = int(1e6)
xs = np.zeros(n+1).astype('int')
xs[0] = np.random.choice([0,1,2]) #randomly choose a starting state

'''
simulate the draw many times 
xs[t+1] represents the simulateion output of 1 draw which tells you what the
next state is (by chance of P_cdf)

P_cdf[xs[t]] takes out the relevent row from P_cdf matrix. xs[?] is {0, 1, 2}
np.argmax(r < P_cdf[xs[t]]) finds the max indice from the relevent row and that 
indice becomes the next state. and at the next loop it will be that state's
prob row being used

'''
for t in range(n):
    r = np.random.rand()
    xs[t+1] = np.argmax(r < P_cdf[xs[t]])

np.mean(xs==0), np.mean(xs==1), np.mean(xs==2)

'''

np.mean(xs==0), np.mean(xs==1), np.mean(xs==2)
Out[85]: (0.3345006654993345, 0.11186588813411187, 0.5536334463665536)

'''

