import numpy as np
import matplotlib.pyplot as plt



#########################################################################
# array
#########################################################################

x = np.array([1,2,3])

type(x)
'''
Out[89]: numpy.ndarray
'''

x.dtype
'''
Out[90]: dtype('int32')
'''

x.strides
'''
(4,)

The strides of an array tell us how many bytes we have to skip in memory to 
move to the next position along a certain axis. For example, we have to skip 
4 bytes (1 value) to move to the next column, but 20 bytes (5 values) to get 
to the same position in the next row.

'''

x.shape
'''
(3,)
'''
x.ndim
'''
1
'''
x.T @ x
'''
14
'''
(x.T @ x).dim
'''

(x.T @ x).dim
Traceback (most recent call last):

  File "<ipython-input-100-8aa905c3ee06>", line 1, in <module>
    (x.T @ x).dim

AttributeError: 'numpy.int32' object has no attribute 'dim'

'''




x.reshape(3,1).shape
'''
(3,1)
'''

x.reshape(3,1).ndim
'''
2
'''

x.reshape(3,1).T @ x.reshape(3,1)
'''
array([[14]])
'''
(x.reshape(3,1).T @ x.reshape(3,1)).ndim
'''
2
'''

#########################################################################
# matrix
#########################################################################

x = np.array([[1,2,3], [4,5,6]], dtype=np.int32)
print(x)
print(x.dtype)
print(x.shape)
print(x.strides)

'''
x = np.array([[1,2,3], [4,5,6]], dtype=np.int32)
print(x)
print(x.dtype)
print(x.shape)
print(x.strides)
[[1 2 3]
 [4 5 6]]
int32
(2, 3)
(12, 4)
'''

#########################################################################
# tensor
#########################################################################

x = np.arange(24).reshape((2,3,4))
print(x)

'''
x = np.arange(24).reshape((2,3,4))
print(x)
[[[ 0  1  2  3]
  [ 4  5  6  7]
  [ 8  9 10 11]]

 [[12 13 14 15]
  [16 17 18 19]
  [20 21 22 23]]]

'''


#########################################################################
# create numpy array from function call
#########################################################################


np.fromfunction(lambda i, j: i*3 + j + 1, (2,3), dtype='float')

'''

np.fromfunction(lambda i, j: i*3 + j + 1, (2,3), dtype='float')
Out[107]: 
array([[1., 2., 3.],
       [4., 5., 6.]])

i -> {0,1}
j -> {0,1,2}

'''

#########################################################################
# create special functions
#########################################################################

np.diag([1,2,3,4], k=0)
'''
Out[111]: 
array([[1, 0, 0, 0],
       [0, 2, 0, 0],
       [0, 0, 3, 0],
       [0, 0, 0, 4]])
'''

np.diag([1,2,3,4], k=1)

'''
Out[108]: 
array([[0, 1, 0, 0, 0],
       [0, 0, 2, 0, 0],
       [0, 0, 0, 3, 0],
       [0, 0, 0, 0, 4],
       [0, 0, 0, 0, 0]])
'''

np.diag([1,2,3,4], k=-2)
'''
Out[110]: 
array([[0, 0, 0, 0, 0, 0],
       [0, 0, 0, 0, 0, 0],
       [1, 0, 0, 0, 0, 0],
       [0, 2, 0, 0, 0, 0],
       [0, 0, 3, 0, 0, 0],
       [0, 0, 0, 4, 0, 0]])
'''

#########################################################################
# distributions
#########################################################################

np.random.uniform(0, 1, (2,3))
'''
Out[113]: 
array([[0.21464071, 0.79183388, 0.42103122],
       [0.58212969, 0.21922488, 0.00529842]])

plt.plot(np.random.uniform(0, 1, 100))

'''

np.random.normal(0, 1, (2,3))
'''
Out[117]: 
array([[-0.41460984, -0.31127859,  1.69895759],
       [ 1.03463915, -0.4468166 , -0.88719476]])

plt.plot(np.random.normal(0, 1, 100))

'''

np.random.randint(0, 10, (4,5))

np.random.poisson(10, (4,5))
plt.hist(np.random.poisson(10, 100))

np.random.multinomial(n=5, pvals=np.ones(5)/5, size=8)
np.random.multivariate_normal(mean=[10,20,30], cov=np.eye(3), size=4)


x = np.random.permutation(list('ABCDEF'))
x
np.random.choice(x, 3)
np.random.choice(x, 10)
try:
    np.random.choice(x, 10, replace=False)
except ValueError as e:
    print(e)





np.zeros((3,2)) + np.ones(2)

'''

ones are broadcast as the shape are compatible

np.zeros((3,2)) + np.ones(2)
Out[130]: 
array([[1., 1.],
       [1., 1.],
       [1., 1.]])

'''

np.zeros((3,2)) + np.ones(3).reshape(3,1)

'''
np.zeros((3,2)) + np.ones(3).reshape(3,1)
Out[134]: 
array([[1., 1.],
       [1., 1.],
       [1., 1.]])

while

np.zeros((3,2)) + np.ones(3)
Traceback (most recent call last):

  File "<ipython-input-133-8c5b50c3c1f3>", line 1, in <module>
    np.zeros((3,2)) + np.ones(3)

ValueError: operands could not be broadcast together with shapes (3,2) (3,) 

'''



x1 = np.zeros((3,4))
x2 = np.ones((3,5))
x3 = np.eye(4)

'''
Binding rows when number of columns is the same
'''

np.r_[x1, x3, x3]

'''
Binding columns when number of rows is the same
'''

np.c_[x1, x2]



x = np.array([1,2,3])

np.repeat(x, 3)

'''
np.repeat(x, 3)
Out[141]: array([1, 1, 1, 2, 2, 2, 3, 3, 3])
'''

np.tile(x, 3)

'''

np.tile(x, 3)
Out[142]: array([1, 2, 3, 1, 2, 3, 1, 2, 3])
'''

