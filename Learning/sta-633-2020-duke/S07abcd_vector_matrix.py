%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

https://nbviewer.jupyter.org/github/cliburn/sta-663-2020/blob/master/notebooks/S07A_Scalars_Annotated.ipynb
https://nbviewer.jupyter.org/github/cliburn/sta-663-2020/blob/master/notebooks/S07B_Vectors_Annotated.ipynb
https://nbviewer.jupyter.org/github/cliburn/sta-663-2020/blob/master/notebooks/S07C_Matrices_Annotated.ipynb
https://nbviewer.jupyter.org/github/cliburn/sta-663-2020/blob/master/notebooks/S07D_Sparse_Matrices_Annotated.ipynb




'''
binary representation of integers
'''


format(16, '032b')

'''
Out[1]: '00000000000000000000000000010000'

'''
format(16 >> 2, '032b')
'''
shift right 2 bits
Out[2]: '00000000000000000000000000000100'
'''


https://en.wikipedia.org/wiki/Single-precision_floating-point_format
'''See Wikipedia for how this binary number is evaluated to 0.15625.'''



from ctypes import c_int, c_float

s = c_int.from_buffer(c_float(0.15625)).value
'''
s
Out[14]: 1042284544
'''
s = format(s, '032b')
s
'''
Out[15]: '00111110001000000000000000000000
''''
rep = {
    'sign': s[:1], 
    'exponent' : s[1:9:], 
    'fraction' : s[9:]
}
rep
'''
Out[16]: {'sign': '0', 
          'exponent': '01111100', 
          'fraction': '01000000000000000000000'}
'''

'''
Most base 10 real numbers are approximations
This is simply because numbers are stored in finite-precision binary format.
Never check for equality of floating point numbers
'''



'''
Associative law doesnt necessarily hold
'''
6.022e23 - 6.022e23 + 1 == 1 + 6.022e23 - 6.022e23

Out[17]: False




'''
Distributive law definitely doesnt hold
'''

a = np.exp(1)
b = np.pi
c = np.sin(1)
a*(b+c) == a*b + a*c

Out[21]: False





'''
Vector space
'''

x = np.random.random((5,1))
x

Out[41]: 
array([[0.80880154],
       [0.77833998],
       [0.99049119],
       [0.04430574],
       [0.60575567]])


'''
The length of a vector is the Euclidean norm
Recall that the 'norm' of a vector v, denoted ||v|| is 
simply its length. For a vector with components

'''


len(x)
Out[42]: 5

np.linalg.norm(x)
Out[43]: 1.6155335875998627

np.sqrt(np.sum(x**2))
Out[44]: 1.6155335875998627


'''reshape(-1,1) means 1 col but dont have to calc/specify 
or know the number of rows'''
u = np.array([3,0]).reshape((-1,1))
v = np.array([0,4]).reshape((-1,1))

'''the distance between 2 vectors has no sign'''
np.linalg.norm(u - v)
Out[49]: 5.0
np.linalg.norm(v - u)
Out[50]: 5.0


'''
Calculating dot products
'''

u = np.array([3,2]).reshape((-1,1))
v = np.array([1,4]).reshape((-1,1))


np.dot(u.T, v)
u.T @ v

'''
Geometrically, the dot product is the product of the length
 of v and the length of the projection of u onto the unit 
 vector vˆ.

u⋅v=|u||v|cosθ
'''

plt.plot(*zip(np.zeros_like(u), u), 'b-')
plt.text(1.8, 0.1, 'v', fontsize=14)
plt.plot(*zip(np.zeros_like(v), v), 'b-')
plt.text(2.8, 2.6, 'u', fontsize=14)
plt.tight_layout()
pass


'''
Angle between two vectors
'''

np.linalg.norm(u) == np.sqrt(np.sum(u**2))
Out[67]: True

dotproduct = np.dot(u.T, v)
ulen_times_vlen = (np.linalg.norm(u)*np.linalg.norm(v))
cos_angle = dotproduct / ulen_times_vlen

Out[61]: array([[0.73994007]])

theta = 180/np.pi*np.arccos(cos_angle)

theta


'''
inner product of 2 vectors is the matrix multiplication 
of 1xn with nx1 -> scalar

outter product of 2 vectors is the opposite. Its the 
matrix multiplication of nx1 with 1xn -> n x n 

'''

u.T @ v
Out[70]: array([[11]])

u @ v.T
Out[71]: 
array([[ 3, 12],
       [ 2,  8]])



'''

'''
n, p = 10, 3
x = np.random.random((n, p))
x


np.cov(x, rowvar=False)
v = x - x.mean(axis=0)
(v.T @ v)/9



A = np.random.poisson(0.2, (5,15)) * np.random.randint(0, 10, (5, 15))
A

rows, cols = np.nonzero(A)
vals = A[rows, cols]

X1 = sparse.coo_matrix(A)
X1
















