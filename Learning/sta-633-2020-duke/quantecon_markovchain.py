import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import quantecon as qe
from mpl_toolkits.mplot3d import Axes3D


#probabilities of the transition matrix in this case is row based. ie the
#row adds to 1

P = [[0.4, 0.6],
     [0.2, 0.8]]

# P = np.asarray(P)

# P_dist = [np.cumsum(P[i, :]) for i in range(len(P))]

# P_dist is an list of array. each array is the cumlative prob for a state
# therefore if random draw from a uniform random distribution is made it can 
# mapped to the CDF therefore it means a random draw in the state's probabilities
# therefore the outup of the random draw will be an index of the next state

def mc_sample_path(P, theta0=None, sample_size=1_000):

    # set up
    P = np.asarray(P)
    X = np.empty(sample_size, dtype=int)

    # Convert each row of P into a cdf
    n = len(P)
    P_dist = [np.cumsum(P[i, :]) for i in range(n)]

    # draw initial state, defaulting to 0
    if theta0 is not None:
        X_0 = qe.random.draw(np.cumsum(theta0))
    else:
        X_0 = 0

    # simulate
    X[0] = X_0
    for t in range(sample_size - 1):
        X[t+1] = qe.random.draw(P_dist[X[t]])
        
    return X

_theta0 = [0.1, 0.9] #initial states

# the output of this function call is a list state represented by their index
# from the transition matrix ie the row index

mc_sample_path(P, theta0=_theta0, sample_size=10)















