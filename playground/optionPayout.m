xdata=2.5:.25:5;
ydata=[.001  .01 .022 .034 .057 .07 .1 .2 .3 .4 .5];
xdata2 = xdata-4;

[zp,resnorm,residual,exitflag,output,lambda,jacobian] = lsqcurvefit(@ff,[10 1 5 .5 4]' ,xdata,ydata);
[zp2,resnorm2,residual2,exitflag2,output2,lambda2,jacobian2] = lsqcurvefit(@ff2,[10 1 5 .5]' ,xdata2,ydata);

%yp=fun(zp,xdata)
%plot(xdata,ydata); hold on;
%plot(xdata,yp);


%%

t = 1/365 * [[7, 14, 30, 60, 90, 180, 365] [1:1:10] * 365]; % time ranges from 7 days to 10yr in 
nvol = 25:1:300;
[t, nvol] = ndgrid(t, nvol);
tnovl = [t(:), nvol(:)];
FmS = 0:1:500; % from 0 to 500bps moneyness




%% 


%  fwd = 3.83;
%  strike = 3.83;
%  expiry = 0.5;
%  volAnnualised = 118.29;
%  swaptionType = 'Payer';
%  logOrNormal = 'Normal';
%  bsPremium(fwd,strike,expiry,volAnnualised, swaptionType, logOrNormal)
% 
%  fwd = 3.911;
%  strike = 3.911;
%  expiry = 10;
%  volAnnualised = 85.39;
%  swaptionType = 'Payer';
%  logOrNormal = 'Normal';
%  bsPremium(fwd,strike,expiry,volAnnualised, swaptionType, logOrNormal)
% 
% 
%  fwd = 3.911;
%  strike = 4.2;
%  expiry = 10;
%  volAnnualised = 86.94;
%  swaptionType = 'Receiver';
%  logOrNormal = 'Normal';
%  bsPremium(fwd,strike,expiry,volAnnualised, swaptionType, logOrNormal)
%% sample swaption payer

 strike = 3.83;
 fwds = 3.83 + [-2:0.01:2];
 intrinsicVals = arrayfun(@(fwd) ifelse(fwd-strike<0,0,fwd-strike), fwds);
 expiry = 0.5;
 volAnnualised = 118.29;
 swaptionType = 'Payer';
 logOrNormal = 'Normal';
 moneyness = fwds - strike;
 
 prems = arrayfun(@(fwd) bsPremium(fwd,strike,expiry,volAnnualised, swaptionType, logOrNormal), fwds);

%decomp premium

part1 = moneyness .* normcdf(moneyness/(volAnnualised/100 * sqrt(expiry)));
part2 = ((volAnnualised/100 * sqrt(expiry)) / sqrt(2 * pi)) * exp(-(moneyness/(volAnnualised/100 * sqrt(expiry))).^2 /2);

plot(moneyness, [part1; part2; part1+part2; intrinsicVals]); grid on;
xlabel('moneyness %');
ylabel('premium in yield %');
legend('Moneyness * N(Z)', 'V(t) * std PDF', 'prem = Moneyness * N(Z) + V(t) * std PDF', 'Intrinsic Val'); 




%% ben method fit

%using ff2 ie moneyness as x

xdata= moneyness; %from section above
ydata= prems;     %from section above

z(1) = 0;  %?
z(2) = 0;   %?
z(3) = 0;    %?
z(4) = 0.5;  %?


options = optimoptions('lsqcurvefit', 'MaxFunctionEvaluations', 4000,'MaxIterations',4000);
[zp,resnorm,residual,exitflag,output,lambda,jacobian] = lsqcurvefit(@BenOvFit,z ,xdata,ydata,[-Inf -Inf -Inf 0],[],options);
yp=BenOvFit(zp,xdata);

ls = plot(moneyness, [part1; part2; part1+part2; intrinsicVals; yp]); grid on;
xlabel('moneyness %');
ylabel('premium in yield %');
legend('Moneyness * N(Z)', 'V(t) * std PDF', 'prem = Moneyness * N(Z) + V(t) * std PDF', 'Intrinsic Val', 'Ben Fit'); 


set(ls(5), 'Color', 'k', 'LineStyle', ':')

%%

z(1) = 0;  %?
z(2) = 0;   %?
z(3) = 0;    %?
z(4) = 0.5;  %?
fwdATM = 4.83;
expiry = 2; % in years 
%swaptionType = 'Payer';
swaptionType = 'Receiver';
logOrNormal = 'Normal';
volAnnualised = 250.29;
fwdRange =  [-5:0.01:5]


output = testFit(z, @BenOvFit, volAnnualised, fwdATM, expiry, fwdRange, swaptionType, logOrNormal);


 
%%

function output = testFit(z, fitFun, volAnnualised, fwdATM, expiry, fwdRange, swaptionType, logOrNormal)
    
    arguments
        z,
        fitFun,
        volAnnualised
        fwdATM
        expiry % in years 
        fwdRange =  [-2:0.01:2]  % go for 2% up and down        
        swaptionType = 'Payer'
        logOrNormal = 'Normal'         
    end
    

     strike = fwdATM;
     fwds = fwdATM + fwdRange;

     if (strcmpi(upper(swaptionType),'PAYER')) 
        intrinsicVals = arrayfun(@(fwd) ifelse(fwd-strike<0,0,fwd-strike), fwds);     
     else 
        intrinsicVals = arrayfun(@(fwd) ifelse(strike-fwd<0,0,strike-fwd), fwds);     
     end 
     
     moneyness = ifelse(strcmpi(upper(swaptionType),'PAYER'),  fwds - strike,  strike - fwds );
     
     prems = arrayfun(@(fwd) bsPremium(fwd,strike,expiry,volAnnualised, swaptionType, logOrNormal), fwds);

     %decomp premium    
     part1 = moneyness .* normcdf(moneyness/(volAnnualised/100 * sqrt(expiry)));
     part2 = ((volAnnualised/100 * sqrt(expiry)) / sqrt(2 * pi)) * exp(-(moneyness/(volAnnualised/100 * sqrt(expiry))).^2 /2);

     options = optimoptions('lsqcurvefit', 'MaxFunctionEvaluations', 4000,'MaxIterations',4000);
     xdata= moneyness; 
     ydata= prems;     
     [zp,resnorm,residual,exitflag,fitOutput,lambda,jacobian] = lsqcurvefit(fitFun,z ,xdata,ydata,[-Inf -Inf -Inf 0],[], options);
     yp=fitFun(zp,xdata);
    

     t = tiledlayout(7, 1, 'TileSpacing', 'compact', 'Padding', 'compact');

     nexttile([6, 1]);

     lChart = plot(moneyness, [part1; part2; part1+part2; intrinsicVals; yp]); grid on;
     xlabel('moneyness %');
     ylabel('premium in yield %');
     legend('Moneyness * CDF(Z)', 'V(t) * std PDF', 'prem = Moneyness * CDF(Z) + V(t) * std PDF', 'Intrinsic Val', 'Ben Fit'); 
     
     if (~strcmpi(upper(swaptionType),'PAYER')) 
        set(gca, 'XDir', 'reverse'); 
     end
        
     title(sprintf('[%.2f] expiry %s %s with V(t) = [%.2f] * sqrt([%.2f]) = [%.0f] bps', expiry, logOrNormal, swaptionType,volAnnualised, expiry, volAnnualised * sqrt(expiry)));

     set(lChart(5), 'Color', 'k', 'LineStyle', ':');

     nexttile(7,[1,1]);

     errChart = plot(moneyness, [ydata - yp]); grid on;
     xlabel('moneyness %');
     ylabel('fit residual bps ');
     
     if (~strcmpi(upper(swaptionType),'PAYER')) 
        set(gca, 'XDir', 'reverse'); 
     end
        
     title(sprintf('fit residual ydata-fit'));

     fig = gcf; % Get the current figure handle
     fig.Position = [fig.Position(1), fig.Position(2)-300, fig.Position(3), 600]; 


     output.moneyness = moneyness;
     output.intrinsicVals = intrinsicVals;
     output.prems = prems;
     output.prems_CDFWhtedMoneyness = part1;
     output.prems_optionality = part2;
     output.testPlot = lChart;
     output.zp = zp;
     output.yp = yp;


end





function output = ff(z,y)
    output = z(1)*exp(-z(2)*abs(y-z(5))-z(3))+z(4)*max(y-z(5),0);    
end 

function output = BenOvFit(z,y)
    output =  z(1)*exp(-z(2)*abs(y)-z(3))+z(4)*max(y,0);
end
    


function output = dBenOvFitdx(z,y)
    output = -z(2) * z(1) * sign(y) * exp(-z(2)*abs(y)-z(3))+ z(4)*max(y,0);
end



function [prem] = bsPremium(fwd,strike,expiry,volAnnualised, swaptionType, logOrNormal)
    arguments
        fwd
        strike,
        expiry,
        volAnnualised, 
        swaptionType {mustBeMember(swaptionType, {'Payer', 'Receiver'})} 
        logOrNormal {mustBeMember(logOrNormal, {'Log', 'Normal'})}
    end
    prem = nan;
    payer_prem = nan;
    receiver_prem = nan;
    % volAnnualised is in bps of underline
    vt = volAnnualised * sqrt(expiry) / 100;

    if strcmp(logOrNormal, 'Log') 
        x = (log(fwd/strike) + vt * vt / 2)/ vt;
        N1 = normcdf(x);
        N2 = normcdf(x - vt);
        payer_prem = fwd .* N1 - strike .* N2;
    elseif strcmp(logOrNormal, 'Normal')
        x = (fwd - strike) / vt;
        N1 = normcdf(x);
        payer_prem = (fwd - strike) .* N1 + (vt / sqrt(2 * pi)) * exp(-(x * x) /2);  
    else
        error("logOrNormal param is either Log or Normal")
    end
    
    receiver_prem = payer_prem - (fwd - strike);

    if strcmp(swaptionType,"Payer")
        prem = payer_prem ;
    else
        prem = receiver_prem ;
    end
end

%{
[p, r] = bsPremium(2.8, 3.1, 0.25, 13, "Payer", "Log")
p = 0.00481958406460797
r = 0.304819584064608

[p, r] = bsPremium(1.32, 1.5, 5, 52, "Payer", "Normal")
p = 0.37941942954578
r = 0.55941942954578
%}