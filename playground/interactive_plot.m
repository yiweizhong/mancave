function interactive_plot()
    % Create a figure
    f = figure;

    % Create a slider for z
    sz = uicontrol('Style', 'slider', 'Min',-10, 'Max',10, 'Value',0, 'Position', [100 20 120 20]);

    % Add listener to slider
    addlistener(sz, 'Value', 'PostSet', @(src, event) updatePlot());

    % Define x and y ranges
    [x, y] = meshgrid(-10:0.5:10, -10:0.5:10);

    % Initial plot
    updatePlot();

    function updatePlot()
        z = get(sz, 'Value');
        
        % Calculate function values
        fval = x.^2 + y.^2 + z^2;

        % Update plot
        clf; % Clear current figure

        % Contour plot (uncomment the one you want to use)
        % contour(x, y, fval);
        
        % OR Surface plot
        surf(x, y, fval);
        xlabel('X-axis');
        ylabel('Y-axis');
        zlabel('Function Value');
        title(sprintf('f(x, y, z) = x^2 + y^2 + %f^2', z));
        colorbar; % Adds a color bar to indicate values
    end
end